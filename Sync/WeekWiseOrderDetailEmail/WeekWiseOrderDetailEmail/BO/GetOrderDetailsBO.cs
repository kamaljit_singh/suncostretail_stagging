﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyOrderEmail.BO
{
    public class GetOrderDetailsBO
    {

        public string CustomerName { get; set; }
        public string Device { get; set; }
        public long OrderNumber { get; set; }
        public double? OrderValue { get; set; }
     

    }
}
