﻿using DailyOrderEmail.BO;
using DailyOrderEmail.DA;
using OrderPostingToFTP.DA;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace DailyOrderEmail
{
    class Program
    {
        static void Main(string[] args)
        {

            Program obj = new Program();
            obj.log("--------------------------------------------------------", "//");
            obj.log("Start", "Suncoast Week Wise Order Detail Email");
            obj.log("--------------------------------------------------------", "//");
            Console.Write("Start of Suncoast Week Wise Order Detail Email\n");
            try
            {
                using (var _db = new Entities())
                {
                    DateTime PresentDate = Convert.ToDateTime(getMST(DateTime.Now).ToString("yyyy-MM-dd"));
                    DateTime FromDate = Convert.ToDateTime(getMST(DateTime.Now).AddDays(-14).ToString("yyyy-MM-dd"));
                    DateTime ToDate = Convert.ToDateTime(getMST(DateTime.Now).AddDays(-8).ToString("yyyy-MM-dd"));
                    DateTime CurrentFromDate = Convert.ToDateTime(getMST(DateTime.Now).AddDays(-7).ToString("yyyy-MM-dd"));
                    DateTime CurrentToDate = Convert.ToDateTime(getMST(DateTime.Now).AddDays(-1).ToString("yyyy-MM-dd"));
                    decimal Totalweblast7days = 0;
                    decimal Totaliphonelast7days = 0;
                    decimal Totalandroidlast7days = 0;

                    decimal Totalwebcurrent7days = 0;
                    decimal Totaliphonecurrent7days = 0;
                    decimal Totalandroidcurrent7days = 0;

                    decimal Percentweblast7days = 0;
                    decimal Percentiphonelast7days = 0;
                    decimal Percentandroidlast7days = 0;

                    decimal Percentwebcurrent7days = 0;
                    decimal Percentiphonecurrent7days = 0;
                    decimal Percentandroidcurrent7days = 0;

                    decimal LastTotal = 0;
                    decimal CurrentTotal = 0;
                    var ctd = _db.trnsCartMasters.Where(c => DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(FromDate.Date) && c.DeviceType!=null).ToList();
                    if (ctd != null)
                    {

                        var weblast7days = ctd.Where(c => c.CreatedDate >= FromDate && c.CreatedDate <= ToDate.Date && c.DeviceType.ToLower() == "web").ToList();
                        if (weblast7days != null)
                        {
                            LastTotal = LastTotal + weblast7days.Count;
                            Totalweblast7days = weblast7days.Count;
                        }
                        var iphonelast7days = ctd.Where(c => c.CreatedDate >= FromDate && c.CreatedDate <= ToDate && c.DeviceType.ToLower() == "iphone").ToList();
                        if (iphonelast7days != null)
                        {
                            LastTotal = LastTotal + iphonelast7days.Count;
                            Totaliphonelast7days = iphonelast7days.Count;
                        }

                        var androidlast7days = ctd.Where(c => c.CreatedDate >= FromDate.Date && c.CreatedDate <= ToDate.Date && c.DeviceType.ToLower() == "android").ToList();
                        if (androidlast7days != null)
                        {
                            LastTotal = LastTotal + androidlast7days.Count;
                            Totalandroidlast7days = androidlast7days.Count;
                        }
                        var webcurrent7days = ctd.Where(c => c.CreatedDate >=CurrentFromDate.Date && c.CreatedDate <= CurrentToDate.Date && c.DeviceType.ToLower() == "web").ToList();

                        if (webcurrent7days != null)
                        {
                            CurrentTotal = CurrentTotal + webcurrent7days.Count;
                            Totalwebcurrent7days = webcurrent7days.Count;
                        }
                        var iphonecurrent7days = ctd.Where(c => c.CreatedDate >= CurrentFromDate.Date && c.CreatedDate <= CurrentToDate.Date && c.DeviceType.ToLower() == "iphone").ToList();
                        if (iphonecurrent7days != null)
                        {
                            CurrentTotal = CurrentTotal + iphonecurrent7days.Count;
                            Totaliphonecurrent7days = iphonecurrent7days.Count;
                        }
                        var androidcurrent7days = ctd.Where(c => c.CreatedDate >= CurrentFromDate.Date && c.CreatedDate <=CurrentToDate.Date && c.DeviceType.ToLower() == "android").ToList();
                        if (androidcurrent7days != null)
                        {
                            CurrentTotal = CurrentTotal + androidcurrent7days.Count;
                            Totalandroidcurrent7days = androidcurrent7days.Count;
                        }

                        if (LastTotal != 0)
                        {
                            Percentweblast7days = ((decimal)Totalweblast7days / (decimal)LastTotal) * 100;
                            Percentiphonelast7days = ((decimal)Totaliphonelast7days / (decimal)LastTotal) * 100;
                            Percentandroidlast7days = 100 - decimal.Round(((decimal)Totalweblast7days / (decimal)LastTotal) * 100,2) - decimal.Round(((decimal)Totaliphonelast7days / (decimal)LastTotal) * 100,2); //(decimal)((decimal)Totalandroidlast7days / (decimal)LastTotal) * 100;
                          
                        }

                        if (CurrentTotal != 0)
                        {
                            Percentwebcurrent7days = ((decimal)Totalwebcurrent7days / (decimal)CurrentTotal) * 100;
                            Percentiphonecurrent7days = ((decimal)Totaliphonecurrent7days / (decimal)CurrentTotal) * 100;
                            Percentandroidcurrent7days = 100 -decimal.Round(((decimal)Totaliphonecurrent7days / (decimal)CurrentTotal) * 100,2) - decimal.Round(((decimal)Totalwebcurrent7days / (decimal)CurrentTotal) * 100,2);//((decimal)Totalandroidcurrent7days / (decimal)CurrentTotal) * 100;
                            
                        }

                        Console.Write("Suncoast Week Wise Order Detail Fetching data\n");

                        var records = ctd.Where(c => c.CreatedDate >= CurrentFromDate.Date && c.CreatedDate <= CurrentToDate.Date && (c.DeviceType.ToLower() == "android" || c.DeviceType.ToLower() == "iphone")).ToList();
                        List<GetOrderDetailsBO> li = new List<GetOrderDetailsBO>();
                        foreach (var cartV in records)
                        {
                            var data = (from a in _db.trnsCartMasters
                                        join cust in _db.CustomerMasters on a.CustomerID equals cust.CustomerID
                                        where (a.CartID == cartV.CartID
                                                && (a.DeviceType != null ||  a.DeviceType.ToLower() == "iphone" || a.DeviceType.ToLower() == "android")
                                               )
                                        let freightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? a.FrieghtCharges : 0
                                        let Calprice = (double?)((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == a.CartID)
                                                                  select new { Price = cd.Price ?? 0, Quantity = cd.Quantity ?? 0 }).ToList().Select(c => (c.Price * c.Quantity)).Sum()) ?? 0
                                        let FraightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? (double?)(from soh in _db.DeliveryCharges
                                                                                                                                     select new { FraightCharge = soh.DeliveryFee }).Take(1).Select(c => c.FraightCharge).Sum() ?? 0 : 0
                                        let coupanAmount = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.CouponAmount : 0
                                        let originalPrice = (double?)((from cd in _db.trnsCartItemsMasterHistories.Where(s => s.CartID == a.CartID)
                                                                       select new
                                                                       {
                                                                           Price = cd.Price ?? 0,
                                                                           Quantity = cd.Quantity ?? 0
                                                                       }).ToList().Select(c => (c.Price * c.Quantity)).Sum()) ?? 0
                                        select new GetOrderDetailsBO
                                        {
                                            Device = a.DeviceType,
                                            OrderNumber = a.CartID,
                                            OrderValue = (double?)((originalPrice == 0 && Calprice != 0) ? (Calprice - coupanAmount + FraightCharge) : (originalPrice - coupanAmount + FraightCharge)),
                                            CustomerName = cust.CustomerName

                                        }).ToList();
                            if (data != null && data.Count > 0)
                            {

                                li.AddRange(data);
                            }
                        }


                        Console.Write("Suncoast Week Wise Order Detail Fetching data Complete\n");

                        string subject = ConfigurationManager.AppSettings["CompanyEmailSubject"].ToString();
                        string Tbody = "";
                        foreach (var item in li)
                        {
                            string Device = item.Device.ToLower() == "android" ? "Android" : "IOS";
                            Tbody = Tbody + "<tr><td style='padding-left:4px'> " + item.CustomerName + "</td><td style='padding-left:20px'>" + item.OrderNumber + "</td><td style='padding-left:10px'>" + Device + "</td><td style='padding-left:15px'>$" + Math.Round(Convert.ToDouble(item.OrderValue),2).ToString("0.00") + "</td ></tr>";

                        }
                        string body = "Dear Suncoast Fresh,"
                                + "<br/><br/>"
                                + "<b><U>LAST WEEKS DEVICE ANALYTICS DATA</U></b>"
                                + "<br/><br/>"
                                + "<b>Please find below last weeks customer ordering device analytics:</b>"
                                + " <br/><br/>"
                                + "<table><tr><td style='width:40%'>Report Date:</td><td style='width:40%'><b>" + PresentDate.ToShortDateString() + "</b></td></tr>"
                                + "<tr><td>Period:</td><td><b>Last week from " + CurrentFromDate.ToShortDateString() + " – " + CurrentToDate.ToShortDateString() + " inclusive</b></td></tr>"
                                + "<tr><td>Total number of customers:</td><td><b>" + CurrentTotal + "</b></td></tr>"
                                + "<tr><td>(a customer defined as anyone who registered in SAAVI)</td><td></td></tr></table>"
                                + "<br/><br/>"
                               + "<table style='width:100%'><tr><td style='width:30%'><b><u>Device Analytics:</u></b></td><td style='width:10%'><b><u>This Week</u></b></td><td style='width:10%'><b><u>As a %</u></b></td><td  style='width:10%'><b><u>Last week</u></b></td><td  style='width:40%'><b><u>As a %</u></b></td></tr>"
                               + "<tr><td>IOS:</td><td style='padding-left:10px;'>" + Totaliphonecurrent7days + "</td><td style='padding-left:4px;'>" + Decimal.Round(Percentiphonecurrent7days,2).ToString("0.00") + "%</td><td style='padding-left:13px;'>" + Totaliphonelast7days + "</td><td style='padding-left:4px;'>" + Decimal.Round(Percentiphonelast7days, 2).ToString("0.00") + "%</td></tr>"
                               + "<tr><td>Android:</td><td style='padding-left:10px;'>" + Totalandroidcurrent7days + "</td><td style='padding-left:4px;'>" + Decimal.Round(Percentandroidcurrent7days,2).ToString("0.00")+ "%</td><td style='padding-left:13px;'>" + Totalandroidlast7days + "</td><td style='padding-left:4px;'>" + Decimal.Round(Percentandroidlast7days,2).ToString("0.00") + "%</td></tr>"
                               + "<tr><td>Online Shop:</td><td style='padding-left:10px;'>" + Totalwebcurrent7days + "</td><td style='padding-left:4px;'>" + Decimal.Round(Percentwebcurrent7days,2).ToString("0.00") + "%</td><td style='padding-left:13px;'>" + Totalweblast7days + "</td><td style='padding-left:4px;'>" + Decimal.Round(Percentweblast7days,2).ToString("0.00") + "%</td></tr>"
                               + "<tr><td>Total Customers:</td><td style='padding-left:10px;'>" + CurrentTotal + "</td><td style='padding-left:4px;'>100.00%</td><td style='padding-left:13px;'>" + LastTotal + "</td><td style='padding-left:4px;'>100.00%</td></tr></table>"
                               + "<br/><br/>"
                               + "<b>Customers who placed an order from a mobile device last week:</b>"
                               + "<br/><br/>"
                               + "<table style='width:62%;'><tr><td><b><u>Customer Name</u></b></td><td><b><u>Order Number</u></b></td><td><b><u>DeviceType</u></b></td><td><b><u>Order Value</u></b></td></tr>"
                               + Tbody
                               + "</table><br/><br/>"
                               + "From the team at SAAVI";

                        string result = SendMail.SendEMail(ConfigurationManager.AppSettings["CompanyAdminEmail"].ToString(), ConfigurationManager.AppSettings["CompanyAdminEmail_CC"].ToString(), "", subject, body, "", ConfigurationManager.AppSettings["FROM_EMAILID"].ToString());
                        Console.Write("Suncoast Week Wise Order Detail Email sent \n");
                        if (result == "ok")
                        {
                            obj.log("--------------------------------------------------------", "//");
                            obj.log("Email Sent Successfully", "Info");
                            obj.log("--------------------------------------------------------", "//");
                        }
                        else
                        {
                            obj.log("--------------------------------------------------------", "//");
                            obj.log(result, "Error Email");
                            obj.log("--------------------------------------------------------", "//");
                        }

                    }




                }
            }
            catch (Exception ex)
            {
                obj.log("Error Occured during getting order", ex.Message);
                throw ex;
            }
        }



        #region Send Order Summary

        public DateTime getServerDateTime()
        {

            using (var _db = new Entities())
            {
                var dateQuery = _db.Database.SqlQuery<DateTime>("SELECT getdate()");
                DateTime serverDate = dateQuery.AsEnumerable().First();
                return Convert.ToDateTime(serverDate);
            }
        }
        /// <summary>
        /// get AUS Eastern standard time
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static DateTime getMST(DateTime val)
        {
            DateTime serverTime = Convert.ToDateTime(val); // gives you current Time in server timeZone
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZone"].ToString());
            DateTime dt = TimeZoneInfo.ConvertTime(serverTime, tzi);
            return dt;
        }

        #region Create log for the actions.
        public void log(string ex, string type)
        {
            if (System.IO.File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (System.IO.Stream str = System.IO.File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.


        #endregion
    }
}
