﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace syncNotYetOrderedOrStoppedOrderingReport
{
    public class Order
    {
        #region CSVForCartAbandonmentReport      
        public string CSVForCartAbandonmentReport(List<ItemRowTable1> li, DateTime ReportDate, string PrefixName)
        {
            if(li.Count == 0)
            {
                return "";
            }
            string dy = ReportDate.Day.ToString("00");
            string mn = ReportDate.Month.ToString("00");
            string yy = ReportDate.Year.ToString();

            string newFileName = PrefixName+"_" + dy + "" + mn + "" + yy + ".csv";  //"Suncoast_Fresh_Manifest_" + dy + "" + mn + "" + yy + ".csv";
            string filePath = ConfigurationManager.AppSettings["CSVPath"].ToString() + newFileName;

            StringBuilder sb = new StringBuilder();
            string dataH = string.Empty;
            dataH = string.Format("{0},{1}",

               "CUSTOMER NAME",
               "EMAIL"           
                );
            sb.Append(dataH);
            sb.AppendLine();

            string data = string.Empty;
            for (int i = 0; i <= li.Count - 1; i++)
            {             
                data = string.Format("{0},{1}",           
              
                li[i].CustomerName,
                li[i].Email
                 );
                sb.Append(data);
                sb.AppendLine();
            }         

            File.AppendAllText(filePath, sb.ToString());

            return filePath;
        }
        #endregion
        #region CSVForLast2WeeksReport
        public string CSVForLast2WeeksReport(List<ItemRowTable2> li, DateTime ReportDate, string PrefixName)
        {
            if (li.Count == 0)
            {
                return "";
            }
            string dy = ReportDate.Day.ToString("00");
            string mn = ReportDate.Month.ToString("00");
            string yy = ReportDate.Year.ToString();

            string newFileName = PrefixName + "_" + dy + "" + mn + "" + yy + ".csv";  //"Suncoast_Fresh_Manifest_" + dy + "" + mn + "" + yy + ".csv";
            string filePath = ConfigurationManager.AppSettings["CSVPath"].ToString() + newFileName;

            StringBuilder sb = new StringBuilder();
            string dataH = string.Empty;
            dataH = string.Format("{0},{1},{2},{3}",

               "CUSTOMER NAME",
               "EMAIL",
               "LAST ORDER NUMBER",
               "TOTAL ORIGINAL ORDER VALUE"
           
                );
            sb.Append(dataH);
            sb.AppendLine();

            string data = string.Empty;
            for (int i = 0; i <= li.Count - 1; i++)
            {
                data = string.Format("{0},{1},{2},{3}",
                li[i].CustomerName,
                li[i].Email,
                li[i].LastOrderNumber,
                li[i].TotalOriginalOrderValue
                 );
                sb.Append(data);
                sb.AppendLine();
            }
       

            File.AppendAllText(filePath, sb.ToString());


            return filePath;
        }
        #endregion

        #region CSVForLast1WeeksTempCartReport
        public string CSVForLast1WeeksTempCartReport(List<ItemRowTable3> li, DateTime ReportDate, string PrefixName)
        {
            if (li.Count == 0)
            {
                return "";
            }
            string dy = ReportDate.Day.ToString("00");
            string mn = ReportDate.Month.ToString("00");
            string yy = ReportDate.Year.ToString();

            string newFileName = PrefixName + "_" + dy + "" + mn + "" + yy + ".csv";  //"Suncoast_Fresh_Manifest_" + dy + "" + mn + "" + yy + ".csv";
            string filePath = ConfigurationManager.AppSettings["CSVPath"].ToString() + newFileName;

            StringBuilder sb = new StringBuilder();
            string dataH = string.Empty;
            dataH = string.Format("{0},{1},{2},{3},{4}",

               "TEMP CART ID",
               "CUSTOMER NAME",
               "EMAIL",
               "TEMP CART ORDER DATE",
               "TOTAL TEMP CART ORDER VALUE"
                );
            sb.Append(dataH);
            sb.AppendLine();

            string data = string.Empty;
            for (int i = 0; i <= li.Count - 1; i++)
            {
                data = string.Format("{0},{1},{2},{3},{4}",
                 li[i].TempCartID,               
                li[i].CustomerName,
                li[i].Email,
                li[i].TempCartOrderDate.ToString("dd/MM/yyy"),
                "$" + li[i].TotalTempCartOrderValue
               
                 );
                sb.Append(data);
                sb.AppendLine();
            }
   

            File.AppendAllText(filePath, sb.ToString());


            return filePath;
        }
        #endregion

        #region GetServerDateTime
        public DateTime getServerDateTime()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            conn.Open();
            using (var cmd = new SqlCommand("select getdate()", conn))
            {
                return Convert.ToDateTime(cmd.ExecuteScalar());
            }
        }
        #endregion

        #region Read Email Template File and Replace File Variables


        private static string ReadEmailTemplateFile(string filePath, string fileName)
        {
            string str = string.Empty;

            string path = filePath + fileName;
            //return path;
            if (System.IO.File.Exists(path))
            {
                try
                {
                    StreamReader reader = System.IO.File.OpenText(path);
                    str = reader.ReadToEnd();
                    reader.Close();
                    return str;
                }
                catch (Exception)
                {
                    throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
                }
            }
            throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
        }

        public static string ReplaceFileVariablesInTemplateFile(Hashtable hashVars, string content)
        {
            IDictionaryEnumerator enumerator = hashVars.GetEnumerator();
            while (enumerator.MoveNext())
            {
                content = content.Replace(enumerator.Key.ToString(), enumerator.Value.ToString());
            }
            return content;
        }
        #endregion
        #region Create log for the actions.

        public void log(string ex, string type)
        {
            if (System.IO.File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (System.IO.Stream str = System.IO.File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.

    }

    public class ItemRowTable1
    {
        public string CustomerName { get; set; }
        public string Email { get; set; }
    }

    public class ItemRowTable2
    {
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public long LastOrderNumber { get; set; }
        public DateTime LastOrderDate { get; set; }
        public double TotalOriginalOrderValue { get; set; }
    }

    public class ItemRowTable3
    {
        public long TempCartID { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public DateTime TempCartOrderDate { get; set; }
        public double TotalTempCartOrderValue { get; set; }
    }
}
