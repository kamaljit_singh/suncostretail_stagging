﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace syncNotYetOrderedOrStoppedOrderingReport
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo di = new DirectoryInfo(Convert.ToString(ConfigurationManager.AppSettings["CSVPath"]));
       
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }          

            Order obj = new Order();
            DateTime ReportDate = obj.getServerDateTime();//.AddDays(-3);

            DateTime ToDate = ReportDate.AddDays(-1);
            DateTime FromDate = ReportDate.AddDays(-7);
            string day = ReportDate.DayOfWeek.ToString();

            if (day.ToUpper() == "MONDAY")
            {
                obj.log("--------------------------------------------------------", "//");
                obj.log("Start", "NotYetOrderedOrStoppedOrdering");
                obj.log("--------------------------------------------------------", "//");

                String dy = ReportDate.Day.ToString("00");
                String mn = ReportDate.Month.ToString("00");
                String yy = ReportDate.Year.ToString();
                string attachments = "";

                List<ItemRowTable1> li = new List<ItemRowTable1>();
                List<ItemRowTable2> li1 = new List<ItemRowTable2>();
                List<ItemRowTable3> li2 = new List<ItemRowTable3>();
              
                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spNotYetOrderedOrStoppedOrdering", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@Todate", ToDate);
                da.SelectCommand.Parameters.AddWithValue("@Fromdate", FromDate);

                da.Fill(ds);

                li = ConvertDataTable<ItemRowTable1>(ds.Tables[0]);
                li1 = ConvertDataTable<ItemRowTable2>(ds.Tables[1]);
                li2 = ConvertDataTable<ItemRowTable3>(ds.Tables[2]);


                string f1 = obj.CSVForCartAbandonmentReport(li, ReportDate, "Customers_Who_Have_Registered_But_Never_Ordered");

                if (f1 != "")
                {
                    attachments += f1 + ";";
                }

                string f2 = obj.CSVForLast2WeeksReport(li1, ReportDate, "Last_2_Weeks");

                if (f2 != "")
                {
                    attachments += f2 + ";";
                }
                string f3 = obj.CSVForLast1WeeksTempCartReport(li2, ReportDate, "Last_1_Weeks_TempCart");

                if (f3 != "")
                {
                    attachments += f3 + ";";
                }

                if (attachments != "")
                {
                    string subject = "CART ABANDONMENT REPORT – MONDAY  " + dy + "  " + mn + "  " + yy;

                    string body = "Dear Suncoast Admin,"
                          + "<br/><br/>"
                          + "Please find below the cart abandonment report."
                          + " <br/><br/>"
                          + "If there are any errors or omissions, please email support@saavi.com.au"
                          + " <br/><br/>"
                          + "Regards"
                          + "<br/><br/>"
                          + "Many Thanks <br/>"
                          + "From the team at SAAVI";

                    string result = SendMail.SendEMail(ConfigurationManager.AppSettings["CompanyAdminEmail"].ToString(), ConfigurationManager.AppSettings["CompanyAdminEmail_CC"].ToString(), "", subject, body, attachments, ConfigurationManager.AppSettings["FROM_EMAILID"].ToString());

                    if (result == "ok")
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Sent Successfully", "Cart Abandonment");
                        obj.log("--------------------------------------------------------", "//");
                    }
                    else
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Not Sent", result);
                        obj.log("--------------------------------------------------------", "//");
                    }

                }
                obj.log("--------------------------------------------------------", "//");
                obj.log("End", "NotYetOrderedOrStoppedOrdering");
                obj.log("--------------------------------------------------------", "//");

            }
        }

        public static List<T> ConvertDataTable<T>(System.Data.DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }

}
