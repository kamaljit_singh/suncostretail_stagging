﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductSpecifications
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                log("Sync process started.", "Info");

                if (ConfigurationManager.AppSettings["PRODUCT"].ToString().Length > 0)
                {
                    CheckIfFileExistsOnFTPAndFetch(ConfigurationManager.AppSettings["PRODUCT"].ToString());
                }

                runSyncProcedures();
                log("Sync process completed.", "Info");
            }
            catch (Exception ex)
            {
                log(ex.Message, "Error");
            }
        }

        #region Check for file on FTP and initiate download
        public static bool CheckIfFileExistsOnFTPAndFetch(string fileName)
        {
            download(fileName);
            //Deletefiles(fileName);
            return true;
        }
        #endregion

        #region Get data from Excel file 
        public static void download(string fileName)
        {
            DataTable dt = GetDataTableFromExcel(ConfigurationManager.AppSettings["FilePath"] + fileName + ".xlsx");
            if (dt != null)
            {
                //dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull || string.IsNullOrWhiteSpace(field as string))).CopyToDataTable();
                //foreach (var column in dt.Columns.Cast<DataColumn>().ToArray())
                //{
                //    if (dt.AsEnumerable().All(dr => dr.IsNull(column)))
                //        dt.Columns.Remove(column);
                //}
                // DataTable dtTemp = new DataTable();

                //dtTemp.Columns.AddRange(new DataColumn[4] { new DataColumn("Product_Code"), new DataColumn("Product_Features"), new DataColumn("Product_Description"), new DataColumn("Share_URL") });
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    for (int k = 2; k < dt.Columns.Count; k++)
                //    {
                //        if (!string.IsNullOrEmpty(dt.Columns[k].ColumnName))
                //        {
                //            dtTemp.Rows.Add(dt.Rows[i][0], dt.Columns[k].ColumnName, dt.Rows[i][k]);
                //        }
                //    }
                //}

                InsertDataIntoSQLServerUsingSQLBulkCopy(dt);
            }
            else
            {
                log("Failed to read the specified file", "Info");
            }

        }
        public static DataTable GetDataTableFromExcel(string filepath)
        {
            try
            {
                string path = System.IO.Path.GetFullPath(filepath);
                // +TableName;
                OleDbConnection oledbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                path + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                oledbConn.Open();
                string commandTxt = "";
                string table = "";
                DataTable dt2 = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                DataTable dt = new DataTable();
                for (int i = 0; i <= dt2.Rows.Count - 1; i++)
                {
                    table = dt2.Rows[i]["TABLE_NAME"].ToString();

                    commandTxt = "SELECT * FROM [" + dt2.Rows[i]["TABLE_NAME"].ToString() + "]";

                    if (commandTxt != "")
                    {
                        OleDbCommand cmd = new OleDbCommand();
                        OleDbDataAdapter oleda = new OleDbDataAdapter();
                        cmd.Connection = oledbConn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = commandTxt;
                        cmd.CommandTimeout = 0;
                        oleda = new OleDbDataAdapter(cmd);
                        oleda.Fill(dt);
                        oledbConn.Close();

                        commandTxt = "";
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                log("Exception in GetDataTableFromExcel :: " + ex.Message, "Error");
                return null;
            }
        }
        #endregion

        #region Insert bulk data into SQL Server
        private static void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable dt)
        {
            string TableName = "Import_SAAVI_ProductDetails";
            try
            {

                using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString()))
                {
                    dbConnection.Open();
                    SqlCommand cmd = new SqlCommand("Truncate Table " + TableName + ";", dbConnection);
                    cmd.ExecuteNonQuery();
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                    {
                        s.BulkCopyTimeout = 0;
                        s.DestinationTableName = TableName;
                        s.WriteToServer(dt);
                    }
                    log("Bulk inserted " + TableName, "Info");
                }
            }
            catch (Exception ex)
            {
                log("Bulk inserted " + TableName, "error" + ex);
                throw;
            }
        }
        #endregion

        #region Run Sync procedure on Server after updating data
        private static void runSyncProcedures()
        {
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString()))
                {
                    dbConnection.Open();
                    SqlCommand cmd = new SqlCommand("sp_SyncDataProductSpecifications", dbConnection);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                log("Exception in SyncProcedure :: " + ex.Message, "Error");
                throw ex;
            }
        }
        #endregion

        #region Maintain log for all the actions
        private static void log(string ex, string type)
        {
            if (File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (Stream str = File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }
        #endregion

        public static void Deletefiles(string fileName)
        {
            try
            {
                string date = DateTime.Now.ToShortDateString().Replace("/", "") + "T" + DateTime.Now.ToLongTimeString().Replace(":", "_") + Path.GetExtension(fileName);
                string newFileName = Path.GetFileNameWithoutExtension(fileName) + "_" + date;

                string Source = ConfigurationManager.AppSettings["FilePath"] + fileName;
                string Destination = ConfigurationManager.AppSettings["FilePathArchived"] + newFileName;
                System.IO.File.Move(Source, Destination);
            }
            catch (Exception ex)
            {
                log("File not moved :: " + ex.Message, "Error");
            }
        }
    }
}
