﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using System.Drawing;
using Syncfusion.Pdf.Graphics;

namespace syncStripePaymentDetail
{
    class Program
    {
        static void Main(string[] args)
        {

            Program obj = new Program();
            obj.log("--------------------------------------------------------", "//");
            obj.log("Start", "Suncoast Fresh Stripe");
            obj.log("--------------------------------------------------------", "//");

            DirectoryInfo di = new DirectoryInfo(Convert.ToString(ConfigurationManager.AppSettings["PDFPath"]));

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            DateTime ReportDate = obj.getServerDateTime();
            DateTime PaymentFromDate = ReportDate.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["DaysBehind"]));
            List<ItemRow> orData = new List<ItemRow>();
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());
            SqlDataAdapter da = new SqlDataAdapter("spGetStripeOrderDetails", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@date", PaymentFromDate);
            da.Fill(ds);
            System.Data.DataTable dt = ds.Tables[0];
            orData = ConvertDataTable<ItemRow>(dt);
            if (orData.Count == 0)
            {
                obj.log("--------------------------------------------------------", "//");
                obj.log("End", "Suncoast Fresh Stripe :: No Record Found");
                obj.log("--------------------------------------------------------", "//");
                return;
            }

            try
            {
                string content = "";
         
                Hashtable htbl = new Hashtable();
                string items = "";

                String dy = ReportDate.Day.ToString();
                String mn = ReportDate.Month.ToString();
                String yy = ReportDate.Year.ToString();
              

                string newFileName = "Suncoast Fresh Stripe Payment Reconciliation Report (" + dy + "_" + mn + "_" + yy + ").pdf";

                string filePath = ConfigurationManager.AppSettings["PDFPath"].ToString() + newFileName;

                foreach (var item in orData)
                { //<td align='center'>" + item.UOM + "</td>
                    string refundamount = "";
                    string Refunddate = "";
                    if (item.RefundAmount != null)
                    {
                        refundamount = item.RefundAmount == 0 ? "" : "$ " + String.Format("{0:0.00}", item.RefundAmount); 
                    }
                    if (item.Refunddate != null)
                    {
                         Refunddate = Convert.ToDateTime(item.Refunddate).ToString("dd MMMM yyyy HH:mm:ss");                     
                    }

                    items += " <tr><td>" + item.Cartid + "</td><td>" + item.CustomerName + "</td><td>" + item.CreatedOn.ToString("dd MMMM yyyy HH:mm:ss") + "</td><td align='center'>" + "$ " + String.Format("{0:0.00}", item.CaptureAmount) + "</td><td align='center'>" + (item.NetAmount == 0 ? "" : "$ " + String.Format("{0:0.00}", item.NetAmount)) + "</td><td>" + item.StripeRef + "</td><td align='center'>" + refundamount + "</td><td>" + Refunddate + "</td></tr>";
                }

                htbl["@image"] = "<img style = 'width: 131%;' src='" + Convert.ToString(ConfigurationManager.AppSettings["LogoPath"]) + "'>";
                htbl["@Items"] = items;
                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy"); 
                htbl["@paymentfromDate"]= PaymentFromDate.ToString("dd/MM/yyy");

                content = ReadEmailTemplateFile(ConfigurationManager.AppSettings["TemplatePath"].ToString(), "order_summary_template.html");

                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                //Initialize HTML to PDF converter 
                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                WebKitConverterSettings settings = new WebKitConverterSettings();

                //Set PDF page orientation 
                settings.Orientation = PdfPageOrientation.Landscape;
                //HTML string and Base URL 
                string htmlText = content;

                string baseUrl = Convert.ToString(ConfigurationManager.AppSettings["baseUrl"]);

                //Set WebKit path
                settings.WebKitPath = Convert.ToString(ConfigurationManager.AppSettings["WebKitPath"]);

                //Add PDF Header
                settings.PdfHeader = AddPdfHeader(settings.PdfPageSize.Width, "PDF report", " ");

                //Add PDF Footer
                settings.PdfFooter = AddPdfFooter(settings.PdfPageSize.Width, ""); //"@Copyright 2020"

                //Assign WebKit settings to HTML converter
                htmlConverter.ConverterSettings = settings;

               

                //Convert HTML string to PDF
                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);


                //Save and close the PDF document 
                document.Save(filePath);

                document.Close(true);


                obj.log("--------------------------------------------------------", "//");
                obj.log("End", "Suncoast Fresh Stripe");
                obj.log("--------------------------------------------------------", "//");

                string subject = ConfigurationManager.AppSettings["CompanyEmailSubject"].ToString();

                string body = "Dear Suncoast Fresh Accounts,"
                        + "<br/><br/>"
                        + "Please find your payment reconciliation summary for all Stripe payments made through the SAAVI Mobile and Online Ordering system from yesterday."
                        + "<br/><br/>"
                        + "If you have any queries, please email support@saavi.com.au"
                        + " <br/><br/>"
                        + "Warmest Regards"
                        + "<br/><br/>"
                        + "The Team and SAAVI";

                string result = SendMail.SendEMail(ConfigurationManager.AppSettings["CompanyAdminEmail"].ToString(), ConfigurationManager.AppSettings["CompanyAdminEmail_CC"].ToString(), "", subject, body, filePath, ConfigurationManager.AppSettings["FROM_EMAILID"].ToString(), newFileName);

                if (result == "ok")
                {
                    obj.log("--------------------------------------------------------", "//");
                    obj.log("Email Sent Successfully", "Info");
                    obj.log("--------------------------------------------------------", "//");
                }
                else
                {
                    obj.log("--------------------------------------------------------", "//");
                    obj.log(result, "Error Email");
                    obj.log("--------------------------------------------------------", "//");
                }

            }
            catch (Exception ex)
            {

                obj.log("--------------------------------------------------------", "//");
                obj.log("Suncoast Fresh Stripe Report: " + ex.Message, "Error");
                obj.log("--------------------------------------------------------", "//");
            }


        }


        #region Read Email Template File and Replace File Variables

        private static PdfPageTemplateElement AddPdfHeader(float width, string title, string description)
        {
            RectangleF rect = new RectangleF(0, 0, width, 20);
            //Create a new instance of PdfPageTemplateElement class.     
            PdfPageTemplateElement header = new PdfPageTemplateElement(rect);
           // PdfGraphics g = header.Graphics;

            ////Draw title.
            //PdfFont font = new PdfTrueTypeFont(new System.Drawing.Font("Helvetica", 16, FontStyle.Bold), true);
            //PdfSolidBrush brush = new PdfSolidBrush(Color.FromArgb(44, 71, 120));
            //float x = (width / 2) - (font.MeasureString(title).Width) / 2;
            //g.DrawString(title, font, brush, new RectangleF(x, (rect.Height / 4) + 3, font.MeasureString(title).Width, font.Height));

            ////Draw description
            //brush = new PdfSolidBrush(Color.Gray);
            //font = new PdfTrueTypeFont(new System.Drawing.Font("Helvetica", 6, FontStyle.Bold), true);
            //PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Left, PdfVerticalAlignment.Bottom);
            //g.DrawString(description, font, brush, new RectangleF(0, 0, header.Width, header.Height - 8), format);

            //Draw some lines in the header
            //PdfPen pen = new PdfPen(Color.DarkBlue, 0.7f);
            //g.DrawLine(pen, 0, 0, header.Width, 0);
            //pen = new PdfPen(Color.DarkBlue, 2f);
            //g.DrawLine(pen, 0, 03, header.Width + 3, 03);
            //pen = new PdfPen(Color.DarkBlue, 2f);
            //g.DrawLine(pen, 0, header.Height - 3, header.Width, header.Height - 3);
            //g.DrawLine(pen, 0, header.Height, header.Width, header.Height);

            return header;
        }

        private static  PdfPageTemplateElement AddPdfFooter(float width, string footerText)
        {
           RectangleF rect = new RectangleF(0, 0, width, 20);
            //Create a new instance of PdfPageTemplateElement class.
            PdfPageTemplateElement footer = new PdfPageTemplateElement(rect);
            //PdfGraphics g = footer.Graphics;

            //// Draw footer text.
            //PdfSolidBrush brush = new PdfSolidBrush(Color.Gray);
            //PdfFont font = new PdfTrueTypeFont(new System.Drawing.Font("Helvetica", 6, FontStyle.Bold), true);
            //float x = (width / 2) - (font.MeasureString(footerText).Width) / 2;
            //g.DrawString(footerText, font, brush, new RectangleF(x, g.ClientSize.Height - 10, font.MeasureString(footerText).Width, font.Height));

            ////Create page number field
            //PdfPageNumberField pageNumber = new PdfPageNumberField(font, brush);

            ////Create page count field
            //PdfPageCountField count = new PdfPageCountField(font, brush);

            //PdfCompositeField compositeField = new PdfCompositeField(font, brush, "Page {0} of {1}", pageNumber, count);
            //compositeField.Bounds = footer.Bounds;
            //compositeField.Draw(g, new PointF(470, 40));

            return footer;
        }


        private static string ReadEmailTemplateFile(string filePath, string fileName)
        {
            string str = string.Empty;

            string path = filePath + fileName;
            //return path;
            if (System.IO.File.Exists(path))
            {
                try
                {
                    StreamReader reader = System.IO.File.OpenText(path);
                    str = reader.ReadToEnd();
                    reader.Close();
                    return str;
                }
                catch (Exception)
                {
                    throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
                }
            }
            throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
        }

        public static string ReplaceFileVariablesInTemplateFile(Hashtable hashVars, string content)
        {
            IDictionaryEnumerator enumerator = hashVars.GetEnumerator();
            while (enumerator.MoveNext())
            {
                content = content.Replace(enumerator.Key.ToString(), enumerator.Value.ToString());
            }
            return content;
        }
        #endregion
        public DateTime getServerDateTime()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            conn.Open();
            using (var cmd = new SqlCommand("select getdate()", conn))
            {
                return Convert.ToDateTime(cmd.ExecuteScalar());
            }
        }

        public static List<T> ConvertDataTable<T>(System.Data.DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        if(dr[column.ColumnName]!=System.DBNull.Value)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    }
                    else
                        continue;
                }
            }
            return obj;
        }

        #region Create log for the actions.

        public void log(string ex, string type)
        {
            if (System.IO.File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (System.IO.Stream str = System.IO.File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.
    }
    public class ItemRow
    {
        public string CustomerName { get; set; }
        public Int64 Cartid { get; set; }
        public DateTime CreatedOn { get; set; }
        public double CaptureAmount { get; set; }
        public double NetAmount { get; set; }
        public string StripeRef { get; set; }
        public double? RefundAmount { get; set; }
        public DateTime? Refunddate { get; set; }


    }

}
