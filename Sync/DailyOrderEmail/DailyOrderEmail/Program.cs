﻿using DailyOrderEmail.BO;
using DailyOrderEmail.DA;
using Microsoft.Office.Interop.Excel;
using OrderPostingToFTP.DA;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace DailyOrderEmail
{
    class Program
    {
        static void Main(string[] args)
        {
            Program obj = new Program();
            obj.GetOrderDetails();
        }

        #region Get saved order details from the database.

        void GetOrderDetails()
        {
            try
            {
                using (var _db = new Entities())
                {

                    DateTime CurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));

                    var ctd = _db.trnsCartMasters.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(CurrentDate.Date)).ToList();

                    if (ctd != null)
                    {
                        List<GetOrderDetailsBO> li = new List<GetOrderDetailsBO>();
                        foreach (var cartV in ctd)
                        {
                            var customer = _db.CustomerMasters.Find(cartV.CustomerID);

                            string warehouse = customer.Warehouse ?? "";

                            var data = (from cart in _db.trnsCartMasters
                                        join cartItems in _db.trnsCartItemsMasters on cart.CartID equals cartItems.CartID
                                        join prod in _db.ProductMasters on cartItems.ProductID equals prod.ProductID
                                        join cust in _db.CustomerMasters on cart.CustomerID equals cust.CustomerID
                                        join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                        join mcat in _db.MainCategoriesMasters on cat.MCategoryId equals mcat.MCategoryId
                                        join unit in _db.UnitOfMeasurementMasters on prod.UOMID equals unit.UOMID

                                        join comment in _db.DeliveryCommentsMasters on cart.CommentID equals comment.CommentID into cTemp
                                        from comment in cTemp.DefaultIfEmpty()

                                        join proCom in _db.DeliveryProductCommentsMasters on cartItems.CommentID equals proCom.PCommentID into cpTemp
                                        from proCom in cpTemp.DefaultIfEmpty()

                                        join rwunit in _db.UnitOfMeasurementMasters on prod.RW_UOMID equals rwunit.UOMID into temp3
                                        from rwunit in temp3.DefaultIfEmpty()

                                        join orunit in _db.UnitOfMeasurementMasters on cartItems.UnitId equals orunit.UOMID into temp2
                                        from orunit in temp2.DefaultIfEmpty()

                                        join filter in _db.ProductFiltersMasters on prod.FilterID equals filter.FilterID

                                        join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into temp6
                                        from soh in temp6.DefaultIfEmpty()

                                        join cusadd in _db.CustomerDeliveryAddresses on cart.AddressID equals cusadd.AddressID into temp4
                                        from cusadd in temp4.DefaultIfEmpty()
                                        where (cartItems.CartID == cartV.CartID
                                                && prod.IsAvailable == true
                                                && prod.IsActive == true && cat.IsActive == true
                                                && mcat.IsActive == true && filter.IsActive == true
                                                && (warehouse == "" || soh.WareHouse == null ||
                                                    (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse)
                                               ))
                                        select new GetOrderDetailsBO
                                        {
                                            ProductID = prod.ProductID,
                                            CartItemID = cartItems.CartItemID,
                                            CartID = cart.CartID,
                                            ProductCode = prod.ProductCode,
                                            ProductName = prod.ProductName,
                                            PONumber = cart.PONumber,
                                            CategoryName = mcat.MCategoryName,
                                            SubCategoryName = cat.CategoryName,
                                            ProductDescription = prod.ProductDescription ?? "",
                                            OrderDate = cart.OrderDate ?? DateTime.Now,
                                            CreatedDate = cart.CreatedDate ?? DateTime.Now,
                                            Price = cartItems.Price,
                                            SpecialPrice = cartItems.Price == null ? 0 : cartItems.Price,
                                            CompanyPrice = prod.ComPrice ?? 0,
                                            Quantity = cartItems.Quantity,
                                            IsPlacedByRep = cart.IsOrderPlpacedByRep ?? false,
                                            Comment = comment.CommentDescription ?? "",
                                            SalesRepUserID = cart.UserID,
                                            CustomerName = cust.CustomerName,
                                            CustomerCode = cust.AlphaCode,
                                            CustomerID = cust.CustomerID,
                                            Address1 = cusadd.AddressID != null ? (cusadd.Address1 != null ? cusadd.Address1.Trim() : "") : (cust.Address1 != null ? cust.Address1.Trim() : ""),
                                            Address2 = cusadd.AddressID != null ? (cusadd.Address2 != null ? cusadd.Address2.Trim() : "") : (cust.Address2 != null ? cust.Address2.Trim() : ""),
                                            Suburb = cusadd.AddressID != null ? (cusadd.Address3 != null ? cusadd.Address3.Trim() : "") : (cust.Suburb != null ? cust.Suburb.Trim() : ""),
                                            AddressId = cart.AddressID ?? 0,
                                            SalesmanCode = cust.SalesmanCode,
                                            ContactName = cust.ContactName,
                                            ContactNo = cust.Phone1,

                                            ProductImage = "https://nsdniwxdrj.saavi.com.au/Admin/Content/Uploads/Products/" + prod.ProductImage,
                                            ProductThumbImage = "https://nsdniwxdrj.saavi.com.au/Admin/Content/Uploads/Products/" + "thumb_" + prod.ProductImage,
                                            UOMID = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? unit.UOMID : (unit.UOMID == null ? 0 : unit.UOMID),
                                            UnitName = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? (rwunit.UOMDesc == null ? "" : rwunit.UOMDesc) : (unit.UOMDesc == null ? "" : unit.UOMDesc),
                                            OrderUnitName = orunit.UOMDesc ?? unit.UOMDesc,
                                            OrderUnitId = (orunit.UOMID == null) ? unit.UOMID : orunit.UOMID,
                                            FilterName = filter.FilterName,
                                            ProductWeight = cartItems.ProductWeight ?? 0,
                                            WeightName = cartItems.WeightName,
                                            WeightDescription = cartItems.WeightDescription,
                                            IsPieceOrWeight = cartItems.IsPieceOrWeight,
                                            IsAvailable = prod.IsAvailable ?? false,
                                            StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                            IsSpecial = cartItems.IsSpecialPrice ?? false,
                                            IsDiscountApplicable = false,
                                            IsCountrywideRewards = prod.RewardItem ?? false,
                                            IsInvoiceComment = cart.IsInvoiceComment ?? false,
                                            IsUnloadComment = cart.IsUnloadComment ?? false,
                                            IsNoPantry = cartItems.IsNoPantry ?? false,
                                            ExtDoc = cart.ExtDoc ?? "",
                                            RunNo = cart.RunNo ?? "",
                                            IsGST = prod.GST ?? false,
                                            PackagingSequence = cart.PackagingSequence ?? "",
                                            Brand = prod.Brand ?? "",
                                            Supplier = prod.Supplier ?? "",
                                            IsSpecialCategory = cat.IsSpecialCategory ?? false,
                                            IsOrderedSpecial = cartItems.IsSpecialPrice ?? false,
                                            CommentLine = cart.CommentLine ?? "",
                                            ProComment = proCom.CommentDescription ?? "",
                                            HasFreightCharges = cart.HasFreightCharges ?? false,
                                            HasNonDeliveryDayCharges = cart.HasNonDeliveryDayCharges ?? false,
                                            Warehouse = warehouse
                                        }).ToList();

                            li.AddRange(data);
                        }

                        // OrderCSV(li);
                        OrderExcel(li);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Send Order Summary

        public void OrderExcel(List<GetOrderDetailsBO> orData)
        {

            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            try
            {

                string filePath = ConfigurationManager.AppSettings["FilePath"].ToString() + "Order_SA.xlsx"; ;
                File.Delete(filePath);

                if (!File.Exists(filePath))
                {


                    File.Copy(ConfigurationManager.AppSettings["FilePath"].ToString() + "order_summary_template.xlsx", filePath);

                    //Create workbook object
                    string str = filePath;
                    Microsoft.Office.Interop.Excel.Workbook workbook = excel.Workbooks.Open(Filename: str);
                    try
                    {
                        //Create worksheet object
                        Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];
                        var results = orData;
                        // Column Headings
                        int iColumn = 0;
                        // Row Data
                        int rows = 0;
                        excel.Cells[3, 1] = "";//results[0].CustomerName;
                        excel.Cells[2, 1] = "Date: " + Convert.ToDateTime(results[0].OrderDate).ToString("yyyy-MM-dd");

                        //time gen row
                        Microsoft.Office.Interop.Excel.Range timegenrow = (Microsoft.Office.Interop.Excel.Range)excel.Rows[31];
                        if (results.Count == 1)
                        {
                            Microsoft.Office.Interop.Excel.Range row2 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[11];
                            row2.Delete();
                        }
                        for (int i = 7; i < results.Count + 7; i++)
                        {
                            string dt = Convert.ToString(excel.Cells[i, 4]);
                            if (rows > 1)
                            {
                                int copyCols = 1;
                                if (rows % 2 == 0)
                                {
                                    for (copyCols = 1; copyCols < 14; copyCols++)
                                    {
                                        Microsoft.Office.Interop.Excel.Range R1 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[10];
                                        R1.Copy(Type.Missing);

                                        Microsoft.Office.Interop.Excel.Range R2 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[i];
                                        R2.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
                                            Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                                    }
                                }
                                else
                                {
                                    for (copyCols = 1; copyCols < 14; copyCols++)
                                    {
                                        Microsoft.Office.Interop.Excel.Range R1 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[11];
                                        R1.Copy(Type.Missing);

                                        Microsoft.Office.Interop.Excel.Range R2 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[i];
                                        R2.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
                                            Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                                    }
                                }
                            }
                            excel.Cells[i, 1] = results[rows].CartID.ToString();
                            excel.Cells[i, 2] = results[rows].CustomerCode;
                            excel.Cells[i, 3] = results[rows].CustomerName;
                            excel.Cells[i, 4] = results[rows].ContactName;  //  customer contact
                            excel.Cells[i, 5] = results[rows].ContactNo; // logged in use id
                            excel.Cells[i, 6] = "";  //ProName + "\r\n" + results[rows].ProComment;
                            excel.Cells[i, 7] = results[rows].OrderDate;
                            excel.Cells[i, 8] = Convert.ToDateTime(results[rows].OrderDate).ToString("yyyy-MM-dd");
                            //getrequireddate(results[rows].OrderDate.To<DateTime>().ToShortDateString());
                            excel.Cells[i, 9] = results[rows].Price;
                            excel.Cells[i, 10] = results[rows].UnitName;
                            excel.Cells[i, 11] = results[rows].Quantity.ToString();
                            excel.Cells[i, 12] = results[rows].PONumber;
                            excel.Cells[i, 13] = results[rows].Comment;

                            rows++;
                        }

                        int genTimeRow = 10 + results.Count + 2;

                        timegenrow.Copy(Type.Missing);
                        Microsoft.Office.Interop.Excel.Range timegenrownew = (Microsoft.Office.Interop.Excel.Range)excel.Rows[genTimeRow];
                        timegenrownew.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
                            Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                        excel.Cells[genTimeRow, 1] = "Generated at: " + DateTime.Now.ToShortTimeString() + ", " + DateTime.Now.ToShortDateString();

                        timegenrow.Delete();
                        ((Microsoft.Office.Interop.Excel._Worksheet)worksheet).Activate();

                        //Save the workbook
                        workbook.Save();

                        //Close the Workbook
                        workbook.Close();

                        //// Finally Quit the Application
                        ((Microsoft.Office.Interop.Excel._Application)excel).Quit();

                        string mailResult = SendMail.SendEMail(HelperClass.ToEmail, "", "", "Daily order summary : "+ DateTime.Now.ToString("yyyy-MM-dd"), "Please find attached file", filePath, HelperClass.FromEmail);
                    
                        if (mailResult == "ok")
                        {
                            log("The mail has been sent successfully", "Info");
                        }
                        else
                        {


                        }
                    }
                    catch (Exception ex)
                    {
                        ((Microsoft.Office.Interop.Excel._Application)excel).Quit();
                        foreach (_Workbook wb in excel.Workbooks)
                        {
                            wb.Close();
                        }
                        log("Error in upload. Message(Inner) :" + ex.Message, "Error");
                        throw ex;
                    }
                }

            }
            catch (Exception ex)
            {

                log("Error in upload. Message :" + ex.Message, "Error");
                ((Microsoft.Office.Interop.Excel._Application)excel).Quit();
                throw ex;
            }


        }


        #region Create log for the actions.

        private static void log(string ex, string type)
        {
            if (File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (Stream str = File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.
        //public string OrderCSV(List<GetOrderDetailsBO> orData)
        //{

        //        try
        //        {
        //            var li = orData;
        //            string newFileName = "SAAVI_ORDER.csv";
        //            string filePath = ConfigurationManager.AppSettings["FilePath"].ToString()  + newFileName;

        //            StringBuilder sb = new StringBuilder();
        //            string data = string.Empty;
        //            var lineitems = 0;
        //            for (int i = 0; i < li.Count; i++)
        //            {
        //                string requiredDate = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(li[i].OrderDate));

        //                string isrep = "0";
        //                if (li[i].IsPlacedByRep)
        //                {
        //                    isrep = "1";
        //                }

        //                string cuscomment = Convert.ToString(li[i].Comment == null ? "" : li[i].Comment);
        //                if (cuscomment.Length > 50)
        //                {
        //                    cuscomment = cuscomment.Substring(0, 50);
        //                }
        //                string ProComment = Convert.ToString(li[i].Comment == null ? "" : li[i].Comment);
        //                if (ProComment.Length > 50)
        //                {
        //                    ProComment = ProComment.Substring(0, 50);
        //                }
        //                lineitems++;

        //                string isdefaultaddress = li[i].AddressId == 0 ? "Y" : "N";

        //                data = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26}",
        //                                    li[i].CustomerCode, String.Format("{0:dd/MM/yyyy}", DateTime.Now), "SA" + li[i].CartID, (li[i].PONumber == "" ? "SA" + li[i].CartID : li[i].PONumber), lineitems, isrep, li[i].ProductCode, li[i].OrderUnitName, li[i].Quantity, li[i].SpecialPrice, requiredDate, cuscomment.Replace("\n", "").Replace("\r", ""), ProComment.Replace("\n", "").Replace("\r", ""), (li[i].Address1 == "" ? "Blank" : li[i].Address1).Replace("\n", "").Replace("\r", ""), li[i].Address2.Replace("\n", "").Replace("\r", ""), (li[i].Suburb == "" ? "Blank" : li[i].Suburb), li[i].Warehouse, li[i].RunNo, "\"" + li[i].ExtDoc.Replace("\n", "").Replace("\r", "") + "\"", ( Convert.ToBoolean(li[i].IsNoPantry) ? "Y" : "N"), (Convert.ToBoolean(li[i].IsInvoiceComment) ? "Y" : "N"), (Convert.ToBoolean(li[i].IsUnloadComment) ? "Y" : "N"), li[i].PackagingSequence, isdefaultaddress, li[i].SalesmanCode, (li[i].IsOrderedSpecial ? "Y" : "N"), li[i].CommentLine);

        //                sb.Append(data);
        //                sb.AppendLine();
        //            }

        //            File.AppendAllText(filePath, sb.ToString());



        //            return filePath;
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //            //return ex.Message + ex.StackTrace;
        //        }

        //}

        #endregion
    }
}
