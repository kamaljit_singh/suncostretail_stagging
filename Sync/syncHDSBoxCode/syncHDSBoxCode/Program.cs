﻿using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace syncHDSBoxCode
{
    class Program
    {
        static void Main(string[] args)
        {


            Program obj = new Program();
            DateTime RD = obj.getServerDateTime();

            string day = RD.DayOfWeek.ToString();
          

            if (day.ToUpper() == "MONDAY" || day.ToUpper() == "TUESDAY" || day.ToUpper() == "THURSDAY" || day.ToUpper() == "FRIDAY")
            {
                obj.log("--------------------------------------------------------", "//");
                obj.log("Start", "HDS");
                obj.log("--------------------------------------------------------", "//");

                DirectoryInfo di = new DirectoryInfo(Convert.ToString(ConfigurationManager.AppSettings["ExcelPath"]));
                DirectoryInfo diT = new DirectoryInfo(Convert.ToString(ConfigurationManager.AppSettings["TempPath"]));
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (FileInfo file in diT.GetFiles())
                {
                    file.Delete();
                }

                DateTime ReportDate = RD.AddDays(1);

                List<ItemRow> li = new List<ItemRow>();
                List<ItemRow> li1 = new List<ItemRow>();
                string attachments = "";

                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spGetHDSOrderDetails", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@date", ReportDate);


                da.Fill(ds);

                // BRIS
                System.Data.DataTable dt = ds.Tables[0];
                li = ConvertDataTable<ItemRow>(dt);

                string f1 = obj.CreateCsvFile(li, ReportDate, "BRIS");
                if (f1 != "")
                {
                    attachments += f1 + ";";
                }

                //GC
                if (day.ToUpper() != "TUESDAY" && day.ToUpper() != "FRIDAY")
                {
                   
                    System.Data.DataTable dt1 = ds.Tables[1];
                    li1 = ConvertDataTable<ItemRow>(dt1);

                    string f2 = obj.CreateCsvFile(li1, ReportDate, "GC");
                    if (f2 != "")
                    {
                        attachments += f2 + ";";
                    }
                }



                if (attachments != "")
                {
                    string dy = ReportDate.Day.ToString("00");
                    string mn = ReportDate.Month.ToString("00");
                    string yy = ReportDate.Year.ToString();

                    string subject = "Suncoast Fresh Delivery Manifest Request for " + dy + "/" + mn + "/" + yy; //ConfigurationManager.AppSettings["CompanyEmailSubject"].ToString();

                    string body = "Dear Home Delivery Customer Service,"
                            + "<br/><br/>"
                            + "Please find attached our delivery manifest for " + dy + "/" + mn + "/" + yy + "."
                            + "<br/><br/>"
                            + "If there are any issues with the file or any specific delivery issues, please call our office on 07 5440 2600."
                            + " <br/><br/>"
                            + "Kind Regards"
                            + "<br/><br/>"
                            + "Suncoast Fresh Team";

                    string result = SendMail.SendEMail(ConfigurationManager.AppSettings["CompanyAdminEmail"].ToString(), ConfigurationManager.AppSettings["CompanyAdminEmail_CC"].ToString(), "", subject, body, attachments.TrimEnd(';'), ConfigurationManager.AppSettings["FROM_EMAILID"].ToString(), "");



                    if (result == "ok")
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Sent Successfully", "Info");
                        obj.log("--------------------------------------------------------", "//");
                    }
                    else
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log(result, "Error Email");
                        obj.log("--------------------------------------------------------", "//");
                    }
                }

            }




            #region Update Lat Lng
            obj.UpdateLatLong();
            #endregion
        }

        private string CreateCsvFile(List<ItemRow> li, DateTime ReportDate, string DeliveryRun)
        {
            Program obj = new Program();
            if (li.Count == 0)
            {

                obj.log("--------------------------------------------------------", "//");
                obj.log("End", "HDS " + DeliveryRun + " :: No Record Found");
                obj.log("--------------------------------------------------------", "//");

                return "";



            }


            List<ItemRow> record = new List<ItemRow>();
            var liCartID = li.Select(x => x.cartid).Distinct().ToList();
            foreach (var nCartID in liCartID)
            {
                int j = 1;
                int OtherQTY = 0;
                int TotalBox = 0;
                foreach (var rec in li.Where(x => x.cartid == nCartID).ToList())
                {


                    if (rec.UOMDesc.ToUpper() == "BOX")
                    {
                        for (int i = 1; i <= rec.Quantity; i++)
                        {
                            ItemRow iobj = new ItemRow();

                            iobj.cartid = rec.cartid;
                            iobj.boxnumber = "SCF-HD-" + rec.cartid.ToString("D4") + "-" + j;
                            iobj.customername = rec.customername;
                            iobj.Phone = rec.Phone == "" ? "" : "61" + CleanSTDCode(rec.Phone);
                            iobj.email = rec.email;
                            iobj.Quantity = 1; //rec.Quantity;
                            iobj.Commentdescription = rec.Commentdescription;
                            iobj.Address1 = rec.Address1;
                            iobj.Address2 = rec.Address2;
                            iobj.Suburb = rec.Suburb;
                            iobj.Statename = rec.Statename;
                            iobj.postcode = rec.postcode;
                            iobj.Time_wind = rec.Time_wind;
                            iobj.Time_wind2 = rec.Time_wind2;
                            iobj.type = rec.type;
                            iobj.UOMDesc = rec.UOMDesc;
                            iobj.SaaviBoxNo = j.ToString();
                         //   iobj.SaaviBoxTotal = rec.Quantity.ToString();
                            record.Add(iobj);
                            TotalBox += 1;
                            j++;
                        }
                     
                    }
                    else
                    {
                        OtherQTY += Convert.ToInt32(rec.Quantity);
                    }
                }

                if (OtherQTY != 0)
                {
                    int nume = Convert.ToInt32(OtherQTY) / 7;
                    int deno = Convert.ToInt32(OtherQTY) % 7;
                    var otherUOM = li.Where(x => x.cartid == nCartID).FirstOrDefault();

                    if (deno != 0 && deno <= 7)
                    {
                        nume = nume + 1;
                    }

                    if (nume != 0)
                    {

                        for (int i = 1; i <= nume; i++)
                        {
                            ItemRow iobj = new ItemRow();

                            iobj.cartid = otherUOM.cartid;
                            iobj.boxnumber = "SCF-HD-" + otherUOM.cartid.ToString("D4") + "-" + j;
                            iobj.customername = otherUOM.customername;
                            iobj.Phone = otherUOM.Phone == "" ? "" : "61" + CleanSTDCode(otherUOM.Phone);
                            iobj.email = otherUOM.email;
                            iobj.Quantity = 1;//5; //otherUOM.Quantity;
                            iobj.Commentdescription = otherUOM.Commentdescription;
                            iobj.Address1 = otherUOM.Address1;
                            iobj.Address2 = otherUOM.Address2;
                            iobj.Suburb = otherUOM.Suburb;
                            iobj.Statename = otherUOM.Statename;
                            iobj.postcode = otherUOM.postcode;
                            iobj.Time_wind = otherUOM.Time_wind;
                            iobj.Time_wind2 = otherUOM.Time_wind2;
                            iobj.type = otherUOM.type;
                            iobj.UOMDesc = otherUOM.UOMDesc;
                            iobj.SaaviBoxNo = j.ToString();
                          //  iobj.SaaviBoxTotal = nume.ToString();
                            record.Add(iobj);
                            TotalBox += 1;
                            j++;

                        }
                    }

                  
                }

                foreach (var rec in record.Where(x => x.cartid == nCartID).ToList())
                {
                    rec.SaaviBoxTotal = TotalBox.ToString();
                }


            }


            li = record;

            string dy = ReportDate.Day.ToString("00");
            string mn = ReportDate.Month.ToString("00");
            string yy = ReportDate.Year.ToString();

            string newFileName = "SCF_" + DeliveryRun + "manifest_" + dy + "" + mn + "" + yy + ".csv";  //"Suncoast_Fresh_Manifest_" + dy + "" + mn + "" + yy + ".csv";
            string filePath = ConfigurationManager.AppSettings["ExcelPath"].ToString() + newFileName;
            string filePathTemp = ConfigurationManager.AppSettings["TempPath"].ToString() + newFileName;

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            if (File.Exists(filePathTemp))
            {
                File.Delete(filePathTemp);
            }

            StringBuilder sb = new StringBuilder();
            string dataH = string.Empty;
            dataH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}",
               //  "SAAVI ORDER #",
               "ORDER NO.",
               "BOX NUMBER",
               "CUSTOMER NAME",
               "PHONE NUMBER",
               "EMAIL",
               "QTY",
               "DELIVERY INSTRUCTIONS",
               "POSTAL ADDRESS",
               "ADDRESS 2",
               "SUBURB",
               "STATE",
               "POSTCODE",
                "Time_window_start",
                "Time_window_end",
                "Type",
                "SAAVI Box No",
                "SAAVI Box Total"
                );
            sb.Append(dataH);
            sb.AppendLine();

            string data = string.Empty;
            for (int i = 0; i <= li.Count - 1; i++)
            {
                string orderid = string.Format("{0}", li[i].cartid.ToString("D4"));
                data = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}",
                //"SA"+orderid,
                "SCF-HD-" + orderid,
                li[i].boxnumber,
                li[i].customername,
                li[i].Phone,
                li[i].email,
                li[i].Quantity.ToString(),
                CleanInvalidChars(li[i].Commentdescription),
                CleanInvalidChars(li[i].Address1),
                CleanInvalidChars(li[i].Address2),
                 CleanInvalidChars(li[i].Suburb),
                li[i].Statename,
                li[i].postcode,
                 "08:00",
                 "17:00",
                 "Residential",
                 li[i].SaaviBoxNo,
                li[i].SaaviBoxTotal
                 );
                sb.Append(data);
                sb.AppendLine();
            }

            File.AppendAllText(filePath, sb.ToString());

            File.AppendAllText(filePathTemp, sb.ToString());


            obj.log("--------------------------------------------------------", "//");
            obj.log("End", "HDS");
            obj.log("--------------------------------------------------------", "//");



            try
            {

                bool isSuccess = true;
                bool FTP_UsePassive = Convert.ToString(ConfigurationManager.AppSettings["FTP_UsePassive"]) == "1";
                FTPHelper ftp = new FTPHelper(Convert.ToString(ConfigurationManager.AppSettings["FTPUrl"]), Convert.ToString(ConfigurationManager.AppSettings["FTPUserName"]), Convert.ToString(ConfigurationManager.AppSettings["FTPPassword"]), FTP_UsePassive);
                if (ftp != null)
                {
                    isSuccess = ftp.upload(newFileName, filePathTemp);
                    if (!isSuccess)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            ftp.delete(newFileName);
                            isSuccess = ftp.upload(newFileName, filePathTemp);
                            if (isSuccess)
                            {// IsOrderPostingToFTP = true after ftp upload
                                obj.log("--------------------------------------------------------", "//");
                                obj.log("Step 2. Successfully uploaded in " + (i + 1) + " attempt : " + newFileName, "Info");
                                obj.log("--------------------------------------------------------", "//");
                                break;
                            }

                        }
                    }
                    if (!isSuccess)
                    {
                        obj.log("(From SYNC)FTP upload failed ", "Error");
                        string subject = "Failed FTP Upload : Suncoast Fresh Delivery " + DeliveryRun + " Manifest Request for " + dy + "/" + mn + "/" + yy;
                        string msg = "Dear Company Admin, This email alert is to advise that Suncoast Fresh Delivery Manifest file was not able to be placed within your  import folder due to network access reasons.";
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Error from sync :" + msg, "Error");
                        obj.log("--------------------------------------------------------", "//");
                        SendMail.SendEMail(Convert.ToString(ConfigurationManager.AppSettings["ErrorEmail"]), "", "", subject, msg, "", ConfigurationManager.AppSettings["FROM_EMAILID"].ToString(), "");

                    }
                    else
                    {

                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Status Changed", "Info");
                        obj.log("Step 1. Successfully uploaded in 1 attempt : " + newFileName, "Info");
                        obj.log("--------------------------------------------------------", "//");
                    }
                }
                else
                {

                    obj.log("--------------------------------------------------------", "//");
                    obj.log("FTP not connected", "Error");
                    obj.log("--------------------------------------------------------", "//");
                }


            }
            catch (Exception ex)
            {

                obj.log("--------------------------------------------------------", "//");
                obj.log("HDS Inner Report: " + ex.Message, "Error");
                obj.log("--------------------------------------------------------", "//");

            }

            return filePath;



        }

        public DateTime getServerDateTime()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            conn.Open();
            using (var cmd = new SqlCommand("select getdate()", conn))
            {
                return Convert.ToDateTime(cmd.ExecuteScalar());
            }
        }

        public static List<T> ConvertDataTable<T>(System.Data.DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }


        public static string CleanInvalidChars(string text)
        {

            text = text.Replace("'", "");
            text = text.Replace("\"", "");
            text = text.Replace(",", "");
            text = text.Replace("\n", "");
            return text;
        }
        public static string CleanSTDCode(string text)
        {
            if (text.Length > 9)
            {
                text = text.Substring(text.Length - 9);
            }

            return text;
        }
        #region Create log for the actions.

        public void log(string ex, string type)
        {
            if (System.IO.File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (System.IO.Stream str = System.IO.File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.


        public void UpdateLatLong()
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            SqlDataAdapter da = new SqlDataAdapter("spGetOrderMissingLatLong", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.Fill(ds);

            System.Data.DataTable dt = ds.Tables[0];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    string orderNo = "";
                    string lat = "";
                    string lng = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        try
                        {
                            var address = dt.Rows[i]["Address1"].ToString() + "," + dt.Rows[i]["Suburb"].ToString() + " , , " + dt.Rows[i]["ZIP"].ToString();

                            string url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=AIzaSyD1SMGHFaBfV9bE2XiE7IpykbWJ2UmZEIE";
                            HttpClient client = new HttpClient();

                            var response = client.GetAsync(url).Result;

                            if (response.IsSuccessStatusCode)
                            {
                                var result = JsonConvert.DeserializeObject<GeoLocation>(response.Content.ReadAsStringAsync().Result);
                                if (result.status != "ZERO_RESULTS" && result.status != "REQUEST_DENIED")
                                {
                                    lat = result.results[0].geometry.location.lat.ToString();
                                    lng = result.results[0].geometry.location.lng.ToString();
                                }
                                else
                                {
                                    lat = "";
                                    lng = "";
                                }

                                orderNo = dt.Rows[i]["CartID"].ToString();
                                conn.Open();
                                using (SqlCommand cmd = new SqlCommand("UPDATE trnsCartMaster SET Latitude=@lat, Longitude=@long" +
                                     " WHERE cartid=@cartid", conn))
                                {
                                    cmd.Parameters.AddWithValue("@cartid", Convert.ToInt64(orderNo));
                                    cmd.Parameters.AddWithValue("@lat", lat);
                                    cmd.Parameters.AddWithValue("@long", lng);

                                    int rows = cmd.ExecuteNonQuery();


                                    log("--------------------------------------------------------", "//");
                                    log("End", "Lat Lng Updated successfully:: Order: " + orderNo);
                                    log("--------------------------------------------------------", "//");

                                }
                                conn.Close();
                            }


                        }
                        catch (Exception ex)
                        {
                            log(ex.Message, "Error In getting Lat Lng. Order: " + dt.Rows[i]["CardID"].ToString());
                            continue;
                        }
                    }
                }
            }
            else
            {
                log("--------------------------------------------------------", "//");
                log("End", "Lat Lng :: No record found");
                log("--------------------------------------------------------", "//");
            }
        }



    }
    public class ItemRow
    {
        public Int64 cartid { get; set; }
        public string boxnumber { get; set; }
        public string customername { get; set; }
        public string Phone { get; set; }
        public string email { get; set; }
        public double Quantity { get; set; }
        public string Commentdescription { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Statename { get; set; }
        public string postcode { get; set; }
        public string Time_wind { get; set; }
        public string Time_wind2 { get; set; }
        public string type { get; set; }
        public string UOMDesc { get; set; }
        public string SaaviBoxNo { get; set; }
        public string SaaviBoxTotal { get; set; }

    }
}
