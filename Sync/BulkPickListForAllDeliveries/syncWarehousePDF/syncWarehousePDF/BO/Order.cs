﻿using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.HtmlToPdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace syncWarehousePDF.BO
{
    public class Order
    {
        #region Delivery PDF


        public string OrderPDFForSummary(DateTime ReportDate, DateTime DeliveryDate)
        {
            try
            {
                string content = "";
                List<Stream> termsList = new List<Stream>();
                List<ItemRow> orData = new List<ItemRow>();
                List<ItemRowWare> orDataWare = new List<ItemRowWare>();
                List<ItemRow> orDataAllProducts = new List<ItemRow>();
                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spOrderSummaryByWarehouse", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);


                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                orData = Common.ConvertDataTable<ItemRow>(ds.Tables[0]);

                DataTable dt1 = ds.Tables[1];
                DataTable dt2 = ds.Tables[2];
                DataTable dt3 = ds.Tables[3];
                orDataWare = Common.ConvertDataTable<ItemRowWare>(ds.Tables[4]);

                orDataAllProducts = Common.ConvertDataTable<ItemRow>(ds.Tables[5]);


                //  orData = _db.Database.SqlQuery<ItemRow>("spOrderSummaryByWarehouse").ToList();

                if (orData.Count == 0)
                {
                    return "";
                }

                Hashtable htbl = new Hashtable();
                string items = "";
                double TOTALQty = 0;


                List<string> list = new List<string>();               

                foreach (ItemRow item in orDataAllProducts.Where(x => x.UOM.ToUpper() == "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                {
                    list.Add(item.ProductCode); 
                }
                list.Add("");
                foreach (ItemRow item in orDataAllProducts.Where(x => x.UOM.ToUpper() != "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                {
                    list.Add(item.ProductCode);
                }

                string[] productcode = list.ToArray();//{ "1520", "1519", "1523", "1521", "2072", "1522", "1524","2003", "", "1950", "1949", "1818","2000","2001" };

                for (int i = 0; i < productcode.Count(); i++)
                {
                    if (productcode[i] == "")
                    {
                        items += " <tr><td bgcolor='#FFFFFF' colspan='5'>&nbsp;</td></tr>"; //" <tr><td> </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td></tr>";

                    }
                    else {
                        var item = orData.Where(x => x.ProductCode == productcode[i]).FirstOrDefault();

                        if(item != null) {
                            items += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + item.UOM + "</td><td align='center'>" + (double)item.QTY + "</td><td style='border: 1px solid #000;'> </td></tr>";
                        }
                      
                        else
                        {
                            var zeroQty = orDataAllProducts.Where(x => x.ProductCode == productcode[i]).FirstOrDefault();
                            items += " <tr><td>" + zeroQty.ProductCode + "</td><td>" + zeroQty.ProductName + "</td><td align='center'>" + zeroQty.UOM + "</td><td align='center'>0</td><td style='border: 1px solid #000;'> </td></tr>";
                        }
                        
                    }
                }

                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
                htbl["@Items"] = items;
                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
                htbl["@RequriedDate"] = DeliveryDate.ToString("dd/MM/yyy");
                htbl["@ALLWAREHOUSE"] = Convert.ToDecimal(dt1.Rows[0][0].ToString()) + Convert.ToDecimal(dt2.Rows[0][0].ToString()) + Convert.ToDecimal(dt3.Rows[0][0].ToString());
                string warehouse = "";
                warehouse += "<tr><td></td><td align='center' style='font-weight: bold;'>DELIVERY</td><td align='center' style='font-weight: bold;'>PICK-UP</td><td align='center' style='font-weight: bold;'>TOTAL</td></tr>";
                warehouse += "<tr><td>COOLUM</td><td align='center'>" + dt1.Rows[0][1].ToString() + " </td><td align='center'>" + dt1.Rows[0][2].ToString() + " </td><td align='center'>" + dt1.Rows[0][0].ToString() + "</td></tr>";
                warehouse += "<tr><td>BRISBANE</td><td align='center'>" + dt2.Rows[0][1].ToString() + "</td><td align='center'>" + dt2.Rows[0][2].ToString() + " </td><td align='center'>" + dt2.Rows[0][0].ToString() + "</td></tr>";
                warehouse += "<tr><td>BYRON BAY</td><td align='center'>" + dt3.Rows[0][1].ToString() + " </td><td align='center'>" + dt3.Rows[0][2].ToString() + " </td><td align='center'>" + dt3.Rows[0][0].ToString() + "</td></tr>";
                htbl["@Warehouse"] = warehouse;

                string ItemsWarehouse = "";

                for (int i = 0; i < productcode.Count(); i++)
                {
                    if (productcode[i] == "")
                    {
                        ItemsWarehouse += " <tr><td bgcolor='#FFFFFF' colspan='7'>&nbsp;</td></tr>"; //" <tr><td> </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td><td> </td><td> </td></tr>";

                    }
                    else
                    {
                        var item = orDataWare.Where(x => x.ProductCode == productcode[i]).FirstOrDefault();

                        if (item != null)
                        {
                            ItemsWarehouse += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + (double)item.BRIS + "</td><td align='center'>" + (double)item.GC + "</td><td align='center'>" + (double)item.TWB + "</td><td align='center'>" + (double)item.COAST + "</td><td align='center'>" + (double)item.BYRON + "</td></tr>";

                        }
                        else
                        {
                            var zeroQty = orDataAllProducts.Where(x => x.ProductCode == productcode[i]).FirstOrDefault();
                            ItemsWarehouse += " <tr><td>" + zeroQty.ProductCode + "</td><td>" + zeroQty.ProductName + "</td><td align='center'>0</td><td align='center'>0</td><td align='center'>0</td><td align='center'>0</td><td align='center'>0</td></tr>";

                        }

                    }
                }

                htbl["@secondTable"] = ItemsWarehouse;

                #region OrderSummary.html
                content = ReadEmailTemplateFile(Common.TemplatePath, "OrderSummary.html");

                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                //Initialize HTML to PDF converter 
                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                WebKitConverterSettings settings = new WebKitConverterSettings();

                //HTML string and Base URL 
                string htmlText = content;

                string baseUrl = Common.baseUrl;

                //Set WebKit path
                settings.WebKitPath = Common.WebKitPath;

                //Assign WebKit settings to HTML converter
                htmlConverter.ConverterSettings = settings;

                //Convert HTML string to PDF
                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);

                //Save and close the PDF document 
                document.Save(Common.PDFPath + "BulkPickListALL1.pdf");

                document.Close(true);
                // Process.Start("E:/Output.pdf");
                #endregion


                #region OrderSummary-2.html



                content = ReadEmailTemplateFile(Common.TemplatePath, "OrderSummary-2.html");

                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                //Initialize HTML to PDF converter 
                HtmlToPdfConverter htmlConverter2 = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                WebKitConverterSettings settings2 = new WebKitConverterSettings();

                //HTML string and Base URL 
                string htmlText2 = content;

                string baseUrl2 = Common.baseUrl;

                //Set WebKit path
                settings2.WebKitPath = Common.WebKitPath;

                //Assign WebKit settings to HTML converter
                htmlConverter2.ConverterSettings = settings2;

                //Convert HTML string to PDF
                PdfDocument document2 = htmlConverter2.Convert(htmlText2, baseUrl2);

                //Save and close the PDF document 
                document2.Save(Common.PDFPath + "BulkPickListALL2.pdf");

                document2.Close(true);
                // Process.Start("E:/Output.pdf");
                #endregion

                Stream streamT1 = File.OpenRead(Common.PDFPath + "BulkPickListALL1.pdf");
                termsList.Add(streamT1);

                Stream streamT2 = File.OpenRead(Common.PDFPath + "BulkPickListALL2.pdf");
                termsList.Add(streamT2);

                Stream[] streams = termsList.ToArray();
                return MergePDF(streams, "BulkPickListALL.pdf");
                //return Common.PDFPath + "BulkPickListALL.pdf";

            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("BulkPickListALL: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }


        public string BulkPickListByDeliveryRun(DateTime ReportDate, DateTime DeliveryDate, string WH)
        {
            try
            {
                string content = "";
                string attachments = "";
                List<Stream> termsList = new List<Stream>();
                List<ItemRow> orData = new List<ItemRow>();
                List<ItemRowWare> orDataWare = new List<ItemRowWare>();

                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spGetDeliveryRun", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                da.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string deliveryRun = dt.Rows[i][0].ToString();
                            DataSet dsDR = new DataSet();
                            SqlDataAdapter sda = new SqlDataAdapter("spBulkPickListByDeliveryRun", conn);
                            sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                            sda.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                            sda.SelectCommand.Parameters.AddWithValue("@deliveryRun", deliveryRun);
                            sda.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                            sda.Fill(dsDR);
                            orData = Common.ConvertDataTable<ItemRow>(dsDR.Tables[0]);
                            DataTable dt1 = dsDR.Tables[1];
                            if (orData.Count != 0)
                            {
                                Hashtable htbl = new Hashtable();
                                string items = "";

                                List<string> list = new List<string>();

                                foreach (ItemRow item in orData.Where(x => x.UOM.ToUpper() == "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                                {
                                    list.Add(item.ProductCode);
                                }
                                list.Add("");
                                foreach (ItemRow item in orData.Where(x => x.UOM.ToUpper() != "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                                {
                                    list.Add(item.ProductCode);
                                }

                                string[] productcode = list.ToArray();

                               // string[] productcode = { "1520", "1519", "1523", "1521", "2072", "1522", "1524", "2003", "", "1950", "1949", "1818", "2000", "2001" };
                                //{ "1520", "1519", "1523", "1521", "2072", "1522", "1524", "", "1950", "1949", "1818" };

                                for (int j = 0; j < productcode.Count(); j++)
                                {

                                    foreach (ItemRow item in orData)
                                    {
                                        if (productcode[j] == "")
                                        {
                                            items += " <tr><td> </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td></tr>";
                                        }
                                        else if (productcode[j] == item.ProductCode)
                                        {
                                            items += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + item.UOM + "</td><td align='center'>" + (double)item.QTY + "</td><td style='border: 1px solid #000;'> </td></tr>";

                                        }
                                    }
                                }

                                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
                                htbl["@Items"] = items;
                                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
                                htbl["@RequriedDate"] = DeliveryDate.ToString("dd/MM/yyy");
                                htbl["@ALLWAREHOUSE"] = dt1.Rows[0][0].ToString();
                                if (WH.Contains("COAST"))
                                {

                                    htbl["@warehouse"] = "COOLUM";
                                }
                                else
                                {
                                    htbl["@warehouse"] = WH;
                                }
                            
                                htbl["@deliveryrun"] = deliveryRun;


                                content = ReadEmailTemplateFile(Common.TemplatePath, "BulkPickListByDeliveryRunTemp.html");

                                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                                //Initialize HTML to PDF converter 
                                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                                WebKitConverterSettings settings = new WebKitConverterSettings();

                                //HTML string and Base URL 
                                string htmlText = content;

                                string baseUrl = Common.baseUrl;

                                //Set WebKit path
                                settings.WebKitPath = Common.WebKitPath;



                                //Assign WebKit settings to HTML converter
                                htmlConverter.ConverterSettings = settings;

                                //Convert HTML string to PDF
                                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);                               

                                //Save and close the PDF document 
                                document.Save(Common.PDFPath + "BulkPickListByDeliveryRun_" + WH + "_" + i.ToString() + ".pdf");

                                document.Close(true);
                                // Process.Start("E:/Output.pdf");


                                Stream streamT1 = File.OpenRead(Common.PDFPath + "BulkPickListByDeliveryRun_" + WH + "_" + i.ToString() + ".pdf");
                                termsList.Add(streamT1);
                            }
                        }
                        Stream[] streams = termsList.ToArray();
                        if (WH == "COAST")
                        {
                            WH = "COOLUM";
                        }

                        attachments = MergePDF(streams, "Bulk_Pick_List_By_Delivery_Run_" + WH + ".pdf");

                    }
                }
                return attachments;
            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("BulkPickListByDeliveryRun: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }


        public string DeliveryRunManiFest(DateTime ReportDate, DateTime DeliveryDate, string WH)
        {
            try
            {
                string content = "";
                string attachments = "";
                List<Stream> termsList = new List<Stream>();
                List<ItemRowManifest> orData = new List<ItemRowManifest>();


                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spGetDeliveryRun", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                da.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string[] productcode = { "1522", "1524" };

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string deliveryRun = dt.Rows[i][0].ToString();
                            DataSet dsDR = new DataSet();
                            SqlDataAdapter sda = new SqlDataAdapter("spDeliveryRunManiFest", conn);
                            sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                            sda.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                            sda.SelectCommand.Parameters.AddWithValue("@deliveryRun", deliveryRun);
                            sda.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                            sda.Fill(dsDR);
                            orData = Common.ConvertDataTable<ItemRowManifest>(dsDR.Tables[0]);
                            DataTable dt1 = dsDR.Tables[1];
                            if (orData.Count != 0)
                            {
                                Hashtable htbl = new Hashtable();
                                string items = "";


                                //string[] productcode = { "1520", "1519", "1523", "1521", "2072", "1522", "1524", "", "1950", "1949", "1818" };

                                //for (int j = 0; j < productcode.Count(); j++)
                                //{

                                foreach (ItemRowManifest item in orData)
                                {
                                    //if (productcode[j] == "")
                                    //{
                                    //    items += " <tr><td> </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td></tr>";
                                    //}
                                    //else if (productcode[j] == item.ProductCode)
                                    //{

                                    DataSet dsDRT = new DataSet();
                                    SqlDataAdapter sdaT = new SqlDataAdapter("spDeliveryRunManiFestOrderItems", conn);
                                    sdaT.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    sdaT.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@deliveryRun", deliveryRun);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@Order", item.Order);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                                    sdaT.Fill(dsDRT);
                                    DataTable dtI = dsDRT.Tables[0];
                                    if (dtI.Rows.Count > 0)
                                    {
                                        int QTY = 0;
                                        int OtherQTY = 0;
                                        if (dtI.Rows.Count > 0)
                                        {
                                            for (int a = 0; a < dtI.Rows.Count; a++)
                                            {
                                                if (dtI.Rows[a]["UOMDesc"].ToString().ToUpper() == "BOX")
                                                {
                                                    if(productcode.Contains(dtI.Rows[a]["ItemsCode"].ToString()))
                                                    {
                                                        QTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]) * 2;
                                                    }
                                                    else{
                                                        QTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]);
                                                    }

                                                  
                                                }
                                                else
                                                {
                                                    OtherQTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]);
                                                }

                                            }

                                            int nume = Convert.ToInt32(OtherQTY) / 7;
                                            int deno = Convert.ToInt32(OtherQTY) % 7;


                                            if (deno != 0 && deno <= 7)
                                            { QTY += 1; }

                                            if (nume != 0)
                                            {

                                                QTY = QTY + nume;

                                            }
                                        }


                                        items += " <tr style ='border: 1px solid black;'><td rowspan=" + dtI.Rows.Count + ">" + item.Order + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Suburb + "</td><td class='cell-hyphens' rowspan=" + dtI.Rows.Count + ">" + item.CustomerName + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Phone + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Address + "</td><td rowspan=" + dtI.Rows.Count + ">Boxes :" + QTY + "  <br>" + item.DeliveryNotes + "</td><td style='border: 1px solid #000;'>" + dtI.Rows[0]["Items"].ToString() + "</td><td align='center' style='border: 1px solid #000;'>" + dtI.Rows[0]["TotalQty"].ToString() + "</td><td style='border: 1px solid #000;'> </td></tr>";

                                        if (dtI.Rows.Count > 1)
                                        {

                                            for (int k = 1; k < dtI.Rows.Count; k++)
                                            {
                                                items += " <tr><td style='border: 1px solid #000;'>" + dtI.Rows[k]["Items"].ToString() + "</td><td align='center' style='border: 1px solid #000;'>" + dtI.Rows[k]["TotalQty"].ToString() + "</td><td style='border: 1px solid #000;'> </td></tr>";
                                            }

                                        }
                                    }

                                    //}
                                }
                                // }

                                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
                                htbl["@Items"] = items;
                                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
                                htbl["@RequriedDate"] = DeliveryDate.ToString("dd/MM/yyy");
                                htbl["@ALLWAREHOUSE"] = dt1.Rows[0][0].ToString();
                                if (WH.Contains("COAST"))
                                {

                                    htbl["@warehouse"] = "COOLUM";
                                }
                                else
                                {
                                    htbl["@warehouse"] = WH;
                                }
                               
                                htbl["@deliveryrun"] = deliveryRun;


                                content = ReadEmailTemplateFile(Common.TemplatePath, "BulkPickListByDeliveryRunTemp_Manifest.html");

                                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                                //Initialize HTML to PDF converter 
                                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                                WebKitConverterSettings settings = new WebKitConverterSettings();

                                //Set PDF page orientation 
                                settings.Orientation = PdfPageOrientation.Landscape;
                                //HTML string and Base URL 
                                string htmlText = content;

                                string baseUrl = Common.baseUrl;

                                //Set WebKit path
                                settings.WebKitPath = Common.WebKitPath;
                                settings.PdfHeader = AddPdfHeader(settings.PdfPageSize.Width, "", "");
                                settings.PdfFooter = AddPdfFooter(settings.PdfPageSize.Width, "");


                                //Assign WebKit settings to HTML converter
                                htmlConverter.ConverterSettings = settings;

                                //Convert HTML string to PDF
                                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);

                                //Save and close the PDF document 
                                document.Save(Common.PDFPath + "DeliveryRunManiFest_" + WH + "_" + i.ToString() + ".pdf");

                                document.Close(true);
                                // Process.Start("E:/Output.pdf");


                                Stream streamT1 = File.OpenRead(Common.PDFPath + "DeliveryRunManiFest_" + WH + "_" + i.ToString() + ".pdf");
                                termsList.Add(streamT1);
                            }
                        }
                        Stream[] streams = termsList.ToArray();


                        if (WH == "COAST")
                        {
                            WH = "COOLUM";
                        }
                        attachments = MergePDF(streams, "Delivery_Run_Manifest_For_" + WH + ".pdf");
                    }
                }

                return attachments;
            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("DeliveryRunManiFest: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }



        public string DeliveryRunManiFestExceptions(DateTime ReportDate, DateTime DeliveryDate, string WH)
        {
            try
            {
                string content = "";
                string attachments = "";
                List<Stream> termsList = new List<Stream>();
                List<ItemRowManifest> orData = new List<ItemRowManifest>();


                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spGetDeliveryRun", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                da.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string[] productcode = { "1522", "1524" };

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string deliveryRun = dt.Rows[i][0].ToString();
                            DataSet dsDR = new DataSet();
                            SqlDataAdapter sda = new SqlDataAdapter("spDeliveryRunExceptions", conn);
                            sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                            sda.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                            sda.SelectCommand.Parameters.AddWithValue("@deliveryRun", deliveryRun);
                            sda.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                            sda.Fill(dsDR);
                            orData = Common.ConvertDataTable<ItemRowManifest>(dsDR.Tables[0]);
                            DataTable dt1 = dsDR.Tables[1];
                            if (orData.Count != 0)
                            {
                                Hashtable htbl = new Hashtable();
                                string items = "";


                                //string[] productcode = { "1520", "1519", "1523", "1521", "2072", "1522", "1524", "", "1950", "1949", "1818" };

                                //for (int j = 0; j < productcode.Count(); j++)
                                //{

                                foreach (ItemRowManifest item in orData)
                                {
                                    //if (productcode[j] == "")
                                    //{
                                    //    items += " <tr><td> </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td></tr>";
                                    //}
                                    //else if (productcode[j] == item.ProductCode)
                                    //{

                                    DataSet dsDRT = new DataSet();
                                    SqlDataAdapter sdaT = new SqlDataAdapter("spDeliveryRunManiFestOrderItems", conn);
                                    sdaT.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    sdaT.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@deliveryRun", deliveryRun);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@Order", item.Order);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                                    sdaT.Fill(dsDRT);
                                    DataTable dtI = dsDRT.Tables[0];
                                    if (dtI.Rows.Count > 0)
                                    {
                                        int QTY = 0;
                                        int OtherQTY = 0;
                                        if (dtI.Rows.Count > 0)
                                        {
                                            for (int a = 0; a < dtI.Rows.Count; a++)
                                            {
                                                if (dtI.Rows[a]["UOMDesc"].ToString().ToUpper() == "BOX")
                                                {
                                                    if (productcode.Contains(dtI.Rows[a]["ItemsCode"].ToString()))
                                                    {
                                                        QTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]) * 2;
                                                    }
                                                    else
                                                    {
                                                        QTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]);
                                                    }


                                                }
                                                else
                                                {
                                                    OtherQTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]);
                                                }

                                            }

                                            int nume = Convert.ToInt32(OtherQTY) / 7;
                                            int deno = Convert.ToInt32(OtherQTY) % 7;


                                            if (deno != 0 && deno <= 7)
                                            { QTY += 1; }

                                            if (nume != 0)
                                            {

                                                QTY = QTY + nume;

                                            }
                                        }


                                        items += " <tr style ='border: 1px solid black;'><td rowspan=" + dtI.Rows.Count + ">" + item.Order + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Suburb + "</td><td class='cell-hyphens' rowspan=" + dtI.Rows.Count + ">" + item.CustomerName + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Phone + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Address + "</td><td rowspan=" + dtI.Rows.Count + ">Boxes :" + QTY + "  <br>" + item.DeliveryNotes + "</td><td style='border: 1px solid #000;'>" + dtI.Rows[0]["Items"].ToString() + "</td><td align='center' style='border: 1px solid #000;'>" + dtI.Rows[0]["TotalQty"].ToString() + "</td><td style='border: 1px solid #000;'> </td></tr>";

                                        if (dtI.Rows.Count > 1)
                                        {

                                            for (int k = 1; k < dtI.Rows.Count; k++)
                                            {
                                                items += " <tr><td style='border: 1px solid #000;'>" + dtI.Rows[k]["Items"].ToString() + "</td><td align='center' style='border: 1px solid #000;'>" + dtI.Rows[k]["TotalQty"].ToString() + "</td><td style='border: 1px solid #000;'> </td></tr>";
                                            }

                                        }
                                    }

                                    //}
                                }
                                // }

                                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
                                htbl["@Items"] = items;
                                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
                                htbl["@RequriedDate"] = DeliveryDate.ToString("dd/MM/yyy");
                                htbl["@ALLWAREHOUSE"] = dt1.Rows[0][0].ToString();
                                if (WH.Contains("COAST"))
                                {

                                    htbl["@warehouse"] = "COOLUM";
                                }
                                else
                                {
                                    htbl["@warehouse"] = WH;
                                }

                                htbl["@deliveryrun"] = deliveryRun;


                                content = ReadEmailTemplateFile(Common.TemplatePath, "BulkPickListByDeliveryRunTemp_Exception.html");

                                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                                //Initialize HTML to PDF converter 
                                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                                WebKitConverterSettings settings = new WebKitConverterSettings();

                                //Set PDF page orientation 
                                settings.Orientation = PdfPageOrientation.Landscape;
                                //HTML string and Base URL 
                                string htmlText = content;

                                string baseUrl = Common.baseUrl;

                                //Set WebKit path
                                settings.WebKitPath = Common.WebKitPath;
                                settings.PdfHeader = AddPdfHeader(settings.PdfPageSize.Width, "", "");
                                settings.PdfFooter = AddPdfFooter(settings.PdfPageSize.Width, "");


                                //Assign WebKit settings to HTML converter
                                htmlConverter.ConverterSettings = settings;

                                //Convert HTML string to PDF
                                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);

                                //Save and close the PDF document 
                                document.Save(Common.PDFPath + "DeliveryRunExceptions_" + WH + "_" + i.ToString() + ".pdf");

                                document.Close(true);
                                // Process.Start("E:/Output.pdf");


                                Stream streamT1 = File.OpenRead(Common.PDFPath + "DeliveryRunExceptions_" + WH + "_" + i.ToString() + ".pdf");
                                termsList.Add(streamT1);
                            }
                        }
                        Stream[] streams = termsList.ToArray();


                        if (WH == "COAST")
                        {
                            WH = "COOLUM";
                        }
                        attachments = MergePDF(streams, "Delivery_Run_Exceptions_For_" + WH + ".pdf");
                    }
                }

                return attachments;
            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("DeliveryRunExceptions: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }


        public DateTime getServerDateTime()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            conn.Open();
            using (var cmd = new SqlCommand("select getdate()", conn))
            {
                return Convert.ToDateTime(cmd.ExecuteScalar());
            }


            //using (var _db = new W59Ftksndg0kjRf8tjkEntities())
            //{
            //    var dateQuery = _db.Database.SqlQuery<DateTime>("SELECT getdate()");
            //    DateTime serverDate = dateQuery.AsEnumerable().First();
            //  return Convert.ToDateTime(dsDR.Tables[0].Rows[0][0].ToString());//.AddDays(1);
            //}
        }

        public string MergePDF(Stream[] streams, string WH)
        {
            //Creates the destination document
            PdfDocument finalDoc = new PdfDocument();
            // Merges PDFDocument.
            PdfDocumentBase.Merge(finalDoc, streams);
            //Saves the document
            finalDoc.Save(Common.PDFPath + WH);
            //closes the document
            finalDoc.Close(true);

            return Common.PDFPath + WH;
        }

        #endregion

        #region Pick up PDF

        public string BulkPickUp(DateTime ReportDate, DateTime DeliveryDate, string warehouse)
        {
            try
            {
                string content = "";
                string attachments = "";

                string WH = "";
                string Site = "";

                List<Stream> termsList = new List<Stream>();
                List<ItemRow> orData = new List<ItemRow>();
                List<ItemRowWare> orDataWare = new List<ItemRowWare>();

                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spGetPickupDetailsForPDFReport", conn); //spGetDeliveryRunandPickUp
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@warehouse", warehouse);
                da.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Int32 PickupID = Convert.ToInt32(dt.Rows[i][0]);
                            WH = dt.Rows[i]["PickupName"].ToString();
                            Site = dt.Rows[i]["PickupAddress"].ToString();
                            DataSet dsDR = new DataSet();
                            SqlDataAdapter sda = new SqlDataAdapter("spBulkPickListByDeliveryRunandPickUp", conn);
                            sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                            sda.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                            //sda.SelectCommand.Parameters.AddWithValue("@deliveryRun", deliveryRun);
                            sda.SelectCommand.Parameters.AddWithValue("@PickupID", PickupID);
                            sda.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                            sda.Fill(dsDR);
                            orData = Common.ConvertDataTable<ItemRow>(dsDR.Tables[0]);
                            DataTable dt1 = dsDR.Tables[1];
                            if (orData.Count != 0)
                            {
                                Hashtable htbl = new Hashtable();
                                string items = "";

                                List<string> list = new List<string>();

                                foreach (ItemRow item in orData.Where(x => x.UOM.ToUpper() == "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                                {
                                    list.Add(item.ProductCode);
                                }
                                list.Add("");
                                foreach (ItemRow item in orData.Where(x => x.UOM.ToUpper() != "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                                {
                                    list.Add(item.ProductCode);
                                }

                                string[] productcode = list.ToArray();

                              //  string[] productcode = { "1520", "1519", "1523", "1521", "2072", "1522", "1524", "2003", "", "1950", "1949", "1818", "2000", "2001" };
                                // { "1520", "1519", "1523", "1521", "2072", "1522", "1524", "", "1950", "1949", "1818" };

                                for (int j = 0; j < productcode.Count(); j++)
                                {

                                    foreach (ItemRow item in orData)
                                    {
                                        if (productcode[j] == "")
                                        {
                                            items += " <tr><td> </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td></tr>";
                                        }
                                        else if (productcode[j] == item.ProductCode)
                                        {
                                            items += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + item.UOM + "</td><td align='center'>" + (double)item.QTY + "</td><td style='border: 1px solid #000;'> </td></tr>";

                                        }
                                    }
                                }

                                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
                                htbl["@Items"] = items;
                                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
                                htbl["@RequriedDate"] = DeliveryDate.ToString("dd/MM/yyy");
                                htbl["@ALLWAREHOUSE"] = dt1.Rows[0][0].ToString();
                                if (warehouse.Contains("COAST"))
                                {

                                    htbl["@warehouse"] = "COOLUM";
                                }
                                else
                                {

                                    htbl["@warehouse"] = dt.Rows[i]["PickupName"].ToString();
                                }
                              //  htbl["@warehouse"] = WH;
                                //htbl["@deliveryrun"] = deliveryRun;
                                htbl["@site"] = Site;

                                content = ReadEmailTemplateFile(Common.TemplatePath, "BulkPickUpTemp.html");

                                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                                //Initialize HTML to PDF converter 
                                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                                WebKitConverterSettings settings = new WebKitConverterSettings();

                                //HTML string and Base URL 
                                string htmlText = content;

                                string baseUrl = Common.baseUrl;

                                //Set WebKit path
                                settings.WebKitPath = Common.WebKitPath;

                            
                                //Assign WebKit settings to HTML converter
                                htmlConverter.ConverterSettings = settings;

                                //Convert HTML string to PDF
                                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);

                                //Save and close the PDF document 

                                document.Save(Common.PDFPath + "BulkPickUp_" + WH + "_" + i.ToString() + ".pdf");

                                document.Close(true);
                                // Process.Start("E:/Output.pdf");


                                Stream streamT1 = File.OpenRead(Common.PDFPath + "BulkPickUp_" + WH + "_" + i.ToString() + ".pdf");
                                termsList.Add(streamT1);
                            }
                        }
                        Stream[] streams = termsList.ToArray();

                        if (warehouse == "COAST")
                        {
                            WH = "COOLUM";
                        }
                        attachments = MergePDF(streams, "Bulk_Pick__List_For_PickUp_Orders_" + WH + ".pdf");
                    }
                }

                return attachments;
            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("BulkPickUp: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }


        public string PickUpDeliveryRunManiFest(DateTime ReportDate, DateTime DeliveryDate, string warehouse)
        {
            try
            {
                string content = "";
                string attachments = "";
                string WH = "";
                string Site = "";
                string sitePDF = "";
                List<Stream> termsList = new List<Stream>();
                List<ItemRowManifest> orData = new List<ItemRowManifest>();


                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spGetPickupDetailsForPDFReport", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@warehouse", warehouse);
                da.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string[] productcode = { "1522", "1524" };
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Int32 PickupID = Convert.ToInt32(dt.Rows[i][0]);
                            WH = dt.Rows[i]["PickupName"].ToString();
                            Site = dt.Rows[i]["PickupAddress"].ToString();
                            DataSet dsDR = new DataSet();
                            SqlDataAdapter sda = new SqlDataAdapter("spRunManiFestPickup", conn);
                            sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                            // sda.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                            sda.SelectCommand.Parameters.AddWithValue("@PickupID", PickupID);
                            sda.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                            sda.Fill(dsDR);
                            orData = Common.ConvertDataTable<ItemRowManifest>(dsDR.Tables[0]);
                            DataTable dt1 = dsDR.Tables[1];
                            if (orData.Count != 0)
                            {
                                Hashtable htbl = new Hashtable();
                                string items = "";


                                //string[] productcode = { "1520", "1519", "1523", "1521", "2072", "1522", "1524", "", "1950", "1949", "1818" };

                                //for (int j = 0; j < productcode.Count(); j++)
                                //{

                                foreach (ItemRowManifest item in orData)
                                {
                                    //if (productcode[j] == "")
                                    //{
                                    //    items += " <tr><td> </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td></tr>";
                                    //}
                                    //else if (productcode[j] == item.ProductCode)
                                    //{

                                    DataSet dsDRT = new DataSet();
                                    SqlDataAdapter sdaT = new SqlDataAdapter("spRunManiFestPickupOrderItems", conn);
                                    sdaT.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    //  sdaT.SelectCommand.Parameters.AddWithValue("@warehouse", WH);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@PickupID", PickupID);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@Order", item.Order);
                                    sdaT.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);
                                    sdaT.Fill(dsDRT);
                                    DataTable dtI = dsDRT.Tables[0];
                                    if (dtI.Rows.Count > 0)
                                    {
                                        int QTY = 0;
                                        int OtherQTY = 0;
                                        if (dtI.Rows.Count > 0)
                                        {
                                            for (int a = 0; a < dtI.Rows.Count; a++)
                                            {
                                                if (dtI.Rows[a]["UOMDesc"].ToString().ToUpper() == "BOX")
                                                {
                                                    if (productcode.Contains(dtI.Rows[a]["ItemsCode"].ToString()))
                                                    {
                                                        QTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]) * 2;
                                                    }
                                                    else
                                                    {
                                                        QTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]);
                                                    }


                                                }
                                                else
                                                {
                                                    OtherQTY += Convert.ToInt32(dtI.Rows[a]["TotalQty"]);
                                                }

                                            }

                                            int nume = Convert.ToInt32(OtherQTY) / 7;
                                            int deno = Convert.ToInt32(OtherQTY) % 7;

                                            if (deno != 0 && deno <= 7)
                                            { QTY += 1; }

                                            if (nume != 0)
                                            {

                                                QTY = QTY + nume;

                                            }

                                        }

                                        items += " <tr style ='border: 1px solid black;'><td rowspan=" + dtI.Rows.Count + ">" + item.Order + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Suburb + "</td><td class='cell-hyphens' rowspan=" + dtI.Rows.Count + ">" + item.CustomerName + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Phone + "</td><td rowspan=" + dtI.Rows.Count + ">" + item.Address + "</td><td rowspan=" + dtI.Rows.Count + ">Boxes : " + QTY + "<br>" + item.DeliveryNotes + "</td><td style='border: 1px solid #000;'>" + dtI.Rows[0]["Items"].ToString() + "</td><td align='center' style='border: 1px solid #000;'>" + dtI.Rows[0]["TotalQty"].ToString() + "</td><td style='border: 1px solid #000;'> </td></tr>";

                                        if (dtI.Rows.Count > 1)
                                        {

                                            for (int k = 1; k < dtI.Rows.Count; k++)
                                            {
                                                items += " <tr><td  style='border: 1px solid #000;'>" + dtI.Rows[k]["Items"].ToString() + "</td><td align='center' style='border: 1px solid #000;'>" + dtI.Rows[k]["TotalQty"].ToString() + "</td><td style='border: 1px solid #000;'> </td></tr>";
                                            }

                                        }
                                    }

                                    //}
                                }
                                // }

                                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
                                htbl["@Items"] = items;
                                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
                                htbl["@RequriedDate"] = DeliveryDate.ToString("dd/MM/yyy");
                                htbl["@ALLWAREHOUSE"] = dt1.Rows[0][0].ToString();


                                if (warehouse.Contains("COAST"))
                                {

                                    htbl["@warehouse"] = "COOLUM";
                                }
                                else
                                {

                                    htbl["@warehouse"] = dt.Rows[i]["PickupName"].ToString();
                                }
                                // htbl["@warehouse"] = WH;
                                //  htbl["@deliveryrun"] = deliveryRun;
                                htbl["@site"] = Site;// orData[0].ShippingMethodTitle ?? "";

                                content = ReadEmailTemplateFile(Common.TemplatePath, "PickUpDeliveryRunManiFest_Manifest.html");

                                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                                //Initialize HTML to PDF converter 
                                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                                WebKitConverterSettings settings = new WebKitConverterSettings();

                                //Set PDF page orientation 
                                settings.Orientation = PdfPageOrientation.Landscape;
                                //HTML string and Base URL 
                                string htmlText = content;

                                string baseUrl = Common.baseUrl;

                                //Set WebKit path
                                settings.WebKitPath = Common.WebKitPath;
                                settings.PdfHeader = AddPdfHeader(settings.PdfPageSize.Width, "", "");
                                settings.PdfFooter = AddPdfFooter(settings.PdfPageSize.Width, "");
                                //Assign WebKit settings to HTML converter
                                htmlConverter.ConverterSettings = settings;

                                //Convert HTML string to PDF
                                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);

                                string[] city = { "Morningside", "Sandgate", "Burleigh Heads", "Coolum Beach", "Byron Bay", "Queens Park", "Slacks Creek", "Pimpama" };



                                foreach (string c in city)
                                {
                                    if (Site.Contains(c))
                                    {
                                        sitePDF = c;
                                    }
                                }

                                //Save and close the PDF document PickUp_Site_Brisbane_Morningside.pdf
                                document.Save(Common.PDFPath + "PickUp_Mainifest_For_" + WH.Replace(" ", "") + "_" + sitePDF.Replace(" ","") + ".pdf");

                                document.Close(true);
                                // Process.Start("E:/Output.pdf");


                                //Stream streamT1 = File.OpenRead(Common.PDFPath + "PickUpDeliveryRunManiFest_" + WH + "_" + i.ToString() + ".pdf");
                                //termsList.Add(streamT1);
                            }
                        }
                        //Stream[] streams = termsList.ToArray();


                        //if (warehouse == "COAST")
                        //{
                        //    WH = "COOLUM";
                        //}
                        //attachments = MergePDF(streams, "PickUpDeliveryRunManiFest_" + WH + ".pdf");
                        attachments = Common.PDFPath + "PickUp_Mainifest_For_" + WH.Replace(" ", "") + "_" + sitePDF.Replace(" ", "") + ".pdf";

                    }

                }

                return attachments;
            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("PickUpDeliveryRunManiFest: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }
        #endregion

        #region Exception report


        public string ExceptionReport(DateTime ReportDate, DateTime DeliveryDate)
        {
            try
            {
                string content = "";
                List<ItemRow> orData = new List<ItemRow>();
                List<ItemRowWare> orDataWare = new List<ItemRowWare>();

                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spGetExceptionreport", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@date", DeliveryDate);


                da.Fill(ds);

                DataTable dt = ds.Tables[0];
                string MissWarehouse = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MissWarehouse += dt.Rows[i][0].ToString() + ',';
                }
                if (MissWarehouse == "")
                { MissWarehouse = "No Record Found"; }
                else { MissWarehouse = MissWarehouse.TrimEnd(','); }

                DataTable dt1 = ds.Tables[1];
                string MissDeliveryZone = ""; ;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    MissDeliveryZone += dt1.Rows[i][0].ToString() + ',';
                }
                if (MissDeliveryZone == "")
                { MissDeliveryZone = "No Record Found"; }
                else { MissDeliveryZone = MissDeliveryZone.TrimEnd(','); }

                DataTable dt2 = ds.Tables[2];
                string MissPostcode = ""; ;
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    MissPostcode += dt2.Rows[i][0].ToString() + ',';
                }
                if (MissPostcode == "")
                { MissPostcode = "No Record Found"; }
                else { MissPostcode = MissPostcode.TrimEnd(','); }

                DataTable dt3 = ds.Tables[3];
                string MissAddress = "";
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    MissAddress += dt3.Rows[i][0].ToString() + ',';
                }
                if (MissAddress == "")
                { MissAddress = "No Record Found"; }
                else { MissAddress = MissAddress.TrimEnd(','); }

                DataTable dt4 = ds.Tables[4];
                string MissPhone = "";
                for (int i = 0; i < dt4.Rows.Count; i++)
                {
                    MissPhone += dt4.Rows[i][0].ToString() + ',';
                }
                if (MissPhone == "")
                { MissPhone = "No Record Found"; }
                else { MissPhone = MissPhone.TrimEnd(','); }

                Hashtable htbl = new Hashtable();
                //string items = "";
                //double TOTALQty = 0;



                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";

                htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
                htbl["@RequriedDate"] = DeliveryDate.ToString("dd/MM/yyy");
                htbl["@ALLWAREHOUSE"] = ds.Tables[5].Rows[0][0].ToString();
                string warehouse = "";

                warehouse += "<tr><td>Missing Delivery Date</td><td>No Record Found</td></tr>";
                warehouse += "<tr><td>Missing Warehouse</td><td>" + MissWarehouse.TrimEnd(',') + "</td></tr>";
                warehouse += "<tr><td>Missing Delivery Run</td><td>" + MissDeliveryZone.TrimEnd(',') + "</td></tr>";
                warehouse += "<tr><td>Missing Shipping Post Code</td><td>" + MissPostcode.TrimEnd(',') + "</td></tr>";
                warehouse += "<tr><td>Missing Suburb</td><td>" + MissAddress.TrimEnd(',') + "</td></tr>";
                warehouse += "<tr><td>Missing Mobile Number</td><td>" + MissPhone.TrimEnd(',') + "</td></tr>";

                htbl["@Warehouse"] = warehouse;
                content = ReadEmailTemplateFile(Common.TemplatePath, "ExceptionReport.html");

                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                //Initialize HTML to PDF converter 
                HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                WebKitConverterSettings settings = new WebKitConverterSettings();

                //HTML string and Base URL 
                string htmlText = content;

                string baseUrl = Common.baseUrl;

                //Set WebKit path
                settings.WebKitPath = Common.WebKitPath;

                //Assign WebKit settings to HTML converter
                htmlConverter.ConverterSettings = settings;

                //Convert HTML string to PDF
                PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);

                //Save and close the PDF document 
                document.Save(Common.PDFPath + "ExceptionReport.pdf");

                document.Close(true);
                // Process.Start("E:/Output.pdf");

                return Common.PDFPath + "ExceptionReport.pdf";

            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("ExceptionReport: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }

        #endregion

        #region Read Email Template File and Replace File Variables
        private PdfPageTemplateElement AddPdfHeader(float width, string title, string description)
        {
            RectangleF rect = new RectangleF(0, 0, width, 50);
            //Create a new instance of PdfPageTemplateElement class.     
            PdfPageTemplateElement header = new PdfPageTemplateElement(rect);
            PdfGraphics g = header.Graphics;

            //Draw title.
            PdfFont font = new PdfTrueTypeFont(new Font("Helvetica", 16, FontStyle.Bold), true);
            PdfSolidBrush brush = new PdfSolidBrush(Color.FromArgb(44, 71, 120));
            float x = (width / 2) - (font.MeasureString(title).Width) / 2;
            g.DrawString(title, font, brush, new RectangleF(x, (rect.Height / 4) + 3, font.MeasureString(title).Width, font.Height));

            //Draw description
            brush = new PdfSolidBrush(Color.Gray);
            font = new PdfTrueTypeFont(new Font("Helvetica", 6, FontStyle.Bold), true);
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Left, PdfVerticalAlignment.Bottom);
            g.DrawString(description, font, brush, new RectangleF(0, 0, header.Width, header.Height - 8), format);



            return header;
        }
        private PdfPageTemplateElement AddPdfFooter(float width, string footerText)
        {
            RectangleF rect = new RectangleF(0, 0, width, 50);
            //Create a new instance of PdfPageTemplateElement class.
            PdfPageTemplateElement footer = new PdfPageTemplateElement(rect);
            PdfGraphics g = footer.Graphics;

            // Draw footer text.
            PdfSolidBrush brush = new PdfSolidBrush(Color.Gray);
            PdfFont font = new PdfTrueTypeFont(new Font("Helvetica", 6, FontStyle.Bold), true);
            float x = (width / 2) - (font.MeasureString(footerText).Width) / 2;
            g.DrawString(footerText, font, brush, new RectangleF(x, g.ClientSize.Height - 10, font.MeasureString(footerText).Width, font.Height));

            //Create page number field
            PdfPageNumberField pageNumber = new PdfPageNumberField(font, brush);

            //Create page count field
            PdfPageCountField count = new PdfPageCountField(font, brush);

            PdfCompositeField compositeField = new PdfCompositeField(font, brush, "Page {0} of {1}", pageNumber, count);
            compositeField.Bounds = footer.Bounds;
            compositeField.Draw(g, new PointF(750, 10));

            return footer;
        }

        private static string ReadEmailTemplateFile(string filePath, string fileName)
        {
            string str = string.Empty;

            string path = filePath + fileName;
            //return path;
            if (System.IO.File.Exists(path))
            {
                try
                {
                    StreamReader reader = System.IO.File.OpenText(path);
                    str = reader.ReadToEnd();
                    reader.Close();
                    return str;
                }
                catch (Exception)
                {
                    throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
                }
            }
            throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
        }

        public static string ReplaceFileVariablesInTemplateFile(Hashtable hashVars, string content)
        {
            IDictionaryEnumerator enumerator = hashVars.GetEnumerator();
            while (enumerator.MoveNext())
            {
                content = content.Replace(enumerator.Key.ToString(), enumerator.Value.ToString());
            }
            return content;
        }
        #endregion

        #region Create log for the actions.

        public void log(string ex, string type)
        {
            if (System.IO.File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (System.IO.Stream str = System.IO.File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.
    }

    public class ItemRow
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string UOM { get; set; }
        public double QTY { get; set; }
        public long CustomerID { get; set; }
        public string ShippingMethod { get; set; }

    }
    public class ItemRowWare
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public double BRIS { get; set; }
        public double GC { get; set; }
        public double TWB { get; set; }
        public double COAST { get; set; }
        public double BYRON { get; set; }
    }


    public class ItemRowManifest
    {
        public string Order { get; set; }
        public string Suburb { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string DeliveryNotes { get; set; }
        public string Items { get; set; }
        public double TotalQty { get; set; }
        public string ShippingMethodTitle { get; set; }


    }

    public class Common
    {

        public static string TemplatePath = Convert.ToString(ConfigurationManager.AppSettings["TemplatePath"]);
        public static string WebKitPath = Convert.ToString(ConfigurationManager.AppSettings["WebKitPath"]);
        public static string baseUrl = Convert.ToString(ConfigurationManager.AppSettings["baseUrl"]);
        public static string PDFPath = Convert.ToString(ConfigurationManager.AppSettings["PDFPath"]);

        public static string CompanyAdminEmail_CC = Convert.ToString(ConfigurationManager.AppSettings["CompanyAdminEmail_CC"]);
        public static string FromEmail = Convert.ToString(ConfigurationManager.AppSettings["FROM_EMAILID"]);

        public static string CompanyAdminEmail = Convert.ToString(ConfigurationManager.AppSettings["CompanyAdminEmail"]);

        public static string LogoPath = Convert.ToString(ConfigurationManager.AppSettings["LogoPath"]);
        public static bool ForAll = Convert.ToBoolean(ConfigurationManager.AppSettings["ForALL"].ToString() == "1");


        public static string BRISemail = Convert.ToString(ConfigurationManager.AppSettings["BRIS"]);
        public static string COASTemail = Convert.ToString(ConfigurationManager.AppSettings["COAST"]);
        public static string BYRONemail = Convert.ToString(ConfigurationManager.AppSettings["BYRON"]);
        //public static string GCemail = Convert.ToString(ConfigurationManager.AppSettings["GC"]);

        //public static string TWBemail = Convert.ToString(ConfigurationManager.AppSettings["TWB"]);
        public static string MainAdminemail = Convert.ToString(ConfigurationManager.AppSettings["MainAdmin"]);



        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
