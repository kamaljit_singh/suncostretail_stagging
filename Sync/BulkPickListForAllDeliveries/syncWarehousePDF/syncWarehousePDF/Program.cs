﻿using syncWarehousePDF.BO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace syncWarehousePDF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                System.IO.DirectoryInfo di = new DirectoryInfo(Common.PDFPath);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

                Order obj = new Order();
                DateTime ReportDate = obj.getServerDateTime().AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["ReportDays"]));

                DateTime DeliveryDate = ReportDate.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["DeliveryDays"]));

                #region Exception report
                // string report = obj.ExceptionReport(ReportDate, DeliveryDate);

                #endregion

                #region Delivery PDF              
                //----------------------------Bulk Pick List ALL Warehouse PDF-----------------------------------------------------------------------------------------

                obj.log("--------------------------------------------------------", "//");
                obj.log("Start", "BulkPickListALLWarehouse");
                obj.log("--------------------------------------------------------", "//");
                string attachments = "";

                string f1 = obj.OrderPDFForSummary(ReportDate, DeliveryDate); // Bulk Pick List - All Warehouse

                if (f1 != "")
                {

                    //-------------------------------------------------------------------------------
                    //------------------NEED REVIEW--------------------------------------------------
                    //-------------------------------------------------------------------------------



                    Email("", f1);
                    obj.log("--------------------------------------------------------", "//");
                    obj.log("Start", "Bulk Pick List - By Delivery Run");
                    obj.log("--------------------------------------------------------", "//");

                    //MONDAY 12:15 AM AUTOMATED REPORTS FOR TUESDAY DELIVERIES(ALL LOCATIONS)

                    //TUESDAY 12:15AM AUTOMATED REPORTS FOR WEDNESDAY DELIVERIES(BRISBANE &SUNSHINE COAST)

                    //WEDNESDAY 12:15AM AUTOMATED REPORTS FOR THURSDAY DELIVERIES(BRISBANE &SUNSHINE COAST)

                    //THURSDAY 12:15AM AUTOMATED REPORTS FOR FRIDAY DELIVERIES(ALL LOCATIONS)

                    //===================================================================New Rule 03/06/2020========================================================================================================

                    //MONDAYS – REPORTS FOR BRISBANE, GC, TWB, COOLUM, BYRON FOR TUESDAY PICK UP AND DELIVERIES

                    //TUESDAYS – REPORTS FOR BRISBANE & COOLUM FOR WEDNESDAY DELIVERIES + CAPULET & CO WEDNESDAY PICK UPS

                    //WEDNESDAYS – REPORTS FOR BRISBANE & COOLUM FOR THURSDAY PICK UP AND DELIVERIES +GOLD COAST WHISTLESTOP PICK UP REPORT FOR THURSDAY PICK UP + BRISBANE EXTRACTION COFFEE PICK UPS

                    //THURSDAYS – REPORTS FOR BRISBANE, GC, TWB, COOLUM, BYRON FOR TUESDAY PICK UP AND DELIVERIES

                    string day = ReportDate.DayOfWeek.ToString();
                    string[] warehouse = new string[] { };

                    if (day.ToUpper() == "TUESDAY" || day.ToUpper() == "WEDNESDAY")
                    {
                        warehouse = new string[2] { "BRIS", "COAST" };
                    }
                    else
                    {
                        warehouse = new string[5] { "BRIS", "GC", "TWB", "COAST", "BYRON" };
                    }
                    // string[] warehouse =  { "BRIS", "GC", "TWB", "COAST", "BYRON" };

                    foreach (string WH in warehouse)
                    {

                        string f2 = obj.BulkPickListByDeliveryRun(ReportDate, DeliveryDate, WH); // Bulk Pick List - By Delivery Run

                        if (f2 != "")
                        {
                            attachments += f2 + ";";
                        }
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Start", "Delivery Run ManiFest");
                        obj.log("--------------------------------------------------------", "//");

                        string f3 = obj.DeliveryRunManiFest(ReportDate, DeliveryDate, WH); // Delivery Run ManiFest

                        if (f3 != "")
                        {
                            attachments += f3 + ";";
                        }
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Start", "Delivery Run ManiFest Exceptions");
                        obj.log("--------------------------------------------------------", "//");
                        string f7 = obj.DeliveryRunManiFestExceptions(ReportDate, DeliveryDate, WH); // Delivery Run ManiFest Exceptions

                        if (f7 != "")
                        {
                            attachments += f7 + ";";
                        }
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Start", "Bulk PickUp");
                        obj.log("--------------------------------------------------------", "//");

                        string f4 = obj.BulkPickUp(ReportDate, DeliveryDate, WH);

                        if (f4 != "")
                        {
                            attachments += f4 + ";";
                        }
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Start", "PickUp Delivery Run ManiFest");
                        obj.log("--------------------------------------------------------", "//");
                        string f5 = obj.PickUpDeliveryRunManiFest(ReportDate, DeliveryDate, WH);

                        if (f5 != "")
                        {
                            attachments += f5 + ";";
                        }

                        string tempwarehouse = "";

                        if (warehouse.Count() == 2)
                        {
                            tempwarehouse = "BRIS";
                        }
                        else
                        {
                            tempwarehouse = "TWB";

                        }

                        if (warehouse.Count() == 2 && WH == tempwarehouse && day.ToUpper() == "WEDNESDAY")
                        {
                            obj.log("--------------------------------------------------------", "//");
                            obj.log("Start", "PickUp Delivery Run ManiFest :: GOLD COAST WHISTLESTOP PICK UP REPORT FOR THURSDAY PICK UP");
                            obj.log("--------------------------------------------------------", "//");
                            string f6 = obj.PickUpDeliveryRunManiFest(ReportDate, DeliveryDate, "GC");

                            if (f6 != "")
                            {
                                attachments += f6 + ";";
                            }                       
                        }
                        if (WH == tempwarehouse)
                        {
                            Email("BRIS", attachments.TrimEnd(';'));
                            attachments = "";
                        }

                        if (WH == "COAST" || WH == "BYRON")
                        {
                            Email(WH, attachments.TrimEnd(';'));
                            attachments = "";
                        }

                    }


                }
                else
                {
                    obj.log("--------------------------------------------------------", "//");
                    obj.log("No Record Found", "PDF");
                    obj.log("--------------------------------------------------------", "//");
                }




                #endregion



            }
            catch (Exception ex)
            {

                throw;
            }
        }

        static void Email(string WH, string attachments)
        {
            string to = "";
            string subject = "";
            string result = "";
            string body = "";
            Order obj = new Order();

            if (Common.ForAll)
            {

                if (WH == "BRIS")
                {
                    to = Common.BRISemail;
                }
                else if (WH == "COAST")
                {
                    to = Common.COASTemail;
                }
                else if (WH == "BYRON")
                {
                    to = Common.BYRONemail;
                }
            
                else
                {
                    to = Common.MainAdminemail;
                    WH = "For All";
                }
            }
            else
            {
                to = Common.CompanyAdminEmail;
            }

            subject = ConfigurationManager.AppSettings["CompanyEmailSubject"].ToString();

            body = "Dear Suncoast Fresh,"
                  + "<br/><br/>"
                  + "Please find your daily delivery run reports for tomorrow."
                  + "<br/><br/>"
                  + "If there are any errors or omissions, please email support@saavi.com.au"
                  + " <br/><br/>"
                  + "Regards"
                  + "<br/><br/>"
                  + "Many Thanks <br/>"
                  + "From the team at SAAVI";

            result =  SendMail.SendEMail(to, Common.CompanyAdminEmail_CC, "", subject, body, attachments, Common.FromEmail);

            if (result == "ok")
            {
                obj.log("--------------------------------------------------------", "//");
                obj.log("Email Sent Successfully", WH);
                obj.log("--------------------------------------------------------", "//");
            }
            else
            {
                obj.log("--------------------------------------------------------", "//");
                obj.log("Email Not Sent", result);
                obj.log("--------------------------------------------------------", "//");
            }
        }
    }
}
