﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RydgesSync
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                log("Sync process started.", "Info");

                ReadFileAndInsertToDB(ConfigurationManager.AppSettings["Suburb"].ToString(), ConfigurationManager.AppSettings["SheetNameSuburb"].ToString());

                runSyncProcedures("spImportSuburb");

                ReadFileAndInsertToDB(ConfigurationManager.AppSettings["DeliveryRuns"].ToString(), ConfigurationManager.AppSettings["SheetNameDeliveryRuns"].ToString());

                runSyncProcedures("spImportDeliveryRuns");

                log("Sync process completed.", "Info");
            }
            catch (Exception ex)
            {
                log(ex.Message, "Error");
            }
        }

        public static void ReadFileAndInsertToDB(string fileName, string sheetName = "")
        {
            DataTable dt = GetDataTableFromExcel(ConfigurationManager.AppSettings["FilePath"] + fileName, sheetName);

            //FOR CSV
            // DataTable dt = GetDataTableFromCSV(ConfigurationManager.AppSettings["FilePath"] + fileName, sheetName);

            if (dt != null)
            {
                InsertDataIntoSQLServerUsingSQLBulkCopy(dt, fileName);

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ArchiveFile"]))
                {
                    Deletefiles(fileName);
                }
            }
            else
            {
                log("Failed to read the specified file", "Info");
            }
        }

        #region Read the data from the csv file and add to Datatable

        private static DataTable GetDataTableFromCSV(string csv_file_path, string sheetName = "")
        {
            if (!File.Exists(csv_file_path))
            {
                log("File not found [" + csv_file_path + "]", "info");
                return null;
            }

            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { ConfigurationManager.AppSettings["Delimiter"].ToString() });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column.Trim());
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        try
                        {
                            string[] fieldData = csvReader.ReadFields();
                            List<string> liS = new List<string>();
                            if (fieldData.Length > colFields.Length)
                            {
                                liS = fieldData.ToList();
                                liS.RemoveAt(fieldData.Length - 1);
                            }
                            else
                            {
                                liS = fieldData.ToList();
                            }

                            csvData.Rows.Add(liS.ToArray());
                        }
                        catch (Exception ex)
                        {
                            log("Export from csv failed " + csv_file_path, "Error: " + ex.Message);
                            continue;
                        }
                    }
                    log("Import into Datatable " + csv_file_path, "info");
                }
            }
            catch (Exception ex)
            {
                log("Export from csv failed " + csv_file_path, "Error: " + ex.Message);
                return null;
            }
            return csvData;
        }

        #endregion Read the data from the csv file and add to Datatable

        #region Get data from excel file.

        public static DataTable GetDataTableFromExcel(string filepath, string sheetName = "")
        {
            try
            {
                if (sheetName == "")
                {
                    sheetName = "sheet1";
                }
                string path = System.IO.Path.GetFullPath(filepath);
                // +TableName;
                OleDbConnection oledbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                path + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                oledbConn.Open();
                string commandTxt = "SELECT * FROM [" + sheetName + "$]";
                DataTable dt = new DataTable();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                cmd.Connection = oledbConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = commandTxt;
                cmd.CommandTimeout = 0;
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(dt);
                oledbConn.Close();

                return dt;
            }
            catch (Exception ex)
            {
                log("Exception in GetDataTableFromExcel :: " + ex.Message, "Error");
                return null;
            }
        }

        #endregion Get data from excel file.

        #region Bulk upload the data into database.

        private static void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData, string TableName)
        {
            try
            {
                //TableName = Path.GetFileNameWithoutExtension(TableName);
                if (String.Compare(TableName, ConfigurationManager.AppSettings["Suburb"].ToString(), true) == 0)
                {
                    TableName = "ImportSuburb";
                }
                else if (string.Compare(TableName, ConfigurationManager.AppSettings["DeliveryRuns"].ToString(), true) == 0)
                {
                    TableName = "ImportDeliveryRuns";
                }

                if (TableName.Length > 0)
                {
                    using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString()))
                    {
                        dbConnection.Open();
                        SqlCommand cmd = new SqlCommand("Truncate Table " + TableName + ";", dbConnection);
                        cmd.ExecuteNonQuery();
                        using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                        {
                            s.BulkCopyTimeout = 0;
                            s.DestinationTableName = TableName;
                            s.WriteToServer(csvFileData);
                        }
                        log("Bulk inserted " + TableName, "Info");
                    }
                }
            }
            catch (Exception ex)
            {
                log("Bulk insert failed for " + TableName, "Error: " + ex.Message);
                throw;
            }
        }

        #endregion Bulk upload the data into database.

        #region Finally run the sync procedure to add the data to the main tables.

        private static void runSyncProcedures(string spName)
        {
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString()))
                {
                    dbConnection.Open();
                    SqlCommand cmd = new SqlCommand(spName, dbConnection);
                    cmd.CommandTimeout = 0;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                log("Error in runSyncProcedures. Message :" + ex.Message, "Error");
            }
        }

        #endregion Finally run the sync procedure to add the data to the main tables.

        #region Rename and move file to Archived

        public static void Deletefiles(string fileName)
        {
            try
            {
                string date = DateTime.Now.ToShortDateString().Replace("/", "");
                string newname = Guid.NewGuid().ToString() + "_" + Path.GetFileNameWithoutExtension(fileName) + "_" + date + Path.GetExtension(fileName);

                string Source = ConfigurationManager.AppSettings["FilePath"] + fileName;
                string Destination = ConfigurationManager.AppSettings["ArchivePath"] + newname;
                System.IO.File.Move(Source, Destination);
            }
            catch (Exception ex)
            {
                log("File not moved :: " + ex.Message, "Error");
            }
        }

        #endregion Rename and move file to Archived

        #region Create log for the actions.

        private static void log(string ex, string type)
        {
            if (File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (Stream str = File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.
    }
}