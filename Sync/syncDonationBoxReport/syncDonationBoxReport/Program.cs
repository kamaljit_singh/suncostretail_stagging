﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace syncDonationBoxReport
{
    class Program
    {
        static void Main(string[] args)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(Common.PDFPath);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            Order obj = new Order();
            DateTime ReportDate = obj.getServerDateTime();

            DateTime ToDate = ReportDate.AddDays(-1);
            DateTime FromDate = ReportDate.AddDays(-7);

            #region Delivery PDF    
            string day = ReportDate.DayOfWeek.ToString();
            if (day.ToUpper() == "MONDAY")
            {
                obj.log("--------------------------------------------------------", "//");
                obj.log("Start", "Donation Box Summary");
                obj.log("--------------------------------------------------------", "//");

                String dy = ReportDate.Day.ToString("00");
                String mn = ReportDate.Month.ToString("00");
                String yy = ReportDate.Year.ToString();

                string attachments =  obj.PDFForDonationBoxSummary(ReportDate, ToDate, FromDate);
                if (attachments != "")
                {
                    string subject = "DONATION BOX REPORT – MONDAY  " + dy + "  " + mn + "  " + yy;

                    string body = "Dear Suncoast Admin,"
                          + "<br/><br/>"
                          + "Please find a list below of all the donation boxes for the following period."
                          + "<br/>"
                          + "Monday " + FromDate.ToString("dd/MM/yyy") + " to Sunday " + ToDate.ToString("dd/MM/yyy")
                          + "<br/><br/>"
                          + "If there are any errors or omissions, please email support@saavi.com.au"
                          + " <br/><br/>"
                          + "Regards"
                          + "<br/><br/>"
                          + "Many Thanks <br/>"
                          + "From the team at SAAVI";

                    string result =  SendMail.SendEMail(Common.CompanyAdminEmail, Common.CompanyAdminEmail_CC, "", subject, body, attachments, Common.FromEmail);

                    if (result == "ok")
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Sent Successfully", "Donation Box");
                        obj.log("--------------------------------------------------------", "//");
                    }
                    else
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Not Sent", result);
                        obj.log("--------------------------------------------------------", "//");
                    }

                }
                obj.log("--------------------------------------------------------", "//");
                obj.log("End", "Donation Box Summary");
                obj.log("--------------------------------------------------------", "//");


                string attachmentsCSV = obj.csvPromoCodeSummary(ReportDate, ToDate, FromDate);
                if (attachmentsCSV != "")
                {
                    string subject = "PROMO CODE REPORT – MONDAY  " + dy + "  " + mn + "  " + yy;

                    string body = "Dear Suncoast Admin,"
                          + "<br/><br/>"
                          + "Please find a list below of all the promo code for the following period."
                          + "<br/>"
                          + "Monday " + FromDate.ToString("dd/MM/yyy") + " to Sunday " + ToDate.ToString("dd/MM/yyy")
                          + "<br/><br/>"
                          + "If there are any errors or omissions, please email support@saavi.com.au"
                          + " <br/><br/>"
                          + "Regards"
                          + "<br/><br/>"
                          + "Many Thanks <br/>"
                          + "From the team at SAAVI";

                    string result = SendMail.SendEMail(Common.CompanyAdminEmail, Common.CompanyAdminEmail_CC, "", subject, body, attachmentsCSV, Common.FromEmail);

                    if (result == "ok")
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Sent Successfully", "Promo Code");
                        obj.log("--------------------------------------------------------", "//");
                    }
                    else
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Not Sent", result);
                        obj.log("--------------------------------------------------------", "//");
                    }

                }
                obj.log("--------------------------------------------------------", "//");
                obj.log("End", "Promo Code Summary");
                obj.log("--------------------------------------------------------", "//");

            }
            #endregion

        }
    }

    public class Common
    {

        public static string TemplatePath = Convert.ToString(ConfigurationManager.AppSettings["TemplatePath"]);
        public static string WebKitPath = Convert.ToString(ConfigurationManager.AppSettings["WebKitPath"]);
        public static string baseUrl = Convert.ToString(ConfigurationManager.AppSettings["baseUrl"]);
        public static string PDFPath = Convert.ToString(ConfigurationManager.AppSettings["PDFPath"]);

        public static string CompanyAdminEmail_CC = Convert.ToString(ConfigurationManager.AppSettings["CompanyAdminEmail_CC"]);
        public static string FromEmail = Convert.ToString(ConfigurationManager.AppSettings["FROM_EMAILID"]);

        public static string CompanyAdminEmail = Convert.ToString(ConfigurationManager.AppSettings["CompanyAdminEmail"]);

        public static string LogoPath = Convert.ToString(ConfigurationManager.AppSettings["LogoPath"]);




        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
