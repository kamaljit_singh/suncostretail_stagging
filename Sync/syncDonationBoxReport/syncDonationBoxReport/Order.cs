﻿using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace syncDonationBoxReport
{
    public class Order
    {


        public string PDFForDonationBoxSummary(DateTime ReportDate, DateTime ToDate, DateTime FromDate)
        {
            string content = "";
            List<ItemRow> orData = new List<ItemRow>();
            Hashtable htbl = new Hashtable();
            string items = "";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            SqlDataAdapter da = new SqlDataAdapter("spGetDonationBoxSummary", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@Todate", ToDate);
            da.SelectCommand.Parameters.AddWithValue("@Fromdate", FromDate);

            da.Fill(ds);

          
            orData = Common.ConvertDataTable<ItemRow>(ds.Tables[2]);

            DataTable dt1 = ds.Tables[0];
            DataTable dt2 = ds.Tables[1];
            if (orData.Count == 0)
            {
                return "";
            }
            double GrandTotal = 0;
            foreach (ItemRow item in orData)
            {
                items += " <tr><td>" + item.OrderDate.ToString("dd/MM/yyy") + "</td><td>" + item.CartID + "</td><td>" + item.CustomerName + "</td><td align='center'>" + (double)item.Quantity + "</td><td>" + item.ProductName + "</td><td align='center'>" + "$"+ String.Format("{0:0.00}", item.Price) + "</td><td align='center'>" + "$" + String.Format("{0:0.00}", item.Total) + "</td></tr>";
                GrandTotal = GrandTotal + item.Total;
            }
            items += " <tr><td></td><td></td><td></td><td></td><td></td><td align='center'></td><td align='center'></td></tr>";

            items += " <tr><td></td><td></td><td></td><td></td><td></td><td align='center'></td><td align='center'></td></tr>";

            items += " <tr><td></td><td></td><td></td><td></td><td></td><td align='center' style='font-weight: bold;'>Grand Total</td><td align='center' style='font-weight: bold;'>" + "$" + String.Format("{0:0.00}", GrandTotal) + "</td></tr>";

            htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
            htbl["@Items"] = items;
            htbl["@Date"] = ReportDate.ToString("dd/MM/yyy");
            htbl["@RequriedDate"] = "Monday " + FromDate.ToString("dd/MM/yyy") + " to Sunday " + ToDate.ToString("dd/MM/yyy");
           
            string producttotal = "";
            producttotal += "<tr><td style='font-weight: bold;'>Total Boxes:</td><td align='center'></td><td align='center'></td></tr>";
            producttotal += "<tr><td>Donate a Box</td><td align='center' style='font-weight: bold;'>" + dt1.Rows[0][0].ToString() + "</td><td align='center' style='font-weight: bold;'>$" + String.Format("{0:0.00}", dt1.Rows[0][1]) + "</td></tr>";
            producttotal += "<tr><td>Donate a Box to Australia Zoo</td><td align='center' style='font-weight: bold;'>" + dt2.Rows[0][0].ToString() + "</td><td align='center' style='font-weight: bold;'>$" + String.Format("{0:0.00}", dt2.Rows[0][1]) + "</td></tr>";
            producttotal += "<tr><td></td><td align='center' style='font-weight: bold;'>TOTAL</td><td align='center' style='font-weight: bold;'>$" + String.Format("{0:0.00}", GrandTotal) + "</td></tr>";
            htbl["@producttotal"] = producttotal;

            content = ReadEmailTemplateFile(Common.TemplatePath, "DonationBoxSummary.html");

            content = ReplaceFileVariablesInTemplateFile(htbl, content);

            //Initialize HTML to PDF converter 
            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

            WebKitConverterSettings settings = new WebKitConverterSettings();

            //Set PDF page orientation 
            settings.Orientation = PdfPageOrientation.Landscape;
            //HTML string and Base URL 
            string htmlText = content;

            string baseUrl = Common.baseUrl;

            //Set WebKit path
            settings.WebKitPath = Common.WebKitPath;

            //Assign WebKit settings to HTML converter
            htmlConverter.ConverterSettings = settings;

            //Convert HTML string to PDF
            PdfDocument document = htmlConverter.Convert(htmlText, baseUrl);

            //Save and close the PDF document 
            document.Save(Common.PDFPath + "Donation_Boxes_Report.pdf");

            document.Close(true);
            // Process.Start("E:/Output.pdf");

            return Common.PDFPath + "Donation_Boxes_Report.pdf";
        }


        public string csvPromoCodeSummary(DateTime ReportDate, DateTime ToDate, DateTime FromDate)
        {
         
            List<ItemRowPromoCode> li = new List<ItemRowPromoCode>();     
       

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            SqlDataAdapter da = new SqlDataAdapter("spGetPromoCodeSummary", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@Todate", ToDate);
            da.SelectCommand.Parameters.AddWithValue("@Fromdate", FromDate);

            da.Fill(ds);


            li = Common.ConvertDataTable<ItemRowPromoCode>(ds.Tables[0]);

          
            if (li.Count == 0)
            {
                return "";
            }


            string dy = ReportDate.Day.ToString("00");
            string mn = ReportDate.Month.ToString("00");
            string yy = ReportDate.Year.ToString();

            string newFileName = "PromoCodeReport_" + dy + "" + mn + "" + yy + ".csv";  //"Suncoast_Fresh_Manifest_" + dy + "" + mn + "" + yy + ".csv";
            string filePath = Common.PDFPath + newFileName;   

            StringBuilder sb = new StringBuilder();
            string dataH = string.Empty;
            dataH = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
            
               "ORDER NO.",
               "ORDER DATE",
               "CUSTOMER NAME",
               "EMAIL",
               "TOTAL ORIGINAL ORDER VALUE",
               "PROMO CODE VALUE",
               "FINAL ORDER VALUE",
               "PROMO CODE USED"             
                );
            sb.Append(dataH);
            sb.AppendLine();

            string data = string.Empty;
            for (int i = 0; i <= li.Count - 1; i++)
            {
                string orderid = string.Format("{0}", li[i].OrderNo.ToString("D4"));
                data = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",         
                "SCF-HD-" + orderid,
                li[i].OrderDate.ToString("dd/MM/yyy"),
                li[i].CustomerName,
                li[i].Email,
                "$" + li[i].TotalOriginalOrderValue,
                "$" + li[i].PromoCodeValue,
                "$" + li[i].FinalOrderValue,
                li[i].PromoCodeused
                 );
                sb.Append(data);
                sb.AppendLine();
            }
            string dataF = string.Empty;
            dataF = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                "",
                "",
                "",
                "",
               "$" + li.Sum(x => x.TotalOriginalOrderValue),
               "$" + li.Sum(x => x.PromoCodeValue),
               "$" + li.Sum(x => x.FinalOrderValue),              
                ""
                 );
            sb.Append(dataF);
            sb.AppendLine();

            File.AppendAllText(filePath, sb.ToString());

       
            return filePath;
        }

            public DateTime getServerDateTime()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            conn.Open();
            using (var cmd = new SqlCommand("select getdate()", conn))
            {
                return Convert.ToDateTime(cmd.ExecuteScalar());
            }
        }

        #region Read Email Template File and Replace File Variables


        private static string ReadEmailTemplateFile(string filePath, string fileName)
        {
            string str = string.Empty;

            string path = filePath + fileName;
            //return path;
            if (System.IO.File.Exists(path))
            {
                try
                {
                    StreamReader reader = System.IO.File.OpenText(path);
                    str = reader.ReadToEnd();
                    reader.Close();
                    return str;
                }
                catch (Exception)
                {
                    throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
                }
            }
            throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
        }

        public static string ReplaceFileVariablesInTemplateFile(Hashtable hashVars, string content)
        {
            IDictionaryEnumerator enumerator = hashVars.GetEnumerator();
            while (enumerator.MoveNext())
            {
                content = content.Replace(enumerator.Key.ToString(), enumerator.Value.ToString());
            }
            return content;
        }
        #endregion
        #region Create log for the actions.

        public void log(string ex, string type)
        {
            if (System.IO.File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (System.IO.Stream str = System.IO.File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.
    }


    public class ItemRow
    {
        public DateTime OrderDate { get; set; }
        public Int64 CartID { get; set; }
        public string CustomerName { get; set; }
        public double Quantity { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
        public double Total { get; set; }

    }
    public class ItemRowPromoCode
    {
      
        public Int64 OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public double TotalOriginalOrderValue { get; set; }
        public double PromoCodeValue { get; set; }
        public double FinalOrderValue { get; set; }
        public string PromoCodeused { get; set; }
      

    }
}
