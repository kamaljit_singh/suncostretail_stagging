﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuncoastFresh
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                log("Sync process started.", "Info");

                //ReadFileAndInsertToDB(ConfigurationManager.AppSettings["Customers"].ToString(), ConfigurationManager.AppSettings["SheetNameCustomer"].ToString());
                //ReadFileAndInsertToDB(ConfigurationManager.AppSettings["Products"].ToString(), ConfigurationManager.AppSettings["SheetNameProducts"].ToString());
                ReadFileAndInsertToDB(ConfigurationManager.AppSettings["Orders"].ToString(), ConfigurationManager.AppSettings["SheetNameOrders"].ToString());

                //ReadFileAndInsertToDB(ConfigurationManager.AppSettings["Postcodes"].ToString(), ConfigurationManager.AppSettings["SheetNamePostcodes"].ToString());
                //runSyncProcedures();

                log("Sync process completed.", "Info");
            }
            catch (Exception ex)
            {
                log(ex.Message, "Error");
            }
        }

        public static void ReadFileAndInsertToDB(string fileName, string sheetName = "")
        {
            try
            {
                DataTable dt = GetDataTableFromExcel(ConfigurationManager.AppSettings["FilePath"] + fileName, sheetName);

                if (dt != null)
                {
                    if (fileName.ToLower() == "orders.xlsx")
                    {
                        dt.Columns.Add("Latitude");
                        dt.Columns.Add("Longitude");
                        string orderNo = "";
                        string lat = "";
                        string lng = "";

                        int hitCnt = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            try
                            {
                                if (orderNo == "" || orderNo != dr["Order Number"].ToString())
                                {
                                    var address = dr["Address 1&2 (Billing)"].ToString() + ", " + dr["City (Billing)"].ToString() + ", " + dr["State Code (Billing)"].ToString() + ", " + dr["Postcode (Billing)"].ToString();

                                    string url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=AIzaSyD1SMGHFaBfV9bE2XiE7IpykbWJ2UmZEIE";
                                    HttpClient client = new HttpClient();

                                    var response = client.GetAsync(url).Result;

                                    if (response.IsSuccessStatusCode)
                                    {
                                        var result = JsonConvert.DeserializeObject<GeoLocation>(response.Content.ReadAsStringAsync().Result);
                                        if (result.status != "ZERO_RESULTS" && result.status != "REQUEST_DENIED")
                                        {
                                            lat = result.results[0].geometry.location.lat.ToString();
                                            lng = result.results[0].geometry.location.lng.ToString();
                                        }
                                        else
                                        {
                                            lat = "";
                                            lng = "";
                                        }
                                    }

                                    hitCnt++;
                                }

                                dr["Latitude"] = lat;
                                dr["Longitude"] = lng;

                                orderNo = dr["Order Number"].ToString();
                            }
                            catch (Exception ex)
                            {
                                log(ex.Message, "Error In getting Lat Lng. Order: " + dr["Order Number"]);
                                continue;
                            }
                        }
                    }
                    InsertDataIntoSQLServerUsingSQLBulkCopy(dt, fileName);

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["ArchiveFile"]))
                    {
                        Deletefiles(fileName);
                    }
                }
                else
                {
                    log("Failed to read the specified file", "Info");
                }
            }
            catch (Exception ex)
            {
                log(ex.Message, "Error");
            }
        }

        #region Get data from excel file.

        public static DataTable GetDataTableFromExcel(string filepath, string sheetName = "")
        {
            try
            {
                if (sheetName == "")
                {
                    sheetName = "sheet1";
                }
                string path = System.IO.Path.GetFullPath(filepath);
                // +TableName;
                OleDbConnection oledbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                path + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                oledbConn.Open();
                string commandTxt = "SELECT * FROM [" + sheetName + "$]";
                DataTable dt = new DataTable();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                cmd.Connection = oledbConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = commandTxt;
                cmd.CommandTimeout = 0;
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(dt);
                oledbConn.Close();

                return dt;
            }
            catch (Exception ex)
            {
                log("Exception in GetDataTableFromExcel :: " + ex.Message, "Error");
                return null;
            }
        }

        #endregion Get data from excel file.

        #region Bulk upload the data into database.

        private static void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData, string TableName)
        {
            try
            {
                //TableName = Path.GetFileNameWithoutExtension(TableName);
                if (String.Compare(TableName, ConfigurationManager.AppSettings["Customers"].ToString(), true) == 0)
                {
                    TableName = "ImportCustomers";
                }
                else if (string.Compare(TableName, ConfigurationManager.AppSettings["Products"].ToString(), true) == 0)
                {
                    TableName = "ImportProducts2";
                }
                else if (string.Compare(TableName, ConfigurationManager.AppSettings["Orders"].ToString(), true) == 0)
                {
                    TableName = "ImportOrders";
                }
                else if (string.Compare(TableName, ConfigurationManager.AppSettings["Postcodes"].ToString(), true) == 0)
                {
                    TableName = "ImportPostcodesMaster";
                }

                if (TableName.Length > 0)
                {
                    using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString()))
                    {
                        dbConnection.Open();
                        SqlCommand cmd = new SqlCommand("Truncate Table " + TableName + ";", dbConnection);
                        cmd.ExecuteNonQuery();
                        using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                        {
                            s.BulkCopyTimeout = 0;
                            s.DestinationTableName = TableName;
                            s.WriteToServer(csvFileData);
                        }
                        log("Bulk inserted " + TableName, "Info");
                    }
                }
            }
            catch (Exception ex)
            {
                log("Bulk insert failed for " + TableName, "Error: " + ex.Message);
                throw;
            }
        }

        #endregion Bulk upload the data into database.

        #region Finally run the sync procedure to add the data to the main tables.

        private static void runSyncProcedures()
        {
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString()))
                {
                    dbConnection.Open();
                    SqlCommand cmd = new SqlCommand("sp_SyncData", dbConnection);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                log("Error in runSyncProcedures. Message :" + ex.Message, "Error");
            }
        }

        #endregion Finally run the sync procedure to add the data to the main tables.

        #region Rename and move file to Archived

        public static void Deletefiles(string fileName)
        {
            try
            {
                string date = DateTime.Now.ToShortDateString().Replace("/", "");
                string newname = Guid.NewGuid().ToString() + "_" + Path.GetFileNameWithoutExtension(fileName) + "_" + date + Path.GetExtension(fileName);

                string Source = ConfigurationManager.AppSettings["FilePath"] + fileName;
                string Destination = ConfigurationManager.AppSettings["ArchivePath"] + newname;
                System.IO.File.Move(Source, Destination);
            }
            catch (Exception ex)
            {
                log("File not moved :: " + ex.Message, "Error");
            }
        }

        #endregion Rename and move file to Archived

        #region Create log for the actions.

        private static void log(string ex, string type)
        {
            if (File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (Stream str = File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.
    }
}