﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarcodeLib;
using CommonFunctions;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;

namespace SendInvoices
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            GetOrdersToInvoice();
        }

        #region Get the Orders to be invoiced

        public static void GetOrdersToInvoice()
        {
            try
            {
                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("sp_Get_Update_OrdersToBeInvoiced", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    try
                    {
                        ds = new DataSet();

                        da = new SqlDataAdapter("sp_Get_Update_OrdersToBeInvoiced", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        da.SelectCommand.Parameters.AddWithValue("@cartID", Convert.ToInt64(dr["CartID"]));
                        da.SelectCommand.Parameters.AddWithValue("@update", false);
                        da.SelectCommand.Parameters.AddWithValue("@invoiceFile", "");

                        da.Fill(ds);

                        List<OrderBO> li = new List<OrderBO>();
                        li = HelperClass.ConvertDataTable<OrderBO>(ds.Tables[0]);

                        if (li.Count > 0)
                        {
                            ///GenerateInvoice(li, Convert.ToInt64(dr["CartID"]));
                            GeneratePDF(li);
                        }
                    }
                    catch (Exception ex)
                    {
                        log("Error fetching data for cartID : " + dr["CartID"] + ", Error:: " + ex.Message, "Error");
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get the Orders to be invoiced

        #region Generate Pdf

        public static void GeneratePDF(List<OrderBO> orData)
        {
            try
            {
                string invoiceNo = "SCF-HD-" + orData[0].CartID.ToString();

                var Pdf = new Document();
                Pdf.DefaultPageSetup.PageFormat = PageFormat.A4;
                Pdf.DefaultPageSetup.Orientation = Orientation.Portrait;
                Pdf.DefaultPageSetup.TopMargin = 5;
                Pdf.DefaultPageSetup.LeftMargin = 15;
                Pdf.DefaultPageSetup.RightMargin = 15;
                Pdf.DefaultPageSetup.BottomMargin = 5;
                Pdf.DefaultPageSetup.DifferentFirstPageHeaderFooter = true;

                Pdf.AddSection();

                #region Styles

                MigraDoc.DocumentObjectModel.Style style = Pdf.Styles["Normal"];
                style.Font.Name = "Calibri";

                style = Pdf.Styles.AddStyle("H1-20", "Normal");
                style.Font.Size = 20;
                style.Font.Bold = true;
                style.Font.Color = HelperClass.TextColorFromHtml("#671f75");

                style = Pdf.Styles.AddStyle("H2-8", "Normal");
                style.Font.Size = 8;

                style = Pdf.Styles.AddStyle("H2-8-Blue", "H2-8");
                style.ParagraphFormat.Font.Color = Colors.Blue;

                style = Pdf.Styles.AddStyle("H2-8B", "H2-8");
                style.Font.Bold = true;

                style = Pdf.Styles.AddStyle("H2-9", "Normal");
                style.Font.Size = 9;

                style = Pdf.Styles.AddStyle("H2-9-Grey", "H2-9");
                style.Font.Color = Colors.Gray;

                style = Pdf.Styles.AddStyle("H2-9B", "H2-9");
                style.Font.Bold = true;

                style = Pdf.Styles.AddStyle("H2-9B-Inverse", "H2-9B");
                style.ParagraphFormat.Font.Color = Colors.White;

                style = Pdf.Styles.AddStyle("H2-9B-Color", "H2-9B");
                style.Font.Color = HelperClass.TextColorFromHtml("#000");

                style = Pdf.Styles.AddStyle("H2-10", "Normal");
                style.Font.Size = 10;

                style = Pdf.Styles.AddStyle("H2-10B", "H2-10");
                style.Font.Bold = true;

                style = Pdf.Styles.AddStyle("H2-10B-Color", "H2-10B");
                style.Font.Color = HelperClass.TextColorFromHtml("#671f75");

                #endregion Styles

                #region Header section for the logo

                HeaderFooter header = Pdf.LastSection.Headers.FirstPage;
                double thirdWidth = Pdf.PageWidth() / 3;
                double halfWidth = Pdf.PageWidth() / 2;

                Table table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Center, Pdf.PageWidth());

                Row row = table.AddRow();
                Image image = row.Cells[0].AddImage(@ConfigurationManager.AppSettings["LogoPath"].ToString() + "headerInv.jpg");
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                image.Height = 100;
                image.Width = Pdf.PageWidth();
                image.RelativeVertical = RelativeVertical.Line;
                image.RelativeHorizontal = RelativeHorizontal.Margin;
                image.Top = ShapePosition.Top;

                row = table.AddRow();
                row.Cells[0].Add(HelperClass.SpacerPara);

                table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Center, Pdf.PageWidth());
                row = table.AddRow();
                TextFrame frame = row.Cells[0].AddTextFrame();
                frame.Height = 36;
                Table subTable = frame.AddTable();
                subTable.AddColumn(halfWidth);
                subTable.AddColumn(halfWidth);

                row = subTable.AddRow();
                //row.TopPadding = 2;
                //row.BottomPadding = 2;
                //row.Height = 10;
                Paragraph para = row.Cells[0].AddParagraph("TAX INVOICE " + invoiceNo, ParagraphAlignment.Center);
                para.Format.Font.Size = 22;
                para.Format.Shading.Color = Colors.Black;
                para.Format.Font.Color = Colors.White;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Top;

                //row.Cells[0].Shading.Color = Colors.Black;
                //row.Cells[0].Format.Font.Color = Colors.White;

                frame = row.Cells[1].AddTextFrame();
                frame.MarginLeft = 15;
                subTable = frame.AddTable();
                subTable.Borders.Top = HelperClass.BorderLineThin;
                subTable.Borders.Bottom = HelperClass.BorderLineThin;
                subTable.Borders.Right = HelperClass.BorderLineThin;
                subTable.Borders.Left = HelperClass.BorderLineThin;
                subTable.AddColumn((halfWidth - 10) / 3);
                subTable.AddColumn((halfWidth - 10) / 3);
                subTable.AddColumn((halfWidth - 10) / 3);

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("Invoice Date", ParagraphAlignment.Center).Format.Font.Bold = true;
                row.Cells[0].Borders.Bottom.Visible = false;

                row.Cells[1].AddParagraph("Account Code", ParagraphAlignment.Center).Format.Font.Bold = true;
                row.Cells[1].Borders.Bottom.Visible = false;

                row.Cells[2].AddParagraph("Page", ParagraphAlignment.Center).Format.Font.Bold = true;
                row.Cells[2].Borders.Bottom.Visible = false;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph(orData[0].OrderDate ?? "", ParagraphAlignment.Center);
                row.Cells[0].Borders.Top.Visible = false;

                row.Cells[1].AddParagraph("RETAIL", ParagraphAlignment.Center);
                row.Cells[1].Borders.Top.Visible = false;

                var parag = row.Cells[2].AddParagraph();
                parag.Format.Alignment = ParagraphAlignment.Center;
                parag.Style = "H2-8";
                parag.AddText("Page ");
                parag.AddPageField();
                parag.AddText(" of ");
                parag.AddNumPagesField();
                row.Cells[2].Borders.Top.Visible = false;
                // Start the table for invoice details
                table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Left, halfWidth);
                table.AddColumn(ParagraphAlignment.Left, halfWidth);

                #region Invoice details section

                row = table.AddRow();
                frame = row.Cells[0].AddTextFrame();
                var frame2 = row.Cells[1].AddTextFrame();

                subTable = frame.AddTable();
                subTable.AddColumn(80);
                subTable.AddColumn(halfWidth - 80);

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("Invoice To", ParagraphAlignment.Left).Format.Font.Size = 12;
                row.Cells[0].Borders.Top = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[0].MergeRight = 1;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].CustomerName, ParagraphAlignment.Left).Format.Font.Size = 12; ;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].Address1, ParagraphAlignment.Left).Format.Font.Size = 12;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].Suburb + ", " + orData[0].Postcode, ParagraphAlignment.Left).Format.Font.Size = 12; ;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].State ?? "", ParagraphAlignment.Left).Format.Font.Size = 12; ;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.TopPadding = 3;
                row.BottomPadding = 2;
                row.Cells[0].AddParagraph("");
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Bottom = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Bottom = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;

                #endregion Invoice details section

                #region Delivery Instructions section

                string comments = (orData[0].Comment ?? "") + (orData[0].CommentLine == "" ? "" : " || " + orData[0].CommentLine);

                if (comments.Length > 140)
                {
                    comments = comments.Substring(0, 140) + ".....";
                }

                row = table.AddRow();
                // frame = row.Cells[1].AddTextFrame();
                frame2.MarginLeft = 15;
                double subWidth = halfWidth - 10;
                subTable = frame2.AddTable();

                subTable.AddColumn(ParagraphAlignment.Right, subWidth - 80);
                subTable.AddColumn(ParagraphAlignment.Right, 80);

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("Delivery Instructions", ParagraphAlignment.Left).Format.Font.Size = 12;
                row.Cells[0].Borders.Top = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].AddParagraph("", ParagraphAlignment.Right).Format.Font.Size = 12;
                row.Cells[1].Borders.Top = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                //row.Cells[0].MergeRight = 1;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph(comments, ParagraphAlignment.Justify).Format.Font.Size = 8; ;
                row.Cells[0].MergeRight = 1;
                row.Cells[0].MergeDown = 1;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Height = 73;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Bottom = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Bottom = HelperClass.BorderLineThin;

                #endregion Delivery Instructions section

                table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Left, Pdf.PageWidth() + 5);

                row = table.AddRow();

                row.Cells[0].AddParagraph("");

                row = table.AddRow();
                row.Cells[0].AddParagraph("Reference: " + orData[0].MCustomerName + ", " + orData[0].MAddress1 + ", " + orData[0].MSuburb + ", " + orData[0].Postcode + ", " + orData[0].MState, ParagraphAlignment.Left);
                //row.Cells[0].MergeRight = 1;
                row.Borders.Visible = true;
                row.Borders.Width = 1;

                #endregion Header section for the logo

                #region Items table

                var section2 = Pdf.LastSection;

                section2.PageSetup.TopMargin = 300;
                var mainWidth = Pdf.PageWidth() - 50;

                var tableItems = section2.AddTable();
                tableItems.Borders.Visible = true;
                tableItems.Borders.Width = 1;
                tableItems.TopPadding = 5;
                tableItems.BottomPadding = 5;

                tableItems.AddColumn(mainWidth * .10); // 20 % of the page width and so on
                tableItems.AddColumn(mainWidth * .41);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);

                row = tableItems.AddRow();
                row.HeadingFormat = true;

                row.Shading.Color = HelperClass.TextColorFromHtml("#c7cbcf");
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph("Code", ParagraphAlignment.Left, "H2-9B");
                row.Cells[1].AddParagraph("Description", ParagraphAlignment.Left, "H2-9B");
                row.Cells[2].AddParagraph("Order", ParagraphAlignment.Center, "H2-9B");
                row.Cells[3].AddParagraph("Supply", ParagraphAlignment.Center, "H2-9B");
                row.Cells[4].AddParagraph("Unit", ParagraphAlignment.Center, "H2-9B");
                row.Cells[5].AddParagraph("Unit Price", ParagraphAlignment.Right, "H2-9B");
                row.Cells[6].AddParagraph("Total", ParagraphAlignment.Right, "H2-9B");

                double? sum = 0;
                double? gst = 0;
                double? promo = 0;
                double? freight = 0;
                foreach (var item in orData)
                {
                    double total = 0;

                    total = item.Quantity * item.Price;

                    var price = item.Price;

                    row = tableItems.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].AddParagraph(item.ProductCode, ParagraphAlignment.Left, "H2-9");
                    row.Cells[1].AddParagraph((item.IsGST ? "(*)" : "") + item.ProductName, ParagraphAlignment.Left, "H2-9");
                    if (item.ProComment.Trim().Length > 0)
                    {
                        row.Cells[1].AddParagraph(item.ProComment, ParagraphAlignment.Left, "H2-9").Format.Font.Color = Colors.Red;
                    }
                    row.Cells[2].AddParagraph(item.Quantity.ToString(), ParagraphAlignment.Center, "H2-9");
                    row.Cells[3].AddParagraph(item.Quantity.ToString(), ParagraphAlignment.Center, "H2-9");
                    row.Cells[4].AddParagraph(item.UOMDesc, ParagraphAlignment.Center, "H2-9");
                    row.Cells[5].AddParagraph(string.Format("{0:#,0.00}", total), ParagraphAlignment.Right, "H2-9");
                    row.Cells[6].AddParagraph(string.Format("{0:#,0.00}", total), ParagraphAlignment.Right, "H2-9");

                    sum = sum + total;
                    if (item.IsGST)
                    {
                        gst = gst + (total * .1);
                    }
                }

                if (orData[0].HasFreightCharges)
                {
                    row = tableItems.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].AddParagraph("CHARGE", ParagraphAlignment.Left, "H2-9");
                    row.Cells[1].AddParagraph("Delivery Charges", ParagraphAlignment.Left, "H2-9");
                    row.Cells[2].AddParagraph("1", ParagraphAlignment.Center, "H2-9");
                    row.Cells[3].AddParagraph("1", ParagraphAlignment.Center, "H2-9");
                    row.Cells[4].AddParagraph("Fee", ParagraphAlignment.Center, "H2-9");
                    row.Cells[5].AddParagraph(string.Format("{0:#,0.00}", orData[0].FreightAmount), ParagraphAlignment.Right, "H2-9");
                    row.Cells[6].AddParagraph(string.Format("{0:#,0.00}", orData[0].FreightAmount), ParagraphAlignment.Right, "H2-9");
                }

                #endregion Items table

                #region Footer section

                HeaderFooter footer = section2.Footers.FirstPage;
                //section2.PageSetup.DifferentFirstPageHeaderFooter = true;

                var tableFooter = footer.AddTable();
                tableFooter.AddColumn(ParagraphAlignment.Left, Pdf.PageWidth() / 2);
                tableFooter.AddColumn(ParagraphAlignment.Right, Pdf.PageWidth() / 2);

                row = tableFooter.AddRow();
                row.Cells[0].MergeRight = 1;
                var pFooter = row.Cells[0].AddParagraph("");
                //pFooter.AddFormattedText("Notes: ").Font.Bold = true;
                //pFooter.AddText(" CUSTOMERS PLEASE NOTE: Please note cut off for next day deliveries is midnight.");
                row.BottomPadding = 1;
                //row.Cells[0].Borders.Top = HelperClass.BorderLineThin;
                //row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                //row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Bottom = HelperClass.BorderLineThin;

                row = tableFooter.AddRow();
                row.Cells[0].Add(HelperClass.SpacerPara);

                row = tableFooter.AddRow();
                TextFrame frame3 = row.Cells[0].AddTextFrame();

                Table sub = frame3.AddTable();
                sub.AddColumn((mainWidth / 2) / 2);
                sub.AddColumn((mainWidth / 2) / 2);

                var row1 = sub.AddRow();
                row1.Cells[0].MergeRight = 1;
                row1.Cells[0].AddParagraph("No claim will be recognized if not advised within 24 hours of receipt of goods", ParagraphAlignment.Left, "H2-8");

                row1 = sub.AddRow();
                row1.Cells[0].MergeRight = 1;
                row1.Cells[0].AddParagraph("Delivery fee applies to all orders under $" + string.Format("{0:#,0.00}", orData[0].MinOrderValue), ParagraphAlignment.Left, "H2-8");

                row1 = sub.AddRow();
                row1.Cells[0].MergeRight = 1;
                row1.Cells[0].AddParagraph("(*) INDICATES TAXABLE ITEM", ParagraphAlignment.Left, "H2-8");

                //row1 = sub.AddRow();
                //row1.Cells[0].MergeRight = 1;
                //row1.Cells[0].Add(HelperClass.SpacerPara);

                //row1 = sub.AddRow();
                //row1.Cells[0].AddParagraph("PLEASE DEPOSIT PAYMENTS DIRECTLY INTO:", ParagraphAlignment.Left, "H2-9B");
                //row1.Cells[0].MergeRight = 1;
                //row1.BottomPadding = 1;

                //row1 = sub.AddRow();
                //row1.Cells[0].AddParagraph("Bank:  COMMONWEALTH BANK ACC", ParagraphAlignment.Left, "H2-9B");
                //row1.Cells[0].MergeRight = 1;
                //row1.BottomPadding = 1;

                //row1 = sub.AddRow();
                //row1.Cells[0].AddParagraph("Branch:  064-153", ParagraphAlignment.Left, "H2-9B");
                //row1.Cells[0].MergeRight = 1;
                //row1.BottomPadding = 1;

                //row1 = sub.AddRow();
                //row1.Cells[0].AddParagraph("Account:  10144629  Ref: MYAC", ParagraphAlignment.Left, "H2-9B");
                //row1.Cells[0].MergeRight = 1;
                //row1.BottomPadding = 1;

                var frame4 = row.Cells[1].AddTextFrame();
                frame4.MarginLeft = 23;
                sub = frame4.AddTable();
                sub.AddColumn((Pdf.PageWidth() / 2) / 3);
                sub.AddColumn((Pdf.PageWidth() / 2) / 3);
                sub.AddColumn((Pdf.PageWidth() / 2) / 4);

                sub.Borders.Top = HelperClass.BorderLineThin;
                sub.Borders.Right = HelperClass.BorderLineThin;
                sub.Borders.Bottom = HelperClass.BorderLineThin;
                sub.Borders.Left = HelperClass.BorderLineThin;

                var row2 = sub.AddRow();
                var pTerms = row2.Cells[0].AddParagraph("Terms: COD");
                pTerms.Format.Font.Bold = true;
                pTerms.Format.Font.Size = 14;
                row2.Cells[0].MergeDown = 1;
                row2.Cells[1].AddParagraph("GST", ParagraphAlignment.Right, "H2-10B");
                row2.Cells[2].AddParagraph("$" + string.Format("{0:#,0.00}", 0), ParagraphAlignment.Right, "H2-10B");
                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                row2 = sub.AddRow();
                row2.Cells[1].AddParagraph("Promo Code", ParagraphAlignment.Right, "H2-10B");
                if (orData[0].IsCouponApplied)
                {
                    promo = orData[0].CouponAmount;
                    row2.Cells[2].AddParagraph("-$" + string.Format("{0:#,0.00}", orData[0].CouponAmount), ParagraphAlignment.Right, "H2-10B");
                }
                else
                {
                    row2.Cells[2].AddParagraph("$0.00", ParagraphAlignment.Right, "H2-10B");
                }

                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                row2 = sub.AddRow();
                row2.Cells[1].AddParagraph("Delivery Charges", ParagraphAlignment.Right, "H2-10B");
                if (orData[0].HasFreightCharges)
                {
                    freight = orData[0].FreightAmount;
                    row2.Cells[2].AddParagraph("$" + string.Format("{0:#,0.00}", orData[0].FreightAmount), ParagraphAlignment.Right, "H2-10B");
                }
                else
                {
                    row2.Cells[2].AddParagraph("$0.00", ParagraphAlignment.Right, "H2-10B");
                }

                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                row2 = sub.AddRow();
                row2.Cells[1].AddParagraph("Invoice Total", ParagraphAlignment.Right, "H2-10B");
                row2.Cells[2].AddParagraph("$" + string.Format("{0:#,0.00}", ((sum + freight + gst) - promo)), ParagraphAlignment.Right, "H2-10B");
                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                #endregion Footer section

                //string unique = invoiceNo;
                string pdfPath = @ConfigurationManager.AppSettings["InvoicePath"].ToString() + invoiceNo + ".pdf";

                PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfFontEmbedding.Always);
                renderer.Document = Pdf;
                renderer.RenderDocument();
                renderer.PdfDocument.Save(pdfPath);
                var hTable = new Hashtable();
                hTable["@CName"] = orData[0].CustomerName;
                hTable["@path"] = HelperClass.LogoPath;
                hTable["@Company"] = HelperClass.Company;

                string msg = SendMail.SendEMail(orData[0].Email, HelperClass.TO_EMAILID, HelperClass.BCC, "Invoice for Order " + invoiceNo, HelperClass.Logpath, "Invoicemain.htm", hTable, pdfPath, HelperClass.FROM_EMAILID);

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("sp_Get_Update_OrdersToBeInvoiced", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                da.SelectCommand.Parameters.AddWithValue("@cartID", orData[0].CartID);
                da.SelectCommand.Parameters.AddWithValue("@update", true);
                da.SelectCommand.Parameters.AddWithValue("@invoiceFile", invoiceNo.Trim());

                var ds = new DataSet();
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                log("Error generating PDF: " + ex.Message, "Error");
            }
        }

        #endregion Generate Pdf

        #region Maintain log for all the actions

        private static void log(string ex, string type)
        {
            if (File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (Stream str = File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Maintain log for all the actions
    }
}