﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;

namespace SendInvoices
{
    public static class HelperClass
    {
        public static string LogoPath = "https://w59ftksndg0kjrf8tjk.saavi.com.au/Rf8tjk/";
        public static string FROM_EMAILID = Convert.ToString(ConfigurationManager.AppSettings["FROM_EMAILID"]);
        public static string TO_EMAILID = Convert.ToString(ConfigurationManager.AppSettings["TO_EMAILID"]);
        public static string BCC = Convert.ToString(ConfigurationManager.AppSettings["BCC"]);
        public static string Logpath = Convert.ToString(ConfigurationManager.AppSettings["Logpath"]);
        public static string Company = Convert.ToString(ConfigurationManager.AppSettings["Company"]);
        public static string InvoiceTitle = Convert.ToString(ConfigurationManager.AppSettings["InvoiceTitle"]);
        public static int InvoiceLogoHeight = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceLogoHeight"]);
        public static int InvoiceLogoWidth = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceLogoWidth"]);

        #region MigraDoc Helpers

        public static string FormatNumber<T>(T number, int maxDecimals = 4)
        {
            return Regex.Replace(String.Format("{0:n" + maxDecimals + "}", number),
                                 @"[" + System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator + "]?0+$", "");
        }

        public static double PageWidth(this Section section)
        {
            return (PageWidth(section.Document));
        }

        public static double PageWidth(this Document document)
        {
            Unit width, height;

            PageSetup.GetPageSize(document.DefaultPageSetup.PageFormat, out width, out height);
            if (document.DefaultPageSetup.Orientation == Orientation.Landscape)
                Swap<Unit>(ref width, ref height);

            return (width.Point - document.DefaultPageSetup.LeftMargin.Point - document.DefaultPageSetup.RightMargin.Point);
        }

        public static Column AddColumn(this Table table, ParagraphAlignment align, Unit unit = new Unit())
        {
            Column column = table.AddColumn();
            column.Width = unit;
            column.Format.Alignment = align;
            return column;
        }

        public static Paragraph AddParagraph(this TextFrame frame, string text, ParagraphAlignment align, string style = null)
        {
            return frame.AddParagraph().AddText(text, align, style);
        }

        public static Paragraph AddParagraph(this Cell cell, string text, ParagraphAlignment align, string style = null)
        {
            return cell.AddParagraph().AddText(text, align, style);
        }

        private static Paragraph AddText(this Paragraph paragraph, string text, ParagraphAlignment align, string style = null)
        {
            paragraph.Format.Alignment = align;
            if (style == null)
                paragraph.AddText(text);
            else
                paragraph.AddFormattedText(text, style);
            return paragraph;
        }

        public static string ToCurrency(this decimal number)
        {
            return ToCurrency(number, string.Empty);
        }

        public static string ToCurrency(this decimal number, string symbol)
        {
            return string.Format("{0}{1:N2}", symbol, number);
        }

        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T tmp = lhs; lhs = rhs; rhs = tmp;
        }

        public static Color TextColorFromHtml(string hex)
        {
            if (hex == null)
                return Colors.Black;
            else
                return new Color((uint)System.Drawing.ColorTranslator.FromHtml(hex).ToArgb());
        }

        public static Border BorderLine
        {
            get
            {
                Border bottomLine = new Border();
                bottomLine.Width = new Unit(2.5);
                bottomLine.Color = HelperClass.TextColorFromHtml("#000000");
                return bottomLine;
            }
        }

        public static Border BorderLineThin
        {
            get
            {
                Border bottomLine = new Border();
                bottomLine.Width = new Unit(1);
                bottomLine.Color = HelperClass.TextColorFromHtml("#000000");
                return bottomLine;
            }
        }

        public static Paragraph SpacerPara
        {
            get
            {
                Paragraph paragraph = new Paragraph();
                paragraph.Format.LineSpacingRule = MigraDoc.DocumentObjectModel.LineSpacingRule.Exactly;
                paragraph.Format.LineSpacing = MigraDoc.DocumentObjectModel.Unit.FromMillimeter(0.0);
                return paragraph;
            }
        }

        #endregion MigraDoc Helpers

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }

    public class OrderBO
    {
        public long CartID { get; set; }
        public string OrderNo { get; set; }
        public string InvoiceNo { get; set; }
        public string OrderDate { get; set; }
        public string CreatedDate { get; set; }
        public string CustomerName { get; set; }
        public string AlphaCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string UOMDesc { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public string CustType { get; set; }
        public bool HasFreightCharges { get; set; }
        public double? FreightAmount { get; set; }
        public bool IsCouponApplied { get; set; }
        public double? CouponAmount { get; set; }
        public bool IsGST { get; set; }
        public string ProComment { get; set; }
        public string Comment { get; set; }
        public string CommentLine { get; set; }
        public string MCustomerName { get; set; }
        public string MAddress1 { get; set; }
        public string MSuburb { get; set; }
        public string MPostcode { get; set; }
        public string State { get; set; }
        public string MState { get; set; }
        public double MinOrderValue { get; set; }
        public string Email { get; set; }
    }
}