﻿using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace syncBoxTotalWeekEnd
{
    public class Order
    {
        public string PDFForBoxTotalSummary(DateTime ReportDate, DateTime ToDate, DateTime FromDate)
        {
            try
            {
                string content = "";

                List<ItemRow> orData = new List<ItemRow>();
                List<ItemRowWare> orDataWare = new List<ItemRowWare>();
                List<ItemRowSale> orDataSale = new List<ItemRowSale>(); //Week Prior All
                List<ItemRowSale> orDataPriorBox = new List<ItemRowSale>();//Week Prior Box
                List<ItemRowSale> orDataPriorAddOn = new List<ItemRowSale>();//Week Prior Addon

                List<ItemRowMargin> orDataMarginBox = new List<ItemRowMargin>();//margin Box
                List<ItemRowMargin> orDataMargionAddOn = new List<ItemRowMargin>();//margin Addon

                DataSet ds = new DataSet();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

                SqlDataAdapter da = new SqlDataAdapter("spBoxTotal", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@ToDate", ToDate);
                da.SelectCommand.Parameters.AddWithValue("@FromDate", FromDate);

                da.Fill(ds);

                DataTable dt = ds.Tables[0];

                orData = Common.ConvertDataTable<ItemRow>(ds.Tables[0]);
                orDataWare = Common.ConvertDataTable<ItemRowWare>(ds.Tables[1]);
                orDataSale = Common.ConvertDataTable<ItemRowSale>(ds.Tables[2]);
                orDataPriorBox = Common.ConvertDataTable<ItemRowSale>(ds.Tables[3]);//Box
                orDataPriorAddOn = Common.ConvertDataTable<ItemRowSale>(ds.Tables[4]);//AddOn
                orDataMarginBox = Common.ConvertDataTable<ItemRowMargin>(ds.Tables[5]);//Box
                orDataMargionAddOn = Common.ConvertDataTable<ItemRowMargin>(ds.Tables[6]);//AddOn

                if (orData.Count == 0)
                {
                    return "";
                }

                Hashtable htbl = new Hashtable();
                string items = "";
                double TOTALQty = 0;


                List<string> list = new List<string>();

                foreach (ItemRow item in orData.Where(x => x.UOM.ToUpper() == "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                {
                    list.Add(item.ProductCode);
                }
                List<string> list2 = new List<string>();
                foreach (ItemRow item in orData.Where(x => x.UOM.ToUpper() != "BOX" && (x.ProductCode != "2229" && x.ProductCode != "2220")))
                {
                    list2.Add(item.ProductCode);
                }

                string[] productcode = list.ToArray();//{ "1520", "1519", "1523", "1521", "2072", "1522", "1524","2003", "", "1950", "1949", "1818","2000","2001" };
                string[] productcode2 = list2.ToArray();


                htbl["@image"] = "<img style = 'width: 131%;' src='" + Common.LogoPath + "'>";
                htbl["@Items"] = items;
                htbl["@Date"] = ToDate.ToString("dd/MM/yyy");
                htbl["@RequriedDate"] = FromDate.ToString("dd/MM/yyy") + " to " + ToDate.ToString("dd/MM/yyy"); ;



                string ItemsWarehouse = "";
                double priceBRIS = 0;
                double priceGC = 0;
                double priceTWB = 0;
                double priceCOAST = 0;
                double priceBYRON = 0;


                double BRIS = 0;
                double GC = 0;
                double TWB = 0;
                double COAST = 0;
                double BYRON = 0;


                double totalpriceBRIS = 0;
                double totalpriceGC = 0;
                double totalpriceTWB = 0;
                double totalpriceCOAST = 0;
                double totalpriceBYRON = 0;


                double totalBRIS = 0;
                double totalGC = 0;
                double totalTWB = 0;
                double totalCOAST = 0;
                double totalBYRON = 0;

                double totalboxallqty = 0;
                double totalboxallprice = 0;
                double totaladdonallqty = 0;
                double totaladdonallprice = 0;
                double GrandTotal = 0;

                for (int i = 0; i < productcode.Count(); i++)
                {
                    foreach (ItemRowWare item in orDataWare)
                    {
                        if (productcode[i] == item.ProductCode)
                        {
                            ItemsWarehouse += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + (double)item.BRIS + " ($" + String.Format("{0:0,0.00}", (double)item.PriceBRIS) + ")</td><td align='center'>" + (double)item.GC + " ($" + String.Format("{0:0,0.00}", (double)item.PriceGC) + ")</td><td align='center'>" + (double)item.TWB + " ($" + String.Format("{0:0,0.00}", (double)item.PriceTWB) + ")</td><td align='center'>" + (double)item.COAST + " ($" + String.Format("{0:0,0.00}", (double)item.PriceCOAST) + ")</td><td align='center'>" + (double)item.BYRON + " ($" + String.Format("{0:0,0.00}", (double)item.PriceBYRON) + ")</td></tr>";
                            priceBRIS += item.PriceBRIS;
                            priceGC += item.PriceGC;
                            priceTWB += item.PriceTWB;
                            priceCOAST += item.PriceCOAST;
                            priceBYRON += item.PriceBYRON;

                            BRIS += item.BRIS;
                            GC += item.GC;
                            TWB += item.TWB;
                            COAST += item.COAST;
                            BYRON += item.BYRON;

                        }
                    }
                }
                // ItemsWarehouse += " <tr><td>y </td><td> </td><td align='center'> </td><td align='center'> </td><td> </td><td> </td><td> </td></tr>";
                ItemsWarehouse += " <tr><td bgcolor='#FFFFFF' colspan='7'>&nbsp;</td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>TOTAL BOXES</td><td align='center'>" + BRIS + " </td><td align='center'>" + GC + " </td><td align='center'> " + TWB + " </td><td align='center'>" + COAST + " </td><td align='center'> " + BYRON + "</td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>TOTAL VALUES ($)</td><td align='center'>" + "$" + String.Format("{0:0,0.00}", priceBRIS) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", priceGC) + "</td><td align='center'> " + "$" + String.Format("{0:0,0.00}", priceTWB) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", priceCOAST) + " </td><td align='center'> " + "$" + String.Format("{0:0,0.00}", priceBYRON) + "</td></tr>";
                ItemsWarehouse += " <tr><td bgcolor='#FFFFFF' colspan='7'>&nbsp;</td></tr>";


                totalpriceBRIS += priceBRIS;
                totalpriceGC += priceGC;
                totalpriceTWB += priceTWB;
                totalpriceCOAST += priceCOAST;
                totalpriceBYRON += priceBYRON;


                totalBRIS += BRIS;
                totalGC += GC;
                totalTWB += TWB;
                totalCOAST += COAST;
                totalBYRON += BYRON;

                totalboxallqty = BRIS + GC + TWB + COAST + BYRON;
                totalboxallprice = priceBRIS + priceGC + priceTWB + priceCOAST + priceBYRON;

                priceBRIS = 0;
                priceGC = 0;
                priceTWB = 0;
                priceCOAST = 0;
                priceBYRON = 0;

                BRIS = 0;
                GC = 0;
                TWB = 0;
                COAST = 0;
                BYRON = 0;

                for (int i = 0; i < productcode2.Count(); i++)
                {
                    foreach (ItemRowWare item in orDataWare)
                    {
                        if (productcode2[i] == item.ProductCode)
                        {
                            // ItemsWarehouse += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + (double)item.BRIS + "</td><td align='center'>" + (double)item.GC + "</td><td align='center'>" + (double)item.TWB + "</td><td align='center'>" + (double)item.COAST + "</td><td align='center'>" + (double)item.BYRON + "</td></tr>";
                            // ItemsWarehouse += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + (double)item.BRIS + " $" + String.Format("{0:0,0.00}", (double)item.PriceBRIS) + "</td><td align='center'>" + (double)item.GC + " $" + String.Format("{0:0,0.00}", (double)item.PriceGC) + " </td><td align='center'>" + (double)item.TWB + " $" + String.Format("{0:0,0.00}", (double)item.PriceTWB) + "</td><td align='center'>" + (double)item.COAST + " $" + String.Format("{0:0,0.00}", (double)item.PriceCOAST) + "</td><td align='center'>" + (double)item.BYRON + " $" + String.Format("{0:0,0.00}", (double)item.PriceBYRON) + "</td></tr>";
                            ItemsWarehouse += " <tr><td>" + item.ProductCode + "</td><td>" + item.ProductName + "</td><td align='center'>" + (double)item.BRIS + " ($" + String.Format("{0:0,0.00}", (double)item.PriceBRIS) + ")</td><td align='center'>" + (double)item.GC + " ($" + String.Format("{0:0,0.00}", (double)item.PriceGC) + ")</td><td align='center'>" + (double)item.TWB + " ($" + String.Format("{0:0,0.00}", (double)item.PriceTWB) + ")</td><td align='center'>" + (double)item.COAST + " ($" + String.Format("{0:0,0.00}", (double)item.PriceCOAST) + ")</td><td align='center'>" + (double)item.BYRON + " ($" + String.Format("{0:0,0.00}", (double)item.PriceBYRON) + ")</td></tr>";

                            priceBRIS += item.PriceBRIS;
                            priceGC += item.PriceGC;
                            priceTWB += item.PriceTWB;
                            priceCOAST += item.PriceCOAST;
                            priceBYRON += item.PriceBYRON;

                            BRIS += item.BRIS;
                            GC += item.GC;
                            TWB += item.TWB;
                            COAST += item.COAST;
                            BYRON += item.BYRON;
                        }
                    }
                }


                totalpriceBRIS += priceBRIS;
                totalpriceGC += priceGC;
                totalpriceTWB += priceTWB;
                totalpriceCOAST += priceCOAST;
                totalpriceBYRON += priceBYRON;


                totalBRIS += BRIS;
                totalGC += GC;
                totalTWB += TWB;
                totalCOAST += COAST;
                totalBYRON += BYRON;



                totaladdonallqty = BRIS + GC + TWB + COAST + BYRON;
                totaladdonallprice = priceBRIS + priceGC + priceTWB + priceCOAST + priceBYRON;
                GrandTotal = totalboxallprice + totaladdonallprice;

                string BrisSale = "";
                string GCSale = "";
                string TWBSale = "";
                string COASTSale = "";
                string BYRONSale = "";

                string BrisClass = "";
                string GCClass = "";
                string TWBClass = "";
                string COASTClass = "";
                string BYRONClass = "";

                string GrandTotalWeek = "";
                string GrandTotalWeekClass = "";

                double TotalBoxPrior = 0;
                double TotalAddonPrior = 0;
                double TotalBoxPricePrior = 0;
                double TotalAddonPricePrior = 0;
                double GrandTotalPricePrior = 0;

                if (orDataSale.Count != 0)
                {
                    BrisSale = DoubleToPercentageString(CalculateChange(orDataSale[0].BRIS, totalBRIS));
                    GCSale = DoubleToPercentageString(CalculateChange(orDataSale[0].GC, totalGC));
                    TWBSale = DoubleToPercentageString(CalculateChange(orDataSale[0].TWB, totalTWB));
                    COASTSale = DoubleToPercentageString(CalculateChange(orDataSale[0].COAST, totalCOAST));
                    BYRONSale = DoubleToPercentageString(CalculateChange(orDataSale[0].BYRON, totalBYRON));

                    BrisClass = getPerClass(BrisSale);
                    GCClass = getPerClass(GCSale);
                    TWBClass = getPerClass(TWBSale);
                    COASTClass = getPerClass(COASTSale);
                    BYRONClass = getPerClass(BYRONSale);

                    GrandTotalPricePrior = orDataSale[0].PriceBRIS + orDataSale[0].PriceGC + orDataSale[0].PriceTWB + orDataSale[0].PriceCOAST + orDataSale[0].PriceBYRON;

                    GrandTotalWeek = DoubleToPercentageString(CalculateChange(GrandTotalPricePrior, GrandTotal));
                    GrandTotalWeekClass = getPerClass(GrandTotalWeek);
                }

                string TotalBoxQty = "";
                string TotalBoxPrice = "";
                string TotalAddOnQty = "";
                string TotalAddOnPrice = "";


                string TotalBoxQtyClass = "";
                string TotalBoxPriceClass = "";
                string TotalAddOnQtyClass = "";
                string TotalAddOnPriceClass = "";


                if (orDataPriorBox.Count != 0)
                {
                    TotalBoxPrior = orDataPriorBox[0].BRIS + orDataPriorBox[0].GC + orDataPriorBox[0].TWB + orDataPriorBox[0].COAST + orDataPriorBox[0].BYRON;
                    TotalBoxPricePrior = orDataPriorBox[0].PriceBRIS + orDataPriorBox[0].PriceGC + orDataPriorBox[0].PriceTWB + orDataPriorBox[0].PriceCOAST + orDataPriorBox[0].PriceBYRON;

                    TotalBoxQty = DoubleToPercentageString(CalculateChange(TotalBoxPrior, totalboxallqty));
                    TotalBoxPrice = DoubleToPercentageString(CalculateChange(TotalBoxPricePrior, totalboxallprice));

                    TotalBoxQtyClass = getPerClass(TotalBoxQty);
                    TotalBoxPriceClass = getPerClass(TotalBoxPrice);
                }

                if (orDataPriorAddOn.Count != 0)
                {

                    TotalAddonPrior = orDataPriorAddOn[0].BRIS + orDataPriorAddOn[0].GC + orDataPriorAddOn[0].TWB + orDataPriorAddOn[0].COAST + orDataPriorAddOn[0].BYRON;
                    TotalAddonPricePrior = orDataPriorAddOn[0].PriceBRIS + orDataPriorAddOn[0].PriceGC + orDataPriorAddOn[0].PriceTWB + orDataPriorAddOn[0].PriceCOAST + orDataPriorAddOn[0].PriceBYRON;

                    TotalAddOnQty = DoubleToPercentageString(CalculateChange(TotalAddonPrior, totaladdonallqty));
                    TotalAddOnPrice = DoubleToPercentageString(CalculateChange(TotalAddonPricePrior, totaladdonallprice));

                    TotalAddOnQtyClass = getPerClass(TotalAddOnQty);
                    TotalAddOnPriceClass = getPerClass(TotalAddOnPrice);
                }

                string BoxMargin = "";
                string AddOnMargin = "";
                string BoxMarginClass = "";
                string AddOnMarginClass = "";
                if (orDataMarginBox.Count != 0)
                {
                    BoxMargin = MarginString(orDataMarginBox[0].MarginPer);
                    BoxMarginClass = getPerClass(BoxMargin);
                }
                if (orDataMargionAddOn.Count != 0)
                {
                    AddOnMargin = MarginString(orDataMargionAddOn[0].MarginPer);
                    AddOnMarginClass = getPerClass(AddOnMargin);
                }

                ItemsWarehouse += " <tr><td bgcolor='#FFFFFF' colspan='7'>&nbsp;</td></tr>"; ;
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>TOTAL ADDONS</td><td align='center'>" + BRIS + " </td><td align='center'>" + GC + " </td><td align='center'> " + TWB + " </td><td align='center'>" + COAST + " </td><td align='center'> " + BYRON + "</td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>TOTAL VALUES ($)</td><td align='center'>" + "$" + String.Format("{0:0,0.00}", priceBRIS) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", priceGC) + "</td><td align='center'> " + "$" + String.Format("{0:0,0.00}", priceTWB) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", priceCOAST) + " </td><td align='center'> " + "$" + String.Format("{0:0,0.00}", priceBYRON) + "</td></tr>";
               // ItemsWarehouse += " <tr><td bgcolor='#FFFFFF' colspan='7'>&nbsp;</td></tr>";

                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>BOXES AND ADDONS TOTAL ALL</td><td align='center'>" + totalBRIS + " </td><td align='center'>" + totalGC + " </td><td align='center'> " + totalTWB + " </td><td align='center'>" + totalCOAST + " </td><td align='center'> " + totalBYRON + "</td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>BOXES AND ADDONS TOTAL ALL ($)</td><td align='center'>" + "$" + String.Format("{0:0,0.00}", totalpriceBRIS) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", totalpriceGC) + "</td><td align='center'> " + "$" + String.Format("{0:0,0.00}", totalpriceTWB) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", totalpriceCOAST) + " </td><td align='center'> " + "$" + String.Format("{0:0,0.00}", totalpriceBYRON) + "</td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>CHANGE (%)</td><td align='center' class='" + BrisClass + "'>" + BrisSale + " </td><td align='center' class='" + GCClass + "'>" + GCSale + " </td><td align='center' class='" + TWBClass + "'> " + TWBSale + " </td><td align='center' class='" + COASTClass + "'>" + COASTSale + " </td><td align='center' class='" + BYRONClass + "'>" + BYRONSale + " </td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'></td><td align='center'>TOTAL COST</td><td align='center'>MARGINS</td><td align='center'>TOTAL SALES</td><td align='center'>CHANGE (%)</td><td align='center'>WEEK PRIOR</td></tr>";
                ItemsWarehouse += "  <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='4'>TOTAL BOXES FOR LAST WEEK:(QTY)</td><td align='center'>" + totalboxallqty + " </td><td align='center' class='" + TotalBoxQtyClass + "'>" + TotalBoxQty + " </td><td align='center'>" + TotalBoxPrior + " </td></tr>";
                ItemsWarehouse += "  <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>TOTAL BOXES FOR LAST WEEK:($)</td><td  align='center'>" + "$" + String.Format("{0:0,0.00}", orDataMarginBox[0].Cost) + " </td><td  align='center'>" + "$" + String.Format("{0:0,0.00}", orDataMarginBox[0].Margin) + " </td><td  align='center'>" + "$" + String.Format("{0:0,0.00}", totalboxallprice) + " </td><td  align='center' class='" + TotalBoxPriceClass + "'>" + String.Format("{0:0,0.00}", TotalBoxPrice) + " </td><td  align='center'>" + "$" + String.Format("{0:0,0.00}", TotalBoxPricePrior) + " </td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='4'></td><td  align='center' class='" + BoxMarginClass + "'>" + BoxMargin + " </td> <td colspan='2'></td></tr>";
                ItemsWarehouse += "  <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='4'>TOTAL ADDONS FOR LAST WEEK:(QTY)</td><td  align='center'>" + totaladdonallqty + " </td><td  align='center' class='" + TotalAddOnQtyClass + "'>" + TotalAddOnQty + " </td><td  align='center'>" + TotalAddonPrior + " </td></tr>";
                ItemsWarehouse += "  <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='2'>TOTAL ADDONS FOR LAST WEEK:($)</td><td  align='center'>" + "$" + String.Format("{0:0,0.00}", orDataMargionAddOn[0].Cost) + " </td><td  align='center'>" + "$" + String.Format("{0:0,0.00}", orDataMargionAddOn[0].Margin) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", totaladdonallprice) + " </td><td align='center' class='" + TotalAddOnPriceClass + "'>" +  String.Format("{0:0,0.00}", TotalAddOnPrice) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", TotalAddonPricePrior) + " </td></tr>";
                ItemsWarehouse += " <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='4'></td><td  align='center' class='" + AddOnMarginClass + "'>" + AddOnMargin + " </td><td colspan='2'></td></tr>";
                //ItemsWarehouse += " <tr><td bgcolor='#FFFFFF' colspan='7'>&nbsp;</td></tr>";

                ItemsWarehouse += "  <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='4'>GRAND TOTAL FOR WEEK:($)</td><td align='center'>" + "$" + String.Format("{0:0,0.00}", GrandTotal) + " </td><td align='center' class='" + GrandTotalWeekClass + "'>" + String.Format("{0:0,0.00}", GrandTotalWeek) + " </td><td align='center'>" + "$" + String.Format("{0:0,0.00}", GrandTotalPricePrior) + " </td></tr>";
                ItemsWarehouse += "  <tr style ='border: 1px solid black;font-weight: bold;'><td colspan='4'>TOTAL NUMBER OF ORDERS:</td><td align='center'>" + (totalboxallqty + totaladdonallqty) + " </td><td></td><td align='center'>" + (TotalBoxPrior + TotalAddonPrior) + " </td></tr>";

                htbl["@secondTable"] = ItemsWarehouse;

                #region OrderSummary-2.html



                content = ReadEmailTemplateFile(Common.TemplatePath, "OrderSummary-2.html");

                content = ReplaceFileVariablesInTemplateFile(htbl, content);

                //Initialize HTML to PDF converter 
                HtmlToPdfConverter htmlConverter2 = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

                WebKitConverterSettings settings2 = new WebKitConverterSettings();

                //HTML string and Base URL 
                string htmlText2 = content;

                string baseUrl2 = Common.baseUrl;

                //Set WebKit path
                settings2.WebKitPath = Common.WebKitPath;

                //Assign WebKit settings to HTML converter
                htmlConverter2.ConverterSettings = settings2;

                //Convert HTML string to PDF
                PdfDocument document2 = htmlConverter2.Convert(htmlText2, baseUrl2);

                //Save and close the PDF document 
                document2.Save(Common.PDFPath + "BoxTotalSummary.pdf");

                document2.Close(true);
                // Process.Start("E:/Output.pdf");
                #endregion

                return Common.PDFPath + "BoxTotalSummary.pdf";

            }
            catch (Exception ex)
            {
                log("--------------------------------------------------------", "//");
                log("BulkPickListALL: " + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                return "";
            }
        }

        string getPerClass(string value)
        {
            string preclass = "positive";
            if (value.Contains("-"))
            {
                preclass = "negative";
            }
            return preclass;

        }

        string MarginString(double d)
        {
            return (Math.Round(d)).ToString() + "%";
        }
        string DoubleToPercentageString(double d)
        {
            return (Math.Round(d, 2) * 100).ToString() + "%";
        }
        double CalculateChange(double previous, double current)
        {

            if (previous == 0)
            {
                // throw new InvalidOperationException();
                return 1;

            }

            else
            {
                var change = current - previous;
                return (double)change / previous;
            }


        }

        public DateTime getServerDateTime()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString());

            conn.Open();
            using (var cmd = new SqlCommand("select getdate()", conn))
            {
                return Convert.ToDateTime(cmd.ExecuteScalar());
            }
        }
        public class ItemRow
        {
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public string UOM { get; set; }
            //public double QTY { get; set; }
            //public long CustomerID { get; set; }
            //public string ShippingMethod { get; set; }

        }
        public class ItemRowWare
        {
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public double BRIS { get; set; }
            public double PriceBRIS { get; set; }
            public double GC { get; set; }
            public double PriceGC { get; set; }
            public double TWB { get; set; }
            public double PriceTWB { get; set; }
            public double COAST { get; set; }
            public double PriceCOAST { get; set; }
            public double BYRON { get; set; }
            public double PriceBYRON { get; set; }
        }
        public class ItemRowSale
        {

            public double BRIS { get; set; }
            public double PriceBRIS { get; set; }
            public double GC { get; set; }
            public double PriceGC { get; set; }
            public double TWB { get; set; }
            public double PriceTWB { get; set; }
            public double COAST { get; set; }
            public double PriceCOAST { get; set; }
            public double BYRON { get; set; }
            public double PriceBYRON { get; set; }
        }
        public class ItemRowMargin
        {

            public double Cost { get; set; }
            public double Sales { get; set; }
            public double Margin { get; set; }
            public double MarginPer { get; set; }
        }
        #region Read Email Template File and Replace File Variables


        private static string ReadEmailTemplateFile(string filePath, string fileName)
        {
            string str = string.Empty;

            string path = filePath + fileName;
            //return path;
            if (System.IO.File.Exists(path))
            {
                try
                {
                    StreamReader reader = System.IO.File.OpenText(path);
                    str = reader.ReadToEnd();
                    reader.Close();
                    return str;
                }
                catch (Exception)
                {
                    throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
                }
            }
            throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
        }

        public static string ReplaceFileVariablesInTemplateFile(Hashtable hashVars, string content)
        {
            IDictionaryEnumerator enumerator = hashVars.GetEnumerator();
            while (enumerator.MoveNext())
            {
                content = content.Replace(enumerator.Key.ToString(), enumerator.Value.ToString());
            }
            return content;
        }
        #endregion
        #region Create log for the actions.

        public void log(string ex, string type)
        {
            if (System.IO.File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (System.IO.Stream str = System.IO.File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.
    }
}
