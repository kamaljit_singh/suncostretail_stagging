﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace syncBoxTotalWeekEnd
{
    class Program
    {
        static void Main(string[] args)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(Common.PDFPath);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            Order obj = new Order();
            DateTime ReportDate = obj.getServerDateTime();

            DateTime ToDate = ReportDate.AddDays(-2);
            DateTime FromDate = ReportDate.AddDays(-6);
            #region Box Total PDF    

            string day = ReportDate.DayOfWeek.ToString();
            if (day.ToUpper() == "SUNDAY")
            {
                obj.log("--------------------------------------------------------", "//");
                obj.log("Start", "Box Total Summary");
                obj.log("--------------------------------------------------------", "//");

                String dy = ReportDate.Day.ToString("00");
                String mn = ReportDate.Month.ToString("00");
                String yy = ReportDate.Year.ToString();

                string attachments = obj.PDFForBoxTotalSummary(ReportDate, ToDate, FromDate);
                if (attachments != "")
                {
                    string subject = "BOX TOTAL WEEKEND REPORT " + dy + "  " + mn + "  " + yy;

                    string body = "Dear Suncoast Admin,"
                          + "<br/><br/>"
                          + "Please find a list below of all the box for the following period."
                          + "<br/>"
                          + "Monday " + FromDate.ToString("dd/MM/yyy") + " to Friday " + ToDate.ToString("dd/MM/yyy")
                          + "<br/><br/>"
                          + "If there are any errors or omissions, please email support@saavi.com.au"
                          + " <br/><br/>"
                          + "Regards"
                          + "<br/><br/>"
                          + "Many Thanks <br/>"
                          + "From the team at SAAVI";

                    string result =  SendMail.SendEMail(Common.CompanyAdminEmail, Common.CompanyAdminEmail_CC, "", subject, body, attachments, Common.FromEmail);

                    if (result == "ok")
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Sent Successfully", "Box Total Summary");
                        obj.log("--------------------------------------------------------", "//");
                    }
                    else
                    {
                        obj.log("--------------------------------------------------------", "//");
                        obj.log("Email Not Sent", result);
                        obj.log("--------------------------------------------------------", "//");
                    }

                }
                obj.log("--------------------------------------------------------", "//");
                obj.log("End", "Box Total Summary");
                obj.log("--------------------------------------------------------", "//");
            }
            #endregion
        }
    }
    public class Common
    {

        public static string TemplatePath = Convert.ToString(ConfigurationManager.AppSettings["TemplatePath"]);
        public static string WebKitPath = Convert.ToString(ConfigurationManager.AppSettings["WebKitPath"]);
        public static string baseUrl = Convert.ToString(ConfigurationManager.AppSettings["baseUrl"]);
        public static string PDFPath = Convert.ToString(ConfigurationManager.AppSettings["PDFPath"]);

        public static string CompanyAdminEmail_CC = Convert.ToString(ConfigurationManager.AppSettings["CompanyAdminEmail_CC"]);
        public static string FromEmail = Convert.ToString(ConfigurationManager.AppSettings["FROM_EMAILID"]);

        public static string CompanyAdminEmail = Convert.ToString(ConfigurationManager.AppSettings["CompanyAdminEmail"]);

        public static string LogoPath = Convert.ToString(ConfigurationManager.AppSettings["LogoPath"]);




        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
