﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace OrderPostingToFTP.DA
{
    public class SendMail
    {
        public static string MailSend(string toMail, string fromMail, string Subject, string body)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(toMail);
                mail.From = new MailAddress(fromMail);
                mail.Subject = Subject;
                mail.Body = body;
                mail.ReplyTo = new MailAddress(fromMail);

                //mail.ReplyTo = ("reetikabhardwaj31@gmail.com");
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.EnableSsl = false;
                smtp.Send(mail);
                return "Ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private static MailMessage AddAttachments(string attachments, MailMessage message)
        {
            if ((attachments.IndexOf(",") != -1) || (attachments.IndexOf(";") != -1))
            {
                char ch = ',';
                if (attachments.IndexOf(",") != -1)
                {
                    ch = ',';
                }
                else
                {
                    ch = ';';
                }
                string[] strArray = attachments.Split(new char[] { ch });
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (strArray[i].ToString().Trim() == "")
                    {
                        continue;
                    }
                    if (!System.IO.File.Exists(strArray[i].ToString().Trim()))
                    {
                        throw new IOException("Attachment File not found. Path: " + strArray[i]);
                    }
                    message.Attachments.Add(new Attachment(strArray[i].ToString().Trim()));
                }
                return message;
            }
            if (attachments.Length > 0)
            {
                if (!System.IO.File.Exists(attachments.Trim()))
                {
                    throw new IOException("Attachment File not found. Path: " + attachments);
                }
                message.Attachments.Add(new Attachment(attachments.Trim()));
            }
            return message;
        }
      
        private static MailMessage addEmailAddress(MailMessage message, string emailIDs, string code)
        {
            if ((emailIDs.IndexOf(",") != -1) || (emailIDs.IndexOf(";") != -1))
            {
                char ch = ',';
                if (emailIDs.IndexOf(",") != -1)
                {
                    ch = ',';
                }
                else
                {
                    ch = ';';
                }
                string[] strArray = emailIDs.Split(new char[] { ch });
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (!IsValidEmailID(strArray[i].ToString().Trim()))
                    {
                        throw new FormatException("Invalid email ID found in address list");
                    }
                    string str = code;
                    if (str == null)
                    {
                        goto Label_00F1;
                    }
                    if (!(str == "cc"))
                    {
                        if (str == "bcc")
                        {
                            goto Label_00CF;
                        }
                        goto Label_00F1;
                    }
                    message.CC.Add(new MailAddress(strArray[i].ToString().Trim()));
                    continue;
                    Label_00CF:
                    message.Bcc.Add(new MailAddress(strArray[i].ToString().Trim()));
                    continue;
                    Label_00F1:
                    message.To.Add(new MailAddress(strArray[i].ToString().Trim()));
                }
                return message;
            }
            if (!IsValidEmailID(emailIDs))
            {
                throw new FormatException("Invalid email ID found in " + code);
            }
            switch (code)
            {
                case "cc":
                    message.CC.Add(new MailAddress(emailIDs));
                    return message;

                case "bcc":
                    message.Bcc.Add(new MailAddress(emailIDs));
                    return message;
            }
            message.To.Add(new MailAddress(emailIDs));
            return message;
        }

        private static bool IsValidEmailID(string emailAddress)
        {
            return Regex.IsMatch(emailAddress, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        private static string ReadEmailTemplateFile(string filePath, string fileName)
        {
            string str = string.Empty;

            if (filePath == null)
            {
                filePath = string.Empty;
            }
            if (fileName == null)
            {
                fileName = string.Empty;
            }
            filePath = filePath.Trim();
            fileName = fileName.Trim();

            if ((filePath != string.Empty) && (filePath.IndexOf("/") != 0))
            {
                filePath = "/" + filePath;
            }
            if ((fileName != string.Empty) && (fileName.IndexOf("/") != 0))
            {
                fileName = "/" + fileName;
            }
            //return filePath + fileName;
            //string path = System.Web.Hosting.HostingEnvironment.MapPath(filePath + fileName); //System.Web.Hosting.HostingEnvironment.MapPath(filePath+fileName);// HttpContext.Current.Server.MapPath("/Image");
            string path = HttpContext.Current.Server.MapPath(filePath + fileName);
            //return path;
            if (System.IO.File.Exists(path))
            {
                try
                {
                    StreamReader reader = System.IO.File.OpenText(path);
                    str = reader.ReadToEnd();
                    reader.Close();
                    return str;
                }
                catch (Exception)
                {
                    throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
                }
            }
            throw new IOException("Error!! File Not located in the specified location.<br />Path: " + path + "<br />");
        }

        public static string ReplaceFileVariablesInTemplateFile(Hashtable hashVars, string content)
        {
            IDictionaryEnumerator enumerator = hashVars.GetEnumerator();
            while (enumerator.MoveNext())
            {
                content = content.Replace(enumerator.Key.ToString(), enumerator.Value.ToString());
            }
            return content;
        }

        private static void SendEMail(MailMessage mess)
        {
            SmtpClient client = new SmtpClient();
            client.EnableSsl = true;
            client.Send(mess);
        }

        public static string SendEMail(string to, string cc, string bcc, string subject, string content, string attachments, string fromKey)
        {
            try
            {
                string emailAddress = string.Empty;
                if ((fromKey == string.Empty) || (fromKey == null))
                {
                    emailAddress = ConfigurationManager.AppSettings["FROM_ID"];
                }
                else if (ConfigurationManager.AppSettings[fromKey] == null)
                {
                    emailAddress = fromKey;
                }
                else
                {
                    emailAddress = ConfigurationManager.AppSettings[fromKey];
                }
                if (emailAddress == null)
                {
                    emailAddress = string.Empty;
                }
                emailAddress = emailAddress.Trim();
                switch (emailAddress)
                {
                    case "":
                    case null:
                        throw new ArgumentNullException("From Email address found empty");
                }
                if ((to == string.Empty) || (to == null))
                {
                    throw new ArgumentNullException("To Email address found empty");
                }
                if (!IsValidEmailID(emailAddress))
                {
                    throw new FormatException("Invalid email ID found in From");
                }
                if ((subject == string.Empty) || (subject == null))
                {
                    throw new ArgumentNullException("Email subject found empty");
                }
                MailMessage message = new MailMessage
                {
                    From = new MailAddress(emailAddress),
                    ReplyTo = new MailAddress(emailAddress)
                };
                if (attachments != string.Empty)
                {
                    message = AddAttachments(attachments, message);
                }
                message = addEmailAddress(message, to, "to");
                if ((cc != string.Empty) && (cc != null))
                {
                    message = addEmailAddress(message, cc, "cc");
                }
                if ((bcc != string.Empty) && (bcc != null))
                {
                    message = addEmailAddress(message, bcc, "bcc");
                }
                message.Subject = subject;
                message.IsBodyHtml = true;
                if ((content == string.Empty) || (content == null))
                {
                    throw new ArgumentNullException("Email body content found empty");
                }
                message.Body = content;
                SendEMail(message);
                return "ok";
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }

        public static string SendEMail(string to, string cc, string bcc, string subject, string templatePath, string templateName, Hashtable hashVars, string attachments, string fromKey)
        {
            try
            {
                string content = string.Empty;
                string emailAddress = string.Empty;
                if ((fromKey == string.Empty) || (fromKey == null))
                {
                    emailAddress = ConfigurationManager.AppSettings["FROM_ID"];
                }
                else if (ConfigurationManager.AppSettings[fromKey] == null)
                {
                    emailAddress = fromKey;
                }
                else
                {
                    emailAddress = ConfigurationManager.AppSettings[fromKey];
                }
                if (emailAddress == null)
                {
                    emailAddress = string.Empty;
                }
                emailAddress = emailAddress.Trim();
                switch (emailAddress)
                {
                    case "":
                    case null:
                        throw new ArgumentNullException("From Email address found empty");
                }
                if ((to == string.Empty) || (to == null))
                {
                    throw new ArgumentNullException("To Email address found empty");
                }
                if (!IsValidEmailID(emailAddress))
                {
                    throw new FormatException("Invalid email ID found in From");
                }
                if ((subject == string.Empty) || (subject == null))
                {
                    throw new ArgumentNullException("Email subject found empty");
                }
                MailMessage message = new MailMessage
                {
                    From = new MailAddress(emailAddress)
                };
                if (attachments != string.Empty)
                {
                    message = AddAttachments(attachments, message);
                }
                message = addEmailAddress(message, to, "to");
                if ((cc != string.Empty) && (cc != null))
                {
                    message = addEmailAddress(message, cc, "cc");
                }
                if ((bcc != string.Empty) && (bcc != null))
                {
                    message = addEmailAddress(message, bcc, "bcc");
                }
                message.Subject = subject;
                message.IsBodyHtml = true;
                content = ReadEmailTemplateFile(templatePath, templateName);
                if (hashVars.Count > 0)
                {
                    content = ReplaceFileVariablesInTemplateFile(hashVars, content);
                }
                if ((content == string.Empty) || (content == null))
                {
                    throw new ArgumentNullException("Email body content found empty");
                }
                message.Body = content;
                SendEMail(message);
                return "ok";
            }
            catch (Exception exception)
            {
                return exception.Message + " Stack: " + exception.StackTrace;
            }
        }
    }
}