﻿using OrderPostingToFTP.BO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace OrderPostingToFTP.DA
{
    public class OrderDA
    {
        #region Get record from DB for order posting  to FTP
        public void PostRecord()
        {
            try
            {
                ///////////////////////////////////////////////////////////////
                /////////////////////NEED TO REVIEW////////////////////////////
                ///////////////////////////////////////////////////////////////

                //if (!IsSpRunning())
                //{

                log("Sync process started.", "Info");
                // If instance is already running.
                //var exists = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1;

                //if (!exists)
                //{


                using (var _db = new Entities())
                {


                    // ProcessStart();

                    //Check for 12 noon : if time 12 noon(12:00 PM) then next day orders for delivery will be released  :: Change Reguest 14042019

                    //if time 10 AM(10:00 AM) then next day orders for delivery will be released   :: Change Reguest 15042019
                    //if time 10 AM(09:00 AM) then next day orders for delivery will be released   :: Change Reguest 18042019
                    //if time 09 AM(08:00 AM) then next day orders for delivery will be released   :: Change Reguest 27052019

                    DateTime dt = DateTime.Parse("1900-01-01 08:00 AM"); //yyyy - MM - dd hh: mm tt

                    DateTime T1 = default(DateTime).Add(DateTime.Now.TimeOfDay);// Current Time
                    DateTime T2 = default(DateTime).Add(dt.TimeOfDay);//10:00 AM (Static Time)
                                                                      /////////////////////////////////////////////////////////////////////

                    DateTime d = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd hh:mm tt")).AddMinutes(-20);
                    DateTime CurrentDate = Convert.ToDateTime(d.ToString("yyyy-MM-dd"));

                    var cartV = new List<trnsCartMaster>();
                    var mo = new List<CartMissingOrder>();


                    if (T1.TimeOfDay > T2.TimeOfDay)//Check 09:00 AM
                    {
                        DateTime TomorrowDate = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"));

                        mo = _db.CartMissingOrders.Where(c => (c.IsOrderPostingToFTP == false || c.IsOrderPostingToFTP == null) && (DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(CurrentDate) || DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(TomorrowDate))).ToList();

                    }
                    else
                    {
                        mo = _db.CartMissingOrders.Where(c => c.UserID != 0 && (c.IsOrderPostingToFTP == false || c.IsOrderPostingToFTP == null) && (DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(CurrentDate))).ToList();

                    }
                    if (mo != null)
                    {
                        if (mo.Count > 0)
                        {
                            foreach (var tm in mo)
                            {

                                MIssingOrderPostingToFTP(Convert.ToInt64(tm.CartID), tm.OrderName, tm.OrderType, tm.customerID ?? 0, tm.UserID, Convert.ToInt32(tm.FilterID), tm.OrderDate);


                            }
                        }
                    }



                    if (T1.TimeOfDay > T2.TimeOfDay)//Check 09:00 AM
                    {
                        DateTime TomorrowDate = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"));

                        cartV = _db.trnsCartMasters.Where(c => c.UserID != 0 && (c.IsOrderPostingToFTP == false || c.IsOrderPostingToFTP == null) && (DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(CurrentDate) || DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(TomorrowDate))).ToList();
                    }
                    else
                    {
                        cartV = _db.trnsCartMasters.Where(c => c.UserID != 0 && (c.IsOrderPostingToFTP == false || c.IsOrderPostingToFTP == null) && (DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(CurrentDate))).ToList();

                    }


                    if (cartV != null)
                    {
                        if (cartV.Count > 0)
                        {
                            foreach (var tm in cartV)
                            {
                                OrderFile(tm, "_A", 0);

                                //if (HelperClass.SplitFoodVegOrder)
                                //{
                                //    OrderFile(tm, "_B", 4);
                                //    log("FoodVegOrder", "Info");
                                //}
                                //if (HelperClass.SplitProcVegOrder)
                                //{
                                //    OrderFile(tm, "_C", 9);
                                //    log("ProcVegOrder", "Info");
                                //}

                                //tm.IsOrderPostingToFTP = true;
                                //_db.SaveChanges();
                            }
                        }
                    }
                    //ProcessCompleted();
                }

                log("Sync process completed.", "Info");


            }
            catch (Exception ex)
            {

                log(ex.Message, "Error");
            }
        }
        #endregion


        #region OrderPosting To Client FTP

        private void OrderFile(trnsCartMaster tm, string name, int filter)
        {
            string newFileName = "orders_" + tm.CartID + name + ".xml";
            try
            {
                string filePath = HelperClass.InvoicePath + newFileName;
                if (File.Exists(filePath))
                {
                    using (var _db = new Entities())
                    {

                        var temp = _db.trnsCartMasters.Where(x => x.CartID == tm.CartID).FirstOrDefault();
                        if (temp != null)
                        {
                            if (HelperClass.IsFTPUploading)
                            {
                                bool isSuccess = true;
                                FTPHelper ftp = new FTPHelper(HelperClass.FTPUrl, HelperClass.FTPUserName, HelperClass.FTPPassword, HelperClass.FTP_UsePassive, temp.CartID);
                                if (ftp != null)
                                {
                                    isSuccess = ftp.upload(newFileName, filePath);
                                    if (!isSuccess)
                                    {
                                        for (int i = 0; i < 3; i++)
                                        {
                                            LogErrorsToServer("(From SYNC)FTP upload failed for order id " + temp.CartID + ". Attempt " + (i + 1), temp.CartID.ToString(), "Error");
                                            ftp.delete(newFileName);
                                            isSuccess = ftp.upload(newFileName, filePath);
                                            if (isSuccess)
                                            {// IsOrderPostingToFTP = true after ftp upload
                                                log("--------------------------------------------------------", "//");
                                                log("Step 2. Successfully uploaded in " + (i + 1) + " attempt : " + newFileName, "Info");
                                                log("--------------------------------------------------------", "//");
                                                break;
                                            }

                                        }
                                    }
                                    if (!isSuccess)
                                    {
                                        InsertMissingOrders(tm.CartID, newFileName, name, temp.CustomerID, temp.UserID, filter, temp.OrderDate);
                                        var customer = _db.CustomerMasters.Find(temp.CustomerID);
                                        LogErrorsToServer("(From SYNC)FTP upload failed for order id " + temp.CartID, temp.CartID.ToString(), "Error");
                                        string msg = ": Dear Company Admin, This email alert is to advise that SAAVI order number " + temp.CartID.ToString() + " for customer " + customer.CustomerName + " was not able to be placed within your ERP import folder due to network access reasons. We will continue for another 3 re-tries. If this order is not able to be placed, please take the appropriate action and either obtain the order file from the order confirmation email which has already been sent or manually enter the details.Attached is a copy of the order file.";
                                        log("--------------------------------------------------------", "//");
                                        log("Error from sync :" + msg, "Error");
                                        log("--------------------------------------------------------", "//");
                                        SendMail.SendEMail(HelperClass.AdminEmail, "", "", "Failed SAAVI Order Placement – order number " + temp.CartID.ToString() + " for customer " + customer.CustomerName, msg, filePath, HelperClass.FromEmail);

                                    }
                                    else
                                    {
                                        temp.IsOrderPostingToFTP = true;
                                        _db.SaveChanges();
                                        UpdateMissingOrders(tm.CartID, newFileName, name);
                                        log("--------------------------------------------------------", "//");
                                        log("Status Changed", "Info");
                                        log("Step 1. Successfully uploaded in 1 attempt : " + newFileName, "Info");
                                        log("--------------------------------------------------------", "//");
                                    }
                                }
                            }
                            else
                            {
                                log("--------------------------------------------------------", "//");
                                log("Status Changed", "Info");
                                log("IsFTPUploading flag is false ", "Info");
                                log("--------------------------------------------------------", "//");
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {

                log("--------------------------------------------------------", "//");
                log("Insert Missing Order", "Info");
                log("Missing Orders successfully inserted into database : " + newFileName, "Info");
                log("--------------------------------------------------------", "//");
                InsertMissingOrders(tm.CartID, newFileName, name, tm.CustomerID, tm.UserID, filter, tm.OrderDate);
                // ProcessCompleted();
                log("--------------------------------------------------------", "//");
                log("Order :(Sync) Order file " + newFileName + " was not able to be placed within your ERP import folder due to (" + ex.Message + ") reasons .We will continue for another 3 re-tries.", "Info");
                log("(Sync) Error in upload. Message :" + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                // throw ex;
            }
        }
        #endregion

        #region InsertMissingOrders
        public void InsertMissingOrders(long cartID, string OrderName, string TypeName, long customerID, long? Userid, int filter, DateTime? OD)
        {
            using (var _db = new Entities())
            {
                var op = _db.CartMissingOrders.Where(x => x.CartID == cartID && x.OrderType == TypeName && x.OrderName == OrderName).FirstOrDefault();
                if (op == null)
                {
                    CartMissingOrder p = new CartMissingOrder();
                    p.CartID = cartID;
                    p.OrderName = OrderName;
                    p.OrderType = TypeName;
                    p.customerID = customerID;
                    p.UserID = Userid;
                    p.FilterID = filter;
                    p.OrderDate = OD;
                    p.IsOrderPostingToFTP = false;
                    _db.CartMissingOrders.Add(p);
                    _db.SaveChanges();
                }

            }

        }
        #endregion

        #region UpdateMissingOrders
        public void UpdateMissingOrders(long cartID, string OrderName, string TypeName)
        {
            using (var _db = new Entities())
            {
                var op = _db.CartMissingOrders.Where(x => x.CartID == cartID && x.OrderType == TypeName && x.OrderName == OrderName).FirstOrDefault();
                if (op != null)
                {
                    op.IsOrderPostingToFTP = true;
                    _db.SaveChanges();
                }

            }
        }
        #endregion

        #region MissingOrderPosting
        public void MIssingOrderPostingToFTP(long CartID, string OrderName, string name, long customerID, long? Userid, int filter, DateTime? OD)
        {
            string newFileName = "orders_" + CartID + name + ".xml";
            try
            {
                string filePath = HelperClass.InvoicePath + newFileName;
                if (File.Exists(filePath))
                {
                    using (var _db = new Entities())
                    {

                        var temp = _db.trnsCartMasters.Where(x => x.CartID == CartID).FirstOrDefault();
                        if (temp != null)
                        {
                            if (HelperClass.IsFTPUploading)
                            {
                                bool isSuccess = true;
                                FTPHelper ftp = new FTPHelper(HelperClass.FTPUrl, HelperClass.FTPUserName, HelperClass.FTPPassword, HelperClass.FTP_UsePassive, temp.CartID);
                                if (ftp != null)
                                {
                                    isSuccess = ftp.upload(newFileName, filePath);

                                    if (!isSuccess)
                                    {
                                        for (int i = 0; i < 3; i++)
                                        {
                                            LogErrorsToServer("Missing :(From SYNC)FTP upload failed for order id " + temp.CartID + ". Attempt " + (i + 1), temp.CartID.ToString(), "Error");
                                            ftp.delete(newFileName);
                                            isSuccess = ftp.upload(newFileName, filePath);
                                            if (isSuccess)
                                            {// IsOrderPostingToFTP = true after ftp upload

                                                log("--------------------------------------------------------", "//");
                                                log("Step 2. Successfully uploaded missing order in  " + (i + 1) + " attempt : " + newFileName, "Info");
                                                log("--------------------------------------------------------", "//");

                                                break;
                                            }
                                        }
                                    }
                                    if (!isSuccess)
                                    {

                                        var customer = _db.CustomerMasters.Find(temp.CustomerID);
                                        LogErrorsToServer("Missing :(From SYNC)FTP upload failed for order id " + temp.CartID, temp.CartID.ToString(), "Error");
                                        string msg = ": Dear Company Admin, This email alert is to advise that SAAVI order number " + temp.CartID.ToString() + " for customer " + customer.CustomerName + " was not able to be placed within your ERP import folder due to network access reasons. We will continue for another 3 re-tries. If this order is not able to be placed, please take the appropriate action and either obtain the order file from the order confirmation email which has already been sent or manually enter the details.Attached is a copy of the order file.";
                                        log("--------------------------------------------------------", "//");
                                        log("Error from missing sync :" + msg, "Error");
                                        log("--------------------------------------------------------", "//");
                                        SendMail.SendEMail(HelperClass.AdminEmail, "", "", "Failed SAAVI Order Placement – order number " + temp.CartID.ToString() + " for customer " + customer.CustomerName, msg, filePath, HelperClass.FromEmail);

                                    }
                                    else
                                    {
                                        temp.IsOrderPostingToFTP = true;
                                        _db.SaveChanges();
                                        UpdateMissingOrders(CartID, newFileName, name);

                                        log("--------------------------------------------------------", "//");
                                        log("Status Changed", "Info");
                                        log("Step 1. Successfully uploaded missing order in 1 attempt : " + newFileName, "Info");
                                        log("--------------------------------------------------------", "//");

                                    }
                                }
                            }
                            else
                            {
                                log("--------------------------------------------------------", "//");
                                log("Status Changed", "Info");
                                log("Missing : IsFTPUploading flag is false ", "Info");
                                log("--------------------------------------------------------", "//");
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {

                InsertMissingOrders(CartID, newFileName, name, customerID, Userid, filter, OD);
                // ProcessCompleted();
                log("--------------------------------------------------------", "//");
                log("Missing :(Sync) Order file " + newFileName + " was not able to be placed within your ERP import folder due to (" + ex.Message + ") reasons .We will continue for another 3 re-tries.", "Info");
                log("Missing :(Sync) Error in missing order file upload. Message :" + ex.Message, "Error");
                log("--------------------------------------------------------", "//");
                // throw ex;
            }

        }
        #endregion

        #region Log Errors to server for order posting

        private void LogErrorsToServer(string ex, string cusOrderID, string type)
        {
            using (var _db = new Entities())
            {
                trnsCartResponseMaster cr = new trnsCartResponseMaster();
                cr.CartId = Convert.ToInt64(cusOrderID);
                cr.MethodName = type;
                cr.Response = ex;
                cr.CreatedDate = DateTime.Now;

                _db.trnsCartResponseMasters.Add(cr);
                _db.SaveChanges();
            }
        }

        #endregion Log Errors to server for order posting

        #region Create log for the actions.

        private static void log(string ex, string type)
        {
            if (File.Exists(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
            {
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
            else
            {
                using (Stream str = File.Create(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt"))
                {
                };
                File.AppendAllText(ConfigurationManager.AppSettings["LogPath"].ToString() + "Log.txt", type + " : " + DateTime.Now + "  -  " + ex + "\r\n" + Environment.NewLine);
            }
        }

        #endregion Create log for the actions.

        #region MissingOrderSp
        public void MissingOrder()
        {
            try
            {
                log("--------------------------------------------------------", "//");
                log("Missing Order SP Start", "Info");
                log("--------------------------------------------------------", "//");
                using (var _db = new Entities())
                {
                    _db.sp_CheckMissingOrders();
                }
                log("--------------------------------------------------------", "//");
                log("Missing Order SP Stop", "Info");
                log("--------------------------------------------------------", "//");
            }
            catch (Exception ex)
            {
                log("Missing Order SP :" + ex.Message, "Error");
            }

        }
        #endregion


        #region PostRecord FV And PV
        public void PostRecordFVAndPV()
        {
            log("Sync FV and PV process started.", "Info");
            using (var _db = new Entities())
            {

                DateTime d = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd hh:mm tt")).AddMinutes(-20);
                DateTime CurrentDate = Convert.ToDateTime(d.ToString("yyyy-MM-dd"));
                var cartV = new List<trnsCartMaster>();
                var mo = new List<CartMissingOrder>();
                mo = _db.CartMissingOrders.Where(c => c.UserID != 0 && (c.IsOrderPostingToFTP == false || c.IsOrderPostingToFTP == null)).ToList(); //&& (DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(CurrentDate))).ToList();

                if (mo != null)
                {
                    if (mo.Count > 0)
                    {
                        foreach (var tm in mo)
                        {
                            MIssingOrderPostingToFTP(Convert.ToInt64(tm.CartID), tm.OrderName, tm.OrderType, tm.customerID ?? 0, tm.UserID, Convert.ToInt32(tm.FilterID), tm.OrderDate);
                        }
                    }
                }

                cartV = _db.trnsCartMasters.Where(c => c.UserID != 0 && (c.IsOrderPostingToFTP == false || c.IsOrderPostingToFTP == null)).ToList(); //&& (DbFunctions.TruncateTime(c.OrderDate) == DbFunctions.TruncateTime(CurrentDate))).ToList();
                if (cartV != null)
                {
                    if (cartV.Count > 0)
                    {
                        foreach (var tm in cartV)
                        {                         
                            if (HelperClass.SplitFoodVegOrder)
                            {
                                OrderFile(tm, "_B", 4);
                                log("FoodVegOrder", "Info");
                            }
                            if (HelperClass.SplitProcVegOrder)
                            {
                                OrderFile(tm, "_C", 9);
                                log("ProcVegOrder", "Info");
                            }
                        }
                    }
                }               
            }

            log("Sync FV and PV process completed.", "Info");

        }
        #endregion

    }
}
