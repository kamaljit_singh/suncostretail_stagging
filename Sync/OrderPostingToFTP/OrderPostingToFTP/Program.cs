﻿using OrderPostingToFTP.BO;
using OrderPostingToFTP.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderPostingToFTP
{
    class Program
    {
        static void Main(string[] args)
        {
            // If instance is already running.
            var exists = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1;

            if (!exists)
            {
                OrderDA objDA = new OrderDA();
                objDA.PostRecord();
                objDA.PostRecordFVAndPV();
                objDA.MissingOrder();
            }
        }
    }
}
