﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderPostingToFTP.BO
{
    public class OrderBO
    {
    }


    public static class HelperClass
    {        
        public static string InvoicePath = ConfigurationManager.AppSettings["InvoicePath"].ToString();
        public static string WebsiteUrl = ConfigurationManager.AppSettings["WEBSITE_URL"].ToString();
        public static bool IsXMLGeneration = Convert.ToString(ConfigurationManager.AppSettings["IsXMLGeneration"]) == "1";
        public static string AdminEmail = Convert.ToString(ConfigurationManager.AppSettings["AdminEmail"]);
        public static bool IsFTPUploading = Convert.ToString(ConfigurationManager.AppSettings["IsFTPUploading"]) == "1";

        public static string ToEmail = Convert.ToString(ConfigurationManager.AppSettings["TO_EMAILID"]);
        public static string FromEmail = Convert.ToString(ConfigurationManager.AppSettings["FROM_EMAILID"]);
        public static string ApiURLLive = Convert.ToString(ConfigurationManager.AppSettings["APIURL"]);
        public static string FTPUrl = Convert.ToString(ConfigurationManager.AppSettings["FTPUrl"]);
        public static string FTPUserName = Convert.ToString(ConfigurationManager.AppSettings["FTPUserName"]);
        public static string FTPPassword = Convert.ToString(ConfigurationManager.AppSettings["FTPPassword"]);
        public static bool FTP_UsePassive = Convert.ToString(ConfigurationManager.AppSettings["FTP_UsePassive"]) == "1";
        public static string TempltePath = Convert.ToString(ConfigurationManager.AppSettings["TEMPLATE_PATH"]);
        public static string Company = ConfigurationManager.AppSettings["CompanyName"];
        public static bool SendCustomerAndRepMail = Convert.ToString(ConfigurationManager.AppSettings["SendCustomerAndRepMail"]) == "1";

        public static bool SplitSubCategoryOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitSubCategoryOrder"]) == "1";
        public static bool SplitCategoryOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitCategoryOrder"]) == "1";
        public static bool SplitFoodVegOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitFoodVegOrder"]) == "1";
        public static bool SplitProcVegOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitProcVegOrder"]) == "1";

        public static string SplitSubCategoryName = Convert.ToString(ConfigurationManager.AppSettings["SplitSubCategoryName"]);
        public static string SplitCategoryName = Convert.ToString(ConfigurationManager.AppSettings["SplitCategoryName"]);

       
    }
}
