﻿using Syncfusion.ExcelToPdfConverter;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using Syncfusion.Pdf.HtmlToPdf;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace syncPDFDEMO
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize HTML converter 

            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

            // WebKit converter settings

            WebKitConverterSettings webKitSettings = new WebKitConverterSettings();

            //Assign the WebKit binaries path

            webKitSettings.WebKitPath = "E:/Working_2019/GIT_Rep_S5/J&R_ForsterMeats/syncSupplierOrderReport/packages/Syncfusion.HtmlToPdfConverter.QtWebKit.WinForms.17.1.0.47/lib/QtBinaries/";

            webKitSettings.Orientation = PdfPageOrientation.Landscape;
            // Enable table of contents
            Program obj = new Program();
            //webKitSettings.EnableToc = true;
            //Set print media type
            webKitSettings.MediaType = MediaType.Print;
            webKitSettings.PdfFooter = obj.AddPdfFooter(webKitSettings.PdfPageSize.Width, "");
            //Assign the WebKit settings

            htmlConverter.ConverterSettings = webKitSettings;

            //Convert HTML to PDF

            PdfDocument document = htmlConverter.Convert("E:/Working_2019/GIT_Rep_S5/SunCoast/Sync/syncPDFDEMO/syncPDFDEMO/demo.html");

            //Save and close the PDF document

            document.Save("Output.pdf");

            document.Close(true);
        }
        public PdfPageTemplateElement AddPdfFooter(float width, string footerText)
        {
            RectangleF rect = new RectangleF(0, 0, width, 20);
            //Create a new instance of PdfPageTemplateElement class.
            PdfPageTemplateElement footer = new PdfPageTemplateElement(rect);
            PdfGraphics g = footer.Graphics;

            // Draw footer text.
            PdfSolidBrush brush = new PdfSolidBrush(Color.Gray);
            PdfFont font = new PdfTrueTypeFont(new Font("Helvetica", 6, FontStyle.Bold), true);
            float x = (width / 2) - (font.MeasureString(footerText).Width) / 2;
            g.DrawString(footerText, font, brush, new RectangleF(x, g.ClientSize.Height - 10, font.MeasureString(footerText).Width, font.Height));

            //Create page number field
            PdfPageNumberField pageNumber = new PdfPageNumberField(font, brush);

            //Create page count field
            PdfPageCountField count = new PdfPageCountField(font, brush);

            PdfCompositeField compositeField = new PdfCompositeField(font, brush, "Page {0} of {1}", pageNumber, count);
            compositeField.Bounds = footer.Bounds;
            compositeField.Draw(g, new PointF(750, 10));

            return footer;
        }
    }
}
