﻿using SaaviOnlineOrdering.Models.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.App_Start
{
    public class AutoMapperConfig
    {
        /// <summary>
        /// Mappings for Automapper to be used throughout the application
        /// </summary>
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<TempCartItem, UpdateCartModel>()
                .ForMember(dest => dest.UnitId, opt => opt.MapFrom(src => src.OrderUnitId))
                .ForMember(dest => dest.IsSpecialPrice, opt => opt.MapFrom(src => src.IsSpecial))
                .ForMember(dest => dest.IsGstApplicable, opt => opt.MapFrom(src => src.IsGST))
                .ForMember(dest => dest.BasePrice, opt => opt.MapFrom(src => src.CompanyPrice));
            });
        }
    }
}