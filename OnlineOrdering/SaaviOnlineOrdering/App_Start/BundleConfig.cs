﻿using System.Web;
using System.Web.Optimization;

namespace SaaviOnlineOrdering
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/scripts.js")
                );

            bundles.Add(new StyleBundle("~/Content/mainCss")
                    .Include("~/Content/bootstrap.min.css")
                     .Include("~/content/fonts/font-awesome-4.7.0/css/font-awesome.min.css")
                     .Include("~/Content/jquery-confirm.min.css")
                    .Include("~/Content/styles.css")
                    .Include("~/Content/responsive.css")
                    .Include("~/content/css/util.css")

                    .Include("~/content/html5tooltips.css")
                    .Include("~/content/html5tooltips.animation.css")
                     .Include("~/Content/airdatepicker.css")
                     .Include("~/Content/lightbox.css")
                     //.Include("~/Content/vuetify.css")
                     .Include("~/Content/css/icheck-material.css")
                      .Include("~/Content/appTheme.css")
                );

            bundles.Add(new StyleBundle("~/Content/Logincss")
                .Include("~/content/vendor/bootstrap/css/bootstrap.min.css")
                .Include("~/content/fonts/font-awesome-4.7.0/css/font-awesome.min.css")
                .Include("~/content/fonts/iconic/css/material-design-iconic-font.min.css")
                .Include("~/content/vendor/css-hamburgers/hamburgers.min.css")
                .Include("~/Content/jquery-confirm.min.css")
                .Include("~/content/css/util.css")
                .Include("~/content/css/main.css")
                 .Include("~/Content/appTheme.css")
                );

            bundles.Add(new ScriptBundle("~/bundles/Loginscripts")
                .Include("~/scripts/jquery-3.3.1.min.js")
                .Include("~/Content/vendor/bootstrap/js/popper.js")
                .Include("~/Content/vendor/bootstrap/js/bootstrap.min.js")
                .Include("~/Scripts/jquery.unobtrusive-ajax.min.js")
                .Include("~/Scripts/blockUI.js")
                .Include("~/content/js/main.js")
                .Include("~/Scripts/jquery-confirm.min.js")
                .Include("~/scripts/loginScript.js")

                );

            bundles.Add(new ScriptBundle("~/bundles/mainJs")
                .Include("~/Scripts/html5tooltips.js")
                .Include("~/Scripts/moment.js")
                .Include("~/Scripts/blockUI.js")
                .Include("~/Scripts/jquery-confirm.min.js")
                //.Include("~/Scripts/scrolling-tabs.js")
                .Include("~/Scripts/lightbox.min.js")
                .Include("~/Scripts/vue-2.5.16.js")
                .Include("~/Scripts/airdatepicker.js")
                .Include("~/Scripts/airdatepicker-en.js")
                .Include("~/Scripts/vue-filters.js")
                .Include("~/Scripts/vuebar.js")
                .Include("~/Scripts/axios.js")
                );

            //BundleTable.EnableOptimizations = true;
        }
    }
}