﻿using CommonFunctions;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using SaaviOnlineOrdering.Models.BO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SaaviOnlineOrdering.Models.DAL
{
    public class AccountDA
    {
        public LoginResponse Login(LoginViewModel model)
        {
            try
            {
                var getTokenUrl = ConfigurationManager.AppSettings["ApiBaseUri"];

                using (HttpClient httpClient = new HttpClient())
                {
                    HttpContent content = new FormUrlEncodedContent(new[]
                    {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", model.Username?.Trim() ?? ""),
                    new KeyValuePair<string, string>("password", model.Password?? ""),
                    new KeyValuePair<string, string>("deviceToken", "Webview"),
                    new KeyValuePair<string, string>("deviceType", "Web Ordering"),
                    new KeyValuePair<string, string>("LoginType", model.IsGuest == true ? "G" : "")
                    });

                    HttpResponseMessage result = httpClient.PostAsync(getTokenUrl + "token", content).Result;
                    string resultContent = result.Content.ReadAsStringAsync().Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var token = JsonConvert.DeserializeObject<Token>(resultContent);

                        AuthenticationProperties options = new AuthenticationProperties();

                        options.AllowRefresh = true;
                        options.IsPersistent = false;
                        options.ExpiresUtc = DateTime.UtcNow.AddSeconds(token.expires_in);

                        string mainFeaturesSerial = GetMainFeatures(getTokenUrl, httpClient);
                        CustomerDetailsResult custData = GetCustomerFeatures(getTokenUrl, httpClient, token);

                        string custFeatures = "";
                        string custDetails = "";
                        string deliveryCharges = "";
                        string pdfDetails = "";
                        string isRetail = custData.Result.customerDetails[0].IsRetailUser ? "Yes" : "No";
                        string defaultInfo = GetDefaultOrderingInfo(getTokenUrl, token);

                        if (custData != null && custData.ResponseCode == "200")
                        {
                            custFeatures = JsonConvert.SerializeObject(custData.Result.customerFeatures);
                            custDetails = JsonConvert.SerializeObject(custData.Result.customerDetails);
                            deliveryCharges = JsonConvert.SerializeObject(custData.Result.DeliveryCharges);
                            pdfDetails = JsonConvert.SerializeObject(custData.Result.PDFDetails);
                        }
                        var claims = new[]
                        {
                            new Claim("Name", token.Name),
                            new Claim("CustomerID", token.customerID.ToString()),
                            new Claim("UserID", token.userID.ToString()),
                            new Claim("BusinessName", token.BusinessName),
                            new Claim("Role", token.role),
                            new Claim("MainFeatures", mainFeaturesSerial),
                            new Claim("CustomerFeatures", custFeatures),
                            new Claim("DeliveryCharges", deliveryCharges),
                            new Claim("PDFDetails", pdfDetails),
                            new Claim("DefaultInfo", defaultInfo),
                            new Claim("AcessToken", string.Format("Bearer {0}", token.access_token)),
                            new Claim("IsRetail", isRetail),
                            new Claim("IsGuest", model.IsGuest == true ? "Yes" : "No")
                        };

                        var identity = new ClaimsIdentity(claims, "ApplicationCookie");

                        HttpContext.Current.Request.GetOwinContext().Authentication.SignIn(options, identity);

                        return new LoginResponse()
                        {
                            Message = "Success",
                            MainFeature = mainFeaturesSerial,
                            CustomerFeatures = custFeatures,
                            DefaultInfo = defaultInfo,
                            CustDetails = custDetails,
                            PDFDetails = custData.Result.PDFDetails
                        };
                    }
                    else
                    {
                        string message = "";
                        if (resultContent.Contains("error"))
                        {
                            var res = JObject.Parse(resultContent);
                            message = res["error_description"].ToString();
                        }

                        return new LoginResponse()
                        {
                            Message = message,
                            MainFeature = "",
                            DefaultInfo = ""
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return new LoginResponse()
                {
                    Message = ex.Message,
                    MainFeature = "",
                    DefaultInfo = ""
                };
            }
        }

        private static string GetMainFeatures(string getTokenUrl, HttpClient httpClient)
        {
            HttpResponseMessage resultmain = httpClient.GetAsync(getTokenUrl + "api/Account/GetMainFeatures").Result;
            string mainFeaturesSerial = "";
            if (resultmain.IsSuccessStatusCode)
            {
                string resultFeatures = resultmain.Content.ReadAsStringAsync().Result;

                var res = JsonConvert.DeserializeObject<ApiResponse>(resultFeatures);

                mainFeaturesSerial = JsonConvert.SerializeObject(res.Result);
            }

            return mainFeaturesSerial;
        }

        private static CustomerDetailsResult GetCustomerFeatures(string getTokenUrl, HttpClient httpClient, Token token)
        {
            var request = new
            {
                CustomerID = token.customerID.To<long>(),
                IsRepUser = false,
                UserID = token.userID.To<long>()
            };

            var postData = JsonConvert.SerializeObject(request);
            var content = new StringContent(postData, Encoding.UTF8, "application/json");

            HttpResponseMessage resultmain = httpClient.PostAsync(getTokenUrl + "api/Account/GetCustomerDetailsAndFeatures", content).Result;

            if (resultmain.IsSuccessStatusCode)
            {
                string resultFeatures = resultmain.Content.ReadAsStringAsync().Result;

                var res = JsonConvert.DeserializeObject<CustomerDetailsResult>(resultFeatures);

                return res;
                // customerFeatures = JsonConvert.SerializeObject(res.Result.customerFeatures);
            }
            else
            {
                return null;
            }
        }

        private static string GetDefaultOrderingInfo(string getTokenUrl, Token token)
        {
            var requestData = new
            {
                customerID = token.customerID.To<long>()
            };

            var postData = JsonConvert.SerializeObject(requestData);

            string defaultInfo = "";

            var client = new RestClient(getTokenUrl + "api/Account/GetDefaultOrderingInfo");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "bearer " + token.access_token);
            request.AddParameter("application/json", postData, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                string result = response.Content;

                var res = JsonConvert.DeserializeObject<DefaultOrderingInfoResult>(result);

                defaultInfo = JsonConvert.SerializeObject(res.Result);
            }

            return defaultInfo;
        }

        #region Registration Method

        public string Register(RegisterViewModel model)
        {
            try
            {
                model.ConfirmPassword = model.Password;
                var registerUrl = ConfigurationManager.AppSettings["ApiBaseUri"] + "api/Account/Register";
                string result = "";
                using (HttpClient httpClient = new HttpClient())
                {
                    //httpClient.DefaultRequestHeaders.Accept.Clear();
                    //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var postData = JsonConvert.SerializeObject(model);
                    var content = new StringContent(postData, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = httpClient.PostAsync(registerUrl, content).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content.ReadAsStringAsync().Result);
                        result = res.Message;
                    }
                    else
                    {
                        result = "An error occured";
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion Registration Method

        #region Forgot passoword

        public string ResetPassword(string email)
        {
            try
            {
                var getTokenUrl = ConfigurationManager.AppSettings["ApiBaseUri"];
                var requestData = new
                {
                    Email = email
                };

                var postData = JsonConvert.SerializeObject(requestData);

                var client = new RestClient(getTokenUrl + "api/Account/ForgotPassword");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                // request.AddHeader("authorization", "bearer " + token);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    string result = response.Content;

                    var res = JsonConvert.DeserializeObject<ApiResponse>(result);

                    return res.Message;
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Forgot passoword
    }
}