﻿using Newtonsoft.Json;
using RestSharp;
using SaaviOnlineOrdering.Models.BO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;

namespace SaaviOnlineOrdering.Models.DAL
{
    public class ProductsDA
    {
        private readonly string baseUrl;

        public ProductsDA()
        {
            baseUrl = ConfigurationManager.AppSettings["ApiBaseUri"];
        }

        #region Get Product filters

        public List<ProductFiltersBO> GetProductFilters()
        {
            try
            {
                var productFilters = new List<ProductFiltersBO>();
                using (var httpClient = new HttpClient())
                {
                    HttpResponseMessage resultmain = httpClient.GetAsync(baseUrl + "api/Products/ProductFilters").Result;
                    if (resultmain.IsSuccessStatusCode)
                    {
                        string resultFeatures = resultmain.Content.ReadAsStringAsync().Result;

                        var res = JsonConvert.DeserializeObject<ApiResponse>(resultFeatures);

                        var filters = JsonConvert.SerializeObject(res.Result);
                        productFilters = JsonConvert.DeserializeObject<List<ProductFiltersBO>>(filters);

                        return productFilters;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Product filters

        #region Get pantry list items

        public object GetPantryListItems(GetPantryBO model)
        {
            try
            {
                List<PantryItem> li = new List<PantryItem>();
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/GetPantryItems");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", model.AccessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    string result = response.Content;

                    var res = JsonConvert.DeserializeObject<ApiResponsePantry>(result);

                    if (res.ResponseCode == "201")
                    {
                        return null;
                    }
                    var pantryItems = JsonConvert.SerializeObject(res.Result.products);

                    li = JsonConvert.DeserializeObject<List<PantryItem>>(pantryItems);

                    return new
                    {
                        pantryItems = li,
                        totalPages = res.Result.TotalPages,
                        savedOrder = res.Result.SavedOrder,
                        totalProducts = res.Result.TotalResults
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get pantry list items

        #region Insert item into temp cart

        public TempCartResponseBO InsertUpdateTempCart(InsertUpdateTempCartBO model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/InsertUpdateTempCart");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<TempCartResponseBO>(response.Content);

                    return res;
                }
                else
                {
                    return new TempCartResponseBO()
                    {
                        ResponseCode = "204",
                        Message = "Insert failed."
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Insert item into temp cart

        #region Get cart count and total

        public CartCountResponse GetCartCount(CartCountModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/GetCartCount");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<CartCountResponse>(response.Content);

                    return res;
                }
                else
                {
                    return new CartCountResponse()
                    {
                        Message = "An error occured",
                        ResponseCode = "204"
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get cart count and total

        #region Get Stripe Keys

        public StripeKeysResponse GetStripeKeys(string accessToken = "")
        {
            try
            {
                var client = new RestClient(baseUrl + "api/Payment/GetStripePublicKeys");
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<StripeKeysResponse>(response.Content);

                    return res;
                }
                else
                {
                    return new StripeKeysResponse();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Stripe Keys

        #region Save Payment Log

        public object SavePaymentLog(string payResponse, long tempCartId, string accessToken)
        {
            try
            {
                var requestData = new
                {
                    PayResponse = payResponse,
                    TempCartID = tempCartId
                };
                var postData = JsonConvert.SerializeObject(requestData);
                var client = new RestClient(baseUrl + "api/Payment/LogPayment");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<string>(response.Content);

                    return new { Message = res };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Save Payment Log

        #region Get stripe payment token

        public string GetPaymentToken(double paymentTotal, bool isCapture, long tempCartID, string accessToken)
        {
            try
            {
                var model = new GetStipePayTokenModel()
                {
                    PaymentTotal = paymentTotal,
                    CurrencyCode = "aud",
                    OnlyAuthorize = !isCapture,
                    TempCartID = tempCartID,
                    DeviceType = "web"
                };
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Payment/GetPaymentToken");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<string>(response.Content);
                }
                else
                {
                    return "Invalid";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPaymentToken(PaymentTokenModel paymentDetails)
        {
            try
            {
                var model = new GetStipePayTokenForPastOrdersModel()
                {
                    PaymentTotal = paymentDetails.TotalAmount,
                    CurrencyCode = "aud",
                    OnlyAuthorize = false,
                    CartIDs = paymentDetails.OrderIds,
                    DeviceType = "web"
                };
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Payment/GetPaymentTokenForPastOrders");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                //request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<string>(response.Content);
                }
                else
                {
                    return "Invalid";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get stripe payment token
        #region  Recurring Payment Subscription Methods

        public object SaveSubcriptionPaymentLog(string payResponse, long customerId, int recurringId,double? amount, string accessToken)
        {
            try
            {
                var requestData = new
                {
                    payResponse = payResponse,
                    recurringId = customerId,
                    customerId = customerId,
                    price= amount,
                };
                var postData = JsonConvert.SerializeObject(requestData);
                var client = new RestClient(baseUrl + "api/Payment/SaveSubcriptionPaymentLog");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<string>(response.Content);

                    return new { Message = res };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string CreateSubscription(SubscriptionDetail model , string accessToken)
        {
            try
            {
                model.devicetype = "web";
                var postData = JsonConvert.SerializeObject(model);
                var client = new RestClient(baseUrl + "api/Payment/CreateSubscription");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<string>(response.Content);
                }
                else
                {
                    return "Invalid";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string CancelSubscription(string planid, long customerid,string accessToken)
        {
              try
                {
                    var model = new CancelRecurringpayment()
                    {
                        CustomerID = customerid,
                        PlanID = planid,
                        DeviceType = "web"
                    };
                    var postData = JsonConvert.SerializeObject(model);

                    var client = new RestClient(baseUrl + "api/Payment/CancelSubscription");
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("content-type", "application/json");
                    request.AddHeader("authorization", accessToken);
                    request.AddParameter("application/json", postData, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                    if (response.IsSuccessful)
                    {
                        return JsonConvert.DeserializeObject<string>(response.Content);
                    }
                    else
                    {
                        return "Invalid";
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
           
          }

        #endregion  Recurring Payment Methods

        #region Get temp cart items

        public ApiResponseGetTempCartItems GetTempCartItems(GetTempCartModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/GetTempCartItems");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponseGetTempCartItems>(response.Content);
                    return res;
                }
                else
                {
                    return new ApiResponseGetTempCartItems()
                    {
                        ResponseCode = "500",
                        Message = "Internal error"
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get temp cart items

        #region Delete items from temp cart

        public string DeleteCartItem(bool isDeleteAll, long cartItemID, bool isSavedOrder)
        {
            try
            {
                var model = new
                {
                    CartItemID = cartItemID,
                    IsSavedOrder = isSavedOrder
                };
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/DeleteCartItems");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }

                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Delete items from temp cart

        #region Update temp cart item

        public string UpdateCartItem(UpdateCartModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/UpdateCartItem");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }
                else
                {
                    return "Error updating cart item";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Update temp cart item

        #region Get / Delete / Add order comments

        public List<OrderCommentsBO> GetOrderComments(long customerID, long productID, bool isProduct, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new
                {
                    CustomerID = customerID,
                    ProductID = productID,
                    IsProduct = isProduct
                });

                var client = new RestClient(baseUrl + "api/Orders/GetComments");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var comments = JsonConvert.DeserializeObject<List<OrderCommentsBO>>(JsonConvert.SerializeObject(res.Result));
                    return comments;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteComment(long commentID, bool isProduct, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new
                {
                    CommentID = commentID,
                    IsProduct = isProduct
                });

                var client = new RestClient(baseUrl + "api/Orders/DeleteComment");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddComments(AddCommentModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/AddComments");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get / Delete / Add order comments

        #region Place / save order

        public object PlaceOrder(PlaceOrderModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/PlaceOrder");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<PlaceOrderResult>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Place / save order

        #region Save Past Order Payment Details

        public object SavePastOrderPaymentDetails(SavePastOrderPaymentModel model)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/SavePastOrderPayment");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                //request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<PlaceOrderResult>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Save Past Order Payment Details

        #region Get Invoices

        public object GetInvoices(GetInvoicesModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/GetInvoices");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<GetInvoicesResult>>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Invoices

        #region Get product categories and sub categories

        public object GetProductCategories(bool showSubOnly, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { showSubOnly = showSubOnly });

                var client = new RestClient(baseUrl + "api/Products/ProductCategories");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<ProductCategoriesBO>>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get product categories and sub categories

        #region Search products

        public object SearchProducts(SearchProductModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/SearchProducts");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<SearchApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<Product>>(JsonConvert.SerializeObject(res.Result.products));
                    return new { Message = res.Message, Data = data, TotalPages = res.Result.TotalPages, TotalProducts = res.Result.TotalResults };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Search products

        #region Get Customer Order History

        public object GetOrderHistory(long customerID, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { CustomerID = customerID });

                var client = new RestClient(baseUrl + "api/Orders/GetOrderHistory");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<OrderHistoryBO>>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Customer Order History

        #region Get Customer Order For Payment

        public object GetPaymentOrders(string payToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { token = payToken });

                var client = new RestClient(baseUrl + "api/Orders/GetPaymentOrders");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                //request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<OrderHistoryBO>>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Customer Order For Payment

        #region Get Customer Pantry Lists

        public object GetCustomerPantryLists(long customerID, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { CustomerID = customerID });

                var client = new RestClient(baseUrl + "api/Products/GetCustomerPantry");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<CustomerPantryBO>>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Customer Pantry Lists

        #region Delete pantry/fav list

        public string DeletePantryList(long pantryListID, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new
                {
                    PantryListID = pantryListID
                });

                var client = new RestClient(baseUrl + "api/Products/DeleteFavoriteList");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Delete pantry/fav list

        #region Get User Profile

        public object GetUserProfile(long customerID, long userID, bool isRep, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { CustomerID = customerID, UserID = userID, IsRepUser = isRep });

                var client = new RestClient(baseUrl + "api/Account/GetUserProfile");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<UserProfileData>>(JsonConvert.SerializeObject(res.Result));
                    return new { Message = res.Message, Data = data[0] };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get User Profile

        #region Get product Detail

        public object GetProductDetail(long productID, long customerID, long userID, bool isRep, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { ProductID = productID, CustomerID = customerID, UserID = userID, IsRepUser = isRep });

                var client = new RestClient(baseUrl + "api/Products/GetProductDetailByID");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ProductDetailResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<ProductDetail>(JsonConvert.SerializeObject(res.Result)).product;
                    return new { Message = res.Message, Data = data };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get product Detail

        #region Send product Enquiry

        public string SendProductEnquiry(long productID, string comment, string token, long customerID, long userID)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new
                {
                    ProductID = productID,
                    CustomerID = customerID,
                    UserID = userID,
                    Comment = comment
                });

                var client = new RestClient(baseUrl + "api/Products/SendItemEnquiry");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", token);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }
                else
                {
                    return "Error while sending enquiry.";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Send product Enquiry

        #region Get Order details

        public object GetOrderDetails(OrderDetailsModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "/api/Orders/GetOrderDetails");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<OrderDetailsApiResult>(response.Content);
                    return new { Message = res.Message, Data = res.Result };
                }
                else
                {
                    return new
                    {
                        Message = "Internal error"
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Order details

        #region Re order

        public object ReOrder(ReorderModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "/api/Orders/AppendOrderToTempCartForReorder");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return new { Message = res.Message };
                }
                else
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return new
                    {
                        Message = res.Message
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Re order

        #region Add pantry list

        public object AddPantryList(AddPantryModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "/api/Products/AddUpdatePantry");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return new { Message = res.Message };
                }
                else
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return new
                    {
                        Message = res.Message
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Add pantry list

        #region Add item to pantry list

        public object AddItemToPantryList(AddPantryItemModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "/api/Products/AddItemsToPantry");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return new { Message = res.Message };
                }
                else
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return new
                    {
                        Message = res.Message
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Add item to pantry list

        #region Get child customers for a parent

        public List<ChildCustomerBO> GetChildCUstomers(GetChildBO model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "/api/Rep/GetRepCustomers");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ChildResponse>(response.Content);

                    if (res.Result != null)
                    {
                        return res.Result.ChildCustomers;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get child customers for a parent

        #region Delete pantry list item

        public string DeletePantryListItem(DeletePantryItemModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/DeleteItemFromPantry");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Delete pantry list item

        #region Save pantry sort Order

        public string SavePantrySortOrder(PantrySortBO model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/SetPantryItemsSortOrder");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res.Message;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Save pantry sort Order

        #region Get page Help text

        public object GetPageHelpText(string pageName)
        {
            try
            {
                var client = new RestClient(baseUrl + "api/Account/GetPageHelp?page=" + pageName);
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                // request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<PageHelpBO>(response.Content);
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get page Help text

        #region Get suggestive items for customer

        public object GetSuggestiveItems(SuggestiveItemsBO model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/GetSuggestiveItems");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<SuggestiveResponse>(response.Content);

                    return res.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get suggestive items for customer

        #region Apply Promo code

        public object ApplyPromoCode(PromoModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/ApplyCoupon");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<PromoResponse>(response.Content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Apply Promo code

        #region Get customer delivery addresses

        public object GetCustomerAddresses(string accessToken)
        {
            try
            {
                //var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Account/GetCustomerAddresses");
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                //request.AddParameter("application/json",);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<CustomerAddressResponse>(response.Content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get customer delivery addresses

        #region Get pickup locations

        public object GetPickupLocations(string accessToken)
        {
            try
            {
                //var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Account/GetPickupLocations");
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                //request.AddParameter("application/json",);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<PickupLocations>(response.Content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get pickup locations

        #region Get pickup locations

        public object GetPickupDeliveryDates(PickupDelDatesMoal model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Account/GetPickupDeliveryDates");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<PickupDatesResult>(response.Content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get pickup locations

        #region Add delivery address

        public object AddDeliveryAddress(AddAddressModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Account/AddDeliveryAddress");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    return JsonConvert.DeserializeObject<AddAddressResponse>(response.Content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Add delivery address

        #region Get whats new items

        public object GetWhatsNew(SearchProductModel model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Products/GetLatestSpecials");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<SearchApiResponse>(response.Content);
                    var data = JsonConvert.DeserializeObject<List<Product>>(JsonConvert.SerializeObject(res.Result.LatestSpecials));
                    return new { Message = res.Message, Data = data, TotalPages = res.Result.TotalPages, TotalProducts = res.Result.TotalResults };
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get whats new items

        #region Delete delivery address

        public object DeleteDeliveryAddress(long addressID, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { AddressID = addressID });

                var client = new RestClient(baseUrl + "api/Account/DeleteDeliveryAddress");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var res = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
                    return res;
                }
                else
                {
                    return new { Message = response.Content };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Delete delivery address

        #region Save / update recurring orders for a customer

        public object ManageRecurringOrders(RecurringOrdersBO model, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(model);

                var client = new RestClient(baseUrl + "api/Orders/ManageRecurringOrders");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                return JsonConvert.DeserializeObject<ApiResponse>(response.Content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Save / update recurring orders for a customer

        #region Get recurring order for customer

        public object GetRecurringOrder(long customerID, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { CustomerID = customerID });

                var client = new RestClient(baseUrl + "api/Orders/GetRecurringOrders");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                return JsonConvert.DeserializeObject<RecurrResult>(response.Content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get recurring order for customer

        #region Delete recurring order item

        public object DeleteRecurrOrderItem(long id, string accessToken)
        {
            try
            {
                var postData = JsonConvert.SerializeObject(new { RecurrProdID = id });

                var client = new RestClient(baseUrl + "api/Orders/DeleteRecurrOrderItem");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", accessToken);
                request.AddParameter("application/json", postData, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                return JsonConvert.DeserializeObject<ApiResponse>(response.Content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Delete recurring order item
    }
}