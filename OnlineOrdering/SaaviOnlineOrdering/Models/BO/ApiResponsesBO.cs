﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class CartCountResult
    {
        public int Count { get; set; }
        public double CartTotal { get; set; }
        public double TotalWithGST { get; set; }
        public double? CouponAmount { get; set; }
    }

    public class CartCountResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public CartCountResult Result { get; set; }
    }

    public class StripeKeysResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public StripeKeys Result { get; set; }
    }

    public class StripeKeys
    {
        public string PublicKey { get; set; } = "";
        public string ConnectedAccountID { get; set; } = "";
    }

    public class InsertTempResult
    {
        public int CartID { get; set; }
    }

    public class TempCartResponseBO
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public InsertTempResult Result { get; set; }
    }
}