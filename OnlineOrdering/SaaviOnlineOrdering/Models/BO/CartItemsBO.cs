﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class TempCartItem
    {
        public long ProductID { get; set; }
        public long CustomerID { get; set; }
        public long CartID { get; set; }
        public long CartItemID { get; set; }
        public string ProductCode { get; set; }
        public double Price { get; set; }
        public double CompanyPrice { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string CustomerCode { get; set; }
        public string ProductImage { get; set; }
        public string ProductThumbImage { get; set; }
        public long UOMID { get; set; }
        public string UnitName { get; set; }
        public string OrderUnitName { get; set; }
        public long OrderUnitId { get; set; }
        public string FilterName { get; set; }
        public long ProductWeight { get; set; }
        public string WeightName { get; set; }
        public string WeightDescription { get; set; }
        public string IsPieceOrWeight { get; set; }
        public bool? IsAvailable { get; set; }
        public bool? IsSpecial { get; set; }
        public bool? IsDiscountApplicable { get; set; }
        public bool? IsCountrywideRewards { get; set; }
        public bool? IsInvoiceComment { get; set; }
        public bool? IsUnloadComment { get; set; }
        public bool? IsNoPantry { get; set; }
        public string ExtDoc { get; set; }
        public string RunNo { get; set; }
        public bool IsGST { get; set; }
        public string PackagingSequence { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public double? SpecialPrice { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public double StockQuantity { get; set; }
        public double? Quantity { get; set; }
        public List<DynamicUOM> DynamicUOM { get; set; }
        public PriceBO Prices { get; set; }
        public double PriceTotal { get; set; }
        public double? MinOQ { get; set; }
        public double? MaxOQ { get; set; }
        public double? UnitsPerCarton { get; set; }
        public bool? BuyIn { get; set; }
        public long? ProdCommentID { get; set; }
        public long? LastOrderUOMID { get; set; }
        public bool IsDonationBox { get; set; }
    }

    public class ResultTempCartItems
    {
        public List<TempCartItem> CartItems { get; set; }
        public double CartTotal { get; set; }
        public double? CartTotalGST { get; set; }
        public bool? AuthPayment { get; set; }
        public double? MinCartValue { get; set; }
        public string Coupon { get; set; }
        public bool IsCouponApplied { get; set; }
        public double? CouponAmount { get; set; }
    }

    public class ApiResponseGetTempCartItems
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public ResultTempCartItems Result { get; set; }
    }

    public class PlaceOrderModel
    {
        public long CustomerID { get; set; }
        public long TempCartID { get; set; }
        public long UserID { get; set; }
        public long CartID { get; set; }
        public long CommentID { get; set; }
        public long AddressID { get; set; }
        public string PONumber { get; set; }
        public long OrderStatus { get; set; }
        public string Comment { get; set; }
        public DateTime OrderDate { get; set; }
        public string CutOffTime { get; set; }
        public string ExtDoc { get; set; }
        public string PackagingSequence { get; set; }
        public bool IsAutoOrdered { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string DeviceVersion { get; set; }
        public string DeviceModel { get; set; }
        public string AppVersion { get; set; }
        public bool SaveOrder { get; set; }
        public bool IsInvoiceComment { get; set; }
        public bool IsUnloadComment { get; set; }
        public long InvoiceCommentID { get; set; }
        public long UnloadCommentID { get; set; }
        public bool IsOrderPlpacedByRep { get; set; }
        public string RunNo { get; set; }
        public bool HasFreightCharges { get; set; }
        public bool HasNonDeliveryDayCharges { get; set; }

        public PayIntent PaymentDetails { get; set; }

        public bool IsCouponApplied { get; set; } = false;

        public string Coupon { get; set; } = "";
        public bool IsLeave { get; set; }
        public bool IsContactless { get; set; }
        public int PickupID { get; set; }
        public bool IsDelivery { get; set; }
        public string DeliveryType { get; set; }
        public string validateDate { get; set; }
        public double? CouponAmount { get; set; }
    }

    public class PayIntent
    {
        public string Status { get; set; }
        public string Id { get; set; }
        public long? Amount { get; set; }
        public string Currency { get; set; }
        public string CustomerId { get; set; }
        public bool Livemode { get; set; }
        public string ReceiptEmail { get; set; }
        public string ClientSecret { get; set; }
    }

    public class SavePastOrderPaymentModel
    {
        public double TotalAmount { get; set; }
        public List<long> OrderIds { get; set; }
        public string Token { get; set; }

        public PayIntent PaymentDetails { get; set; }
    }

    public class PaymentTokenModel
    {
        public double TotalAmount { get; set; }
        public List<long> OrderIds { get; set; }
    }
}