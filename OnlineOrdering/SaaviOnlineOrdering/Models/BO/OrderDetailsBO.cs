﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class OrderDetails
    {
        public long ProductID { get; set; }
        public long CustomerID { get; set; }
        public long CartID { get; set; }
        public long CartItemID { get; set; }
        public string ProductCode { get; set; }
        public double Price { get; set; }
        public double CompanyPrice { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string CustomerCode { get; set; }
        public string ProductImage { get; set; }
        public string ProductThumbImage { get; set; }
        public long UOMID { get; set; }
        public string UnitName { get; set; }
        public string OrderUnitName { get; set; }
        public long OrderUnitId { get; set; }
        public string FilterName { get; set; }
        public double? ProductWeight { get; set; }
        public string WeightDescription { get; set; }
        public string IsPieceOrWeight { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsDiscountApplicable { get; set; }
        public bool IsCountrywideRewards { get; set; }
        public bool IsInvoiceComment { get; set; }
        public bool IsUnloadComment { get; set; }
        public bool IsNoPantry { get; set; }
        public string ExtDoc { get; set; }
        public string RunNo { get; set; }
        public bool IsGST { get; set; }
        public string PackagingSequence { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public double? SpecialPrice { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public double? StockQuantity { get; set; }
        public double? Quantity { get; set; }
        public DynamicUOM DynamicUOM { get; set; }
        public PriceBO Prices { get; set; }
        public double? PriceTotal { get; set; }
        public double? UnitsPerCarton { get; set; }
        public long? ProdCommentID { get; set; }
    }

    public class OrderDetailsApiResult
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public List<OrderDetails> Result { get; set; }
    }
}