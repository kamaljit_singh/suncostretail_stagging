﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class ChildCustomerBO
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Alphacode { get; set; }
        public bool IsActive { get; set; }
        public bool OnHold { get; set; }
        public string ContactName { get; set; }
    }

    public class ChildResult
    {
        public int TotalResults { get; set; }
        public List<ChildCustomerBO> ChildCustomers { get; set; }
        public int TotalPages { get; set; }
    }

    public class ChildResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public ChildResult Result { get; set; }
    }
}