﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class CartCountModel
    {
        public long CartID { get; set; }
        public bool IsSavedOrder { get; set; }
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public bool IsRepUser { get; set; }
    }

    public class GetTempCartModel
    {
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public bool IsPlacedByRep { get; set; }
        public bool IsSavedOrder { get; set; }
        public long CartID { get; set; }
    }

    public class OrderCommentsBO
    {
        public long CommentID { get; set; }
        public long PCommentID { get; set; }
        public string CommentDescription { get; set; }
    }

    public class AddCommentModel
    {
        public long CustomerID { get; set; }
        public long ProductID { get; set; }
        public bool IsProduct { get; set; } = false;
        public bool IsInvoice { get; set; } = true;
        public string CommentDescription { get; set; }
    }

    public class PlaceOrderResult
    {
        public int OrderID { get; set; }
        public string MailStatus { get; set; }
    }

    public class GetInvoicesModel
    {
        public bool IsPdfListing { get; set; }
        public string Email { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime TillDate { get; set; }
        public long CustomerID { get; set; }
        public long UserID { get; set; }
    }

    public class GetInvoicesResult
    {
        public string InvoiceNumber { get; set; }
        public string PdfPath { get; set; }
        public string OrderDate { get; set; }
        public string CustomerName { get; set; }
    }

    public class SearchProductModel
    {
        public long CustomerID { get; set; }
        public long MainCategoryID { get; set; }
        public long SubCategoryID { get; set; }
        public long FilterID { get; set; }
        public string Searchtext { get; set; }
        public bool IsSpecial { get; set; }
        public long PageSize { get; set; }
        public long PageIndex { get; set; }
        public long UserID { get; set; }
        public bool IsRepUser { get; set; }
    }

    public class PriceBO
    {
        public double Price { get; set; } = 0;
        public bool? IsSpecial { get; set; } = false;
        public bool? IsPromotional { get; set; } = false;
        public double? CostPrice { get; set; } = 0;
        public long? UOMID { get; set; } = 0;
        public string UOMDesc { get; set; } = "";
        public double? QuantityPerUnit { get; set; } = 0;
    }

    public class GetChildBO
    {
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public string Searchtext { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public bool Debug { get; set; }
    }

    public class WeeklySalesBO
    {
        public Nullable<double> Week1Sales { get; set; } = 0;
        public Nullable<double> Week2Sales { get; set; } = 0;
        public Nullable<double> Week3Sales { get; set; } = 0;
        public Nullable<double> Week4Sales { get; set; } = 0;
        public Nullable<double> Week5Sales { get; set; } = 0;
        public Nullable<double> Week6Sales { get; set; } = 0;
        public Nullable<double> Week7Sales { get; set; } = 0;
        public Nullable<double> Week8Sales { get; set; } = 0;
        public Nullable<double> Week9Sales { get; set; } = 0;
        public Nullable<double> Week10Sales { get; set; } = 0;
        public Nullable<double> Week11Sales { get; set; } = 0;
        public Nullable<double> Week12Sales { get; set; } = 0;
        public Nullable<double> Week13Sales { get; set; } = 0;
        public Nullable<double> Week14Sales { get; set; } = 0;
        public Nullable<double> Week15Sales { get; set; } = 0;
        public Nullable<double> Week16Sales { get; set; } = 0;
    }

    public class OrderHistoryBO
    {
        public long CartID { get; set; }
        public string OrderNumber { get; set; }
        public long? CommentID { get; set; }
        public string Comment { get; set; }
        public int OrderStatus { get; set; }
        public string CheckOrderStatus { get; set; }
        public DateTime OrderDate { get; set; }
        public bool? IsAutoOrdered { get; set; }
        public double? Price { get; set; }
        public long DateFormatLong { get; set; }
        public string OrderStatusDesc { get; set; }
    }

    public class CustomerPantryBO
    {
        public long PantryListID { get; set; }
        public long CustomerID { get; set; }
        public string PantryListName { get; set; }
        public string PantryListType { get; set; }
        public bool? IsSynced { get; set; }
        public bool? IsCopy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class Messages
    {
        public int MessageID { get; set; }
        public object GroupID { get; set; }
        public int UserID { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class UserProfileData
    {
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public object DefaultPassword { get; set; }
        public string Phone { get; set; }
        public object Image { get; set; }
        public int CustomerID { get; set; }
        public string BusinessName { get; set; }
        public bool DebtorOnHold { get; set; }
        public List<Messages> Messages { get; set; }
        public string Salesrep { get; set; }
    }

    public class OrderDetailsModel
    {
        public long CustomerID { get; set; }
        public long OrderID { get; set; }
        public long UserID { get; set; }
        public bool IsOrderHistory { get; set; }
    }

    public class ReorderModel
    {
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public long OrderID { get; set; }
        public bool AppendToSavedOrder { get; set; }
        public List<long> Products { get; set; }
        public bool IsPlacedByRep { get; set; }
    }

    public class AddPantryModel
    {
        public long PantryListID { get; set; }
        public long CustomerID { get; set; }
        public string PantryListName { get; set; }
        public long CreatedBy { get; set; }
        public bool IsCreatedByRepUser { get; set; }
        public bool IsActive { get; set; }
        public string PantryListType { get; set; }
        public bool IsSynced { get; set; }
        public bool IsSorted { get; set; }
        public bool IsCopy { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class AddPantryItemModel
    {
        public long PantryItemID { get; set; }
        public long PantryListID { get; set; }
        public long ProductID { get; set; }
        public decimal Quantity { get; set; } = 0;
        public bool IsActive { get; set; }
        public bool IsCore { get; set; } = false;
        public long CustomerID { get; set; }
    }

    public class GetCommentsModel
    {
        public long CustomerID { get; set; }
        public long ProductID { get; set; }
        public bool IsProduct { get; set; }
    }

    public class DeletePantryItemModel
    {
        public long PantryItemID { get; set; }
        public long PantryListID { get; set; }
        public long CustomerID { get; set; }
    }

    public class PantrySortBO
    {
        public long PantryListID { get; set; }
        public List<long> ProductID { get; set; }
    }
    public class CancelRecurringpayment
    {
        public long CustomerID { get; set; }
        public string PlanID { get; set; }
        public string DeviceType { get; set; }
        
    }
    public class SubscriptionDetail
    {

        public int planid { get; set; }
        public int newplanid { get; set; }
        public string paymentmethod { get; set; }
        public Int64 customerid { get; set; }
        public double? price { get; set; }
        public string devicetype { get; set; }
    }
    public class GetStipePayTokenModel
    {
        public double PaymentTotal { get; set; }
        public string CurrencyCode { get; set; }
        public bool OnlyAuthorize { get; set; }
        public long TempCartID { get; set; }
        public string DeviceType { get; set; }
    }

    public class GetStipePayTokenForPastOrdersModel
    {
        public double PaymentTotal { get; set; }
        public string CurrencyCode { get; set; }
        public bool OnlyAuthorize { get; set; }
        public List<long> CartIDs { get; set; }
        public string DeviceType { get; set; }
    }

    public class PageHelpBO
    {
        public int ID { get; set; }
        public string PageName { get; set; }
        public string Description { get; set; }
        public int SortOrder { get; set; }
    }

    public class SuggestiveItemsBO
    {
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public long CartID { get; set; }
    }

    public class PromoModel
    {
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public long CartID { get; set; }
        public string Coupon { get; set; }
    }

    public class RecurringOrdersBO
    {
        public int ID { get; set; }
        public long CartID { get; set; }
        public long CustomerID { get; set; }
        public int? AddressID { get; set; } = 0;
        public int? PickupID { get; set; } = 0;
        public string Frequency { get; set; }
        public bool? IsSuspended { get; set; }
        public string OrderType { get; set; }
        public DateTime StartDate { get; set; }
        public List<RecurrProducts> RecurrProds { get; set; }
    }

    public partial class RecurrResult
    {
        public long ResponseCode { get; set; }
        public string Message { get; set; }
        public ResultRe Result { get; set; }
    }

    public partial class ResultRe
    {
        public RecurringOrder RecurringOrder { get; set; }
        public List<GetAllProductsBO> AllProducts { get; set; }
    }

    public partial class RecurringOrder
    {
        public long Id { get; set; }
        public long CartId { get; set; }
        public long CustomerId { get; set; }
        public long AddressId { get; set; }
        public long PickupId { get; set; }
        public string Frequency { get; set; }
        public bool IsSuspended { get; set; }
        public string OrderType { get; set; }
        public DateTime StartDate { get; set; }
        public List<RecurrProducts> RecurrProds { get; set; }
    }

    public class RecurrProducts
    {
        public int RecurrProdID { get; set; }
        public long? ProductID { get; set; }
        public double? Quantity { get; set; }
        public double? Price { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
    }

    public class GetAllProductsBO
    {
        public long ProductID { get; set; }
        public double? Price { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public double? Quantity { get; set; } = 1;
    }

    public class DeleteRecurrItemBO
    {
        public int RecurrProdID { get; set; }
    }
}