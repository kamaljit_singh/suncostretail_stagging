﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class GetPantryBO
    {
        public long PantryListID { get; set; } = 0;
        public int PageSize { get; set; } = 0;
        public int PageIndex { get; set; } = 0;
        public string SortBy { get; set; } = "";
        public long FilterID { get; set; } = 0;
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public string Searchtext { get; set; } = "";
        public bool IsRepUser { get; set; } = false;
        public bool ShowAll { get; set; } = true;
        public bool IsWebView { get; set; } = false;
        public string AccessToken { get; set; }
    }
}