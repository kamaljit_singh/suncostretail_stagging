﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class SubCategory
    {
        public long MCategoryId { get; set; }
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        public bool? IsSpecialCategory { get; set; }
        public string CategoryCode { get; set; }
    }

    public class ProductCategoriesBO
    {
        public long MCategoryId { get; set; }
        public string MCategoryName { get; set; }
        public bool IsActive { get; set; }
        public string MCategoryCode { get; set; }
        public List<SubCategory> SubCategories { get; set; }
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}