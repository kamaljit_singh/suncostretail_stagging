﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class CartItem
    {
        public long CartItemID { get; set; }
        public long CartID { get; set; }
        public long ProductID { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double ProductWeight { get; set; }
        public string WeightName { get; set; }
        public string WeightDescription { get; set; }
        public string IsPieceOrWeight { get; set; }
        public bool IsSpecialPrice { get; set; }
        public long CommentID { get; set; }
        public long UnitId { get; set; }
        public bool IsGstApplicable { get; set; }
        public double ItemPricePerUnit { get; set; }
        public double OrderedQuantity { get; set; }
        public double RecievedQuantity { get; set; }
        public double UnitsPerCarton { get; set; }
        public double BasePrice { get; set; }
        public bool IsDiscountApplicable { get; set; }
        public bool IsBoxDiscount { get; set; }
        public string PriceType { get; set; }
        public bool IsNoPantry { get; set; }
    }

    public class InsertUpdateTempCartBO
    {
        public long CartID { get; set; }
        public long CustomerID { get; set; }
        public bool IsOrderPlpacedByRep { get; set; }
        public long RepUserID { get; set; }
        public string RunNo { get; set; }
        public string CommentLine { get; set; }
        public string PackagingSequence { get; set; }
        public CartItem CartItem { get; set; }
        public bool IsSavedOrder { get; set; }
        public string Warehouse { get; set; }
        public long AddressID { get; set; }
    }
}