﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class WeeklySales
    {
        public double? Week1Sales { get; set; }
        public double? Week2Sales { get; set; }
        public double? Week3Sales { get; set; }
        public double? Week4Sales { get; set; }
        public double? Week5Sales { get; set; }
        public double? Week6Sales { get; set; }
        public double? Week7Sales { get; set; }
        public double? Week8Sales { get; set; }
        public double? Week9Sales { get; set; }
        public double? Week10Sales { get; set; }
        public double? Week11Sales { get; set; }
        public double? Week12Sales { get; set; }
        public double? Week13Sales { get; set; }
        public double? Week14Sales { get; set; }
        public double? Week15Sales { get; set; }
        public double? Week16Sales { get; set; }
    }

    public class Prices
    {
        public double Price { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsPromotional { get; set; }
        public double CostPrice { get; set; }
        public int UOMID { get; set; }
        public string UOMDesc { get; set; }
        public double QuantityPerUnit { get; set; }
    }

    public class ProductD
    {
        public long ProductID { get; set; }
        public long PantryListID { get; set; }
        public long CategoryID { get; set; }
        public long FilterID { get; set; }
        public long UOMID { get; set; }
        public long ProductCount { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string MainCategoryName { get; set; }
        public string FilterName { get; set; }
        public string UOMDesc { get; set; }
        public double? Price { get; set; }

        // public double? CompanyPrice { get; set; }
        public double? SpecialPrice { get; set; }

        public string Description { get; set; }
        public string ProductCode { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageThumb { get; set; }
        public double? Quantity { get; set; }
        public bool IsActive { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsManagerApproved { get; set; }
        public bool IsGST { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public bool IsSpecialCategory { get; set; }
        public bool PiecesAndWeight { get; set; }
        public double? StockQuantity { get; set; }
        public WeeklySales WeeklySales { get; set; }
        public bool IsInPantry { get; set; }
        public bool IsInCart { get; set; }
        public List<DynamicUOM> DynamicUOM { get; set; }
        public Prices Prices { get; set; }
        public double? UnitsPerCarton { get; set; }
        public List<ProductImage> ProductImages { get; set; }
        public bool IsCountrywideRewards { get; set; }
        public double? MinOQ { get; set; }
        public double? MaxOQ { get; set; }
        public bool BuyIn { get; set; }
        public long? LastOrderUOMID { get; set; }
        public string ProductFeature { get; set; }
    }

    public class ProductDetail
    {
        public ProductD product { get; set; }
    }

    public class ProductDetailResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public ProductDetail Result { get; set; }
    }
}