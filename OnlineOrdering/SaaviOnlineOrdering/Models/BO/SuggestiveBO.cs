﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class LatestSpecial
    {
        public int ProductID { get; set; }
        public int PantryListID { get; set; }
        public int CategoryID { get; set; }
        public int FilterID { get; set; }
        public int UOMID { get; set; }
        public int ProductCount { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string MainCategoryName { get; set; }
        public string FilterName { get; set; }
        public string UOMDesc { get; set; }
        public object ProductComment { get; set; }
        public double Price { get; set; }
        public double CompanyPrice { get; set; }
        public double SpecialPrice { get; set; }
        public string Description { get; set; }
        public string ProductCode { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageThumb { get; set; }
        public double Quantity { get; set; }
        public bool IsActive { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsManagerApproved { get; set; }
        public bool IsGST { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public bool IsSpecialCategory { get; set; }
        public bool PiecesAndWeight { get; set; }
        public double StockQuantity { get; set; }
        public object WeeklySales { get; set; }
        public bool IsInPantry { get; set; }
        public bool IsInCart { get; set; }
        public object DynamicUOM { get; set; }
        public Prices Prices { get; set; }
        public double UnitsPerCarton { get; set; }
        public List<ProductImage> ProductImages { get; set; }
        public bool IsCountrywideRewards { get; set; }
        public double MinOQ { get; set; }
        public double MaxOQ { get; set; }
        public bool BuyIn { get; set; }
        public bool IsStatusIN { get; set; }
        public int LastOrderUOMID { get; set; }
        public bool SellBelowCost { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string Feature1 { get; set; }
        public string Feature2 { get; set; }
        public string Feature3 { get; set; }
        public bool IsNew { get; set; }
        public bool IsOnSale { get; set; }
        public bool IsBackSoon { get; set; }
        public object Barcode { get; set; }
        public string ShareURL { get; set; }
        public string ProductFeature { get; set; }
        public int PantryListItemID { get; set; }
    }

    public class SuggestiveBO
    {
        public List<LatestSpecial> Specials { get; set; }
        public List<Product> SuggestiveItems { get; set; }
    }

    public class SuggestiveResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public SuggestiveBO Result { get; set; }
    }

    public partial class PromoResponse
    {
        public long ResponseCode { get; set; }
        public string Message { get; set; }
        public PromoResult Result { get; set; }
    }

    public partial class PromoResult
    {
        public string Message { get; set; }
        public long Count { get; set; }
        public double? CartTotal { get; set; }
        public double? TotalWithGst { get; set; }
        public double? TotalAmountWithDiscount { get; set; }
    }

    public partial class CustomerAddressResponse
    {
        public long ResponseCode { get; set; }
        public string Message { get; set; }
        public List<CustAddresses> Result { get; set; }
    }

    public partial class CustAddresses
    {
        public long CustomerId { get; set; }
        public long AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string AddressCode { get; set; }
        public long StateId { get; set; }
        public string StateName { get; set; }
        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public long PostCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public object Err { get; set; }
        public object Warehouse { get; set; }
        public string Suburb { get; set; }
        public string ContactName { get; set; }
    }

    public class PickupLocations
    {
        public long ResponseCode { get; set; }
        public string Message { get; set; }
        public PickupList Result { get; set; }
    }

    public class PickupList
    {
        public List<Pickup> Pickups { get; set; }
    }

    public class Pickup
    {
        public int ID { get; set; }
        public string PickupName { get; set; }
        public string PickupAddress { get; set; }
    }

    public class PickupDelDatesMoal
    {
        public int PickupID { get; set; }
        public bool IsDelivery { get; set; }
        public long AddressID { get; set; }
    }

    public partial class PickupDatesResult
    {
        public long ResponseCode { get; set; }
        public string Message { get; set; }
        public PickupDates Result { get; set; }
    }

    public partial class PickupDates
    {
        public List<string> OrderDates { get; set; }
        public List<long> OrderDate2 { get; set; }
        public List<string> PermittedDays { get; set; }
        public bool SundayOrdering { get; set; }
        public List<string> OrderDatesLong { get; set; }
        public bool LimitOrdersTo1Month { get; set; }
        public bool AllowPickup { get; set; }
        public bool SlotsByDate { get; set; }
        public List<Slot> Slots { get; set; }
    }

    public partial class Slot
    {
        public string DeliveryDate { get; set; }
        public long Slots { get; set; }
    }

    public class AddAddressModel
    {
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string StreetAddress { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public long CustomerID { get; set; }
    }

    public partial class AddAddressResponse
    {
        public long ResponseCode { get; set; }
        public string Message { get; set; }
        public AddressRes Result { get; set; }
    }

    public partial class AddressRes
    {
        public long AddressId { get; set; }
    }
}