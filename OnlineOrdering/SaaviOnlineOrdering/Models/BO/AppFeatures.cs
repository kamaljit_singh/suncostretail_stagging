﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class MainFeatures
    {
        public bool? IsNonFoodVersion { get; set; } // to be used
        public bool? IsAdvancedPantry { get; set; }
        public bool? IsAddSpecialPriceFromPantry { get; set; }
        public bool? IsRepProductBrowsing { get; set; }
        public bool? IsHighlightRewardItem { get; set; } // to be used
        public bool? IsShowOrderStatus { get; set; } // to be used
        public bool? IsShowTermConditionsPopup { get; set; } // to be used // Login page
        public bool? IsShowProductClasses { get; set; } // to be used
        public bool? IsShowOnlyStockItems { get; set; }
        public bool? IsMultipleAddresses { get; set; } // to be used
        public bool? IsShowContactDetails { get; set; } // to be used
        public bool? IsLiquorControlPopup { get; set; } // to be used // Login Page
        public bool? IsItemEnquiryPopup { get; set; } // to be used
        public bool? IsShowProductCost { get; set; }
        public bool? IsShowProductHistory { get; set; } // to be used
        public bool? IsPDFSpecialProducts { get; set; }
        public bool? IsPictureViewEnabled { get; set; } // to be used
        public bool? IsShowFutureDate { get; set; } // to be used
        public bool? IsEnableRepToAddSpecialPrice { get; set; }
        public bool? IsShowPriceChange { get; set; } = false;
        public string PrimaryAppColor { get; set; }
        public string SecondaryAppColor { get; set; }
        public string PrimaryAppColorRGB { get; set; }
        public string SecondaryAppColorRGB { get; set; }
        public bool? ShowWalkthrough { get; set; }
        public bool? IsDatePickerEnabled { get; set; }
        public string Currency { get; set; }
        public bool? IsShowSubCategory { get; set; }
        public bool? IsParentChild { get; set; }
        public bool? ReturnLastOrderUOM { get; set; }
        public bool? IsHandleBackOrders { get; set; }
        public bool? IsStripePayment { get; set; }
        public bool? ShowCategory { get; set; }
    }
}