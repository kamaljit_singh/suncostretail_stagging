﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class ProductFiltersBO
    {
        public long FilterID { get; set; }
        public string FilterName { get; set; }
    }
}