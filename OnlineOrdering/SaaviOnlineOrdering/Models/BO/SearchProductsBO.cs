﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class ProductImage
    {
        public string ImageName { get; set; }
    }

    public class Product
    {
        public long ProductID { get; set; }
        public long PantryListID { get; set; }
        public long CategoryID { get; set; }
        public long FilterID { get; set; }
        public long UOMID { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string MainCategoryName { get; set; }
        public string FilterName { get; set; }
        public string UOMDesc { get; set; }
        public double? Price { get; set; }
        public double? CompanyPrice { get; set; }
        public double? SpecialPrice { get; set; }
        public string Description { get; set; }
        public string ProductCode { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageThumb { get; set; }
        public string ProductFeature { get; set; }
        public double? Quantity { get; set; }
        public bool IsActive { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsManagerApproved { get; set; }
        public bool IsGST { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public bool IsSpecialCategory { get; set; }
        public bool PiecesAndWeight { get; set; }
        public double? StockQuantity { get; set; }
        public WeeklySalesBO WeeklySales { get; set; }
        public bool IsInPantry { get; set; }
        public bool IsInCart { get; set; }
        public List<DynamicUOM> DynamicUOM { get; set; }
        public PriceBO Prices { get; set; }
        public double? UnitsPerCarton { get; set; }
        public List<ProductImage> ProductImages { get; set; }
        public bool IsCountrywideRewards { get; set; }
        public double? MinOQ { get; set; }
        public double? MaxOQ { get; set; }
        public bool BuyIn { get; set; }
        public long? LastOrderUOMID { get; set; }
    }

    public class SearchProductBO
    {
        public List<Product> products { get; set; }
        public List<Product> LatestSpecials { get; set; }
        public int TotalResults { get; set; }
        public int TotalPages { get; set; }
    }

    public class SearchApiResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public SearchProductBO Result { get; set; }
    }
}