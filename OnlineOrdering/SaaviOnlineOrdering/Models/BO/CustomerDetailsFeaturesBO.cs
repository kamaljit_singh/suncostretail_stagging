﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class CustomerFeatures
    {
        public long ID { get; set; }
        public long UserID { get; set; }
        public bool? IsSearchProduct { get; set; }
        public bool? IsLatestSpecials { get; set; }
        public bool? IsCheckDelivery { get; set; }
        public bool? IsCheckDeliveryDetail { get; set; }
        public bool? IsCheckDeliveryComment { get; set; }
        public bool? IsNoticeBoard { get; set; }
        public bool? IsInvoice { get; set; }
        public bool? IsOrderHistory { get; set; }
        public bool? IsPantryList { get; set; }
        public bool? IsPantrySorting { get; set; }
        public bool? IsPurchaseHistory { get; set; }
        public bool? IsNextDeliveryDate { get; set; }
        public bool? IsShowPrice { get; set; }
        public bool? IsShowStock { get; set; }
        public bool? IsSundayOrdering { get; set; }
        public bool? IsPONumber { get; set; }
        public bool? IsOrderDate { get; set; }
        public bool? IsPDF { get; set; }
        public bool? IsTargetMarketing { get; set; }
        public bool? IsManagement { get; set; }
        public bool? IsDynamicUOM { get; set; }
        public bool? IsShelfFeature { get; set; }
        public bool? IsSaveOrder { get; set; }
        public bool? IsAutoOrdering { get; set; }
        public bool? IsCoreItems { get; set; }
        public bool? IsSuggestiveSell { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsAllowUserToAddPantry { get; set; }
        public bool? IsBarCodeScanningEnabled { get; set; }
        public bool? IsShowBrand { get; set; }
        public bool? IsShowSupplier { get; set; }
        public bool? IsShowProductLongDetail { get; set; }
        public bool? IsDisplayUnitsPerCarton { get; set; }
        public bool? IsFreightChargeApplicable { get; set; }
        public bool? IsBrowsingEnabledForHoldCust { get; set; }
        public bool? IsAddItemToDefaultPantry { get; set; }
        public bool? IsAddItemToPantry { get; set; }
        public bool? IsCopyPantryEnabled { get; set; }
        public bool? IsHighlightStock { get; set; }
        public bool? IsStripePayment { get; set; }
        public bool? IsNonDeliveryDayOrdering { get; set; }
        public bool? IsProductComment { get; set; }
        public bool? IsSpecialProductRequest { get; set; }
        public bool? IsMOQ { get; set; }
        public bool? IsNoBackorder { get; set; }
        public bool? IsMaxOQ { get; set; }
        public bool? IsInvoicePdf { get; set; }
        public bool? IsAllowDecimal { get; set; }
    }

    public class CustomerDetail
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long CustomerID { get; set; }
        public string ContactName { get; set; }
        public string CustomerName { get; set; }
        public bool IsActive { get; set; }
        public string ProfileImage { get; set; }
        public bool IsRep { get; set; }
        public bool IsSundayrdering { get; set; }
        public bool DebtorOnHold { get; set; }
        public string ABN { get; set; }
        public string AlphaCode { get; set; }
        public string Email { get; set; }
        public string TermconditionMessage { get; set; }
        public bool IsRetailUser { get; set; }
    }

    public class CustomerDetailOriginal
    {
        public long UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long CustomerID { get; set; }
        public string ContactName { get; set; }
        public string CustomerName { get; set; }
        public bool IsActive { get; set; }
        public string ProfileImage { get; set; }
        public bool IsRep { get; set; }
        public bool IsSundayrdering { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string AccountType { get; set; }
        public bool DebtorOnHold { get; set; }
        public string ABN { get; set; }
        public string AlphaCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string PostCode { get; set; }
        public string SalesmanCode { get; set; }
        public string Suburb { get; set; }
        public string StateName { get; set; }
        public string Warehouse { get; set; }
        public string CreditLimit { get; set; }
        public string CurrentBalance { get; set; }
        public string OverDueBalance { get; set; }
        public string TotalBalance { get; set; }
        public string YTDSales { get; set; }
        public string MTDSales { get; set; }
        public string PrevMonth { get; set; }
        public string PriceCode { get; set; }
        public string BalancePeriod1 { get; set; }
        public string BalancePeriod2 { get; set; }
        public string BalancePeriod3 { get; set; }
        public string TermconditionMessage { get; set; }
    }

    public class ContactDetail
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public object Phone2 { get; set; }
    }

    public class DeliveryCharge
    {
        public double? OrderValue { get; set; }
        public double? DeliveryFee { get; set; }
        public double? NonDeliveryDaysFee { get; set; }
        public string FrightMessage { get; set; }
        public string NonDeliveryDaysMessage { get; set; }
    }

    public class PDFProduct
    {
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageThumb { get; set; }
        public long UOMID { get; set; }
        public string UOMDesc { get; set; }
        public long FilterID { get; set; }
        public string FilterName { get; set; }
        public double Price { get; set; }
        public double CompanyPrice { get; set; }
        public double SpecialPrice { get; set; }
        public bool IsActive { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsAvailable { get; set; }
        public bool PiecesAndWeight { get; set; }
        public double Quantity { get; set; }
    }

    public class GroupDetail
    {
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public string PdfFile { get; set; }
        public List<PDFProduct> PDFProducts { get; set; }
    }

    public class PDFDetails
    {
        public bool IsGrouped { get; set; }
        public string PdfFile { get; set; }
        public object PDFProducts { get; set; }
        //public List<GroupDetail> GroupDetails { get; set; }
    }

    public class Result
    {
        public CustomerFeatures customerFeatures { get; set; }
        public List<CustomerDetail> customerDetails { get; set; }
        public List<ContactDetail> ContactDetails { get; set; }
        public List<DeliveryCharge> DeliveryCharges { get; set; }
        public PDFDetails PDFDetails { get; set; }
    }

    public class CustomerDetailsResult
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public Result Result { get; set; }
    }
}