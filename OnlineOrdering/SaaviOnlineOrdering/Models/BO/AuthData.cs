﻿using CommonFunctions;
using SaaviOnlineOrdering.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class AuthData : IAuthInterface
    {
        public string Token
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("AcessToken").Value; }
        }

        public string BusinessName
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("BusinessName").Value ?? ""; }
        }

        public string Name
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("Name").Value ?? ""; }
        }

        public string Role
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("Role").Value; }
        }

        public long UserID
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("UserID").Value.To<long>(); }
        }

        public long CustomerID
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("CustomerID").Value.To<long>(); }
        }

        public string MainFeatures
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("MainFeatures").Value; }
        }

        public string CustomerFeatures
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("CustomerFeatures").Value; }
        }

        public string DeliveryDetails
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("DeliveryCharges").Value; }
        }

        public string DefaultInfo
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("DefaultInfo").Value; }
        }

        public string PdfDetails
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("PDFDetails").Value; }
        }

        public string IsRetail
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("IsRetail").Value; }
        }

        public string IsGuest
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("IsGuest").Value; }
        }
    }
}