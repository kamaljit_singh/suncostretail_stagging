﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Error { get; set; }
        public bool IsGuest { get; set; } = false;
    }

    public class RegisterViewModel
    {
        public string UserName { get; set; }
        public string BusinessName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Phone { get; set; }
        public byte UserTypeID { get; set; } = 4;
        public string DeviceToken { get; set; } = "Online Web Ordering";
        public string DeviceType { get; set; } = "WEB";
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string Message { get; set; }
        public bool ExistingAccount { get; set; } = false;
        public string StreetAddress { get; set; } = "";
        public string StreetAddress2 { get; set; } = "";
        public string ABN { get; set; } = "";
        public string Suburb { get; set; } = "";
        public string Postcode { get; set; } = "";
        public bool GoogleMap { get; set; } = false;
        public string GoogleAPI { get; set; }
    }

    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string BusinessName { get; set; }
        public string Name { get; set; }
        public string role { get; set; }
        public string userID { get; set; }
        public string customerID { get; set; }
    }

    public class ApiResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }

    public class LoginResponse
    {
        public string Message { get; set; }
        public string DefaultInfo { get; set; }
        public string MainFeature { get; set; }
        public string CustDetails { get; set; }
        public PDFDetails PDFDetails { get; set; }
        public string CustomerFeatures { get; set; }
    }
}