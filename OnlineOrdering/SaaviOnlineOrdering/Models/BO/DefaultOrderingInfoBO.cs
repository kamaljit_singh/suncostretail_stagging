﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.BO
{
    public class Intro
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool NotificationsEnabled { get; set; }
        public string OrderCutOff { get; set; }
        public string Company { get; set; }
    }

    public class DOResult
    {
        public Intro intro { get; set; }
        public List<string> orderDates { get; set; }

        public List<string> orderDate2 { get; set; }
        public List<string> permittedDays { get; set; }

        public bool sundayOrdering { get; set; }
        public string customerPhone { get; set; }
        public List<string> orderDatesLong { get; set; }
    }

    public class DefaultOrderingInfoResult
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public DOResult Result { get; set; }
    }
}