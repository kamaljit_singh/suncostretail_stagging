﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaviOnlineOrdering.Models.Interface
{
    public interface IAuthInterface
    {
        string Token { get; }
        string BusinessName { get; }
        string Name { get; }
        string Role { get; }
        long UserID { get; }
        long CustomerID { get; }
        string MainFeatures { get; }
        string CustomerFeatures { get; }
        string DeliveryDetails { get; }
        string DefaultInfo { get; }
        string PdfDetails { get; }
        string IsRetail { get; }
        string IsGuest { get; }
    }
}