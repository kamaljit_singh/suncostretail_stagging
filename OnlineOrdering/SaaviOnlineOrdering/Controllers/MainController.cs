﻿using AutoMapper;
using Newtonsoft.Json;
using SaaviOnlineOrdering.Models.BO;
using SaaviOnlineOrdering.Models.DAL;
using SaaviOnlineOrdering.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace SaaviOnlineOrdering.Controllers
{
    [Authorize]
    public class MainController : Controller
    {
        private readonly IAuthInterface _authData;
        private readonly ProductsDA _da;

        public MainController()
        {
            _authData = new AuthData();
            _da = new ProductsDA();
        }

        #region Get Views

        public ActionResult DefaultPantry(long? id)
        {
            if (id != null)
            {
                User.AddUpdateClaim("CustomerID", id.ToString());
            }
            return View();
        }

        public ActionResult Cart()
        {
            if (_authData.IsGuest == "Yes")
            {
                return RedirectToAction("SearchProducts", "Main");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Invoices()
        {
            return View();
            //if (_authData.IsRetail == "Yes")
            //{
            //    return RedirectToAction("SearchProducts", "Main");
            //}
            //else
            //{
            //    return View();
            //}
        }

        public ActionResult SearchProducts()
        {
            return View();
            //if (_authData.IsRetail == "Yes")
            //{
            //    return RedirectToAction("DefaultPantry", "Main");
            //}
            //else
            //{
            //    return View();
            //}
        }

        public ActionResult OrderHistory()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Payment(string token)
        {
            ViewBag.Token = token;
            return View();
        }

        public ActionResult FavoriteList()
        {
            return View();
        }

        public ActionResult UserProfile() => View();

        public ActionResult ProductDetail() => View();

        public ActionResult WhatsNew() => View();

        public ActionResult OrderDetails()
        {
            return View();
            //if (_authData.IsRetail == "Yes")
            //{
            //    return RedirectToAction("SearchProducts", "Main");
            //}
            //else
            //{
            //    return View();
            //}
        }

        public ActionResult RecurringOrders()
        {
            return View();
        }

        public ActionResult ChooseCustomer(string id)
        {
            var cID = Convert.ToInt64(id);
            var childBO = new GetChildBO()
            {
                CustomerID = cID,
                Searchtext = "",
                PageIndex = 0,
                PageSize = 100,
                Debug = true,
                UserID = _authData.UserID
            };

            var childCustomers = _da.GetChildCUstomers(childBO, _authData.Token);

            if (childCustomers == null)
            {
                return View("DefaultPantry");
            }
            else
            {
                ViewBag.Customers = childCustomers;
                return View();
            }
        }

        #endregion Get Views

        #region Get product filters

        public JsonResult GetFilters()
        {
            //User.AddUpdateClaim("CustomerID", "101");
            var res = _da.GetProductFilters();
            var mainFeatures = JsonConvert.DeserializeObject<MainFeatures>(_authData.MainFeatures);
            var custFeatures = JsonConvert.DeserializeObject<CustomerFeatures>(_authData.CustomerFeatures);
            return Json(new
            {
                Filters = res,
                PantryLists = _da.GetCustomerPantryLists(_authData.CustomerID, _authData.Token)
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion Get product filters

        #region Get Managed Features

        public JsonResult GetManagedFeatures()
        {
            var mainFeatures = JsonConvert.DeserializeObject<MainFeatures>(_authData.MainFeatures);
            var custFeatures = JsonConvert.DeserializeObject<CustomerFeatures>(_authData.CustomerFeatures);

            return Json(new
            {
                ShowBrand = custFeatures.IsShowBrand ?? false,
                ShowSupplier = custFeatures.IsShowSupplier ?? false,
                ShowPrice = custFeatures.IsShowPrice ?? false,
                LongDetail = custFeatures.IsShowProductLongDetail ?? false,
                AddToDefaultPantry = custFeatures.IsAddItemToDefaultPantry ?? false,
                HighlightStock = custFeatures.IsHighlightStock ?? false,
                PictureView = mainFeatures.IsPictureViewEnabled ?? false,
                ShowFilters = mainFeatures.IsShowProductClasses ?? false,
                ItemEnquiry = mainFeatures.IsItemEnquiryPopup ?? false,
                ShowHistory = mainFeatures.IsShowProductHistory ?? false,
                HighlightRewardItems = mainFeatures.IsHighlightRewardItem ?? false,
                NonFoodVersion = mainFeatures.IsNonFoodVersion ?? false,
                ShowFavoriteIcon = HelperClass.ShowFavoriteIcon,
                ShowUnitsPerCarton = custFeatures.IsDisplayUnitsPerCarton ?? false,
                ShowCountOnAll = HelperClass.ShowCountOnAll,
                ProductLongDetail = custFeatures.IsShowProductLongDetail ?? false,
                ShowStock = custFeatures.IsShowStock ?? false,
                ShowOrderStatus = mainFeatures.IsShowOrderStatus ?? false,
                SearchProducts = custFeatures.IsSearchProduct ?? false,
                ShowInvoices = custFeatures.IsInvoice ?? false,
                ShowOrderHistory = custFeatures.IsOrderHistory ?? false,
                AddPantryList = custFeatures.IsAllowUserToAddPantry ?? false,
                AddItemToPantryList = custFeatures.IsAddItemToPantry ?? false,
                AddItemToDefaultPantry = custFeatures.IsAddItemToDefaultPantry ?? false,
                SaveOrder = custFeatures.IsSaveOrder ?? false,
                DatePicker = mainFeatures.IsDatePickerEnabled ?? false,
                IsDecimal = custFeatures.IsAllowDecimal ?? false,
                Currency = mainFeatures.Currency ?? "$",
                ShowSubCategory = mainFeatures.IsShowSubCategory ?? false,
                ShowBIPopUP = HelperClass.ShowBIPopup,
                MarketText = HelperClass.MarketText,
                CopyPantry = custFeatures.IsCopyPantryEnabled ?? false,
                ReturnLastUOM = mainFeatures.ReturnLastOrderUOM ?? false,
                BackOrder = mainFeatures.IsHandleBackOrders ?? false,
                PantrySorting = custFeatures.IsPantrySorting ?? false,
                IsStripePayment = mainFeatures.IsStripePayment ?? false,
                ShowCategory = mainFeatures.ShowCategory ?? false,
                IsGuest = _authData.IsGuest == "Yes" ? true : false,
                PantryDelete = HelperClass.PantryDelete,
                IsStripePaymentCustomer = custFeatures.IsStripePayment ?? false,
                IstripePaymentAdmin = mainFeatures.IsStripePayment ?? false
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion Get Managed Features

        #region Get pantry list items

        [HttpPost]
        public JsonResult GetPantryListItems(int pageSize, int pageIndex, bool showAll, long pantryListID, string search, long filterID)
        {
            try
            {
                GetPantryBO model = new GetPantryBO();
                model.PantryListID = pantryListID;
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;
                model.AccessToken = _authData.Token;
                model.PageIndex = pageIndex;
                model.PageSize = pageSize;
                model.ShowAll = showAll;
                model.IsWebView = true;
                model.Searchtext = search;
                model.FilterID = filterID;

                var res = _da.GetPantryListItems(model);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Get pantry list items

        #region Insert item in temp cart

        [HttpPost]
        public JsonResult InsertUpdateTempCart(InsertUpdateTempCartBO model)
        {
            try
            {
                model.RepUserID = _authData.UserID;
                model.CustomerID = _authData.CustomerID;
                model.IsOrderPlpacedByRep = false;
                model.IsSavedOrder = false;

                var res = _da.InsertUpdateTempCart(model, _authData.Token);

                if (res.Message == "Success")
                {
                    var cartModel = new CartCountModel();

                    cartModel.CartID = res.Result.CartID;
                    cartModel.UserID = _authData.UserID;
                    cartModel.CustomerID = _authData.CustomerID;
                    cartModel.IsRepUser = false;
                    cartModel.IsSavedOrder = false;

                    var cartRes = _da.GetCartCount(cartModel, _authData.Token);

                    return Json(new
                    {
                        Result = true,
                        CartID = res.Result.CartID,
                        CartCount = cartRes.Result.Count,
                        CartTotal = cartRes.Result.CartTotal
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        Result = false,
                        Message = res.Message
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Insert item in temp cart

        #region Get cart count and Cart total

        public JsonResult GetCartCount()
        {
            try
            {
                var cartModel = new CartCountModel();

                cartModel.CartID = 0;
                cartModel.UserID = _authData.UserID;
                cartModel.CustomerID = _authData.CustomerID;
                cartModel.IsRepUser = false;
                cartModel.IsSavedOrder = false;

                var cartRes = _da.GetCartCount(cartModel, _authData.Token);

                return Json(new
                {
                    CartCount = cartRes.Result.Count,
                    Total = cartRes.Result.CartTotal,
                    TotalWithGST = cartRes.Result.TotalWithGST,
                    CouponAmount = cartRes.Result.CouponAmount,
                    Message = "Success"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Get cart count and Cart total

        #region Get cart items

        public JsonResult GetTempCartItems(bool isPlacedByRep, bool isSavedOrder)
        {
            try
            {
                var model = new GetTempCartModel();

                model.CartID = 0;
                model.CustomerID = _authData.CustomerID;
                model.IsPlacedByRep = false;
                model.IsSavedOrder = false;
                model.UserID = _authData.UserID;

                var res = _da.GetTempCartItems(model, _authData.Token);

                var mainFeatures = JsonConvert.DeserializeObject<MainFeatures>(_authData.MainFeatures);
                var custFeatures = JsonConvert.DeserializeObject<CustomerFeatures>(_authData.CustomerFeatures);
                var delCharges = JsonConvert.DeserializeObject<List<DeliveryCharge>>(_authData.DeliveryDetails)[0];

                return Json(new
                {
                    CartItems = res.Result.CartItems,
                    CartTotal = res.Result.CartTotal,
                    CartTotalGST = res.Result.CartTotalGST,
                    IsCapture = res.Result.AuthPayment == false ? true : false,
                    Response = res.Message,
                    IsMultipleAddresses = mainFeatures.IsMultipleAddresses,
                    IsPONumber = custFeatures.IsPONumber ?? false,
                    IsSaveOrder = custFeatures.IsSaveOrder ?? false,
                    IsFreight = custFeatures.IsFreightChargeApplicable ?? false,
                    IsNonDayDelivery = custFeatures.IsNonDeliveryDayOrdering ?? false,
                    IsStripePaymentCustomer = custFeatures.IsStripePayment ?? false,
                    IstripePaymentAdmin = mainFeatures.IsStripePayment ?? false,
                    MinCartValue = res.Result.MinCartValue,
                    Coupon = res.Result.Coupon,
                    IsCouponApplied = res.Result.IsCouponApplied,
                    CouponAmount = res.Result.CouponAmount,
                    MinFreightOrderValue = delCharges.OrderValue,
                    DeliveryFee = delCharges.DeliveryFee,
                    DelMessage = delCharges.FrightMessage
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Response = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Get cart items

        #region Get Stripe Keys

        [AllowAnonymous]
        public JsonResult GetStripeKeys()
        {
            try
            {
                //var res = _da.GetStripeKeys(_authData.Token);
                var res = _da.GetStripeKeys();

                return Json(new
                {
                    result = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Response = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Get Stripe Keys

        #region Save Payment Response Log

        public JsonResult SavePaymentLog(string payResponse, long tempCartId)
        {
            try
            {
                var res = _da.SavePaymentLog(payResponse, tempCartId, _authData.Token);

                return Json(new
                {
                    result = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Response = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Save Payment Response Log

        #region Get Payment Token

        public JsonResult GetPaymentToken(double PaymentTotal, bool IsCapture, long TempCartID)
        {
            try
            {
                var res = _da.GetPaymentToken(PaymentTotal, IsCapture, TempCartID, _authData.Token);

                return Json(new
                {
                    result = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Response = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Get Payment Token
        #region Recurring Payment Methods
        [HttpPost]
        public JsonResult CreateSubsciption(SubscriptionDetail subscriptiondetails)
        {
            try
            {
                var res  = _da.CreateSubscription(subscriptiondetails, _authData.Token);
              
                return Json(new { Res = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }
        [HttpPost]
        public JsonResult CancelSubsciption(string PlanId, long CustomerId)
        {
            try
            {
                var res = _da.CancelSubscription(PlanId, CustomerId, _authData.Token);
                return Json(new { Res = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        public JsonResult SaveSubcriptionPaymentLog(string payResponse, long customerId,int recurringId,double? price)
        {
            try
            {
                var res = _da.SaveSubcriptionPaymentLog(payResponse, customerId, recurringId, price, _authData.Token);

                return Json(new
                {
                    result = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Response = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion Get Payment Token
        #region Get Payment Token For Past Orders

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetPaymentTokenForPastOrders(PaymentTokenModel paymentDetails)
        {
            try
            {
                var res = _da.GetPaymentToken(paymentDetails);

                return Json(new
                {
                    result = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Response = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Get Payment Token For Past Orders

        #region Delete item from cart

        public JsonResult DeleteCartItem(bool isDeleteAll, long cartItemID, bool isSavedOrder)
        {
            try
            {
                var res = _da.DeleteCartItem(isDeleteAll, cartItemID, isSavedOrder);

                return Json(new { Message = res });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Delete item from cart

        #region Update item in cart. Quantity, UOM and product comment

        public JsonResult UpdateCartItem(TempCartItem model)
        {
            try
            {
                var cartModel = new UpdateCartModel();
                Mapper.Map(model, cartModel);
                cartModel.CommentID = model.ProdCommentID;
                var res = _da.UpdateCartItem(cartModel, _authData.Token);

                return Json(res);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion Update item in cart. Quantity, UOM and product comment

        #region Get Order comments

        [HttpPost]
        public JsonResult GetOrderComments(GetCommentsModel model)
        {
            try
            {
                var res = _da.GetOrderComments(_authData.CustomerID, model.ProductID, model.IsProduct, _authData.Token);
                return Json(new
                {
                    Comments = res,
                    Message = "Success"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Order comments

        #region Delete comment

        [HttpPost]
        public JsonResult DeleteComment(long commentID, bool isProduct)
        {
            try
            {
                var res = _da.DeleteComment(commentID, isProduct, _authData.Token);

                return Json(new { Message = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Delete comment

        #region Add comment

        [HttpPost]
        public JsonResult AddComments(AddCommentModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                var res = _da.AddComments(model, _authData.Token);

                return Json(new { Message = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Add comment

        #region Place or save an order

        [HttpPost]
        public JsonResult PlaceOrder(PlaceOrderModel model, PayIntent paymentDetails)
        {
            try
            {
                if (model.OrderDate.Year == 1 && model.IsDelivery == true)
                {
                    model.OrderDate = Convert.ToDateTime(model.validateDate.Split('/')[1] + "/" + model.validateDate.Split('/')[0] + "/" + model.validateDate.Split('/')[2]);
                }
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;
                model.IsOrderPlpacedByRep = _authData.Role == "SalesRep" ? true : false;
                model.IsInvoiceComment = model.CommentID > 0 ? true : false;
                model.OrderDate = model.OrderDate; // temporary // to be replaced by date picker in the front end.

                model.PaymentDetails = paymentDetails;
                var res = _da.PlaceOrder(model, _authData.Token);

                return Json(new { Res = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Place or save an order

        #region Save Past Order Payment Details

        [AllowAnonymous]
        [HttpPost]
        public JsonResult SavePastOrderPaymentDetails(SavePastOrderPaymentModel model)
        {
            try
            {
                var res = _da.SavePastOrderPaymentDetails(model);

                return Json(new { Res = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Save Past Order Payment Details

        #region Get Invoices

        public JsonResult GetInvoices(GetInvoicesModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;

                var res = _da.GetInvoices(model, _authData.Token);

                return Json(new { Message = "Success", Result = res });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Invoices

        #region Get Product Categories

        public JsonResult GetProductCategories(bool showSubOnly)
        {
            try
            {
                var res = _da.GetProductCategories(showSubOnly, _authData.Token);

                return Json(new
                {
                    Message = "Success",
                    Result = res,
                    PantryLists = _da.GetCustomerPantryLists(_authData.CustomerID, _authData.Token)
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Product Categories

        #region Product search

        public JsonResult GetSearchProducts(SearchProductModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;
                model.IsRepUser = _authData.Role == "SalesRep" ? true : false;
                model.IsSpecial = false;
                model.Searchtext = model.Searchtext ?? "";
                model.Searchtext = HttpUtility.UrlDecode(model.Searchtext);

                var res = _da.SearchProducts(model, _authData.Token);

                return Json(new { Message = "Success", Result = res });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Product search

        #region Get Order History

        public JsonResult GetOrderHistory()
        {
            try
            {
                var res = _da.GetOrderHistory(_authData.CustomerID, _authData.Token);

                return Json(new { Message = "Success", Result = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Order History

        #region Get Payment Orders

        [AllowAnonymous]
        public JsonResult GetPaymentOrders(string token)
        {
            try
            {
                var res = _da.GetPaymentOrders(token);

                return Json(new { Message = "Success", Result = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Payment Orders

        #region Get Favorite lists for customer

        public JsonResult GetCustomerPantryLists()
        {
            try
            {
                var res = _da.GetCustomerPantryLists(_authData.CustomerID, _authData.Token);

                return Json(new { Message = "Success", Result = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Favorite lists for customer

        #region Delete Pantry list

        [HttpPost]
        public JsonResult DeletePantryList(long pantryListID)
        {
            try
            {
                var res = _da.DeletePantryList(pantryListID, _authData.Token);

                return Json(new { Message = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Delete Pantry list

        #region Get user profile data

        [HttpPost]
        public JsonResult GetUserProfile()
        {
            try
            {
                bool isRep = _authData.Role == "SalesRep" ? true : false;
                var res = _da.GetUserProfile(_authData.CustomerID, _authData.UserID, isRep, _authData.Token);
                return Json(new { Data = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get user profile data

        #region Get product detail

        [HttpPost]
        public JsonResult GetProductDetail(long productID)
        {
            try
            {
                bool isRep = _authData.Role == "SalesRep" ? true : false;
                var res = _da.GetProductDetail(productID, _authData.CustomerID, _authData.UserID, isRep, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get product detail

        #region Send Product Enquiry

        [HttpPost]
        public JsonResult SendProductEnquiry(long productID, string comment)
        {
            try
            {
                var res = _da.SendProductEnquiry(productID, comment, _authData.Token, _authData.CustomerID, _authData.UserID);
                return Json(new { Message = res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Send Product Enquiry

        #region Get order detail

        [HttpPost]
        public JsonResult GetOrderDetails(long orderID)
        {
            try
            {
                var model = new OrderDetailsModel();
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;
                model.OrderID = orderID;
                model.IsOrderHistory = true;

                var res = _da.GetOrderDetails(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get order detail

        #region Re order

        [HttpPost]
        public JsonResult ReOrder(long orderID, List<long> products)
        {
            try
            {
                var model = new ReorderModel();
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;
                model.OrderID = orderID;
                model.AppendToSavedOrder = false;
                model.Products = products;
                model.IsPlacedByRep = _authData.Role == "SalesRep" ? true : false;

                var res = _da.ReOrder(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Re order

        #region Add pantry lists

        [HttpPost]
        public JsonResult AddPantry(AddPantryModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                model.CreatedBy = _authData.UserID;
                model.CreatedDate = DateTime.Now;
                model.IsCreatedByRepUser = _authData.Role == "SalesRep" ? true : false;

                var res = _da.AddPantryList(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Add pantry lists

        #region Add Item to pantry lis

        [HttpPost]
        public JsonResult AddItemToPantry(AddPantryItemModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                model.IsActive = true;
                model.IsCore = false;
                var res = _da.AddItemToPantryList(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Add Item to pantry lis

        #region Delete item from pantry list

        [HttpPost]
        public JsonResult DeletePantryListItem(DeletePantryItemModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;

                var res = _da.DeletePantryListItem(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Delete item from pantry list

        #region Save pantry sort order

        [HttpPost]
        public JsonResult SavePantrySortOrder(PantrySortBO model)
        {
            try
            {
                var res = _da.SavePantrySortOrder(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Save pantry sort order

        #region Get page Help text

        [HttpPost]
        public JsonResult GetPageHelpText(string pageName)
        {
            try
            {
                var res = _da.GetPageHelpText(pageName);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get page Help text

        #region Get Suggestive items for customer

        [HttpPost]
        public JsonResult GetSuggestiveItems(long cartID)
        {
            try
            {
                var model = new SuggestiveItemsBO();

                model.CartID = cartID;
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;

                var res = _da.GetSuggestiveItems(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Suggestive items for customer

        #region Apply promo code

        [HttpPost]
        public JsonResult ApplyPromoCode(long cartID, string promoCode)
        {
            try
            {
                var model = new PromoModel();

                model.CartID = cartID;
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;
                model.Coupon = promoCode;

                var res = _da.ApplyPromoCode(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Apply promo code

        #region Get customer delivery addresses

        public JsonResult GetCustomerAddresses()
        {
            try
            {
                var res = _da.GetCustomerAddresses(_authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get customer delivery addresses

        #region Get pickup addresses

        public JsonResult GetPickupLocations()
        {
            try
            {
                var res = _da.GetPickupLocations(_authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get pickup addresses

        #region Get pickup / Delivery dates

        [HttpPost]
        public JsonResult GetPickupOrDeliveryDates(int pickupID, bool isDelivery, long addressID)
        {
            try
            {
                var model = new PickupDelDatesMoal()
                {
                    PickupID = pickupID,
                    IsDelivery = isDelivery,
                    AddressID = addressID
                };
                var res = _da.GetPickupDeliveryDates(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get pickup / Delivery dates

        #region Add delivery address

        [HttpPost]
        public JsonResult AddDeliveryAddress(AddAddressModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                var res = _da.AddDeliveryAddress(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Add delivery address

        #region Get Whats New items

        public JsonResult GetWhatsNew(SearchProductModel model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                model.UserID = _authData.UserID;
                model.IsRepUser = _authData.Role == "SalesRep" ? true : false;
                model.IsSpecial = false;
                model.Searchtext = "";

                var res = _da.GetWhatsNew(model, _authData.Token);

                return Json(new { Message = "Success", Result = res });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Whats New items

        #region delete delivery address

        [HttpPost]
        public JsonResult DeleteDeliveryAddress(long addressID)
        {
            try
            {
                var res = _da.DeleteDeliveryAddress(addressID, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion delete delivery address

        #region Save / Update recurring orders for a customer

        [HttpPost]
        public JsonResult ManageRecurringOrders(RecurringOrdersBO model)
        {
            try
            {
                model.CustomerID = _authData.CustomerID;
                var res = _da.ManageRecurringOrders(model, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Save / Update recurring orders for a customer

        #region Get recurring order

        public JsonResult GetRecurringOrder()
        {
            try
            {
                var res = _da.GetRecurringOrder(_authData.CustomerID, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get recurring order

        #region Delete recurring order item

        [HttpPost]
        public JsonResult DeleteRecurrOrderItem(DeleteRecurrItemBO model)
        {
            try
            {
                var res = _da.DeleteRecurrOrderItem(model.RecurrProdID, _authData.Token);
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Delete recurring order item
    }
}