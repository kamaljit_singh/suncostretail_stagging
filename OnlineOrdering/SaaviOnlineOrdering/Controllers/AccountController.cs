﻿using Newtonsoft.Json;
using SaaviOnlineOrdering.Models.BO;
using SaaviOnlineOrdering.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace SaaviOnlineOrdering.Controllers
{
    public class AccountController : Controller
    {
        private readonly AccountDA _da;

        public AccountController()
        {
            _da = new AccountDA();
        }

        // GET: Account
        public ActionResult Login()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (model.IsGuest)
            {
                model.Password = "";
                model.Username = "";
            }
            var res = _da.Login(model);
            ModelState.Clear();

            if (res.Message == "Success")
            {
                var mainFeatures = JsonConvert.DeserializeObject<MainFeatures>(res.MainFeature);
                var customerFeatures = JsonConvert.DeserializeObject<CustomerFeatures>(res.CustomerFeatures);
                var defaultInfo = res.DefaultInfo;

                var customerDetail = JsonConvert.DeserializeObject<CustomerDetail>(res.CustDetails.Replace("[", "").Replace("]", ""));
                var pdfDetails = res.PDFDetails;

                return Json(new
                {
                    Message = "Success",
                    ShowTermsContitions = mainFeatures.IsShowTermConditionsPopup,
                    LiquorPopup = mainFeatures.IsLiquorControlPopup,
                    IsNoticeBoard = customerFeatures.IsNoticeBoard,
                    DefaultInfo = defaultInfo,
                    TermsAndConditions = customerDetail.TermconditionMessage,
                    PDF = pdfDetails,
                    IsParent = mainFeatures.IsParentChild,
                    CustomerID = customerDetail.CustomerID,
                    ShowImportantNotice = HelperClass.ImportantNotice
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Message = res.Message });
            }
        }

        // GET: Account
        public ActionResult Register()
        {
            return View(new RegisterViewModel()
            {
                Message = "",
                GoogleMap = HelperClass.GoogleMap,
                GoogleAPI = HelperClass.GoogleAPI,
                ExistingAccount = true
            });
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            var result = _da.Register(model);
            if (result == "Success")
            {
                ModelState.Clear();
            }

            var res = new RegisterViewModel()
            {
                Message = result,
                GoogleMap = HelperClass.GoogleMap,
                GoogleAPI = HelperClass.GoogleAPI,
                ExistingAccount = true
            };
            return View(res);
        }

        [HttpPost]
        public JsonResult ResetPassword(string email)
        {
            var res = _da.ResetPassword(email);

            return Json(new { Result = res }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GuestLogin()
        {
            var loginModel = new LoginViewModel()
            {
                Username = "",
                Password = "",
                IsGuest = true
            };

            var res = this.Login(loginModel);
            if (res != null)
            {
                return RedirectToAction("SearchProducts", "Main");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}