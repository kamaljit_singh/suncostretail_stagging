(function ($) {
    "use strict";

    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })
    })

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit', function () {
        var check = true;

        if ($(this).hasClass('ignore')) {
            return true;
        }

        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            }
        }
        if (check && $('.validate-form').hasClass('register')) {
            if (!$('#ExistingAccount').prop('checked') && window.sessionStorage.getItem('parsed') != 'Y') {
                $.confirm({
                    columnClass: 'medium',
                    title: false,
                    content: '<span class="fs-19">Are you a Retail / Private Customer?</span>',
                    type: 'blue',
                    buttons: {
                        Ok: {
                            text: 'Yes',
                            btnClass: 'btn-blue',
                            keys: ['enter', 'shift'],
                            action: function () {
                                $('#ExistingAccount').prop('checked', true);

                                setTimeout(function () {
                                    $('#btnRegister').trigger('click');
                                }, 100);
                            }
                        },
                        No: {
                            text: 'No',
                            action: function () {
                                setTimeout(function () {
                                    window.sessionStorage.setItem('parsed', 'Y');
                                    $('#btnRegister').trigger('click');
                                }, 100);
                            }
                        }
                    }
                });
                return false;
            } else {
                return check;
            }
        } else {
            return check;
        }
    });

    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    function validate(input) {
        if ($(input).attr('name') === 'ABN' || $(input).attr('name') === 'BusinessName') {
            return true;
        }

        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'Email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        } else if ($(input).attr('type') == 'password') {
            if ($(input).val().trim() == '') {
                $(input).parent().attr('data-validate', 'Enter password');
                return false;
            } else if ($(input).val().length < 8 && $(input).data('type') === 'R') {
                $(input).parent().attr('data-validate', 'Password should contain 8 characters or more');
                return false;
            }
        } else if ($(input).attr('name') == 'Phone') {
            if ($(input).val().length < 10) {
                $(input).parent().attr('data-validate', 'Phone should contain 10 characters');
                return false;
            }
        }
        else {
            if ($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    $('.btn-show-pass').on('click', function () {
        if (showPass == 0) {
            $(this).next('input').attr('type', 'text');
            $(this).find('i').removeClass('zmdi-eye');
            $(this).find('i').addClass('zmdi-eye-off');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type', 'password');
            $(this).find('i').addClass('zmdi-eye');
            $(this).find('i').removeClass('zmdi-eye-off');
            showPass = 0;
        }
    });
})(jQuery);