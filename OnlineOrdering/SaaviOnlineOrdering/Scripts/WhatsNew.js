﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: "What's New",
        search: '',
        schema: '',
        allData: [],
        viewType: 1, // 1 = main categories , 2 = sub Categories, 3 = Products
        categories: [],
        subCategories: [],
        selectedMCategory: 0,
        products: [],
        getCategoriesModel: {
            "showSubOnly": false
        },
        getProductsModel: {
            "CustomerID": 0,
            "MainCategoryID": 0,
            "SubCategoryID": 0,
            "FilterID": 0,
            "Searchtext": "",
            "IsSpecial": true,
            "PageSize": 40,
            "PageIndex": 0,
            "UserID": 0,
            "IsRepUser": false
        },
        showSupplier: false,
        showBrand: false,
        longDetail: false,
        showPrice: false,
        highlightStock: false,
        pictureView: false,
        showFilters: false,
        itemEnquiry: false,
        highlightRewardItem: false,
        showOrderHistory: false,
        showFavoriteIcon: false,
        showUnitPerCtn: false,
        productLongDetail: false,
        searchProducts: false,
        showInvoices: false,
        favouriteLists: [],
        addToDefaultPantry: false,
        isPantryOnlySearch: false,
        isSearch: false,
        isDecimal: false,
        currency: '',
        marketText: false,
        copyPantry: false,
        lastUOM: false,
        backOrder: false,
        isShowSubCategory: false,
        totalPages: 0,
        selectedCategory: '',
        isRetail: false,
        isGuest: false,
        addPantryItemModel: {
            "PantryItemID": 0,
            "PantryListID": 0,
            "ProductID": 0,
            "Quantity": 0.0,
            "IsActive": true,
            "IsCore": false,
            "CustomerID": 0
        },
        pantryModel: {
            "PantryListID": 0,
            "CustomerID": 0,
            "PantryListName": "",
            "IsCreatedByRepUser": false,
            "IsActive": true,
            "PantryListType": "C",
            "IsSynced": false,
            "IsSorted": false,
            "IsCopy": false,
        },
        tempCartInsertionModel: {
            "RunNo": "",
            "CommentLine": "",
            "PackagingSequence": "",
            "CartItem": {
                "ProductID": 0,
                "Quantity": 0,
                "Price": 0,
                "ProductWeight": 0,
                "WeightName": "",
                "WeightDescription": "",
                "IsPieceOrWeight": "",
                "IsSpecialPrice": false,
                "CommentID": 0,
                "UnitId": 0,
                "IsGstApplicable": false,
                "ItemPricePerUnit": 0,
                "OrderedQuantity": 0,
                "RecievedQuantity": 0,
                "UnitsPerCarton": 0,
                "BasePrice": 0,
                "IsDiscountApplicable": false,
                "IsBoxDiscount": false,
                "PriceType": "",
                "IsNoPantry": false
            },
            "IsSavedOrder": true,
            "Warehouse": "",
            "AddressID": 0
        },
        showcategory: false,
        showCat: false,
        pageHelpText: '',
        readMore: false,
        productInclusion: '',
    },

    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();
        $('#divMainProducts').css('height', (window.innerHeight - 120).toString() + 'px');
        // getCartCount and total order value
        this.getCartCount();

        // Get features
        this.getManageFeatures();

        // Dynamic height
        var divHeight = $('#divWhatsNew').height();
        var windowHeight = $(window).height() - 180;

        $('#divWhatsNew').css('height', (windowHeight));
        $('#divMainProducts').css('height', (windowHeight));
        $(window).resize(function () {
            // console.log('resize');
            var windowHeight = $(window).height() - 180;
            $('#divWhatsNew').css('height', (windowHeight));
            $('#divMainProducts').css('height', (windowHeight));
        });

        this.searchProductsDb();

        // Get page Help Text
        //axios.post(Router.action('Main', 'GetPageHelpText'), { pageName: 'WhatsNew' })
        //    .then(response => {
        //        var res = response.data.Description;
        //        this.pageHelpText = res;
        //        console.log(res);
        //    });
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        this.getProductsModel.MainCategoryID = 0;
                        this.getProductsModel.Searchtext = this.search;
                        this.products = [];
                        this.searchProductsDb(0, true, 'Categories', this.search);
                        this.isSearch = true;
                    }
                } else {
                    showAlert(null, 'Enter a keyword to search');
                }
            }
        },

        // function to get all the pantry list items
        getFavLists: function () {
            //ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerPantryLists')).then(response => {
                this.favouriteLists = response.data.Result.Data;
                //$.unblockUI();
            });
        },
        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    console.log(response);
                    this.showBrand = response.data.ShowBrand;
                    this.showSupplier = response.data.ShowSupplier;
                    this.showPrice = response.data.ShowPrice;
                    this.pictureView = response.data.PictureView;
                    this.showHistory = response.data.ShowHistory;
                    this.longDetail = response.data.LongDetail;
                    this.unitPerCtn = response.data.ShowUnitPerCtn;
                    this.highlightRewardItem = response.data.HighlightRewardItems;
                    this.nonFoodVersion = response.data.NonFoodVersion;
                    this.showFavoriteIcon = response.data.ShowFavoriteIcon;
                    this.showUnitPerCtn = response.data.ShowUnitsPerCarton;
                    this.itemEnquiry = response.data.ItemEnquiry;
                    this.highlightStock = response.data.HighlightStock;
                    this.productLongDetail = response.data.ProductLongDetail;
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.isDecimal = response.data.IsDecimal;
                    this.currency = response.data.Currency;
                    this.isShowSubCategory = response.data.ShowSubCategory;
                    this.marketText = response.data.MarketText;
                    this.lastUOM = response.data.ReturnLastUOM;
                    this.backOrder = response.data.BackOrder;
                    this.isRetail = response.data.IsRetail;
                    this.isGuest = response.data.IsGuest;
                    this.showCat = response.data.ShowCategory;

                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data
                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // function to get all the pantry list items
        getCategories: function () {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetProductCategories'), this.getCategoriesModel).then(response => {
                this.allData = response.data.Result.Data;
                this.categories = this.allData;
                $.unblockUI();
            });
        },

        // function to get products based on selection
        searchProductsDb: function () {
            ajaxStartLoading();

            axios.post(Router.action('Main', 'GetWhatsNew'), this.getProductsModel).then(response => {
                console.log(response);
                if (response.data.Result === undefined) {
                    $('#divEmpty p').html('<h3 class="text-center" style="margin:15% auto;color:#000 !important;font-weight:500;">No products found</h3>');
                } else {
                    this.products = response.data.Result.Data;
                }
                if (this.products.length === 0) {
                    $('#divEmpty p').html('<h3 class="text-center" style="margin:15% auto;color:#000 !important;font-weight:500;">No products found</h3>');
                }

                $.unblockUI();
            });
        },

        // function to set price based on UOM DDL change
        setPrice: function (e, idx, dUOM) {
            var select = $(e.target);
            var selectedIndex = select.prop('selectedIndex');

            var lastUom = select.data('lastuom');
            var selectedUOM = dUOM[selectedIndex].UOMID;

            if (lastUom === selectedUOM) {
                if (!select.hasClass('lastUom')) {
                    select.addClass('lastUom');
                }
            } else {
                select.removeClass('lastUom');
            }

            var price = dUOM[selectedIndex].Price;
            price = Math.round(price * Math.pow(10, 2)) / Math.pow(10, 2);
            $('#' + idx + 'priceSpan').html('$' + price.toFixed(2));

            if (this.marketText && price === 0) {
                $('#' + idx + 'priceSpan').html('Market ' + this.currency);
            }

            $('#' + idx + '_uom').val(select.prop('selectedIndex'));

            if (this.showUnitPerCtn) {
                if (dUOM[selectedIndex].UOMDesc.toLowerCase() === 'ctn' || dUOM[selectedIndex].UOMDesc.toLowerCase() === 'carton') {
                    $('#' + idx + '_upc').html('[U\CTN=' + dUOM[selectedIndex].QuantityPerUnit + ']');
                    $('#' + idx + '_upc').removeClass('hidden');
                } else {
                    $('#' + idx + '_upc').addClass('hidden');
                }
            }
        },

        checkNumeric: function (e) {
            var evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var el = $(e.target);

            if (this.isDecimal) {
                if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                    evt.preventDefault();;
                } else {
                    var len = el.val().length;
                    var index = el.val().indexOf('.');
                    if (index >= 0 && charCode == 46) {
                        evt.preventDefault();;
                    }
                    if (index >= 0) {
                        var charAfterdot = (len + 1) - index;
                        if (charAfterdot > 3) {
                            evt.preventDefault();;
                        }
                    }
                }
                return true;
            } else {
                if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode === 46) {
                    evt.preventDefault();;
                } else {
                    return true;
                }
            }
        },

        // Set the quantity to a hidden field
        setQuantity: function (e, idx, product) {
            var quantity = $(e.target).val();
            $('#' + idx + '_quantity').val(quantity === '' ? '0' : quantity);

            if (e.keyCode === 13) {
                this.addItemToTempCart(product, idx);
                $('#' + (idx + 1) + '_qtyInput').focus();
            }
            $('#' + idx + '_uom').val($('#' + idx + '_ddl option:selected').index());
        },

        // Add item to temp cart
        addItemToTempCart: function (prod, index) {
            if (this.isGuest) {
                showAlert(null, 'In order to use this feature, please register with Suncoast Fresh.');
                return false;
            }

            if (prod.StockQuantity <= 0 && this.backOrder) {
                if (prod.FilterID !== 9 && prod.FilterID !== 4 && prod.CategoryID !== 29) {
                    showAlert('Oops!', 'Unfortunately, there is not sufficient stock available to complete your order.');
                    return false;
                }
            }
            // show the loading
            ajaxStartLoading();
            var qty = parseFloat($('#' + index + '_quantity').val());
            var existing = $('#' + index + '_addToCart').hasClass('green-border-btn');
            if (qty === 0) {
                showAlertRed('', 'Please enter order quantity');
                // End loading
                $.unblockUI();
                return false;
            }

            if (prod.BuyIn && !existing) {
                $.confirm({
                    title: '',
                    content: 'This item is an ORDER IN ONLY  item. WILL DELIVER ASAP.',
                    type: 'dark',
                    buttons: {
                        add: {
                            text: 'I Acknowledge',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                app.addToDB(prod, index);
                            }
                        },
                        cancel: {
                            text: 'Cancel Item',
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                //app.addFavList();
                                $.unblockUI();
                            }
                        },
                    }
                });
            } else {
                if (qty > prod.StockQuantity && this.backOrder && prod.FilterID !== 9 && prod.FilterID !== 4 && prod.CategoryID !== 29) {
                    $.confirm({
                        title: 'Oops!',
                        content: 'Sorry, Only ' + prod.StockQuantity + ' items are currently available. By pressing OK we will submit only the available stock quantity.',
                        type: 'dark',
                        buttons: {
                            add: {
                                text: 'OK',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    app.addToDB(prod, index);
                                }
                            },
                            cancel: {
                                text: 'CANCEL ITEM',
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    //app.addFavList();
                                    $.unblockUI();
                                }
                            },
                        }
                    });
                } else {
                    app.addToDB(prod, index);
                }
            }
        },

        addToDB: function (prod, index) {
            var btn = $('#' + index + '_addToCart');
            var qty = parseFloat($('#' + index + '_quantity').val());
            var price = parseInt($('#' + index + '_uom').val());
            var unitName = '';

            if (qty > prod.StockQuantity && this.backOrder && prod.FilterID !== 9 && prod.FilterID !== 4 && prod.CategoryID !== 29) {
                qty = prod.StockQuantity;
                $('#' + index + '_qtyInput').val(qty);
            }

            this.tempCartInsertionModel.CartItem.ProductID = prod.ProductID;
            this.tempCartInsertionModel.CartItem.Quantity = qty;
            this.tempCartInsertionModel.CartItem.IsGstApplicable = prod.IsGST;

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.Price = prod.DynamicUOM[price].Price;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.Price = prod.Prices.Price;
                } else {
                    this.tempCartInsertionModel.CartItem.Price = prod.Price;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.IsSpecialPrice = prod.DynamicUOM[price].IsSpecial;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.IsSpecialPrice = prod.Prices.IsSpecial;
                } else {
                    this.tempCartInsertionModel.CartItem.IsSpecialPrice = false;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.UnitId = prod.DynamicUOM[price].UOMID;
            } else {
                this.tempCartInsertionModel.CartItem.UnitId = prod.UOMID;
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.DynamicUOM[price].QuantityPerUnit;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.Prices.QuantityPerUnit;
                } else {
                    this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.UnitsPerCarton;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.BasePrice = prod.DynamicUOM[price].CostPrice;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.BasePrice = prod.Prices.CostPrice;
                } else {
                    this.tempCartInsertionModel.CartItem.BasePrice = prod.CompanyPrice;
                }
            }

            if (prod.DynamicUOM != null) {
                unitName = prod.DynamicUOM[price].UOMDesc;
            } else {
                if (prod.Prices != null) {
                    unitName = prod.Prices.UOMDesc;
                } else {
                    unitName = prod.UOMDesc;
                }
            }

            this.tempCartInsertionModel.CartItem.OrderedQuantity = qty;
            this.tempCartInsertionModel.CartItem.RecievedQuantity = 0;
            this.tempCartInsertionModel.CartItem.IsDiscountApplicable = false;
            this.tempCartInsertionModel.CartItem.IsPieceOrWeight = false;
            this.tempCartInsertionModel.CartItem.WeightName = '';
            this.tempCartInsertionModel.CartItem.IsNoPantry = false;

            // Send the data back to controller for processing
            axios.post(Router.action('Main', 'InsertUpdateTempCart'), this.tempCartInsertionModel).then(response => {
                if (response.data.Result) {
                    if (response.data.CartCount > 0) {
                        $('#itemCount').html(response.data.CartCount);
                        $('#itemCount').show();

                        if (!btn.hasClass('green-border-btn')) {
                            btn.removeAttr('class');
                            btn.addClass('green-border-btn');
                        }
                    }
                    showAlert(prod.ProductName, qty + ' ' + unitName + ' added to cart');
                } else {
                    showAlert(prod.ProductName, response.data.Message);
                }

                // End loading
                $.unblockUI();
            });
        },

        // Show default image if Productimage not found
        showDefault: function (e) {
            $(e.target).attr('src', this.schema + '/content/images/no-product.png');
        },

        // Send Item enquiry
        inquiryPopup: function (pID) {
            // prompt
            $.confirm({
                title: 'Item Enquiry',
                type: 'dark',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Enter enquiry details:</label>' +
                    '<textarea class="comment form-control" style="resize:none;" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',

                        action: function () {
                            var comment = this.$content.find('.comment').val();
                            if (!comment) {
                                showAlert(null, 'Provide detail about the enquiry')
                                return false;
                            }
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'SendProductEnquiry'), {
                                productID: pID,
                                comment: comment
                            }).then(response => {
                                showAlert(null, 'Your enquiry has been sent');
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        },

        // Set product id
        setProductID: function (pID, remove) {
            if (remove) {
                this.addPantryItemModel.ProductID = 0;
                $('#divFavLists input[type="checkbox"]').prop('checked', false);
            } else {
                this.addPantryItemModel.ProductID = pID;
                // var data = window.sessionStorage.getItem('AddtoDefaultPantry');
                if (this.addToDefaultPantry) {
                    this.addPantryItemModel.ProductID = pID;
                    this.addItemToFav();
                } else {
                    if (this.favouriteLists === null || this.favouriteLists.length === 0) {
                        $.confirm({
                            title: '',
                            content: 'No favourite lists exist. Do you want to create a new one?',
                            type: 'dark',
                            buttons: {
                                add: {
                                    text: 'Yes',
                                    btnClass: 'btn-blue',
                                    keys: ['enter'],
                                    action: function () {
                                        //$('#modalFavList').modal('hide');
                                        app.addFavList();
                                    }
                                },
                                no: function () {
                                    app.addPantryItemModel.ProductID = 0;
                                },
                            }
                        });
                        return;
                    } else {
                        $('#modalFavList').modal('show');
                    }
                }
            }
        },

        // Add items to fav list
        addItemToFav: function () {
            if ($('#divFavLists input[type="checkbox"]').filter(':checked').length === 0 && this.addToDefaultPantry === false) {
                showAlert(null, 'Please choose a favourite list');
            } else {
                ajaxStartLoading();
                // bind Products
                axios.post(Router.action('Main', 'AddItemToPantry'), this.addPantryItemModel).then(response => {
                    console.log(response.data.Message);
                    if (response.data.Message == 'Success') {
                        //var data = window.localStorage.getItem('AddtoDefaultPantry');
                        if (this.addToDefaultPantry) {
                            // showAlert(null, "Product added to Default pantry successfully.")
                            $.confirm({
                                title: '',
                                content: 'Product added to your pantry',
                                type: 'dark',
                                buttons: {
                                    Done: function () {
                                        this.addToDefaultPantry = false;
                                    },
                                    more: {
                                        text: 'Add More',
                                        btnClass: 'btn-blue',
                                        keys: ['enter'],
                                        action: function () {
                                            $('#modalFavList').modal('hide');
                                        }
                                    }
                                }
                            });
                        } else {
                            showAlert(null, "Product added to your favourite list")
                        }
                    } else {
                        showAlert(null, response.data.Message);
                    }
                    $('#modalFavList').modal('hide');
                    $('#divFavLists input[type="checkbox"]').prop('checked', false);
                    $.unblockUI();
                });
            }
        },

        // Set fav list for adding
        setFavList: function (e, favID) {
            var state = $(e.target).prop('checked');
            this.addPantryItemModel.PantryListID = favID;
            $('#divFavLists input[type="checkbox"]').prop('checked', false);
            $(e.target).prop('checked', state);
        },

        // function to get all the pantry list items
        getFavLists: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerPantryLists')).then(response => {
                this.favouriteLists = response.data.Result.Data;
                $.unblockUI();
            });
        },

        // add fav list if none exist while adding product
        addFavList: function () {
            // prompt
            $.confirm({
                title: 'Add Favourite List',
                type: 'dark',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Comments:</label>' +
                    '<input type="text" placeholder="Enter favourite list name" class="fav form-control" style="resize:none;" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',
                        action: function () {
                            var fav = this.$content.find('.fav').val();
                            if (!fav) {
                                showAlert(null, 'Please enter favourite list name');
                                return false;
                            }
                            app.pantryModel.PantryListName = fav;
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'AddPantry'), app.pantryModel)
                                .then(response => {
                                    console.log(response);
                                    if (response.data.Message == 'Success') {
                                        app.getFavLists();
                                        $('#modalFavList').modal('show');
                                    } else {
                                        showAlert(null, response.data.Message);
                                    }
                                    $.unblockUI();
                                });
                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        },

        // Function to be called for scroll based paging
        scrolledBottom: function () {
            // Use paging only when the ALL products is selected.
            this.getProductsModel.PageIndex = this.getProductsModel.PageIndex + 1;

            if (this.getProductsModel.PageIndex <= (this.totalPages - 1)) {
                ajaxStartLoading();
                if (this.isShowSubCategory) {
                    this.searchProductsDb(this.getProductsModel.SubCategoryID, false, this.selectedCategory, '', this.getProductsModel.PageIndex);
                } else {
                    this.searchProductsDb(0, false, this.selectedCategory, '', this.getProductsModel.PageIndex);
                }
            }
            console.log("Bottom");
        },

        showProductInclusions: function (product) {
            this.productInclusion = product.ProductFeature;
            this.readMore = true;
        }
    }
});