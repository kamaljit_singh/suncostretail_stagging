﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Recurring Order',
        search: '',
        orders: [],
        showOrderStatus: false,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        isPantryOnlySearch: false,
        copyPantry: false,
        IsStripePaymentAdmin: false,
        IsStripePaymentCustomer: false,
        customerId: 0,
        recurringId:0,
        isRetail: false,
        pageHelpText: '',
        customerAddresses: [],
        pickupLocations: [],
        di: null,
        isDeliveryRecurr: true,
        recurringOrderModal: {
            "ID": 0,
            "CustomerID": 0,
            "AddressID": 0,
            "PickupID": 0,
            "Frequency": 'Weekly',
            "IsSuspended": false,
            "OrderType": 'Delivery',
            "StartDate": '',
            "RecurrProds": []
        },
        initOrderType: '',
        initAddress: '',
        initStartDate: '',
        initFrequency: '',
        AllProducts: [],
        SelectedProducts: [],
        ChargedAmount: 0
    },
    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();

        //this.loadStripeForm();
        // Get managed features
        this.getManageFeatures();

        // getCartCount and total order value
        this.getCartCount();

        this.getDeliveryAddresses();
        this.getPickupLocations();

        //@@click="GetPickupOrDeliveryDates(0, true, customerAddresses[0].AddressId)"
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },
        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.showOrderStatus = response.data.ShowOrderStatus;
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.isRetail = response.data.IsRetail;                    
                    app.IsStripePaymentAdmin = response.data.IstripePaymentAdmin;
                    app.IsStripePaymentCustomer = response.data.IsStripePaymentCustomer;                    
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // Get delivery addresses for the customer
        getDeliveryAddresses: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerAddresses')).then(response => {
                if (response.data.Message === 'Success') {
                    this.customerAddresses = response.data.Result;
                    //  this.addressModal = true;
                }
            });
        },

        // Get pickup locations for the customers
        getPickupLocations: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetPickupLocations')).then(response => {
                if (response.data.Message === 'Success') {
                    this.pickupLocations = response.data.Result.Pickups;
                    //  this.addressModal = true;
                    this.showRecurringOrderModal();
                }
            });
        },

        // initialize the date picker
        initDatepicker: function (type) {
            var availableDays = [];
            var pDays = this.di.permittedDays;

            if (pDays === undefined) {
                pDays = this.di.PermittedDays;
            }

            $.each(pDays, function (idx, val) {
                if (val === 'Monday') {
                    availableDays.push(1);
                } else if (val === 'Tuesday') {
                    availableDays.push(2);
                } else if (val === 'Wednesday') {
                    availableDays.push(3);
                } else if (val === 'Thursday') {
                    availableDays.push(4);
                } else if (val === 'Friday') {
                    availableDays.push(5);
                } else if (val === 'Saturday') {
                    availableDays.push(6);
                } else if (val === 'Sunday') {
                    availableDays.push(0);
                }
            });
            //debugger;
            var minDate = new Date(moment(this.di.OrderDatesLong[0]));
            var maxDate = new Date();
            // minDate.setDate(minDate.getDate() + 2);
            maxDate.setDate(maxDate.getDate() + 30);
            var todayDay = new Date().getDate();
            var month = new Date().getMonth();

            $('.datepicker-Recurr').datepicker({
                minDate: minDate,
                maxDate: maxDate,
                showOtherMonths: false,
                onSelect: function (formattedDate, date, inst) {
                    app.recurringOrderModal.StartDate = date;
                },
                onRenderCell: function (date, cellType) {
                    if (cellType == 'day') {
                        var day = date.getDay(),
                            isDisabled = availableDays.indexOf(day) == -1;
                        var clss = '';
                        if (availableDays.indexOf(day) > -1 && !isDisabled) {
                            clss = 'bgGreen';
                        }
                        return {
                            disabled: isDisabled,
                            classes: clss //isDisabled == true ? 'bgDisabled' :
                        }
                    }
                }
            });
            var dPicker = $('.datepicker-Recurr').datepicker().data('datepicker');
            dPicker.clear();
        },

        // Get Delivery Dates based on selection
        GetPickupOrDeliveryDates: function (pickupID, isDelivery, addressID) {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetPickupOrDeliveryDates'), {
                pickupID: pickupID,
                isDelivery: isDelivery,
                addressID: addressID
            }).then(response => {
                this.di = response.data.Result;
                if (pickupID > 0) {
                    $('#ddlDateType').val('U');
                } else {
                    $('#ddlDateType').val('D');
                }

                this.initDatepicker();
                $.unblockUI();
            });
        },
        loadStripeForm: function () {
            axios.get(Router.action('Main', 'GetStripeKeys'))
                .then(response => {
                    var res = response.data.result

                    var stripe = Stripe(res.Result.PublicKey, {
                        stripeAccount: res.Result.ConnectedAccountID
                    });
                    var elements = stripe.elements();
                    var style = {
                        base: {
                            color: "#32325d",
                        }
                    };

                    var card = elements.create("card", { style: style });
                    card.mount("#stripe-card-element");

                    card.addEventListener('change', function (event) {
                        var displayError = document.getElementById('stripe-card-errors');
                        if (event.error) {
                            displayError.textContent = event.error.message;
                        } else {
                            displayError.textContent = '';
                        }
                    });

                    var form = document.getElementById('stripe-payment-form');
                    form.addEventListener('submit', function (ev) {
                        ev.preventDefault();
                        app.createSubscriptionStripePayment(stripe, card);
                    });
                });
        },
        saveStripeResponse: function (data) {
            axios.post(Router.action('Main', 'SaveSubcriptionPaymentLog'), { payResponse: data, customerId: app.customerId, recurringId: app.recurringId, price: app.ChargedAmount }).then(response => {
                console.log(response);
            });
        },
        createSubscriptionStripePayment: function (stripe, card) {
            ajaxStartLoading();           
            stripe.createPaymentMethod({
                type: 'card',
                card: card,
            }).then(function (result) {
                    if (result === undefined || result === null || result === '') {            
                        saveSubscribeStripeResponse('Error Occur During Payment Method Creation From Web View and result is null:' + result.error);
                        $('#stripe-card-errors').text('We cannot proceed with order because of payment error. Please contact admin if it is a problem.');
                        $.unblockUI();
                        return;
                    }                            
                setTimeout(function () {
                    var respStr = JSON.stringify(result);      
                    app.saveStripeResponse(respStr);
                }, 2000);
                    //console.log(result);
                    if (result.error) {
                        // Show error to your customer (e.g., insufficient funds)
                        $('#stripe-card-errors').text(result.error.message);
                        app.saveStripeResponse('Error Occur During Payment From Web View and Error Message :' + result.error.message);
                        $.unblockUI();
                    } else {
                        $('#modalAddStripe').modal('hide');
                        app.placeSubscriptionWithStripe({
                            planid: app.recurringId,
                            paymentmethod: result.paymentMethod.id,
                            customerid: app.customerId,                           
                            newplanid: null,
                            price: app.ChargedAmount
                        });               
                    }
                });
           
        },
        // Place order final with stripe or without stripe
        placeSubscriptionWithStripe: function (subscriptiondetails) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'CreateSubsciption'), {subscriptiondetails: subscriptiondetails }).then(response => {
                var res = response.data;      
                $('#modalAddStripe').modal('hide');                
                if (res.Res.Message === 'Success') {
                    $.confirm({
                        columnClass: 'medium',
                        title: false,
                        content: '<span class="fs-19">Recurring payment has been placed successfully. Your Order No is ' + res.Res.Data.OrderID + '</span>',
                        type: 'dark',
                        buttons: {
                            Ok: {
                                text: 'Continue',
                                btnClass: 'btn-blue',
                                keys: ['enter', 'shift'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'SearchProducts');
                                }
                            }
                        }
                    });
                }else {
                    showAlert(null, 'Recurring payment posting failed. Message : ' + res.Res.Message);
                }
                $.unblockUI();
            });
        },
        showRecurringOrderModal: function () {
            // ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetRecurringOrder')).then(response => {
                if (response.data.Message === 'Success') {
                    console.log(response.data);
                   
                    var data = response.data.Result.RecurringOrder;
                    this.AllProducts = response.data.Result.AllProducts

                    if (data.AddressId === 0 && data.PickupId === 0) {
                        app.initAddress = 'N/A';
                        app.initFrequency = 'N/A';
                        app.initOrderType = 'N/A';
                        app.initStartDate = 'N/A',
                        app.initCharge='N/A'

                        app.recurringOrderModal = {
                            "ID": data.ID,
                            "AddressID": data.AddressId == 0 ? app.customerAddresses[0].AddressId : data.AddressId,
                            "PickupID": data.PickupID,
                            "Frequency": data.Frequency == null ? 'Weekly' : data.Frequency,
                            "IsSuspended": data.IsSuspended,
                            "OrderType": data.OrderType == null ? 'Delivery' : data.OrderType,
                            "StartDate": null
                        }

                        app.GetPickupOrDeliveryDates(0, true, app.customerAddresses[0].AddressId);
                    } else {
                        app.recurringOrderModal.ID = data.Id;
                        app.recurringOrderModal.IsSuspended = data.IsSuspended;
                        data.StartDate = '2020-07-31';
                        app.initStartDate = moment(data.StartDate).format('DD/MM/YYYY');

                        if (data.AddressId > 0 && data.PickupId === 0) {
                            app.recurringOrderModal.AddressID = data.AddressId;
                            $('#ddlReccurDeliveryAddress').val(data.AddressId);
                            app.initAddress = $('#ddlReccurDeliveryAddress :selected').text();

                            app.GetPickupOrDeliveryDates(0, true, data.AddressId);
                        } else {
                            app.recurringOrderModal.PickupID = data.PickupId;
                            app.initAddress = $('#ddlReccurPickupAddress :selected').text();
                            app.GetPickupOrDeliveryDates(data.PickupId, false, 0);
                        }
                        setTimeout(function () {
                            var dPicker = $('.datepicker-Recurr').datepicker().data('datepicker');
                            //dPicker.selectDate(new Date(moment(data.StartDate)));
                            var date = '2020-07-31';
                            $('.datepicker-Recurr').datepicker("setDate", date)
                        }, 1000);

                        app.initFrequency = data.Frequency;
                        app.initOrderType = data.OrderType;
                        app.customerId = data.CustomerId;
                        app.recurringId = data.Id;
                        app.SelectedProducts = data.RecurrProds;
                        // get sum of msgCount prop across all objects in array
                        var Amount = data.RecurrProds.reduce(function (prev, cur) {
                            return prev + cur.Price;
                        }, 0);
                        app.ChargedAmount = Amount;
                        $.unblockUI();
                      
                    }
                }
                //$.unblockUI();
            })
                .catch(error => {
                    console.log(error.response)
                });;
        },

        updateRecurrDatepicker: function (type) {
            if (type === 'D') {
                if (this.recurringOrderModal.AddressID === 0) {
                    this.recurringOrderModal.AddressID = this.customerAddresses[0].AddressId;
                }
                this.GetPickupOrDeliveryDates(0, true, this.recurringOrderModal.AddressID, 'R');
                this.recurringOrderModal.PickupID = 0;
            } else {
                if (this.recurringOrderModal.PickupID == 0) {
                    this.recurringOrderModal.PickupID = this.pickupLocations[0].ID;
                }

                this.GetPickupOrDeliveryDates(this.recurringOrderModal.PickupID, false, 0, 'R');
                this.recurringOrderModal.AddressID = 0;
            }
            this.recurringOrderModal.StartDate = null
        },

        setAddressType: function () {
            if (this.recurringOrderModal.OrderType === 'Delivery') {
                this.recurringOrderModal.AddressID = this.customerAddresses[0].AddressId;
                this.GetPickupOrDeliveryDates(0, true, this.recurringOrderModal.AddressID);
                this.recurringOrderModal.PickupID = 0;
                this.isDeliveryRecurr = true;
            } else {
                this.recurringOrderModal.PickupID = this.pickupLocations[0].ID;
                this.GetPickupOrDeliveryDates(this.recurringOrderModal.PickupID, false, 0);
                this.recurringOrderModal.AddressID = 0;
                this.isDeliveryRecurr = false;
            }
        },

        manageRecurringOrder: function () {
            this.recurringOrderModal.StartDate ='2020-07-31';
            if (this.recurringOrderModal.StartDate === null) {
                showAlert(null, 'Please select a start date');
                return false;
            }

            if (this.SelectedProducts.length === 0) {
                showAlert(null, 'Please select at least one product for the recurring order.');
                return false;
            }

            this.recurringOrderModal.RecurrProds = app.SelectedProducts;

            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'ManageRecurringOrders'), {
                model: app.recurringOrderModal
            }).then(response => {
                if (response.data.Message === 'Success') {
                    $('#modalStandingOrder').modal('hide');
                    showAlert(null, 'Recurring Order detail saved successfully');
                    $.unblockUI();
                    this.showRecurringOrderModal(); 
                    //setTimeout(function () {
                    //    if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin) {
                    //        $('#modalAddStripe').modal('show');
                    //        return;
                    //    }
                    //}, 500);
                }
                
            });

            console.log(this.recurringOrderModal);
        },

        toggleActive: function (e) {
            var item = $(e.target).toggleClass('active');
        },

        addItemsToTable: function () {
            $('div.list-group a').each(function () {
                var item = $(this);
                if (item.hasClass('active')) {
                    app.SelectedProducts.push(app.AllProducts[item.data('index')]);

                    item.remove();
                }
            });

            console.log(app.SelectedProducts);
            // $('div.list-group a').removeClass('active');
        },

        DeleteRecurringItem: function (id, index) {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'DeleteRecurrOrderItem'), {
                RecurrProdID: id
            }).then(response => {
                if (response.data.Message === 'Success') {
                    var allItems = [];

                    $.each(app.SelectedProducts, function (key, val) {
                        if (key != index) {
                            allItems.push(val);
                        }
                    });

                    app.SelectedProducts = allItems;
                    showAlert(null, 'Deleted');
                } else {
                    showAlert(null, response.data.Message);
                }
                $.unblockUI();
            });
        }
    }
});