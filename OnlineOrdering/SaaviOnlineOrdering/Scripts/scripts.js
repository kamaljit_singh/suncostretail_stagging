$(document).ready(function () {
    lightbox.option({
        'resizeDuration': 200,
        'disableScrolling': true
    })
    // $('[data-toggle="tooltip"]').tooltip();
    $.protip({
        defaults: {
            gravity: true,
            position: 'bottom',
            size: 'small',
            animate: 'slideIn',
            scheme: 'blue'
        }
    });
    $('#myCarousel').carousel({
        interval: 7000
    })
    $('#myCarousel2').carousel({
        interval: 3000
    })

    $('#nav-toggle').click(function () {
        if ($(this).hasClass('navBtnActive')) {
            if ($(window).width() <= 991) {
                $(".navigation-bar").animate({
                    left: "-" + $(".navigation-bar").width(),
                }, 500);
                $(".main-content").animate({
                    width: "100%",
                    left: "0",
                }, 500).css("position", "inherit");
            }
            else {
                $(".navigation-bar").animate({
                    left: "-" + $(".navigation-bar").width(),
                }, 500);
                $(".main-content").animate({
                    width: "100%",
                }, 500);
            }
            $('body').removeClass('nav-opened');
        }
        else {
            if ($(window).width() <= 991) {
                $(".navigation-bar").animate({
                    left: "0",
                }, 500);
                $(".main-content").animate({
                    left: "55%",
                    width: "100%",
                }, 500).css("position", "absolute");
            }
            else {
                $(".navigation-bar").animate({
                    left: "0",
                }, 500);
                $(".main-content").animate({
                    width: $(window).width() - $(".navigation-bar").width(),
                }, 500);
            }
            $('body').addClass('nav-opened');
        }

        $(this).toggleClass('navBtnActive');
    })

    $('.form .icon').click(function () {
        $('body').toggleClass('expanded');
        if ($(this).hasClass('searchActive')) {
            $('.header #search-form-wrapper').animate({ width: "23%" }, 500);
            $('.header #nav-btn-wrapper').animate({ width: "25%" }, 500);
            $('.header #logo-wrapper').animate({ width: "50%" }, 500);
        }
        else {
            $('.header #nav-btn-wrapper').animate({ width: "0" }, 500);
            $('.header #logo-wrapper').animate({ width: "30%" }, 500);
            $('.header #search-form-wrapper').animate({ width: "68%" }, 500);
            $('#txtSearch').focus();
        }
        $(this).toggleClass('searchActive');
        event.stopPropagation();
    });

    $('#cart').click(function () {
        if ($('#itemCount').html() === '') {
            $(".detail-cart").slideToggle(200);
        } else {
            window.location.href = Router.action('Main', 'Cart');
            //@Url.Action("Cart", "Main")
        }
        event.stopPropagation();
    });

    $('#cartclose').click(function () {
        $(".detail-cart").hide(200);
    });

    $('#settings').click(function (event) {
        event.stopPropagation();
        $(".setting-detail").slideToggle(200);
        $('.setting').toggleClass('setting-bg');
    });

    $('html').click(function () {
        if ($('.setting').hasClass('setting-bg')) {
            $(".setting-detail").slideToggle(200);
            $('.setting').toggleClass('setting-bg');
        }
        $('.detail-cart').hide();
    });
});

// Show alerts
function showAlert(title, message) {
    $.alert({
        title: title,
        titleClass: 'text-center fs-19',
        content: '<div class="text-center fs-19">' + message + '</div>',
        animation: 'scaleX',
        closeAnimation: 'scaleX',
        draggable: false,
        animateFromElement: false,
        type: 'dark',
        buttons: {
            okay: {
                text: 'OK',
                btnClass: 'btn-blue',
                keys: ['enter'],
            }
        }
    });
}

// Show alerts
function showAlertRed(title, message) {
    $.alert({
        title: title,
        titleClass: 'text-center fs-19',
        content: '<div class="text-center fs-19">' + message + '</div>',
        animation: 'scaleX',
        closeAnimation: 'scaleX',
        draggable: false,
        animateFromElement: false,
        type: 'blue',
        buttons: {
            okay: {
                text: 'OK',
                btnClass: 'btn-blue',
                keys: ['enter'],
            }
        }
    });
}

function showAlertGuest(title, message) {
    $.alert({
        title: title,
        titleClass: 'text-center fs-19',
        content: '<div class="text-center fs-19">' + message + '</div>',
        animation: 'scaleX',
        closeAnimation: 'scaleX',
        draggable: false,
        animateFromElement: false,
        type: 'dark',
        buttons: {
            okay: {
                text: 'OK',
                btnClass: 'btn-blue',
                keys: ['enter'],
                action: function () {
                    window.location.href = Router.action('Account', 'Register');
                }
            }
        }
    });
}