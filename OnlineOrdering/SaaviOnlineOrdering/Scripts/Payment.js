﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Payment',
        search: '',
        orders: [],
        showOrderStatus: false,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        isPantryOnlySearch: false,
        copyPantry: false,
        isRetail: false,
        pageHelpText: '',
        toBePaidCartIds: [],
        chargeAmount: 0
    },
    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();
        this.loadStripeForm();
        // Get managed features
        //this.getManageFeatures();

        // getCartCount and total order value
        //this.getCartCount();

        // Get customer invoices
        this.getOrders();
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },
        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.showOrderStatus = response.data.ShowOrderStatus;
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.isRetail = response.data.IsRetail;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        loadStripeForm: function () {
            axios.get(Router.action('Main', 'GetStripeKeys'))
                .then(response => {
                    var res = response.data.result

                    var stripe = Stripe(res.Result.PublicKey, {
                        stripeAccount: res.Result.ConnectedAccountID
                    });
                    var elements = stripe.elements();
                    var style = {
                        base: {
                            color: "#32325d",
                        }
                    };

                    var card = elements.create("card", { style: style });
                    card.mount("#stripe-card-element");

                    card.addEventListener('change', function (event) {
                        var displayError = document.getElementById('stripe-card-errors');
                        if (event.error) {
                            displayError.textContent = event.error.message;
                        } else {
                            displayError.textContent = '';
                        }
                    });

                    var form = document.getElementById('stripe-payment-form');
                    form.addEventListener('submit', function (ev) {
                        ev.preventDefault();
                        app.submitStripePayment(stripe, card);
                    });
                });
        },

        // function to get all the pantry list items
        getOrders: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetPaymentOrders'), { params: { token: payToken } }).then(response => {
                this.orders = response.data.Result.Data;

                console.log(this.orders);

                $.unblockUI();
            });
        },

        // Local search for orders
        searchFunction: function () {
            var input, filter, table, tr, td, td2, i, txtValue, txtValue2;
            input = document.getElementById("tblSearch");
            filter = input.value.toUpperCase();
            table = document.getElementById("tblHistory");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                td2 = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    txtValue2 = td2.textContent || td2.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        },

        selectAllChecks: function (event) {
            if (event.target.checked == true) {
                $('input.pay-checkbox').attr('checked', 'checked');
            } else {
                $('input.pay-checkbox').removeAttr('checked');
            }
        },

        makePayment: function (event) {
            var toBePaidCartIds = [], paymentTotal = 0;
            $('input.pay-checkbox').each(function (index, element) {
                if (element.checked == true) {
                    toBePaidCartIds.push(element.value);
                }
            });
            if (toBePaidCartIds.length < 1) {
                alert('Please select some order to pay.');
                return false;
            }
            this.toBePaidCartIds = toBePaidCartIds;
            for (var i = 0; i < this.orders.length; i++) {
                var item = this.orders[i];
                if (toBePaidCartIds.indexOf(item.CartID.toString()) != -1) {
                    paymentTotal += item.Price;
                }
            }
            paymentTotal = (Math.round(paymentTotal * 100) / 100);
            if (paymentTotal <= 1) {
                alert('Payment total must be greater that $1, total for your selected order/s is: $'+ paymentTotal);
                return false;
            }
            this.chargeAmount = paymentTotal;
            $('#paymentTotalForStripe').text(paymentTotal);
            $('#modalAddStripe').modal('show');
            return;
        },

        saveStripeResponse: function (data) {
            axios.post(Router.action('Main', 'SavePaymentLog'), { payResponse: data, tempCartID: 0 }).then(response => {
                console.log(response);
            });
        },

        submitStripePayment: function (stripe, card) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'GetPaymentTokenForPastOrders'), { model: { TotalAmount: this.chargeAmount, OrderIds: this.toBePaidCartIds } }).then(response => {
                //console.log(response);
                var clientSecret = response.data.result;
                stripe.confirmCardPayment(clientSecret, {
                    payment_method: {
                        card: card,
                        //billing_details: {
                        //    name: 'Jenny Rosen'
                        //}
                    }
                }).then(function (result) {
                    if (result === undefined || result === null || result === '') {
                        app.saveStripeResponse('Error Occur During Payment From Web View and result is null.');
                        $('#stripe-card-errors').text('We cannot proceed with order because of payment error. Please contact admin if it is a problem.');
                        $.unblockUI();
                        return;
                    }
                    var respStr = JSON.stringify(result);
                    app.saveStripeResponse(respStr);
                    //console.log(result);
                    if (result.error) {
                        // Show error to your customer (e.g., insufficient funds)
                        //console.log(result.error.message);
                        $('#stripe-card-errors').text(result.error.message);
                        app.saveStripeResponse('Error Occur During Payment From Web View and Error Message :' + result.error.message);
                        $.unblockUI();
                    } else {
                        // The payment has been processed!
                        if (result.paymentIntent.status === 'succeeded' || result.paymentIntent.status === 'requires_capture') {
                            result.paymentIntent.client_secret = btoa(result.paymentIntent.client_secret);
                            //$.unblockUI();
                            $('#modalAddStripe').modal('hide');
                            app.saveFinalPaymentDetails({
                                Status: result.paymentIntent.status,
                                Id: result.paymentIntent.id,
                                Amount: result.paymentIntent.amount,
                                Currency: result.paymentIntent.currency,
                                CustomerId: '',
                                Livemode: result.paymentIntent.livemode,
                                ReceiptEmail: result.paymentIntent.receipt_email,
                                ClientSecret: result.paymentIntent.client_secret
                            });
                        } else {
                            app.saveStripeResponse('Error Occur During Payment From Web View and Payment status :' + result.paymentIntent.status);
                            $('#stripe-card-errors').text('We cannot proceed with order because of payment status: ' + result.paymentIntent.status + '. Please contact admin if it is a problem.');
                            $.unblockUI();
                        }
                    }
                });
            });
        },

        saveFinalPaymentDetails: function (paymentDetails) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'SavePastOrderPaymentDetails'), { model: { TotalAmount: this.chargeAmount, OrderIds: this.toBePaidCartIds, Token: payToken, PaymentDetails: paymentDetails } }).then(response => {
                var res = response.data;
                console.log(res);
                $.alert({
                    title: 'Success!',
                    content: '<span class="fs-19">Payment processed successfully.</span>',
                    type: 'blue',
                    buttons: {
                        Ok: {
                            text: 'Ok',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                location.reload(true);
                            }
                        }
                    }
                });
            }
        }
    }
});