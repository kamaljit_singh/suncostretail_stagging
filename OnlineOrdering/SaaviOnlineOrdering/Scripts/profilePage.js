﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Your Profile',
        search: '',
        userData: [],
        searchProducts: false,
        showOrderHistory: false,
        showInvoices: false,
        isPantryOnlySearch: false,
        copyPantry: false,
        isRetail: false,
        pageHelpText: '',
        isGuest: false,
    },
    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();

        this.getManageFeatures();

        // getCartCount and total order value
        this.getCartCount();

        // Get customer invoices
        this.getUserData();
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.isRetail = response.data.IsRetail;
                    this.isGuest = response.data.IsGuest;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },
        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // function to get all the pantry list items
        getUserData: function () {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetUserProfile')).then(response => {
                this.userData = response.data.Data.Data;

                console.log(this.userData);

                $.unblockUI();
            });
        },
    }
});