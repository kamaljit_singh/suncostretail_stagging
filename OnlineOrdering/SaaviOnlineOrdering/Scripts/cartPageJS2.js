﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Cart',
        search: '',
        cartItems: [],
        selectedUomPrice: [],
        isPlacedByRep: false,
        isSavedOrder: false,
        isPOEnabled: false,
        isMultipleAddresses: false,
        freightCharges: 0,
        nonDeliveryDayCharges: 0,
        deliveryDate: '',
        cartTotal: 0,
        comments: [],
        addresses: [],
        orderDeliveryDate: '',
        newComment: '',
        commentID: 0,
        tempCartID: 0,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        di: null,
        isMobile: false,
        saveOrder: false,
        isPantryOnlySearch: false,
        cutOff: '',
        isDecimal: false,
        currency: '',
        showBIPopup: false,
        biItems: [],
        marketText: false,
        copyPantry: false,
        lastUOM: false,
        backOrder: false,
        moq: false,
        isRetail: false,
        IsStripePaymentAdmin: false,
        IsStripePaymentCustomer: false,
        cartTotalGST: 0,
        IsCapture: false,
        ChargedAmount: '',
        placeOrder: {
            "CustomerID": 0, // server side
            "TempCartID": 0, // on page load
            "UserID": 0, // server side
            "CartID": 0, // server side
            "CommentID": 0,
            "AddressID": 0, // if multiple addresses enabled
            "PONumber": "",
            "OrderStatus": 1,
            "Comment": "",
            "OrderDate": "", // moment
            "CutOffTime": "", // server side
            "ExtDoc": "",
            "PackagingSequence": "", // server side
            "IsAutoOrdered": false,
            "DeviceToken": "webview",
            "DeviceType": "web",
            "DeviceVersion": "Saavi 5",
            "DeviceModel": "",
            "AppVersion": "5",
            "SaveOrder": false,
            "IsInvoiceComment": false,
            "IsUnloadComment": false,
            "InvoiceCommentID": 0,
            "UnloadCommentID": 0,
            "IsOrderPlpacedByRep": false, // server side
            "RunNo": "",
            "HasFreightCharges": false,
            "HasNonDeliveryDayCharges": false,
            "IsDelivery": false,
            "IsLeave": false,
            "IsContactless": false,
            "PickupID": 0,
            "DeliveryType": '',
            "Coupon": ''
        },
        commentModel: {
            "CustomerID": 0,
            "ProductID": 0,
            "IsProduct": true
        },
        datePicker: false,
        prodComments: [],
        commentProdID: 0,
        prodCommentID: 0,
        selectedCartItem: null,
        isChanged: false,
        pageHelpText: '',
        settings: [],
        tempCartID: 0,
        isDelivery: true,
        isPickup: false,
        isLeave: true,
        isContactless: true,
        minCartValue: 0,
        coupon: '',
        couponAmount: 0,
        isCouponApplied: false,
        promoModal: false,
        addressModal: false,
        appliedPromo: '',
        promoResponse: '',
        promoApplySuccess: false,
        customerAddresses: [],
        selectedAddress: ''
    },
    // function to call on page load
    mounted() {
        this.loadStripeForm();
        //
        this.getManageFeatures();

        // getCartCount and total order value
        this.getCartCount();

        // bind Products
        this.getTempCartItems(this.isPlacedByRep, this.isSavedOrder);

        // Get order comments
        this.getComments(0, false);

        // init datepicker
        this.initDatepicker();

        // Set the flag for mobile device
        this.detectmob();

        // Dynamic height

        var divHeight = $('#divCartItems').height();
        var windowHeight = $(window).height() - 270;

        $('#divCartItems').css('height', (windowHeight));

        $(window).resize(function () {
            // console.log('resize');
            var windowHeight = $(window).height() - 275;
            $('#divCartItems').css('height', (windowHeight));
        });

        axios.post(Router.action('Main', 'GetPageHelpText'), { pageName: 'CartPage' })
            .then(response => {
                var res = response.data.Description;
                this.pageHelpText = res;
                console.log(res);
            });

        this.getDeliveryAddresses();
        //$('#modalSUggestiveItems').modal('show');
        //$('#modalPickupDelivery').modal('show');
    },
    methods: {
        detectmob: function () {
            if (navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i)
                || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)
            ) {
                this.isMobile = true;
            }
            else {
                this.isMobile = false;
            }
        },

        //
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.saveOrder = response.data.SaveOrder;
                    this.datePicker = response.data.DatePicker;
                    this.isDecimal = response.data.IsDecimal;
                    this.currency = response.data.Currency;
                    this.showBIPopup = response.data.ShowBIPopUP;
                    this.marketText = response.data.MarketText;
                    this.lastUOM = response.data.ReturnLastUOM;
                    this.backOrder = response.data.BackOrder;
                    this.IsStripePaymentAdmin = response.IsStripePayment;
                    this.isRetail = response.data.IsRetail;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.TotalWithGST;
                        this.ChargedAmount = res.TotalWithGST;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // Get existing order comments
        getComments: function (pID, isProduct) {
            this.commentModel.ProductID = pID;
            this.commentModel.IsProduct = isProduct;

            axios.post(Router.action('Main', 'GetOrderComments'), { model: this.commentModel })
                .then(response => {
                    if (pID > 0) {
                        this.prodComments = response.data.Comments;
                    } else {
                        this.comments = response.data.Comments;
                    }
                });
        },

        // initialize the date picker
        initDatepicker: function () {
            this.di = JSON.parse(window.localStorage.getItem('defaultInfo'));
            this.cutOff = this.di.intro.OrderCutOff;
            $('#date1').text(moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL'));
            $('#date2').text(moment(JSON.stringify(this.di.orderDatesLong[1]), "MM/DD/YYYY").format('LL'));

            $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');

            $('#chkDate1').attr('data-date', this.di.orderDatesLong[0]);
            $('#chkDate2').attr('data-date', this.di.orderDatesLong[1]);

            this.placeOrder.OrderDate = this.di.orderDatesLong[0];
            var availableDays = [];

            $.each(this.di.permittedDays, function (idx, val) {
                if (val === 'Monday') {
                    availableDays.push(1);
                } else if (val === 'Tuesday') {
                    availableDays.push(2);
                } else if (val === 'Wednesday') {
                    availableDays.push(3);
                } else if (val === 'Thursday') {
                    availableDays.push(4);
                } else if (val === 'Friday') {
                    availableDays.push(5);
                } else if (val === 'Saturday') {
                    availableDays.push(6);
                } else if (val === 'Sunday') {
                    availableDays.push(0);
                }
            });

            var minDate = new Date();
            var maxDate = new Date();
            minDate.setDate(minDate.getDate() + 1);
            maxDate.setDate(maxDate.getDate() + 30);
            var todayDay = new Date().getDate();
            var month = new Date().getMonth();
            $('.datepicker-here').datepicker({
                minDate: minDate,
                maxDate: maxDate,
                onSelect: function (formattedDate, date, inst) {
                    $('#chkDate1').prop('checked', false);
                    $('#chkDate2').prop('checked', false);
                    app.deliveryDate = formattedDate;
                    //$('#dateToggle').html('SELECTED DATE: <b>' + moment(formattedDate, "MM/DD/YYYY").format('LL') + '</b>');
                },
                onRenderCell: function (date, cellType) {
                    if (cellType == 'day') {
                        var day = date.getDay(),
                            isDisabled = availableDays.indexOf(day) == -1;
                        var clss = '';
                        if (availableDays.indexOf(day) > -1 && !isDisabled && date.getDate() >= todayDay && date.getMonth() >= month) {
                            clss = 'bgGreen';
                        }
                        return {
                            disabled: isDisabled,
                            classes: clss //isDisabled == true ? 'bgDisabled' :
                        }
                    }
                }
            });
        },

        // function to get all the pantry list items
        getTempCartItems: function (isPlacedByRep, isSavedOrder) {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetTempCartItems'), {
                isPlacedByRep: isPlacedByRep,
                isSavedOrder: isSavedOrder
            }).then(response => {
                this.cartItems = response.data.CartItems;
                this.cartTotal = response.data.CartTotal;
                this.cartTotalGST = response.data.CartTotalGST;
                this.ChargedAmount = response.data.CartTotalGST;
                this.IsCapture = response.data.IsCapture;
                this.IsStripePaymentAdmin = response.data.IstripePaymentAdmin;
                this.IsStripePaymentCustomer = response.data.IsStripePaymentCustomer;

                this.isSavedOrder = response.data.IsSaveOrder;
                this.freightCharges = response.data.IsFreight;
                this.nonDeliveryDayCharges = response.data.IsNonDayDelivery;
                this.isPOEnabled = response.data.IsPONumber;
                this.isMultipleAddresses = response.data.IsMultipleAddresses;
                this.minCartValue = response.data.MinCartValue;
                this.coupon = response.data.Coupon;
                this.couponAmount = response.data.CouponAmount;
                this.isCouponApplied = response.data.IsCouponApplied;

                if (this.cartItems !== undefined && this.cartItems.length > 0) {
                    this.placeOrder.TempCartID = response.data.CartItems[0].CartID;
                    this.tempCartID = response.data.CartItems[0].CartID;

                    this.getSuggestiveItems(this.tempCartID);
                } else {
                    $('#divCartEmpty').html('No items added in the cart!');
                }

                $.each(this.cartItems, function (key, val) {
                    if (val.BuyIn) {
                        app.biItems.push(val);
                    }
                });

                $.unblockUI();
            });
        },

        // function to set price based on UOM DDL change
        setPrice: function (e, idx, dUOM, cartItem) {
            var select = $(e.target);
            var selectedIndex = select.prop('selectedIndex');

            var lastUom = select.data('lastuom');
            var selectedUOM = dUOM[selectedIndex].UOMID;

            if (lastUom === selectedUOM) {
                if (!select.hasClass('lastUom')) {
                    select.addClass('lastUom');
                }
            } else {
                select.removeClass('lastUom');
            }

            var price = dUOM[selectedIndex].Price;
            price = Math.round(price * Math.pow(10, 2)) / Math.pow(10, 2);
            select.parent().prev('li').html('$' + price.toFixed(2));

            if (this.marketText && price === 0) {
                select.parent().prev('li').html('Market ' + this.currency);
            }

            $('#' + idx + '_uom').val(select.prop('selectedIndex'));

            cartItem.OrderUnitId = dUOM[selectedIndex].UOMID;
            cartItem.OrderUnitName = dUOM[selectedIndex].UOMDesc;
            cartItem.Price = price;
            cartItem.UnitsPerCarton = dUOM[selectedIndex].QuantityPerUnit;
            cartItem.CompanyPrice = dUOM[selectedIndex].CostPrice;
            if (price > 0) {
                this.updateCartItem(cartItem);
            }
        },

        // Set the quantity to a hidden field
        setQuantity: function (e, idx) {
            var quantity = $(e.target).val();
            $('#' + idx + '_quantity').val(quantity === '' ? '0' : quantity);
        },

        // Function to delete a cart item
        deleteCartItem: function (cartitemID, product) {
            $.confirm({
                //theme: 'material',
                title: false,
                content: '<span class="fs-19">Delete "<b>' + product + '</b>" from cart?</span>',
                type: 'red',
                buttons: {
                    Delete: {
                        text: 'Delete',
                        btnClass: 'btn-danger',
                        action: function () {
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'DeleteCartItem'), {
                                isDeleteAll: app.isPlacedByRep,
                                cartItemID: cartitemID,
                                isSavedOrder: app.isSavedOrder
                            }).then(response => {
                                let res = response.data;
                                if (res.Message === 'Item deleted') {
                                    $('#cartitem_' + cartitemID).css({ 'background': 'red', 'color': 'white' }).fadeTo("slow", 0.2, function () {
                                        $(this).remove();
                                    });
                                    app.getCartCount();

                                    // reset the promocode related settings
                                    app.coupon = '';
                                    app.couponAmount = 0;
                                    app.isCouponApplied = false;
                                    app.promoApplySuccess = false;

                                    let i = app.biItems.map(item => item.CartItemID).indexOf(cartitemID); // find index of your object
                                    app.biItems.splice(i, 0); // remove it from array
                                }
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //$.alert('Canceled!');
                    }
                }
            });
        },

        // Allow only numeric values in Quantity
        checkNumeric: function (e) {
            var evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var el = $(e.target);

            if (this.isDecimal) {
                if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                    evt.preventDefault();;
                } else {
                    var len = el.val().length;
                    var index = el.val().indexOf('.');
                    if (index >= 0 && charCode == 46) {
                        evt.preventDefault();;
                    }
                    if (index >= 0) {
                        var charAfterdot = (len + 1) - index;
                        if (charAfterdot > 3) {
                            evt.preventDefault();;
                        }
                    }
                }
                return true;
            } else {
                if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode === 46) {
                    evt.preventDefault();;
                } else {
                    return true;
                }
            }
        },

        // Update the order quantity
        updateQuantity: function (e, idx, item, dUOM) {
            var qty = $(e.target).val();
            if (qty > item.StockQuantity && this.backOrder && item.FilterName !== 'Fruit & Veg' && item.FilterName !== 'Processed Veg') {
                $.confirm({
                    title: 'Oops!',
                    content: 'Sorry, Only ' + item.StockQuantity + ' items are currently available. By pressing OK we will submit only the available stock quantity.',
                    type: 'blue',
                    buttons: {
                        add: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                item.Quantity = item.StockQuantity;
                                app.updateCartItem(item);
                            }
                        },
                        cancel: {
                            text: 'CANCEL ITEM',
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                //app.addFavList();
                                $.unblockUI();
                            }
                        },
                    }
                });
            } else {
                item.Quantity = $(e.target).val();
                if (item.Quantity.trim() === '' || item.Quantity.trim() === '0') {
                    showAlert(null, "Quantity can not be blank or 0");
                    $(e.target).val('');
                } else {
                    this.updateCartItem(item);
                }
            }
        },

        // Update the cart item in the database after value change.
        updateCartItem: function (item) {
            this.cartItem = item;

            console.log(item);

            // add code to update cart item to database.
            ajaxStartLoading();
            axios.post(Router.action('Main', 'UpdateCartItem'), { model: item })
                .then(response => {
                    this.getCartCount();
                    $.unblockUI();
                });
        },

        // delete order comment
        deleteComment: function (commentID) {
            $.confirm({
                //theme: 'material',
                title: false,
                content: '<span class="fs-19">Are you sure you want to delete this comment?</span>',
                type: 'red',
                buttons: {
                    Delete: {
                        text: 'Delete',
                        btnClass: 'btn-danger',
                        action: function () {
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'DeleteComment'), {
                                CommentID: commentID,
                                IsProduct: false
                            }).then(response => {
                                var res = response.data;
                                if (res.Message === 'Comment deleted.') {
                                    $('#' + commentID).css({ 'background': 'red', 'color': 'white' }).fadeTo("slow", 0.2, function () {
                                        $(this).remove();
                                    })
                                }
                                console.log(res);
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //$.alert('Canceled!');
                    }
                }
            });
        },

        // Set comment for the order
        getSelectedComment: function (commentID, e, comment) {
            var state = $(e.target).prop('checked');

            $('#divComments').find('input[type=checkbox]').prop('checked', false);

            $(e.target).prop('checked', state);
            this.placeOrder.CommentID = commentID;

            if ($(e.target).prop('checked')) {
                $('#commentHolder').attr('placeholder', comment);
            } else {
                $('#commentHolder').attr('placeholder', 'Add Comment');
                this.placeOrder.CommentID = 0;
            }
        },

        // Add new comment into the database
        addNewComment: function (isProduct, isInvoice) {
            if (this.newComment.trim().length === 0) {
                showAlert(null, 'Please enter a comment');
                return false;
            }

            ajaxStartLoading();

            this.comments = [];
            axios.post(Router.action('Main', 'AddComments'), {
                IsProduct: isProduct,
                IsInvoice: isInvoice,
                CommentDescription: this.newComment,
                ProductID: this.commentProdID
            }).then(response => {
                let res = response.data;
                console.log(res);
                if (res.Message !== 'Success') {
                    showAlert(null, res.Message.replace('!', ''));
                }
                else {
                    if (isInvoice) {
                        $('#txtComment').val('');
                        $('#modalAddComment').modal('hide');
                        this.getComments(0, false);
                    } else {
                        $('#txtProdComment').val('');
                        $('#modalAddProdComment').modal('hide');
                        this.getComments(this.commentProdID, true);
                    }
                }

                $.unblockUI();
            });
        },

        //BI Popup
        continueToPlaceOrder: function (type) {
            //if (this.showBIPopup && this.biItems.length > 0) {
            //    $('#modalOrderIn').modal('show');
            //} else {
            //    this.placeOrderToDB(type);
            //}
            if (!type) {
                if (this.cartTotal < this.minCartValue) {
                    showAlert('Minimum Order Value', 'Sorry, the minimum order value is $' + this.minCartValue + '.00. Please add further items to ensure you can place your order');
                } else {
                    $('#modalPickupDelivery').modal('show');
                }
            }
        },

        loadStripeForm: function () {
            axios.get(Router.action('Main', 'GetStripeKeys'))
                .then(response => {
                    var res = response.data.result

                    var stripe = Stripe(res.Result.PublicKey, {
                        stripeAccount: res.Result.ConnectedAccountID
                    });
                    var elements = stripe.elements();
                    var style = {
                        base: {
                            color: "#32325d",
                        }
                    };

                    var card = elements.create("card", { style: style });
                    card.mount("#stripe-card-element");

                    card.addEventListener('change', function (event) {
                        var displayError = document.getElementById('stripe-card-errors');
                        if (event.error) {
                            displayError.textContent = event.error.message;
                        } else {
                            displayError.textContent = '';
                        }
                    });

                    var form = document.getElementById('stripe-payment-form');
                    form.addEventListener('submit', function (ev) {
                        ev.preventDefault();
                        app.submitStripePayment(stripe, card);
                    });
                });
        },

        saveStripeResponse: function (data) {
            axios.post(Router.action('Main', 'SavePaymentLog'), { payResponse: data, tempCartID: app.placeOrder.TempCartID }).then(response => {
                console.log(response);
            });
        },

        submitStripePayment: function (stripe, card) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'GetPaymentToken'), { PaymentTotal: this.ChargedAmount, IsCapture: this.IsCapture, TempCartID: app.placeOrder.TempCartID }).then(response => {
                //console.log(response);
                var clientSecret = response.data.result;
                stripe.confirmCardPayment(clientSecret, {
                    payment_method: {
                        card: card,
                        //billing_details: {
                        //    name: 'Jenny Rosen'
                        //}
                    }
                }).then(function (result) {
                    var respStr = JSON.stringify(result);
                    app.saveStripeResponse(respStr);
                    //console.log(result);
                    if (result.error) {
                        // Show error to your customer (e.g., insufficient funds)
                        //console.log(result.error.message);
                        $('#stripe-card-errors').text(result.error.message);
                        $.unblockUI();
                    } else {
                        // The payment has been processed!
                        if (result.paymentIntent.status === 'succeeded' || result.paymentIntent.status === 'requires_capture') {
                            result.paymentIntent.client_secret = btoa(result.paymentIntent.client_secret);
                            //$.unblockUI();
                            $('#modalAddStripe').modal('hide');
                            app.placeOrderFinalWithStripe(app.placeOrder, {
                                Status: result.paymentIntent.status,
                                Id: result.paymentIntent.id,
                                Amount: result.paymentIntent.amount,
                                Currency: result.paymentIntent.currency,
                                CustomerId: '',
                                Livemode: result.paymentIntent.livemode,
                                ReceiptEmail: result.paymentIntent.receipt_email,
                                ClientSecret: result.paymentIntent.client_secret
                            });
                        } else {
                            $('#stripe-card-errors').text('We cannot proceed with order because of payment status: ' + result.paymentIntent.status + '. Please contact admin if it is a problem.');
                            $.unblockUI();
                        }
                    }
                });
            });
        },

        // Place order final with stripe or without stripe
        placeOrderFinalWithStripe: function (placeOrder, paymentDetails) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'PlaceOrder'), { model: placeOrder, paymentDetails: paymentDetails }).then(response => {
                var res = response.data;
                console.log(res);

                if (res.Res.Message === 'Order Placed') {
                    $.confirm({
                        columnClass: 'medium',
                        title: false,
                        content: '<span class="fs-19">Order has been placed successfully. Your Order No is ' + res.Res.Data.OrderID + '</span>',
                        type: 'blue',
                        buttons: {
                            Ok: {
                                text: 'Continue',
                                btnClass: 'btn-blue',
                                keys: ['enter', 'shift'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "Order Saved successfully.") {
                    //$.alert('Order saved successfully.');
                    $.alert({
                        title: 'Success!',
                        content: '<span class="fs-19">Order saved successfully</span>',
                        type: 'blue',
                        buttons: {
                            Ok: {
                                text: 'Ok',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "You already have a saved Order. Please delete it or append items to that order.") {
                    showAlert(null, 'You already have a saved Order. Please delete it or append items to that order.');
                } else {
                    showAlert(null, 'Order posting failed. Message : ' + res.Res.Message);
                }
                $.unblockUI();
            });
        },

        // place or save order to db
        placeOrderToDB: function (isSavedOrder) {
            if (this.showBIPopup) {
                $('#modalOrderIn').modal('hide');
            }
            var content = '<h4>Are you sure you want to place the order?</h4>';
            var btnText = 'Place Order';
            if (isSavedOrder) {
                this.placeOrder.SaveOrder = true;
                content = '<h4>Are you sure you want to save this order?</h4>';
                btnText = 'Save Order';
            } else {
                this.placeOrder.SaveOrder = false;
            }

            var placeOrderModel = this.placeOrder;
            if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin) {
                $.confirm({
                    theme: 'material',
                    title: false,
                    content: content,
                    type: 'blue',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                $('#modalAddStripe').modal('show');
                                return;
                            }
                        },
                        cancel: function () {
                            //$.alert('Canceled!');
                        }
                    }
                });
            }
            else {
                $.confirm({
                    theme: 'material',
                    title: false,
                    content: content,
                    type: 'blue',
                    buttons: {
                        placeOrder: {
                            btnClass: 'btn-primary',
                            action: function () {
                                if (app.isPOEnabled && isSavedOrder === false) {
                                    $.confirm({
                                        title: 'PO Number',
                                        type: 'blue',
                                        content: '' +
                                            '<form action="" class="formName">' +
                                            '<div class="form-group">' +
                                            '<label>Enter PO Number</label>' +
                                            '<input type="text" class="poNum form-control" />' +
                                            '</div>' +
                                            '</form>',
                                        buttons: {
                                            formSubmit: {
                                                text: 'OK',
                                                btnClass: 'btn-blue',
                                                action: function () {
                                                    var poNum = this.$content.find('.poNum').val();
                                                    placeOrderModel.PONumber = poNum;
                                                    if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin) {
                                                        $('#modalAddStripe').modal('show');
                                                        return;
                                                    }
                                                    else {
                                                        app.placeOrderFinalWithStripe(placeOrderModel, null);
                                                    }
                                                    //app.placeOrderFinal(placeOrderModel);
                                                }
                                            },
                                            cancel: {
                                                text: 'Skip PO',
                                                action: function () {
                                                    if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin) {
                                                        $('#modalAddStripe').modal('show');
                                                        return;
                                                    }
                                                    else {
                                                        app.placeOrderFinalWithStripe(placeOrderModel, null);
                                                    }
                                                    //app.placeOrderFinal(placeOrderModel);
                                                }
                                            },
                                            skip: {
                                                text: 'Cancel'
                                            }
                                        },
                                        onContentReady: function () {
                                            // bind to events
                                            var jc = this;
                                            this.$content.find('form').on('submit', function (e) {
                                                // if the user submits the form by pressing enter in the field.
                                                e.preventDefault();
                                                jc.$$formSubmit.trigger('click'); // reference the button and click it
                                            });
                                        }
                                    });
                                } else {
                                    if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin) {
                                        $('#modalAddStripe').modal('show');
                                        return;
                                    }

                                    else {
                                        app.placeOrderFinalWithStripe(placeOrderModel, null);
                                    }
                                    //app.placeOrderFinal(placeOrderModel);
                                }
                            }
                        },
                        cancel: function () {
                            //$.alert('Canceled!');
                        }
                    }
                });
            }
        },

        // Place order final
        placeOrderFinal: function (placeOrderModel) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'PlaceOrder'), placeOrderModel).then(response => {
                var res = response.data;
                console.log(res);

                if (res.Res.Message === 'Order Placed') {
                    $.confirm({
                        columnClass: 'medium',
                        title: false,
                        content: '<span class="fs-19">Order has been placed successfully. Your Order No is ' + res.Res.Data.OrderID + '</span>',
                        type: 'blue',
                        buttons: {
                            Ok: {
                                text: 'Continue',
                                btnClass: 'btn-blue',
                                keys: ['enter', 'shift'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "Order Saved successfully.") {
                    //$.alert('Order saved successfully.');
                    $.alert({
                        title: 'Success!',
                        content: '<span class="fs-19">Order saved successfully</span>',
                        type: 'blue',
                        buttons: {
                            Ok: {
                                text: 'Ok',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "You already have a saved Order. Please delete it or append items to that order.") {
                    showAlert(null, 'You already have a saved Order. Please delete it or append items to that order.');
                } else {
                    showAlert(null, 'Order posting failed. Message : ' + res.Res.Message);
                }
                $.unblockUI();
            });
        },

        // Set delivery date from checkboxes
        setDetaultDate: function (e) {
            if ($(e.target).attr('id') === 'modalClose') {
                this.deliveryDate = '';
                $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
                return;
            }

            var state = $(e.target).prop('checked');

            if (this.deliveryDate != '') {
                var myDatepicker = $('.datepicker-here').datepicker().data('datepicker');
                myDatepicker.clear();
            }
            $(e.target).prop('checked', state);

            if ($(e.target).attr('id') == 'chkDate1') {
                $('#chkDate2').prop('checked', false);
                this.deliveryDate = this.di.orderDatesLong[0];
                // $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
            } else {
                $('#chkDate1').prop('checked', false);
                this.deliveryDate = this.di.orderDatesLong[1];
                // $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[1]), "MM/DD/YYYY").format('LL') + '</b>');
            }

            if ($('#chkDate1').prop('checked') === false && $('#chkDate2').prop('checked') === false && this.deliveryDate === '') {
                //$('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
            }

            if ($('#chkDate1').prop('checked') === false && $('#chkDate2').prop('checked') === false) {
                this.deliveryDate = '';
                // $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
            }
        },

        // set delivery date into the Json model
        setDeliveryDate: function () {
            if (this.deliveryDate === '') {
                showAlertRed(null, 'Please choose a delivery date');
            } else {
                $.confirm({
                    title: 'Confirm delivery date',
                    content: 'Is <b>' + moment(app.deliveryDate).format('LL') + '</b> correct?',
                    type: 'blue',
                    buttons: {
                        confirm: {
                            text: 'Yes',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                app.placeOrder.OrderDate = app.deliveryDate;
                                $('#dateToggle').html('SELECTED DATE: <b>' + moment(app.deliveryDate).format('LL') + '</b>');
                                $('#modalDeliveryDate').modal('hide');
                                app.placeOrderToDB(false);
                            }
                        },
                        cancel: function () {
                            $('#modalDeliveryDate').modal('show');
                        }
                    }
                });
            }
        },

        // message to display on web for telephone no.
        mobilePopup: function () {
            showAlertRed(null, 'Call functionality is not available on this device');
        },

        getProductComments: function (pID, cID, item) {
            this.commentProdID = pID;
            this.prodComments = [];
            this.selectedCartItem = item;
            this.isChanged = false;
            ajaxStartLoading();

            this.getComments(pID, true);
            $('#modalProdComments').modal('show');

            setTimeout(function () {
                $.unblockUI();
                var chkp = $('#chkp_' + cID);
                if (chkp != undefined) {
                    chkp.prop('checked', true);
                }
            }, 1000);
        },

        setProductComment: function (e) {
            var state = $(e.target).prop('checked');
            $('#divProdComments').find('input[type=checkbox]').prop('checked', false);
            $(e.target).prop('checked', state);
            this.isChanged = true;

            if (state) {
                this.prodCommentID = $(e.target).data('cid');
            } else {
                this.prodCommentID = 0;
            }
        },

        updateProductComment: function () {
            //var sel = $('#divProdComments').find('input[type=checkbox]').prop('checked', true);

            var item = this.selectedCartItem;
            if (this.isChanged) {
                item.ProdCommentID = this.prodCommentID;
                this.updateCartItem(item);
            }
            $('#modalProdComments').modal('hide');
        },

        getSuggestiveItems: function (cartID) {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetSuggestiveItems'), {
                cartID: cartID
            }).then(response => {
                console.log(response);
            });
        },

        checkDeliveryOrPickup: function () {
            if (this.isDelivery && !this.isLeave) {
                showAlert(null, "Sorry, but we can't do deliveries without having your permission to leave");
            } else if (!$('#cbPickup').prop('checked') && !$('#cbDelivery').prop('checked')) {
                showAlert(null, "Please choose one delivery option");
            } else {
                $('#modalPickupDelivery').modal('hide');

                if (this.isDelivery) {
                    this.placeOrder.IsDelivery = true;
                    this.placeOrder.IsLeave = this.isLeave;
                    this.placeOrder.IsContactless = this.isContactless;
                    this.placeOrder.DeliveryType = 'Delivery';
                    if (this.isMultipleAddresses) {
                        this.addressModal = true;
                    } else {
                        $('#modalDeliveryDate').modal('show');
                    }
                } else {
                    this.placeOrder.DeliveryType = 'Pick Up';

                    $('#modalDeliveryDate').modal('show');
                }
            }
        },

        updateDeliveryOptions: function (ctrl, type) {
            var val = $(ctrl.target).prop('checked');

            if (type === 'L') {
                this.isLeave = val;
            } else if (type === 'D') {
                this.isDelivery = val;
                this.isPickup = false;
            } else if (type === 'P') {
                this.isPickup = val;
                this.isDelivery = false;
            } else {
                this.isContactless = val;
            }
        },

        promoModalClose: function () {
            this.promoApplySuccess = false;
            this.promoModal = false;
            this.appliedPromo = '';
            this.promoResponse = '';
        },

        applyPromoCode: function () {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'ApplyPromoCode'), {
                cartID: app.tempCartID,
                promoCode: app.appliedPromo
            }).then(response => {
                console.log(response);
                var res = response.data;
                if (res.Message === 'Success') {
                    var promo = res.Result;
                    if (promo.TotalAmountWithDiscount < promo.TotalWithGst) {
                        this.isCouponApplied = true;
                        this.couponAmount = promo.TotalWithGst - promo.TotalAmountWithDiscount;
                        this.promoApplySuccess = true;
                        //this.cartTotal = promo.TotalAmountWithDiscount;
                    } else {
                        //this.isCouponApplied = false;
                        //this.couponAmount = 0;
                        this.promoResponse = promo.Message;
                        this.appliedPromo = '';
                    }
                }
                $.unblockUI();
            });
            //alert(this.appliedPromo);
        },

        getDeliveryAddresses: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerAddresses')).then(response => {
                if (response.data.Message === 'Success') {
                    this.customerAddresses = response.data.Result;
                    //  this.addressModal = true;
                }
            });
        },

        saveAddress: function () {
            if (this.selectedAddress === '') {
                showAlert(null, 'Sorry, you must select at least one delivery location');
            } else {
                this.placeOrder.AddressID = this.selectedAddress;
                $('#modalDeliveryDate').modal('show');
                this.addressModal = false;
                console.log(this.placeOrder.AddressID);
            }
        }
    }
});