﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Favourite lists',
        search: '',
        favLists: [],
        isPdfInvoice: true,
        searchProducts: false,
        showOrderHistory: false,
        showInvoices: false,
        addPantry: false,
        listName: '',
        isPantryOnlySearch: false,
        copyPantry: false,
        isRetail: false,
        pantryModel: {
            "PantryListID": 0,
            "CustomerID": 0,
            "PantryListName": "",
            "IsCreatedByRepUser": false,
            "IsActive": true,
            "PantryListType": "C",
            "IsSynced": false,
            "IsSorted": false,
            "IsCopy": false,
        },
        pageHelpText: ''
    },
    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();
        // getCartCount and total order value
        this.getCartCount();

        // Get customer invoices
        this.getFavLists();

        this.getManageFeatures();
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.addPantry = response.data.AddPantryList;
                    this.isRetail = response.data.IsRetail;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // function to get all the pantry list items
        getFavLists: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerPantryLists')).then(response => {
                this.favLists = response.data.Result.Data;

                if (this.favLists === null || this.favLists.length === 0) {
                    $('#noLists').html('No favourite list available');
                }
                $.unblockUI();
            });
        },

        loadPantry: function (name, id) {
            var pantryData = {
                PantryID: id,
                Name: name
            }

            window.localStorage.setItem('PantryData', JSON.stringify(pantryData));

            window.location.href = Router.action('Main', 'DefaultPantry');
        },

        // delete pantry list
        deletePantry: function (pantryID, pantryName) {
            $.confirm({
                theme: 'material',
                title: false,
                type: 'red',
                content: '<span class="fs-19">Delete favourite list <b>' + pantryName + '</b>?</span>',
                buttons: {
                    Delete: {
                        text: 'Delete',
                        btnClass: 'btn-danger',
                        action: function () {
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'DeletePantryList'), {
                                pantryListID: pantryID
                            }).then(response => {
                                let res = response.data;
                                if (res.Message === 'Favorite List deleted successfully.') {
                                    $('#' + pantryID).css({ 'background': 'red', 'color': 'white' }).fadeTo("slow", 0.2, function () {
                                        $(this).remove();
                                    })
                                }
                                console.log(res);
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                    }
                }
            });
        },

        // add Pantry list
        addNewList: function () {
            if (this.listName.trim().length === 0) {
                showAlert(null, 'Please enter favourite list name');
                return false;
            }

            ajaxStartLoading();
            this.pantryModel.PantryListName = this.listName;
            axios.post(Router.action('Main', 'AddPantry'), this.pantryModel)
                .then(response => {
                    console.log(response);
                    if (response.data.Message == 'Success') {
                        $('#txtName').val('');
                        $('#modalAddList').modal('hide');
                        this.getFavLists();
                    } else {
                        showAlert(null, response.data.Message);
                    }
                    $.unblockUI();
                });
        }
    }
});