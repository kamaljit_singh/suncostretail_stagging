﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'favourite list',
        filters: [],
        products: [],
        schema: '',
        isSearchMainProducts: false,
        selectedUomPrice: [],
        selectedFilter: 0,
        filteredCount: 0,
        pageSize: 20,
        pageIndex: 0,
        totalPages: 0,
        showAll: false,
        search: '',
        showSupplier: false,
        showBrand: false,
        longDetail: false,
        showPrice: false,
        addToDefaultPantry: false,
        highlightStock: false,
        pictureView: false,
        showFilters: false,
        showHistory: false,
        itemEnquiry: false,
        nonFoodVersion: false,
        highlightRewardItem: false,
        showFavoriteIcon: false,
        showUnitPerCtn: false,
        showCountOnAll: false,
        productLongDetail: false,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        addToDefaultPantry: false,
        showDefImage: true,
        isPantryOnlySearch: false,
        favouriteLists: [],
        dragDisabled: false,
        isDecimal: false,
        currency: '',
        filterID: 0,
        marketText: false,
        copyPantry: false,
        lastUOM: false,
        backOrder: false,
        pantrySort: false,
        isRetail: false,
        isGuest: false,
        addPantryItemModel: {
            "PantryItemID": 0,
            "PantryListID": 0,
            "ProductID": 0,
            "Quantity": 0.0,
            "IsActive": false,
            "IsCore": false,
            "CustomerID": 0
        },
        pantryModel: {
            "PantryListID": 0,
            "CustomerID": 0,
            "PantryListName": "",
            "IsCreatedByRepUser": false,
            "IsActive": true,
            "PantryListType": "C",
            "IsSynced": false,
            "IsSorted": false,
            "IsCopy": false,
        },
        tempCartInsertionModel: {
            "RunNo": "",
            "CommentLine": "",
            "PackagingSequence": "",
            "CartItem": {
                "ProductID": 0,
                "Quantity": 0,
                "Price": 0,
                "ProductWeight": 0,
                "WeightName": "",
                "WeightDescription": "",
                "IsPieceOrWeight": "",
                "IsSpecialPrice": false,
                "CommentID": 0,
                "UnitId": 0,
                "IsGstApplicable": false,
                "ItemPricePerUnit": 0,
                "OrderedQuantity": 0,
                "RecievedQuantity": 0,
                "UnitsPerCarton": 0,
                "BasePrice": 0,
                "IsDiscountApplicable": false,
                "IsBoxDiscount": false,
                "PriceType": "",
                "IsNoPantry": false
            },
            "IsSavedOrder": true,
            "Warehouse": "",
            "AddressID": 0
        },
        delPantryItem: {
            "PantryItemID": 0,
            "PantryListID": 0,
            "CustomerID": 0
        },
        sortModel: {
            "PantryListID": 0,
            "ProductID": []
        },
        pageHelpText: '',
        pantryDelete: false
    },
    // function to call on page load
    mounted: function () {
        $('#loader').remove();
        $('#appMain').show();

        $('#divMainProducts').css('height', (window.innerHeight - 200).toString() + 'px');
        //alert(window.innerHeight - 120);

        // bind the product filters
        axios.get(Router.action('Main', 'GetFilters'))
            .then(response => {
                this.filters = response.data.Filters;
                this.favouriteLists = response.data.PantryLists.Data;
                console.log(this.favouriteLists);
            });

        //Get Managed Features for the customer
        this.getManageFeatures();

        // bind the product filters
        axios.get(Router.action('Main', 'GetCartCount'))
            .then(response => {
                var res = response.data

                if (res.CartCount > 0) {
                    $('#itemCount').html(res.CartCount);
                    $('#itemCount').show();
                };
            });

        // search if query string has key
        var key = GetQueryStringParams("key");
        var pantry = GetQueryStringParams("pantry");

        if (key != undefined) {
            this.isSearch = true;
            this.search = key.replace(/%20/g, ' ');
            this.isPantryOnlySearch = true;
            this.getPantryListItems(this.pageSize, 0, this.showAll, key, 0, 0);
        } else {
            // bind Products
            this.getPantryListItems(this.pageSize, this.pageIndex, this.showAll, '', 0, 0);
        }

        // Get page Help Text
        axios.post(Router.action('Main', 'GetPageHelpText'), { pageName: 'DefaultPantry' })
            .then(response => {
                var res = response.data.Description;
                this.pageHelpText = res;
                console.log(res);
            });
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (!this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "SearchProducts") + '?key=' + app.search;
                    } else {
                        $('#txtSearch').val('');
                        this.pageIndex = 0;
                        this.products = [];
                        this.getPantryListItems(this.pageSize, this.pageIndex, this.showAll, this.search, 0, 0);
                    }
                } else {
                    this.getPantryListItems(this.pageSize, this.pageIndex, this.showAll, '', 0, 0);
                }
            }
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    console.log(response);
                    this.showBrand = response.data.ShowBrand;
                    this.showSupplier = response.data.ShowSupplier;
                    this.showPrice = response.data.ShowPrice;
                    this.pictureView = response.data.PictureView;
                    this.showHistory = response.data.ShowHistory;
                    this.longDetail = response.data.LongDetail;
                    this.highlightRewardItem = response.data.HighlightRewardItems;
                    this.nonFoodVersion = response.data.NonFoodVersion;
                    this.showFavoriteIcon = response.data.ShowFavoriteIcon;
                    this.showUnitPerCtn = response.data.ShowUnitsPerCarton;
                    this.itemEnquiry = response.data.ItemEnquiry;
                    this.showCountOnAll = response.data.ShowCountOnAll;
                    this.highlightStock = response.data.HighlightStock;
                    this.productLongDetail = response.data.ProductLongDetail;
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.addToDefaultPantry = response.data.AddItemToDefaultPantry;
                    this.isDecimal = response.data.IsDecimal;
                    this.currency = response.data.Currency;
                    this.marketText = response.data.MarketText;
                    this.copyPantry = response.data.CopyPantry;
                    this.lastUOM = response.data.ReturnLastUOM;
                    this.backOrder = response.data.BackOrder;
                    this.pantrySort = response.data.PantrySorting;
                    this.isRetail = response.data.IsRetail;
                    this.isGuest = response.data.IsGuest;
                    this.pantryDelete = response.PantryDelete;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }

                    if (this.nonFoodVersion) {
                        this.title = 'favourite list';
                    } else {
                        //this.title = 'your pantry';
                    }

                    var p = window.localStorage.getItem('PantryData');
                    if (p != null || p != undefined) {
                        var pData = JSON.parse(p);
                        this.title = pData.Name;
                        this.addToDefaultPantry = false;
                        this.pantryModel.PantryListID = pData.PantryID;
                    }
                    window.localStorage.removeItem('PantryData');
                });
        },

        // function to get all the pantry list items
        getPantryListItems: function (pageSize, pageIndex, showAll, searchText, pantrylistID, fID) {
            //pantrylistID = 0;
            var p = window.localStorage.getItem('PantryData');

            if (p != null || p != undefined) {
                var pData = JSON.parse(p);
                pantrylistID = pData.PantryID;
                //this.title = pData.Name;
            }

            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetPantryListItems'), {
                pageSize: pageSize,
                pageIndex: pageIndex,
                showAll: showAll,
                pantryListID: pantrylistID,
                search: searchText,
                filterID: fID
            }).then(response => {
                var totalProducts = response.data.totalProducts;

                if (this.products.length == 0) {
                    this.products = response.data.pantryItems;
                    this.totalPages = response.data.totalPages;

                    if (this.products != undefined) {
                        this.pantryModel.PantryListID = this.products[0].PantryListID;
                    }
                    if (response.data.pantryItems == undefined && this.search.length > 0) {
                        $('#divNoProducts p').html('<h3 class="text-center" style="margin:15% auto;color:#000 !important;font-weight:500;">No products found</h3>');
                    } else {
                        $('#divNoProducts p').html('');
                    }
                    if (totalProducts === undefined) {
                        $('#divNoProducts p').html('<h3 class="text-center" style="margin:15% auto;color:#000 !important;font-weight:500;">No products found</h3>');
                    }

                    if (this.showCountOnAll && fID === 0 && totalProducts !== undefined) {
                        setTimeout(function () {
                            $('#0_filter').next('span').remove();
                            $('#0_filter').after('<span class="number">' + totalProducts + '</span>');
                        }, 200);
                    };

                    var state = app.$vuebar.getState(app.$refs.mainProductDiv);
                    state.el2.scrollTop = 0;
                } else {
                    let P2 = this.products.concat(response.data.pantryItems);
                    this.products = P2;
                    if (this.showCountOnAll && totalProducts !== undefined) {
                        setTimeout(function () {
                            $('#0_filter').next('span').html(totalProducts);
                        }, 200);
                    };
                }

                setTimeout(function () {
                    this.filteredCount = $('.detail-bar').length;
                    $('.nav-tabs li span').remove();
                    if (fID >= 0 && this.filteredCount > 0) {
                        if (fID > 0) {
                            $('#' + fID + '_filter').after('<span class="number">' + totalProducts + '</span>')// remove();
                        }
                        if (fID === 0 && app.showCountOnAll) {
                            $('#' + fID + '_filter').after('<span class="number">' + totalProducts + '</span>')// remove();
                        }
                        $('#divNoProducts p').html('');
                    } else {
                        if (fID > 0) {
                            $('#divNoProducts p').html('<h3 class="text-center" style="margin:15% auto;color:#000 !important;font-weight:500;">No products found</h3>');
                        }
                    }

                    if (app.pantrySort) {
                        var el = document.getElementById('items');
                        var sortable = Sortable.create(el, {
                            onEnd: function (/**Event*/evt) {
                                var products = sortable.toArray();

                                products.splice(products.length - 1);

                                app.sortModel.PantryListID = app.pantryModel.PantryListID;
                                app.sortModel.ProductID = products;

                                //Code to save the pantry list sort order
                                app.savePantrySort(app.sortModel);
                            },
                        });
                    };
                }, 50);

                $.unblockUI();
            });
        },

        // function to set price based on UOM DDL change
        setPrice: function (e, idx, dUOM) {
            var select = $(e.target);
            var selectedIndex = select.prop('selectedIndex');

            var lastUom = select.data('lastuom');
            var selectedUOM = dUOM[selectedIndex].UOMID;

            if (lastUom === selectedUOM) {
                if (!select.hasClass('lastUom')) {
                    select.addClass('lastUom');
                }
            } else {
                select.removeClass('lastUom');
            }

            var price = dUOM[selectedIndex].Price;
            price = Math.round(price * Math.pow(10, 2)) / Math.pow(10, 2);
            $('#' + idx + 'priceSpan').html('$' + price.toFixed(2));

            if (this.marketText && price === 0) {
                $('#' + idx + 'priceSpan').html('Market ' + this.currency);
            }

            //select.next('span').html('$' + price);

            $('#' + idx + '_uom').val(select.prop('selectedIndex'));

            if (this.showUnitPerCtn) {
                if (dUOM[selectedIndex].UOMDesc.toLowerCase() === 'ctn' || dUOM[selectedIndex].UOMDesc.toLowerCase() === 'carton') {
                    $('#' + idx + '_upc').html('[U/CTN=' + dUOM[selectedIndex].QuantityPerUnit + ']');
                    $('#' + idx + '_upc').removeClass('hidden');
                } else {
                    $('#' + idx + '_upc').addClass('hidden');
                }
            }
        },

        checkNumeric: function (e) {
            var evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var el = $(e.target);

            if (this.isDecimal) {
                if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                    evt.preventDefault();;
                } else {
                    var len = el.val().length;
                    var index = el.val().indexOf('.');
                    if (index >= 0 && charCode == 46) {
                        evt.preventDefault();;
                    }
                    if (index >= 0) {
                        var charAfterdot = (len + 1) - index;
                        if (charAfterdot > 3) {
                            evt.preventDefault();;
                        }
                    }
                }
                return true;
            } else {
                if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode === 46) {
                    evt.preventDefault();;
                } else {
                    return true;
                }
            }
        },

        // Set the quantity to a hidden field
        setQuantity: function (e, idx, product) {
            //var evt = (evt) ? evt : window.event;
            //var charCode = (evt.which) ? evt.which : evt.keyCode;
            //if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            //    //evt.preventDefault();;
            //} else {
            var quantity = $(e.target).val();
            $('#' + idx + '_quantity').val(quantity === '' ? '0' : quantity);

            if (e.keyCode === 13) {
                this.addItemToTempCart(product, idx);
                $('#' + (idx + 1) + '_qtyInput').focus();
            }
            $('#' + idx + '_uom').val($('#' + idx + '_ddl option:selected').index());
        },

        // Filter the products
        getFilteredProducts: function (e, filter) {
            this.products = [];
            this.filterID = filter;
            this.pageIndex = 0;
            console.log(this.pantryModel.PantryListID);
            this.getPantryListItems(this.pageSize, this.pageIndex, this.showAll, '', this.pantryModel.PantryListID, this.filterID);
        },

        // Add item to temp cart
        addItemToTempCart: function (prod, index) {
            if (this.isGuest) {
                showAlertGuest(null, 'In order to use this feature, please register with Suncoast Fresh.');
                return false;
            }

            if (prod.StockQuantity <= 0 && this.backOrder && prod.FilterId !== 9 && prod.FilterId !== 4 && prod.CategoryId !== 29) {
                showAlert('Oops!', 'Unfortunately, there is not sufficient stock available to complete your order.');
                return false;
            }

            // show the loading
            ajaxStartLoading();
            var qty = parseFloat($('#' + index + '_quantity').val());
            var existing = $('#' + index + '_addToCart').hasClass('green-border-btn');

            if (qty === 0) {
                showAlertRed(null, 'Please enter order quantity');
                // End loading
                $.unblockUI();
                return false;
            }

            if (prod.BuyIn && !existing) {
                $.confirm({
                    title: '',
                    content: 'This item is an ORDER IN ONLY  item. WILL DELIVER ASAP.',
                    type: 'dark',
                    buttons: {
                        add: {
                            text: 'I Acknowledge',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                app.addToDB(prod, index);
                            }
                        },
                        cancel: {
                            text: 'Cancel Item',
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                //app.addFavList();
                                $.unblockUI();
                            }
                        },
                    }
                });
            } else {
                if (qty > prod.StockQuantity && this.backOrder && prod.FilterId !== 9 && prod.FilterId !== 4 && prod.CategoryId !== 29) {
                    $.confirm({
                        title: 'Oops!',
                        content: 'Sorry, Only ' + prod.StockQuantity + ' items are currently available. By pressing OK we will submit only the available stock quantity.',
                        type: 'dark',
                        buttons: {
                            add: {
                                text: 'OK',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    app.addToDB(prod, index);
                                }
                            },
                            cancel: {
                                text: 'CANCEL ITEM',
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    //app.addFavList();
                                    $.unblockUI();
                                }
                            },
                        }
                    });
                } else {
                    app.addToDB(prod, index);
                }
            }
        },

        addToDB: function (prod, index) {
            var btn = $('#' + index + '_addToCart');
            var qty = parseFloat($('#' + index + '_quantity').val());
            var price = parseInt($('#' + index + '_uom').val());
            var unitName = '';

            if (qty > prod.StockQuantity && this.backOrder && prod.FilterId !== 9 && prod.FilterId !== 4 && prod.CategoryId !== 29) {
                qty = prod.StockQuantity;
                $('#' + index + '_qtyInput').val(qty);
            }

            this.tempCartInsertionModel.CartItem.ProductID = prod.ProductID;
            this.tempCartInsertionModel.CartItem.Quantity = qty;
            this.tempCartInsertionModel.CartItem.IsGstApplicable = prod.IsGST;

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.Price = prod.DynamicUOM[price].Price;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.Price = prod.Prices.Price;
                } else {
                    this.tempCartInsertionModel.CartItem.Price = prod.Price;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.IsSpecialPrice = prod.DynamicUOM[price].IsSpecial;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.IsSpecialPrice = prod.Prices.IsSpecial;
                } else {
                    this.tempCartInsertionModel.CartItem.IsSpecialPrice = false;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.UnitId = prod.DynamicUOM[price].UOMID;
            } else {
                this.tempCartInsertionModel.CartItem.UnitId = prod.UOMID;
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.DynamicUOM[price].QuantityPerUnit;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.Prices.QuantityPerUnit;
                } else {
                    this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.UnitsPerCarton;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.BasePrice = prod.DynamicUOM[price].CostPrice;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.BasePrice = prod.Prices.CostPrice;
                } else {
                    this.tempCartInsertionModel.CartItem.BasePrice = prod.CompanyPrice;
                }
            }

            if (prod.DynamicUOM != null) {
                unitName = prod.DynamicUOM[price].UOMDesc;
            } else {
                if (prod.Prices != null) {
                    unitName = prod.Prices.UOMDesc;
                } else {
                    unitName = prod.UOMDesc;
                }
            }

            this.tempCartInsertionModel.CartItem.OrderedQuantity = qty;
            this.tempCartInsertionModel.CartItem.RecievedQuantity = 0;
            this.tempCartInsertionModel.CartItem.IsDiscountApplicable = false;
            this.tempCartInsertionModel.CartItem.IsPieceOrWeight = false;
            this.tempCartInsertionModel.CartItem.WeightName = '';
            this.tempCartInsertionModel.CartItem.IsNoPantry = false;

            // Send the data back to controller for processing
            axios.post(Router.action('Main', 'InsertUpdateTempCart'), this.tempCartInsertionModel).then(response => {
                if (response.data.Result) {
                    if (response.data.CartCount > 0) {
                        $('#itemCount').html(response.data.CartCount);
                        $('#itemCount').show();

                        if (!btn.hasClass('green-border-btn')) {
                            btn.removeAttr('class');
                            btn.addClass('green-border-btn');
                        }
                    }
                    showAlert(prod.ProductName, qty + ' ' + unitName + ' added to cart');
                } else {
                    showAlert(prod.ProductName, response.data.Message);
                }

                // End loading
                $.unblockUI();
            });
        },

        // Function to be called for scroll based paging
        scrolledBottom: function () {
            // Use paging only when the ALL products is selected.
            if (0 === 0) {
                this.pageIndex = this.pageIndex + 1;

                if (this.pageIndex <= (this.totalPages - 1)) {
                    ajaxStartLoading();
                    console.log(this.pageIndex);
                    this.getPantryListItems(this.pageSize, this.pageIndex, this.showAll, '', 0, this.filterID);
                }
            }
            console.log("Bottom");
        },

        // Show default image if Productimage not found
        showDefault: function (e) {
            $(e.target).attr('src', this.schema + '/content/images/no-product.png');
        },

        // show item enquiry popup
        inquiryPopup: function (pID) {
            // prompt
            $.confirm({
                title: 'Item Enquiry',
                type: 'dark',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Enter enquiry details:</label>' +
                    '<textarea class="comment form-control" style="resize:none;" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',
                        action: function () {
                            var comment = this.$content.find('.comment').val();
                            if (!comment) {
                                showAlert(null, 'Provide detail about the enquiry')
                                return false;
                            }
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'SendProductEnquiry'), {
                                productID: pID,
                                comment: comment
                            }).then(response => {
                                showAlert(null, 'Your enquiry has been sent');
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        },

        // Set productid for fav list
        setProductID: function (pID, remove) {
            if (remove) {
                this.addPantryItemModel.ProductID = 0;
                $('#divFavLists input[type="checkbox"]').prop('checked', false);
            } {
                if (this.favouriteLists === null || this.favouriteLists.length === 0) {
                    $.confirm({
                        title: '',
                        content: 'No favourite lists exist. Do you want to create a new one?',
                        type: 'dark',
                        buttons: {
                            add: {
                                text: 'Yes',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    app.addFavList();
                                }
                            },
                            no: function () {
                                app.addPantryItemModel.ProductID = 0;
                            },
                        }
                    });
                    return;
                } else {
                    this.addPantryItemModel.ProductID = pID;
                    $('#modalFavList').modal('show');
                }
            }
        },

        // Add item to fav list
        addItemToFav: function () {
            if ($('#divFavLists input[type="checkbox"]').filter(':checked').length === 0) {
                showAlert(null, 'Please choose a favourite list');
            } else {
                ajaxStartLoading();
                // bind Products
                axios.post(Router.action('Main', 'AddItemToPantry'), this.addPantryItemModel).then(response => {
                    if (response.data.Message == 'Success') {
                        showAlert(null, "Product added to your favourite list")
                    } else {
                        showAlert(null, response.data.Message);
                    }
                    $('#modalFavList').modal('hide');
                    $('#divFavLists input[type="checkbox"]').prop('checked', false);
                    $.unblockUI();
                });
            }
        },

        setFavList: function (e, favID) {
            var state = $(e.target).prop('checked');
            this.addPantryItemModel.PantryListID = favID;
            $('#divFavLists input[type="checkbox"]').prop('checked', false);
            $(e.target).prop('checked', state);
        },

        setPropForAddDefault: function () {
            window.sessionStorage.setItem('AddtoDefaultPantry', 1);
            window.location.href = Router.action('Main', 'SearchProducts');
        },

        // function to get all the pantry list items
        getFavLists: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerPantryLists')).then(response => {
                this.favouriteLists = response.data.Result.Data;
                $.unblockUI();
            });
        },

        // add fav list if none exist while adding product
        addFavList: function () {
            // prompt
            $.confirm({
                title: 'Add Favourite List',
                type: 'dark',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Favourite list name</label>' +
                    '<input type="text" placeholder="Enter favourite list name" class="fav form-control" style="resize:none;" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',
                        action: function () {
                            var fav = this.$content.find('.fav').val();
                            if (!fav) {
                                showAlert(null, 'Please enter favourite list name');
                                return false;
                            }
                            app.pantryModel.PantryListName = fav;
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'AddPantry'), app.pantryModel)
                                .then(response => {
                                    console.log(response);
                                    if (response.data.Message == 'Success') {
                                        app.getFavLists();
                                        $('#modalFavList').modal('show');
                                    } else {
                                        showAlert(null, response.data.Message);
                                    }
                                    $.unblockUI();
                                });
                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        },

        // Copy pantry list
        copyPantryList: function () {
            if (this.pantryModel.PantryListID === 0) {
                showAlert(null, "Customer does not have a pantry list");
            } else {
                $.confirm({
                    title: 'Copy Pantry List',
                    type: 'dark',
                    animation: 'scaleX',
                    closeAnimation: 'scaleX',
                    draggable: false,
                    animateFromElement: false,
                    content: '' +
                        '<form action="" class="formName">' +
                        '<div class="form-group">' +
                        '<label>Pantry Name:</label>' +
                        '<input type="text" class="comment form-control" required />' +
                        '</div>' +
                        '</form>',
                    buttons: {
                        formSubmit: {
                            text: 'Submit',
                            btnClass: 'btn-blue',
                            action: function () {
                                var comment = this.$content.find('.comment').val();
                                if (!comment) {
                                    showAlert(null, 'Enter pantry list name')
                                    return false;
                                }
                                ajaxStartLoading();

                                app.pantryModel.PantryListName = comment;
                                app.pantryModel.IsCopy = true;

                                axios.post(Router.action('Main', 'AddPantry'), app.pantryModel)
                                    .then(response => {
                                        console.log(response);
                                        if (response.data.Message == 'Success') {
                                            showAlert(null, "Pantry list added");
                                        } else {
                                            showAlertRed(null, response.data.Message);
                                        }
                                        $.unblockUI();
                                    });
                            }
                        },
                        cancel: function () {
                            //close
                        },
                    },
                    onContentReady: function () {
                        // bind to events
                        var jc = this;
                        this.$content.find('form').on('submit', function (e) {
                            // if the user submits the form by pressing enter in the field.
                            e.preventDefault();
                            jc.$$formSubmit.trigger('click'); // reference the button and click it
                        });
                    }
                });
            }
        },

        // Delete an item from Pantry list
        deletePantryItem: function (product) {
            app.delPantryItem.PantryItemID = product.PantryItemID;
            app.delPantryItem.PantryListID = product.PantryListID;

            console.log(app.delPantryItem);
            $.confirm({
                //theme: 'material',
                title: false,
                content: '<span class="fs-19">Delete "<b>' + product.ProductName + '</b>" from pantry?</span>',
                type: 'red',
                buttons: {
                    Delete: {
                        text: 'Delete',
                        btnClass: 'btn-danger',
                        action: function () {
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'DeletePantryListItem'), {
                                model: app.delPantryItem
                            }).then(response => {
                                let res = response.data;
                                if (res === 'Success') {
                                    let i = app.products.map(item => item.ProductID).indexOf(product.ProductID); // find index of your object
                                    app.products.splice(i, 1) // remove it from array
                                } else {
                                    showAlertRed(null, res);
                                }
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //$.alert('Canceled!');
                    }
                }
            });
        },

        // Save the sort order of the panty items
        savePantrySort: function (sortData) {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'SavePantrySortOrder'), { model: sortData }).then(response => {
                //console.log(response.data.Result);
                $.unblockUI();
            });
        }
    },
});