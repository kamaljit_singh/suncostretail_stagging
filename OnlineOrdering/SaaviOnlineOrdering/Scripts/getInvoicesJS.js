﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Invoices',
        search: '',
        invoices: [],
        isPdfInvoice: true,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        isPantryOnlySearch: false,
        copyPantry: false,
        getInvoicesModel: {
            "IsPdfListing": true,
            "Email": "",
            "FromDate": "",
            "TillDate": ""
        },
        pageHelpText: ''
    },
    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();
        this.getManageFeatures();
        // getCartCount and total order value
        this.getCartCount();

        // Get customer invoices
        this.getInvoices();
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                });
        },
        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // function to get all the pantry list items
        getInvoices: function () {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetInvoices'), this.getInvoicesModel).then(response => {
                this.invoices = response.data.Result.Data;

                console.log(this.invoices);

                $.unblockUI();
            });
        },

        // place or save order to db
        placeOrderToDB: function (isSavedOrder) {
            if (isSavedOrder) {
                this.placeOrder.SaveOrder = true;
            } else {
                this.placeOrder.SaveOrder = false;
            }

            var placeOrderModel = this.placeOrder;

            $.confirm({
                theme: 'material',
                title: false,
                content: '<h4>Are you sure you want to place the order?</h4>',
                buttons: {
                    placeOrder: {
                        text: 'Place Order',
                        btnClass: 'btn-primary',
                        action: function () {
                            ajaxStartLoading();

                            axios.post(Router.action('Main', 'PlaceOrder'), placeOrderModel).then(response => {
                                var res = response.data;
                                console.log(res);

                                if (res.Res.Message === 'Order Placed') {
                                    $.confirm({
                                        columnClass: 'medium',
                                        title: false,
                                        content: 'Order has been placed successfully. Order ID is : ' + res.Res.Data.OrderID,
                                        buttons: {
                                            Ok: {
                                                text: 'Continue',
                                                btnClass: 'btn-blue',
                                                keys: ['enter', 'shift'],
                                                action: function () {
                                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    $.alert('Order posting failed. Message : ' + res.Res.Message);
                                }
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //$.alert('Canceled!');
                    }
                }
            });
        },

        searchFunction: function () {
            var input, filter, table, tr, td, td2, i, txtValue, txtValue2;
            input = document.getElementById("tblSearch");
            filter = input.value.toUpperCase();
            table = document.getElementById("tblHistory");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                td2 = tr[i].getElementsByTagName("td")[2];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    txtValue2 = td2.textContent || td2.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    }
});