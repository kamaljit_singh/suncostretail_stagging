﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Order details',
        search: '',
        orderDetail: [],
        showOrderStatus: false,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        isPantryOnlySearch: false,
        copyPantry: false,
        orderTotal: 0,
        currency: '',
        isRetail: false,
        pageHelpText: ''
    },
    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();
        // getCartCount and total order value
        this.getCartCount();

        // get managed features
        this.getManageFeatures();
        // Get customer invoices
        this.getOrderDetails();
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },

        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.showOrderStatus = response.data.ShowOrderStatus;
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.currency = response.data.Currency;
                    this.isRetail = response.data.IsRetail;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // function to get all the pantry list items
        getOrderDetails: function () {
            ajaxStartLoading();
            // bind Products
            var oID = GetQueryStringParams("oID");

            axios.post(Router.action('Main', 'GetOrderDetails'), { orderID: oID }).then(response => {
                console.log(response.data.Data);
                if (response.data.Message === 'Success') {
                    this.orderDetail = response.data.Data;

                    var sum = 0;
                    for (var i = 0; i < this.orderDetail.length; i++) {
                        sum += parseFloat(this.orderDetail[i].Price * this.orderDetail[i].Quantity);
                    }

                    sum = sum.toFixed(2);

                    this.orderTotal = sum;

                    //$('#orderTotal').html('$' + sum);
                } else {
                    showAlert(null, "No data found for the order");
                }

                $.unblockUI();
            });
        },

        reOrder: function () {
            var productIds = [];
            $('.product-table ul').each(function () {
                var chk = $(this).find('input[type="checkbox"]');
                var val = chk.prop('checked');
                if (val) {
                    productIds.push(chk.data('pid'));
                }
            });

            if (productIds.length === 0) {
                showAlert(null, "Please choose at least one product to reorder");
                return false;
            }
            ajaxStartLoading();
            // bind Products
            var oID = GetQueryStringParams("oID");

            axios.post(Router.action('Main', 'ReOrder'), { orderID: oID, products: productIds }).then(response => {
                console.log(response.data.Data);
                if (response.data.Message === 'Success') {
                    window.location.href = Router.action('Main', 'Cart');
                } else {
                    showAlert(null, "No data found for the order");
                }

                $.unblockUI();
            });
        }
    }
});