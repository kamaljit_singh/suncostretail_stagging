﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Order history',
        search: '',
        orders: [],
        showOrderStatus: false,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        isPantryOnlySearch: false,
        copyPantry: false,
        isRetail: false,
        pageHelpText: ''
    },
    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();
        // Get managed features
        this.getManageFeatures();

        // getCartCount and total order value
        this.getCartCount();

        // Get customer invoices
        this.getOrders();
    },

    methods: {
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },
        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.showOrderStatus = response.data.ShowOrderStatus;
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.isRetail = response.data.IsRetail;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // function to get all the pantry list items
        getOrders: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetOrderHistory')).then(response => {
                this.orders = response.data.Result.Data;

                console.log(this.orders);

                $.unblockUI();
            });
        },

        // Local search for orders
        searchFunction: function () {
            var input, filter, table, tr, td, td2, i, txtValue, txtValue2;
            input = document.getElementById("tblSearch");
            filter = input.value.toUpperCase();
            table = document.getElementById("tblHistory");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                td2 = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    txtValue2 = td2.textContent || td2.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    }
});