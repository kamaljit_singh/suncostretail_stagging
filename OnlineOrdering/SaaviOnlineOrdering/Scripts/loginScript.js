﻿$(document).ajaxStop($.unblockUI);

function startLoading() {
    $.blockUI({
        message: $('#domMessage'),
        css:
            {
                border: '0px',
                backgroundColor: 'transparent'
            }
    });
}
function success(res) {
    if (res.Message === 'Success') {
        window.localStorage.setItem('defaultInfo', res.DefaultInfo);
        window.localStorage.setItem('liquorPopup', res.LiquorPopup);
        window.localStorage.setItem('noticeBoard', res.IsNoticeBoard);
        window.localStorage.setItem('isParent', res.IsParent);
        window.localStorage.setItem('customerID', res.CustomerID);

        if (res.IsNoticeBoard) {
            $('#frmPDF').attr('src', res.PDF.PdfFile); // + '#zoom=45'
        }
        $('#divTerms P').html(res.TermsAndConditions);

        if (res.ShowImportantNotice) {
            var defaultInfo = JSON.parse(res.DefaultInfo);

            $('#divIntroMessage P').html(defaultInfo.intro.Description);
            $('#txtCutOffTime').val(defaultInfo.intro.OrderCutOff);
        }

        if (res.ShowTermsContitions) {
            $('#modalTerms').modal('show');
        } else {
            if (res.LiquorPopup) {
                $('#modalLiqour').modal('show');
            } else {
                if (res.ShowImportantNotice) {
                    $('#modalImportantNotice').modal('show');
                } else {
                    window.location.href = Router.action('Main', 'SearchProducts');
                }
            }
        }
        // var url = Router.action('Main', 'DefaultPantry');
    } else {
        if (res.Message.indexOf('The Email') != -1) {
            showAlert(null, 'The email address you entered is incorrect');
        } else {
            showAlert(null, res.Message);
        }
    }
}

function TermsAgree() {
    var liquor = window.localStorage.getItem('liquorPopup');
    if (liquor == 'true') {
        $('#modalLiqour').modal('show');
    } else {
        $('#modalImportantNotice').modal('show');
    }
}

function LiquorAgree() {
    $('#modalImportantNotice').modal('show');
}

function ImportantNotice() {
    var noticeBoard = window.localStorage.getItem('noticeBoard');
    var isParent = window.localStorage.getItem('isParent');

    if (noticeBoard === 'true') {
        $('#modalPDF').modal('show');
    } else {
        if (isParent === 'true') {
            window.location.href = Router.action('Main', 'ChooseCustomer') + '/' + cID;
        } else {
            window.location.href = Router.action('Main', 'SearchProducts');
        }
    }
}

function resetPassword() {
    $.confirm({
        animation: 'zoom',
        title: 'Forgot Password',
        titleClass: 'text-center',
        columnClass: 'col-md-6 col-md-offset-3',
        type: 'black',
        content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Enter your email:</label>' +
            '<input type="text" placeholder="Your email" class="email input-forgot-pwd" required />' +
            '</div>' +
            '</form>',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'login100-form-btn btn-border-black',
                action: function () {
                    var email = this.$content.find('.email').val();
                    if (!email) {
                        showAlert(null, 'Please provide a valid email');
                        return false;
                    }
                    startLoading();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: Router.action('Account', 'ResetPassword'),
                        data: "{'email':'" + email + "'}",
                        success: function (response) {
                            if (response.Result.indexOf('Email does') > 0) {
                                showAlert(null, 'Email does not exist');
                            } else {
                                showAlert(null, response.Result.replace('.', ''));
                            }

                            $.unblockUI;
                        }
                    });
                }
            },
            Cancel: {
                btnClass: 'login100-form-btn btn-border-black',
                action: function () { }
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

// Show alerts
function showAlert(title, message) {
    $.alert({
        title: title,
        titleClass: 'text-center',
        content: '<div class="text-center fs-19">' + message + '</div>',
        animation: 'scaleX',
        closeAnimation: 'scaleX',
        draggable: false,
        animateFromElement: false,
        type: 'red',
        buttons: {
            okay: {
                text: 'OK',
                btnClass: 'btn-blue',
                keys: ['enter'],
            }
        }
    });
}

function pdfContinue() {
    //window.location.href = Router.action('Main', 'DefaultPantry');
    var cID = window.localStorage.getItem('customerID');
    var isParent = window.localStorage.getItem('isParent');

    if (isParent === 'true') {
        window.location.href = Router.action('Main', 'ChooseCustomer') + '/' + cID;
    } else {
        window.location.href = Router.action('Main', 'SearchProducts');
    }
}