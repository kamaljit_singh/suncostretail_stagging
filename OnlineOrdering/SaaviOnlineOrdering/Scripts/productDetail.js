﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Product Details',
        search: '',
        schema: '',
        showBrand: false,
        showSupplier: false,
        showHistory: false,
        showUnitPerCtn: false,
        highlightRewardItem: false,
        showFavoriteIcon: false,
        showPrice: false,
        itemEnquiry: false,
        highlightStock: false,
        showStock: false,
        searchProducts: false,
        showInvoices: false,
        isPantryOnlySearch: false,
        showOrderHistory: false,
        isDecimal: false,
        currency: '',
        marketText: false,
        copyPantry: false,
        lastUOM: false,
        backOrder: false,
        product: [],
        isRetail: false,
        isGuest: false,
        tempCartInsertionModel: {
            "RunNo": "",
            "CommentLine": "",
            "PackagingSequence": "",
            "CartItem": {
                "ProductID": 0,
                "Quantity": 0,
                "Price": 0,
                "ProductWeight": 0,
                "WeightName": "",
                "WeightDescription": "",
                "IsPieceOrWeight": "",
                "IsSpecialPrice": false,
                "CommentID": 0,
                "UnitId": 0,
                "IsGstApplicable": false,
                "ItemPricePerUnit": 0,
                "OrderedQuantity": 0,
                "RecievedQuantity": 0,
                "UnitsPerCarton": 0,
                "BasePrice": 0,
                "IsDiscountApplicable": false,
                "IsBoxDiscount": false,
                "PriceType": "",
                "IsNoPantry": false
            },
            "IsSavedOrder": true,
            "Warehouse": "",
            "AddressID": 0
        },
        addPantryItemModel: {
            "PantryItemID": 0,
            "PantryListID": 0,
            "ProductID": 0,
            "Quantity": 0.0,
            "IsActive": true,
            "IsCore": false,
            "CustomerID": 0
        },
        favouriteLists: [],
        pageHelpText: '',
        imageThumbs: [],
        hasDescription: false
    },

    // function to call on page load
    mounted() {
        $('#loader').remove();
        $('#appMain').show();

        //getCartCount and total order value
        this.getCartCount();

        // Get Managed features
        this.getManageFeatures();

        $('body').addClass('product-details');

        // Get product details
        this.getProductDetail();

        // bind the product filters
        this.getFavLists();

        // Dynamic height
        var divHeight = $('#divPdetail').height();
        var windowHeight = $(window).height() - 250;

        $('#divPdetail').css('height', (windowHeight));
        $(window).resize(function () {
            var windowHeight = $(window).height() - 250;
            $('#divPdetail').css('height', (windowHeight));
        });
    },

    methods: {
        // function to get all the pantry list items
        getFavLists: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerPantryLists')).then(response => {
                this.favouriteLists = response.data.Result.Data;
                $.unblockUI();
            });
        },

        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.Total;
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                    };
                });
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.showBrand = response.data.ShowBrand;
                    this.showSupplier = response.data.ShowSupplier;
                    this.showPrice = response.data.ShowPrice;
                    this.showHistory = response.data.ShowHistory;
                    this.highlightRewardItem = response.data.HighlightRewardItems;
                    this.showFavoriteIcon = response.data.ShowFavoriteIcon;
                    this.showUnitPerCtn = response.data.ShowUnitsPerCarton;
                    this.itemEnquiry = response.data.ItemEnquiry;
                    this.highlightStock = response.data.HighlightStock;
                    this.showStock = response.data.ShowStock;
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.isDecmial = response.data.IsDecimal;
                    this.currency = response.data.Currency;
                    this.marketText = response.data.MarketText;
                    this.lastUOM = response.data.ReturnLastUOM;
                    this.backOrder = response.data.BackOrder;
                    this.isRetail = response.data.IsRetail;
                    this.isGuest = response.data.IsGuest;

                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // function to get all the pantry list items
        getProductDetail: function () {
            ajaxStartLoading();
            // bind Products
            var pID = GetQueryStringParams("pID");

            axios.post(Router.action('Main', 'GetProductDetail'), { productID: pID }).then(response => {
                //console.log(response.data.Data.Product);
                this.product = response.data.Data;

                // this.productImages = this.product.ProductImages;

                $.each(this.product.ProductImages, function (key, val) {
                    var len = val.ImageName.split('/').length;
                    var thumb = val.ImageName.split('/')[len - 1];
                    var name = val.ImageName.replace(thumb, 'thumb_' + thumb);
                    app.imageThumbs.push(name);
                });

                this.hasDescription = this.product.Description.length > 0 ? true : false;
                $.unblockUI();
            });
        },

        // function to set price based on UOM DDL change
        setPrice: function (e, dUOM) {
            var select = $(e.target);
            var selectedIndex = select.prop('selectedIndex');

            var lastUom = select.data('lastuom');
            var selectedUOM = dUOM[selectedIndex].UOMID;

            if (lastUom === selectedUOM) {
                if (!select.hasClass('lastUom')) {
                    select.addClass('lastUom');
                }
            } else {
                select.removeClass('lastUom');
            }

            var price = dUOM[selectedIndex].Price;
            price = Math.round(price * Math.pow(10, 2)) / Math.pow(10, 2);
            $('#priceSpan').html('$' + price.toFixed(2));

            if (this.marketText && price === 0) {
                $('#priceSpan').html('Market ' + this.currency);
            }

            //select.next('span').html('$' + price);

            $('#uom').val(select.prop('selectedIndex'));

            if (this.showUnitPerCtn) {
                if (dUOM[selectedIndex].UOMDesc.toLowerCase() === 'ctn' || dUOM[selectedIndex].UOMDesc.toLowerCase() === 'carton') {
                    $('#upc').html('[U/CTN=' + dUOM[selectedIndex].QuantityPerUnit + ']');
                    $('#upc').removeClass('hidden');
                } else {
                    $('#upc').addClass('hidden');
                }
            }
        },

        // Show default image if Productimage not found
        showDefault: function (e) {
            $(e.target).attr('src', this.schema + '/content/images/no-product.png');
        },

        // Item enquiry popup
        inquiryPopup: function (pID) {
            // prompt
            $.confirm({
                title: 'Item Enquiry',
                type: 'blue',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Enter enquiry details:</label>' +
                    '<textarea class="comment form-control" style="resize:none;" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',
                        action: function () {
                            var comment = this.$content.find('.comment').val();
                            if (!comment) {
                                showAlert(null, 'Provide detail about the enquiry')
                                return false;
                            }
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'SendProductEnquiry'), {
                                productID: pID,
                                comment: comment
                            }).then(response => {
                                showAlert(null, 'Your enquiry has been sent');
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        },

        checkNumeric: function (e) {
            var evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var el = $(e.target);
            if (charCode == 13) {
                return true;
            }
            if (this.isDecimal) {
                if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                    evt.preventDefault();;
                } else {
                    var len = el.val().length;
                    var index = el.val().indexOf('.');
                    if (index >= 0 && charCode == 46) {
                        evt.preventDefault();;
                    }
                    if (index >= 0) {
                        var charAfterdot = (len + 1) - index;
                        if (charAfterdot > 3) {
                            evt.preventDefault();;
                        }
                    }
                }
                return true;
            } else {
                if (charCode < 48 || charCode > 57) {
                    evt.preventDefault();;
                } else {
                    return true;
                }
            }
        },

        // Set the quantity to a hidden field
        setQuantity: function (e, product) {
            var evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            var quantity = $(e.target).val();
            $('#quantity').val(quantity === '' ? '0' : quantity);

            if (e.keyCode === 13) {
                this.addItemToTempCart(product);
            }
            return true;
        },

        // Add item to temp cart
        addItemToTempCart: function (prod) {
            if (this.isGuest) {
                showAlertGuest(null, 'In order to use this feature, please register with Suncoast Fresh.');
                return false;
            }
            if (prod.StockQuantity === 0 && this.backOrder) {
                showAlert('Oops!', 'Unfortunately, there is not sufficient stock available to complete your order.');
                return false;
            }
            // show the loading
            ajaxStartLoading();
            var qty = parseFloat($('#quantity').val());
            var existing = $('#addToCart').hasClass('green-border-btn');

            if (qty === 0) {
                showAlertRed('', 'Please enter order quantity');
                // End loading
                $.unblockUI();
                return false;
            }

            if (prod.BuyIn && !existing) {
                $.confirm({
                    title: '',
                    content: 'This item is an ORDER IN ONLY  item. WILL DELIVER ASAP.',
                    type: 'blue',
                    buttons: {
                        add: {
                            text: 'I Acknowledge',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                app.addToDB(prod);
                            }
                        },
                        cancel: {
                            text: 'Cancel Item',
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                //app.addFavList();
                                $.unblockUI();
                            }
                        },
                    }
                });
            } else {
                if (qty > prod.StockQuantity && this.backOrder) {
                    $.confirm({
                        title: 'Oops!',
                        content: 'Sorry, Only ' + prod.StockQuantity + ' items are currently available. By pressing OK we will submit only the available stock quantity.',
                        type: 'blue',
                        buttons: {
                            add: {
                                text: 'OK',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    app.addToDB(prod);
                                }
                            },
                            cancel: {
                                text: 'CANCEL ITEM',
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    //app.addFavList();
                                    $.unblockUI();
                                }
                            },
                        }
                    });
                } else {
                    app.addToDB(prod);
                }
            }
        },

        addToDB: function (prod) {
            var btn = $('#addToCart');
            var qty = parseFloat($('#quantity').val());
            var price = parseInt($('#uom').val());
            var unitName = '';

            if (qty > prod.StockQuantity && this.backOrder) {
                qty = prod.StockQuantity;
                $('#qtyInput').val(qty);
            }

            this.tempCartInsertionModel.CartItem.ProductID = prod.ProductID;
            this.tempCartInsertionModel.CartItem.Quantity = qty;
            this.tempCartInsertionModel.CartItem.IsGstApplicable = prod.IsGST;

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.Price = prod.DynamicUOM[price].Price;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.Price = prod.Prices.Price;
                } else {
                    this.tempCartInsertionModel.CartItem.Price = prod.Price;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.IsSpecialPrice = prod.DynamicUOM[price].IsSpecial;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.IsSpecialPrice = prod.Prices.IsSpecial;
                } else {
                    this.tempCartInsertionModel.CartItem.IsSpecialPrice = false;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.UnitId = prod.DynamicUOM[price].UOMID;
            } else {
                this.tempCartInsertionModel.CartItem.UnitId = prod.UOMID;
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.DynamicUOM[price].QuantityPerUnit;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.Prices.QuantityPerUnit;
                } else {
                    this.tempCartInsertionModel.CartItem.UnitsPerCarton = prod.UnitsPerCarton;
                }
            }

            if (prod.DynamicUOM != null) {
                this.tempCartInsertionModel.CartItem.BasePrice = prod.DynamicUOM[price].CostPrice;
            } else {
                if (prod.Prices != null) {
                    this.tempCartInsertionModel.CartItem.BasePrice = prod.Prices.CostPrice;
                } else {
                    this.tempCartInsertionModel.CartItem.BasePrice = prod.CompanyPrice;
                }
            }

            if (prod.DynamicUOM != null) {
                unitName = prod.DynamicUOM[price].UOMDesc;
            } else {
                if (prod.Prices != null) {
                    unitName = prod.Prices.UOMDesc;
                } else {
                    unitName = prod.UOMDesc;
                }
            }

            this.tempCartInsertionModel.CartItem.OrderedQuantity = qty;
            this.tempCartInsertionModel.CartItem.RecievedQuantity = 0;
            this.tempCartInsertionModel.CartItem.IsDiscountApplicable = false;
            this.tempCartInsertionModel.CartItem.IsPieceOrWeight = false;
            this.tempCartInsertionModel.CartItem.WeightName = '';
            this.tempCartInsertionModel.CartItem.IsNoPantry = false;

            // Send the data back to controller for processing
            axios.post(Router.action('Main', 'InsertUpdateTempCart'), this.tempCartInsertionModel).then(response => {
                if (response.data.Result) {
                    if (response.data.CartCount > 0) {
                        $('#itemCount').html(response.data.CartCount);
                        $('#itemCount').show();

                        if (!btn.hasClass('green-border-btn')) {
                            btn.removeAttr('class');
                            btn.addClass('green-border-btn');
                        }
                    }
                    showAlert(prod.ProductName, qty + ' ' + unitName + ' added to cart');
                } else {
                    showAlert(prod.ProductName, response.data.Message);
                }

                // End loading
                $.unblockUI();
            });
        },

        setProductID: function (pID, remove) {
            if (remove) {
                this.addPantryItemModel.ProductID = 0;
                $('#divFavLists input[type="checkbox"]').prop('checked', false);
            } {
                if (this.favouriteLists === null || this.favouriteLists.length === 0) {
                    $.confirm({
                        title: '',
                        content: 'No favourite lists exist. Do you want to create a new one?',
                        type: 'blue',
                        buttons: {
                            add: {
                                text: 'Yes',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    //$('#modalFavList').modal('hide');
                                    app.addFavList();
                                }
                            },
                            no: function () {
                                app.addPantryItemModel.ProductID = 0;
                            },
                        }
                    });
                    return;
                } else {
                    this.addPantryItemModel.ProductID = pID;
                    $('#modalFavList').modal('show');
                }
            }
        },

        addItemToFav: function () {
            if ($('#divFavLists input[type="checkbox"]').filter(':checked').length === 0) {
                showAlert(null, 'Please choose a favourite list');
            } else {
                ajaxStartLoading();
                // bind Products
                axios.post(Router.action('Main', 'AddItemToPantry'), this.addPantryItemModel).then(response => {
                    if (response.data.Message == 'Success') {
                        showAlert(null, "Product added to your favourite list")
                    } else {
                        showAlert(null, response.data.Message);
                    }
                    $('#modalFavList').modal('hide');
                    $('#divFavLists input[type="checkbox"]').prop('checked', false);
                    $.unblockUI();
                });
            }
        },

        setFavList: function (e, favID) {
            var state = $(e.target).prop('checked');
            this.addPantryItemModel.PantryListID = favID;
            $('#divFavLists input[type="checkbox"]').prop('checked', false);
            $(e.target).prop('checked', state);
        },

        // add fav list if none exist while adding product
        addFavList: function () {
            // prompt
            $.confirm({
                title: 'Add Favourite List',
                type: 'blue',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Favourite list name:</label>' +
                    '<input type="text" class="fav form-control" style="resize:none;" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',
                        action: function () {
                            var fav = this.$content.find('.fav').val();
                            if (!fav) {
                                showAlert(null, 'Please enter favourite list name');
                                return false;
                            }
                            app.pantryModel.PantryListName = fav;
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'AddPantry'), app.pantryModel)
                                .then(response => {
                                    console.log(response);
                                    if (response.data.Message == 'Success') {
                                        app.getFavLists();
                                        $('#modalFavList').modal('show');
                                    } else {
                                        showAlert(null, response.data.Message);
                                    }
                                    $.unblockUI();
                                });
                        }
                    },
                    cancel: function () {
                        //close
                    },
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        },
    }
});