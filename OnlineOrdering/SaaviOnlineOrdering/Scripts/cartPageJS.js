﻿var app = new Vue({
    el: '#appMain',
    vuetify: new Vuetify(),
    data: {
        title: 'Cart',
        search: '',
        cartItems: [],
        selectedUomPrice: [],
        isPlacedByRep: false,
        isSavedOrder: false,
        isPOEnabled: false,
        isMultipleAddresses: false,
        freightCharges: 0,
        nonDeliveryDayCharges: 0,
        deliveryDate: '',
        cartTotal: 0,
        comments: [],
        addresses: [],
        orderDeliveryDate: '',
        newComment: '',
        commentID: 0,
        tempCartID: 0,
        searchProducts: false,
        showInvoices: false,
        showOrderHistory: false,
        di: null,
        isMobile: false,
        saveOrder: false,
        isPantryOnlySearch: false,
        cutOff: '',
        isDecimal: false,
        currency: '',
        showBIPopup: false,
        biItems: [],
        marketText: false,
        copyPantry: false,
        lastUOM: false,
        backOrder: false,
        moq: false,
        isRetail: false,
        IsStripePaymentAdmin: false,
        IsStripePaymentCustomer: false,
        cartTotalGST: 0,
        IsCapture: false,
        ChargedAmount: '',
        placeOrder: {
            "CustomerID": 0, // server side
            "TempCartID": 0, // on page load
            "UserID": 0, // server side
            "CartID": 0, // server side
            "CommentID": 0,
            "AddressID": 0, // if multiple addresses enabled
            "PONumber": "",
            "OrderStatus": 1,
            "Comment": "",
            "OrderDate": "", // moment
            "CutOffTime": "", // server side
            "ExtDoc": "",
            "PackagingSequence": "", // server side
            "IsAutoOrdered": false,
            "DeviceToken": "webview",
            "DeviceType": "web",
            "DeviceVersion": "Saavi 5",
            "DeviceModel": "",
            "AppVersion": "5",
            "SaveOrder": false,
            "IsInvoiceComment": false,
            "IsUnloadComment": false,
            "InvoiceCommentID": 0,
            "UnloadCommentID": 0,
            "IsOrderPlpacedByRep": false, // server side
            "RunNo": "",
            "HasFreightCharges": false,
            "HasNonDeliveryDayCharges": false,
            "IsDelivery": false,
            "IsLeave": false,
            "IsContactless": false,
            "PickupID": 0,
            "DeliveryType": '',
            "Coupon": '',
            "validateDate": '',
            "IsCouponApplied": false,
            "CouponAmount": 0
        },
        commentModel: {
            "CustomerID": 0,
            "ProductID": 0,
            "IsProduct": true
        },
        datePicker: false,
        prodComments: [],
        commentProdID: 0,
        prodCommentID: 0,
        selectedCartItem: null,
        isChanged: false,
        pageHelpText: '',
        settings: [],
        tempCartID: 0,
        isDelivery: true,
        isPickup: false,
        isLeave: true,
        isContactless: true,
        minCartValue: 0,
        coupon: '',
        couponAmount: 0,
        isCouponApplied: false,
        promoModal: false,
        addressModal: false,
        pickupModal: false,
        subTotal: 0,
        appliedPromo: '',
        promoResponse: '',
        promoApplySuccess: false,
        customerAddresses: [],
        pickupLocations: [],
        selectedAddress: 0,
        selectedPickup: '',
        deliveryCharges: 0,
        deliveryMessage: '',
        minFreightValue: 0,
        addAddressModal: false,
        addressModel: {
            "contactName": null,
            "phone": null,
            "streetAddress": null,
            "suburb": null,
            "postCode": null,
            "customerID": 0
        },
        valid: false,
        reqRule: [
            v => !!v || 'This field is required'
        ],
        phoneRule: [
            v => !!v || 'This field is required',
            v => (v && v.length === 10) || 'Phone must contain 10 characters',
        ],
        contactRule: [
            v => !!v || 'This field is required',
            v => (v && v.length >= 6) || 'Contact name must contain 6 or more characters',
        ],
        componentForm: {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        },
        placeSearch: '',
        autocomplete: null,
        hasDonationItem: false,
        amountWOdonation: 0,
        isOnlyDonation: false,
    },
    // function to call on page load
    mounted() {
        //setTimeout(function () {
        //    app.initMap();
        //}, 1000);
        $('#loader').remove();
        $('#appMain').show();

        this.loadStripeForm();
        //
        this.getManageFeatures();

        // getCartCount and total order value
        this.getCartCount();

        // bind Products
        this.getTempCartItems(this.isPlacedByRep, this.isSavedOrder);

        // Get order comments
        this.getComments(0, false);

        // init datepicker
        //this.di = JSON.parse(window.localStorage.getItem('defaultInfo'));
        //this.initDatepicker();

        // Set the flag for mobile device
        this.detectmob();

        // Dynamic height

        var divHeight = $('#divCartItems').height();
        var windowHeight = $(window).height() - 270;

        $('#divCartItems').css('height', (windowHeight));

        $(window).resize(function () {
            // console.log('resize');
            var windowHeight = $(window).height() - 275;
            $('#divCartItems').css('height', (windowHeight));
        });

        axios.post(Router.action('Main', 'GetPageHelpText'), { pageName: 'CartPage' })
            .then(response => {
                var res = response.data.Description;
                this.pageHelpText = res;
                console.log(res);
            });

        this.getDeliveryAddresses();
        this.getPickupLocations();

        //this.initAutocomplete();
    },
    methods: {
        detectmob: function () {
            if (navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i)
                || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)
            ) {
                this.isMobile = true;
            }
            else {
                this.isMobile = false;
            }
        },

        //
        searchByEnter: function (e) {
            if (e === null || e.keyCode === 13) {
                if (e !== null) {
                    e.stopPropagation();
                }
                if (this.search.length > 0 && this.search != '#') {
                    if (this.isPantryOnlySearch) {
                        window.location.href = Router.action("Main", "DefaultPantry") + '?key=' + app.search;
                    } else {
                        window.location.href = Router.action('Main', 'SearchProducts') + '?key=' + this.search;
                    }
                }
            }
        },

        // Function to get the managed and admin features for the customer.
        getManageFeatures: function () {
            axios.get(Router.action('Main', 'GetManagedFeatures'))
                .then(response => {
                    this.searchProducts = response.data.SearchProducts;
                    this.showInvoices = response.data.ShowInvoices;
                    this.showOrderHistory = response.data.ShowOrderHistory;
                    this.saveOrder = response.data.SaveOrder;
                    this.datePicker = response.data.DatePicker;
                    this.isDecimal = response.data.IsDecimal;
                    this.currency = response.data.Currency;
                    this.showBIPopup = response.data.ShowBIPopUP;
                    this.marketText = response.data.MarketText;
                    this.lastUOM = response.data.ReturnLastUOM;
                    this.backOrder = response.data.BackOrder;
                    this.isRetail = response.data.IsRetail;
                    if (this.isRetail) {
                        this.searchProducts = false;
                        this.showOrderHistory = false;
                        this.isPantryOnlySearch = true;
                    }
                });
        },

        // Get cart item count and total
        getCartCount: function () {
            axios.get(Router.action('Main', 'GetCartCount'))
                .then(response => {
                    var res = response.data

                    if (res.CartCount > 0) {
                        $('#itemCount').html(res.CartCount);
                        $('#itemCount').show();
                        this.cartTotal = res.TotalWithGST;
                        this.ChargedAmount = res.TotalWithGST;

                        var cAmount = res.CouponAmount;
                        if (cAmount !== undefined && cAmount > 0) {
                            this.couponAmount = res.CouponAmount;
                        }

                        var donationAmount = 0;
                        var cntDonation = 0;
                        $.each(this.cartItems, function (key, val) {
                            if (val.IsDonationBox) {
                                app.hasDonationItem = true;
                                donationAmount += (val.Price * val.Quantity)
                                cntDonation++;
                            }
                        });

                        if (this.isCouponApplied) {
                            this.subTotal = this.cartTotal - this.couponAmount;
                        } else {
                            this.subTotal = this.cartTotal;
                        }

                        if (this.placeOrder.HasFreightCharges) {
                            this.subTotal = this.cartTotal + this.deliveryCharges;
                        }
                        this.ChargedAmount = this.subTotal;

                        this.amountWOdonation = this.ChargedAmount - donationAmount;

                        if (cntDonation === this.cartItems.length && this.cartItems.length > 0) {
                            this.isOnlyDonation = true;

                            if (this.placeOrder.HasFreightCharges) {
                                this.placeOrder.HasFreightCharges = false;
                                this.subTotal = this.subTotal - this.deliveryCharges;
                                this.ChargedAmount = this.subTotal;
                            }
                        }
                    } else {
                        $('#itemCount').html('');
                        $('#itemCount').hide();
                        this.cartTotal = 0;
                        this.subTotal = 0;
                    };
                });
        },

        // Get existing order comments
        getComments: function (pID, isProduct) {
            this.commentModel.ProductID = pID;
            this.commentModel.IsProduct = isProduct;

            axios.post(Router.action('Main', 'GetOrderComments'), { model: this.commentModel })
                .then(response => {
                    if (pID > 0) {
                        this.prodComments = response.data.Comments;
                    } else {
                        this.comments = response.data.Comments;
                    }
                });
        },

        // initialize the date picker
        initDatepicker: function () {
            //this.di = JSON.parse(window.localStorage.getItem('defaultInfo'));
            //this.cutOff = this.di.intro.OrderCutOff;
            //$('#date1').text(moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL'));
            //$('#date2').text(moment(JSON.stringify(this.di.orderDatesLong[1]), "MM/DD/YYYY").format('LL'));

            //$('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');

            //$('#chkDate1').attr('data-date', this.di.orderDatesLong[0]);
            //$('#chkDate2').attr('data-date', this.di.orderDatesLong[1]);

            // this.placeOrder.OrderDate = this.di.orderDatesLong[0];
            var availableDays = [];
            var pDays = this.di.permittedDays;

            if (pDays === undefined) {
                pDays = this.di.PermittedDays;
            }

            $.each(pDays, function (idx, val) {
                if (val === 'Monday') {
                    availableDays.push(1);
                } else if (val === 'Tuesday') {
                    availableDays.push(2);
                } else if (val === 'Wednesday') {
                    availableDays.push(3);
                } else if (val === 'Thursday') {
                    availableDays.push(4);
                } else if (val === 'Friday') {
                    availableDays.push(5);
                } else if (val === 'Saturday') {
                    availableDays.push(6);
                } else if (val === 'Sunday') {
                    availableDays.push(0);
                }
            });
            //debugger;
            var minDate = new Date(moment(this.di.OrderDatesLong[0]));
            var maxDate = new Date();
            // minDate.setDate(minDate.getDate() + 2);
            maxDate.setDate(maxDate.getDate() + 30);
            var todayDay = new Date().getDate();
            var month = new Date().getMonth();
            $('.datepicker-here').datepicker({
                minDate: minDate,
                maxDate: maxDate,
                showOtherMonths: false,
                onSelect: function (formattedDate, date, inst) {
                    $('#chkDate1').prop('checked', false);
                    $('#chkDate2').prop('checked', false);
                    app.deliveryDate = formattedDate;
                    //$('#dateToggle').html('SELECTED DATE: <b>' + moment(formattedDate, "MM/DD/YYYY").format('LL') + '</b>');
                },
                onRenderCell: function (date, cellType) {
                    if (cellType == 'day') {
                        var day = date.getDay(),
                            isDisabled = availableDays.indexOf(day) == -1;
                        var clss = '';
                        if (availableDays.indexOf(day) > -1 && !isDisabled) {
                            clss = 'bgGreen';
                        }
                        return {
                            disabled: isDisabled,
                            classes: clss //isDisabled == true ? 'bgDisabled' :
                        }
                    }
                }
            });

            var dPicker = $('.datepicker-here').datepicker().data('datepicker');
            dPicker.clear();
        },

        // function to get all the pantry list items
        getTempCartItems: function (isPlacedByRep, isSavedOrder) {
            ajaxStartLoading();
            // bind Products
            const headersConst = {
                "cache-control": "no-cache"
            };
            axios.post(Router.action('Main', 'GetTempCartItems'), {
                isPlacedByRep: isPlacedByRep,
                isSavedOrder: isSavedOrder
            }, { headers: headersConst }).then(response => {
                this.cartItems = response.data.CartItems;
                this.cartTotal = response.data.CartTotal;
                this.cartTotalGST = response.data.CartTotalGST;
                this.ChargedAmount = response.data.CartTotalGST;
                this.IsCapture = response.data.IsCapture;
                app.IsStripePaymentAdmin = response.data.IstripePaymentAdmin;
                app.IsStripePaymentCustomer = response.data.IsStripePaymentCustomer;

                this.isSavedOrder = response.data.IsSaveOrder;
                this.freightCharges = response.data.IsFreight;
                this.nonDeliveryDayCharges = response.data.IsNonDayDelivery;
                this.isPOEnabled = response.data.IsPONumber;
                this.isMultipleAddresses = response.data.IsMultipleAddresses;
                this.minCartValue = response.data.MinCartValue;
                this.coupon = response.data.Coupon;
                this.couponAmount = response.data.CouponAmount;
                this.isCouponApplied = response.data.IsCouponApplied;
                this.deliveryCharges = response.data.DeliveryFee;
                this.minFreightValue = response.data.MinFreightOrderValue;
                this.deliveryMessage = response.data.DelMessage;
                //debugger;
                if (this.isCouponApplied) {
                    this.subTotal = this.cartTotal - this.couponAmount;

                    app.placeOrder.IsCouponApplied = true;
                    app.placeOrder.Coupon = this.coupon;
                    app.placeOrder.CouponAmount = this.couponAmount;
                }
                if (this.cartItems !== undefined && this.cartItems.length > 0) {
                    this.placeOrder.TempCartID = response.data.CartItems[0].CartID;
                    this.tempCartID = response.data.CartItems[0].CartID;

                    this.getSuggestiveItems(this.tempCartID);
                } else {
                    $('#divCartEmpty').html('No items added in the cart!');
                }
                this.ChargedAmount = this.subTotal;
                var donationAmount = 0;
                var cntDonation = 0;
                $.each(this.cartItems, function (key, val) {
                    if (val.BuyIn) {
                        app.biItems.push(val);
                    }
                    if (val.IsDonationBox) {
                        app.hasDonationItem = true;
                        donationAmount += (val.Price * val.Quantity)
                        cntDonation++;
                    }
                });

                if (cntDonation === this.cartItems.length) {
                    this.isOnlyDonation = true;
                }

                this.amountWOdonation = this.ChargedAmount - donationAmount;
                $.unblockUI();
            });
        },

        // function to set price based on UOM DDL change
        setPrice: function (e, idx, dUOM, cartItem) {
            var select = $(e.target);
            var selectedIndex = select.prop('selectedIndex');

            var lastUom = select.data('lastuom');
            var selectedUOM = dUOM[selectedIndex].UOMID;

            if (lastUom === selectedUOM) {
                if (!select.hasClass('lastUom')) {
                    select.addClass('lastUom');
                }
            } else {
                select.removeClass('lastUom');
            }

            var price = dUOM[selectedIndex].Price;
            price = Math.round(price * Math.pow(10, 2)) / Math.pow(10, 2);
            select.parent().prev('li').html('$' + price.toFixed(2));

            if (this.marketText && price === 0) {
                select.parent().prev('li').html('Market ' + this.currency);
            }

            $('#' + idx + '_uom').val(select.prop('selectedIndex'));

            cartItem.OrderUnitId = dUOM[selectedIndex].UOMID;
            cartItem.OrderUnitName = dUOM[selectedIndex].UOMDesc;
            cartItem.Price = price;
            cartItem.UnitsPerCarton = dUOM[selectedIndex].QuantityPerUnit;
            cartItem.CompanyPrice = dUOM[selectedIndex].CostPrice;
            if (price > 0) {
                this.updateCartItem(cartItem);
            }
        },

        // Set the quantity to a hidden field
        setQuantity: function (e, idx) {
            var quantity = $(e.target).val();
            $('#' + idx + '_quantity').val(quantity === '' ? '0' : quantity);
        },

        // Function to delete a cart item
        deleteCartItem: function (item) {
            $.confirm({
                //theme: 'material',
                title: false,
                content: '<span class="fs-19">Delete "<b>' + item.ProductName + '</b>" from cart?</span>',
                type: 'red',
                buttons: {
                    Delete: {
                        text: 'Delete',
                        btnClass: 'btn-danger',
                        action: function () {
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'DeleteCartItem'), {
                                isDeleteAll: false,
                                cartItemID: item.CartItemID,
                                isSavedOrder: app.isSavedOrder
                            }).then(response => {
                                var allCartItems = app.cartItems;

                                let res = response.data;
                                if (res.Message === 'Item deleted') {
                                    //$('#cartitem_' + item.ProductCode).css({ 'background': 'red', 'color': 'white' }).fadeTo("slow", 0.2, function () {
                                    //    $(this).remove();
                                    //});
                                    app.getCartCount();

                                    // reset the promocode related settings
                                    app.coupon = '';
                                    app.couponAmount = 0;
                                    app.isCouponApplied = false;
                                    app.promoApplySuccess = false;

                                    app.placeOrder.IsCouponApplied = false;
                                    app.placeOrder.Coupon = '';
                                    app.placeOrder.CouponAmount = 0;

                                    //let i = app.biItems.map(it => it.CartItemID).indexOf(item.CartItemID); // find index of your object
                                    //app.biItems.splice(i, 0); // remove it from array

                                    var ci = allCartItems.map(item => item.CartItemID).indexOf(item.CartItemID); // find index of your object
                                    allCartItems.splice(ci, 1);

                                    app.cartItems = allCartItems;
                                }
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //$.alert('Canceled!');
                    }
                }
            });
        },

        // Allow only numeric values in Quantity
        checkNumeric: function (e) {
            var evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var el = $(e.target);

            if (this.isDecimal) {
                if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                    evt.preventDefault();;
                } else {
                    var len = el.val().length;
                    var index = el.val().indexOf('.');
                    if (index >= 0 && charCode == 46) {
                        evt.preventDefault();;
                    }
                    if (index >= 0) {
                        var charAfterdot = (len + 1) - index;
                        if (charAfterdot > 3) {
                            evt.preventDefault();;
                        }
                    }
                }
                return true;
            } else {
                if (charCode < 48 || charCode > 57) {
                    evt.preventDefault();;
                } else {
                    return true;
                }
            }
        },

        // Update the order quantity
        updateQuantity: function (e, idx, item, dUOM) {
            var qty = $(e.target).val();
            if (qty > item.StockQuantity && this.backOrder) {
                $.confirm({
                    title: 'Oops!',
                    content: 'Sorry, Only ' + item.StockQuantity + ' items are currently available. By pressing OK we will submit only the available stock quantity.',
                    type: 'dark',
                    buttons: {
                        add: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                item.Quantity = item.StockQuantity;
                                app.updateCartItem(item);
                            }
                        },
                        cancel: {
                            text: 'CANCEL ITEM',
                            action: function () {
                                //$('#modalFavList').modal('hide');
                                //app.addFavList();
                                $.unblockUI();
                            }
                        },
                    }
                });
            } else {
                item.Quantity = $(e.target).val();
                if (item.Quantity.trim() === '' || item.Quantity.trim() === '0') {
                    showAlert(null, "Quantity can not be blank or 0");
                    $(e.target).val('');
                } else {
                    this.updateCartItem(item);
                }
            }
        },

        // Update the cart item in the database after value change.
        updateCartItem: function (item) {
            this.cartItem = item;

            console.log(item);

            // add code to update cart item to database.
            ajaxStartLoading();
            axios.post(Router.action('Main', 'UpdateCartItem'), { model: item })
                .then(response => {
                    this.getCartCount();
                    $.unblockUI();
                });
        },

        // delete order comment
        deleteComment: function (commentID) {
            $.confirm({
                //theme: 'material',
                title: false,
                content: '<span class="fs-19">Are you sure you want to delete this comment?</span>',
                type: 'red',
                buttons: {
                    Delete: {
                        text: 'Delete',
                        btnClass: 'btn-danger',
                        action: function () {
                            ajaxStartLoading();
                            axios.post(Router.action('Main', 'DeleteComment'), {
                                CommentID: commentID,
                                IsProduct: false
                            }).then(response => {
                                var res = response.data;
                                if (res.Message === 'Comment deleted.') {
                                    $('#' + commentID).css({ 'background': 'red', 'color': 'white' }).fadeTo("slow", 0.2, function () {
                                        $(this).remove();
                                    });
                                    $('#commentHolder').attr('placeholder', 'Add Delivery Comment');
                                    $('#commentHolder').val('');
                                    app.placeOrder.CommentID = 0;
                                    $('#divComments').find('input[type=checkbox]').prop('checked', false);
                                }
                                console.log(res);
                                $.unblockUI();
                            });
                        }
                    },
                    cancel: function () {
                        //$.alert('Canceled!');
                    }
                }
            });
        },

        // Set comment for the order
        getSelectedComment: function (commentID, e, comment) {
            var state = $(e.target).prop('checked');

            $('#divComments').find('input[type=checkbox]').prop('checked', false);

            $(e.target).prop('checked', state);
            this.placeOrder.CommentID = commentID;

            if ($(e.target).prop('checked')) {
                $('#commentHolder').attr('placeholder', comment);
                $('#commentHolder').val(comment);
            } else {
                $('#commentHolder').val('');
                $('#commentHolder').attr('placeholder', 'Add Delivery Comment');
                this.placeOrder.CommentID = 0;
            }
        },

        // Add new comment into the database
        addNewComment: function (isProduct, isInvoice) {
            if (this.newComment.trim().length === 0) {
                showAlert(null, 'Please enter a comment');
                return false;
            }

            ajaxStartLoading();

            this.comments = [];
            axios.post(Router.action('Main', 'AddComments'), {
                IsProduct: isProduct,
                IsInvoice: isInvoice,
                CommentDescription: this.newComment,
                ProductID: this.commentProdID
            }).then(response => {
                let res = response.data;
                console.log(res);
                if (res.Message !== 'Success') {
                    showAlert(null, res.Message.replace('!', ''));
                }
                else {
                    if (isInvoice) {
                        $('#txtComment').val('');
                        $('#modalAddComment').modal('hide');
                        $('#commentHolder').val(this.newComment);
                        this.getComments(0, false);
                        setTimeout(function () {
                            $('#chk_0').prop('checked', true);
                            app.getSelectedCommentAfterAdd(app.newComment);
                            app.newComment = '';
                        }, 800);
                    } else {
                        $('#txtProdComment').val('');
                        $('#modalAddProdComment').modal('hide');
                        this.getComments(this.commentProdID, true);
                        setTimeout(function () {
                            $('#chkp_0').prop('checked', true);
                        }, 500);
                    }
                }

                $.unblockUI();
            });
        },

        //BI Popup
        continueToPlaceOrder: function (type) {
            if (this.isOnlyDonation) {
                var placeOrderModel = this.placeOrder;
                var content = '<h4>Are you sure you want to place the order?</h4>';

                $.confirm({
                    theme: 'material',
                    title: false,
                    content: content,
                    type: 'dark',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin) {
                                    $('#modalAddStripe').modal('show');
                                    return;
                                } else {
                                    app.placeOrderFinalWithStripe(placeOrderModel, null);
                                }
                            }
                        },
                        cancel: function () {
                            //$.alert('Canceled!');
                        }
                    }
                });
                return false;
            }

            if (!type) {
                if (this.amountWOdonation < this.minCartValue && this.amountWOdonation > 0) {
                    showAlert('Minimum Order Value', 'Sorry, the minimum order value is $' + this.minCartValue + '.00. Please add further items to ensure you can place your order');
                } else {
                    $('#modalPickupDelivery').modal('show');
                }
            }
        },

        loadStripeForm: function () {
            axios.get(Router.action('Main', 'GetStripeKeys'))
                .then(response => {
                    var res = response.data.result

                    var stripe = Stripe(res.Result.PublicKey, {
                        stripeAccount: res.Result.ConnectedAccountID
                    });
                    var elements = stripe.elements();
                    var style = {
                        base: {
                            color: "#32325d",
                        }
                    };

                    var card = elements.create("card", { style: style });
                    card.mount("#stripe-card-element");

                    card.addEventListener('change', function (event) {
                        var displayError = document.getElementById('stripe-card-errors');
                        if (event.error) {
                            displayError.textContent = event.error.message;
                        } else {
                            displayError.textContent = '';
                        }
                    });

                    var form = document.getElementById('stripe-payment-form');
                    form.addEventListener('submit', function (ev) {
                        ev.preventDefault();
                        app.submitStripePayment(stripe, card);
                    });
                });
        },

        saveStripeResponse: function (data) {
            axios.post(Router.action('Main', 'SavePaymentLog'), { payResponse: data, tempCartID: app.placeOrder.TempCartID }).then(response => {
                console.log(response);
            });
        },

        submitStripePayment: function (stripe, card) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'GetPaymentToken'), { PaymentTotal: this.ChargedAmount, IsCapture: this.IsCapture, TempCartID: app.placeOrder.TempCartID }).then(response => {
                //console.log(response);
                var clientSecret = response.data.result;
                stripe.confirmCardPayment(clientSecret, {
                    payment_method: {
                        card: card,
                        //billing_details: {
                        //    name: 'Jenny Rosen'
                        //}
                    }
                }).then(function (result) {
                    if (result === undefined || result === null || result === '') {
                        app.saveStripeResponse('Error Occur During Payment From Web View and result is null.');
                        $('#stripe-card-errors').text('We cannot proceed with order because of payment error. Please contact admin if it is a problem.');
                        $.unblockUI();
                        return;
                    }
                    var respStr = JSON.stringify(result);
                    app.saveStripeResponse(respStr);
                    //console.log(result);
                    if (result.error) {
                        // Show error to your customer (e.g., insufficient funds)
                        //console.log(result.error.message);
                        $('#stripe-card-errors').text(result.error.message);
                        app.saveStripeResponse('Error Occur During Payment From Web View and Error Message :' + result.error.message);
                        $.unblockUI();
                    } else {
                        // The payment has been processed!
                        if (result.paymentIntent.status === 'succeeded' || result.paymentIntent.status === 'requires_capture') {
                            result.paymentIntent.client_secret = btoa(result.paymentIntent.client_secret);
                            //$.unblockUI();
                            $('#modalAddStripe').modal('hide');
                            app.placeOrderFinalWithStripe(app.placeOrder, {
                                Status: result.paymentIntent.status,
                                Id: result.paymentIntent.id,
                                Amount: result.paymentIntent.amount,
                                Currency: result.paymentIntent.currency,
                                CustomerId: '',
                                Livemode: result.paymentIntent.livemode,
                                ReceiptEmail: result.paymentIntent.receipt_email,
                                ClientSecret: result.paymentIntent.client_secret
                            });
                        } else {
                            app.saveStripeResponse('Error Occur During Payment From Web View and Payment status :' + result.paymentIntent.status);
                            $('#stripe-card-errors').text('We cannot proceed with order because of payment status: ' + result.paymentIntent.status + '. Please contact admin if it is a problem.');
                            $.unblockUI();
                        }
                    }
                });
            });
        },

        // Place order final with stripe or without stripe
        placeOrderFinalWithStripe: function (placeOrder, paymentDetails) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'PlaceOrder'), { model: placeOrder, paymentDetails: paymentDetails }).then(response => {
                var res = response.data;
                console.log(res);

                if (res.Res.Message === 'Order Placed') {
                    $.confirm({
                        columnClass: 'medium',
                        title: false,
                        content: '<span class="fs-19">Order has been placed successfully. Your Order No is ' + res.Res.Data.OrderID + '</span>',
                        type: 'dark',
                        buttons: {
                            Ok: {
                                text: 'Continue',
                                btnClass: 'btn-blue',
                                keys: ['enter', 'shift'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'SearchProducts');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "Order Saved successfully.") {
                    //$.alert('Order saved successfully.');
                    $.alert({
                        title: 'Success!',
                        content: '<span class="fs-19">Order saved successfully</span>',
                        type: 'blue',
                        buttons: {
                            Ok: {
                                text: 'Ok',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "You already have a saved Order. Please delete it or append items to that order.") {
                    showAlert(null, 'You already have a saved Order. Please delete it or append items to that order.');
                } else {
                    showAlert(null, 'Order posting failed. Message : ' + res.Res.Message);
                }
                $.unblockUI();
            });
        },

        // place or save order to db
        placeOrderToDB: function (isSavedOrder) {
            if (this.showBIPopup) {
                $('#modalOrderIn').modal('hide');
            }

            var content = '<h4>Are you sure you want to place the order?</h4>';
            var btnText = 'Place Order';
            if (isSavedOrder) {
                this.placeOrder.SaveOrder = true;
                content = '<h4>Are you sure you want to save this order?</h4>';
                btnText = 'Save Order';
            } else {
                this.placeOrder.SaveOrder = false;
            }

            var placeOrderModel = this.placeOrder;
            if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin && app.subTotal > 0) {
                $.confirm({
                    theme: 'material',
                    title: false,
                    content: content,
                    type: 'dark',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                $('#modalAddStripe').modal('show');
                                return;
                            }
                        },
                        cancel: function () {
                            //$.alert('Canceled!');
                        }
                    }
                });
            }
            else {
                $.confirm({
                    theme: 'material',
                    title: false,
                    content: content,
                    type: 'dark',
                    buttons: {
                        placeOrder: {
                            btnClass: 'btn-primary',
                            action: function () {
                                if (app.isPOEnabled && isSavedOrder === false) {
                                    $.confirm({
                                        title: 'PO Number',
                                        type: 'dark',
                                        content: '' +
                                            '<form action="" class="formName">' +
                                            '<div class="form-group">' +
                                            '<label>Enter PO Number</label>' +
                                            '<input type="text" class="poNum form-control" />' +
                                            '</div>' +
                                            '</form>',
                                        buttons: {
                                            formSubmit: {
                                                text: 'OK',
                                                btnClass: 'btn-blue',
                                                action: function () {
                                                    var poNum = this.$content.find('.poNum').val();
                                                    placeOrderModel.PONumber = poNum;
                                                    if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin && app.subTotal > 0) {
                                                        $('#modalAddStripe').modal('show');
                                                        return;
                                                    }
                                                    else {
                                                        app.placeOrderFinalWithStripe(placeOrderModel, null);
                                                    }
                                                    //app.placeOrderFinal(placeOrderModel);
                                                }
                                            },
                                            cancel: {
                                                text: 'Skip PO',
                                                action: function () {
                                                    if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin && app.subTotal > 0) {
                                                        $('#modalAddStripe').modal('show');
                                                        return;
                                                    }
                                                    else {
                                                        app.placeOrderFinalWithStripe(placeOrderModel, null);
                                                    }
                                                    //app.placeOrderFinal(placeOrderModel);
                                                }
                                            },
                                            skip: {
                                                text: 'Cancel'
                                            }
                                        },
                                        onContentReady: function () {
                                            // bind to events
                                            var jc = this;
                                            this.$content.find('form').on('submit', function (e) {
                                                // if the user submits the form by pressing enter in the field.
                                                e.preventDefault();
                                                jc.$$formSubmit.trigger('click'); // reference the button and click it
                                            });
                                        }
                                    });
                                } else {
                                    if (app.IsStripePaymentCustomer && app.IsStripePaymentAdmin && app.subTotal > 0) {
                                        $('#modalAddStripe').modal('show');
                                        return;
                                    }
                                    else {
                                        app.placeOrderFinalWithStripe(placeOrderModel, null);
                                    }
                                    //app.placeOrderFinal(placeOrderModel);
                                }
                            }
                        },
                        cancel: function () {
                            //$.alert('Canceled!');
                        }
                    }
                });
            }
        },

        // Place order final
        placeOrderFinal: function (placeOrderModel) {
            ajaxStartLoading();
            axios.post(Router.action('Main', 'PlaceOrder'), placeOrderModel).then(response => {
                var res = response.data;
                console.log(res);

                if (res.Res.Message === 'Order Placed') {
                    $.confirm({
                        columnClass: 'medium',
                        title: false,
                        content: '<span class="fs-19">Order has been placed successfully. Your Order No is ' + res.Res.Data.OrderID + '</span>',
                        type: 'dark',
                        buttons: {
                            Ok: {
                                text: 'Continue',
                                btnClass: 'btn-blue',
                                keys: ['enter', 'shift'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "Order Saved successfully.") {
                    //$.alert('Order saved successfully.');
                    $.alert({
                        title: 'Success!',
                        content: '<span class="fs-19">Order saved successfully</span>',
                        type: 'blue',
                        buttons: {
                            Ok: {
                                text: 'Ok',
                                btnClass: 'btn-blue',
                                keys: ['enter'],
                                action: function () {
                                    window.location.href = Router.action('Main', 'DefaultPantry');
                                }
                            }
                        }
                    });
                } else if (res.Res.Message === "You already have a saved Order. Please delete it or append items to that order.") {
                    showAlert(null, 'You already have a saved Order. Please delete it or append items to that order.');
                } else {
                    showAlert(null, 'Order posting failed. Message : ' + res.Res.Message);
                }
                $.unblockUI();
            });
        },

        // Set delivery date from checkboxes
        setDetaultDate: function (e) {
            if ($(e.target).attr('id') === 'modalClose') {
                this.deliveryDate = '';
                $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
                return;
            }

            var state = $(e.target).prop('checked');

            if (this.deliveryDate != '') {
                var myDatepicker = $('.datepicker-here').datepicker().data('datepicker');
                myDatepicker.clear();
            }
            $(e.target).prop('checked', state);

            if ($(e.target).attr('id') == 'chkDate1') {
                $('#chkDate2').prop('checked', false);
                this.deliveryDate = this.di.orderDatesLong[0];
                // $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
            } else {
                $('#chkDate1').prop('checked', false);
                this.deliveryDate = this.di.orderDatesLong[1];
                // $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[1]), "MM/DD/YYYY").format('LL') + '</b>');
            }

            if ($('#chkDate1').prop('checked') === false && $('#chkDate2').prop('checked') === false && this.deliveryDate === '') {
                //$('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
            }

            if ($('#chkDate1').prop('checked') === false && $('#chkDate2').prop('checked') === false) {
                this.deliveryDate = '';
                // $('#dateToggle').html('SELECTED DATE: <b>' + moment(JSON.stringify(this.di.orderDatesLong[0]), "MM/DD/YYYY").format('LL') + '</b>');
            }
        },

        // set delivery date into the Json model
        setDeliveryDate: function () {
            if (this.deliveryDate === '') {
                var msg = 'Please choose a delivery date';
                if ($('#ddlDateType').val() === 'U') {
                    msg = 'Please choose a pick up date';
                }
                showAlertRed(null, msg);
            } else {
                var msg = 'Confirm delivery date';
                if ($('#ddlDateType').val() === 'U') {
                    msg = 'Confirm pick up date';
                }
                $.confirm({
                    title: msg,
                    content: 'Is <b>' + moment(app.deliveryDate).format('LL') + '</b> correct?',
                    type: 'dark',
                    buttons: {
                        confirm: {
                            text: 'Yes',
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function () {
                                //debugger;
                                app.placeOrder.OrderDate = app.deliveryDate; // moment(app.deliveryDate).format('DD/MM/YYYY');
                                app.placeOrder.validateDate = app.deliveryDate;
                                $('#dateToggle').html('SELECTED DATE: <b>' + moment(app.deliveryDate).format('LL') + '</b>');
                                $('#modalDeliveryDate').modal('hide');
                                app.placeOrderToDB(false);
                            }
                        },
                        cancel: function () {
                            $('#modalDeliveryDate').modal('show');
                        }
                    }
                });
            }
        },

        // message to display on web for telephone no.
        mobilePopup: function () {
            showAlertRed(null, 'Call functionality is not available on this device');
        },

        // Get the product comments
        getProductComments: function (pID, cID, item) {
            this.commentProdID = pID;
            this.prodComments = [];
            this.selectedCartItem = item;
            this.isChanged = false;
            ajaxStartLoading();

            this.getComments(pID, true);
            $('#modalProdComments').modal('show');

            setTimeout(function () {
                $.unblockUI();
                var chkp = $('#chkp_' + cID);
                if (chkp != undefined) {
                    chkp.prop('checked', true);
                }
            }, 1000);
        },

        // Set a product commentin modal
        setProductComment: function (e) {
            var state = $(e.target).prop('checked');
            $('#divProdComments').find('input[type=checkbox]').prop('checked', false);
            $(e.target).prop('checked', state);
            this.isChanged = true;

            if (state) {
                this.prodCommentID = $(e.target).data('cid');
            } else {
                this.prodCommentID = 0;
            }
        },

        // choose a different comment for the product
        updateProductComment: function () {
            //var sel = $('#divProdComments').find('input[type=checkbox]').prop('checked', true);

            var item = this.selectedCartItem;
            if (this.isChanged) {
                item.ProdCommentID = this.prodCommentID;
                this.updateCartItem(item);
            }
            $('#modalProdComments').modal('hide');
        },

        // Get suggestive items for the customer
        getSuggestiveItems: function (cartID) {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetSuggestiveItems'), {
                cartID: cartID
            }).then(response => {
                console.log(response);
            });
        },

        // Check if the delivery type for the order is Delivery of pickup
        checkDeliveryOrPickup: function () {
            if (this.isDelivery && !this.isLeave) {
                showAlert(null, "Sorry, but we can't do deliveries without having your permission to leave");
            } else if (!$('#cbPickup').prop('checked') && !$('#cbDelivery').prop('checked')) {
                showAlert(null, "Please choose one delivery option");
            } else {
                $('#modalPickupDelivery').modal('hide');

                if (this.isDelivery) {
                    this.placeOrder.IsDelivery = true;
                    this.placeOrder.IsLeave = this.isLeave;
                    this.placeOrder.IsContactless = this.isContactless;
                    this.placeOrder.DeliveryType = 'Delivery';
                    if (this.isMultipleAddresses) {
                        this.addressModal = true;
                    } else {
                        $('#modalDeliveryDate').modal('show');
                    }
                } else {
                    this.placeOrder.DeliveryType = 'Pick Up';
                    this.placeOrder.IsDelivery = false;

                    this.pickupModal = true;
                    if (this.placeOrder.HasFreightCharges) {
                        this.placeOrder.HasFreightCharges = false;
                        this.subTotal = this.subTotal - this.deliveryCharges;
                        app.ChargedAmount = app.subTotal;
                    }

                    //$('#modalDeliveryDate').modal('show');
                }
            }
        },

        // show hide fields based on the selection
        updateDeliveryOptions: function (ctrl, type) {
            var val = $(ctrl.target).prop('checked');

            if (type === 'L') {
                this.isLeave = val;
            } else if (type === 'D') {
                this.isDelivery = val;
                this.isPickup = false;
            } else if (type === 'P') {
                this.isPickup = val;
                this.isDelivery = false;
            } else {
                this.isContactless = val;
            }
        },

        // Close the applu promocode modal and set the values for the parameters
        promoModalClose: function () {
            this.promoApplySuccess = false;
            this.promoModal = false;
            this.appliedPromo = '';
            this.promoResponse = '';
        },

        // Apply and check if the promocode is a valid one or not
        applyPromoCode: function () {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'ApplyPromoCode'), {
                cartID: app.tempCartID,
                promoCode: app.appliedPromo
            }).then(response => {
                console.log(response);
                var res = response.data;
                if (res.Message === 'Success') {
                    var promo = res.Result;
                    if (promo.TotalAmountWithDiscount < promo.TotalWithGst && promo.TotalAmountWithDiscount >= 0) {
                        this.isCouponApplied = true;
                        this.couponAmount = promo.TotalWithGst - promo.TotalAmountWithDiscount;
                        this.promoApplySuccess = true;

                        app.placeOrder.IsCouponApplied = true;
                        app.placeOrder.Coupon = app.appliedPromo;
                        app.placeOrder.CouponAmount = app.couponAmount;

                        if (this.placeOrder.HasFreightCharges) {
                            this.subTotal = promo.TotalAmountWithDiscount + this.deliveryCharges;
                        } else {
                            this.subTotal = promo.TotalAmountWithDiscount;
                        }
                        app.ChargedAmount = app.subTotal;
                    } else {
                        //this.isCouponApplied = false;
                        //this.couponAmount = 0;
                        this.promoResponse = promo.Message;
                        this.appliedPromo = '';
                    }
                }
                $.unblockUI();
            });
            //alert(this.appliedPromo);
        },

        // Get delivery addresses for the customer
        getDeliveryAddresses: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetCustomerAddresses')).then(response => {
                if (response.data.Message === 'Success') {
                    this.customerAddresses = response.data.Result;
                    //  this.addressModal = true;
                }
            });
        },

        // Get pickup locations for the customers
        getPickupLocations: function () {
            ajaxStartLoading();
            // bind Products
            axios.get(Router.action('Main', 'GetPickupLocations')).then(response => {
                if (response.data.Message === 'Success') {
                    this.pickupLocations = response.data.Result.Pickups;
                    //  this.addressModal = true;
                }
            });
        },

        // Set addressid in the order modal
        setAddress: function () {
            if (this.selectedAddress === 0) {
                showAlert(null, 'Sorry, you must select at least one delivery location');
            } else {
                var isValidAddress = true;
                $.each(app.customerAddresses, function (key, val) {
                    if (val.AddressId === app.selectedAddress && val.AddressCode !== 'Y') {
                        showAlertRed(null, "Sorry, that selected suburb is currently not in our delivery coverage area. Please either select a different suburb or select a pick-up location nearest to you.");
                        isValidAddress = false;
                        return false;
                    }
                });

                if (!isValidAddress) {
                    return false;
                };

                this.placeOrder.AddressID = this.selectedAddress;
                this.placeOrder.PickupID = 0;
                this.placeOrder.IsDelivery = true;
                this.placeOrder.DeliveryType = 'Delivery';

                this.addressModal = false;
                this.GetPickupOrDeliveryDates(0, true, this.selectedAddress);

                this.doubleCheckFreight();
                //setTimeout(function () {
                //    $('#modalDeliveryDate').modal('show');
                //}, 500);
            }
        },

        // Set pickup id in the order modal
        setPickup: function () {
            if (this.selectedPickup === '') {
                showAlert(null, 'Sorry, you must select at least one pick-up location');
            } else {
                this.placeOrder.PickupID = this.selectedPickup;
                // Add code to fetch delivery dates
                var myDatepicker = $('.datepicker-here').datepicker().data('datepicker');
                myDatepicker.destroy();

                this.pickupModal = false;

                this.placeOrder.IsDelivery = false;
                this.placeOrder.DeliveryType = 'Pick up';
                this.placeOrder.AddressID = 0;

                this.GetPickupOrDeliveryDates(this.selectedPickup, false, 0);

                setTimeout(function () {
                    $('#modalDeliveryDate').modal('show');
                }, 500);
            }
        },

        // Get Delivery Dates based on selection
        GetPickupOrDeliveryDates: function (pickupID, isDelivery, addressID) {
            ajaxStartLoading();
            // bind Products
            axios.post(Router.action('Main', 'GetPickupOrDeliveryDates'), {
                pickupID: pickupID,
                isDelivery: isDelivery,
                addressID: addressID
            }).then(response => {
                this.di = response.data.Result;
                if (pickupID > 0) {
                    $('#ddlDateType').val('U');
                } else {
                    $('#ddlDateType').val('D');
                }

                this.initDatepicker();
                $.unblockUI();
            });
        },

        // Get date based on DDL inside date popup
        GetPickupDeliveryDDL: function () {
            var type = $('#ddlDateType').val();
            app.deliveryDate = '';
            if (type === 'D') {
                $('#modalDeliveryDate').modal('hide');
                this.addressModal = true;
                this.placeOrder.IsDelivery = true;
                this.placeOrder.DeliveryType = 'Delivery';
            } else {
                $('#modalDeliveryDate').modal('hide');
                this.selectedPickup = '';
                this.placeOrder.IsDelivery = false;
                this.placeOrder.DeliveryType = 'Pick up';

                this.pickupModal = true;
                if (this.placeOrder.HasFreightCharges) {
                    this.placeOrder.HasFreightCharges = false;
                    this.subTotal = this.subTotal - this.deliveryCharges;
                    app.ChargedAmount = app.subTotal;
                }
            }
        },

        // Add delivery address
        addDeliveryAddress: function () {
            console.log(this.addressModel);
            ajaxStartLoading();
            axios.post(Router.action('Main', 'AddDeliveryAddress'), { model: this.addressModel }).then(response => {
                if (response.data.Message === 'Success') {
                    this.addAddressModal = false;

                    this.getDeliveryAddresses();
                    this.addressModal = true;
                    this.selectedAddress = response.data.Result.AddressId;
                    this.$refs.form.reset();
                } else {
                    showAlertRed(null, response.data.Message);
                }
                $.unblockUI();
            });
        },

        reset() {
            this.addAddressModal = false;
            //this.addressModal = true;
            this.$refs.form.reset();
        },

        // Initialize the Google Map API
        initMap: function () {
            // geographical location types.
            this.autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('street_number'), { types: ['geocode'], placeholder: '' });

            // Set initial restrict to the greater list of countries.

            this.autocomplete.setComponentRestrictions({ 'country': ['au'] });

            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the address components.

            this.autocomplete.setFields(['address_component']);

            // When the user selects an address from the drop-down, populate the
            // address fields in the form.

            this.autocomplete.addListener('place_changed', this.fillInAddress);
            setTimeout(function () {
                $('#street_number').removeAttr('placeholder');
            }, 30);
        },

        // Fillthe address
        fillInAddress: function () {
            // Get the place details from the autocomplete object.
            var place = this.autocomplete.getPlace();

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (this.componentForm[addressType]) {
                    var val = place.address_components[i][this.componentForm[addressType]];
                    //if (addressType === 'route') {
                    //    //var s1 = $('#street_number').val();
                    //    //$('#street_number').val(s1 + ' ' + val);
                    //    app.addressModel.streetAddress = app.addressModel.streetAddress + ' ' + val;
                    //} else {
                    //    //$('#' + addressType).val(val);
                    //    if (addressType === 'street_number') {
                    //        app.addressModel.streetAddress = val;
                    //    }
                    //}
                    if (addressType === 'locality') {
                        app.addressModel.suburb = val;
                    } else if (addressType === 'postal_code') {
                        app.addressModel.postCode = val;
                    }
                }
            }
            app.addressModel.streetAddress = '';
            app.addressModel.streetAddress = $('#street_number').val().split(',')[0];
        },

        deleteAddress: function () {
            $.confirm({
                title: '',
                content: 'Are you sure you want to delete this address?',
                type: 'dark',
                buttons: {
                    NO: {
                        text: 'NO',
                        btnClass: 'btn-blue',
                        action: function () {
                        }
                    },
                    YES: {
                        text: 'YES',
                        btnClass: 'btn-blue',
                        action: function () {
                            ajaxStartLoading();
                            // bind Products
                            axios.post(Router.action('Main', 'DeleteDeliveryAddress'), { addressID: app.selectedAddress }).then(response => {
                                app.getDeliveryAddresses();
                                app.selectedAddress = 0;
                                $.unblockUI();
                            });
                        }
                    }
                }
            });
        },

        doubleCheckFreight: function () {
            if (this.amountWOdonation < this.minFreightValue && this.amountWOdonation > 0 && !app.placeOrder.HasFreightCharges) {
                // showAlert('Freight Charges', this.deliveryMessage);
                $.confirm({
                    title: 'Delivery Charges',
                    content: app.deliveryMessage,
                    type: 'dark',
                    buttons: {
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                            }
                        },
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-blue',
                            action: function () {
                                app.placeOrder.HasFreightCharges = true;
                                // app.cartTotal = app.cartTotal + app.deliveryCharges;
                                app.subTotal = app.subTotal + app.deliveryCharges;
                                app.ChargedAmount = app.subTotal;
                                $('#modalDeliveryDate').modal('show');
                            }
                        }
                    }
                });
            } else {
                $('#modalDeliveryDate').modal('show');
            }
        },

        checkAddressPhone: function (e) {
            var evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var el = $(e.target);

            if ((charCode > 31 && (charCode < 48 || charCode > 57))) {
                evt.preventDefault();
            } else {
                if (el.val().length < 1 && charCode !== 48) {
                    app.addressModel.phone = '0' + el.val();
                    //el.val('0' + el.val());
                }
                return true;
            }
        },

        getSelectedCommentAfterAdd: function (comment) {
            var cID = $('#chk_0').data('id');

            this.placeOrder.CommentID = cID;
            $('#commentHolder').attr('placeholder', comment);
            $('#commentHolder').val(comment);
        }
    }
});