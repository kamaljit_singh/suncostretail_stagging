﻿using System.Web;
using System.Web.Optimization;

namespace Saavi5Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/Login").Include(
                     "~/scripts/jquery-3.2.1.min.js",
                     "~/scripts/bootstrap.min.js",
                     "~/scripts/jquery.mCustomScrollbar.concat.min.js",
                     "~/scripts/stacktable.min.js",
                     "~/scripts/jquery.blockUI.js",
                     "~/Scripts/jquery.unobtrusive-ajax.min.js",
                      "~/scripts/globalfunctions.js",
                     "~/Scripts/scripts.js"));

            bundles.Add(new StyleBundle("~/Content/login").Include(
                     "~/Content/css/bootstrap.min.css",
                     "~/Content/css/styles.css",
                     "~/Content/css/jquery.mCustomScrollbar.min.css"
                     ));

            //POD scripts
            bundles.Add(new ScriptBundle("~/Scripts").Include(
                //  "~/Scripts/main_script.js",
                "~/scripts/jquery-3.2.1.min.js",
               "~/Scripts/bootstrap.min.js",
                "~/Scripts/bootstrap-datepicker.min.js",
               // "~/Scripts/tableHeadFixer.js",
               "~/Scripts/jquery.mCustomScrollbar.concat.min.js",
                "~/scripts/jquery.blockUI.js",
                     "~/Scripts/jquery.unobtrusive-ajax.min.js",
               "~/Scripts/scripts.js",
               "~/Scripts/jquery.validate.min.js",
               "~/Scripts/jquery-confirm.min.js",
               "~/Scripts/jquery.bootstrap-growl.min.js",
                "~/scripts/globalfunctions.js"
                ));
        }
    }
}
