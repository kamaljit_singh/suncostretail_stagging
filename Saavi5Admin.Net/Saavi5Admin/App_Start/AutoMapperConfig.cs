﻿using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Saavi5Admin.App_Start
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<CustomerMaster, CustomerMaster>().ReverseMap();
                config.CreateMap<CustomerMaster, CustomerBO>().ReverseMap();
                config.CreateMap<CustomerMaster, AddEditCustomerBO>().ReverseMap();
                config.CreateMap<ProductMaster, GetAllProductBO>().ReverseMap();
                config.CreateMap<NoticeMaster, GetNoticeBO>().ReverseMap();
            });
        }
    }
}