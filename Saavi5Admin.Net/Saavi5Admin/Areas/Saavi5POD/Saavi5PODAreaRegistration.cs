﻿using System.Web.Mvc;

namespace Saavi5Admin.Areas.Saavi5POD
{
    public class Saavi5PODAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Saavi5POD";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Saavi5POD_default",
                "POD/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Saavi5Admin.Areas.Saavi5POD.Controllers" }
            );
        }
    }
}