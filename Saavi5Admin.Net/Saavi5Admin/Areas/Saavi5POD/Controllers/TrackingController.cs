﻿using DataModel.BO;
using DataModel.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Saavi5Admin.Areas.Saavi5POD.Controllers
{
    public class TrackingController : Controller
    {
        private readonly TrackingDA _da;
        public TrackingController()
        {
            _da = new TrackingDA();
        }
        // GET: Saavi5POD/Tracking
        public ActionResult Tracking()
        {            
            return View();
        }
        //GET: vehicle list
        public ActionResult _VehicleList(string id)
        {
            GetListModel model = new GetListModel();
            model.SearchText = id;
            ViewBag.Vehicle = _da.GetAllVehicle(model);
            return PartialView();
        }
        //GET: vehicle location
        public ActionResult _VehicleLocation(long? id)
        {
            ViewBag.DeliveryTrackList = _da.GetDeliveryTrackList(id);
            return PartialView();
        }
    }
}