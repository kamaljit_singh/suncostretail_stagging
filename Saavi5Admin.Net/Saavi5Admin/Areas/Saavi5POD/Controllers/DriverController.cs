﻿using DataModel.BO;
using DataModel.DA;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Saavi5Admin.Areas.Saavi5POD.Controllers
{
    public class DriverController : Controller
    {
        private readonly DriverDA _da;
        public DriverController()
        {
            _da = new DriverDA();
        }
        #region GET methods
        // GET: Driver
        public ActionResult ManageDrivers()
        {
            var model = new GetListModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.SearchText = "";

            var data = _da.GetAllDrivers(model);
            ViewBag.Driver = data;
            ViewBag.Vehicle = _da.GetAllVehicles(model).Where(x=>x.IsDeleted==false).ToList();
            return View();
        }
        // GET:  manage Driver
        public ActionResult _ManageDriver(int? id)
        {
            if (id > 0)
            {
                var data = _da.GetUserByID(id);
                return PartialView(data);
            }
            else
            {
                return PartialView(new UserBO());
            }
        }
        //GET: driver detail
        public ActionResult _DriverDetail(int? id)
        {
            var data = _da.GetUserByID(id);
            return PartialView(data);
        }

        //GET: vehicle
        public ActionResult ManageVehicle()
        {
            var model = new GetListModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.SearchText = "";

            var data = _da.GetAllVehicles(model);
            ViewBag.Vehicle = data;
            ViewBag.Driver = _da.GetAllDrivers(model);
            return View();
        }
        //GET: manage vehicle
        public ActionResult _ManageVehicle(int? id)
        {
            if (id > 0)
            {
                var data = _da.GetVehicleByID(id);
                var runs = _da.GetDeliveryRuns();

                ViewBag.Runs = runs;

                return PartialView(data);
            }
            else
            {
                var data = new VehicleBO();
                data.IsFixturesFittingOK = data.IsLightSignalOK = data.IsTyreConditionOK = data.IsVehicleConditionOK = data.IsWindscreenOK = true;
                var runs = _da.GetDeliveryRuns();

                ViewBag.Runs = runs;
                return PartialView(data);
            }

        }
        //GET: vehicle detail
        public ActionResult _VehicleDetail(int? id)
        {
            var data = _da.GetVehicleByID(id);
            return PartialView(data);
        }

        #endregion

        #region Search vehicle   
        [HttpPost]
        public JsonResult GetAllVehicle(string id, string type)
        {
            try
            {
                GetListModel model = new GetListModel { SearchText = id };
                if (type == "d")
                {
                    return Json(new { Message = "Success", Result = _da.GetAllDrivers(model) });
                }
                else { return Json(new { Message = "Success", Result = _da.GetAllVehicles(model) }); }

            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }
        #endregion

        #region Manage Driver
        /// <summary>
        ///  Manage Driver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageDrivers(UserBO model)
        {
            try
            {
                if (Request.Files["fuLicense"] != null)
                {
                    model.licenceImage = Request.Files["fuLicense"];
                }
                if (Request.Files["fuimage"] != null)
                {
                    model.driverImage = Request.Files["fuimage"];
                }
                var data = _da.ManageDriver(model);
                ViewData["Message"] = data;
                //return RedirectToAction("ManageDrivers");
                var model2 = new GetListModel();
                model2.PageNumber = 0;
                model2.PageSize = 50;
                model2.SearchText = "";

                var data2 = _da.GetAllDrivers(model2);
                ViewBag.Driver = data2;
                ViewBag.Vehicle = _da.GetAllVehicles(model2);
                return View();
                //  return PartialView(model);
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                var model2 = new GetListModel();
                model2.PageNumber = 0;
                model2.PageSize = 50;
                model2.SearchText = "";

                var data2 = _da.GetAllDrivers(model2);
                ViewBag.Driver = data2;
                ViewBag.Vehicle = _da.GetAllVehicles(model2);
                return View();
            }
        }
        #endregion

        #region Assign Vehicle To Driver
        public JsonResult AssignVehicleToDriver(int vID, int dID)
        {
            try
            {
                var data = _da.AssignVehicleToDriver(vID, dID);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region Manage Vehicle
        [HttpPost]
        public JsonResult ManageVehicle(VehicleBO model)
        {
            try
            {
                var data = _da.ManageVehicle(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
        #endregion

        #region Delete vehicle
        [HttpPost]
        public JsonResult DeleteVehicle(int vehicleID, bool val)
        {
            try
            {
                var data = _da.DeleteVehicle(vehicleID, val);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion


    }
}