﻿using DataModel.BO;
using DataModel.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Saavi5Admin.Areas.Saavi5POD.Controllers
{
    public class DeliveriesController : Controller
    {
        private readonly DeliveriesDA _da;

        public DeliveriesController()
        {
            _da = new DeliveriesDA();
        }

        #region GET methods

        // GET: Saavi5POD/Deliveries
        public ActionResult Deliveries()
        {
            //var model = new GetListModel();
            //model.PageNumber = 0;
            //model.PageSize = 50;
            //model.SearchText = "";
            //ViewBag.Customer = _da.GetAllCustomer(model);
            return View();
        }

        //GET: Customer
        public ActionResult _Customers(string id)
        {
            GetListModel model = new GetListModel { SearchText = id };
            //var model = new GetListModel();
            //model.PageNumber = 0;
            //model.PageSize = 50;
            //model.SearchText = "";
            ViewBag.Customer = _da.GetAllCustomer(model);
            return PartialView();
        }

        //GET: Orders
        public ActionResult _Orders(int? id, int orderid, string type, DateTime? delDate)
        {
            ViewBag.Orders = _da.GetAllOrderByCustomerID(id, orderid, type, delDate);
            return PartialView();
        }

        //GET: Order detail
        public ActionResult _OrderDetail(int id)
        {
            var data = _da.GetOrderDeliveryDetail(id);
            return View(data);
        }

        //GET: Deliveries By Invoice
        public ActionResult DeliveriesByInvoice()
        {
            // ViewBag.Orders = _da.GetAllOrderByCustomerID(0, 0);
            return View();
        }

        //GET: Issues
        public ActionResult Issues()
        {
            return View();
        }

        //GET: Issues By Invoice
        public ActionResult IssuesByInvoice()
        {
            // ViewBag.Orders = _da.GetAllOrderByCustomerID(0, 0);
            return View();
        }

        //GET: deliveries by driver
        public ActionResult DeliveriesByDriver()
        {
            return View();
        }

        //GET: Delivery images
        public ActionResult _DeliveryImages(int id, bool hasissue)
        {
            ViewBag.DeliveryImages = _da.GetDeliveryImages(id, hasissue);
            return PartialView();
        }

        //GET: pod copy
        public ActionResult PODcopy()
        {
            return View();
        }

        //GET: POD all customers
        public ActionResult _PODAllCustomers(string id)
        {
            GetListModel model = new GetListModel { SearchText = id };
            ViewBag.Customer = _da.GetAllCustomer(model);
            return PartialView();
        }

        //GET POD all orders by customer
        public ActionResult _PODAllOrdersByCustomer(int? id)
        {
            ViewBag.Orders = _da.GetPODAllOrdersByCustomerID(id);
            return PartialView();
        }

        //GET: Get all existing driver with assigned vehicle
        public ActionResult _AssignedDrivers(string id)
        {
            GetListModel model = new GetListModel { SearchText = id };
            ViewBag.AssignedDrivers = _da.GetAllAssignedDrivers(model);
            return PartialView();
        }

        //GET: Get all assigned orders
        public ActionResult _AssignedOrders(int id, string runno, string suburb = "")
        {
            ViewBag.AssignedOrders = _da.GetAllPendingOrders(id, runno, suburb);
            return PartialView();
        }

        //GET: Get all unassigned orders
        public ActionResult _UnassignedOrderList(int id, string runno, string suburb = "")
        {
            ViewBag.UnassignedOrders = _da.GetAllPendingOrders(id, runno, suburb);
            return PartialView();
        }

        //GET: Order short detail
        public ActionResult _OrderShortDetail(int id)
        {
            var data = _da.GetOrderDeliveryDetail(id);
            return View(data);
        }

        //GET: manage delivery date
        public ActionResult _ManageDelDate(int id, DateTime? delDate)
        {
            DeliveryDateBO model = new DeliveryDateBO();
            model.orderID = id;
            model.PrevDeliveryDate = delDate;
            return PartialView(model);
        }

        //GET: manage reassign orders to another drivers
        public ActionResult _ReassignOrders()
        {
            GetListModel model = new GetListModel();
            ViewBag.AssignedDrivers = _da.GetAllAssignedDrivers(model);
            return PartialView();
        }

        //GET: get _FieldChooser
        public ActionResult _FieldChooser()
        {
            //ViewBag.RunNoList = _da.GetCustomerRunNo();
            return PartialView();
        }

        //GET: Warehouse
        public ActionResult Warehouse()
        {
            var runs = _da.DeliveryRunsByWarehouse("Brisbane");
            var subs = _da.GetSuburbsByDeliveryRuns(runs[0]);
            ViewBag.Suburbs = subs;

            ViewBag.DeliveryRuns = runs;
            GetListModel model = new GetListModel();
            ViewBag.AssignedDrivers = _da.GetAllAssignedDrivers(model);
            ViewBag.AssignedOrders = _da.GetAllPendingOrders(-1, runs[0], "");
            ViewBag.UnassignedBySuburb = _da.GetAllPendingOrders(0, runs[0], subs[0]);
            return View();
        }

        //GET: Get all assigned orders
        public ActionResult _AssignedOrdersSuburbs(int id, string runno = "", string suburb = "")
        {
            ViewBag.AssignedOrders = _da.GetAllPendingOrders(id, runno, suburb);
            return PartialView();
        }

        public ActionResult _UnassignedBySuburbs(int id, string runno = "", string suburb = "")
        {
            ViewBag.UnassignedBySuburb = _da.GetAllPendingOrders(id, runno, suburb);
            return PartialView();
        }

        #endregion GET methods

        #region Get customer by search

        [HttpPost]
        public JsonResult GetAllCustomer(string id)
        {
            try
            {
                GetListModel model = new GetListModel { SearchText = id };
                var data = _da.GetAllCustomer(model);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion Get customer by search

        #region Assign order to driver

        [HttpPost]
        public JsonResult AssignOrderToDriver(int id, string orderIDs, string type)
        {
            try
            {
                var data = _da.AssignOrderToDriver(id, orderIDs, type);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion Assign order to driver

        #region Manage Order's Delivery date

        [HttpPost]
        public JsonResult ManageDelDate(DeliveryDateBO model)
        {
            try
            {
                var data = _da.ManageDelDate(model.orderID, model.DeliveryDate);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion Manage Order's Delivery date

        #region Get Delivery runs by warehosue

        [HttpPost]
        public JsonResult GetDeliveryRunByWarehouse(string warehouse)
        {
            try
            {
                var res = _da.DeliveryRunsByWarehouse(warehouse);
                var suburbs = _da.GetSuburbsByDeliveryRuns(res[0]);
                return Json(new
                {
                    Runs = res,
                    Subs = suburbs
                });
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion Get Delivery runs by warehosue

        #region Get Suburbs by delivery runs

        [HttpPost]
        public JsonResult GetSuburbsByDeliveryRuns(string run)
        {
            try
            {
                var suburbs = _da.GetSuburbsByDeliveryRuns(run);
                return Json(suburbs);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion Get Suburbs by delivery runs

        //GET: manage reassign orders to another drivers
        public ActionResult _ReassignWarehouseOrders()
        {
            GetListModel model = new GetListModel();
            ViewBag.AssignedDrivers = _da.GetAllAssignedDrivers(model);
            return PartialView();
        }
    }
}