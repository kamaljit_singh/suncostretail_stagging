﻿using CommonFunctions;
using DataModel.BO;
using DataModel.DA;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Saavi5Admin.Areas.Saavi5POD.Controllers
{
    public class HomeController : Controller
    {
        private readonly DriverDA _da;
        private readonly DeliveriesDA _dda;
        private readonly CompanyDA _cda;
        public HomeController()
        {
            _da = new DriverDA();
            _dda = new DeliveriesDA();
            _cda = new CompanyDA();
        }
        #region get method
        // GET: Saavi5POD/Home
        public ActionResult Dashboard()
        {
            //var model = new GetListModel();
            //model.PageNumber = 0;
            //model.PageSize = 50;
            //model.SearchText = "";
            //ViewBag.Driver = _da.GetallDriverComp();
            //ViewBag.Vehicle = _da.GetAllVehicles(model);
            ViewBag.GetAllOrderHasIssues = _dda.GetAllOrderByCustomerID(0,0,"i",null);
            return View();
        }

        [AllowAnonymous]
        public ActionResult _TopHeader()
        {
            return PartialView(GetLoginData());
        }
        //GET: drivers
        public ActionResult _Drivers(string id)
        {
            var model = new GetListModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.SearchText = id;
            ViewBag.Driver = _da.GetallDriverComp(model);
            return PartialView();
        }
        //GET: vehicles
        public ActionResult _Vehicles(string id)
        {
            var model = new GetListModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.SearchText = id;
            ViewBag.Vehicle = _da.GetVehiclesDeliveryStatus(model);
            return PartialView();
        }
        //GET: completion sidebar
        public ActionResult _CompletionSidebar()
        {
            DeliveriesDA dDA = new DeliveriesDA();
            ViewBag.Completionsidebar = dDA.GetCompletionSidebar();
            return PartialView();
        }
        //GET: Order delivery issue item list
        public ActionResult _DeliveryIssuesItem(int id)
        {
            ViewBag.DeliveryIssuesItem = _dda.GetDeliveryIssueItemList(id);
            return PartialView();
        }
        //GET: Driver's location 
        public ActionResult _DriversLiveLocation()
        { TrackingDA _tda = new TrackingDA();
            ViewBag.DriversLiveLocation = _tda.GetDriversLiveLocation();
            return PartialView();
        }
        //GET:My account
        public ActionResult MyAccount()
        {
            var userData = GetLoginData();
            var data = _cda.GetUserByID(userData.UserID);
            return View(data);
        }
        //GET:Change password
        public ActionResult _ChangePassword()
        {
            return PartialView(new ChangePasswordModel());
        }
        //GET:Login account
        public ActionResult _LoginAccount(string usertypeids)
        {
            ViewBag.Users = _cda.GetUsers(usertypeids);
            return PartialView();
        }
        //GET:Login accounts
        public ActionResult _LoginAccounts(long? id)
        {
            var SelectedUser = _cda.GetUserByID(id.To<long>());
            return PartialView(SelectedUser);
        }
        //GET: manage login account
        public ActionResult _ManageLoginAccount(long? id)
        {
            ViewBag.UserType = _cda.GetUserType();
            var SelectedUser = new UserBO();
            if (id > 0)
            {
                SelectedUser = _cda.GetUserByID(id.To<long>());
            }
            return PartialView(SelectedUser);
        }
        #endregion

        #region Get user data from the Authentication ticket
        /// <summary>
        /// Fetch user details that are stored in the auth ticket to be user in the application.
        /// </summary>
        /// <returns></returns>
        private AuthData GetLoginData()
        {
            FormsIdentity formsIdentity = User.Identity as FormsIdentity;            
            FormsAuthenticationTicket ticket = formsIdentity.Ticket;
            string userData = ticket.UserData;

            var data = JsonConvert.DeserializeObject<AuthData>(userData);
            return data;
        }
        #endregion
        #region Manage my account
        /// <summary>
        ///  Manage my account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageMyAccount(UserBO model)
        {
            try
            {
                var data = _cda.ManageMyAccount(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region Change password
        /// <summary>
        ///  Change password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var data = _cda.ChangePassword(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion
    }
}