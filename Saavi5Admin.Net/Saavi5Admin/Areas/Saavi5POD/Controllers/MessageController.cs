﻿using DataModel.BO;
using CommonFunctions;
using DataModel.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Saavi5Admin.Areas.Saavi5POD.Controllers
{
    public class MessageController : Controller
    {
        private readonly MessageDA _da;
        public MessageController()
        {
            _da = new MessageDA();
        }
        #region GET methods

        // GET: Saavi5POD/Message
        public ActionResult SendMessage()
        {
            DriverDA _daD = new DriverDA();
            var model = new GetListModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.SearchText = "";

            var data = _da.GetAllDrivers(model);
            ViewBag.Driver = data;
            return View();
        }
        //GET: manage groups
        public ActionResult _ManageGroups(long? id)
        {
            var model = new GetListModel();
            model.PageNumber = 0;
            model.PageSize = 0;
            model.SearchText = "";
            model.GroupID = id.To<long>();
            ViewBag.Driver = _da.GetAllDrivers(model);
            if (id > 0)
            {
                var data = _da.GetGroup(id.To<long>())[0];
                return PartialView(data);
            }
            else
            {
                return PartialView(new GetGroupCustomers());
            }
        }
        //GET: Send message
        public ActionResult _SendMessage()
        {
            return PartialView();
        }
        #endregion
        #region get all groups
        /// <summary>
        /// get all groups
        /// </summary>
        /// <returns></returns>
        public JsonResult GetGroup()
        {
            try
            {
                var data = _da.GetGroup(0);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }
        #endregion

        #region manage groups
        /// <summary>
        /// manage groups
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageGroups(GetGroupCustomers model)
        {
            try
            {
                var data = _da.ManageGroups(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion
        #region get group driver
        /// <summary>
        /// get group driver
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public JsonResult GetGroupCustomers(Int64 groupID, string type)
        {
            try
            {
                var data = _da.GetGroupUsers(groupID, type);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }
        #endregion

        #region Get all driver
        [HttpPost]
        /// <summary>
        ///  Get all driver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetAllDriver(string id)
        {
            try
            {
                GetListModel model = new GetListModel { SearchText = id, PageSize = 50 };
                var data = _da.GetAllDrivers(model);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }

        }
        #endregion

        #region send message
        /// <summary>
        /// send message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SendMessage(GetMessageModel model)
        {
            try
            {
                var data = _da.SendMessage(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion
    }
}