﻿using CommonFunctions;
using DataModel.BO;
using DataModel.DA;
using DataModel.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Saavi5Admin.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private readonly CompanyDA _da;
        private readonly OrderDA _daO;

        public CompanyController()
        {
            _da = new CompanyDA();
            _daO = new OrderDA();
        }

        #region GET: methods

        // GET: Company
        public ActionResult Dashboard()
        {
            GetOrderModel model = new GetOrderModel();
            model.SearchText = "";
            model.AscDesc = "";
            model.SortType = "";
            model.OrderType = "0";
            model.PageNumber = 0;
            model.PageSize = 50;

            ViewBag.Stats = _daO.GetOrderStats();
            ViewBag.Customers = _da.GetTotalCustomers();
            ViewBag.Orders = _daO.ViewOrders(model);
            return View();
        }

        //GET: Get latest added product
        public ActionResult _LatestAddedProduct(string id)
        {
            ProductDA _daP = new ProductDA();
            ViewBag.LatestAddedProduct = _daP.GetLatestAddedProduct(id);
            return PartialView();
        }

        // GET: Customers
        public ActionResult ManageUsers()
        {
            var model = new GetCustomersModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.IsRep = false;
            model.SearchText = "";

            ViewBag.Customers = _da.GetAllCustomers(model);

            return View();
        }

        //GET: customer detail
        public ActionResult _CustomerDetail(long? id)
        {
            ViewBag.ProductCategory = _da.GetProductCategory();

            if (id > 0)
            {
                // ViewBag.CustomerChildlist = _da.GetCustomerChildlist(id.To<long>());
                var customerDetails = _da.GetCustomerByID(id.To<long>());
                return PartialView(customerDetails);
            }
            else
            {
                return PartialView(new CustomerBO());
            }
        }

        //GET: new request
        public ActionResult NewRequests()
        {
            var model = new GetCustomersModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.IsRep = false;
            model.SearchText = "";

            ViewBag.Customers = _da.GetAllCustomers(model);
            return View();
        }

        //GET: manage customer
        public ActionResult _ManageCustomer(long? id)
        {
            if (id > 0)
            {
                var customerDetails = _da.GetCustomerForEdit(id.To<long>());

                return PartialView(customerDetails);
            }
            else
            {
                return PartialView(new AddEditCustomerBO());
            }
        }

        //GET: manage features
        public ActionResult _ManageFeatures()
        {
            return PartialView();
        }

        //GET: add user
        public ActionResult _AddUser(long? id)
        {
            UserMaster obj = new UserMaster { CustomerID = id };
            return PartialView(obj);
        }

        //GET: delete user
        public ActionResult _DeleteUser(long? id)
        {
            UserMaster obj = new UserMaster { CustomerID = id };
            return PartialView(obj);
        }

        //GET: Pantry list
        public ActionResult _RenamePantryName(long? id)
        {
            //var data = _da.GetPantryList(Convert.ToInt64(id));
            return PartialView();
        }

        // GET: Rep Customers
        public ActionResult ManageRepUser()
        {
            var model = new GetCustomersModel();
            model.PageNumber = 0;
            model.PageSize = 0;
            model.IsRep = true;
            model.SearchText = "";
            ViewBag.IsRepView = true;
            ViewBag.Customers = _da.GetAllCustomers(model);
            return View();
        }

        //GET: rep customer detail
        public ActionResult _RepUserDetail(long? id)
        {
            if (id > 0)
            {
                var customerDetails = _da.GetCustomerByID(id.To<long>());
                return PartialView(customerDetails);
            }
            else
            {
                return PartialView(new CustomerBO());
            }
        }

        //GET: manage customer
        public ActionResult _ManageRepUser(long? id)
        {
            if (id > 0)
            {
                var customerDetails = _da.GetCustomerForEdit(id.To<long>());

                return PartialView(customerDetails);
            }
            else
            {
                return PartialView(new AddEditCustomerBO());
            }
        }

        //GET:My account
        public ActionResult MyAccount()
        {
            var userData = GetLoginData();
            var data = _da.GetUserByID(userData.UserID);
            return View(data);
        }

        //GET:Change password
        public ActionResult _ChangePassword()
        {
            return PartialView(new ChangePasswordModel());
        }

        //GET:Login account
        public ActionResult _LoginAccount(string usertypeids)
        {
            ViewBag.Users = _da.GetUsers(usertypeids);
            return PartialView();
        }

        //GET:Login accounts
        public ActionResult _LoginAccounts(long? id)
        {
            var SelectedUser = _da.GetUserByID(id.To<long>());
            return PartialView(SelectedUser);
        }

        //GET: manage login account
        public ActionResult _ManageLoginAccount(long? id)
        {
            ViewBag.UserType = _da.GetUserType();
            var SelectedUser = new UserBO();
            if (id > 0)
            {
                SelectedUser = _da.GetUserByID(id.To<long>());
            }
            return PartialView(SelectedUser);
        }

        #endregion GET: methods
        #region Get user data from the Authentication ticket

        /// <summary>
        /// Fetch user details that are stored in the auth ticket to be user in the application.
        /// </summary>
        /// <returns></returns>
        private AuthData GetLoginData()
        {
            FormsIdentity formsIdentity = User.Identity as FormsIdentity;

            FormsAuthenticationTicket ticket = formsIdentity.Ticket;
            string userData = ticket.UserData;

            var data = JsonConvert.DeserializeObject<AuthData>(userData);
            return data;
        }

        #endregion Get user data from the Authentication ticket
        #region

        /// <summary>
        /// Get customer paging wise
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetCustomersWithPaging(GetCustomersModel model)
        {
            try
            {
                var customers = _da.GetAllCustomers(model);

                return Json(new { Message = "Success", Result = customers });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// Manage customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ManageCustomer(AddEditCustomerBO model)
        {
            try
            {
                var data = _da.ManageCustomer(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteCustomer(long id)
        {
            try
            {
                var data = _da.DeleteCustomer(id);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// Change status of Customer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SetCustActive(long id, bool status)
        {
            try
            {
                var data = _da.SetCustActive(id, status);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion
        #region

        /// <summary>
        /// Get new request
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetNewRequests()
        {
            try
            {
                var reqs = _da.GetNewRequests();
                return Json(new { Message = "Success", Result = reqs });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        //#region
        ///// <summary>
        ///// Delete request
        ///// </summary>
        ///// <param name="userID"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public JsonResult delRequest(long userID)
        //{
        //    try
        //    {
        //        var reqs = _da.DeleteRequest(userID);
        //        return Json(reqs);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(ex.Message);
        //    }

        //}
        //#endregion

        #region

        /// <summary>
        /// Link user to customer
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult linkUserToCustomer(long userID, long customerID)
        {
            try
            {
                var lnkUser = _da.linkUserToCustomer(userID, customerID);
                return Json(lnkUser);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get salesmancode
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSalesmanCode()
        {
            try
            {
                var data = _da.GetSalesmanCode();
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get All countries
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllCountry()
        {
            try
            {
                var country = _da.GetAllCountry();

                return Json(new { Message = "Success", Result = country });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get States based on countryID
        /// </summary>
        /// <param name="cID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllStateByCountryID(int cID)
        {
            try
            {
                var state = _da.GetAllStateByCountryID(cID);

                return Json(new { Message = "Success", Result = state });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        ///  Get user by customer id
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUserByCustomerID(long customerID)
        {
            try
            {
                var user = _da.GetUserByCustomerID(customerID);
                return Json(new { Message = "Success", Result = user });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get feature for user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetFeaturesForUser(long userID)
        {
            try
            {
                var features = _da.GetFeatures(userID);
                return Json(new { Message = "Success", Result = features });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        ///
        /// </summary>
        /// <param name="column"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult UpdateFeatureForUser(string column, string value, long userID)
        {
            string result = _da.UpdateFeatureForUser(column, value == "False" ? "0" : "1", userID);

            return Json(result);
        }

        #endregion

        #region

        /// <summary>
        /// Add User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ManageUser(UserMaster model)
        {
            try
            {
                var data = _da.ManageUser(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteUser(long userID)
        {
            try
            {
                var data = _da.DeleteUser(userID);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// get pantry list by customer id
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public JsonResult GetPantryList(long customerID)
        {
            try
            {
                var data = _da.GetPantryList(customerID);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage pantry list name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManagePantryList(PantryListMaster model)
        {
            try
            {
                var data = _da.ManagePantryList(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region manage tag name for customer

        public JsonResult ManageCustomerTags(CustomerBO obj)
        {
            try
            {
                var data = _da.ManageCustomerTags(obj);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region Manage my account

        /// <summary>
        ///  Manage my account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageMyAccount(UserBO model)
        {
            try
            {
                var data = _da.ManageMyAccount(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region Change password

        /// <summary>
        ///  Change password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var data = _da.ChangePassword(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region Get customer Parent/Child List

        /// <summary>
        /// Get customer Parent/Child List
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetParentChildList(ParentChildModel model)
        {
            try
            {
                var li = (object)null;
                //if (model.IsParent) {
                li = _da.GetChildByParentID(model);
                //}
                // else {
                //li= _da.GetParentByChildID(model);
                //  }
                return Json(new { Message = "Success", Result = li });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region GetParentInfo

        [HttpPost]
        public JsonResult GetParentInfo(long ParentId)
        {
            try
            {
                var li = (object)null;

                li = _da.GetParentInfo(ParentId);

                return Json(new { Message = "Success", Result = li });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region GetTopCustomers

        [HttpPost]
        public JsonResult GetTopCustomers(GetTopCustomerModel model)
        {
            try
            {
                var isSales = false;
                if (model.StatsType == "S")
                {
                    isSales = true;
                }
                var data = _daO.GetTopPerformingCustomers(model.n, isSales);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region GetSalesDistribution

        [HttpPost]
        public JsonResult GetSalesDistribution(GetStatsModel model)
        {
            try
            {
                var thisWeek = false;
                if (model.StatsType == "T")
                {
                    thisWeek = true;
                }
                var data = _daO.GetSalesDistribution(thisWeek);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region GetTopProducts

        [HttpPost]
        public JsonResult GetTopProducts(GetStatsModel model)
        {
            try
            {
                var isSales = false;
                if (model.StatsType == "S")
                {
                    isSales = true;
                }
                var data = _daO.GetTopPerformingProducts(isSales);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region GetTopCategories

        [HttpPost]
        public JsonResult GetTopCategories(GetStatsModel model)
        {
            try
            {
                var isSales = false;
                if (model.StatsType == "S")
                {
                    isSales = true;
                }
                var data = _daO.GetTopPerformingCategories(isSales);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region Edit user partial view

        //GET: add user
        public ActionResult _EditUser(long? id)
        {
            UserMaster obj = new UserMaster { CustomerID = id };
            obj = _da.GetUserByCustomer(id.To<long>());
            obj.ABN = "Edit";
            obj.DefaultPassword = "";
            return PartialView(obj);
        }

        #endregion
    }
}