﻿using CommonFunctions;
using DataModel.BO;
using DataModel.DA;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Saavi5Admin.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private readonly MessageDA _da;
        public MessageController()
        {
            _da = new MessageDA();
        }
        #region GET: methods
        // GET: Message
        public ActionResult SendMessage(string id)
        {
            if (id !="" && id!=null)
            {
                ViewData["Message"] = id;

            }
            var model = new GetCustomersModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.IsRep = false;
            model.SearchText = "";

            ViewBag.Customers = _da.GetAllCustomers(model);

            return View();
        }

        //GET: manage groups
        public ActionResult _ManageGroups(long? id)
        {
            if (id > 0)
            {
                var data = _da.GetGroup(id.To<long>())[0];
                return PartialView(data);
            }
            else
            {
                return PartialView(new GetGroupCustomers());
            }
        }
        //GET: Send message
        public ActionResult _SendMessage()
        {
            return PartialView();
        }
        //GET: get pdf file
        public ActionResult _UploadPDF(long id, string type)
        {
            var pdf = _da.GetPdfFile(id, type);
            return PartialView(pdf);
        }
        #endregion

        #region
        /// <summary>
        /// Get customer paging wise
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetCustomersWithPaging(GetCustomersModel model)
        {
            try
            {
                var customers = _da.GetAllCustomers(model);

                return Json(new { Message = "Success", Result = customers });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }
        #endregion

        #region 
        /// <summary>
        /// get all groups
        /// </summary>
        /// <returns></returns>
        public JsonResult GetGroup()
        {
            try
            {
                var data = _da.GetGroup(0);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }
        #endregion

        #region get group customer
        /// <summary>
        /// get group customer
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public JsonResult GetGroupCustomers(Int64 groupID, string type)
        {
            try
            {
                var data = _da.GetGroupCustomers(groupID, type);

                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }
        #endregion

        #region manage groups
        /// <summary>
        /// manage groups
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageGroups(GetGroupCustomers model)
        {
            try
            {
                var data = _da.ManageGroups(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region 
        /// <summary>
        /// delete group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteGroup(long id)
        {
            try
            {
                var data = _da.DeleteGroup(id);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region send message
        /// <summary>
        /// send message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SendMessage(GetMessageModel model)
        {
            try
            {
                var data = _da.SendMessage(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region 
        /// <summary>
        /// add/update pdf file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult _UploadPDF(GetPdfFileModel model)
        {
            try
            {
                HttpPostedFileBase file = Request.Files["fuPDF"];
                if (file.FileName != null && file.FileName != "")
                {
                    var filename = file.FileName;
                    model.PDF_Name = model.PDF_Name != null ? model.PDF_Name : filename;
                    if (model.GroupID == 0 && model.CustomerID == 0)
                    {
                        filename = ConfigurationManager.AppSettings["WEBSITE_NAME"] + " PDF.pdf";
                    }
                    file.SaveAs(Path.Combine(Server.MapPath("~/Content/Uploads/PDF/"), filename));
                    model.PDF = filename;
                }
                var data = _da.ManagePdfFile(model);
                ViewData["Message"] = data;

                return RedirectToAction("SendMessage",new { id=data});//PartialView(model);
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message; 
                return RedirectToAction("SendMessage", new { id = ex.Message });
            }
        }
        #endregion

        #region 
        /// <summary>
        /// get all non group products
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public JsonResult GetAllNonProducts(GetNonProductsModel model)//string SearchText, long groupID)
        {
            try
            {
                var data = _da.GetAllNonProducts(model);// SearchText, groupID);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region 
        /// <summary>
        /// get all group products
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public JsonResult GetAllGroupProducts(long groupID)
        {
            try
            {
                var data = _da.GetAllGroupProducts(groupID);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region 
        /// <summary>
        /// add/remove group products
        /// </summary>
        /// <param name="productArray"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public JsonResult ManageGroupProducts(string productArray, long groupID)
        {
            try
            {
                var data = _da.ManageGroupProducts(productArray, groupID);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

     public JsonResult GetModelDeliveryRunList(Int64 groupID, string type)
        {
            try
            {
                var data = _da.GetAllDeliveryRun(groupID,type);

                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
    }
}