﻿using DataModel.BO;
using DataModel.DA;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Saavi5Admin.Controllers
{
    [Authorize(Roles = "CompanyAdmin,SuperAdmin")]
    public class ProductController : Controller
    {
        private readonly ProductDA _da;

        public ProductController()
        {
            _da = new ProductDA();
        }

        #region GET: methods

        //GET: get all categories with sub-categories
        public ActionResult ManageCategories()
        {
            //var model = new GetCustomersModel();
            //model.PageNumber = 0;
            //model.PageSize = 50;
            //model.
            string SearchText = "";

            ViewBag.Catogories = _da.GetAllCatogories(SearchText);
            return View();
        }

        //GET: get main categories
        public ActionResult _ManageCategory(long id)
        {
            if (id > 0)
            {
                var cat = _da.getMainCategoryByID(id);

                return PartialView(cat);
            }
            else
            {
                return PartialView(new MainCategoriesMaster());
            }
        }

        //GET: get sub-categories
        public ActionResult _ManageSubCategory(long id)
        {
            if (id > 0)
            {
                var subCat = _da.getSubCategoryByID(id);
                return PartialView(subCat);
            }
            else
            { return PartialView(new ProductCategoriesMaster()); }
        }

        //GET: product
        public ActionResult Products()
        {
            return View();
        }

        //GET: get product
        public ActionResult ManageProduct(long? id)
        {
            if (id > 0)
            {
                var Product = _da.GetProductByID(Convert.ToInt64(id));
                return View(Product);
            }
            else
            {
                Random r = new Random();
                int num = r.Next(1000, 9999);
                string pCode = num.ToString();
                return View(new GetAllProductBO() { ProductCode = pCode });
            }
        }

        //GET: get suggestive product
        public ActionResult SuggestiveProducts()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        //GET: get suggestive product
        public ActionResult LatestProducts()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion GET: methods

        #region

        /// <summary>
        /// search categories
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        public JsonResult searchCategories(string SearchText)
        {
            try
            {
                var data = _da.GetAllCatogories(SearchText);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get product by category id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult getProductByCatID(GetProductsModel model)
        {
            try
            {
                var data = _da.GetAllProductsByCatID(model);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get product by id
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetProductByID(long productID)
        {
            try
            {
                var data = _da.GetProductByID(productID);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// add/update main category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageCategory(MainCategoriesMaster model)
        {
            try
            {
                var cat = _da.ManageCategory(model);
                return Json(cat);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// Delete main category
        /// </summary>
        /// <param name="catID"></param>
        /// <returns></returns>
        public JsonResult DeleteCategory(long catID)
        {
            try
            {
                var data = _da.DeleteCategory(catID);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// add/update sub-category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageSubCategory(ProductCategoriesMaster model)
        {
            try
            {
                var data = _da.ManageSubCategory(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// delete sub-category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteSubCategory(long scatID)
        {
            try
            {
                var data = _da.DeleteSubCategory(scatID);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// get all sub-category
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult getAllSubCategory()
        {
            try
            {
                var subCat = _da.getAllSubCategory();
                return Json(new { Message = "Success", Result = subCat });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion
        #region

        /// <summary>
        /// get all Unit of measurement
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult getAllUnitofMeasurement()
        {
            try
            {
                var unit = _da.getAllUnitofMeasurement();
                return Json(new { Message = "Success", Result = unit });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion
        #region

        /// <summary>
        /// get all product filter
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult getAllProductFilter()
        {
            try
            {
                var filter = _da.getAllProductFilter();
                return Json(new { Message = "Success", Result = filter });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion
        #region

        /// <summary>
        /// add/update product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ManageProduct(GetAllProductBO model)
        {
            try
            {
                List<FileList> li = new List<FileList>();
                if (Request.Files["fuimage"].FileName != "")
                {
                    li.Add(new FileList { File = Request.Files["fuimage"], FileID = "file1" });
                }
                if (Request.Files["fuimage2"].FileName != "")
                {
                    li.Add(new FileList { File = Request.Files["fuimage2"], FileID = "file2" });
                }
                if (Request.Files["fuimage3"].FileName != "")
                {
                    li.Add(new FileList { File = Request.Files["fuimage3"], FileID = "file3" });
                }
                if (Request.Files["fuimage4"].FileName != "")
                {
                    li.Add(new FileList { File = Request.Files["fuimage4"], FileID = "file4" });
                }
                model.FileList = li;
                var data = _da.AddUpdateProduct(model);
                ViewData["Message"] = data;
                if (data == "Success")
                {
                    return RedirectToAction("Products", "Product");
                }
                else
                {
                    return View(new GetAllProductBO());
                }
                //else {
                // return View(model);
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                return View(model);
            }
        }

        #endregion

        #region

        /// <summary>
        /// delete product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteProduct(long id)
        {
            try
            {
                var data = _da.DeleteProduct(id);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage product filter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageProductFilter(ProductFiltersMaster model)
        {
            try
            {
                var data = _da.ManageProductFilter(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// delete product filter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteProductFilter(long id)
        {
            try
            {
                var data = _da.DeleteProductFilter(id);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage unit of measurements
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageUnitofMeasurements(UnitOfMeasurementMaster model)
        {
            try
            {
                var data = _da.ManageUnitofMeasurements(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// delete unit of measurement
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteUnitofMeasurement(long id)
        {
            try
            {
                var data = _da.DeleteUnitofMeasurement(id);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get all products
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllProducts(GetNonProductsModel model)
        {
            try
            {
                List<GetAllProductBO> product = null;
                if (model.Type == "s")
                {
                    product = _da.GetAllNonSuggestiveProducts(model);
                }
                else
                {
                    product = _da.GetAllNonLatestProducts(model);
                }
                return Json(product);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// get all suggestive products
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllNonProducts(string type)
        {
            try
            {
                if (type == "s")
                {
                    var product = _da.GetAllSuggestiveProducts();
                    return Json(product);
                }
                else
                {
                    var product = _da.GetAllLatestProducts();
                    return Json(product);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// add/remove suggestive product
        /// </summary>
        /// <param name="productArray"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ManageNonProduct(string productArray, string type)
        {
            try
            {
                if (type == "s")
                {
                    var data = _da.ManageSuggestiveProduct(productArray);
                    return Json(data);
                }
                else
                {
                    var data = _da.ManageLatestProduct(productArray);
                    return Json(data);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// Change status of Product
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SetProductActive(long id, bool status)
        {
            try
            {
                var data = _da.SetProductActive(id, status);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        //delete images from product
        #region

        /// <summary>
        /// delete product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteProductImage(long id)
        {
            try
            {
                var data = _da.DeleteProductImage(id);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion
    }
}