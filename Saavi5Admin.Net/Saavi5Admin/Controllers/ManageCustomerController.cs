﻿using CommonFunctions;
using DataModel.BO;
using DataModel.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Saavi5Admin.Controllers
{   
    [Authorize]
    public class ManageCustomerController : Controller
    {
        private readonly CompanyDA _da;


        public ManageCustomerController()
        {
            _da = new CompanyDA();

        }
        // GET: ManageCustomer
        public ActionResult Index()
        {
            var model = new GetCustomersModel();
            model.PageNumber = 0;
            model.PageSize = 50;
            model.IsRep = false;
            model.SearchText = "";

            ViewBag.Customers = _da.GetAllCustomersByEmail(model);

            return View();
        }


        [HttpPost]
        public JsonResult GetCustomersWithPaging(GetCustomersModel model)
        {
            try
            {
                var customers = _da.GetAllCustomersByEmail(model);

                return Json(new { Message = "Success", Result = customers });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }


        //GET: customer detail
        public ActionResult _CustomerDetail(long? id)
        {
            ViewBag.ProductCategory = _da.GetProductCategory();

            if (id > 0)
            {
                // ViewBag.CustomerChildlist = _da.GetCustomerChildlist(id.To<long>());
                var customerDetails = _da.GetMangeCustomerByID(id.To<long>());
                return PartialView(customerDetails);
            }
            else
            {

                return PartialView(new CustomerBO());
            }
        }

        #region manage customer Details
        public JsonResult ManageCustomer(CustomerBO obj)
        {
            try
            {
                var data = _da.ManageCustomer(obj);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion
    }
}