﻿using DataModel.BO;
using DataModel.DA;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Saavi5Admin.Controllers
{
    [Authorize(Roles = "CompanyAdmin,SuperAdmin")]
    public class PaymentController : Controller
    {
        private readonly PaymentDA _da;


        public PaymentController()
        {
            _da = new PaymentDA();

        }

        #region Connect Stripe Account
        /// <summary>
        /// Connect Stripe Account
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ConnectAccount(string code, string client_id = "")
        {
            if (code.Length > 5 && ConfigurationManager.AppSettings["StripeConnectedAccountID"] == "")
            {
                var res = _da.ConnectAccount(code, client_id);
                ViewBag.IsConnected = res.Connected;
                ViewBag.ConnectedAccountId = res.AccountID;
                ConfigurationManager.AppSettings["StripeConnectedAccountID"] = res.AccountID;
                ViewBag.Error = res.Error;
            }
            else
            {
                ViewBag.IsConnected = false;
                ViewBag.Error = "Invalid request.";
            }
            return PartialView();
        }
        #endregion Connect Stripe Account

        #region Capture Payment
        /// <summary>
        /// Capture Payment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult _CapturePayment(long id, double orderTotal, double? authorizedAmt, long? UserID)
        {
            PaymentBO model = new PaymentBO();
            model.CartID = id;
            model.CustomerID = 0;
            model.UserID = UserID;
            var paymentDetail = _da.GetPaymentDetail(id);
            if (!string.IsNullOrEmpty(paymentDetail))
            {
                model.PaymentReference = paymentDetail;
            }
            ViewBag.OrderTotal = orderTotal;
            ViewBag.CapturePrice = authorizedAmt;
            return PartialView(model);
        }
        #endregion

        #region 
        /// <summary>
        /// Capture Payment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CapturePayment(PaymentBO model)
        {
            try
            {
                var result = _da.CapturePayment(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region 
        /// <summary>
        /// Refund Payment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult _RefundPayment(long id, double orderTotal, double? authorizedAmt, long? UserID,long? CustomerID, int Status, int Click)
        {
            RefundBO model = new RefundBO();
            model.CartID = id;
            model.CustomerID = CustomerID!=null?Convert.ToInt64(CustomerID): 0;
            if(CustomerID!=null)
            {
                var customer= _da.GetCustomerDetail(id,CustomerID);
                if(customer!=null && customer.Email!=null)
                {
                    model.CustomerEmail = customer.Email;
                    model.CustomerName = customer.CustomerName;
                }
            }
            model.UserID = UserID;
            ViewBag.OrderTotal = orderTotal;
            ViewBag.CapturePrice = authorizedAmt;
            ViewBag.RefundAmount = 0.00;
            ViewBag.RefundStatus = Status;
            model.TotalCaptureAmount = orderTotal;
            model.PaymentClick = Click;
            model.ShowAmount = "";
            if (Click == 2)
            {
                model.Amount = authorizedAmt != null ? Math.Round(Convert.ToDouble(authorizedAmt), 2) : 0;
                model.ShowAmount= authorizedAmt != null ? String.Format("{0:$#,##0.00;($#,##0.00)}", Math.Round(Convert.ToDouble(authorizedAmt), 2)) : "";
            }
            if (Status == 1)
            {
                var striperefunddetail = _da.GetRefundDetail(id);
                if (striperefunddetail != null && striperefunddetail.LogID != 0 && !string.IsNullOrEmpty(Convert.ToString(striperefunddetail.LogID)))
                {
                    ViewBag.RefundAmount = striperefunddetail.RefundAmount;
                    ViewBag.OrderTotal = striperefunddetail.CaptureAmount;
                    model.RefundCreatedOn = striperefunddetail.CreatedOn.Value.ToString("dd/MM/yyyy");
                    model.RefundCreatedTime = striperefunddetail.CreatedOn.Value.ToString("hh:mm tt");
                    model.RefundReference = striperefunddetail.RefundId;
                }
                else
                {
                    ViewBag.OrderTotal = !string.IsNullOrEmpty(Convert.ToString(orderTotal)) ? 0.00 : orderTotal;
                    ViewBag.RefundAmount = 0.00;
                    ViewBag.CapturePrice = !string.IsNullOrEmpty(Convert.ToString(authorizedAmt)) ? 0.00 : authorizedAmt;
                }
            }
            var stripedetail = _da.GetRefundPaymentDetail(id);
            if (stripedetail != null && stripedetail.ID != 0 && !string.IsNullOrEmpty(Convert.ToString(stripedetail.ID)))
            {
                model.PaymentReference = stripedetail.StripeChargeID;
                model.Netpayment = stripedetail.NetAmount;
                model.TotalFees = stripedetail.TotalFees;
                model.Tax = stripedetail.Tax;
                model.ApplicationFees = stripedetail.ApplicationFees;
                model.StripeProcessingFee = stripedetail.StripeProcessingFee;
                model.CreatedOn = stripedetail.CreatedOn.Value.ToString("dd/MM/yyyy");
                model.CreatedTime = stripedetail.CreatedOn.Value.ToString("hh:mm tt");
                model.RefundTax = stripedetail.RefundTax;
                model.RefundStripeProcessingFee = stripedetail.RefundStripeProcessingFee;
                model.TotalCaptureAmount = stripedetail.TotalCaptureAmount;
            }
            else
            {
                model.Netpayment = 0.00;
                model.TotalFees = 0.00;
                model.Tax = 0.00;
                model.ApplicationFees = 0.00;
                model.StripeProcessingFee = 0.00;
                model.RefundTax = 0.00;
                model.RefundStripeProcessingFee = 0.00;
                model.TotalCaptureAmount = 0.00;
            }
            return PartialView(model);
        }

        /// <summary>
        /// Refund Payment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult _RefundPaymentHistory(long id)
        {
            var model = new OrderRefundHistory();
            var res = _da.GetOrderDetails(id);
            var paymentRefundDetail = _da.GetRefundDetail(id);
            ViewBag.OrderTotal = 0;
            model.CartID = id;
            ViewBag.RefundAmount = 0;
            model.RevisedFreightCharge = 0;
            if (res.Count > 0)
            {
                model.CustomerEmail = res[0].CustomerEmail;
                model.CustomerName = res[0].CustomerName;
                model.OrderDetails = res;            
                ViewBag.OrderTotal = res.Sum(s => s.RevisedPrice * s.RevisedQuantity);
                model.OrderTotal = res.Sum(s => s.RevisedPrice * s.RevisedQuantity);
                model.OrderTotal = model.OrderTotal + res[0].RevisedFreightCharge - res[0].CouponAmount;
                ViewBag.OrderTotal = model.OrderTotal;
                model.IsHasFreightCharges = res[0].IsHasFreightCharges;
                model.RevisedFreightCharge = res[0].RevisedFreightCharge;
                model.OriginalFreightCharge = res[0].OriginalFreightCharge; 
                model.CouponAmount = res[0].CouponAmount;
                model.TotalFreightDifference = res[0].OriginalFreightCharge - res[0].RevisedFreightCharge;
            }

            var stripedetail = _da.GetRefundPaymentDetail(id);
            if (stripedetail != null && stripedetail.ID != 0 && !string.IsNullOrEmpty(Convert.ToString(stripedetail.ID)))
            {  
                model.PaymentReference = stripedetail.StripeChargeID;
                model.CreatedOn = stripedetail.CreatedOn.Value.ToString("dd/MM/yyyy");
                model.CreatedTime = stripedetail.CreatedOn.Value.ToString("hh:mm tt");
            }
            else
            {              
                ViewBag.RefundAmount = 0.00;
            }
            var striperefunddetail = _da.GetRefundDetail(id);
            if (striperefunddetail != null && striperefunddetail.LogID != 0 && !string.IsNullOrEmpty(Convert.ToString(striperefunddetail.LogID)))
            {
                ViewBag.RefundAmount = striperefunddetail.RefundAmount;              
                model.RefundCreatedOn = striperefunddetail.CreatedOn.Value.ToString("dd/MM/yyyy");
                model.RefundCreatedTime = striperefunddetail.CreatedOn.Value.ToString("hh:mm tt");
                model.RefundReference = striperefunddetail.RefundId;
            }
            else
            {
               ViewBag.RefundAmount = 0.00;                
            }

            return PartialView(model);
        }

        /// <summary>
        /// Refund Payment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult _PaymentOrderHistory(long id)
        {
            var model = new OrderPaymentHistory();
            var res = _da.GetOrderHistoryDetails(id);           
            ViewBag.OrderTotal = 0;
            model.CartID = id;
            if(id>0)
            {
                var customer = _da.GetCustomerDetail(id,0);
                if (customer != null && customer.Email != null)
                {
                    model.CustomerEmail = customer.Email;
                    model.CustomerName = customer.CustomerName;
                }
            }
            if (res.Count > 0)
            {     
                if(res.Count==1)
                {
                    model.FirstOrderDetails = res;
                }
                else if(res.Count > 1)
                {
                    model.FirstOrderDetails = res.Take(1).ToList();
                    model.OrderDetails = res.Skip(1).ToList();
                }
                
                
                ViewBag.OrderTotal = res[0].TotatRevisedPrice;
                model.OrderTotal = res[0].TotatRevisedPrice;                                         
                model.CouponAmount = res[0].CouponAmount;
               
            }

            return PartialView(model);
        }
        #endregion
        #region 
        /// <summary>
        /// Capture Payment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult StripeRefundPayment(RefundBO model)
        {
            try
            {

                var result = _da.StripeRefundPayment(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

    }
}