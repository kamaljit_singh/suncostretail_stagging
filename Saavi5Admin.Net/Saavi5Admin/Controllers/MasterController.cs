﻿using DataModel.BO;
using DataModel.DA;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Saavi5Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class MasterController : Controller
    {
        private readonly AccountDA _da;
        public MasterController()
        {
            _da = new AccountDA();
        }

        #region Get user data from the Authentication ticket
        /// <summary>
        /// Fetch user details that are stored in the auth ticket to be user in the application.
        /// </summary>
        /// <returns></returns>
        private AuthData GetLoginData()
        {
            FormsIdentity formsIdentity = User.Identity as FormsIdentity;

            FormsAuthenticationTicket ticket = formsIdentity.Ticket;
            string userData = ticket.UserData;

            var data = JsonConvert.DeserializeObject<AuthData>(userData);
            return data;
        }
        #endregion

        #region
        /// <summary>
        ///  Common Top Header
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult _TopHeader()
        {
            //FormsIdentity formsIdentity = User.Identity as FormsIdentity;
            //if (formsIdentity != null)
            //{
            return PartialView(GetLoginData());
            //}
            //else {return RedirectToAction("Login", "Account"); }
        }
        #endregion

        #region 
        /// <summary>
        /// Fetch Admin and Main features for a user.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManagedFeatures()
        {
            var features = _da.GetAdminAndMainFeatures();

            ViewBag.AdminFeatures = features.AdminFeatures;
            ViewBag.MainFeatures = features.MainFeatures;

            return View();
        }
        #endregion

        #region 
        /// <summary>
        /// Update Admin or Main features for a customer.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult UpdateAdminOrMainFeature(string column, string value, string type)
        {
            string result = _da.UpdateAdminOrMainFeature(column, value == "False" ? "0" : "1", type == "admin" ? "adminfeaturesmaster" : "mainfeaturesmaster");

            return Json(result);
        }
        #endregion       
    }
}