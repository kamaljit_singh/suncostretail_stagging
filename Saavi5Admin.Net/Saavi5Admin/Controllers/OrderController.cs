﻿using CommonFunctions;
using DataModel.BO;
using DataModel.DA;
using DataModel.Model;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;

//using System.Net.Http;
using System.Web;

//using System.Web.Http;
using System.Web.Mvc;

namespace Saavi5Admin.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly OrderDA _da;

        public OrderController()
        {
            _da = new OrderDA();
        }

        #region GET: methods

        //GET: Settings
        public ActionResult Settings()
        {
            return View();
        }

        //GET: view Order
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ViewOrders()
        {
            GetOrderModel model = new GetOrderModel();
            model.SearchText = "";
            model.AscDesc = "";
            model.SortType = "";
            model.OrderType = "0";
            model.PageNumber = 0;
            model.PageSize = 50;

            ViewBag.Orders = _da.ViewOrders(model);
            return View();
        }

        //GET: offline
        public ActionResult Offline()
        {
            return View();
        }

        //GET: Delivery charges
        public ActionResult _DeliveryCharges()
        {
            var data = _da.GetDeliveryCharges();
            return PartialView(data);
        }

        //GET: Set App Discount
        public ActionResult _SetAppDiscount()
        {
            var data = _da.GetAppDiscount();
            return PartialView(data);
        }

        //GET: Select Currency
        public ActionResult _SelectCurrency()
        {
            var data = _da.GetSelectedCurrency();
            return PartialView(data);
        }

        #endregion GET: methods

        #region

        public JsonResult GetAllOrders(GetOrderModel model)
        {
            try
            {
                var data = _da.ViewOrders(model);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region GetOrderStats

        public JsonResult GetOrderStats(GetStatsModel model)
        {
            try
            {
                var isSales = false;
                if (model.StatsType == "S")
                {
                    isSales = true;
                }
                var data = _da.GetKeyOrderStats(isSales);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// get permitted days
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPermittedDays()
        {
            try
            {
                var data = _da.GetPermittedDays("");
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// check for permitted days, is it conflicted or not
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult CheckPermittedDays(GetPermittedDaysModel model)
        {
            try
            {
                var data = _da.CheckPermittedDays(model);
                return Json(new
                {
                    Message = "Success",
                    Result = data
                });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// get notice detail
        /// </summary>
        /// <returns></returns>
        public JsonResult GetNotice()
        {
            try
            {
                var data = _da.GetNotice();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage notice
        /// </summary>
        /// <param name="li"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageNotice(List<PermittedDaysMaster> li, GetPermittedDaysModel model)
        {
            try
            {
                var data = _da.ManageNotice(li, model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// get auto order detail
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAutoOrder()
        {
            try
            {
                var data = _da.GetAutoOrder();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage auto order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageAutoOrder(AutoOrderMaster model)
        {
            try
            {
                var data = _da.ManageAutoOrder(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// save notice mesaage
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveNotice(GetNoticeBO model)
        {
            try
            {
                var data = _da.saveNotice(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// get popup message
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPopupMessage()
        {
            try
            {
                var data = _da.GetPopupMessage();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage popup message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManagePopupMessage(PopupMaster model)
        {
            try
            {
                var data = _da.ManagePopupMessage(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region Upload customer suburbs and postcodes

        /// <summary>
        /// manage customer days
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageCustomerDays()
        {
            try
            {
                HttpPostedFileBase file = Request.Files["fuUploadDeliveryDays"];

                if (file.ContentLength > 0)
                {
                    DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/Content/Uploads/TempFiles/"));
                    foreach (FileInfo f in di.GetFiles())
                    {
                        f.Delete();
                    }
                    file.SaveAs(Server.MapPath("~/Content/Uploads/TempFiles/") + file.FileName);

                    DataTable dt = _da.GetDataTableFromExcel(Server.MapPath("~/Content/Uploads/TempFiles/") + file.FileName, "ALL");
                    string res = "";
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        res = _da.InsertDataIntoSQLServerUsingSQLBulkCopy(dt, "Postcode");
                    }

                    ViewData["Message"] = res;
                    //return Json(data);
                    return RedirectToAction("Settings", "Order");
                }
                ViewData["Message"] = "Success";
                //return Json("Success");
                return RedirectToAction("Settings", "Order");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public JsonResult UploadCustomerToOrder()
        {
            try
            {
                HttpPostedFileBase file = Request.Files[""];
                var data = _da.UploadCustomerToOrder(file);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage delivery charges
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageDeliveryCharges(DeliveryCharge model)
        {
            try
            {
                var data = _da.ManageDeliveryCharges(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage cart settings like discount and profit margin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageCartSettings(CartSettingsModel model)
        {
            try
            {
                var data = _da.ManageCartSettings(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage select currency
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ManageCurrency(string currency)
        {
            try
            {
                var data = _da.ManageCurrency(currency);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// send order mail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SendOrderMail(GetOrderMail model)
        {
            try
            {
                var data = _da.SendOrderMail(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get order xml file by id
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetOrderFileById(long OrderId, string type)
        {
            try
            {
                var data = _da.GetOrderFileById(OrderId, type);
                return Json(new { Message = "Success", Result = data });
            }
            catch (Exception ex)
            {
                return Json(new { Message = ex.Message });
            }
        }

        #endregion

        #region

        /// <summary>
        /// Download seleced order in zip
        /// </summary>
        /// <param name="orderIDs"></param>
        /// <returns></returns>
        public JsonResult DownloadSelectedOrders(string orderIDs)
        {
            try
            {
                var data = _da.DownloadSelectedOrders(orderIDs);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region Fetch text related to selected help page UpdatePageHelpInfo

        public JsonResult GetPageHelpInfo(string pageName)
        {
            try
            {
                var res = _da.GetPageHelpInfo(pageName);

                if (res.StartsWith("Error"))
                {
                    return Json(new { Message = "Failed" });
                }
                else
                {
                    return Json(new { Message = "Success", Description = res });
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region Update page help

        public JsonResult UpdatePageHelpInfo(string pageName, string description)
        {
            try
            {
                var res = _da.UpdatePageHelpInfo(pageName, description);

                if (res.StartsWith("Error"))
                {
                    return Json(new { Message = "Failed" });
                }
                else
                {
                    return Json(new { Message = "Success", Description = res });
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion

        #region Download sample postcode file

        public ActionResult DownloadPostCodeTemplate(string type)
        {
            if (type == "P")
            {
                string fullPath = Path.Combine(Server.MapPath("~/Content/Uploads/"), "Suburb and Post Codes.xlsx");
                return File(fullPath, "application/vnd.ms-excel", "Suburb and Post Codes.xlsx");
            }
            else if (type == "U")
            {
                string fullPath = Path.Combine(Server.MapPath("~/Content/Uploads/"), "SuburbPostcodeTemplate.xlsx");
                return File(fullPath, "application/vnd.ms-excel", "SuburbPostcodeTemplate.xlsx");
            }
            else if (type == "D")
            {
                string fullPath = Path.Combine(Server.MapPath("~/Content/Uploads/"), "Delivery Runs.xlsx");
                return File(fullPath, "application/vnd.ms-excel", "Delivery Runs.xlsx");
            }
            else
            {
                string fullPath = Path.Combine(Server.MapPath("~/Content/Uploads/"), "DeliveryRunTemplate.xlsx");
                return File(fullPath, "application/vnd.ms-excel", "DeliveryRunTemplate.xlsx");
            }
        }

        #endregion

        #region Get Postcode / suburbs / pickups / deliveryruns

        [HttpPost]
        public JsonResult GetCutOffData(string type, string states)
        {
            var res = _da.GetCutOffData(type, states.TrimEnd(','));

            return Json(res);
        }

        #endregion

        #region Set Order limit value

        [HttpPost]
        public JsonResult SetOrderLimit(OrderLimitBO model)
        {
            var res = _da.SetOrderLimit(model);
            return Json(res);
        }

        #endregion

        #region Save order cutoff time

        [HttpPost]
        public JsonResult SaveOrderCutOff(CutoffTimeBO model)
        {
            var res = _da.SaveOrderCutOff(model);
            return Json(new
            {
                Message = res.Split('_')[0],
                CutOff = res.Split('_')[1]
            });
        }

        #endregion

        public ActionResult _OrderCutOffSection()
        {
            ViewBag.ReqDate = _da.GetRequiredDate();
            return PartialView();
        }

        public ActionResult _PromoCodes()
        {
            var _db = new SaaviAdminEntities();
            ViewBag.IsPromo = _db.MainFeaturesMasters.FirstOrDefault().IsPromoCode;

            return PartialView();
        }

        public ActionResult _ManagePopups()
        {
            return PartialView();
        }

        #region Save Promo codes

        [HttpPost]
        public JsonResult ManagePromoCodes(PromoCodeBO model)
        {
            if (model.Type == "G")
            {
                var res = _da.GetPromocodeData(model.PromoID);
                return Json(res);
            }
            else
            {
                if (model.Type != "D")
                {
                    if (model.Products != null && model.Products.Length > 0)
                    {
                        model.Prods = string.Join(",", model.Products);
                    }
                }

                var res = _da.ManagePromoCodes(model);
                return Json(res);
            }
        }

        public JsonResult UpdateManageFeature(string value)
        {
            var objMaster = new AccountDA();

            var res = objMaster.UpdateAdminOrMainFeature("IsPromoCode", value == "False" ? "0" : "1", "mainfeaturesmaster");

            return Json(res);
        }

        #endregion

        public JsonResult ManagePermittedDays(PermittedDaysBO model)
        {
            var res = _da.ManagePermittedDays(model.li, model.PermitBy, model.From, model.To, model.ID);
            return Json(res);
        }

        public JsonResult UpdateIndividualFeatures(string column, bool value)
        {
            var res = _da.UpdateIndividualFeatures(column, value);
            return Json(res);
        }

        #region Manage Run Pop

        [HttpPost]
        public JsonResult ManageRunPopPop(ManageModelPopBO model)
        {
            var res = _da.ManageRunPop(model);
            return Json(res);
        }

        [HttpPost]
        public JsonResult GetModelGroupList(string GroupType)
        {
            var res = _da.GetAllGroupList(GroupType);
            return Json(res);
        }

        [HttpPost]
        public JsonResult AddUpdateModelGroupDetails(ManageModelPopDetailsBO model)
        {
            var res = _da.AddUpdateGroupDetails(model);
            return Json(res);
        }

        [HttpPost]
        public JsonResult GetGroupDetailByID(int GroupID, string Type)
        {
            var res = _da.GetAllGroupDetailByID(GroupID, Type);
            return Json(res);
        }

        [HttpPost]
        public JsonResult RemoveGroupByID(int GroupID, string Type)
        {
            var res = _da.DeleteGroupByID(GroupID, Type);
            return Json(res);
        }

        [HttpPost]
        public JsonResult RemoveModelGroupDetails(ManageModelPopDetailsBO model)
        {
            var res = _da.RemoveGroupDetails(model);
            return Json(res);
        }

        #endregion

        #region Upload customer delivery days template

        /// <summary>
        /// manage customer days
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageDeliveryRunData()
        {
            try
            {
                HttpPostedFileBase file = Request.Files["fuUploadDeliveryRunData"];

                if (file.ContentLength > 0)
                {
                    DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/Content/Uploads/TempFiles/"));
                    foreach (FileInfo f in di.GetFiles())
                    {
                        f.Delete();
                    }
                    file.SaveAs(Server.MapPath("~/Content/Uploads/TempFiles/") + file.FileName);

                    DataTable dt = _da.GetDataTableFromExcel(Server.MapPath("~/Content/Uploads/TempFiles/") + file.FileName, "ALL");
                    string res = "";
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        res = _da.InsertDataIntoSQLServerUsingSQLBulkCopy(dt, "ImportDeliveryRuns");
                    }

                    ViewData["Message"] = res;
                    //return Json(data);
                    return RedirectToAction("Settings", "Order");
                }
                ViewData["Message"] = "Success";
                //return Json("Success");
                return RedirectToAction("Settings", "Order");
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                return RedirectToAction("Settings", "Order");
                //return Json(ex.Message);
            }
        }

        #endregion

        #region Edit products in Admin

        [HttpPost]
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult _GetOrderToEdit(long cartID, bool? isPaymentDone)
        {
            var model = new GetOrderToEditBO();
            var res = _da.GetOrderDetails(cartID);
            var paymentRefundDetail = _da.GetRefundDetail(cartID);

            if (res != null && res.Count > 0)
            {
                model.DiscountValue = "0.00";
                model.DiscountPercentage = "";
                model.CouponName = "";
                if (!string.IsNullOrEmpty(res[0].CouponName))
                {
                    string productIds = string.Empty;
                    model.CouponName = res[0].CouponName;
                    var promoCode = _da.GetPromoCodeDetails(res[0].CouponName);
                    if (promoCode != null && promoCode.CodeType == "Discount")
                    {
                        model.DiscountPercentage = Convert.ToString(promoCode.Value) + "%";
                        productIds = promoCode.Products;
                    }
                    else if (promoCode != null && promoCode.CodeType == "Value")
                    {
                        model.DiscountValue = Convert.ToString(Math.Round(Convert.ToDouble(promoCode.Value), 2));
                        productIds = promoCode.Products;
                    }
                    if (!string.IsNullOrEmpty(productIds) && productIds.Length > 0)
                    {
                        foreach (var item in res)
                        {
                            if (productIds.Contains(Convert.ToString(item.ProductID)))
                            {
                                item.IsCouponItem = true;
                            }
                        }
                    }
                }

                model.CustomerID = res[0].CustomerID;
                model.CustomerName = res[0].CustomerName;
                model.CustomerEmail = res[0].CustomerEmail;
                model.DeliveryName = res[0].DeliveryName;
                model.CustomerPhone = res[0].CustomerPhone;
                model.Warehouse = res[0].Warehouse;
                model.OrderDate = res[0].OrderDate.To<DateTime>().ToString("dd/MM/yyyy");
                model.Address = res[0].Address;
                model.RunNo = res[0].RunNo;
                model.Suburb = res[0].Suburb;
                model.OrderComments = res[0].OrderComments;
                model.IsDelivery = res[0].IsDelivery;
                model.DeliveryType = res[0].DeliveryType;
                model.CartID = res[0].CartID;
                model.CreatedDate = res[0].CreatedDate.To<DateTime>().ToString("dd/MM/yyyy");
                model.DeliveryNotes = res[0].DeliveryNotes;
                model.TotatOriginal = res.Sum(s => s.Price * s.Quantity);
                model.Total = model.TotatOriginal;
                model.TotalRevised = res.Sum(s => s.RevisedPrice * s.RevisedQuantity);
                model.TotalDifference = res.Sum(s => s.Price * s.Quantity) - res.Sum(s => s.RevisedPrice * s.RevisedQuantity);
                model.OrderDetails = res;
                model.IsHasFreightCharges = res[0].IsHasFreightCharges;
                model.RevisedFreight = res[0].RevisedFreightCharge;
                model.OriginalFreight = res[0].OriginalFreightCharge;
                model.AddressID = res[0].AddressID;
                model.DAddress1 = res[0].DAddress1;
                model.DSuburb = res[0].DSuburb;
                model.DZIP = res[0].DZIP;


                if (res[0].OriginalFreightCharge > 0 || res[0].CouponAmount > 0)
                {
                    model.TotalFreightDifference = res[0].RevisedFreightCharge - res[0].OriginalFreightCharge;
                    model.TotalDifference = model.TotalDifference + model.TotalFreightDifference;
                    if (model.TotatOriginal > 0)
                    {
                        model.TotatOriginal = model.TotatOriginal + res[0].RevisedFreightCharge - res[0].CouponAmount;
                        model.Total = model.TotatOriginal;
                    }
                    else
                    {
                        model.TotatOriginal = model.TotatOriginal;
                        model.Total = model.TotatOriginal;
                    }
                    if (model.TotalRevised > 0)
                    {
                        model.TotalRevised = model.TotalRevised + res[0].OriginalFreightCharge;
                    }
                    else
                    {
                        model.TotalRevised = model.TotalRevised;
                    }
                }
                model.CouponAmount = res[0].CouponAmount;
                model.isPaymentDone = isPaymentDone;
                model.isRefundDone = false;
                if (paymentRefundDetail != null)
                {
                    model.isRefundDone = true;
                }
                model.InvoiceNumber = res[0].InvoiceNumber;
            }

            model.PickupList = _da.GetPickUpAddress();
            model.PickUpId = "0";
            if (res != null && res.Count > 0)
            {
                model.PickUpId = Convert.ToString(res[0].PickUpId);
            }

            ViewBag.Runs = _da.GetDeliveryRuns();

            return PartialView("_GetOrderToEdit", model);
        }

        [HttpPost]
        public ActionResult _GetProductsToAdd(long cartID)
        {
            var products = _da.GetProducts(cartID);

            return PartialView("_GetProductsToAdd", products);
        }

        #endregion

        public JsonResult SetCutoffPermittedDays(SetCutoffPermittedDaysBO model)
        {
            var res = _da.SetCutoffPermittedDays(model);
            return Json(res);
        }

        public JsonResult GetCutoffPermittedDays(GetCutoffPermittedDaysBO model)
        {
            var res = _da.GetPermittedDays("", model.GroupID, model.PermitBy);
            return Json(res);
        }

        public JsonResult AddProductsToOrder(long cartID, List<AddProductsToOrderBO> products)
        {
            var res = _da.AddProductsToOrder(cartID, products);
            return Json(res);
        }

        public JsonResult DeleteCartItem(long itemID)
        {
            var res = _da.DeleteCartItem(itemID);
            return Json(res);
        }

        public JsonResult updateOrderDetails(UpdateOrderDetailsBO model)
        {
            var res = _da.updateOrderDetails(model);
            return Json(res);
        }

        #region Download Customer orders into excel file.

        public ActionResult DownloadOrders(string orders)
        {
            var exportOrders = _da.DownloadOrders(orders);
            byte[] bin = null;
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                //create a WorkSheet
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Orders");

                worksheet.Cells["A1"].LoadFromCollection(exportOrders, true).AutoFitColumns();

                //convert the excel package to a byte array
                bin = excelPackage.GetAsByteArray();
            }

            // string fullPath = Path.Combine(Server.MapPath("~/Content/Uploads/"), "SuburbPostcodeTemplate.xlsx");
            return File(bin, "application/vnd.ms-excel", "OrdersList.xlsx");
        }

        #endregion

        #region Send invoice to customers by Email

        [HttpPost]
        public JsonResult CheckAndSendInvoice(string email, long cartID, bool IsRefund)
        {
            try
            {
                string path = Server.MapPath("~/content/tempInvoices/");
                var res = _da.GenerateAndSendInvoice(email, cartID, IsRefund, path);
                return Json(res);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        #endregion


        #region Update Delivery Address

        public JsonResult UpdateCustomerDeliveryAddress(int AddressID ,string deliveryaddress,string suburb,string zip)
        {
            var res = _da.updateDeliveryAddress( AddressID,  deliveryaddress,  suburb,  zip);
            return Json(res);
        }
       
        #endregion
    }
}