﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Saavi5Admin.Controllers
{
    [Authorize]
    public class SupplierController : Controller
    {
        #region GET: methods
        // GET: Supplier
        public ActionResult BySupplier()
        {
            return View();
        }
        // GET: Items
        public ActionResult ByItems()
        {
            return View();
        }
#endregion
    }
}