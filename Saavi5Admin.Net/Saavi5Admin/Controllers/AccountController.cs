﻿using DataModel.BO;
using DataModel.DA;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Saavi5Admin.Controllers
{
    
    public class AccountController : Controller
    {
        private readonly AccountDA _da;
        public AccountController()
        {
            _da = new AccountDA();
        }

        private ActionResult ValidateLogin() {
            if ((System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (System.Web.HttpContext.Current.User.IsInRole("SuperAdmin"))
                {
                    return RedirectToAction("ManagedFeatures", "Master");
                }
                else if (System.Web.HttpContext.Current.User.IsInRole("CompanyAdmin"))
                {
                    return RedirectToAction("Dashboard", "Company");
                }
                else
                {
                    return RedirectToAction("LogOff", "Account");
                }
            }
            return null;
        }

        #region Get Methods
        // GET: Login
        public ActionResult Login()
        {
            var action = ValidateLogin();
            if (action != null) {
                return action;
            }
            return View();
        }
        //GET: Forgot Password
        public ActionResult _ForgotPassword()
        {
            return PartialView();
        }
        //GET: Reset password
        public ActionResult ResetPassword()
        {
            return View();
        }
       
        #endregion
        #region 
        /// <summary>
        /// Validate login credentials for a user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(LoginViewModel model)
        {
            var action = ValidateLogin();
            if (action != null)
            {
                return action;
            }
            var result = _da.ValidateUser(model.Email, model.Password);

            if (result.Message == "Success")
            {
                if (string.Compare(result.Role, "SuperAdmin", true) == 0)
                {
                    return RedirectToAction("ManagedFeatures", "Master");
                }
                else if (string.Compare(result.Role, "CompanyAdmin", true) == 0)
                {
                    return RedirectToAction("Dashboard", "Company");
                }
                //else
                //{ 
                // return RedirectToAction("ManageDrivers", "Saavi5POD/Driver"); //}
                else
                {
                  //  ViewBag.UserType = _da.GetAllUserType();
                    ViewData["Message"] = "Success";
                    return View(model);
                }
            }
            else
            {
               // ViewBag.UserType = _da.GetAllUserType();
                ModelState.AddModelError("", result.Message);
                return View(model);
            }
        }
        #endregion

        #region 
        /// <summary>
        /// Logout user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[Authorize(Roles="SuperAdmin")]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
        #endregion

        #region 
        /// <summary>
        /// Forgot password for SuperAdmin/CompanyAdmin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                var data = _da.ForgotPassword(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion

        #region 
        /// <summary>
        /// Reset password for SuperAdmin/CompanyAdmin
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ResetPassword(ResetPasswordViewModel model, string id = "")
        {
            try
            {
                model.Code = id;
                var data = _da.ResetPassword(model);
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        #endregion
       
    }
}