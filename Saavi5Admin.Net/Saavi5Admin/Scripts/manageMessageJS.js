﻿// Change url based on Domain
var URL = '/Message/';
var podURL = '/POD/Message/';
if (window.location.href.indexOf('saavi.com') !== -1) {
    URL = "/Admin/Message/";
    podURL = "/Admin/POD/Message/";
}

$(function () {

});

// Initialize the scrollbar 
function initScrollbar(val) {
    $("#liCustomers").mCustomScrollbar({
        theme: "minimal-dark",
        callbacks: {/*user custom callback function on scroll end reached event*/
            onTotalScroll: function () {
                getCustomersPaging(val, 1);
            },
            onTotalScrollOffset: 10, /*scroll end reached offset: integer (pixels)*/
        },
        advanced: {
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        scrollButtons: {
            enable: true
        }
    });
}
function searchCustomer() {
    getCustomersPaging(0, 0);

}
// Fetch customers from the DB. 50 at a time
function getCustomersPaging(v1, val2) {
    var len = $("#liCustomers li").length;
    //var divcustID = 'divCustomers';
    if (val2 === 1 && len > 0 && len < 50) {
    } else {
        var stext = $("#txtCustSearch2").val();
        if (v1 === 1) {
            stext = '';
            startLoading();
            // divcustID = 'managegroupModal';
        } else {
            loadingOnElementStart('divCustomers');
        }
        var pagesize = 50; var grpID = 0;
        if (val2 === 0) {
            len = 0;
        }
        if (v1 === 1 && val2 === 0) {
            pagesize = 0; grpID = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
        }
        var model = {
            IsRep: false,
            SearchText: stext,
            PageNumber: (len / 50),
            PageSize: pagesize,
            GroupID: grpID
        }
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URL + 'GetCustomersWithPaging',
            data: "{'model':" + JSON.stringify(model) + "}",
            success: function (response) {
                var cust = response.Result;
                if (response.Result.length > 0) {
                    if (v1 === 0) {
                        var html = '';
                        $.each(cust, function (key, val) {
                            if (v1 === 0) {
                                //  html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="getCustomerDetails(this);">' + val.CustomerName + '</a></li>'
                                html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="selectCustomer(this);">' + val.CustomerName + '</a></li>'
                            } else {
                                //  html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="setCustomerID(this);">' + val.CustomerName + '</a></li>'
                                html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="selectCustomer(this);">' + val.CustomerName + '</a></li>'
                            }
                        });
                        if (val2 === 0) {
                            $("#mCSB_1_container").html('');
                        }
                        $("#mCSB_1_container").append(html);
                        setTimeout(function () {
                            $("#liCustomers").mCustomScrollbar("update");
                        }, 200);
                    } else {
                        $("#ddlCustomers").empty();
                        $.each(cust, function (key, val) {
                            $("#ddlCustomers").append($("<option></option>").val(val.CustomerID).html(val.CustomerName));
                        });
                    }
                } if (v1 === 1) { stopLoading(); } else { loadingOnElementStop('divCustomers'); }
            }
        });
    }
}

//get all groups
function getGroup() {
    $("#deliveryRunsGroupList").empty();
    $.ajax({
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        url: URL + "GetGroup",
        success: function (response) {
            if (response.Message === "Success") {
                if (response.Result.length > 0) {
                    $("#ddlGroup").empty().append($("<option></option>").val("0").html("Select"));
                    $.each(response.Result, function (key, val) {
                        $("#ddlGroup").append($("<option></option>").val(val.GroupID).html(val.GroupName));
                    });
                }
            }
        }
    });







}

//get group customer
function getGroupCustomers(val, type) {

    $("#deliveryRunsGroupList").empty();
    startLoading();
    var groupID = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
    $("#GroupID").val(groupID);
    $("#ID").val(groupID);
    $("#UserType").val('G');
    $("#liCustomers").find('a').removeClass('active');
    $("#liCustomers").find('li').removeClass('active');
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        url: (type == "c" ? URL : podURL) + "GetGroupCustomers",
        data: "{'groupID':" + groupID + ",'type':'" + type + "'}",
        async: false,
        success: function (response) {
            DDLDeliveryRun("");
            if (response.Message === "Success") {

                if (response.Result.length > 0) {
                    if (val === 0) {
                        var html = '';
                        $.each(response.Result, function (key, val) {
                            html += '<p>' + val.CustomerName + '</p>';
                        });
                        $("#divRecipients").html(html);

                       
                    }
                    else {
                        $("#ddlGroupCustomer").empty();
                        $.each(response.Result, function (key, val) {
                            $("#ddlGroupCustomer").append($("<option></option>").val(val.CustomerID).html(val.CustomerName));
                        });
                    }
                } else { $("#divRecipients").html(''); }
            }
        }
    });

    getAllGroupProducts(0);
    setTimeout(function () {
        stopLoading();
    }, 200);
}

function setGroupCustomers() {
    var Array = [];
    $("#ddlGroupCustomer >option").each(function () {
        Array.push(this.value);
    });
    $("#GroupCustomers").val(Array);
}

function manageGroup(type, userType) {
    var link = URL;
    if (userType == "d") {
        link = podURL;
    }
    if (type === 0) {
        $('#modalManageGroup').load(link + '_ManageGroups/');
    } else {
        if ($('#ddlGroup').val() === "0") { $.alert('Please select a group.'); }
        else {
            $('#modalManageGroup').load(link + '_ManageGroups/' + $('#ddlGroup').val());
        }
    }
}

//delete group
function deleteGroup() {
    var id = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
    if (id > "0") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected group?',
            buttons: {
                confirm: function () {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteGroup',
                        data: "{'id':" + id + "}",
                        success: function (response) {
                            if (response === "Success") {
                                getGroup();
                                $("#divRecipients").html('');
                                $("#deliveryRunsGroupList").empty();
                                growlNotification('Selected group deleted.', 'success', 3000);
                            }
                            else { growlNotification(response, 'warning', 3000); }
                            stopLoading();
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        $.alert('Please select a group.');
    }
}

//get all products
function getAllNonProducts(val, type) {
    var groupid = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
    var searchtext = '';
    var len = $("#ddlNonProducts option").length;
    if (val === 0) { len = 0; }
    if (val === 1 && len > 0 && len < 50) {
    } else {
        if (type !== '') {
            startLoading();
            searchtext = $("#txtProductSearch").val();
        }
        var model = {
            SearchText: searchtext,
            GroupID: groupid,
            PageNumber: len / 50,
            PageSize: 50,
            Type: type
        }
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URL + 'GetAllNonProducts',
            data: "{'model':" + JSON.stringify(model) + "}",//"{'SearchText':'" + searchtext + "','groupID':" + groupid + "}",
            async: false,
            success: function (response) {
                //console.log(response);
                var data = response;
                if (data.length > 0) {
                    if (val === 0) {
                        $("#ddlNonProducts").empty();
                    }
                    $.each(data, function (key, val) {
                        $("#ddlNonProducts").append($("<option></option>").val(val.ProductID).html(val.ProductName));
                    });
                }
                else if (data.length === 0 && val === 1) { }
                else if (data.length === 0 && val === 0) { $("#ddlNonProducts").empty(); }
                // else { $("#ddlNonProducts").html('No products available.'); }
                if (type !== '') { stopLoading(); }
            }
        });
    }
}

//get all group products
function getAllGroupProducts(val) {
    var groupid = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'GetAllGroupProducts',
        data: "{'groupID':" + groupid + "}",
        async: false,
        success: function (response) {
            // console.log(response);
            var data = response;
            if (data.length > 0) {
                if (val === 0) {
                    var html = '';
                    $("#liSpecialProductinPdf").html('');
                    $.each(data, function (key, val) {
                        html += "<p>" + val.ProductName + "</p>";
                    });
                    $("#liSpecialProductinPdf").html(html);
                } else {
                    $("#ddlGroupProducts").empty();
                    $.each(data, function (key, val) {
                        $("#ddlGroupProducts").append($("<option></option>").val(val.ProductID).html(val.ProductName));
                    });
                }
            } else { $("#liSpecialProductinPdf").html(''); }
        }
    });
}

//manage group products
function manageGroupProduct() {
    var groupid = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
    if (groupid > 0) {
        startLoading();
        var productArray = [];
        $("#ddlGroupProducts >option").each(function () {
            productArray.push(this.value);
        });
        console.log(productArray);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URL + 'ManageGroupProducts',
            data: "{'productArray':'" + productArray + "','groupID':" + groupid + "}",
            success: function (response) {
                if (response === 'Success') {
                    getAllGroupProducts(0);
                    $("#specialproductModel").modal('hide');
                    growlNotification('Product added to Pdf successfully.', 'success', 3000);
                } else { growlNotification(response, 'warning', 3000); }
                stopLoading();
            }
        });
    }
    else {
        $.alert('Select any group');
    }
}


//move record left to right
function moveItemLeftToRight(divLeft, divRight) {
    if ($("#" + divLeft)[0].selectedOptions.length > 0) {
        var data = $("#" + divLeft)[0].selectedOptions;
        $($("#" + divLeft)[0].selectedOptions).each(function () {
            $("#" + divRight + " option[value='" + this.value + "']").remove();
            $("#" + divRight).append($("<option></option>").val(this.value).html(this.text));
            $("#" + divLeft + " option[value='" + this.value + "']").remove();
        });
    }
    else { $.alert('Please select atleast one record in list to move'); }
}

//move record right to left
function moveItemRightToLeft(divLeft, divRight) {
    if ($("#" + divRight)[0].selectedOptions.length > 0) {
        var data = $("#" + divRight)[0].selectedOptions;
        $($("#" + divRight)[0].selectedOptions).each(function () {
            $("#" + divLeft).append($("<option></option>").val(this.value).html(this.text));
            $("#" + divRight + " option[value='" + this.value + "']").remove();
        });
    }
    else { $.alert('Please select atleast one record in list to move'); }
}

//get partial page
function getPartial(ctrl) {
    loadingOnElementStart('divMain');
    var loadId = $(ctrl).data('id'),
        ddlGrp = $('#ddlGroup'),
        selectedCustomer = $('#liCustomers li>a.active');

    if (loadId == '_UploadPDF') {
        var customerId = selectedCustomer.data('id'),
            groupId = ddlGrp.val();

        if (groupId > 0) {
            loadId += '?id=' + groupId + '&type=G';
        } else if (customerId > 0) {
            loadId += '?id=' + customerId + '&type=S';
        } else {
            loadId += '?id=0&type=A';
        }
    }
    $('#divPartial').load(URL + loadId, function () {
        if (loadId == '_SendMessage' || loadId == '_UploadPDF') {
            if (ddlGrp.val() > 0) {
                getGroupCustomers(0, 'c');
            } else {

                if (selectedCustomer.length > 0) selectCustomer(selectedCustomer[0]);
            }
        }
    });
    $("#liTab").find('li').removeClass('active');
    $(ctrl).parent('li').addClass('active');
    setTimeout(function () {
        loadingOnElementStop('divMain');
    }, 200);
}

function checkAndLoadUploadedPDF() {
    var selTab = $("#liTab").find('li.active a');
    if (selTab.length < 1 || selTab.data('id') != '_UploadPDF') {
        return false;
    }
    getPartial(selTab[0]);
}

//open special product model
function getSpProductModel() {
    var groupid = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
    if (groupid > 0) {
        startLoading();
        getAllNonProducts(0, '');
        getAllGroupProducts(1);
        $("#specialproductModel").modal('show');
        setTimeout(function () {
            stopLoading();
        }, 200);
    }
    else {
        $.alert('Please select a group.');
    }
}

//select customer
function selectCustomer(ctrl) {
    $("#ddlGroup").val('0');
    $("#divRecipients").html('');
    $("#liSpecialProductinPdf").html('');
    $("#liCustomers").find('a').removeClass('active');
    $("#liCustomers").find('li').removeClass('active');
    $(ctrl).addClass('active');
    var id = parseInt($(ctrl).data('id')), userType = 'S';
    if (id === 0) {
        userType = 'A';
    }
    $('#ID').val(id);
    $('#CustomerID').val(id);
    $('#UserType').val(userType);
}


//Get all vehicle
function GetAllDriver() {
    //var model = {
    //    SearchText: $("#txtMessageSearch").val(),
    //    PageNumber: 0,
    //    PageSize: 50
    //}
    //  console.log(model);      
    var txt = $("#txtMessageSearch").val();
    $.ajax({
        type: "POST",
        contentType: "appliaction/json;",
        dataType: "json",
        url: podURL + "GetAllDriver/" + txt,
        // data: '{"id":"' + txt + '","type":"' + type + '"}',
        //data: "{'model':" + JSON.stringify(model) + "}",
        async: false,
        success: function (response) {
            if (response.Message === "Success") {
                var html = '';
                $("#liCustomers").html('');
                console.log(response.Result.length);
                if (response.Result.length > 0) {
                    $.each(response.Result, function (key, value) {
                        html += '<li class="outer ' + (key === 0 ? "active" : "") + '" data-id="' + value.ID + '" onclick="selectCustomer(this);"><a href="javascript:;" class="head">' + value.Name + '</a></li>';
                    });
                    $("#liCustomers").html(html);

                } else { $("#liCustomers").html('No record found!'); }
            } else { growlNotification(response.Message, "warning", 3000); }
        },
        error: function (response) {
            console.log(JSON.stringify(response));
        }
    });
}

//
//open partial page
function openPartialPage(divModal, partialPage) {
    $('#' + divModal).load(URL + partialPage);
}
function openPartialPagePOD(divModal, partialPage) {
    $('#' + divModal).load(podURL + partialPage);
}
////send message
//function SendMessage() {
//    var model = {
//        ID: 0,
//        UserType: '',
//        Message:''
//    }
//    $.ajax({
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        // url: URL + 'SendMessage',
//        data: "{'model':" + JSON.stringify(model) + "}",
//        async: false,
//        success: function (response) {
//            //console.log(response);
//            var data = response;
//            if (data.length > 0) {
//                $("#ddlNonProducts").empty();
//                $.each(data, function (key, val) {
//                    $("#ddlNonProducts").append($("<option></option>").val(val.ProductID).html(val.ProductName));
//                });
//            }
//            else { $("#ddlNonProducts").html('No products available.'); }
//        }
//}


function ddlMsgFilter(ctrl) {

    var filterVal = $(ctrl).val();
    $("#ddlGroupCustomer").empty();
    if (filterVal == "D") {
        $("#Type").val('R');
        $("#divLeft h4").text("All Delivery Run");
        $("#divRight h4").text("Group Delivery Run");
        DDLDeliveryRun("P");
        getDeliveryRun();

    }
    else {
        $("#Type").val('c');
        $("#divLeft h4").text("All Customers");
        $("#divRight h4").text("Group Customers");
        getCustomersPaging(1, 0);
        getGroupCustomers(1, 'c');

    }
}

function getDeliveryRun() {
    var type = "c";
    startLoading();
    var groupID = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());
    $("#GroupID").val(groupID);
    $("#ID").val(groupID);
    $("#UserType").val('G');
    $("#liCustomers").find('a').removeClass('active');
    $("#liCustomers").find('li').removeClass('active');
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        url: (type == "c" ? URL : podURL) + "GetGroupCustomers",
        data: "{'groupID':" + groupID + ",'type':'R'}",
        async: false,
        success: function (response) {
            if (response.Message === "Success") {
                if (response.Result.length > 0) {
                  
                   
                        $("#ddlGroupCustomer").empty();
                        $.each(response.Result, function (key, val) {
                            $("#ddlGroupCustomer").append($("<option></option>").val(val.CustomerID).html(val.CustomerName));
                        });
                    
                } else { $("#divRecipients").html(''); }
            }
        }
    });

    getAllGroupProducts(0);
    setTimeout(function () {
        stopLoading();
    }, 200);
}

function DDLDeliveryRun(type) {
    var listID = "";
    var innertype = "";
    if (type == "P") {
        listID = "#divLeft #ddlCustomers";
        innertype = "R";
    }
    else {
        listID = "#deliveryRunsGroupList";
        innertype = "RM";// "RM";
    }
   var grpID = parseInt($("#ddlGroup").val() === "" ? 0 : $("#ddlGroup").val());

    $.ajax({
        url: URL + 'GetModelDeliveryRunList',
        type: 'POST',
        data: "{'groupID':" + grpID + ",'type':'"+innertype+"'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(listID).empty();
            if (response.Message === "Success") {
           

                $.each(response.Result, function (idx, val) {

                    $(listID).append($("<option></option>").val(val.CustomerID).html(val.CustomerName));

                });


                //  resetManagemodelPop();
            }
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}