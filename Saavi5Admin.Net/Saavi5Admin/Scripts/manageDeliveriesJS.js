﻿// Change url based on Domain
var URL = '/POD/Deliveries/';
if (window.location.href.indexOf('saavi.com') !== -1) {
    URL = "/Admin/POD/Deliveries/";
}

//get customers
function GetCustomers() {
    startLoading();
    var txt = $("#txtDeliverySearch").val();
    $("#liCustomer").load(URL + "_Customers/" + txt.split(' ').join('%20'));
    stopLoading();
}

//get order with issues
function GetOrderHasIssue(ctrl) {
    GetOrderList(ctrl, "i");
}

//get order list with/without issue
function GetOrderList(ctrl, type) {
    loadingOnElementStart('divOrderList');
    var custID = $(ctrl).data('id');
    var orderID = 0;
    $("#divOrderList").load(URL + "_Orders/" + custID + "?orderid=" + orderID + "&type=" + type + "&delDate=null");
    $("#liCustomer").find('li').removeClass('active');
    $(ctrl).addClass('active');
    setTimeout(function () {
        loadingOnElementStop('divOrderList');
    }, 400);
}

//get order list by delivery date
function GetSearchedOrderList() {
    var type = "";
    var ctrl = $("#divOrderList").find('li').eq(0);
    loadingOnElementStart('divOrderList');
    var orderID = 0; var delDate = null;
    if ($("#txtDeliverySearch").val() !== "") {
        orderID = parseInt($("#txtDeliverySearch").val());
    }
    if ($("#txtPickDeliveryDate").val() !== "") {
        delDate = Date($("#txtPickDeliveryDate").val());
    }
    $("#divOrderList").load(URL + "_Orders/0?orderid=" + orderID + "&type=" + type + "&delDate='" + delDate + "'");
    $("#liCustomer").find('li').removeClass('active');
    $(ctrl).addClass('active');
    setTimeout(function () {
        loadingOnElementStop('divOrderList');
    }, 400);
}

//get selected order detail
function GetOrderDetail(ctrl) {
    loadingOnElementStart('divOrderDetail');
    var orderID = $(ctrl).data('id');
    $("#divOrderDetail").load(URL + "_OrderDetail/" + orderID);
    $("#divOrderList").find('li').removeClass('active');
    $(ctrl).addClass('active');
    setTimeout(function () {
        var dID = $("#DeliveryID").val();
        console.log(dID);
        $("#divDeliveryImages").load(URL + "_DeliveryImages/" + dID + "?hasissue=true")
        loadingOnElementStop('divOrderDetail');
    }, 1000);
}
//
function GetDelIssueLoad() {
    startLoading();
    $("#liCustomer").load(URL + "_Customers")
    setTimeout(function () {
        var li = $("#liCustomer").find('li').eq(0);
        console.log(li);
        if (li.length > 0) {
            GetOrderList(li);
        }
        stopLoading();
    }, 800);
}
// get all customer for pod copy
function GetPODAllCustomers() {
    startLoading();
    var txt = $("#txtDeliverySearch").val();
    $("#liCustomer").load(URL + "_PODAllCustomers/" + txt.split(' ').join('%20'));
    stopLoading();
}

//get all order by selected customer for pod copy
function GetPODALLOrderByCustomer(ctrl) {
    loadingOnElementStart('divOrderList');
    var custID = $(ctrl).data('id');
    $("#divOrderList").load(URL + "_PODAllOrdersByCustomer/" + custID);
    $("#liCustomer").find('li').removeClass('active');
    $(ctrl).addClass('active');
    setTimeout(function () {
        loadingOnElementStop('divOrderList');
    }, 1000);
}

//get order detail for selected order for pod copy
function GetPODOrderDetail(ctrl) {
    loadingOnElementStart('divOrderDetail');
    var orderID = $(ctrl).data('id');
    //  $("#divOrderDetail").empty();
    $("#divOrderDetail").load(URL + "_OrderDetail/" + orderID);
    $("#modalOrderDetail").modal('show');
    setTimeout(function () {
        loadingOnElementStop('divOrderDetail');
    }, 600);
}

// get all assigned drivers
function GetAllAssignedDrivers() {
    startLoading();
    var txt = $("#txtDeliverySearch").val();
    $("#liAssignedDrivers").load(URL + "_AssignedDrivers/" + txt.split(' ').join('%20'));

    stopLoading();
}
//get all assigned orders for selected driver
function GetAssignedOrders(ctrl, type) {
    console.log('assign')
    // loadFieldChooser();
    setTimeout(function () {
        loadingOnElementStart('sourceFields');
        var id = $(ctrl).data('id');
        $("#hdnDriverID").val(id);
        $("#sourceFields").load(URL + "_AssignedOrders/" + id);
        $("#liAssignedDrivers").find('li').removeClass('active');
        $(ctrl).addClass('active');
        setTimeout(function () {
            loadingOnElementStop('sourceFields');
        }, 1000);
    }, 400);
}
function GetAssignedOrdersByDriver(ctrl, divID) {
    if ($(ctrl).val() != "") {
        if ($("#ddlDriverLeft").val() == $("#ddlDriverRight").val()) {
            $(ctrl).val('');
            $("#" + divID).empty();
            growlNotification("Choose different driver in both lists!", "warning", 3000);
        } else {
            loadingOnElementStart(divID);
            var id = $(ctrl).val();
            $("#" + divID).load(URL + "_AssignedOrders/" + id);
            setTimeout(function () {
                loadingOnElementStop(divID);
            }, 1000);
        }
    }
    else { $("#" + divID).empty(); }
}
//get all unassigned orders
function GetUnassignedOrders() {
    console.log('unassign')
    loadingOnElementStart('destinationFields');
    var li = $("#liAssignedDrivers").find('li.active').eq(0);
    var run = li.data('run');
    $('#lblRun').html('<b>Run No:</b> ' + run);
    $('#lblDriver').html('<b>Driver: </b>' + li.data('driver'));
    $('#lblVehicle').html('<b>Vehicle No:</b> ' + li.data('vehicle'));
    //var runno = $("#ddlRunNo").val();
    $("#destinationFields").load(URL + "_UnassignedOrderList/0" + "?runno=" + run);
    setTimeout(function () {
        loadingOnElementStop('destinationFields');
    }, 800);
}

//get selected order's short detail
function GetOrderShortDetail(ctrl) {
    var type = $(ctrl).closest('div.ddorderList').data('type');
    if (type != "p") {
        var orderID = $(ctrl).data('id');
        if (orderID != null && orderID != "") {
            loadingOnElementStart('divOrderShortDetail');
            $("#divOrderShortDetail").load(URL + "_OrderShortDetail/" + orderID);
            $("#sourceFields").find('div').removeClass('active');
            $("#destinationFields").find('div').removeClass('active');
            $(ctrl).addClass('active');
            setTimeout(function () {
                loadingOnElementStop('divOrderShortDetail');
            }, 600);
        }
    }
    //$(document).on('keydown click', function (e) {
    //    //  console.log($(ctrl));
    //    console.log('target', e.target);
    //    if (e.type == "keydown") {
    //        if (e.keyCode == 16 || e.keyCode == 17) {
    //            // console.log('this', $(ctrl).data('id'));
    //            //    console.log('divid_2', divID);
    //            console.log('target2', e.target);
    //            $(ctrl).addClass('fc-selected');
    //        }
    //    } else { $('div.fc-field').removeClass('fc-selected'); }
    //});
}
function loadFieldChooser() {
    $("#fieldChooser").load(URL + '_FieldChooser');
}
function setRunNo(ctrl) {
    localStorage.setItem('runno', $(ctrl).val());
    loadFieldChooser();
}
function setActiveDriver(ctrl) {
    var id = $(ctrl).data('id');
    $("#hdnDriverID").val(id);
    $("#liAssignedDrivers").find('li').removeClass('active');
    $(ctrl).addClass('active');
    loadFieldChooser();
}
// assign/unassign order to driver
function AssignOrderToDriver(orderIDs, driverID, type) {
    startLoading();
    console.log(type, orderIDs, driverID);
    if (orderIDs != "") {
        if (driverID != "") {
            $.ajax({
                type: "POST",
                contentType: "json/application",
                dataType: "json",
                url: URL + "AssignOrderToDriver/" + driverID + "?orderIDs=" + orderIDs + "&type=" + type,
                async: true,
                success: function (response) {
                    if (response.Message === "Success") {
                        var del = "Delivery";
                        if (orderIDs.split(',').length > 2) { del = "Deliveries"; }
                        if (response.Result === "Token") {
                            $.alert(del + " assigned successfully. But device token not found for this driver.");
                        } else if (response.Result === "Success") {
                            var txt = del + " assigned successfully. And notification sent to driver.";
                            if (type === "U") { txt = del + " unassigned successfully." }
                            //$.alert(txt);
                        } else if (response.Result == "off") { $.alert(del + " assigned successfully. But notification is off."); }
                        else { $.alert(response.Message); }
                    } else { $.alert(response.Message); }

                    loadFieldChooser();

                    stopLoading();
                },
                error: function (err) {
                    stopLoading();
                    console.log(err);
                }
            })
        } else {
            stopLoading();
            $.alert(
                {
                    title: 'Warning!',
                    content: 'Select any driver!',
                });
        }
    }
    else {
        stopLoading();
        $.alert(
            {
                title: 'Warning!',
                content: 'Select any invoice to drag in the list',
            });
    }
}

function ReAssignOrderToDriver(orderIDs, driverID, type, fun, funtype) {
    startLoading();
    console.log(type, orderIDs, driverID);
    if (orderIDs != "") {
        if (driverID != "") {
            $.ajax({
                type: "POST",
                contentType: "json/application",
                dataType: "json",
                url: URL + "AssignOrderToDriver/" + driverID + "?orderIDs=" + orderIDs + "&type=" + type,
                async: true,
                success: function (response) {
                    if (response.Message === "Success") {
                        var del = "Delivery";
                        if (orderIDs.split(',').length > 2) { del = "Deliveries"; }
                        if (response.Result === "Token") {
                            $.alert(del + " assigned successfully. But device token not found for this driver.");
                        } else if (response.Result === "Success") {
                            var txt = del + " assigned successfully. And notification sent to driver.";
                            if (type === "U") { txt = del + " unassigned successfully." }
                            //$.alert(txt);
                        } else if (response.Result == "off") { $.alert(del + " assigned successfully. But notification is off."); }
                        else { $.alert(response.Message); }
                    } else { $.alert(response.Message); }

                    if (funtype == "L") {
                        $("#ddlDriverLeft").val(driverID);
                        $("#ddlDriverLeft").trigger('change');

                        $("#sourceFields1").load(URL + "_AssignedOrders/" + driverID);
                        $("#destinationFields1").load(URL + "_AssignedOrders/" + fun);
                    }
                    else if (funtype == "R") {
                        $("#ddlDriverRight").val(driverID);
                        $("#ddlDriverRight").trigger('change');

                        $("#sourceFields1").load(URL + "_AssignedOrders/" + fun);
                        $("#destinationFields1").load(URL + "_AssignedOrders/" + driverID);
                    }

                    stopLoading();
                },
                error: function (err) {
                    stopLoading();
                    console.log(err);
                }
            })
        } else {
            stopLoading();
            $.alert(
                {
                    title: 'Warning!',
                    content: 'Select any driver!',
                });
        }
    }
    else {
        stopLoading();
        $.alert(
            {
                title: 'Warning!',
                content: 'Select any invoice to drag in the list',
            });
    }
}

//change order's delivery date
function setOrderDelDate(ctrl) {
    var orderID = $(ctrl).data('id');
    var delDate = $(ctrl).data('deldate');
    $("#divManageDelDate").load(URL + "_ManageDelDate/" + orderID + "?delDate='" + delDate + "'");
}
//open partial page
function openPartialPage(divModal, partialPage) {
    $('#' + divModal).load(URL + partialPage);
}
//
function getOrdersByActiveDriver() {
    var li = $("#liAssignedDrivers").find('li.active').eq(0);
    if (li.length <= 0) {
        setTimeout(function () {
            li = $("#liAssignedDrivers").find('li').eq(0);
            $("#liAssignedDrivers").find('li').eq(0).trigger('click');
            GetAssignedOrders(li);
        }, 1000);
    }
    GetAssignedOrders(li);
}

// New code for warehouse page Started -- Karan

function getRunsByWarhouse(ctrl) {
    var warehouse = $(ctrl).val();

    startLoading();
    $.ajax({
        url: URL + 'GetDeliveryRunByWarehouse',
        type: 'POST',
        data: "{'warehouse':'" + warehouse + "'}",
        contentType: "application/json;charset=utf-8",
        success: function (response) {
            $('#content-2').empty();
            $('#ddlDelSuburb').empty();
            $('#suburbWHRe').empty();

            var html = '';
            $.each(response.Runs, function (key, val) {
                html += '<li class="outer" data-run="' + val + '" onclick="getSuburbsByRuns(this)"><a href="javascript:;" class="head">' + val + '</a></li >';
            });
            $('#content-2').html(html);

            html = '<option value="">All Suburbs</option>';
            $.each(response.Subs, function (key, val) {
                html += '<option  value="' + val.trim() + '" >' + val.trim() + '</option >';
            });
            $('#ddlDelSuburb').html(html);
            $('#suburbWHRe').html(html);
            $('#content-2 li').eq(0).addClass('active');
            // $("#divWhAssigned").load(URL + "_AssignedOrdersSuburbs/0?runno=?suburb=" + $('#suburbWHRe').val());
            $("#divWhAssigned").load(URL + "_AssignedOrdersSuburbs", { id: 0, runno: "", suburb: $('#suburbWHRe').val() });

            $("#destinationFields").load(URL + "_UnassignedOrderList", { id: 0, runno: $('#content-2 li.active').data('run'), suburb: $('#suburbWHRe').val() });
            // $("#lstUnassignedBySuburb").load(URL + "_UnassignedOrderList/0" + "?runno=");

            setTimeout(function () {
                var li = $("#destinationFields").find('div').eq(0);
                GetOrderShortDetail(li);
            }, 200);
            console.log(response);

            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function getSuburbsByRuns(run) {
    $('#content-2 li').removeClass('active');
    $(run).addClass("active");
    var run = $(run).data('run');

    startLoading();
    $.ajax({
        url: URL + 'GetSuburbsByDeliveryRuns',
        type: 'POST',
        data: "{'run':'" + run + "'}",
        contentType: "application/json;charset=utf-8",
        success: function (response) {
            $('#ddlDelSuburb').empty();
            $('#suburbWHRe').empty();

            var html = '<option value="">All Suburbs</option>';
            $.each(response, function (key, val) {
                html += '<option  value="' + val.trim() + '" >' + val.trim() + '</option >';
            });

            $('#ddlDelSuburb').html(html);

            $('#suburbWHRe').html(html);
            $("#divWhAssigned").load(URL + "_AssignedOrdersSuburbs", { id: -1, runno: run, suburb: $('#suburbWHRe').val() });

            $('#lstUnassignedBySuburb').empty();
            $("#lstUnassignedBySuburb").load(URL + "_UnassignedBySuburbs", { id: 0, runno: run, suburb: $('#suburbWHRe').val() });

            $("#destinationFields").load(URL + "_UnassignedOrderList", { id: 0, runno: run, suburb: $('#suburbWHRe').val() });
            // $("#lstUnassignedBySuburb").load(URL + "_UnassignedOrderList/0" + "?runno=");

            setTimeout(function () {
                var li = $("#destinationFields").find('div').eq(0);
                GetOrderShortDetail(li);
            }, 200);
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function reassignWHOrders() {
    //debugger;
    //$('#divManageReassignOrder').load(URL + '_ReassignWarehouseOrders')
    $('#reassignByWarehouse').modal('show');
}

function getAssignedOrdersBySUburbs(ctrl) {
    var run = $('#content-2 li.active').data('run');
    var sub = $(ctrl).val();
    startLoading();
    $("#divWhAssigned").load(URL + "_AssignedOrdersSuburbs", { id: -1, runno: run, suburb: sub.trim() }, function () {
        stopLoading();
    });
}

function getUnAssignedOrdersBySUburbs(ctrl) {
    var sub = $(ctrl).val();
    startLoading();
    $('#lstUnassignedBySuburb').empty();
    //$("#divWhAssigned").load(URL + "_UassignedBySuburbs/0?runno=?suburb=" + $('#suburbWHRe').val());
    $("#lstUnassignedBySuburb").load(URL + "_UnassignedBySuburbs", { id: 0, runno: "", suburb: sub.trim() }, function () {
        stopLoading();
    });
}

function getAssignedOrdersByDriverWH(ctrl, div) {
    var id = $(ctrl).val();
    if (id === '') {
        $("#" + div).empty();
        $("#" + div).html('<span class="spnNoRecord"> No record found!</span>');
    } else {
        startLoading();
        $("#" + div).load(URL + "_UnassignedBySuburbs", { id: id, runno: "", suburb: "" }, function () {
            stopLoading();
        });
    }
}

// Assign or remove the assignment from the orders in the orders popup
function assignRemoveOrdersFromDriver(ctrl) {
    var type = $(ctrl).data('type');

    if ($('#ddlDriverWH').val() === '') {
        growlNotification('Please choose a driver for assignment', 'danger');
    } else {
        var orderIDs = '';
        var driverID = $('#ddlDriverWH').val();
        var typ = 'A';
        if (type === 'AA') {
            var all = $('#lstUnassignedBySuburb').find('div.outer');

            $.each(all, function () {
                orderIDs += $(this).data('id') + ',';
            });
        } else if (type === 'AS') {
            var all = $('#lstUnassignedBySuburb').find('div.active');

            $.each(all, function () {
                orderIDs += $(this).data('id') + ',';
            });
        } else if (type === 'RS') {
            typ = 'U';
            var all = $('#lstDriverOrders').find('div.active');

            $.each(all, function () {
                orderIDs += $(this).data('id') + ',';
            });
        } else {
            typ = 'U';
            var all = $('#lstDriverOrders').find('div.outer');

            $.each(all, function () {
                orderIDs += $(this).data('id') + ',';
            });
        }

        // Call the method to assign or unassign the deliveries.
        AssignOrderToDriver(orderIDs.trim(','), driverID, typ);

        // startLoading();
        setTimeout(function () {
            //Reset the sections in the open modal dialog
            $("#lstDriverOrders").empty();
            $("#lstDriverOrders").load(URL + "_UnassignedBySuburbs", { id: driverID, runno: "", suburb: "" });
            getUnAssignedOrdersBySUburbs($('#suburbWHRe'));

            // Reset the assigned section on the main page
            $('#ddlDelSuburb').val('');
            getAssignedOrdersBySUburbs($('#ddlDelSuburb'));

            // Update the unassigned list on the main page
            $("#destinationFields").load(URL + "_UnassignedOrderList", { id: 0, runno: "", suburb: "" });
        }, 600);
    }
}

function toggleClass(ctrl) {
    $(ctrl).toggleClass('active');
}