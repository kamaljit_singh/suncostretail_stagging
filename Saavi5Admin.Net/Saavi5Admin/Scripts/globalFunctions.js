﻿$(function () {
    /**
            * Custom validator for contains at least one lower-case letter
           */
    $.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
        return this.optional(element) || /[a-z]+/.test(value);
    }, "Must have at least one lowercase letter");

    /**
     * Custom validator for contains at least one upper-case letter.
     */
    $.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
        return this.optional(element) || /[A-Z]+/.test(value);
    }, "Must have at least one uppercase letter");

    /**
     * Custom validator for contains at least one number.
     */
    $.validator.addMethod("atLeastOneNumber", function (value, element) {
        return this.optional(element) || /[0-9]+/.test(value);
    }, "Must have at least one number");

    /**
     * Custom validator for contains at least one symbol.
     */
    $.validator.addMethod("atLeastOneSymbol", function (value, element) {
        return this.optional(element) || /[!@#$%^&*()]+/.test(value);
    }, "Must have at least one symbol");
});

function selectedMenu() {
    $('.mainMenu li a').each(function () {
        if (window.location.href.toLowerCase().indexOf($(this).attr('href').toLowerCase()) !== -1) {
            $(this).parent().addClass('active');
            $(this).parents('li.parent').addClass('active open');
        }
    });
}

//Message: message to display, type: success/failure/warning, title if Any
function jqueryConfirmAlert(message, type, title) {
    $.alert({
        theme: 'supervan',
        content: '<h4>' + message + '</h4>',
        title: title,
        type: type
    });
}

//Global alert-message display function
function alertMessage(type, message, element) {
    var icon = '';
    if (type === 'Success') {
        icon = 'mdi-check'
    } else if (type === 'Danger') {
        icon = 'mdi-close-circle-o';
    } else {
        icon = 'mdi-alert-triangle';
    }

    var html = '<div role="alert" class="alert alert-contrast alert-' + type.toLowerCase() + ' alert-dismissible">'
        + '<div class="icon"><span class="mdi ' + icon + '"></span></div>'
        + '<div class="message">'
        + '<button type="button" data-dismiss="alert" aria-label="Close" class="close">'
        + '<span aria-hidden="true" class="mdi mdi-close"></span></button>'
        + '<strong>' + type + '!</strong> '
        + message
        + '</div></div>';

    $('#' + element).html(html);

    setTimeout(function () {
        $('#' + element).html('');
    }, 8000)
}

// global form validation method
function validateForm(form) {
    $('#' + form).validate({
        errorElement: 'span',
        ignore: ".ignore",
        errorClass: 'help-block help-block-error',
        focusInvalid: true,
        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        invalidHandler: function (event, validator) {
            stopLoading();
        },
    });
}

function resetForm(form) {
    $('#' + form)[0].reset();
    var validator = $("#" + form).validate();
    validator.resetForm();

    return false;
}

//start the loading animation on a container
function startLoading() {
    $.blockUI({
        message: $('#divLoading'),
        css: {
            border: 'none',
            backgroundColor: 'transparent',
            cursor: 'wait',
            zIndex: 9999
        },
        overlayCSS: {
            zIndex: 9999
        }
    });
}

//Stop the loading element on a container
function stopLoading() {
    $.unblockUI();
}

// Start loading on a particular element
function loadingOnElementStart(elem) {
    $('#' + elem).block({
        message: $('#divLoading'),//'<h1>Processing</h1>',
        css: {
            border: 'none',
            backgroundColor: 'transparent',
            cursor: 'wait'
        }
    });
}

// Stop loading on a particular element
function loadingOnElementStop(elem) {
    $('#' + elem).unblock();
}

//scroll to top of the page after a successfull form post.
function scrollToTop() {
    $('html, body').animate({ scrollTop: 0 }, 500);
}

//jquery confirm simple alert dialog
function showAlert(msg, title) {
    $.alert({
        theme: 'supervan',
        content: msg,
        title: title
    });
}

$('.number').keydown(function (event) {
    debugger;
    //if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9) {
    //}
    //else {
    //    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
    //        event.preventDefault();
    //    }
    //}
});

////bind browsed image
//function readURL(fileName, imgHolder) {
//    if (fileName.files && fileName.files[0]) {
//        var reader = new FileReader();
//        var size = Math.floor(fileName.files[0].size / 1000000);

//        if (size > 4) {
//            alert('Please choose an image with size less than 4 MB.');
//            return false;
//        }
//        reader.onload = function (e) {
//            $('#' + imgHolder)
//                .attr('src', e.target.result)
//                .height(40);
//        };
//        reader.readAsDataURL(fileName.files[0]);
//    }
//};
//bind browsed image
function readURL(fileName, imgHolder) {
    console.log(fileName.files[0].size);
    if (fileName.files && fileName.files[0]) {
        if (fileName.files[0].size > (2 * 1024 * 1024)) {
            alert('Image Selected Is Too Big! Maximum Allowed Size Is 2MB.');
            filename.value = "";
            return false;
        }
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + imgHolder)
                .attr('src', e.target.result)
                .height(40);
        };
        reader.readAsDataURL(fileName.files[0]);
    }
}
function decimals(n, d) {
    if ((typeof n !== 'number') || (typeof d !== 'number'))
        return false;
    n = parseFloat(n) || 0;
    return n.toFixed(d);
}
//set format of price
function formatprice(prc) {
    //var cFormatter = new Intl.NumberFormat('en-AU', {
    //    style: 'currency',
    //    currency: 'AUD'
    //});
    return Math.round((prc * 100) / 100, 2);// cFormatter.format(Math.round(prc * 100) / 100);
    //newprc = newprc + "";
    //var dotIndex = newprc.indexOf(".");
    //if (dotIndex >= 0) {
    //    var afterdot = newprc.substring(dotIndex + 1);
    //    if (afterdot.length === 1) {
    //        newprc = newprc + "0";
    //    }
    //}
    //else {
    //    newprc = newprc + ".00";
    //}
    //return newprc;
}

function growlNotification(message, type, delay) {
    $.bootstrapGrowl(message, {
        ele: 'body', // which element to append to
        type: type, // (null, 'info', 'danger', 'success', 'warning')
        offset: {
            from: 'top',
            amount: 50
        }, // 'top', or 'bottom'
        align: 'center', // ('left', 'right', or 'center')
        width: 'auto', // (integer, or 'auto')
        delay: delay, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
        allow_dismiss: true, // If true then will display a cross to close the popup.
        stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
}

//start the loading animation on a container
function startElementLoading(el) {
    $('#' + el).block({
        message: $('#divLoading'),
        css: {
            border: 'none',
            backgroundColor: 'transparent',
            cursor: 'wait',
            zIndex: 9999
        },
        overlayCSS: {
            zIndex: 9999
        }
    });
}

//start the loading animation on a container
function stopElementLoading(el) {
    $('#' + el).unblock();
}

// Numeric only control handler
jQuery.fn.ForceNumericOnly =
    function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };