﻿// Change url based on Domain
var URL = '/POD/Driver/';
var mainURL = '/Company/';
if (window.location.href.indexOf('saavi.com') !== -1) {
    URL = "/Admin/POD/Driver/";
    mainURL = "/Admin/Company/";
}

//open partial page
function openPartialPage(divModal, partialPage) {
    $('#' + divModal).load(URL + partialPage);
}
//set hidden field value
function seValue(ctrl, hdnID) {
    $("#" + hdnID).val($(ctrl).val());
}
//get driver by id
function GetDriverByID(ctrl) {
    startLoading();
    var userID = $(ctrl).data('id');
    $("#liUser").find('li').removeClass('active');
    $(ctrl).addClass('active');
    $("#divDriverDetail").load(URL + "/_DriverDetail/" + userID);
    setTimeout(function () {
        var vID = $("#VehicleID").val();
        $("#divVehicleList a").removeClass('active');
        $("#divVehicleList a[data-id='" + vID + "'").addClass('active');
        stopLoading();
    }, 800);
}

//Delete user
function deleteUser() {
    var userID = $("#UserID").val();
    if (userID !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected driver?',
            buttons: {
                confirm: function () {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        contentType: 'application/json',
                        url: mainURL + 'DeleteUser',
                        data: "{'userID':" + userID + "}",
                        dataType: "json",
                        async: false,
                        success: function (response) {
                            if (response === "Success") {
                                $("#liUser li[data-id='" + userID + "'").remove();
                                growlNotification('Driver deleted successfully.', 'success', 3000);
                            } else {
                                growlNotification(response, 'warning', 3000);
                            } stopLoading();
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else { growlNotification('Select any driver', 'warning', 3000); }
}
//get vehicle by id
function GetVehicleByID(ctrl) {
    startLoading();
    var userID = $(ctrl).data('id');
    $("#liVehicle").find('li').removeClass('active');
    $(ctrl).addClass('active');
    $("#divVehicleDetail").load(URL + "/_VehicleDetail/" + userID);
    setTimeout(function () {
        var dID = $("#DriverID").val();
        console.log('did', dID);
        $("#divDriverList a").removeClass('active');
        $("#divDriverList a[data-id='" + dID + "'").addClass('active');
        stopLoading();
    }, 800);
}
//
function GetDID(ctrl) {
    var vID = $(ctrl).data('id');
    var dID = $("#UserID").val();
    assignVehicleToDriver(dID, vID, ctrl);
}
//
function GetVID(ctrl) {
    var dID = $(ctrl).data('id');
    var vID = $("#VehicleID").val();
    assignVehicleToDriver(dID, vID, ctrl);
}
//assign vehicle to driver
function assignVehicleToDriver(dID, vID, ctrl) {
    console.log(vID, dID);
    if (vID !== "" || dID !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to assign this vehicle?',
            buttons: {
                confirm: function () {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json",
                        dataType: "json",
                        url: URL + "AssignVehicleToDriver",
                        data: "{'vID':" + vID + ",'dID':" + dID + "}",
                        success: function (response) {
                            if (response === "Success") {
                                growlNotification('Vehicle assigned successfully.', 'success', 3000);
                                $("#divDriverList").find('a').removeClass('active');
                                $("#divVehicleList").find('a').removeClass('active');
                                $(ctrl).addClass('active');
                            } else {
                                growlNotification(response, 'warning', 3000);
                            } stopLoading();
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else { growlNotification('Vehicle/Driver not available!', 'warning', 3000); }
}

//delete vehicle
function deleteVehicle(val) {
    var vID = $("#VehicleID").val();
    var type = val == 1 ? "inactivate" : "activate";
    if (vID !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to ' + type + ' the selected vehicle?',
            buttons: {
                confirm: function () {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        contentType: 'application/json',
                        url: URL + 'DeleteVehicle',
                        data: "{'vehicleID':" + vID + ",'val':" + val + "}",
                        dataType: "json",
                        async: false,
                        success: function (response) {
                            if (response === "Success") {
                                $("#liVehicle li[data-id='" + vID + "'").trigger('click');
                                if (val == 1) {
                                    $("#liVehicle li[data-id='" + vID + "'").addClass('disabled');
                                } else {
                                    $("#liVehicle li[data-id='" + vID + "'").removeClass('disabled');
                                }
                                growlNotification('Vehicle ' + type + ' successfully.', 'success', 3000);
                            } else {
                                growlNotification(response, 'warning', 3000);
                            } stopLoading();
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else { growlNotification('Select any vehicle', 'warning', 3000); }
}

//Get all vehicle
function GetAllDriVeh(ctrl) {
    var txt = $("#txtDriverSearch").val();
    //var model = {
    //    SearchText: $("#txtDriverSearch").val(),
    //    PageNumber: 0,
    //    PageSize: 0
    //}
    //  console.log(model);
    var type = $(ctrl).data('type');
    var li = type == "d" ? "liUser" : "liVehicle";
    var method = type == "d" ? "GetDriverByID" : "GetVehicleByID";
    $.ajax({
        type: "POST",
        contentType: "appliaction/json;",
        dataType: "json",
        url: URL + "GetAllVehicle/" + txt + "?type=" + type,
        // data: '{"id":"' + txt + '","type":"' + type + '"}',
        async: false,
        success: function (response) {
            //  debugger;
            if (response.Message === "Success") {
                var html = '';
                $("#" + li).html('');
                console.log(response.Result.length);
                if (response.Result.length > 0) {
                    $.each(response.Result, function (key, value) {
                        html += '<li class="outer ' + (key === 0 ? "active" : "") + '" data-id="' + value.ID + '" onclick="' + method + '(this);"><a href="javascript:;" class="head">' + value.Name + '</a></li>';
                    });
                    $("#" + li).html(html);
                    var li1 = $("#" + li + " li").eq(0);
                    if (type == "d") { GetDriverByID(li1); }
                    else {
                        GetVehicleByID(li1);
                    }
                } else { $("#" + li).html('No record found!'); }
            } else { growlNotification(response.Message, "warning", 3000); }
        },
        error: function (response) {
            console.log(JSON.stringify(response));
        }
    });
}
