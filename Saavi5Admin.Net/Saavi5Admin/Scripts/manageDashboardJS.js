﻿// Change url based on Domain
var URL = '/POD/Home/';
if (window.location.href.indexOf('saavi.com') !== -1) {
    URL = "/Admin/POD/Home/";
}

//
function getDeliveryIssueItems(ctrl) {
    debugger;
    loadingOnElementStart('divOrderHasIssue');
    var orderID = $(ctrl).data('id');
    //  $("#divOrderHasIssue").empty();
    $("#divOrderHasIssue").load(URL + "_DeliveryIssuesItem/" + orderID);
    $("#modalOrderHasIssue").modal('show');
    setTimeout(function () {
        loadingOnElementStop('divOrderHasIssue');
    }, 600);
}
//
function GetallDriverComp() {
    $("#tblDrivers").load(URL + "_Drivers/" + $("#txtDashboardSearch").val());
}

//
function GetVehiclesDeliveryStatus() {
    $("#tblVehicles").load(URL + "_Vehicles/" + $("#txtDashboardSearch").val());
}