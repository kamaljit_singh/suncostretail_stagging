﻿// Change url based on Domain
var URL = '/Company/';
var URLc = '/ManageCustomer/';
if (window.location.href.indexOf('saavi.com') !== -1) {
    URL = "/Admin/Company/";
    URLc = "/Admin/ManageCustomer/";
}

$(function () {
});

// Initialize the scrollbar
function initScrollbar(divID, val) {
    $("#" + divID).mCustomScrollbar({
        theme: "minimal-dark",
        callbacks: {/*user custom callback function on scroll end reached event*/
            onTotalScroll: function () {
                if (val !== 2) {
                    // getCustomersPaging(val, 1);
                    if (window.location.pathname == '/Admin/ManageCustomer/Index') {
                        getCustomersAndUserPaging(val, 1);
                    }
                    else {
                        getCustomersPaging(val, 1);
                    }
                }
            },
            onTotalScrollOffset: 10, /*scroll end reached offset: integer (pixels)*/
        },
        advanced: {
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        scrollButtons: {
            enable: true
        }
    });
}
function searchUsers(type) {
    if (type == "" || type == null) {
        getCustomersPaging(0, 0);
    } else { getRepCustomersPaging(0, 0); }
}

// Fetch customers from the DB. 50 at a time
function getCustomersPaging(v1, val2) {
    var len = $("#liCustomers li").length;
    if (val2 === 1 && len > 0 && len < 50) {
    } else {
        var stext = $("#txtCustSearch").val();
        if (v1 === 1) {
            stext = $("#txtSearchKeyword").val();
        }
        loadingOnElementStart('divCustomers');

        if (val2 === 0) {
            len = 0;
        }
        var model = {
            IsRep: false,
            SearchText: stext,
            PageNumber: (len / 50),
            PageSize: 50
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URL + 'GetCustomersWithPaging',
            data: "{'model':" + JSON.stringify(model) + "}",
            success: function (response) {
                var cust = response.Result;
                if (response.Result.length > 0) {
                    var html = '';
                    $.each(cust, function (key, val) {
                        if (v1 === 0) {
                            html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="getCustomerDetails(this);">' + val.CustomerName + '</a></li>'
                        } else {
                            html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="setCustomerID(this);">' + val.CustomerName + '</a></li>'
                        }
                    });
                    if (val2 === 0) {
                        $("#mCSB_1_container").html('');
                    }
                    $("#mCSB_1_container").append(html);
                    setTimeout(function () {
                        $("#liCustomers").mCustomScrollbar("update");
                    }, 200);
                } else {
                    if (val2 === 0) {
                        $("#mCSB_1_container").html('No record found!');
                    }
                }
                loadingOnElementStop('divCustomers');
            }
        });
    }
}

//get customer details
function getCustomerDetails(ctrl) {
    loadingOnElementStart('divCustMain');

    var customerID = $(ctrl).data('id');
    $('#divCustomerDetail').load(URL + '_CustomerDetail/' + customerID);
    $("#liCustomers").find('a').removeClass('active');
    $(ctrl).addClass('active');
    setTimeout(function () {
        loadingOnElementStop('divCustMain');
    }, 600);
}

//get customer
function getCustomer(val) {
    startLoading();
    if (val === 0) {
        $('#modalCustomer').load(URL + '_ManageCustomer/0');
    } else {
        $('#modalCustomer').load(URL + '_ManageCustomer/' + $("#CustomerID").val());
    }
    setTimeout(function () {
        $("#customerformModal").modal('show');
        stopLoading();
    }, 800);
}

//bind salesmancode
function bindSalesmanCode(code) {
    $.ajax({
        type: "POST",
        url: URL + 'GetSalesmanCode',
        dataType: "json",
        async: false,
        success: function (response) {
            var data = response.Result;
            $("#ddlSalesmanCode").html('');
            $("#ddlSalesmanCode").append($("<option></option>").val('').html('Select'));
            $.each(data, function (key, value) {
                var code = value.SalesmanCode.split(', ');
                if (code.length > 1) {
                    for (var i = 0; i < code.length; i++) {
                        if ($("#ddlSalesmanCode option[value='" + code[i] + "']").length < 1) {
                            $("#ddlSalesmanCode").append($("<option></option>").val(code[i]).html(code[i]));
                        }
                    }
                } else {
                    $("#ddlSalesmanCode").append($("<option></option>").val(value.SalesmanCode).html(value.SalesmanCode));
                }
            });
            if (code > 0) {
                $("#ddlSalesmanCode").val(code);
            }
        }
    });
}

//bind country
function bindCountry(country) {
    $.ajax({
        type: "POST",
        url: URL + 'GetAllCountry',
        dataType: "json",
        async: false,
        success: function (response) {
            var data = response.Result;
            $("#ddlCountry").html('');
            $("#ddlCountry").append($("<option></option>").val('').html('Select'));
            $.each(data, function (key, value) {
                $("#ddlCountry").append($("<option></option>").val(value.CountryID).html(value.CountryName));
            });
            if (parseInt(country) > 0) {
                $("#ddlCountry").val(country);
            }
        }
    });
}

//bind state
function bindState() {
    getID('ddlCountry', 'CountryID');
    var cID = parseInt($("#ddlCountry").val());
    $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: URL + 'GetAllStateByCountryID',
        data: "{'cID':" + cID + "}",
        dataType: "json",
        success: function (response) {
            var data = response.Result;
            $("#ddlState").html('');
            $("#ddlState").append($("<option></option>").val('').html('Select'));
            $.each(data, function (key, value) {
                $("#ddlState").append($("<option></option>").val(value.StateID).html(value.StateName));
            });
            $("#ddlState").val($('#StateID').val());
        }
    });
}

//Bind ddl id's into hidden field
function getID(ddlID, hdnID) {
    $("#" + hdnID).val($("#" + ddlID).val());
}
//
function getSaleCode() {
    $("#SalesmanCode").val('');
    console.log($("#chkSalesmanCode:checked").length);
    if ($("#chkSalesmanCode:checked").length > 0) {
        $("#txtSalesmanCode").val('');
        $("#txtSalesmanCode").show();
        $("#msgSalesmancode").show();
        $("#ddlSalesmanCode").hide();
    }
    else {
        $("#ddlSalesmanCode").show();
        $("#txtSalesmanCode").hide();
        $("#msgSalesmancode").hide();
        var code = [];

        code.push($("#ddlSalesmanCode").val());
        $("#SalesmanCode").val(code);
    }
}
//Delete selected Customer
function deleteCustomer(type) {
    var msg = 'Are you sure you want to delete the selected customer?';
    if (type === 'r') {
        msg = 'Are you sure you want to delete the selected rep?';
    }
    var cID = parseInt($("#CustomerID").val());
    if (cID > 0) {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: msg,
            buttons: {
                confirm: function () {
                    loadingOnElementStart('divCustomers');
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteCustomer',
                        data: "{'id':" + cID + "}",
                        success: function (response) {
                            if (response === "Success") {
                                var ancCtrl = $("#liCustomers").find('a[data-id="' + cID + '"]');
                                var nextAncCtrl = ancCtrl.parent('li').prev('li');
                                if (nextAncCtrl === "undefined" || nextAncCtrl.length === 0) {
                                    nextAncCtrl = ancCtrl.parent('li').next('li');
                                }
                                (nextAncCtrl.find('a')).trigger('onclick');
                                (ancCtrl.parent('li')).remove();
                            }
                            else { $.alert(response); }
                            loadingOnElementStop('divCustomers');
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        $.alert('Select any customer');
    }
}

//Change custoer status
function SetCustActive(ctrl) {
    var cID = $(ctrl).data('id');
    var status = ($(ctrl).data('status')).toString().toLowerCase() === 'true' ? false : true;
    console.log(cID, status);
    startLoading();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'SetCustActive',
        data: "{'id':" + cID + ",'status':'" + status + "'}",
        success: function (response) {
            $(ctrl).data('status', status);
            if (status === false) {
                $(ctrl).addClass('btnRed');
            } else { $(ctrl).removeClass('btnRed'); }
            $(ctrl).find('span').html(status === true ? "Active" : "Inactive");
            stopLoading();
        }
    });
}

// Get New Request List
function GetNewRequests(val) {
    startLoading();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'GetNewRequests',
        success: function (response) {
            var data = response.Result;
            if (val === 0) {
                $("#lblRequestCount").html(response.Result.length);
            } else {
                var html = '';
                if (response.Result.length > 0) {
                    html += '<table class="order-table card-table requests-table"><thead><tr>' +
                        '<th style="text-align:left; padding-left:10px;">Business Name</th>' +
                        '<th style="text-align:left; padding-left:10px;">Contact Name</th>' +
                        '<th style="text-align:left; padding-left:10px;">Email</th>' +
                        '<th>Phone</th>' +
						'<th style="text-align:left; padding-left:10px;">Date</th>' +
                        '<th>Action</th></tr></thead><tbody>';

                    $.each(data, function (key, value) {
                        html += '<tr id="tr_' + value.UserID + '"><td>' + value.BusinessName + '</td><td>' + value.ContactName + '</td>' +
                            '<td>' + value.Email + '</td><td>' + value.Phone + '</td>' + '<td>' + moment(value.CreatedOn).format("DD/MM/YYYY hh:mm a") + '</td >' +
                            '<td><a href="javascript:;"  data-id="' + value.UserID + '" onclick="deleteUser(this);" class="btn-secondary">X</a><a href="javascript:;" '
                            + 'data-id="' + value.UserID + '" data-Name="' + value.BusinessName + '" data-cName="' + value.ContactName + '" data-email="' + value.Email + '" '
                            + 'data-phone="' + value.Phone + '" data-street="' + value.Street + '" data-street2="' + value.Street2 + '" data-postcode="' + value.Postcode + '" '
                            + 'data-abn="' + value.ABN + '" data-suburb="' + value.Suburb + '" onclick="activateUser(this);" class="btn-secondary">Activate</a></td></tr>';
                    });
                    html += '</tbody></table>'
                }
                else { html += ' <h5>No request found!</h5>'; }
                $("#divNewRequest").html(html);
            }
            stopLoading();
        }
    });
}

//get user detail to activate
function activateUser(ctrl) {
    $("#activateuserModal").modal('show');
    $("#hdnUserID").html($(ctrl).attr('data-id'));
    $("#lblCustomerName").html($(ctrl).attr('data-Name'));
    $("#lblPhone").html($(ctrl).attr('data-phone'));
    $("#lblContactName").html($(ctrl).attr('data-cName'));
    $("#lblEmail").html($(ctrl).attr('data-Email'));

    $("#lblAbn").html($(ctrl).data('abn'));
    $("#lblStreet").html($(ctrl).data('street'));
    $("#lblStreet2").html($(ctrl).data('street2'));
    $("#lblSuburb").html($(ctrl).data('suburb'));
    $("#lblpostcode").html($(ctrl).data('postcode'));
    initScrollbar(1);
}

//link user to customer
function linkUserToCustomer() {
    var userID = $("#hdnUserID").html();
    var customerID = $("#hdnCustomerID").html();;
    if (userID !== '' && customerID !== '') {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URL + 'linkUserToCustomer',
            data: "{'userID':" + userID + ",'customerID':" + customerID + "}",
            success: function (response) {
                if (response === "Success") {
                    $('#tr_' + userID).remove();
                    growlNotification('Selected user linked to customer.', 'success', 3000);
                    $("#activateuserModal").modal('hide');
                } else { growlNotification(response, 'warning', 3000); }
            }
        });
    } else { growlNotification('Firstly select customer from list.', 'warning', 3000); }
}
//Set customer id
function setCustomerID(ctrl) {
    $("#hdnCustomerID").html($(ctrl).attr('data-id'));
    $("#liCustomers").find('a').removeClass('active');
    $(ctrl).addClass('active');
}

//open partial page
function openPartialPage(divModal, partialPage) {
    $('#' + divModal).load(URL + partialPage);
}

//bind user by CustomerID
function bindUserbyCustID(cID, type) {
    $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: URL + 'GetUserByCustomerID',
        data: "{'customerID':" + cID + "}",
        dataType: "json",
        async: false,
        success: function (response) {
            var data = response.Result;
            if (data.length > 0) {
                var html = '';
                html += '<table class="table table-bordered table-responsive text-center">' +
                    '<tbody><tr><th style="text-align:center;">No</th><th style="text-align:left;">Email</th><th style="text-align:center;">Is Management</th><th style="text-align:center;">Delete</th></tr>';
                $("#ddlUser").html('');
                $.each(data, function (key, value) {
                    if (type === 'd') {
                        html += '<tr id="tr_' + value.UserID + '"><td> ' + ++key + '</td><td style="text-align:left;">' + value.Email + '</td>' +
                            '<td><div class=""><input type="checkbox" id="chk' + key + '" data-id="' + value.UserID + '" onclick="manageUser(this);" ' + (value.IsManagement.toString() === "false" ? "" : "checked") + '><label for="chk' + key + '"></label>' +
                            '</div></td><td>';
                        if (value.IsPrimaryEmail) {
                            html += 'Primary Email';
                        } else { html += '<a data-id="' + value.UserID + '" data-email="' + value.Email + '" onclick="deleteUser(this);" class="btn btn-xs btn-danger">Delete</a>'; }
                        html += '</td></tr>';
                    } else { $("#ddlUser").append($("<option></option>").val(value.UserID).html(value.Name)); }
                });
                if (type === 'd') {
                    html += '</tbody></table>'
                    $("#divUserList").html(html);
                } else { getFeaturesForUser(); }
            }
        }
    });
}

//Get user's features
function getFeaturesForUser() {
    var userID = $("#ddlUser").val();
    $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: URL + 'GetFeaturesForUser',
        data: "{'userID':" + userID + "}",
        dataType: "json",
        async: false,
        success: function (response) {
            var data = response.Result.AdminFeatures;
            console.log(response.Result.AdminFeatures);
            if (data != null && data.length > 0) {
                var html = '';
                $.each(data, function (key, value) {
                    if (key === 0) {
                        html += '<div class="col-sm-5 col">';
                    }
                    var fName = value.Name.replace('Is', '').replace(/([a-z])([A-Z])/g, '$1 $2');
                    if (fName == 'PONumber') {
                        fName = 'PO Number';
                    } else if (fName == 'Invoice Pdf') {
                        fName = 'Invoice PDF';
                    } else if (fName == 'Allow Decimal') {
                        fName = 'Allow 2 Decimal Places';
                    }
                    html +=
                        '<div class="checkbox text-left right brder_feature">' +
                        '<input onclick="updateFeaturesForUser(this);" type= "checkbox" data-column="' + value.Name + '" id= "chk_' + key + '" ' + (value.Selected === true ? "checked" : "") + '/>' +
                        '<label   for="chk_' + key + '">' + fName + ' <i title="' + value.Description.replace(/"/g, '\\"') + '" data-toggle="tooltip" class="info fa fa-info-circle"> </i>' + '</label></div>';
                    //console.log((data.length / 2).toFixed(0));
                    if ((key + 1).toFixed(0) === (data.length / 2).toFixed(0)) {
                        html += '</div><div class="col-sm-2 col"></div> <div class="col-sm-5 col">';
                    }
                });
                html += '</div>';
                $("#divUserFeatures").html(html);
            }
        }
    });
}

// update user's features
function updateFeaturesForUser(ctrl) {
    startLoading();

    var data = {
        column: $(ctrl).data('column'),
        value: $(ctrl).prop('checked'),
        userID: $("#ddlUser").val()
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: URL + 'UpdateFeatureForUser',
        data: JSON.stringify(data),
        success: function (response) {
            stopLoading();
        }
    });
}

//Manage user
function manageUser(ctrl) {
    startLoading();
    var data = {
        UserID: $(ctrl).data('id'),
        IsManagement: $(ctrl).prop('checked')
    }
    console.log(data);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: URL + 'ManageUser',
        data: JSON.stringify(data),
        success: function (response) {
            stopLoading();
        }
    });
}

//Delete user
function deleteUser(ctrl) {
    var userID = $(ctrl).data('id');
    if (userID !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the user?',
            buttons: {
                confirm: function () {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        contentType: 'application/json',
                        url: URL + 'DeleteUser',
                        data: "{'userID':" + userID + "}",
                        dataType: "json",
                        async: false,
                        success: function (response) {
                            debugger;
                            if (response === "Success") {
                                $("#tr_" + userID).remove();
                                growlNotification('User deleted successfully.', 'success', 3000);
                                console.log($(ctrl).data('email'));
                                $('span:contains("' + $(ctrl).data('email') + '")').remove();
                            } else {
                                growlNotification(response, 'warning', 3000);
                            } stopLoading();
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else { growlNotification('Select any user', 'warning', 3000); }
}

//bind Pantry list
function bindPantryList() {
    var customerID = parseInt($("#CustomerID").val());
    if (customerID !== null) {
        $.ajax({
            type: "POST",
            contentType: 'application/json',
            url: URL + 'GetPantryList',
            data: "{'customerID':" + customerID + "}",
            dataType: "json",
            success: function (response) {
                debugger;
                var data = response.Result;
                $("#ddlPantryList").empty('').append($("<option></option>").val('').html('Select'));
                $.each(data, function (key, value) {
                    $("#ddlPantryList").append($("<option></option>").val(value.PantryListID).html(value.PantryListName));
                });
            }
        });
    }
    else { $.alert('Please select any customer.'); }
}

function getItem() {
    $("#PantryListName").val($("#ddlPantryList :selected").text());
    getID('ddlPantryList', 'PantryListID');
}

/*sale rep*/

//get rep customer details
function getRepCustomerDetails(ctrl) {
    loadingOnElementStart('divCustMain');

    var customerID = $(ctrl).data('id');
    $('#divCustomerDetail').load(URL + '_RepUserDetail/' + customerID);
    $("#liCustomers").find('a').removeClass('active');
    $(ctrl).addClass('active');
    setTimeout(function () {
        loadingOnElementStop('divCustMain');
    }, 300);
}
// Fetch customers from the DB. 50 at a time
function getRepCustomersPaging(v1, val2) {
    var len = $("#liCustomers li").length;
    if (val2 === 1 && len > 0 && len < 50) {
    } else {
        var stext = $("#txtCustSearch").val();
        if (v1 === 1) {
            stext = $("#txtSearchKeyword").val();
        }
        loadingOnElementStart('divCustomers');

        if (val2 === 0) {
            len = 0;
        }
        var model = {
            IsRep: true,
            SearchText: stext,
            PageNumber: (len / 50),
            PageSize: 50
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URL + 'GetCustomersWithPaging',
            data: "{'model':" + JSON.stringify(model) + "}",
            success: function (response) {
                var cust = response.Result;
                if (response.Result.length > 0) {
                    var html = '';
                    $.each(cust, function (key, val) {
                        if (v1 === 0) {
                            html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="getRepCustomerDetails(this);">' + val.CustomerName + '</a></li>'
                        } else {
                            html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="setCustomerID(this);">' + val.CustomerName + '</a></li>'
                        }
                    });
                    if (val2 === 0) {
                        $("#mCSB_1_container").html('');
                    }
                    $("#mCSB_1_container").append(html);
                    setTimeout(function () {
                        $("#liCustomers").mCustomScrollbar("update");
                    }, 200);
                } else {
                    if (val2 === 0) {
                        $("#mCSB_1_container").html('No record found!');
                    }
                } loadingOnElementStop('divCustomers');
            }
        });
    }
}
//get rep customer
function getRepCustomer(val) {
    startLoading();
    if (val === 0) {
        $('#modalCustomer').load(URL + '_ManageRepUser/0');
    } else {
        $('#modalCustomer').load(URL + '_ManageRepUser/' + $("#CustomerID").val());
    }
    setTimeout(function () {
        $("#repcustomerformModal").modal('show');
        stopLoading();
    }, 800);
}

//manage tag names for customer
function ManageCustomerTags() {
    startLoading();
    var custID = $("#CustomerID").val();

    var IsSpecialCategory = $('#IsSpecialCategory').val();

    var tagname = $("#ddlTagNames option:selected").map(function () {
        return $(this).text();
    }).get().join();

    var IsParent = $('#IsParent').val();

    var childlist = [];
    $.each($("#chklistchild input[name='Child']:checked"), function () {
        childlist.push($(this).val());
    });

    var IsChild = $('#IsChild').val();

    var obj = {
        IsSpecialCategory: IsSpecialCategory,
        CustomerID: custID,
        TagNames: tagname,
        ChildList: childlist,
        IsParent: IsParent,
        IsChild: IsChild
    }
    // console.log(obj);
    $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: URL + 'ManageCustomerTags',
        data: "{'obj':" + JSON.stringify(obj) + "}",
        dataType: "json",
        success: function (response) {
            console.log(response);
            stopLoading();
            $('#divCustomerDetail').load(URL + '_CustomerDetail/' + custID);
        },
        error: function (response) {
            console.log(response);
            stopLoading();
        }
    });
}
//get latest added product on dashboard
function searchLatestProduct() {
    var txt = $("#txtLatestProductSearch").val();
    $("#liProduct").load(URL + "_LatestAddedProduct/" + txt);
}

//14092019
function searchUsersAndCustomer(type) {
    if (type == "" || type == null) {
        getCustomersAndUserPaging(0, 0);
    } else { getCustomersAndUserPaging(0, 0); }
}

// Fetch customers from the DB. 50 at a time
function getCustomersAndUserPaging(v1, val2) {
    var len = $("#liCustomers li").length;
    if (val2 === 1 && len > 0 && len < 50) {
    } else {
        var stext = $("#txtCustSearch").val();
        if (v1 === 1) {
            stext = $("#txtSearchKeyword").val();
        }
        loadingOnElementStart('divCustomers');

        if (val2 === 0) {
            len = 0;
        }
        var model = {
            IsRep: false,
            SearchText: stext,
            PageNumber: (len / 50),
            PageSize: 50
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URLc + 'GetCustomersWithPaging',
            data: "{'model':" + JSON.stringify(model) + "}",
            success: function (response) {
                var cust = response.Result;
                if (response.Result.length > 0) {
                    var html = '';
                    $.each(cust, function (key, val) {
                        if (v1 === 0) {
                            html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="getManageCustomerDetails(this);">' + val.CustomerName + '</a></li>'
                        } else {
                            html += '<li><a href="javascript:;" data-id="' + val.CustomerID + '" onclick="setCustomerID(this);">' + val.CustomerName + '</a></li>'
                        }
                    });
                    if (val2 === 0) {
                        $("#mCSB_1_container").html('');
                    }
                    $("#mCSB_1_container").append(html);
                    setTimeout(function () {
                        $("#liCustomers").mCustomScrollbar("update");

                        var li = $("#liCustomers li").eq(0).find('a');
                        getManageCustomerDetails(li);
                    }, 200);
                } else {
                    if (val2 === 0) {
                        $("#mCSB_1_container").html('No record found!');
                    }
                }
                loadingOnElementStop('divCustomers');
            }
        });
    }
}
//get customer details
function getManageCustomerDetails(ctrl) {
    loadingOnElementStart('divCustMain');

    var customerID = $(ctrl).data('id');
    $('#divCustomerDetail').load(URLc + '_CustomerDetail/' + customerID);
    $("#liCustomers").find('a').removeClass('active');
    $(ctrl).addClass('active');
    setTimeout(function () {
        loadingOnElementStop('divCustMain');
    }, 600);
}

//manage tag names for customer
function ManageCustomerTagsDetails() {
    startLoading();
    var custID = $("#CustomerID").val();

    var User = [];
    $(".UserMaster").each(function () {
        //console.log($(this).val());
        var UID = $(this).data('id');
        User.push({
            UserID: $(this).data('id'),
            Email: $(this).val(),
            DefaultPassword: $("#P_" + UID).val(),
        })
    });
    debugger;

    console.log(User);
    var IsRep = $('#IsRep').val();
    var obj = {
        CustomerID: custID,
        IsRep: IsRep,
        UserEmailAndID: User
    }

    $.ajax({
        type: "POST",
        contentType: 'application/json',
        url: URLc + 'ManageCustomer',
        data: "{'obj':" + JSON.stringify(obj) + "}",
        dataType: "json",
        success: function (response) {
            console.log(response);
            stopLoading();
            $('#divCustomerDetail').load(URLc + '_CustomerDetail/' + custID);
        },
        error: function (response) {
            console.log(response);
            stopLoading();
        }
    });
}

//End