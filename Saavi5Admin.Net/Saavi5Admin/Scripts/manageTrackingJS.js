﻿// Change url based on Domain
var URL = '/POD/Tracking/';
if (window.location.href.indexOf('saavi.com') !== -1) {
    URL = "/Admin/POD/Tracking/";
}


//
function GetAllVehicle() {
    startLoading();
    var txt = $("#txtTrackSearch").val();
    $("#liVehicle").load(URL + "_VehicleList/" + txt);
  //  var li = $("#liVehicle").find('li').eq(0);
  //  GetVehicleLocation(li);
    stopLoading();
}
//
function GetVehicleLocation(ctrl) {
    startLoading();
    var deliveryID = $(ctrl).data('id');
    $("#liVehicle").find('li').removeClass('active');
    $(ctrl).addClass('active');
    $("#divVehicleLocation").load(URL + "_VehicleLocation/" + deliveryID)
    stopLoading();
}
