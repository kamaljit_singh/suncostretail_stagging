﻿// Change url based on Domain
var URL = '/Product/';
if (window.location.href.indexOf('saavi.com') !== -1) {
    URL = "/Admin/Product/";
}

// Initialize the scrollbar
function initScrollbar(val, val2) {
    $("#" + val).mCustomScrollbar({
        theme: "minimal-dark",
        callbacks: {/*user custom callback function on scroll end reached event*/
            onTotalScroll: function () {
                if (val2 === 2) {
                    getProductByCatID($("#lblCategoryID").val(), 1);
                }
                if (val2 === 3) {
                    console.log(1);
                    getAllProducts('s');
                }
            },
            onTotalScrollOffset: 10, /*scroll end reached offset: integer (pixels)*/
        },
        advanced: {
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        scrollButtons: {
            enable: true
        }
    });
}

// Fetch category with search
function searchCategories() {
    var searchtext = $("#txtSearch").val();
    loadingOnElementStart('divCategories');

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'searchCategories',
        data: "{'SearchText':'" + searchtext + "'}",
        success: function (response) {
            var cust = response.Result;
            if (response.Result.length > 0) {
                var html = '';
                //  console.log('cust', cust);
                $.each(cust, function (key, val) {
                    html += '<li class="outer">' +
                        '<a onclick="getCatID(this);" href="javascript:;" data-divid="#collapse-div' + key + '"  class="' + (val.SubCategoryList.length > 0 ? "head" : "") + '" data-id="' + val.CategoryID + '">' + val.CategoryName + '</a>' +
                        '<ul id="collapse-div' + key + '" class="collapse-div" style="display:none;">';

                    $.each(val.SubCategoryList, function (key2, val2) {
                        html += '<li role="presentation" class="">' +
                            '<a href="javascript:;" data-id="' + val2.CategoryID + '" onclick="getProductByCatID(' + val2.CategoryID + ',0);">' + val2.CategoryName + '</a></li>';
                    });
                    html += ' </ul></li>';
                });
                $("#mCSB_2_container").html(html);
                setTimeout(function () {
                    $("#liCategories").mCustomScrollbar("update");
                }, 200);

                $('#hdnCatID').val($('a[data-divid="#collapse-div0"]').data('id'));
                var li = $("#collapse-div0 li").eq(0).find('a').data('id');
                setTimeout(function () {
                    getProductByCatID(li, 0);
                }, 600);
            } else { $("#mCSB_2_container").html('No category available.'); }

            loadingOnElementStop('divCategories');
        }
    });
}

//Get All product by category id
function getProductByCatID(id, v) {
    //debugger;
    var searchtext = $("#txtSearch").val();

    $("#lblCategoryID").val(id);
    var len = $("#liProducts li").length;
    if (v === 1 && len > 0 && len < 50) {
    } else {
        loadingOnElementStart('divProduct');
        if (v === 0) { len = 0; }
        if ($("#lblCategoryID").val() !== "") {
            var model = {
                SearchText: searchtext,
                CategoryID: id,
                PageNumber: (len / 50),
                PageSize: 50
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: URL + 'getProductByCatID',
                data: "{'model':" + JSON.stringify(model) + "}",
                success: function (response) {
                    //  console.log(response);
                    var data = response.Result;
                    if (data.length > 0) {
                        var html = '';
                        $.each(data, function (key, val) {
                            html += '<li class=""><a href="javascript:;" data-id="' + val.ProductID + '" onclick=GetProductByID(this);>' + val.ProductName + '</a></li>';
                        });

                        if (v === 0) {
                            $("#mCSB_1_container").html('');
                        }
                        $("#mCSB_1_container").append(html);
                        setTimeout(function () {
                            $("#liProducts").mCustomScrollbar("update");
                        }, 200);
                    } else if (data.length <= 0 && v === 1) { }
                    else { $("#mCSB_1_container").html('No products available.'); }

                    loadingOnElementStop('divProduct');
                    if (v === 0) {
                        $(".collapse-div").find('a').removeClass('active');
                        $('.collapse-div a[data-id="' + id + '"').addClass('active');
                        var pli = $("#liProducts li").eq(0).find('a');
                        setTimeout(function () {
                            GetProductByID(pli);
                        }, 600);
                    }
                }
            });
        }
        else {
            setTimeout(function () {
                $("#mCSB_1_container").html('No products available.');
                loadingOnElementStop('divProduct');
            }, 200);
        }
    }
}

//Get product by id
function GetProductByID(ctrl) {
    loadingOnElementStart('divProductDetail');
    var pID = $(ctrl).data('id');
    if ($(ctrl).data('id') === null || $(ctrl).length <= 0) {
        resetForm();
        loadingOnElementStop('divProductDetail');
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'GetProductByID',
        data: "{'productID':" + pID + "}",
        success: function (response) {
            //   console.log(response);
            if (response.Message === "Success") {
                var data = response.Result;
                var btnActive = $("#productActiveBtn");
                btnActive.attr({ 'data-id': data.ProductID, 'data-status': data.IsActive });
                if (data.IsActive == true) {
                    btnActive.removeClass('btnRed').find('span').text('Active');
                } else {
                    btnActive.addClass('btnRed').find('span').text('Inactive');
                }
                var price = 0;

                $("#lblCode").html(data.ProductCode);
                $("#lblName").html(data.ProductName);
                if (data.Price !== undefined && data.Price > 0) {
                    $("#lblPrice").html("$" + data.Price.toFixed(2) + "/" + data.UnitName);
                } else {
                    $("#lblPrice").html("$0.00");
                }

                $("#lblFilterName").html(data.FilterName);
                $("#lblAvailability").html((data.IsAvailable.toString().toLowerCase() === 'true' ? "Yes" : "No"));
                $("#lblCreatedDate").html(data.CreatedDate === null ? '' : convertDateFormat(data.CreatedDate));
                $("#lblModifiedDate").html(data.ModifiedDate === null ? '' : convertDateFormat(data.ModifiedDate));
                $("#imgThumb").attr('src', '../Content/images/images.jpg');
                $("#hdnProductID").val(data.ProductID);
                $("#lblCategoryName").html(data.CategoryName);
                $("#lblDescription").html(data.ProductDescription);
                if (data.ImageList.length > 0) {
                    var html = "";
                    var carosual = '';
                    $.each(data.ImageList, function (key, value) {
                        html += '<img data-target="#myCarousel" data-slide-to="' + key + '" src="' + value.BaseUrl + "thumb_" + value.ProductImage + '" class="img-thumbnail" alt="" width="25%"/>';
                        carosual += '<div class="item ' + (key == 0 ? "active" : "") + '"><img src="' + value.BaseUrl + value.ProductImage + '" class="img pull-right" alt="" width="70%"/></div>';
                    });

                    $("#myCarousel").addClass('carousel slide');
                    $("#divProductCarosual").html(carosual);
                    $("#divProductThumbimg").html(html);
                } else {
                    $("#divProductThumbimg").html('');
                    $("#divProductCarosual").html('');
                    $("#myCarousel").removeClass();
                }
            } else {
                resetForm(); $("#divProductThumbimg").html('');
                growlNotification(response.Message, 'warning', 3000);
            }
            loadingOnElementStop('divProductDetail');
            $("#liProducts").find('a').removeClass('active');
            $(ctrl).addClass('active');
        }
    });
}

//delete category
function deleteCategory(catID) {
    if (catID !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected category?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteCategory',
                        data: "{'catID':" + catID + "}",
                        success: function (response) {
                            if (response === "Success") {
                                var ancCtrl = $("#liCategories").find('a[data-id="' + catID + '"]');
                                var nextAncCtrl = ancCtrl.parent('li').prev('li');
                                if (nextAncCtrl === "undefined" || nextAncCtrl.length === 0) {
                                    nextAncCtrl = ancCtrl.parent('li').next('li');
                                }
                                (nextAncCtrl.find('a')).trigger('onclick');
                                (ancCtrl.parent('li')).remove();
                            }
                            else { growlNotification(response, 'warning', 3000); }
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        growlNotification('Select any category', 'warning', 3000);
    }
}

//delete sub-category
function deleteSubCategory(scatID) {
    if (scatID !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected sub-category?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteSubCategory',
                        data: "{'scatID':" + scatID + "}",
                        success: function (response) {
                            if (response === "Success") {
                                var ancCtrl = $(".collapse-div").find('a[data-id="' + scatID + '"]');
                                var nextAncCtrl = ancCtrl.parent('li').prev('li');
                                if (nextAncCtrl === "undefined" || nextAncCtrl.length === 0) {
                                    nextAncCtrl = ancCtrl.parent('li').next('li');
                                }
                                (nextAncCtrl.find('a')).trigger('onclick');
                                (ancCtrl.parent('li')).remove();
                            }
                            else { growlNotification(response, 'warning', 3000); }
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        growlNotification('Select any sub-category', 'warning', 3000);
    }
}

//get all subcategories
function bindSubCategories() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'getAllSubCategory',
        async: false,
        success: function (response) {
            if (response.Message === "Success") {
                var subcat = response.Result;
                if (subcat.length > 0) {
                    $("#ddlCategory").empty().append($("<option></option>").val('').html('Select'));
                    $.each(subcat, function (key, value) {
                        $("#ddlCategory").append($("<option></option>").val(value.CategoryID).html(value.CategoryName));
                    });
                }
            }
        }
    });
}

//get all units
function bindAllUnits() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'getAllUnitofMeasurement',
        async: false,
        success: function (response) {
            // console.log(response);
            if (response.Message === "Success") {
                var units = response.Result;
                if (units.length > 0) {
                    $("#ddlUnit").empty().append($("<option></option>").val('').html('Select'));
                    $.each(units, function (key, value) {
                        $("#ddlUnit").append($("<option></option>").val(value.UOMID).html(value.UOMDesc));
                    });
                }
            }
        }
    });
}

//get all filters
function bindAllFilters() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'getAllProductFilter',
        async: false,
        success: function (response) {
            if (response.Message === "Success") {
                var filters = response.Result;
                if (filters.length > 0) {
                    $("#ddlFilter").empty().append($("<option></option>").val('').html('Select'));
                    $.each(filters, function (key, value) {
                        $("#ddlFilter").append($("<option></option>").val(value.FilterID).html(value.FilterName));
                    });
                }
            }
        }
    });
}

//delete product
function deleteProduct(id, v) {
    if (id !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected product?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteProduct',
                        data: "{'id':" + id + "}",
                        success: function (response) {
                            if (response === "Success") {
                                debugger;
                                if (v === 0) { window.location.href = URL + 'Products'; } else {
                                    var ancCtrl = $("#liProducts").find('a[data-id="' + id + '"]');
                                    var nextAncCtrl = ancCtrl.parent('li').prev('li');
                                    if (nextAncCtrl === "undefined" || nextAncCtrl.length === 0) {
                                        nextAncCtrl = ancCtrl.parent('li').next('li');
                                    }
                                    (nextAncCtrl.find('a')).trigger('onclick');
                                    (ancCtrl.parent('li')).remove();
                                }
                            }
                            else { growlNotification(response, 'warning', 3000); }
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        growlNotification('Select any product', 'warning', 3000);
    }
}

//delete filter
function deleteFilter() {
    var id = $("#FilterID").val();
    if (id !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected filter?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteProductFilter',
                        data: "{'id':" + id + "}",
                        success: function (response) {
                            if (response === "Success") {
                                bindAllFilters(); $("#manageFilterModal").modal('hide');
                                growlNotification('Selected filter deleted.', 'success', 3000);
                            }
                            else { growlNotification(response, 'warning', 3000); }
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        growlNotification('Select any filter', 'warning', 3000);
    }
}

//delete filter
function deleteUnit() {
    var id = $("#UOMID").val();
    if (id !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected unit?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteUnitofMeasurement',
                        data: "{'id':" + id + "}",
                        success: function (response) {
                            if (response === "Success") {
                                bindAllUnits(); $("#manageUnitModal").modal('hide');
                                growlNotification('Selected unit deleted', 'success', 3000);
                            }
                            else { growlNotification(response, 'warning', 3000); }
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        growlNotification('Select any unit', 'warning', 3000);
    }
}

//get all products
function getAllProducts(val, type) {
    var searchtext = $("#txtSearch").val();

    var len = $("#divProduct option").length;
    if (val === 0) { len = 0; }
    if (val === 1 && len > 0 && len < 50) {
    } else {
        loadingOnElementStart('divProduct');
        var model = {
            SearchText: searchtext,
            PageNumber: len / 50,
            PageSize: 50,
            Type: type
        }
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: URL + 'GetAllProducts',
            data: "{'model':" + JSON.stringify(model) + "}",
            async: false,
            success: function (response) {
                console.log(response);
                var data = response;
                if (data.length > 0) {
                    if (val === 0) {
                        $("#ddlProducts").empty();
                    }
                    $.each(data, function (key, val) {
                        $("#ddlProducts").append($("<option></option>").val(val.ProductID).html(val.ProductName));
                    });
                }
                else if (data.length === 0 && val === 1) { }
                else if (data.length === 0 && val === 0) { $("#ddlProducts").empty(); }
                // else { $("#ddlProducts").empty(); $("#lblEmpty").html('No products available.'); }
                loadingOnElementStop('divProduct');
            }
        });
    }
}

//get all suggestive products
function getAllNonProducts(type) {
    //debugger;
    //loadingOnElementStart('divNonProduct');

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'GetAllNonProducts',
        data: "{'type':'" + type + "'}",
        async: false,
        success: function (response) {
            //console.log(response);
            var data = response;
            if (data.length > 0) {
                $("#ddlNonProducts").empty();
                $.each(data, function (key, val) {
                    $("#ddlNonProducts").append($("<option></option>").val(val.ProductID).html(val.ProductName));
                });
            }
            //  loadingOnElementStop('divNonProduct');
        }
    });
}

//add product in suggestive list
function addtoSuggestiveList() {
    if ($("#ddlProducts")[0].selectedOptions.length > 0) {
        var data = $("#ddlProducts")[0].selectedOptions;
        $($("#ddlProducts")[0].selectedOptions).each(function () {
            $("#ddlNonProducts option[value='" + this.value + "']").remove();
            $("#ddlNonProducts").append($("<option></option>").val(this.value).html(this.text));
            $("#ddlProducts option[value='" + this.value + "']").remove();
        });
    }
    else { $.alert('Please select atleast one in products to move'); }
}

//delete from suggestive list
function deletefromSuggestiveList() {
    if ($("#ddlNonProducts")[0].selectedOptions.length > 0) {
        var data = $("#ddlNonProducts")[0].selectedOptions;
        $($("#ddlNonProducts")[0].selectedOptions).each(function () {
            $("#ddlProducts").append($("<option></option>").val(this.value).html(this.text));
            $("#ddlNonProducts option[value='" + this.value + "']").remove();
        });
    }
    else { $.alert('Please select atleast one in product items to move'); }
}

//manage suggestive products
function manageSuggestiveProduct(type) {
    startLoading();
    var productArray = [];
    $("#ddlNonProducts >option").each(function () {
        productArray.push(this.value);
    });
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'ManageNonProduct',
        data: "{'productArray':'" + productArray + "','type':'" + type + "'}",
        success: function (response) {
            if (response === 'Success') {
                growlNotification('Product saved successfully.', 'success', 3000);
            } else { growlNotification(response, 'warning', 3000); }
            stopLoading();
        }
    });
}

//Reset Product detail form
function resetForm(form) {
    $("#lblCode").html('');
    $("#lblName").html('');
    $("#lblPrice").html('');
    $("#lblFilterName").html('');
    $("#lblAvailability").html('');
    $("#lblCreatedDate").html('');
    $("#lblModifiedDate").html('');
    $("#imgThumb").removeAttr('src')
}

//fetch value for update main/sub category
function editCategory(val) {
    if (val === 0) {
        if ($('#hdnCatID').val() !== "") {
            openPartialPage('modalManageCategory', '_ManageCategory/' + $('#hdnCatID').val());
        } else { $.alert('Select any category'); }
    } else {
        if ($('#lblCategoryID').val() !== "") {
            openPartialPage('modalManageSubCategory', '_ManageSubCategory/' + $('#lblCategoryID').val());
        } else { $.alert('Select any sub-category!'); }
    }
}

//fetch value for update filter/unit
function fetchValue(type) {
    if (type === 'f') {
        if ($("#ddlFilter").val() === "") { $.alert('Please select a filter.'); } else {
            $("#manageFilterModal").modal('show');
            $("#btnFilterDelete").removeClass('hidden');
            $("#FilterID").val($("#ddlFilter").val());
            $("#FilterName").val($("#ddlFilter :selected").text());
        }
    }
    else if (type === 'fa') {
        $("#manageFilterModal").modal('show');
        $("#FilterID").val('');
        $("#FilterName").val('');
    }
    else if (type === 'u') {
        if ($("#ddlUnit").val() === "") { $.alert('Please select a unit.'); } else {
            $("#manageUnitModal").modal('show');
            $("#btnUnitDelete").removeClass('hidden');
            $("#UOMID").val($("#ddlUnit").val());
            $("#UOMDesc").val($("#ddlUnit :selected").text());
        }
    } else if (type === 'ua') {
        $("#manageUnitModal").modal('show');
        $("#UOMID").val('');
        $("#UOMDesc").val('');
    }
}

//get category id on-click
function getCatID(ctrl) {
    $("#hdnCatID").val($(ctrl).data('id'));
    $($(ctrl).data('divid')).slideToggle();
    $('li.outer a').removeClass('active');
    $(ctrl).toggleClass("active");
}

//Bind ddl id's into hidden field
function getID(ddlID, hdnID) {
    $("#" + hdnID).val($("#" + ddlID).val());
}

//open partial pages
function openPartialPage(divModal, partialPage) {
    $('#' + divModal).load(URL + partialPage);
}

function openLink(val) {
    if (val === 0) {
        window.location.href = URL + 'ManageProduct/' + $("#hdnProductID").val();
    } else { window.location.href = URL + 'ManageProduct/'; }
}

//Convert from json date format
function convertDateFormat(val) {
    var dateString = val.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();

    month = month.toString().length === 1 ? '0' + month.toString() : month.toString();

    var date = day + "/" + month + "/" + year;
    return date;
}

//set price format in input
function setPriceFormat(ctrl) {
    $(ctrl).blur(function () {
        if (isNaN($(ctrl).val())) {
            $(ctrl).val("");
        }
        else if ($(ctrl).val() !== "" && $(ctrl).val() !== "0") {
            $(ctrl).val(formatprice($(ctrl).val()));
        }
    });
}

function SetProductActive(ctrl) {
    var pID = $(ctrl).data('id');
    var status = ($(ctrl).data('status')).toString().toLowerCase() === 'true' ? false : true;
    startLoading();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: URL + 'SetProductActive',
        data: "{'id':" + pID + ",'status':'" + status + "'}",
        success: function (response) {
            $(ctrl).data('status', status);
            if (status === false) {
                $(ctrl).addClass('btnRed');
            } else { $(ctrl).removeClass('btnRed'); }
            $(ctrl).find('span').html(status === true ? "Active" : "Inactive");
            stopLoading();
        }
    });
}
//delete product images
function DeleteProductImage(id, v) {
    if (id !== "") {
        $.confirm({
            theme: 'modern',
            title: 'Confirm!',
            content: 'Are you sure you want to delete the selected Image?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: URL + 'DeleteProductImage',

                        data: "{'id':" + id + "}",
                        success: function (response) {
                            if (response === "Success") {
                                debugger;
                                if (v === 0) { window.location.href = URL + 'Products'; } else {
                                    var ancCtrl = $("#liProducts").find('a[data-id="' + id + '"]');
                                    var nextAncCtrl = ancCtrl.parent('li').prev('li');
                                    if (nextAncCtrl === "undefined" || nextAncCtrl.length === 0) {
                                        nextAncCtrl = ancCtrl.parent('li').next('li');
                                    }
                                    (nextAncCtrl.find('a')).trigger('onclick');
                                    (ancCtrl.parent('li')).remove();
                                }
                                window.location.reload();
                            }
                            else { growlNotification(response, 'warning', 3000); }
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
    }
    else {
        growlNotification('Select any product', 'warning', 3000);
    }
}