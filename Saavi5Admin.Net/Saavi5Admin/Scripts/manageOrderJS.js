﻿// Change url based on DomainddlCutoffTime
var siteURL = '/Order/';
var siteURL1 = "/Payment/";
if (window.location.href.indexOf('saavi.com') !== -1) {
    siteURL = "/Admin/Order/";
    siteURL1 = "/Admin/Payment/";
}

function bindSettingsData() {
    startLoading();
    bindCutoffTime();
    getNotice();

    getPopupMessage();
    getAutoOrderMessage();
    setTimeout(function () {
        stopLoading();
    }, 200);

    //getCutoffData('D', '', 'OC');
    //getCutoffData('D', '', 'OL');
    saveUpdateRemovePromoCode('G', 0);
    //getPermittedDays();
    setTimeout(function () {
        GetCutoffPermittedDays();
    }, 500);
}

// Initialize the scrollbar
function initScrollbar(val, val2, type) {
    $("#" + val).mCustomScrollbar({
        theme: "3d-dark",
        autoHideScrollbar: true,
        callbacks: {/*user custom callback function on scroll end reached event*/
            onTotalScroll: function () {
                GetAllOrders(val2, type);
            },
            onTotalScrollOffset: 0, /*scroll end reached offset: integer (pixels)*/
            //whileScrolling: function () {
            //    $(".dataTables_scrollHead").scrollLeft($("#" + val).mcs.left * -1);
            //},
        },
        advanced: {
            updateOnContentResize: true,
            updateOnImageLoad: false
        },
        scrollButtons: {
            enable: true
        }
    });
}

//bind cut of time in ddl's
function bindCutoffTime() {
    var dt = new Date();
    // $("#txtCuttOffdate").val(moment(dt).add(1, 'days').format('DD/MM/YYYY'));//(dt.getDate() + 1) + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear());
    $("#ddlCutoffTime").empty();
    $("#ddlPermittedDaysFrom").empty();
    $("#ddlPermittedDaysTo").empty();
    for (var hour = 1; hour <= 12; hour++) {
        for (var minute = 0; minute < 60;) {
            var hr = hour < 10 ? "0" + hour : hour;
            var min = minute < 10 ? "0" + minute : minute;
            $("#ddlCutoffTime").append($("<option></option>").html(hr + ":" + min));
            $("#ddlCutoffTime2").append($("<option></option>").html(hr + ":" + min));
            minute = parseInt(minute) + 15;
        }
    }

    for (var hour = 1; hour <= 23; hour++) {
        for (var minute = 0; minute < 60;) {
            var hr = hour < 10 ? "0" + hour : hour;
            var min = minute < 10 ? "0" + minute : minute;
            $("#ddlPermittedDaysFrom").append($("<option></option>").html(hr + ":" + min).val(hr + ":" + min));
            $("#ddlPermittedDaysTo").append($("<option></option>").html(hr + ":" + min).val(hr + ":" + min));
            minute = parseInt(minute) + 15;
        }
    }
}

//get permitted days
function getPermittedDays() {
    var reqdt = $("#txtCuttOffdate").val();
    console.log(reqdt);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: siteURL + 'GetPermittedDays',
        async: false,
        success: function (response) {
            var type = $('#ddlSetting').val();

            if (type === "U") {
                $('#ddlPermittedDaysType').val('U').trigger('onchange');
                $('#ddlPermittedDaysFrom').val(response[0].PermitFrom);
                $('#ddlPermittedDaysTo').val(response[0].PermitTo);
            } else {
                $('#ddlPermittedDaysType').val(type).trigger('onchange');
            }

            //setTimeout(function () {
            //    $('#ddlPermittedDaysFor').val(response[0].GroupID);
            //}, 500);

            var html = '';
            $("#ddlSelectDays").empty();
            $.each(response, function (key, value) {
                //' + (value.IsActive === true ? "checked" : "") + '
                html += '<div><input type="checkbox" data-day="' + value.ShortName + '" data-id="' + (parseInt(value.DayValue) - 1) + '" id="' + value.ShortName + '" /><label for="' + value.ShortName + '">' + value.ShortName + '</label></div>';
                $("#ddlSelectDays").append($("<option></option>").val(value.ID).html(value.DayName));
            });
            $("#wkDays").html(html);
            // $("#txtCuttOffdate").val(reqdt);
            $('#ddlPermittedDaysFor').trigger('onchange');
        }
    });
}

//get notice detail
function getNotice() {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        url: siteURL + 'GetNotice',
        async: false,
        success: function (response) {
            console.log(response)

            $("#ddlIsAvailable").val(response.IsActive.toString());
            var time = response.OrderCutOff.split(' ');
            $("#ddlCutoffTime").val(time[0]);
            $("#ddlTimeZone").val(time[1]);
            $("#txtNoticeMessage").val(response.Description);
            $("#txtNoticeMessageTrigger").text(response.Description);

            $("#chkIsPermitSameDay").prop('checked', response.IsPermitSameDay);
            $("#chkLimitOrder").prop('checked', response.LimitOrdersTo1Month);
            $("#chkAllowPickUp").prop('checked', response.AllowPickup);
            $("#chkEnableCutOffRules").prop('checked', response.EnableCutoffRules);

            if (response.EnableCutoffRules) {
                $('#dviCutoffFeatures').show();
            }

            if (response.AllowPickup) {
                $('#ddlOrderCutoffType').append($("<option></option>").val('U').html('Pick Ups'));
                $('#ddlSelectOrderLimitType').append($("<option></option>").val('U').html('Pick Ups'));
                $('#ddlPermittedDaysType').append($("<option></option>").val('U').html('Pick Ups'));
            }

            setTimeout(function () {
                $('#ddlSetting').val(response.CutoffType).trigger('onchange');
            }, 500);

            //var dt = new Date();
            //var t2 = moment(dt).format('DD/MM/YYYY'); //dt.getHours() + ":" + dt.getMinutes();
            //console.log(moment(dt).format('hh:mm a'), time, $("#txtCuttOffdate").val());
            //console.log(moment(dt).format('HH:MM a'), moment(time, 'hh:mm A').format('HH:MM a'));
            //if (moment(dt, 'hh:mm a').format('HH:MM a') < moment(time, 'hh:mm A').format('HH:MM a')) {
            //    console.log('t1 is smaller');
            //    //set cut of date
            //    $("#txtCuttOffdate").val(moment(dt).format('DD/MM/YYYY'));
            //}
        }
    });
}

//manage permitted days
function managePermittedDays() {
    startLoading();
    var reqdate = $("#txtCuttOffdate").val();
    var reqtime = $("#ddlCutoffTime").val() + " " + $("#ddlTimeZone").val();
    var isPermitSameDay = $("#chkIsPermitSameDay").is(':checked');
    var dayspermitted = "";

    $("#wkDays input").each(function () {
        if ($(this).is(":checked")) {
            dayspermitted += $(this).data('id') + ",";
        }
    });
    dayspermitted = dayspermitted.trim(',');
    if (dayspermitted != "") {
        var model = {
            RequiredDate: reqdate,
            RequiredTime: reqtime,
            //PermittedDays: dayspermitted,
            // IsPermitSameDay: isPermitSameDay,
            // AllowPickup: $("#chkAllowPickUp").prop('checked'),
            //LimitOrdersTo1Month: $("#chkLimitOrder").prop('checked'),
            EnableCutoffRules: $("#chkEnableCutOffRules").prop('checked')
        }
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: siteURL + 'CheckPermittedDays',
            data: "{'model':" + JSON.stringify(model) + "}",
            async: false,
            success: function (response) {
                // console.log(response);
                stopLoading();
                if (response.Message === "Success") {
                    var data = response.Result;
                    if (data.code === "1") {
                        $.confirm({
                            theme: 'modern',
                            title: 'Confirm!',
                            content: 'Your date is conflicting with Permitted Days, next available will be ' + data.NewDate,
                            buttons: {
                                confirm: function () {
                                    manageNotice(data.NewDate);
                                },
                                cancel: function () {
                                }
                            }
                        });
                    }
                    else if (data.code === "2") {
                        $.confirm({
                            theme: 'modern',
                            title: 'Confirm!',
                            content: 'No day is permited. Do you want to continue?',
                            buttons: {
                                confirm: function () {
                                    manageNotice(data.NewDate);
                                },
                                cancel: function () {
                                }
                            }
                        });
                    }
                    else {
                        manageNotice(data.NewDate);
                    }
                }
                else { growlNotification(response.Message, 'warning', 3000); }
            }
        });
    } else { stopLoading(); growlNotification('Select atleast one permitted day!', 'danger', 3000); }
}

//manage notice
function manageNotice(newDate) {
    startLoading();
    var reqdate = $("#txtCuttOffdate").val();
    var reqtime = $("#ddlCutoffTime").val() + " " + $("#ddlTimeZone").val();
    var isDayAvailable = $("#ddlIsAvailable").val();
    var isPermitSameDay = $("#chkIsPermitSameDay").is(':checked');
    var li = [];
    $("#wkDays input").each(function () {
        li.push({ 'ID': $(this).data('id'), 'IsActive': $(this).prop('checked') });
    });
    var model = {
        RequiredDate: reqdate,
        RequiredTime: reqtime,
        IsDayAvailable: isDayAvailable,
        IsPermitSameDay: isPermitSameDay,
        AllowPickup: $("#chkAllowPickUp").prop('checked'),
        LimitOrdersTo1Month: $("#chkLimitOrder").prop('checked'),
        EnableCutoffRules: $("#chkEnableCutOffRules").prop('checked')
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: siteURL + 'ManageNotice',
        data: "{'li':" + JSON.stringify(li) + ",'model':" + JSON.stringify(model) + "}",
        success: function (response) {
            stopLoading();
            if (response === "Success") {
                growlNotification('Notice saved successfully.', 'success', 3000);
                $("#txtCuttOffdate").val(newDate);
                // getNotice();
            }
            else { growlNotification(response.Message, 'warning', 3000); }
        }
    });
}

//set days according order type
function setAutoOrderType(ctrl) {
    if ($(ctrl).val() === "W") {
        $("#ddlSelectDays").prop('disabled', false);
    } else { $("#ddlSelectDays").prop('disabled', true); }
}

//get auto order detail
function getAutoOrderMessage() {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        url: siteURL + 'GetAutoOrder',
        success: function (response) {
            $("#ddlIsAvailable2").val(response.IsActive.toString());
            var time = response.AutoOrderTime.split(' ');
            $("#ddlCutoffTime2").val(time[0]);
            $("#ddlTimeZone2").val(time[1]);
            $("#ddlAutoOrderType").val(response.AutoOrderType);
            $("#ddlSelectDays").val(response.AutoOrderDay);
        }
    });
}

//manage auto order
function manageAutoOrder() {
    startLoading();
    var autoordertype = $("#ddlAutoOrderType").val();
    var autoordertime = $("#ddlCutoffTime2").val() + " " + $("#ddlTimeZone2").val();
    var autoorderday = autoordertype === "W" ? $("#ddlSelectDays").val() : null;
    var isactive = $("#ddlIsAvailable2").val();
    var model = {
        AutoOrderType: autoordertype,
        AutoOrderTime: autoordertime,
        AutoOrderDay: autoorderday,
        IsActive: isactive
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: siteURL + 'ManageAutoOrder',
        data: "{'model':" + JSON.stringify(model) + "}",
        success: function (response) {
            stopLoading();
            if (response === "Success") {
                growlNotification('Auto Order saved successfully.', 'success', 3000);
            }
            else { growlNotification(response.Message, 'warning', 3000); }
        }
    });
}

//manage notice master
function saveNotice() {
    startLoading();
    var desp = $("#txtNoticeMessage").val();
    var model = {
        Description: desp
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: siteURL + 'SaveNotice',
        data: "{'model':" + JSON.stringify(model) + "}",
        success: function (response) {
            stopLoading();
            if (response === "Success") {
                growlNotification('Popup message saved successfully.', 'success', 3000);
                $("#txtNoticeMessageTrigger").text(desp);
                $('#popUpMsgModal .close').trigger('click');
            }
            else { growlNotification(response.Message, 'warning', 3000); }
        }
    });
}

//get popup message detail
function getPopupMessage() {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        url: siteURL + 'GetPopupMessage',
        success: function (response) {
            $("#txtWelcomeMessage").val(response.Description);
            $("#txtWelcomeMessageTrigger").text(response.Description);
        }
    });
}

//manage popup message
function managePopupMessage() {
    startLoading();
    var desp = $("#txtWelcomeMessage").val();
    var model = {
        Description: desp
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: siteURL + 'ManagePopupMessage',
        data: "{'model':" + JSON.stringify(model) + "}",
        success: function (response) {
            stopLoading();
            if (response === "Success") {
                growlNotification('Welcome message saved successfully.', 'success', 3000);
                $("#txtWelcomeMessageTrigger").text(desp);
                $('#welcomeMsgModal .close').trigger('click');
            }
            else { growlNotification(response.Message, 'warning', 3000); }
        }
    });
}

//
function setOrderBy(type, rbID) {
    $("#hdnAscDesc").val(type);

    $('input[name=radioSearch]').prop('checked', false);
    if (rbID !== 'od' && rbID !== 'st') {
        $("#hdntype").val("");
        $("input[id=" + rbID + "]").prop('checked', true);
        GetAllOrders(0);
    }
    else {
        $("#hdntype").val(rbID);
        GetAllOrders(0, rbID);
    }
}

//get all order
function GetAllOrders(v, type) {
    loadingOnElementStart('divOrderList');
    var len = $("#tblbodyList tr").length;
    if (v === 0) { len = 0; }
    var searchtxt = $("#txtOrderSearch").val();
    var sorttype = 'Date';
    var orderType = 0;
    if (type == 'st') {
        sorttype = 'Status';
        orderType = 0;
    }
    else if (type == 'od') {
        sorttype = 'RequiredDate';
        orderType = 0;
    }
    else {
        if ($("#hdntype").val() == 'st') {
            sorttype = 'Status';
            orderType = 0;
        }
        else if ($("#hdntype").val() == 'od') {
            sorttype = 'RequiredDate';
            orderType = 0;
        }
        else {
            sorttype = $("input[name=radioSearch]:checked").val();
            orderType = $("#rbByOrder:checked").length > 0 ? "0" : "1";
        }
    }

    var model = {
        SearchText: searchtxt.length == 0 ? ' ' : searchtxt,
        AscDesc: $("#hdnAscDesc").val(),
        SortType: sorttype,
        OrderType: orderType,
        PageNumber: (len / 50),
        PageSize: 50
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: siteURL + 'GetAllOrders',
        data: "{'model':" + JSON.stringify(model) + "}",
        success: function (response) {
            var data = response.Result;
            if (data != undefined && data.length > 0) {
                var html = ''; var xml = "xml"; var pdf = "pdf", image = '', invoicedimage = '';
                $.each(data, function (key, val) {
                    var invoice = val.Invoice;
                    // image = (val.isPaymentDone == true) ? '<img src="../Content/images/green-tick.png" style="max-width:25% !important;" />' : (val.isPaymentDone == false && val.isAuthCount == true) ? '<img src="../Content/images/not-paid-tick.png" style="max-width:30% !important;" />' : '<b>N/A</b>';
                    image = (val.isRefundDone) ? '<img src="../Content/images/refund.png" title="Refund Done" onclick="refundpaymentOrder(' + val.OrderID + ',' + val.Price + ',' + val.CapturePrice + ',' + val.UserID + ',' + val.CustomerID + ',1,0);" style="max-width:27px !important;cursor:pointer;" />' : (val.isPaymentDone == true) ? '<img src="../Content/images/green-tick.png" onclick="refundpaymentOrder(' + val.OrderID + ',' + val.Price + ',' + val.CapturePrice + ',' + val.UserID + ',' + val.CustomerID + ',0,1);"  style="max-width:25% !important;cursor:pointer;" />' : (val.isPaymentDone == false && val.isAuthCount == true) ? '<img src="../Content/images/not-paid-tick.png" onclick="paymentOrder("' + val.OrderID + '","' + val.Price + '","' + val.CapturePrice + '","' + val.UserID + '");" style="max-width:30% !important;cursor:pointer;" />' : '<b>N/A</b>';
                    invoicedimage = (invoice.toFixed(2) == 0.00) ? '<img src="../Content/images/green-tick.png" style = "max-width:25% !important;" />' : '<img src="../Content/images/rightarrow.jpg" style = "max-width:39px !important;" />';
                    html += '<tr><td><div>';
                    if (type == "d") {
                        html += val.OrderID;
                    } else {
                        html += '<input type="checkbox" id="' + val.OrderID + '"> <label for="' + val.OrderID + '">' + val.OrderID + '</label>';
                    }
                    html += '</div></td>' +
                        '<td>' + convertDateFormat(val.CreatedOn) + '</td>' +
                        '<td>' + convertDateFormat(val.OrderDate) + '</td>' +
                        '<td class="text-left" style="padding-left:10px;">' + val.CustomerName + '</td>' +
                        '<td>$' + decimals(val.ActualPrice, 2) + '</td>' +
                        '<td>' + image + '</td>' +
                        '<td>' + invoicedimage + '</td>' +
                        '<td style="text-align:center">$' + decimals(val.Price, 2) + '</td>';
                    if (type != "d") { html += '<td>' + val.OrderStatus + '</td>'; }
                    html += '<td>';
                    if (val.isAuthCount == true) { html += '<a href="javascript:;" class="btn-secondary" data-id="' + val.OrderID + '"  onclick="paymentOrder(this,' + val.Price + ',' + val.CapturePrice + ',' + val.UserID + ');">Payment</a>'; }
                    //if (type != "d") { html += '<a href="javascript:;" data-id="' + val.OrderID + '"  style="vertical-align:bottom;" data-type="pdf" onclick="downloadOrder(this);"><img src="../Content/images/viewicon.png" height="54" /></a> '; }

                    //Edit order button
                    if (val.isPaymentDone) {
                        html += '<a href="javascript:;" data-id="' + val.OrderID + '" data-type="xml" onclick="GetOrderToEdit(' + val.OrderID + ',true);"><img src="../Content/images/editicon.png" height="48" /></a>';
                    }
                    else {
                        html += '<a href="javascript:;" data-id="' + val.OrderID + '" data-type="xml" onclick="GetOrderToEdit(' + val.OrderID + ',false);"><img src="../Content/images/editicon.png" height="48" /></a>';
                    }
                    //Export order button
                    html += '<a href="javascript:;" data-id="' + val.OrderID + '" data-type="xml" onclick="CheckAndExportOrders(' + val.OrderID + ');"><img src="../Content/images/downloadicon.png" height="48" /></a>';

                    //download order button
                    html += '<a href="javascript:;" data-id="' + val.OrderID + '" data-type="pdf" onclick="downloadOrder(this);"><img src="../Content/images/invoiceicon.png" height="48" /></a>';

                    //Send Invouce button
                    html += '<a href="javascript:;" data-id="' + val.OrderID + '" data-email="' + val.Email + '" data-refund="' + val.isRefundDone + '" onclick="setInvoiceValues(this);"><img src="../Content/images/sendicon.png" height="48" /></a>';

                    //Not yet used
                    html += '<a href="javascript:;" data-id="' + val.OrderID + '" data-type="" onclick=""><img src="../Content/images/intigrateicon.png" height="48" /></a></td></tr>';
                });

                if (v === 0) {
                    $("#tblbodyList").html('');
                }
                $("#tblbodyList").append(html);
                setTimeout(function () {
                    $("#divOrderList").mCustomScrollbar("update");
                }, 200);
            } else if (data != undefined && (data.length <= 0 && v === 1)) { }
            else { $("#tblbodyList").html('No order available.'); }

            //check all checkbox
            checkAll();
            loadingOnElementStop('divOrderList');
        }
    });
}

//Convert from json date format
function convertDateFormat(val) {
    var dateString = val.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();

    month = month.toString().length === 1 ? '0' + month.toString() : month.toString();

    var date = day + "-" + month + "-" + year;
    return date;
}

function validateAndFixNumericValue(ctrl) {
    $(ctrl).blur(function () {
        if (isNaN($(ctrl).val())) {
            $(ctrl).val("");
        }
    });
}
function setPriceFormat(ctrl) {
    $(ctrl).blur(function () {
        if (isNaN($(ctrl).val())) {
            $(ctrl).val("");
        }
        else if ($(ctrl).val() !== "" && $(ctrl).val() !== "0") {
            $(ctrl).val(formatprice($(ctrl).val()));
        }
    });
}
function refundpaymentOrder(orderID, orderTotal, authorizedAmt, UserID, CustomerID, status, click) {
    $('#modalRefundPayment').load(siteURL1 + '_RefundPayment/' + orderID + '?orderTotal=' + orderTotal + '&authorizedAmt=' + authorizedAmt + '&UserID=' + UserID + '&CustomerID=' + CustomerID + '&Status=' + status + '&Click=' + click);
}

//open partial pages
function openPartialPage(divModal, partialPage) {
    $('#' + divModal).load(siteURL + partialPage);
}

//check all checkbox
function checkAll() {
    if ($("#cbHeader").is(':checked')) {
        $("#tblbodyList").find('input[type=checkbox]').prop('checked', true);
    }
    else { $("#tblbodyList").find('input[type=checkbox]').prop('checked', false); }
}
//check count of checked checkoboxes
function checkCount() {
    var totalCount = $('#tblbodyList td').find('input[type="checkbox"]').length;
    var checked = $('#tblbodyList td').find('input[type="checkbox"]:checked').length;

    if (totalCount === checked) {
        $('#cbHeader').prop('checked', true);
    } else {
        $('#cbHeader').prop('checked', false);
    }
}

//function paymentOrder(ctrl, orderTotal, authorizedAmt, UserID) {
//    var orderID = $(ctrl).data('id');
//    $('#modalManagePayment').load(siteURL1 + '_CapturePayment/' + orderID + '?orderTotal=' + orderTotal + "&authorizedAmt=" + authorizedAmt + "&UserID=" + UserID);
//}
function paymentOrder(orderID, orderTotal, authorizedAmt, UserID) {
    $('#modalManagePayment').load(siteURL1 + '_CapturePayment/' + orderID + '?orderTotal=' + orderTotal + "&authorizedAmt=" + authorizedAmt + "&UserID=" + UserID);
}

function refundpaymentOrderNew() {
    startLoading();
    var cartID = $('#hdnCartID').val();
    var CustomerID = $('#hdnCustomerID').val();
    var orderTotal = $('#hdnTotalorderdataAmount').val();
    var status = 0;
    if ($('#hdnIsrefunddone').val() == 'true') {
        status = 1;
    }
    var refundamount = Number.parseFloat($('#hdnDifference').val()).toFixed(2) * -1;
    if ($('#hdnIspaymentdone').val() == 'true') {
        $('#modalRefundPayment').load(siteURL1 + '_RefundPayment/' + cartID + '?orderTotal=' + orderTotal + '&authorizedAmt=' + refundamount + '&UserID=' + 0 + '&CustomerID=' + CustomerID + '&Status=' + status + '&Click=' + 2, function () {
            stopLoading();
        });
    }
    else {
        $('#myModalAlerMessage').modal("hide");
        if ($('#hdnIsrefunddone').val() == 'false') {
            $("#error").html("Please first do the payment");
            $('#myModalAlerMessage').modal("show");
            return;
        }
        stopLoading();
    }
}

function OtherHistory() {
    $('#myModalAlerMessage').modal("hide");
    var cartID = $('#hdnCartID').val();
    startLoading();
    $('#modalOrderPaymentHistory').load(siteURL1 + '_PaymentOrderHistory/' + cartID, function () {
        stopLoading();
    });

    return true;
}
function RefundHistory() {
    $('#myModalAlerMessage').modal("hide");
    var cartID = $('#hdnCartID').val();

    if ($('#hdnIspaymentdone').val() == 'true') {
        startLoading();
        $('#modalRefundPaymentHistory').load(siteURL1 + '_RefundPaymentHistory/' + cartID, function () {
            stopLoading();
        });
    }
}

//download xml file
function downloadOrder(ctrl) {
    var orderID = $(ctrl).data('id');
    var type = $(ctrl).data('type');
    $.ajax({
        url: siteURL + 'GetOrderFileById',
        type: 'POST',
        data: "{'OrderId' : '" + orderID + "','type':'" + type + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.Message === "Success") {
                var filename = response.Result;
                //   console.log(filename);
                if (filename.indexOf(".xml") > 0) {
                    var link = document.createElement("a");
                    link.setAttribute('target', '_blank');
                    link.setAttribute("href", filename);
                    document.body.appendChild(link);
                    link.click();
                } else if (filename.indexOf(".pdf") > 0) {
                    startLoading();
                    $("#ifrmPdfFile").attr("src", filename + "#zoom=80");
                    $("#orderDetailModal").modal('show');
                    setTimeout(function () {
                        stopLoading();
                    }, 500);
                }
                else {
                    $.alert("Sorry, no invoice is available at this time. Please try again later.");
                }
            }
            else {
                growlNotification(response.Message, 'warning', 3000);
            }
        }
    });
}

//download selected orders
function downloadSelectedOrders() {
    var orderIDs = [];
    if ($('#tblbodyList td').find('input[type="checkbox"]:checked').length > 0) {
        $('#tblbodyList td').find('input[type="checkbox"]:checked').each(function () {
            orderIDs.push($(this).attr('id'));
        });
    }
    else { growlNotification('You need to select the record to download.', 'warning', 3000); }
    if (orderIDs.length > 0) {
        $.ajax({
            //  url: siteURL + 'DownloadSelectedOrders',
            type: 'POST',
            data: "{'orderIDs' : '" + orderIDs + "'}",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response === "Success") {
                }
                else {
                    growlNotification(response, 'warning', 3000);
                }
            }
        });
    }
}

function fetchPageData(ctrl) {
    var pageName = $(ctrl).val();
    if (pageName !== '') {
        $.ajax({
            url: siteURL + 'GetPageHelpInfo',
            type: 'POST',
            data: "{'pageName' : '" + pageName + "'}",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.Message === "Success") {
                    // $('#txtPageHelp').val(response.Description);
                    CKEDITOR.instances.txtpageHelp.setData('<p>' + response.Description + '</p>');
                    $('#txtPageHelpModal').html(response.Description);
                }
                else {
                    growlNotification(response.Message, 'warning', 3000);
                }
            }
        });
    }
}

function managePageHelp() {
    var pageName = $('#ddlPages').val();

    if (pageName !== '' && CKEDITOR.instances.txtpageHelp.getData() !== '') {
        var desc = CKEDITOR.instances.txtpageHelp.getData();

        $.ajax({
            url: siteURL + 'UpdatePageHelpInfo',
            type: 'POST',
            data: "{'pageName' : '" + pageName + "', 'description':'" + desc + "'}",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.Message === "Success") {
                    // $('#txtPageHelp').val(response.Description);
                    CKEDITOR.instances.txtpageHelp.setData(response.Description);
                    $('#txtPageHelpModal').html(response.Description);
                    var value = $("#ddlPages option:selected");

                    $('#lblPageName').text(value.text());
                    $('#pageHelpModal').modal('hide');
                }
                else {
                    growlNotification(response.Message, 'warning', 3000);
                }
            }
        });
    }
}
function setPickUpAddessFunctionaity() {
    var val = $('#ddlDelType').val();
    $('#ddlAddressType').val("0");
    if (val == "U") {
        $('#pickAddress').show();
        $('#pickAddress1').show();
        $('#ddlAddressType').val("0");
    } else if (val == "D") {
        $('#ddlAddressType').val("0");
        $('#pickAddress').hide();
        $('#pickAddress1').hide();
    }
    else {
        $('#ddlAddressType').val("0");
        $('#pickAddress').hide();
        $('#pickAddress1').hide();
    }
}

function getPickAddress(type) {
    startLoading();

    $.ajax({
        url: siteURL + 'GetCutOffData',
        type: 'POST',
        data: "{'type' : '" + type + "', 'states' : '" + null + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            var res = response.Data;
            $('#ddlAddressType').empty();
            $('#ddlAddressType').append($("<option></option>").val('').html('- Select -'));
            $.each(res, function (idx, val) {
                $('#ddlAddressType').append($("<option></option>").val(val.GID).html(val.Type));
            });

            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function setCutOffFunctionaity(ctrl) {
    var val = $(ctrl).val();
    var showStates = false;
    if (val == "U") {
        $('#btnCutOffType').data('type', 'U');
        $('#btnCutOffType').text('Manage Pick Ups');
    } else if (val == "D") {
        $('#btnCutOffType').data('type', 'D');
        $('#btnCutOffType').text('Manage Run');
    } else if (val == "S") {
        $('#btnCutOffType').data('type', 'S');
        $('#btnCutOffType').text('Manage Suburb');
        $('#lblStatesCaption').text('Use Suburbs in:');
    } else {
        $('#btnCutOffType').data('type', 'P');
        $('#btnCutOffType').text('Manage PostCodes');
        $('#lblStatesCaption').text('Enable these states:');
    }

    //if ($('#chkEnableCutOffRules').prop('checked')) {
    //    $('#ddlSelectOrderLimitType').val(val).trigger('onchange').prop('disabled', true);
    //    $('#ddlPermittedDaysType').val(val).trigger('onchange').prop('disabled', true);
    //}
    getCutoffData(val, '', 'OC');
};

function showHideCutoffOptions(ctrl) {
    var val = $(ctrl).val();
    if (val === 'true') {
    }
}

function showHideRules(ctrl) {
    var val = $(ctrl).prop('checked');

    if (val) {
        $('#dviCutoffFeatures').show();
        $('#lblCutOffTime').text('Cut Off Time:');
    } else {
        $('#dviCutoffFeatures').hide();
        $('#lblCutOffTime').text('Global Cut Off Time:');
    }
}

function showHidePermittedDaysData(ctrl) {
    var val = $(ctrl).val();

    if (val == "U") {
        $('.pickUp').show();
    } else if (val == "D") {
        $('.pickUp').hide();
        // Add code to fetch runs
    } else if (val == "S") {
        $('.pickUp').hide();
        // Add code to fetch suburbs
    } else {
        $('.pickUp').hide();
        //Add code to fetch post codes
    }
}

function getCutoffData(type, ctrl, section, states) {
    startLoading();
    if (type === '') {
        type = $(ctrl).val();
    }

    if (states === undefined) {
        states = '';
    }

    $.ajax({
        url: siteURL + 'GetCutOffData',
        type: 'POST',
        data: "{'type' : '" + type + "', 'states' : '" + states + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            var res = response.Data;
            if (section === 'OL' && states === '') {
                $('#ddlOrderLimitFor').empty();
                $('#ddlOrderLimitFor').append($("<option></option>").val('').html('- Select -'));
                $.each(res, function (idx, val) {
                    $('#ddlOrderLimitFor').append($("<option></option>").val(val.GID + ";" + val.Limit).html(val.Type));
                });
                $('#txtOrderLimit').val('');
            }
            else if (section === 'OC' && states === '') {
                $('#ddlCutOffFor').empty();

                $('#ddlCutOffFor').append($("<option></option>").val('').html('- Select -'));
                $.each(res, function (idx, val) {
                    $('#ddlCutOffFor').append($("<option></option>").val(val.GID + ";" + val.Cutoff + ";" + val.CutoffDate).html(val.Type));
                });
            } else if (section === 'PD' && states === '') {
                $('#ddlPermittedDaysFor').empty();

                $('#ddlPermittedDaysFor').append($("<option></option>").val('0').html('- Select -'));
                $.each(res, function (idx, val) {
                    $('#ddlPermittedDaysFor').append($("<option></option>").val(val.GID).html(val.Type));
                });
            }

            if (section === 'M') {
                $('#ddlCutOffForModel').empty();
                $.each(res, function (idx, val) {
                    $('#ddlCutOffForModel').append($("<option></option>").val(val.Type).html(val.Type));
                });
            }
            if (section === 'MS') {
                $('#ddlAllSubrubs').empty();
                $.each(res, function (idx, val) {
                    $('#ddlAllSubrubs').append($("<option></option>").val(val.Type).html(val.Type));
                });
            }
            if (section === 'MP') {
                $('#ddlAllPostCode').empty();
                $.each(res, function (idx, val) {
                    $('#ddlAllPostCode').append($("<option></option>").val(val.Type).html(val.Type));
                });
            }

            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function saveOrderLimitValue() {
    var type = $('#ddlSelectOrderLimitType').val();
    var value = $("#ddlOrderLimitFor option:selected").text(); //$('#ddlOrderLimitFor').val();
    var orderLimit = $('#txtOrderLimit').val();
    var id = $("#ddlOrderLimitFor").val().split(';')[0];
    if (value === '') {
        alert('Please set order limit for');
        return false;
    }

    if (orderLimit === '') {
        alert('Please set order limit');
        return false;
    }

    startLoading();

    var model = {
        Type: type,
        Value: value,
        Limit: orderLimit,
        ID: id
    };

    $.ajax({
        url: siteURL + 'SetOrderLimit',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            var res = response.Data;
            $("#ddlOrderLimitFor option:selected").val(id + ';' + orderLimit);
            stopLoading();
        }
    });
}

function setLimit(ctrl) {
    var val = $(ctrl).val().split(';')[1];
    if (val == "0") {
        val = '';
    }
    $('#txtOrderLimit').val(val);
}

function setCutOff(ctrl) {
    var val = $(ctrl).val().split(';')[1];
    var date = $(ctrl).val().split(';')[2];
    if (val !== "") {
        var time = val.split(' ')[0];
        var zone = val.split(' ')[1];

        $('#ddlCutoffTime').val(time);
        $('#ddlTimeZone').val(zone);
    }
    if (date === "null") {
        $('#txtCuttOffdate').val('');
    } else {
        $('#txtCuttOffdate').val(date);
    }
}

function fetchDataBasedOnState() {
    var selected = '';
    $('#divStates input:checked').each(function () {
        selected += $(this).data('state') + ",";
    });

    console.log(selected);
    var type = $('#ddlOrderCutoffType').val();
    getCutoffData(type, '', 'OC', selected);
}

function saveCutOffRules() {
    if ($('#chkEnableCutOffRules').prop('checked') === false) {
        managePermittedDays();
    } else {
        var type = $('#ddlOrderCutoffType').val();
        var value = $("#ddlCutOffFor option:selected").text();
        var id = $("#ddlCutOffFor").val().split(';')[0];

        var cutOffTime = $('#ddlCutoffTime').val() + " " + $('#ddlTimeZone').val();

        if (value === '- Select -') {
            alert('Please select a value for selected cut off type');
            return false;
        }

        //console.log(type + " " + value + " " + cutOffTime);
        var model = {
            Type: type,
            Value: value,
            CutoffTime: cutOffTime,
            ID: id
        };
        startLoading();
        $.ajax({
            url: siteURL + 'SaveOrderCutOff',
            type: 'POST',
            data: "{'model':" + JSON.stringify(model) + "}",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                console.log(response);
                $('#txtCuttOffdate').val(response.CutOff);
                stopLoading();
            }
        });
    }
}

function openModal(ctrl) {
    var type = $(ctrl).data('type');

    if (type === 'U') {
        $('#managepickupModal').modal('show');
    } else if (type === 'P') {
        $('#managepostcodeModal').modal('show');
    } else if (type === 'S') {
        $('#managesuburbsModal').modal('show');
    } else {//D
        $('#managedeliveryrunsModal').modal('show');
    }
    BindAllGroupList(type);
}

function togglePromocode(ctrl) {
    var enable = $(ctrl).prop('checked');

    if (enable) {
        $('#promoCodeList').attr('disabled', false);
    } else {
        $('#promoCodeList').attr('disabled', true);
    }

    startElementLoading('divPromocode');
    var data = {
        column: 'IsPromoCode',
        value: enable,
        type: 'main'
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: siteURL + 'UpdateManageFeature',
        data: JSON.stringify(data),
        success: function (response) {
            stopElementLoading('divPromocode');
        }
    });
}

function getPromoCodeData() {
    startLoading();
    $.ajax({
        url: siteURL + 'ManagePromoCodes',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function saveUpdateRemovePromoCode(type, promoID) {
    var model = {};
    if (type === 'G') {
        model = {
            Type: 'G',
            PromoID: promoID[0]
        }
    } else if (type === 'D') {
        var promoID = $('#promoCodeList').val().length > 0 ? $('#promoCodeList').val()[0] : 0;

        if (promoID > 0) {
            model = {
                Type: 'D',
                PromoID: promoID
            }
        } else {
            alert('No Promocode selected');
            return false;
        }
    }
    else {
        var promoCode = $('#txtPromoCode').val();
        var promoType = $('#ddlPromoType').val();
        var promoValue = $('#txtPromoValue').val();
        var promoFrom = $('#txtPromoFrom').val();
        var promoTo = $('#txtPromoTo').val();
        var redemptions = $('#txtPromoRedemptions').val();
        var minCartValue = $('#txtMinCartValue').val();
        var products = $('#ddlPromoProducts').val();
        var limitPerCust = $('#chkLimitPerCustomer').prop('checked');

        var customer = $('#ddlPromoCustomer').val();
        var promoID = $('#promoCodeList').val().length > 0 ? $('#promoCodeList').val()[0] : 0;
        var exist = '';
        $('#promoCodeList option').filter(function () {
            exist = $(this).text() === promoCode.trim();
        });

        if (promoCode === '' || promoValue === '' || promoFrom === '' || promoTo === '') {
            alert('Please fill in all the fields marked as *');
            return false;
        }

        if (exist && promoID === 0) {
            alert('The promo code name is already in use, please use a different code.');
            return false;
        };

        model = {
            PromoCode: promoCode,
            PromoType: promoType,
            PromoValue: promoValue,
            From: promoFrom,
            To: promoTo,
            Redemptions: redemptions,
            MinCartVal: minCartValue,
            Type: type,
            PromoID: promoID,
            CustomerID: customer,
            Products: products,
            LimitPerCust: limitPerCust
        };
    }

    startElementLoading('divPromocode');
    $.ajax({
        url: siteURL + 'ManagePromoCodes',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);

            if (model.Type === 'G') {
                if (promoID == 0) {
                    $('#ddlPromoCustomer').empty();
                    $('#ddlPromoCustomer').append($("<option></option>").val('0').html('- Select Customer -'));
                    $.each(response.Customers, function (idx, val) {
                        $('#ddlPromoCustomer').append($("<option></option>").val(val.CustomerID).html(val.CustomerName));
                    });

                    $('#ddlPromoProducts').empty();
                    $('#promoCodeList').empty();
                    // $('#promoCodeList').append($("<option></option>").val('').html('- Select Customer -'));
                    $.each(response.PromoCodes, function (idx, val) {
                        $('#promoCodeList').append($("<option></option>").val(val.ID).html(val.Code));
                    });

                    $.each(response.Products, function (idx, val) {
                        $('#ddlPromoProducts').append($("<option></option>").val(val.ProductID).html(val.ProductName));
                    });
                } else {
                    var promo = response.Promo;
                    $('#ddlPromoCustomer').val(promo.CustomerID);
                    $('#txtPromoCode').val(promo.Code);
                    $('#ddlPromoType').val(promo.CodeType == "Discount" ? 'D' : 'V');
                    $('#txtPromoValue').val(promo.Value);
                    $('#txtPromoFrom').val(moment(promo.StartDate).format('DD/MM/YYYY'));
                    $('#txtPromoTo').val(moment(promo.EndDate).format('DD/MM/YYYY'));
                    $('#txtPromoRedemptions').val(promo.Redemptions);
                    $('#txtMinCartValue').val(promo.MinCartValue);
                    $('#ddlPromoProducts').val(response.Prods);
                    $('#chkLimitPerCustomer').prop('checked', promo.Limit1PerCustomer);
                }
                stopElementLoading('divPromocode');
            } else {
                saveUpdateRemovePromoCode('G', 0);
                resetPromoValues();
            }
        },
        error: function () {
            stopElementLoading('divPromocode');
        }
    });
}

function getPromoCodeData(ctrl) {
    var code = $(ctrl).val();
    saveUpdateRemovePromoCode('G', code);
    // alert(code);
}

function checkNUmeric(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function resetPromoValues() {
    $('#ddlPromoCustomer').val('0');
    $('#promoCodeList').val('');
    $('#ddlPromoProducts').val('');
    $('#txtPromoCode').val('');
    $('#ddlPromoType').val('D');
    $('#txtPromoValue').val('');
    $('#txtPromoFrom').val('');
    $('#txtPromoTo').val('');
    $('#txtPromoRedemptions').val('');
    $('#txtMinCartValue').val('');
    $('#chkLimitPerCustomer').prop('checked', false);
}

function savePermittedDays() {
    if ($('#chkEnableCutOffRules').prop('checked') === false) {
        managePermittedDays();
    } else {
        var permittedDayType = $('#ddlPermittedDaysType').val();
        var from = '';
        var to = '';
        var id = $('#ddlPermittedDaysFor').val();

        if (permittedDayType === 'U') {
            from = $('#ddlPermittedDaysFrom').val();
            to = $('#ddlPermittedDaysTo').val();
        }

        var li = [];
        $("#wkDays input").each(function () {
            li.push({ 'ID': $(this).data('id'), 'IsActive': $(this).prop('checked') });
        });
        var model = {
            li: li,
            PermitBy: permittedDayType,
            From: from,
            To: to,
            ID: id
        };
        startLoading();
        $.ajax({
            url: siteURL + 'ManageCutOffPermittedDays',
            type: 'POST',
            data: "{'model':" + JSON.stringify(model) + "}",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                console.log(response);
                stopLoading();
            },
            error: function () {
                stopLoading();
            }
        });
    }
}

function updateIndividualFeatures(ctrl) {
    var col = $(ctrl).data('col');
    var val = false;

    if ($(ctrl).attr('id') === 'ddlIsAvailable') {
        if ($(ctrl).val() === 'true') {
            val = true;
        }
    } else {
        val = $(ctrl).prop('checked');
    }

    startLoading();
    $.ajax({
        url: siteURL + 'UpdateIndividualFeatures',
        type: 'POST',
        data: "{'column':'" + col + "', 'value': " + val + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (col === 'AllowPickup' && val === false) {
                $("#ddlSelectOrderLimitType option[value='U']").remove();
                $("#ddlOrderCutoffType option[value='U']").remove();
                $("#ddlPermittedDaysType option[value='U']").remove();
            } else if (col === 'AllowPickup' && val === true) {
                $('#ddlOrderCutoffType').append($("<option></option>").val('U').html('Pick Ups'));
                $('#ddlSelectOrderLimitType').append($("<option></option>").val('U').html('Pick Ups'));
                $('#ddlPermittedDaysType').append($("<option></option>").val('U').html('Pick Ups'));
            }

            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

// Code By Venu Start
function ManageRunPoppop(ctrl) {
    startLoading();
    var type = $(ctrl).data('type');
    var name = '';
    var add = '';
    var days = '';
    var id = 0;

    if (type === 'D') {
        name = $('#txtDeliveryRunName').val();
        add = $('#txtDeliveryRunAddress').val();
        days = $('#txtDeliveryRunDay').val();
    } else if (type === 'P') {
        name = $('#txtPostCodeName').val();
        add = $('#txtPostCodeWarehouse').val();
        days = $('#txtPostCodeDay').val();
    } else if (type === 'S') {
        name = $('#txtSubrubName').val();
        add = $('#txtSubrubWarehouse').val();
        days = $('#txtSubrubDay').val();
    } else {
        name = $('#txtNewPickUpName').val();
        add = $('#txtNewPickUpAdderss').val();
        id = $('#hdnID').val();
    }

    model = {
        Name: name,
        Address: add,
        Days: days,
        ModelType: type,
        ID: id
    };

    $.ajax({
        url: siteURL + 'ManageRunPopPop',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            resetManagemodelPop();
            BindAllGroupList(type);
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function resetManagemodelPop() {
    $('#txtDeliveryRunName').val('');
    $('#txtDeliveryRunAddress').val('Brisbane');
    $('#txtDeliveryRunDay').val('');
    $('#txtPostCodeName').val('');
    $('#txtPostCodeWarehouse').val('');
    $('#txtPostCodeDay').val('');
    $('#txtSubrubName').val('');
    $('#txtSubrubWarehouse').val('');
    $('#txtSubrubDay').val('');
    // $('#ddlModelCutoffType').val('');
    $('#ddlCutOffForModel').empty();
    $('#divModelChkListStates').hide();
    $('#divModelState input:checked').each(function () {
        $(this).prop('checked', false);
    });
    $('#GroupName').text('----------');
    $('#GroupNamePost').text('----------');
    $('#txtNewPickUpName').val('');
    $('#txtNewPickUpAdderss').val('');
    $('#hdnID').val('0');
}

function BindAllGroupList(type) {
    var listID = "";
    if (type === 'P') {
        listID = "#postCodeGroupList";
    } else if (type === 'S') {
        listID = "#suburbGroupList";
    } else if (type === 'D') {//D
        listID = "#deliveryRunsGroupList";
    } else {
        listID = "#listPickUps";
    }

    $.ajax({
        url: siteURL + 'GetModelGroupList',
        type: 'POST',
        data: "{'GroupType':'" + type + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            $(listID).empty();
            $.each(response.GroupList, function (idx, val) {
                if (type === 'U') {
                    $(listID).append($("<option></option>").val(val.ID).html(val.PickupName + " (" + val.PickupAddress + ")"));
                } else {
                    $(listID).append($("<option></option>").val(val.GroupID).html(val.GroupName));
                }
            });

            //  resetManagemodelPop();
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function setModelCutOffFunctionaity(ctrl) {
    var val = $(ctrl).val();
    var showStates = false;

    if (val == "S") {
        showStates = true;
    } else if (val == "P") {
        showStates = true;
    }

    $('#ddlCutOffForModel').empty();
    if (showStates) {
        $('#divModelState input:checked').each(function () {
            $(this).prop('checked', false);
        });
        $('#divModelChkListStatesDeliveryRun').show();
        //alert('Please choose the states');
    } else {
        $('#divModelChkListStatesDeliveryRun').hide();
    }
}

function fetchModelDataBasedOnState() {
    var selected = '';
    $('#divModelChkListStatesDeliveryRun input:checked').each(function () {
        selected += $(this).data('state') + ",";
    });

    console.log(selected);
    var type = $('#ddlModelCutoffType').val();
    if (type === '') { return; }
    if (selected === "") {
        return;
    }
    getCutoffData(type, '', 'M', selected);
    if (type === 'D') {
    }
}

function RemoveRunPoppop(ctrl) {
    var type = $(ctrl).data('type');
    var groupID;
    var ddlID = '';
    var selectedValues = '';
    if (type === '') { return; }
    if (type === 'D') {
        ddlID = "#deliveryRunsGroupList";
        $("#ddlSelectedDeliveryRun :selected").each(function () {
            selectedValues += $(this).val() + ",";
        });
        groupID = $("#deliveryRunsGroupList :selected").val();

        if (typeof (groupID) === 'undefined') {
            alert("Please select Delivery Run");
        }
    }
    else if (type === 'P') {
        ddlID = "#postCodeGroupList";

        groupID = $("#postCodeGroupList :selected").val();
        if (typeof (groupID) === 'undefined') {
            alert("Please select Delivery Run");
        }
        $("#ddlSelectedPostcode :selected").each(function () {
            selectedValues += $(this).val() + ",";
        });
    } else if (type === 'S') {
        ddlID = "#suburbGroupList";
        groupID = $("#suburbGroupList :selected").val();
        if (typeof (groupID) === 'undefined') {
            alert("Please select Delivery Run");
        }
        $("#ddlSelectedSuburbs :selected").each(function () {
            selectedValues += $(this).val() + ",";
        });
    }
    model = {
        GroupID: groupID,
        ListValue: selectedValues
    };

    $.ajax({
        url: siteURL + 'RemoveModelGroupDetails',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            stopLoading();

            GetGroupDetail($(ddlID));
        },
        error: function () {
            stopLoading();
        }
    });
}

function AddRunPoppop(ctrl) {
    var type = $(ctrl).data('type');
    var selected = '';
    var selectedValues = '';
    var cuttype = '';
    var groupID;
    var ddlID = '';
    if (type === '') { return; }
    if (type === 'D') {
        ddlID = "#deliveryRunsGroupList";
        groupID = $("#deliveryRunsGroupList :selected").val();

        $('#divModelChkListStatesDeliveryRun input:checked').each(function () {
            selected += $(this).data('state') + ",";
        });

        cuttype = $('#ddlModelCutoffType').val();

        $("#ddlCutOffForModel :selected").each(function () {
            selectedValues += $(this).val() + ",";
        });
    } else if (type === 'P') {
        ddlID = "#postCodeGroupList";
        groupID = $("#postCodeGroupList :selected").val();

        $('#divModelChkListStatesPostCode input:checked').each(function () {
            selected += $(this).data('state') + ",";
        });

        cuttype = type;

        $("#ddlAllPostCode :selected").each(function () {
            selectedValues += $(this).val() + ",";
        });
    } else if (type === 'S') {
        ddlID = "#suburbGroupList";
        groupID = $("#suburbGroupList :selected").val();

        $('#divModelChkListStatesSuburbs input:checked').each(function () {
            selected += $(this).data('state') + ",";
        });

        cuttype = type;

        $("#ddlAllSubrubs :selected").each(function () {
            selectedValues += $(this).val() + ",";
        });
    }

    model = {
        GroupID: groupID,
        Filter: cuttype,
        ChkValue: selected,
        ListValue: selectedValues
    };

    $.ajax({
        url: siteURL + 'AddUpdateModelGroupDetails',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            // resetManagemodelPop();
            // BindAllGroupList(type);
            stopLoading();

            GetGroupDetail($(ddlID));
        },
        error: function () {
            stopLoading();
        }
    });
}

function DeleteRunPoppop(ctrl) {
    var type = $(ctrl).data('type');
    var ListID = "";
    var ddlID = "";
    if (type === 'D') {
        ListID = "#deliveryRunsGroupList";
        ddlID = "#ddlSelectedDeliveryRun";
    } else if (type === 'P') {
        ListID = "#postCodeGroupList";
        ddlID = "#ddlSelectedPostcode";
    } else if (type === 'S') {
        ListID = "#suburbGroupList";
        ddlID = "#ddlSelectedSuburbs";
    } else {
        ListID = "#listPickUps";
    }
    var groupID = $(ListID).val();
    $.ajax({
        url: siteURL + 'RemoveGroupByID',
        type: 'POST',
        data: "{'GroupID':" + groupID + ", 'Type':'" + type + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            resetManagemodelPop();
            BindAllGroupList(type);
            $(ddlID).empty();
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function GetGroupDetail(ctrl) {
    var type = $(ctrl).data('type');
    var groupID = $(ctrl).val()[0];

    // var groupText = $(ctrl).text();

    $.ajax({
        url: siteURL + 'GetGroupDetailByID',
        type: 'POST',
        data: "{'GroupID':" + groupID + ", 'Type':'" + type + "'}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            var ddlID = "";

            if (type === 'D') {
                ddlID = "#ddlSelectedDeliveryRun";
                $("#chkAllowPickUp").prop('checked', response.AllowPickup);

                // $('#ddlModelCutoffType option:selected').removeAttr('selected');
                var ddlvaltype = response.GroupDetailList[0].GroupFilter;
                if (ddlvaltype === 'Suburb') {
                    ddlvaltype = 'S';
                } else {
                    ddlvaltype = 'P';
                }
                //  debugger;
                $("#ddlModelCutoffType  option").each(function () {
                    //  $(this).prop('selected', false);
                    if ($(this).val() === ddlvaltype) {
                        $(this).attr('selected', 'selected');
                    }
                });
                $('#divModelChkListStatesDeliveryRun').show();
                $('#divModelState input:checked').each(function () {
                    $(this).prop('checked', false);
                });
            } else if (type === 'P') {
                $('#GroupNamePost').text($(ctrl).find('option:selected').text());
                ddlID = "#ddlSelectedPostcode";
            } else if (type === 'S') {
                $('#GroupName').text($(ctrl).find('option:selected').text());
                ddlID = "#ddlSelectedSuburbs";
            } else {
                $('#txtNewPickUpName').val(response.GroupDetailList.PickupName);
                $('#txtNewPickUpAdderss').val(response.GroupDetailList.PickupAddress);
                $('#hdnID').val(response.GroupDetailList.ID);
            }
            debugger;

            $.each(response.GroupDetailList, function (idx, val) {
                var code = val.ChkFilterValue.split(',');
                if (code.length > 0) {
                    for (var i = 0; i < code.length; i++) {
                        if (type === 'D') {
                            $('#divModelChkListStatesDeliveryRun  #chk' + code[i]).prop('checked', true).trigger("change");
                        }
                        else if (type === 'P') {
                            $('#divModelChkListStatesPostCode  #chk' + code[i]).prop('checked', true).trigger("change");
                        }
                        else if (type === 'S') {
                            $('#divModelChkListStatesSuburbs  #chk' + code[i]).prop('checked', true).trigger("change");
                        }
                    }
                }
            });

            if (type !== 'U') {
                $(ddlID).empty();

                $.each(response.GroupDetailList, function (idx, val) {
                    var code = val.ListValue.split(',');
                    if (code.length > 0) {
                        for (var i = 0; i < code.length; i++) {
                            //if ($(ddlID + " option[value='" + code[i] + "']").length < 1) {
                            $(ddlID).append($("<option></option>").val(code[i]).html(code[i]));
                            //}
                        }
                    }
                });
            }
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function fetchModelDataBasedOnStateSubandPost(type) {
    var selected = '';

    var method = '';

    var chkDivID = '';

    var ddldiv = '';
    //  console.log(selected);
    if (type === 'S') {
        method = 'MS';
        chkDivID = "#divModelChkListStatesSuburbs input:checked";
        ddldiv = "#ddlAllSubrubs";
    }
    else if (type === 'P') {
        method = 'MP';
        chkDivID = "#divModelChkListStatesPostCode input:checked";
        ddldiv = "#ddlAllPostCode";
    }
    $(chkDivID).each(function () {
        selected += $(this).data('state') + ",";
    });

    if (selected === "") {
        $(ddldiv).empty();

        return;
    }
    getCutoffData(type, '', method, selected);
}

function MasterCutOff(ctrl) {
    var ddlvalue = $(ctrl).val();
    $("#ddlOrderCutoffType").val(ddlvalue).attr("selected", "selected").prop('disabled', true);
    $("#ddlSelectOrderLimitType").val(ddlvalue).attr("selected", "selected").prop('disabled', true);
    $("#ddlPermittedDaysType").val(ddlvalue).attr("selected", "selected").prop('disabled', true);

    setCutOffFunctionaity(ctrl);
    getCutoffData('', ctrl, 'OL');

    showHidePermittedDaysData(ctrl);
    getCutoffData('', ctrl, 'PD');

    $("#wkDays input").each(function () {
        $(this).prop('checked', false);
    });
};
// Code by venu End

function manageCutoffPermittedDays() {
    if ($('#chkEnableCutOffRules').prop('checked') === false) {
        managePermittedDays();
    } else {
        var dayspermitted = '';
        var permittedDayType = $('#ddlPermittedDaysType').val();
        var from = '';
        var to = '';

        var id = $('#ddlPermittedDaysFor').val();

        if (permittedDayType === 'U') {
            from = $('#ddlPermittedDaysFrom').val();
            to = $('#ddlPermittedDaysTo').val();
        }

        $("#wkDays input").each(function () {
            if ($(this).is(":checked")) {
                dayspermitted += $(this).data('day') + ",";
            }
        });

        var model = {
            GroupID: id,
            Days: dayspermitted,
            From: from,
            To: to,
            PermitBy: permittedDayType
        };

        startLoading();
        $.ajax({
            url: siteURL + 'SetCutoffPermittedDays',
            type: 'POST',
            data: "{'model':" + JSON.stringify(model) + "}",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                console.log(response);
                stopLoading();
            },
            error: function () {
                stopLoading();
            }
        });
    }
}

function GetCutoffPermittedDays() {
    var id = $('#ddlPermittedDaysFor').val();
    var permittedDayType = $('#ddlPermittedDaysType').val();

    var model = {
        GroupID: id,
        PermitBy: permittedDayType
    };
    startLoading();

    $.ajax({
        url: siteURL + 'GetCutoffPermittedDays',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);
            var cutOff = response.CutOff;

            $("#wkDays").html('');
            var html = '';
            $.each(response.Days, function (key, value) {
                html += '<div><input type="checkbox" data-day="' + value.ShortName + '" data-id="' + (parseInt(value.DayValue) - 1) + '" id="' + value.ShortName + '" ' + (value.IsActive === true ? "checked" : "") + '/><label for="' + value.ShortName + '">' + value.ShortName + '</label></div>';
            });
            $("#wkDays").html(html);
            if (id > 0) {
                $('#ddlPermittedDaysFrom').val(cutOff.PermitFrom);
                $('#ddlPermittedDaysTo').val(cutOff.PermitTo);
            } else {
                $('#ddlPermittedDaysFrom').val('01:00');
                $('#ddlPermittedDaysTo').val('01:00');
            }

            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function AddProductsToOrder() {
    var cartID = $('#hdnCartID').val();

    var selectedProducts = [];

    $('#tblAddProducts tbody tr').each(function () {
        if ($(this).find('input[type=checkbox]').prop('checked')) {
            var quantity = $(this).find('input[type=text]').val();
            selectedProducts.push({
                ProductID: $(this).find('input[type=checkbox]').data('pid'),
                Quantity: quantity
            });
        }
    });

    startLoading();
    $.ajax({
        url: siteURL + 'AddProductsToOrder',
        type: 'POST',
        data: "{'cartID':" + cartID + ", 'products':" + JSON.stringify(selectedProducts) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response === "Success") {
                GetOrderToEdit(cartID);

                $('#modalAddProducts').modal('hide');
            }
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });

    console.log(selectedProducts);
}

function deleteCartItem(itemID) {
    startLoading();
    $.ajax({
        url: siteURL + 'DeleteCartItem',
        type: 'POST',
        data: "{'itemID':" + itemID + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response === "Success") {
                $('#row_' + itemID).css({ 'background': 'red', 'color': 'white' }).fadeTo("slow", 0.2, function () {
                    $(this).remove();
                });

                var cnt = 1;
                setTimeout(function () {
                    $('#tblEditOrder tbody tr').each(function () {
                        console.log($(this).find('th').eq(0).html());
                        $(this).find('th').eq(0).html(cnt);
                        cnt++;
                    });
                }, 1000);
            }
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function updateOrderDetails() {
    $('#myModalAlerMessage').modal("hide");
    if ($('#ddlDelType').val() == '') {
        $("#error").html("Please select delivery type.");
        $('#myModalAlerMessage').modal("show");
        return;
    }
    if ($('#ddlAddressType').val() == '0' && $('#ddlDelType').val() == 'U') {
        $("#error").html("Please select Pick Up Address.");
        $('#myModalAlerMessage').modal("show");
        return;
    }
    var cartID = $('#hdnCartID').val();
    var requiredDate = $('#txtOrderDetailDate').val();
    var runNo = $("#ddlDeliveryRuns").val();
    var deliveryType = $('#ddlDelType').val();
    var orderComment = $('#txtOrderComment').val();
    var deliveryNotes = $('#txtDeliveryNotes').val();
    var address = $('#txtOrderAddress').val();
    var isAddressChanged = false;
    var paymentdone = $('#hdnIspaymentdone').val();

    if ($('#hdnAddress').val() !== address) {
        isAddressChanged = true;
    }

    var proQty = [];

    $('#tblEditOrder tbody tr').each(function () {
        var pID = $(this).find('td').eq(2).find('input[type=text]').data('pid');
        var qty = $(this).find('td').eq(2).find('input[type=text]').val();
        var price = $(this).find('td').eq(4).find('input[type=text]').val();

        if (pID != null && typeof pID != "undefined") {
            proQty.push({ ProductID: pID, Quantity: qty, Price: price });
        }
    });
    console.log(proQty);

    var model = {
        OrderDate: requiredDate,
        RunNo: runNo,
        IsDelivery: deliveryType == 'U' ? false : true,
        OrderComment: orderComment,
        DeliveryNotes: deliveryNotes,
        CartID: cartID,
        Address: address,
        IsAddressChanged: isAddressChanged,
        Items: proQty,
        PickupID: deliveryType == 'U' ? $('#ddlAddressType').val() : null,
        PaymentDone: paymentdone
    }

    startLoading();
    $.ajax({
        url: siteURL + 'updateOrderDetails',
        type: 'POST',
        data: "{'model':" + JSON.stringify(model) + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.Result === "Success") {
                stopLoading();
                GetAllOrders(0);
                //setTimeout(function () {
                //    GetAllOrders(0);
                //}, 100);
                SuccessCall(response);
            }
            else {
                stopLoading();
                $('#myModalAlerMessage').modal("hide");
                $("#error").html(response.Result);
                $('#myModalAlerMessage').modal("show");
                return;
            }
        },
        error: function () {
            stopLoading();
        }
    });

    function SuccessCall(data) {
        $.ajax({
            url: siteURL + '_GetOrderToEdit',
            type: 'POST',
            data: "{'cartID':" + data.CartId + ", 'isPaymentDone':" + data.PamentStatus + "}",
            contentType: "application/json;charset=utf-8",
            dataType: "html",
            success: function (response) {
                $('#divOrderDetail').html("");
                $('#divOrderDetail').html(response);
                stopLoading();
                $('#txtOrderDetailDate').datepicker({
                    minDate: new Date(),
                    autoClose: true
                });

                return false;
            },
            error: function () {
                stopLoading();
            }
        });
    }
}

function CheckAndExportOrders(order) {
    startLoading();
    var orders = '';
    var url = '';
    if (order === '') {
        if ($('#tblbodyList td').find('input[type="checkbox"]:checked').length > 0) {
            $('#tblbodyList td').find('input[type="checkbox"]:checked').each(function () {
                orders += $(this).attr('id') + ',';
            });
        }

        url = siteURL + 'DownloadOrders?orders=' + orders;
    } else {
        url = siteURL + 'DownloadOrders?orders=' + order;
    }

    window.location.href = url;
    setTimeout(function () {
        stopLoading();
        $("#tblbodyList").find('input[type=checkbox]').prop('checked', false);
    }, 4000);
}
function setWarehouse(ctrl) {
    var val = $(ctrl).val().split('_')[1];
    $('#lblWarehouse').text(val);
}

function setInvoiceValues(ctrl) {
    $('#hdnInvCartID').val($(ctrl).data('id'));
    $('#hdnInvRefund').val($(ctrl).data('refund'));
    $('#txtInvoiceEmail').val($(ctrl).data('email'));
    $('#sendInvoiceModal').modal('show');
}

function sendInvoiceByEmail() {
    var email = $('#txtInvoiceEmail').val();
    var cartID = $('#hdnInvCartID').val();
    var isRefund = $('#hdnInvRefund').val() == 'False' ? false : true;

    if (email.trim() == "") {
        alert("The email field can not be blank");
        return false;
    }

    startLoading();
    $.ajax({
        url: siteURL + 'CheckAndSendInvoice',
        type: 'POST',
        data: "{'email':'" + email + "', 'cartID':" + cartID + ", 'IsRefund':" + isRefund + "}",
        contentType: "application/json;charset=utf-8",
        dataType: "html",
        success: function (response) {
            if (response === '"Success"') {
                growlNotification('Invoice sent to ' + email + '.', 'success', 4000);
                $('#txtInvoiceEmail').val('');
                $('#sendInvoiceModal').modal('hide');
            } else {
                growlNotification(response, 'danger', 5000);
            }
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}

function OpenAddressPopup(AddressID, Address, Suburb, Zip) {
    $('#modalUpdateAddress').modal('show');

    $('#hdnAddresID').val(AddressID);
    $('#txtDeliveryAddress1').val(Address);
    $('#txtSuburb').val(Suburb);
    $('#txtZIP').val(Zip);

    debugger;
}

function UpdateCustomerDeliveryAddress() {
    var AddressID = $('#hdnAddresID').val();
    var DeliveryAddress1 = $('#txtDeliveryAddress1').val();
    var Suburb = $('#txtSuburb').val();
    var ZIP = $('#txtZIP').val();

    $('#txtOrderAddress').val(DeliveryAddress1 + " " + Suburb + " " + ZIP);

    startLoading();
    $.ajax({
        url: siteURL + 'UpdateCustomerDeliveryAddress',
        type: 'POST',
        data: "{'addressid':" + AddressID + ", 'deliveryaddress':'" + DeliveryAddress1 + "', 'suburb':'" + Suburb + "', 'zip':'" + ZIP + "' }",
        contentType: "application/json;charset=utf-8",
        dataType: "html",
        success: function (response) {
            if (response === "Success") {
                growlNotification('Delivery Address Updated Successfully ', 'success', 4000);
            } else {
                growlNotification(response, 'danger', 5000);
            }
            stopLoading();
        },
        error: function () {
            stopLoading();
        }
    });
}