﻿using AutoMapper;
using CommonFunctions;
using DataModel.BO;
using DataModel.Model;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;

namespace DataModel.DA
{
    public class OrderDA
    {
        private readonly SaaviAdminEntities _db;
        private List<CalculatedOrdersBO> _calculatedOrders;

        public OrderDA()
        {
            _db = new SaaviAdminEntities();
            _calculatedOrders = null;
        }

        #region get AUS Eastern standard time

        /// <summary>
        /// get AUS Eastern standard time
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static DateTime getMST(DateTime val)
        {
            DateTime serverTime = Convert.ToDateTime(val); // gives you current Time in server timeZone
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZone"].ToString());
            DateTime dt = TimeZoneInfo.ConvertTime(serverTime, tzi);
            return dt;
        }

        #endregion get AUS Eastern standard time

        #region Get required date

        /// <summary>
        /// Get required date
        /// </summary>
        /// <returns></returns>
        public string GetRequiredDate(byte? daysPrior = 0, string cutoffTime = "")
        {
            try
            {
                var notice = _db.NoticeMasters.FirstOrDefault();

                if (notice.GroupID > 0)
                {
                    var res = _db.CutoffGroupMasters.Find(notice.GroupID);
                    if (res != null)
                    {
                        daysPrior = res.Day;
                    }
                }
                var d = daysPrior == 0 ? 1 : daysPrior;
                DateTime requiredDate = getMST(DateTime.Now.AddHours(d.To<double>()));

                if (notice != null)
                {
                    DateTime t1 = getMST(Convert.ToDateTime(DateTime.Now));
                    DateTime t2 = Convert.ToDateTime(cutoffTime.Length > 0 ? cutoffTime : notice.OrderCutOff);
                    if (notice.IsPermitSameDay == true)
                    {
                        requiredDate = getMST(DateTime.Now);
                    }
                    else
                    {
                        if (t1.TimeOfDay.Ticks < t2.TimeOfDay.Ticks)
                        {
                            if (daysPrior == 0)
                            {
                                requiredDate = getMST(DateTime.Now);
                            }
                            else
                            {
                                requiredDate = getMST(DateTime.Now.AddHours(daysPrior.To<double>()));
                            }
                        }
                        int dct = 0;
                        bool isDayAvailable = true;
                        List<PermittedDaysMaster> daysData = GetPermittedDays("", notice.GroupID.To<int>()).Where(i => i.IsActive == false).ToList();
                    getrequired:
                        foreach (var dayData in daysData)
                        {
                            if (dayData.DayName.ToLower() == requiredDate.DayOfWeek.ToString().ToLower())
                            {
                                requiredDate = requiredDate.AddDays(1);
                                dct++;
                                if (dct <= 7)
                                {
                                    goto getrequired;
                                }
                                else
                                {
                                    isDayAvailable = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                return Convert.ToDateTime(requiredDate).ToShortDateString();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get required date

        #region get permitted days

        public List<PermittedDaysMaster> GetPermittedDays(string dayName, int groupID)
        {
            if (dayName != "")
            {
                return _db.PermittedDaysMasters.Where(x => x.DayName.ToLower() == dayName.ToLower()).OrderBy(x => x.DayValue).ToList();
            }
            else
            {
                var cutOffDays = _db.CutoffPermittedDaysMasters.Where(p => p.GroupID == groupID).FirstOrDefault();

                if (cutOffDays != null)
                {
                    string[] selected = cutOffDays.Days.TrimEnd(',').Split(',');

                    var pDays = _db.PermittedDaysMasters.OrderBy(x => x.DayValue).ToList();

                    foreach (var day in pDays)
                    {
                        if (selected.Contains(day.ShortName))
                        {
                            day.IsActive = true;
                        }
                        else
                        {
                            day.IsActive = false;
                        }
                    }

                    return pDays;
                }
                else
                {
                    return _db.PermittedDaysMasters.Where(x => x.DayName.ToLower() == dayName.ToLower()).OrderBy(x => x.DayValue).ToList();
                }
            }
        }

        /// <summary>
        /// get permitted days
        /// </summary>
        /// <param name="dayName"></param>
        /// <returns></returns>
        public object GetPermittedDays(string dayName, int groupID = 0, string permitBy = "")
        {
            try
            {
                if (dayName == "")
                {
                    var cutOffDays = _db.CutoffPermittedDaysMasters.Where(p => p.GroupID == groupID).FirstOrDefault();

                    if (cutOffDays != null)
                    {
                        string[] selected = cutOffDays.Days.TrimEnd(',').Split(',');

                        var pDays = _db.PermittedDaysMasters.OrderBy(x => x.DayValue).ToList();

                        foreach (var day in pDays)
                        {
                            if (selected.Contains(day.ShortName))
                            {
                                day.IsActive = true;
                            }
                            else
                            {
                                day.IsActive = false;
                            }
                        }

                        return new { Days = pDays, CutOff = cutOffDays };
                    }
                    else
                    {
                        cutOffDays = _db.CutoffPermittedDaysMasters.Where(p => p.GroupID == 0).FirstOrDefault();
                        if (cutOffDays != null)
                        {
                            string[] selected = cutOffDays.Days.TrimEnd(',').Split(',');
                            var pDays = _db.PermittedDaysMasters.OrderBy(x => x.DayValue).ToList();

                            foreach (var day in pDays)
                            {
                                if (selected.Contains(day.ShortName))
                                {
                                    day.IsActive = true;
                                }
                                else
                                {
                                    day.IsActive = false;
                                }
                            }
                            return new { Days = pDays, CutOff = cutOffDays }; ;
                        }
                        else
                        {
                            return new { Days = _db.PermittedDaysMasters.Where(x => x.DayName.ToLower() == dayName.ToLower()).OrderBy(x => x.DayValue).ToList(), CutOff = "" };
                        }
                    }
                    //}
                    //else
                    //{
                    //    return _db.PermittedDaysMasters.OrderBy(x => x.DayValue).ToList();
                    //}
                }
                else
                {
                    return new { Days = _db.PermittedDaysMasters.Where(x => x.DayName.ToLower() == dayName.ToLower()).OrderBy(x => x.DayValue).ToList(), CutOff = "" };
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get permitted days

        #region check permitted days, is it conflicted or not

        /// <summary>
        /// check permitted days, is it conflicted or not
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public object CheckPermittedDays(GetPermittedDaysModel model)
        {
            try
            {
                DateTime requiredDate = getMST(DateTime.Now.AddDays(1));
                DateTime t1 = getMST(Convert.ToDateTime(DateTime.Now));
                DateTime t2 = model.RequiredTime;
                if (model.IsPermitSameDay == true)
                {
                    requiredDate = getMST(DateTime.Now);

                    return new { code = "3", NewDate = String.Format("{0:dd/MM/yyyy}", requiredDate) };
                }
                else
                {
                    if (t1.TimeOfDay.Ticks < t2.TimeOfDay.Ticks)
                    {
                        requiredDate = getMST(DateTime.Now);
                    }
                    bool isDayAvailable = false; int dct = 0;
                    while (dct <= 7)
                    {
                        string[] days = (model.PermittedDays != null && model.PermittedDays != "") ? model.PermittedDays.TrimEnd(',').Split(',') : null;
                        if (days.Contains(Convert.ToInt16(requiredDate.DayOfWeek).ToString()))
                        {
                            isDayAvailable = true;
                            break;
                        }
                        dct++;
                        requiredDate = requiredDate.AddDays(1);
                    }
                    if (dct > 1 || requiredDate.Date != getMST(DateTime.Now.Date))
                    {
                        return new { code = "1", NewDate = String.Format("{0:dd/MM/yyyy}", requiredDate) };
                    }
                    else if (isDayAvailable)
                    {
                        return new { code = "0", NewDate = String.Format("{0:dd/MM/yyyy}", requiredDate) };
                    }
                    else
                    {
                        return new { code = "2", NewDate = String.Format("{0:dd/MM/yyyy}", requiredDate) };
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion check permitted days, is it conflicted or not

        #region get notice detail

        /// <summary>
        /// get notice detail
        /// </summary>
        /// <returns></returns>
        public NoticeMaster GetNotice()
        {
            try
            {
                _db.Configuration.ProxyCreationEnabled = false;
                return _db.NoticeMasters.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get notice detail

        #region manage notice

        /// <summary>
        /// manage notice
        /// </summary>
        /// <param name="li"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageNotice(List<PermittedDaysMaster> li, GetPermittedDaysModel model)
        {
            try
            {
                //update permitted days
                //ManagePermittedDays(li);

                if (Convert.ToBoolean(model.IsPermitSameDay))
                {
                    model.RequiredDate = getMST(Convert.ToDateTime(DateTime.Now));
                }

                NoticeMaster obj = new NoticeMaster
                {
                    OrderCutOff = model.RequiredTime.ToString("hh:mm tt"),
                    OrderCutOffDate = model.RequiredDate.ToString("MM/dd/yyyy"),
                    GroupID = 0,
                    CutoffType = ""
                    //// IsActive = Convert.ToBoolean(model.IsDayAvailable),
                    // IsPermitSameDay = Convert.ToBoolean(model.IsPermitSameDay),
                    // AllowPickup = model.AllowPickup,
                    // LimitOrdersTo1Month = model.LimitOrdersTo1Month,
                    // EnableCutoffRules = model.EnableCutoffRules
                };

                var notice = _db.NoticeMasters.FirstOrDefault();
                if (notice == null)
                {
                    obj.CreatedDate = DateTime.Now;
                    _db.NoticeMasters.Add(obj);
                }
                else
                {
                    notice.ModifiedDate = DateTime.Now;
                    notice.OrderCutOff = obj.OrderCutOff;
                    notice.OrderCutOffDate = obj.OrderCutOffDate;
                    notice.GroupID = 0;
                    notice.CutoffType = "";
                    //notice.IsActive = obj.IsActive;
                    //notice.IsPermitSameDay = obj.IsPermitSameDay;
                    //notice.AllowPickup = model.AllowPickup;
                    //notice.LimitOrdersTo1Month = model.LimitOrdersTo1Month;
                    //notice.EnableCutoffRules = model.EnableCutoffRules;
                }
                _db.SaveChanges();

                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage notice

        #region update permitted days

        /// <summary>
        /// update permitted days
        /// </summary>
        /// <param name="li"></param>
        /// <returns></returns>
        public string ManagePermittedDays(List<PermittedDaysMaster> li, string permitby = "", string from = "", string to = "", int gID = 0)
        {
            try
            {
                foreach (var item in li)
                {
                    var days = _db.PermittedDaysMasters.Where(x => x.DayValue == (item.ID + 1)).FirstOrDefault();
                    if (days != null)
                    {
                        days.IsActive = item.IsActive;
                        days.ModifiedDate = DateTime.Now;
                        if (permitby != "")
                        {
                            days.PermitBy = permitby;
                            days.PermitFrom = from;
                            days.PermitTo = to;
                            days.GroupID = gID;
                        }
                        _db.SaveChanges();
                    }
                }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion update permitted days

        #region get auto order detail

        /// <summary>
        /// get auto order detail
        /// </summary>
        /// <returns></returns>
        public AutoOrderMaster GetAutoOrder()
        {
            try
            {
                return _db.AutoOrderMasters.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get auto order detail

        #region manage auto order

        /// <summary>
        /// manage auto order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageAutoOrder(AutoOrderMaster model)
        {
            try
            {
                var notice = _db.AutoOrderMasters.FirstOrDefault();
                if (notice == null)
                {
                    model.CreatedDate = DateTime.Now;
                    _db.AutoOrderMasters.Add(model);
                }
                else
                {
                    notice.ModifiedDate = DateTime.Now;
                    notice.AutoOrderDay = model.AutoOrderDay;
                    notice.AutoOrderTime = model.AutoOrderTime;
                    notice.AutoOrderType = model.AutoOrderType;
                    notice.IsActive = model.IsActive;
                }
                // _db.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage auto order

        #region manage notice master

        /// <summary>
        /// manage notice master
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string saveNotice(GetNoticeBO model)
        {
            try
            {
                NoticeMaster notice = new NoticeMaster();
                notice = _db.NoticeMasters.FirstOrDefault();
                if (notice == null)
                {
                    notice = new NoticeMaster();
                    notice.CreatedDate = DateTime.Now;
                    notice.Description = model.Description;
                    // Mapper.Map(model, notice);
                    _db.NoticeMasters.Add(notice);
                }
                else
                {
                    notice.ModifiedDate = DateTime.Now;
                    notice.Description = model.Description;
                    //Mapper.Map(model, notice);
                }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage notice master

        #region get popup message

        /// <summary>
        /// get popup message
        /// </summary>
        /// <returns></returns>
        public PopupMaster GetPopupMessage()
        {
            try
            {
                return _db.PopupMasters.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get popup message

        #region manage popup message

        /// <summary>
        /// manage popup message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManagePopupMessage(PopupMaster model)
        {
            try
            {
                var data = _db.PopupMasters.FirstOrDefault();
                if (data == null)
                {
                    model.CreatedDate = DateTime.Now;
                    _db.PopupMasters.Add(model);
                }
                else
                {
                    data.ModifiedDate = DateTime.Now;
                    data.Description = model.Description;
                }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion manage popup message

        #region update customer delivery days

        /// <summary>
        /// update customer delivery days
        /// </summary>
        /// <param name="li"></param>
        /// <returns></returns>
        public string ManageCustomerDays(List<GetCustomerDeliveryDays> li)
        {
            try
            {
                //set empty Delivery days for all customers
                int update = _db.Database.ExecuteSqlCommand("update dbo.CustomerMaster set StandardDeliveryDays=''");

                //update delivery days for customers
                foreach (GetCustomerDeliveryDays item in li)
                {
                    var customer = _db.CustomerMasters.Where(x => (x.AlphaCode == null ? "" : x.AlphaCode.ToLower().Trim()) == item.AlphaCode).FirstOrDefault();
                    if (customer != null)
                    {
                        string dayCode = "";
                        var dayData = GetPermittedDays(item.DayName, 0);
                        if (dayData.Count > 0) { dayCode = dayData[0].Code; }
                        string existingdays = customer.StandardDeliveryDays == null ? "" : customer.StandardDeliveryDays.Trim();

                        if (existingdays.IndexOf(dayCode) < 0)
                        {
                            existingdays += dayCode;
                        }
                        customer.StandardDeliveryDays = existingdays;
                        _db.SaveChanges();
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion update customer delivery days

        #region upload customer to order

        /// <summary>
        /// upload customer to order
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string UploadCustomerToOrder(HttpPostedFileBase file)
        {
            try
            {
                if (file.FileName != "" && (Path.GetExtension(file.FileName) == ".xlsx" || Path.GetExtension(file.FileName) == ".xls"))
                {
                    //truncate table data
                    _db.Database.ExecuteSqlCommand("TRUNCATE TABLE OrderExpectedCustomerMaster");

                    using (var excel = new ExcelPackage(file.InputStream))
                    {
                        var tbl = new DataTable();
                        var ws = excel.Workbook.Worksheets.First();
                        var hasHeader = true;  // adjust accordingly
                                               // add DataColumns to DataTable
                        foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            tbl.Columns.Add(hasHeader ? firstRowCell.Text
                                : String.Format("Column {0}", firstRowCell.Start.Column));

                        // add DataRows to DataTable
                        int startRow = hasHeader ? 2 : 1;
                        for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                        {
                            var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                            //Response.Write(wsRow[rowNum, 1] == null ? "" : wsRow[rowNum, 1].Text.ToString() + "<br/>");

                            string Alphacode = wsRow[rowNum, 1] == null ? "" : wsRow[rowNum, 1].Text.ToString().Trim();
                            if (Alphacode != "")
                            {
                                Int64 CustomerId = 0;
                                var customer = _db.CustomerMasters.Where(x => (x.AlphaCode == null ? "" : x.AlphaCode.ToLower().Trim()) == Alphacode).FirstOrDefault();
                                if (customer != null)
                                {
                                    CustomerId = customer.CustomerID;
                                }
                                if (CustomerId == 0)
                                {
                                    continue;
                                }
                                OrderExpectedCustomerMaster data = _db.OrderExpectedCustomerMasters.Where(s => s.CustomerId == CustomerId).FirstOrDefault();
                                if (data == null)
                                {
                                    data = new OrderExpectedCustomerMaster();
                                    data.CreatedDate = DateTime.Now;
                                    data.CustomerId = CustomerId;
                                    _db.OrderExpectedCustomerMasters.Add(data);
                                }
                                else
                                {
                                    data.ModifiedDate = DateTime.Now;
                                }
                                _db.SaveChanges();
                            }
                        }                     //  return "Customers are updated successfully.";
                    }
                }
                else
                {
                    return "You did not specify a file to upload.";
                }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion upload customer to order

        #region get orders by number of days with calculated order totals

        protected List<CalculatedOrdersBO> GetOrdersByNumberOfDays(int days)
        {
            if (days < 1)
            {
                days = 30;
            }
            var dateFrom = DateTime.Now.AddDays(-days);
            if (_calculatedOrders == null)
            {
                _calculatedOrders = (from tcm in _db.trnsCartMasters
                                     join tcim in _db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                     join prod in _db.ProductMasters on tcim.ProductID equals prod.ProductID
                                     where tcm.CreatedDate > dateFrom && tcm.OrderStatus != 4
                                     group new { tcm, tcim, CostPrice = prod.ComPrice } by tcm.CartID into g
                                     select new CalculatedOrdersBO
                                     {
                                         OrderValue = g.Sum(i => i.tcim.Price * i.tcim.Quantity),
                                         OrderCostValue = g.Sum(i => i.CostPrice * i.tcim.Quantity),
                                         OrderID = g.Select(i => i.tcm.CartID).FirstOrDefault(),
                                         CustomerID = g.Select(i => i.tcm.CustomerID).FirstOrDefault(),
                                         OrderDate = g.Select(i => i.tcm.CreatedDate).FirstOrDefault(),
                                         IsOrderPlacedByRep = g.Select(i => i.tcm.IsOrderPlpacedByRep).FirstOrDefault(),
                                         DeviceType = g.Select(i => i.tcm.DeviceType).FirstOrDefault()
                                     }).ToList();
            }
            return _calculatedOrders;
        }

        #endregion get orders by number of days with calculated order totals

        #region get order stats

        public OrderStatsBO GetOrderStats()
        {
            try
            {
                var data = GetOrdersByNumberOfDays(30);

                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var dataThisMonth = data.Where(i => i.OrderDate.Value.Date >= firstDayOfMonth).ToList();
                var todaysData = data.Where(i => i.OrderDate.Value.Date == DateTime.Now.Date).ToList();
                var yesterdaysData = data.Where(i => i.OrderDate.Value.Date == DateTime.Now.AddDays(-1).Date).ToList();
                var sameDayLastWeekData = data.Where(i => i.OrderDate.Value.Date == DateTime.Now.AddDays(-7).Date).ToList();

                var dayOfWeekToday = (int)DateTime.Now.DayOfWeek;
                var thisWeekSalesData = data.Where(i => i.OrderDate.Value.Date >= DateTime.Now.AddDays(-1 * dayOfWeekToday).Date && i.OrderDate.Value.Date <= DateTime.Now.Date).ToList();
                var lastWeekSalesData = data.Where(i => i.OrderDate.Value.Date >= DateTime.Now.AddDays(-1 * (dayOfWeekToday + 7)).Date && i.OrderDate.Value.Date < DateTime.Now.AddDays(-1 * dayOfWeekToday).Date).ToList();

                return (new OrderStatsBO
                {
                    TodayOrdersTotal = todaysData.Sum(i => i.OrderValue),
                    TodayAverageValue = todaysData.Average(i => i.OrderValue) ?? 0,
                    TodayAverageMargin = todaysData.Average(i => (i.OrderValue - i.OrderCostValue)) ?? 0,
                    //TodayOrdersByCustomers = todaysData.Where(i => i.IsOrderPlacedByRep == false).Count(),
                    TodayOrdersByReps = todaysData.Where(i => i.IsOrderPlacedByRep == true).Count(),
                    TodayTotalOrders = todaysData.Count(),

                    YesterdayOrdersTotal = yesterdaysData.Sum(i => i.OrderValue),
                    YesterdayAverageValue = yesterdaysData.Average(i => i.OrderValue) ?? 0,
                    YesterdayAverageMargin = yesterdaysData.Average(i => (i.OrderValue - i.OrderCostValue)) ?? 0,
                    YesterdayTotalOrders = yesterdaysData.Count(),

                    SameDayLastWeekOrdersTotal = sameDayLastWeekData.Sum(i => i.OrderValue),
                    SameDayLastWeekTotalOrders = sameDayLastWeekData.Count(),

                    ThisMonthTotalOrderValue = dataThisMonth.Sum(i => i.OrderValue),
                    ThisMonthTotalOrders = dataThisMonth.Count(),
                    ThisMonthActiveCustomers = dataThisMonth.Select(i => i.CustomerID).Count(),
                    ThisMonthAppCustomers = dataThisMonth.Where(i => i.DeviceType == "iPhone" || i.DeviceType == "ANDROID").Select(i => i.CustomerID).Count(),

                    LastMonthActiveCustomers = data.Select(i => i.CustomerID).Count(),
                    LastMonthAppCustomers = data.Where(i => i.DeviceType == "iPhone" || i.DeviceType == "ANDROID").Select(i => i.CustomerID).Count(),
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get order stats

        #region GetKeyOrderStats

        public OrderKeyStatsBO GetKeyOrderStats(bool isSales)
        {
            try
            {
                var data = GetOrdersByNumberOfDays(30);

                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                var dataThisMonth = data.Where(i => i.OrderDate.Value.Date >= firstDayOfMonth).ToList();
                var todaysData = data.Where(i => i.OrderDate.Value.Date == DateTime.Now.Date).ToList();
                var yesterdaysData = data.Where(i => i.OrderDate.Value.Date == DateTime.Now.AddDays(-1).Date).ToList();
                var sameDayLastWeekData = data.Where(i => i.OrderDate.Value.Date == DateTime.Now.AddDays(-7).Date).ToList();

                var dayOfWeekToday = (int)DateTime.Now.DayOfWeek;
                var thisWeekSalesData = data.Where(i => i.OrderDate.Value.Date >= DateTime.Now.AddDays(-1 * dayOfWeekToday).Date && i.OrderDate.Value.Date <= DateTime.Now.Date).ToList();
                var lastWeekSalesData = data.Where(i => i.OrderDate.Value.Date >= DateTime.Now.AddDays(-1 * (dayOfWeekToday + 7)).Date && i.OrderDate.Value.Date < DateTime.Now.AddDays(-1 * dayOfWeekToday).Date).ToList();

                if (isSales)
                {
                    return (new OrderKeyStatsBO
                    {
                        TodayTotal = todaysData.Sum(i => i.OrderValue),
                        YesterdayTotal = yesterdaysData.Sum(i => i.OrderValue),
                        SameDayLastWeekTotal = sameDayLastWeekData.Sum(i => i.OrderValue),
                        ThisMonthTotalValue = dataThisMonth.Sum(i => i.OrderValue)
                    });
                }
                else
                {
                    return (new OrderKeyStatsBO
                    {
                        TodayTotal = todaysData.Sum(i => (i.OrderValue - i.OrderCostValue)),
                        YesterdayTotal = yesterdaysData.Sum(i => (i.OrderValue - i.OrderCostValue)),
                        SameDayLastWeekTotal = sameDayLastWeekData.Sum(i => (i.OrderValue - i.OrderCostValue)),
                        ThisMonthTotalValue = dataThisMonth.Sum(i => (i.OrderValue - i.OrderCostValue))
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion GetKeyOrderStats

        #region Get Weekly Sales Distribution

        public List<WeekSalesData> GetSalesDistribution(bool thisWeek)
        {
            try
            {
                var data = GetOrdersByNumberOfDays(30);
                var dayOfWeekToday = (int)DateTime.Now.DayOfWeek;
                List<CalculatedOrdersBO> intendedWeekData;
                if (thisWeek)
                {
                    intendedWeekData = data.Where(i => i.OrderDate.Value.Date >= DateTime.Now.AddDays(-1 * dayOfWeekToday).Date && i.OrderDate.Value.Date <= DateTime.Now.Date).ToList();
                }
                else
                {
                    intendedWeekData = data.Where(i => i.OrderDate.Value.Date >= DateTime.Now.AddDays(-1 * (dayOfWeekToday + 7)).Date && i.OrderDate.Value.Date < DateTime.Now.AddDays(-1 * dayOfWeekToday).Date).ToList();
                }
                var salesDistribution = (from tws in intendedWeekData
                                         group tws by tws.OrderDate.Value.Date into twsg
                                         select new WeekSalesData
                                         {
                                             OrderDay = (int)twsg.Select(s => s.OrderDate.Value.DayOfWeek).FirstOrDefault(),
                                             OrderDate = twsg.Select(s => s.OrderDate.Value.Date).FirstOrDefault(),
                                             OrderTotalValue = twsg.Sum(s => s.OrderValue)
                                         }).OrderBy(s => s.OrderDay).ToList();
                return salesDistribution;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get Weekly Sales Distribution

        #region Get Top performing customers

        public List<CustomerOrdersBO> GetTopPerformingCustomers(int n, bool isSales)
        {
            try
            {
                var data = GetOrdersByNumberOfDays(30);
                var customerOrders = (from d in data
                                      join cm in _db.CustomerMasters on d.CustomerID equals cm.CustomerID
                                      group new { d, cm } by d.CustomerID into g
                                      select new CustomerOrdersBO
                                      {
                                          CustomerID = g.Select(i => i.cm.CustomerID).FirstOrDefault(),
                                          CustomerName = g.Select(i => i.cm.CustomerName).FirstOrDefault(),
                                          TotalOrders = g.Count(),
                                          OrdersValue = g.Sum(i => i.d.OrderValue),
                                          OrderMarginValue = g.Sum(i => (i.d.OrderValue - i.d.OrderCostValue))
                                      }).ToList();
                if (isSales)
                {
                    return (from co in customerOrders
                            orderby co.OrdersValue descending
                            select co).Take(n).ToList();
                }
                else
                {
                    return (from co in customerOrders
                            orderby co.OrderMarginValue descending
                            select co).Take(n).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get Top performing customers

        protected List<OrderProductsBO> GetProductSales()
        {
            try
            {
                var dateFrom = DateTime.Now.AddDays(-30);
                var dataProducts = (from tcm in _db.trnsCartMasters
                                    join tcim in _db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                    join prod in _db.ProductMasters on tcim.ProductID equals prod.ProductID
                                    where tcm.OrderStatus == 1 && tcm.CreatedDate > dateFrom
                                    group new { prod.ProductID, tcim.Quantity, tcim.Price, prod.ProductName, prod.ProductCode, CostPrice = prod.ComPrice, prod.CategoryID } by prod.ProductID into g
                                    select new OrderProductsBO
                                    {
                                        ProductID = g.Select(i => i.ProductID).FirstOrDefault(),
                                        CategoryID = g.Select(i => i.CategoryID).FirstOrDefault(),
                                        ProductCode = g.Select(i => i.ProductCode).FirstOrDefault(),
                                        ProductName = g.Select(i => i.ProductName).FirstOrDefault(),
                                        TotalProductUnitsSold = g.Sum(i => i.Quantity),
                                        TotalProductSalesValue = g.Sum(i => i.Price * i.Quantity),
                                        TotalProductMargin = g.Sum(i => (i.Price - i.CostPrice) * i.Quantity),
                                    }).ToList();

                return dataProducts;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Get Top performing products

        public List<OrderProductsBO> GetTopPerformingProducts(bool topBySales)
        {
            try
            {
                var dataProducts = GetProductSales();

                if (topBySales)
                {
                    return dataProducts.OrderByDescending(i => i.TotalProductSalesValue).Take(5).ToList();
                }
                else
                {
                    return dataProducts.OrderByDescending(i => i.TotalProductMargin).Take(5).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get Top performing products

        #region Get Top performing Categories

        public List<OrderCategoriesBO> GetTopPerformingCategories(bool topBySales)
        {
            try
            {
                var dataProducts = GetProductSales();
                var dataCategories = (from p in dataProducts
                                      join cat in _db.ProductCategoriesMasters on p.CategoryID equals cat.CategoryID
                                      group new { cat.CategoryID, cat.CategoryName, cat.CategoryCode, p } by cat.CategoryID into g
                                      select new OrderCategoriesBO
                                      {
                                          CategoryID = g.Select(i => i.CategoryID).FirstOrDefault(),
                                          CategoryCode = g.Select(i => i.CategoryCode).FirstOrDefault(),
                                          CategoryName = g.Select(i => i.CategoryName).FirstOrDefault(),
                                          TotalCategoryUnitsSold = g.Sum(i => i.p.TotalProductUnitsSold),
                                          TotalCategorySalesValue = g.Sum(i => i.p.TotalProductSalesValue),
                                          TotalCategoryMargin = g.Sum(i => i.p.TotalProductMargin),
                                      }).ToList();

                if (topBySales)
                {
                    return dataCategories.OrderByDescending(i => i.TotalCategorySalesValue).Take(5).ToList();
                }
                else
                {
                    return dataCategories.OrderByDescending(i => i.TotalCategoryMargin).Take(5).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get Top performing Categories

        #region Get all orders

        /// <summary>
        /// Get all orders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<AllOrderBO> ViewOrders(GetOrderModel model)
        {
            try
            {
                model.SearchText = model.SearchText == "" ? " " : model.SearchText;
                long cartID = 0;
                bool isNumeric = false;
                if (Regex.IsMatch(model.SearchText, @"^\d+$"))
                {
                    isNumeric = true;
                    cartID = model.SearchText.To<long>();
                }
                DateTime PresentDate = getMST(DateTime.Now).AddDays(-7);
                var data = (from a in _db.trnsCartMasters
                            join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID
                            where (model.OrderType == "0" ? true : a.OrderStatus == 1)
                            && (model.SearchText == "" ? true : isNumeric ? a.CartID == cartID : b.CustomerName.ToLower().Contains(model.SearchText.ToLower())
                           || model.SearchText == "" || model.SearchText == null)
                            let freightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? a.FrieghtCharges : 0
                            let Calprice = (double?)((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == a.CartID)
                                                      select new { Price = cd.Price ?? 0, Quantity = cd.Quantity ?? 0 }).ToList().Select(c => (c.Price * c.Quantity)).Sum()) ?? 0
                            let FraightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? (double?)(from soh in _db.DeliveryCharges
                                                                                                                         select new { FraightCharge = soh.DeliveryFee }).Take(1).Select(c => c.FraightCharge).Sum() ?? 0 : 0
                            let coupanAmount = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.CouponAmount : 0
                            let originalPrice = (double?)((from cd in _db.trnsCartItemsMasterHistories.Where(s => s.CartID == a.CartID)
                                                           select new
                                                           {
                                                               Price = cd.Price ?? 0,
                                                               Quantity = cd.Quantity ?? 0
                                                           }).ToList().Select(c => (c.Price * c.Quantity)).Sum()) ?? 0
                            select new AllOrderBO
                            {
                                OrderID = a.CartID,
                                UserID = a.UserID,
                                CustomerID = a.CustomerID,
                                CustomerName = b.CustomerName,
                                Email = b.Email ?? "",
                                PostCode = b.PostCode,
                                CreatedOn = a.CreatedDate,
                                OrderStatus = a.SupplierOrderNumber ?? "Open", // (a.OrderStatus == 1) ? "Open" : (a.OrderStatus == 2) ? "Shipped" : (a.OrderStatus == 3) ? "Closed" : "Cancelled",
                                ActualPrice = (double?)((originalPrice == 0 && Calprice != 0) ? (Calprice - coupanAmount + FraightCharge) : (originalPrice - coupanAmount + FraightCharge)),
                                OrderDate = a.OrderDate,
                                isAuthCount = (bool?)(from pt in _db.PaymentMasters.Where(s => s.CartID == a.CartID && s.IsStripeAuthorisationSuccess == true
                                                       && s.IsStripePaymentSuccess == false && s.CreatedOn >= PresentDate)
                                                      select pt).Any() ?? false,
                                CapturePrice = (double?)((from pm in _db.PaymentMasters.Where(s => s.CartID == a.CartID && s.IsStripeAuthorisationSuccess == true
                                                        && s.IsStripePaymentSuccess == false)
                                                          select new
                                                          {
                                                              Amount = pm.AuthorizeAmount
                                                          }).ToList().Select(m => m.Amount)).FirstOrDefault() ?? 0,
                                isPaymentDone = (bool?)(from pt in _db.PaymentMasters.Where(s => s.CartID == a.CartID && (s.IsStripeAuthorisationSuccess == false || s.IsStripeAuthorisationSuccess == true)
                                                       && s.IsStripePaymentSuccess == true)
                                                        select pt).Any() ?? false,
                                isRefundDone = (bool?)(from pt in _db.PaymentRefundMasters.Where(s => s.CartID == a.CartID)
                                                       select pt).Any() ?? false,
                                CouponAmount = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.CouponAmount : 0,
                                FraightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? (double?)(from soh in _db.DeliveryCharges
                                                                                                                         select new { FraightCharge = soh.DeliveryFee }).Take(1).Select(c => c.FraightCharge).Sum() ?? 0 : 0,
                                Price = Calprice != 0 ? (double?)(Calprice - coupanAmount + freightCharge) : 0,
                                Invoice = (((originalPrice == 0 && Calprice != 0) ? (Calprice - coupanAmount + FraightCharge) : (originalPrice - coupanAmount + FraightCharge)) - (Calprice - coupanAmount + freightCharge))
                            });
                switch (model.SortType)
                {
                    case "Customer":
                        if (model.AscDesc == "ASC")
                        {
                            data = data.OrderBy(q => q.CustomerName);
                        }
                        else
                        {
                            data = data.OrderByDescending(q => q.CustomerName);
                        }
                        break;

                    case "Status":
                        if (model.AscDesc == "ASC")
                        {
                            data = data.OrderBy(q => q.OrderStatus);
                        }
                        else
                        {
                            data = data.OrderByDescending(q => q.OrderStatus);
                        }
                        break;

                    case "RequiredDate":
                        if (model.AscDesc == "ASC")
                        {
                            data = data.OrderBy(q => q.CreatedOn);
                        }
                        else
                        {
                            data = data.OrderByDescending(q => q.CreatedOn);
                        }
                        break;

                    case "Date":
                        if (model.AscDesc == "ASC")
                        {
                            data = data.OrderBy(q => q.OrderDate);
                        }
                        else
                        {
                            data = data.OrderByDescending(q => q.OrderDate);
                        }
                        break;

                    case "Order":
                        if (model.AscDesc == "ASC")
                        {
                            data = data.OrderBy(q => q.OrderID);
                        }
                        else
                        {
                            data = data.OrderByDescending(q => q.OrderID);
                        }
                        break;

                    case "":
                        data = data.OrderByDescending(q => q.OrderID);
                        break;
                }
                return data.Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get all orders

        #region get App Discount

        /// <summary>
        /// get App Discount
        /// </summary>
        /// <returns></returns>
        public CartSettingsModel GetAppDiscount()
        {
            try
            {
                CartSettingsModel settings = new CartSettingsModel();
                var discount = _db.Discounts.FirstOrDefault();
                var profitLimit = _db.ProfitMarginLimits.FirstOrDefault();
                settings.DiscountValue = discount.DiscountValue;
                settings.IsPercentage = discount.IsPercentage;
                settings.ProfitLimitValue = profitLimit.ProfitLimitValue ?? 0;
                settings.MinCartValue = discount.MinCartValue;
                return settings;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get App Discount

        #region get delivery charges

        /// <summary>
        /// get delivery charges
        /// </summary>
        /// <returns></returns>
        public DeliveryCharge GetDeliveryCharges()
        {
            try
            {
                return _db.DeliveryCharges.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get delivery charges

        #region manage delivery charges

        /// <summary>
        /// manage delivery charges
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageDeliveryCharges(DeliveryCharge model)
        {
            try
            {
                var data = _db.DeliveryCharges.FirstOrDefault();
                if (data != null)
                {
                    data.ModifiedDate = DateTime.Now;
                    data.OrderValue = model.OrderValue;
                    data.DeliveryFee = model.DeliveryFee;
                    data.NonDeliveryDayFee = model.NonDeliveryDayFee;
                }
                else
                {
                    model.CreatedDate = DateTime.Now;
                    _db.DeliveryCharges.Add(model);
                }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage delivery charges

        #region manage discount

        /// <summary>
        /// manage discount
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected bool ManageDiscount(Discount model)
        {
            try
            {
                var ddiscount = _db.Discounts.FirstOrDefault();
                if (ddiscount != null)
                {
                    ddiscount.ModifiedDate = DateTime.Now;
                    ddiscount.DiscountValue = model.DiscountValue;
                    ddiscount.IsPercentage = model.IsPercentage;
                    ddiscount.MinCartValue = model.MinCartValue;
                }
                else
                {
                    Discount discount = new Discount();
                    discount.DiscountValue = model.DiscountValue;
                    discount.IsPercentage = model.IsPercentage;
                    discount.CreatedDate = DateTime.Now;
                    discount.MinCartValue = model.MinCartValue;
                    _db.Discounts.Add(discount);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage discount

        #region manage profit limit

        /// <summary>
        /// manage profit limit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected bool ManageProfitLimit(ProfitMarginLimit model)
        {
            try
            {
                var dlimit = _db.ProfitMarginLimits.FirstOrDefault();
                if (dlimit != null)
                {
                    dlimit.ModifiedDate = DateTime.Now;
                    dlimit.ProfitLimitValue = model.ProfitLimitValue;
                }
                else
                {
                    ProfitMarginLimit limit = new ProfitMarginLimit();
                    limit.ProfitLimitValue = model.ProfitLimitValue;
                    limit.CreatedDate = DateTime.Now;
                    _db.ProfitMarginLimits.Add(limit);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage profit limit

        #region manage cart settings

        /// <summary>
        /// manage cart settings like discount and profit margin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageCartSettings(CartSettingsModel model)
        {
            try
            {
                Discount discount = new Discount();
                discount.DiscountValue = model.DiscountValue;
                discount.IsPercentage = model.IsPercentage;
                discount.MinCartValue = model.MinCartValue;

                ManageDiscount(discount);

                ProfitMarginLimit limit = new ProfitMarginLimit();
                limit.ProfitLimitValue = model.ProfitLimitValue;
                ManageProfitLimit(limit);

                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage cart settings

        #region get selected Currency

        /// <summary>
        /// get selected Currency
        /// </summary>
        /// <returns></returns>
        public MainFeaturesMaster GetSelectedCurrency()
        {
            try
            {
                return _db.MainFeaturesMasters.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get selected Currency

        #region manage Currency

        /// <summary>
        /// manage currency
        /// </summary>
        /// <param name="currency"></param>
        /// <returns></returns>
        public string ManageCurrency(string currency)
        {
            try
            {
                var data = _db.MainFeaturesMasters.FirstOrDefault();
                if (data != null)
                {
                    data.Currency = currency;
                    _db.SaveChanges();
                }

                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion manage Currency

        #region Send order mail

        /// <summary>
        /// Send order mail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string SendOrderMail(GetOrderMail model)
        {
            try
            {
                if (model.Email.Trim() != "")
                {
                    if (model.StartDate != null && model.EndDate != null)
                    {
                        var orders = _db.trnsCartMasters.Where(s => s.CreatedDate <= model.EndDate && s.CreatedDate >= model.StartDate).FirstOrDefault();
                        if (orders != null)
                        {
                            var results = (from or in _db.trnsCartMasters
                                           where (or.CreatedDate <= model.EndDate && or.CreatedDate >= model.StartDate)
                                           select new
                                           {
                                               ID = or.CartID,
                                               CustomerName = _db.CustomerMasters.Where(x => x.CustomerID == or.CustomerID).FirstOrDefault().CustomerName,
                                               OrderDate = or.CreatedDate
                                           }).ToList();
                            List<string> filenames = new List<string>();
                            //  int i = 0;
                            string html = "";
                            foreach (var ordata in results)
                            {
                                //try
                                //{
                                string servicesPath = ConfigurationManager.AppSettings["WBS_BASE_PATH"] + "Invoices/Order_SAAVI_" + ordata.ID + ".pdf";

                                //var request = (FtpWebRequest)WebRequest.Create("ftp://" + ConfigurationManager.AppSettings["WBS_BASE_PATH"].Replace("https://", "") + "Invoices/Order_SAAVI_" + ordata.ID + ".pdf");
                                //request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["FTP_user"], ConfigurationManager.AppSettings["FTP_pwd"]);
                                //request.Method = WebRequestMethods.Ftp.GetFileSize;
                                //FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                                WebRequest request;
                                WebResponse response;
                                String strMSG = string.Empty;
                                request = WebRequest.Create(new Uri(servicesPath));
                                request.Method = "HEAD";
                                response = request.GetResponse();
                                strMSG = string.Format("{0}{1}", response.ContentLength, response.ContentType);

                                filenames.Add(servicesPath);
                                html += "<tr><td>" + ordata.ID + "</td><td>" + ordata.CustomerName + "</td><td>" + Convert.ToDateTime(ordata.OrderDate).ToShortDateString() + "</td><td><a download href='" + servicesPath + "'>download</a></td></tr>";
                                //  }
                                //catch (WebException ex)
                                //{
                                //    FtpWebResponse response = (FtpWebResponse)ex.Response;
                                //    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                                //    {
                                //        // return "";
                                //    }
                                //    // return ex.Message;
                                //}
                            }
                            if (filenames != null && filenames.Count > 0)
                            {
                                //using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                                //{
                                //string date = DateTime.Now.ToString("d");
                                //date = date.Replace("/", "");
                                //zip.AddFiles(filenames, date);
                                //Random rd = new Random();
                                //string filenm = "~/Content/order-downloads/" + date + "_" + rd.Next() + ".zip";
                                //string zippath = System.Web.Hosting.HostingEnvironment.MapPath(filenm);
                                //zip.Save(zippath);

                                string companyname = ConfigurationManager.AppSettings["CompanyName"].ToString();
                                string fromEmail = ConfigurationManager.AppSettings["FROM_EMAILID"].ToString();

                                string mailContent = File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplates/orderspdfmail.htm"));
                                mailContent = mailContent.Replace("@pathImages", ConfigurationManager.AppSettings["WBS_BASE_PATH"] + "Images");
                                mailContent = mailContent.Replace("@path", ConfigurationManager.AppSettings["SITE_URL"].ToString());
                                mailContent = mailContent.Replace("@OrderList", html);
                                mailContent = mailContent.Replace("@Company", companyname);

                                SendMail.SendEMail(model.Email, "", "", companyname + " - Requested Mobile Orders", mailContent, "", fromEmail);

                                return "Success";
                                // }
                            }
                            else
                            {
                                return "No orders found.";
                            }
                        }
                        else
                        {
                            return "No orders found.";
                        }
                    }
                    else
                    {
                        return "Please select a date.";
                    }
                }
                else
                {
                    return "Please enter Email ID.";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Send order mail

        #region Get order xml/pdf file by id

        /// <summary>
        ///  Get order xml/pdf file by id
        /// </summary>
        /// <param name="OrderId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string GetOrderFileById(long OrderId, string type)
        {
            try
            {
                // string OrderDownFileType = ConfigurationManager.AppSettings["OrderDownFileType"].ToString();
                string orderName = "orders_" + OrderId + ".xml";
                if (type == "pdf") { orderName = "SCF-HD-" + OrderId.ToString() + ".pdf"; }

                string path = ConfigurationManager.AppSettings["WBS_BASE_PATH"] + "invoices/" + orderName;
                WebRequest request;
                WebResponse response;
                String strMSG = string.Empty;
                request = WebRequest.Create(new Uri(path));
                request.Method = "HEAD";
                response = request.GetResponse();
                strMSG = string.Format("{0}{1}", response.ContentLength, response.ContentType);
                return path;
            }
            catch (Exception ex)
            {
                //In case of File not Exist Server return the (404) Error
                return ex.Message;
            }
            //var request = (FtpWebRequest)WebRequest.Create("ftp://" + ConfigurationManager.AppSettings["WBS_BASE_PATH"].Replace("https://", "") + "Invoices/orders_" + OrderId + ".xml");
            //request.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["FTP_user"], ConfigurationManager.AppSettings["FTP_pwd"]);
            //request.Method = WebRequestMethods.Ftp.GetFileSize;

            //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            //return path;
            //}
            //catch (WebException ex)
            //{
            //    FtpWebResponse response = (FtpWebResponse)ex.Response;
            //    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
            //    {
            //        return "File not found!";
            //    }
            //    return ex.Message;
            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}
        }

        #endregion Get order xml/pdf file by id

        #region Download seleced order in zip

        /// <summary>
        /// Download seleced order in zip
        /// </summary>
        /// <param name="orderIDs"></param>
        /// <returns></returns>
        public string DownloadSelectedOrders(string orderIDs)
        {
            try
            {
                if (orderIDs.Trim() != "")
                {
                    string[] orders = orderIDs.Split(',');
                    List<string> allfiles = Directory.GetFiles(ConfigurationManager.AppSettings["WBS_BASE_PATH"], "Invoices/orders_*.xml").ToList();

                    //string[] filenames = new string[results.Count];
                    List<string> filenames = new List<string>();
                    foreach (string OrderId in orders)
                    {
                        string servicesFullPath = allfiles.Where(s => s.Contains("orders_" + OrderId)).FirstOrDefault();
                        int i = 0;
                        if (servicesFullPath != null && servicesFullPath != string.Empty && servicesFullPath != "")
                        {
                            if (File.Exists(servicesFullPath))
                            {
                                filenames.Add(servicesFullPath);
                                i++;
                            }
                        }
                    }
                    if (filenames != null && filenames.Count > 0)
                    {
                        using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                        {
                            Random rd = new Random();
                            string date = DateTime.Now.ToString("d");
                            date = date.Replace("/", "");
                            zip.AddFiles(filenames, date + rd.Next());
                            string zipfilename = date + "_" + rd.Next() + ".zip";
                            string filenm = "order-downloads/" + zipfilename;
                            string zippath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + filenm);
                            zip.Save(zippath);

                            HttpContext.Current.Response.Clear();
                            HttpContext.Current.Response.ContentType = "application/zip";
                            HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + zipfilename);
                            HttpContext.Current.Response.TransmitFile(zippath);
                            HttpContext.Current.Response.Flush();
                            HttpContext.Current.Response.End();
                            HttpContext.Current.Response.Charset = "";
                        }
                    }
                    else
                    {
                        return "No orders found.";
                    }
                }
                else
                {
                    return "Please select an order.";
                }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Download seleced order in zip

        #region Get page help UpdatePageHelpInfo

        public string GetPageHelpInfo(string pageName)
        {
            try
            {
                var res = _db.PageHelpMasters.Where(p => p.PageName == pageName).FirstOrDefault();

                if (res != null)
                {
                    return res.Description;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        #endregion Get page help UpdatePageHelpInfo

        #region Update page help

        public string UpdatePageHelpInfo(string pageName, string description)
        {
            try
            {
                var res = _db.PageHelpMasters.Where(p => p.PageName == pageName).FirstOrDefault();

                if (res != null)
                {
                    res.Description = description;
                    _db.SaveChanges();

                    return res.Description;
                }
                else
                {
                    return "Page not found";
                }
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        #endregion Update page help

        #region Get data from excel file.

        public DataTable GetDataTableFromExcel(string filepath, string sheetName = "")
        {
            try
            {
                if (sheetName == "")
                {
                    sheetName = "sheet1";
                }
                string path = System.IO.Path.GetFullPath(filepath);
                // +TableName;
                OleDbConnection oledbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                path + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                oledbConn.Open();
                string commandTxt = "SELECT * FROM [" + sheetName + "$]";
                DataTable dt = new DataTable();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                cmd.Connection = oledbConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = commandTxt;
                cmd.CommandTimeout = 0;
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(dt);
                oledbConn.Close();

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData, string table)
        {
            try
            {
                //string TableName = "ImportPostcodes";
                if (table.Length > 0)
                {
                    using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConn"].ToString()))
                    {
                        csvFileData.Columns.RemoveAt(6);
                        csvFileData.AcceptChanges();
                        dbConnection.Open();
                        SqlCommand cmd = new SqlCommand("Truncate Table " + table + ";", dbConnection);
                        cmd.ExecuteNonQuery();
                        using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                        {
                            s.BulkCopyTimeout = 0;
                            s.DestinationTableName = table;
                            s.WriteToServer(csvFileData);
                        }
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Get data from excel file.

        #region Get Postcode / suburbs / pickups / deliveryruns

        public object GetCutOffData(string type, string states = null)
        {
            try
            {
                string[] selected = null;

                if (!string.IsNullOrEmpty(states))
                {
                    selected = states.Split(',');
                }

                if (type == "P")
                {
                    if (states.Length > 0)
                    {
                        var postCodes = (from a in _db.Postcodes
                                         where (states == null || selected.Contains(a.State))
                                         select new
                                         {
                                             Type = a.Postcode1,
                                             Limit = 0,
                                             Cutoff = ""
                                         }).Distinct().ToArray();
                        return new { Data = postCodes };
                    }
                    else
                    {
                        var postCodes = (from a in _db.CutoffGroupMasters
                                         join b in _db.OrderLimitMasters on a.GroupID equals b.GroupID into temp2
                                         from b in temp2.DefaultIfEmpty()
                                         join c in _db.OrderCutoffMasters on a.GroupID equals c.GroupID into temp
                                         from c in temp.DefaultIfEmpty()
                                         where a.GroupType == "Postcode"
                                         select new
                                         {
                                             Type = a.GroupName,
                                             Limit = b.OrderLimit ?? 0,
                                             Cutoff = c.CutoffTime ?? "",
                                             CutoffDate = c.CutoffDate,
                                             GID = a.GroupID
                                         }).ToList();
                        return new { Data = postCodes };
                    }
                }
                else if (type == "D")
                {
                    var runs = (from a in _db.CutoffGroupMasters
                                join b in _db.OrderLimitMasters on a.GroupID equals b.GroupID into temp2
                                from b in temp2.DefaultIfEmpty()
                                join c in _db.OrderCutoffMasters on a.GroupID equals c.GroupID into temp
                                from c in temp.DefaultIfEmpty()
                                where a.GroupType == "Delivery"
                                select new
                                {
                                    Type = a.GroupName,
                                    Limit = b.OrderLimit ?? 0,
                                    Cutoff = c.CutoffTime ?? "",
                                    CutoffDate = c.CutoffDate,
                                    GID = a.GroupID
                                }).ToList();

                    return new { Data = runs };
                }
                else if (type == "S")
                {
                    if (states.Length > 0)
                    {
                        var subUrb = (from a in _db.Postcodes
                                      where (states == null || selected.Contains(a.State))
                                      select new
                                      {
                                          Type = a.Suburb,
                                          Limit = 0,
                                          Cutoff = ""
                                      }).Distinct().ToArray();
                        return new { Data = subUrb };
                    }
                    else
                    {
                        var subUrb = (from a in _db.CutoffGroupMasters
                                      join b in _db.OrderLimitMasters on a.GroupID equals b.GroupID into temp2
                                      from b in temp2.DefaultIfEmpty()
                                      join c in _db.OrderCutoffMasters on a.GroupID equals c.GroupID into temp
                                      from c in temp.DefaultIfEmpty()
                                      where a.GroupType == "Suburb"
                                      select new
                                      {
                                          Type = a.GroupName,
                                          Limit = b.OrderLimit ?? 0,
                                          Cutoff = c.CutoffTime ?? "",
                                          GID = a.GroupID,
                                          CutoffDate = c.CutoffDate,
                                      }).ToList();

                        return new { Data = subUrb };
                    }
                }
                else
                {
                    var pickUp = (from a in _db.DeliveryPickupMasters
                                  join b in _db.OrderLimitMasters on a.ID equals b.GroupID into temp2
                                  from b in temp2.DefaultIfEmpty()
                                  join c in _db.OrderCutoffMasters on a.ID equals c.GroupID into temp
                                  from c in temp.DefaultIfEmpty()
                                  select new
                                  {
                                      Type = a.PickupName + " (" + a.PickupAddress + ")",
                                      Limit = b.OrderLimit ?? 0,
                                      Cutoff = c.CutoffTime ?? "",
                                      CutoffDate = c.CutoffDate,
                                      GID = a.ID
                                  }).ToList();
                    return new { Data = pickUp };
                }
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        #endregion Get Postcode / suburbs / pickups / deliveryruns

        #region Set Order limit values

        public string SetOrderLimit(OrderLimitBO model)
        {
            try
            {
                model.Type = model.Type == "P" ? "Postcode" : model.Type == "D" ? "Delivery" : model.Type == "S" ? "Suburb" : "Pickup";
                var orderLimit = _db.OrderLimitMasters.Where(o => o.Type == model.Type && o.Value == model.Value).FirstOrDefault();

                if (orderLimit == null)
                {
                    var oLimit = new OrderLimitMaster();

                    oLimit.Type = model.Type;
                    oLimit.Value = model.Value;
                    oLimit.OrderLimit = model.Limit;
                    oLimit.CreatedOn = DateTime.Now;
                    oLimit.GroupID = model.ID;
                    _db.OrderLimitMasters.Add(oLimit);
                }
                else
                {
                    orderLimit.Type = model.Type;
                    orderLimit.Value = model.Value;
                    orderLimit.OrderLimit = model.Limit;
                    orderLimit.CreatedOn = DateTime.Now;
                    orderLimit.GroupID = model.ID;
                }

                _db.SaveChanges();

                return "Success";
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        #endregion Set Order limit values

        #region Save order cutoff times

        public string SaveOrderCutOff(CutoffTimeBO model)
        {
            try
            {
                string cutoffType = model.Type;
                model.Type = model.Type == "P" ? "Postcode" : model.Type == "D" ? "Delivery" : model.Type == "S" ? "Suburb" : "Pickup";
                var orderCutoff = _db.OrderCutoffMasters.Where(o => o.Type == model.Type && o.Value == model.Value).FirstOrDefault();
                var group = _db.CutoffGroupMasters.Find(model.ID);
                var noticemaster = _db.NoticeMasters.FirstOrDefault();

                byte days = 0;
                if (group != null && cutoffType != "U")
                {
                    days = group.Day.To<byte>();
                }
                string reqDate = this.GetRequiredDate(days, model.CutoffTime);

                if (orderCutoff == null)
                {
                    orderCutoff = new OrderCutoffMaster();

                    orderCutoff.Type = model.Type;
                    orderCutoff.Value = model.Value;
                    orderCutoff.CutoffTime = model.CutoffTime;
                    orderCutoff.CreatedOn = DateTime.Now;
                    orderCutoff.GroupID = model.ID;
                    orderCutoff.CutoffDate = reqDate;
                    _db.OrderCutoffMasters.Add(orderCutoff);
                }
                else
                {
                    orderCutoff.Type = model.Type;
                    orderCutoff.Value = model.Value;
                    orderCutoff.CutoffTime = model.CutoffTime;
                    orderCutoff.CreatedOn = DateTime.Now;
                    orderCutoff.GroupID = model.ID;
                    orderCutoff.CutoffDate = reqDate;
                }

                if (cutoffType != "U")
                {
                    noticemaster.CutoffType = cutoffType;
                    noticemaster.GroupID = orderCutoff.GroupID;
                    noticemaster.OrderCutOffDate = reqDate;
                }
                _db.SaveChanges();

                return "Success_" + noticemaster.OrderCutOffDate;
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        #endregion Save order cutoff times

        #region Save / Update / Remove / Get data for promo codes

        public object GetPromocodeData(int promoID)
        {
            try
            {
                if (promoID == 0)
                {
                    var prods = (from a in _db.ProductMasters
                                 select new
                                 {
                                     a.ProductID,
                                     a.ProductName
                                 }).ToList();

                    var customers = (from a in _db.CustomerMasters
                                     select new
                                     {
                                         a.CustomerID,
                                         a.CustomerName
                                     }).ToList();

                    var promocodes = _db.PromoCodeMasters.ToList();

                    return new
                    {
                        Customers = customers,
                        PromoCodes = promocodes,
                        Products = prods
                    };
                }
                else
                {
                    var promo = _db.PromoCodeMasters.Find(promoID);
                    return new
                    {
                        Promo = promo,
                        Prods = promo.Products != null ? Array.ConvertAll(promo.Products.Split(','), long.Parse) : new long[] { 0 }
                    };
                }
            }
            catch (Exception ex)
            {
                return new { Message = ex.Message };
            }
        }

        public string ManagePromoCodes(PromoCodeBO model)
        {
            try
            {
                var promoCode = new PromoCodeMaster();
                if (model.Type == "D")
                {
                    promoCode = _db.PromoCodeMasters.Where(p => p.ID == model.PromoID).FirstOrDefault();

                    _db.PromoCodeMasters.Remove(promoCode);
                    _db.SaveChanges();

                    return "Deleted";
                }
                if (model.PromoID > 0)
                {
                    promoCode = _db.PromoCodeMasters.Where(p => p.ID == model.PromoID).FirstOrDefault();

                    promoCode.CodeType = model.PromoType == "D" ? "Discount" : "Value";
                    promoCode.Code = model.PromoCode.ToUpper();
                    promoCode.StartDate = model.From;
                    promoCode.EndDate = model.To;
                    promoCode.CustomerID = model.CustomerID;
                    promoCode.MinCartValue = model.MinCartVal;
                    promoCode.Redemptions = model.Redemptions;
                    promoCode.Value = model.PromoValue;
                    promoCode.ModifiedOn = DateTime.Now;
                    promoCode.Products = model.Prods;
                    promoCode.Limit1PerCustomer = model.LimitPerCust;
                }
                else
                {
                    promoCode.CodeType = model.PromoType == "D" ? "Discount" : "Value";
                    promoCode.Code = model.PromoCode.ToUpper();
                    promoCode.StartDate = model.From;
                    promoCode.EndDate = model.To;
                    promoCode.CustomerID = model.CustomerID;
                    promoCode.MinCartValue = model.MinCartVal;
                    promoCode.Redemptions = model.Redemptions;
                    promoCode.Value = model.PromoValue;
                    promoCode.CreatedOn = DateTime.Now;
                    promoCode.Products = model.Prods;
                    promoCode.Limit1PerCustomer = model.LimitPerCust;
                    _db.PromoCodeMasters.Add(promoCode);
                }

                _db.SaveChanges();

                return "Success";
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        #endregion Save / Update / Remove / Get data for promo codes

        public string UpdateIndividualFeatures(string column, bool value)
        {
            try
            {
                int update = _db.Database.ExecuteSqlCommand("update dbo.NoticeMaster set " + column + " = '" + value + "'");

                if (update > 0)
                {
                    return "Success";
                }
                else
                {
                    return "Failed";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #region Manage Run Pop

        public string ManageRunPop(ManageModelPopBO model)
        {
            try
            {
                if (model.ModelType == "U")
                {
                    var pick = new DeliveryPickupMaster();

                    if (model.ID > 0)
                    {
                        pick = _db.DeliveryPickupMasters.Find(model.ID);
                        pick.PickupName = model.Name;
                        pick.PickupAddress = model.Address;
                        pick.ModifiedOn = DateTime.Now;
                    }
                    else
                    {
                        pick.PickupName = model.Name;
                        pick.PickupAddress = model.Address;
                        pick.CreatedOn = DateTime.Now;

                        _db.DeliveryPickupMasters.Add(pick);
                    }
                    _db.SaveChanges();
                }
                else
                {
                    CutoffGroupMaster obj = new CutoffGroupMaster();

                    obj.GroupName = model.Name;
                    obj.GroupDetail = model.Address;
                    obj.GroupType = model.ModelType == "P" ? "Postcode" : model.ModelType == "D" ? "Delivery" : model.ModelType == "S" ? "Suburb" : "Pickup";
                    obj.Day = Convert.ToByte(model.Days == "" ? "0" : model.Days);
                    obj.IsActive = true;
                    obj.CreatedDate = DateTime.Now;
                    _db.CutoffGroupMasters.Add(obj);
                    _db.SaveChanges();
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        public object GetAllGroupList(string groupType)
        {
            try
            {
                if (groupType == "U")
                {
                    var list = _db.DeliveryPickupMasters.ToList();
                    return new
                    {
                        GroupList = list
                    };
                }
                else
                {
                    groupType = groupType == "P" ? "Postcode" : groupType == "D" ? "Delivery" : groupType == "S" ? "Suburb" : "Pickup";
                    var groupList = _db.CutoffGroupMasters.Where(x => x.GroupType == groupType).ToList();

                    return new
                    {
                        GroupList = groupList
                    };
                }
            }
            catch (Exception ex)
            {
                return new { Message = ex.Message };
            }
        }

        public string AddUpdateGroupDetails(ManageModelPopDetailsBO model)
        {
            try
            {
                var groupDetail = _db.CutoffGroupDetailMasters.Where(x => x.GroupID == model.GroupID).FirstOrDefault();
                string type = model.Filter == "S" ? "Suburb" : "Postcode";
                if (groupDetail != null)
                {
                    if (groupDetail.GroupFilter == type)
                    {
                        var existing = groupDetail.ListValue.Split(',');
                        var newList = model.ListValue.TrimEnd(',').Split(',');

                        var difference = newList.Except(existing).ToArray();
                        string updatedVal = string.Join(",", existing) + "," + string.Join(",", difference);

                        // groupDetail.GroupFilter = type;
                        groupDetail.ChkFilterValue = model.ChkValue.TrimEnd(',');
                        groupDetail.ListValue = updatedVal;
                    }
                    else
                    {
                        groupDetail.GroupFilter = type;
                        groupDetail.ChkFilterValue = model.ChkValue.TrimEnd(',');
                        groupDetail.ListValue = model.ListValue.TrimEnd(',');
                    }

                    _db.SaveChanges();
                }
                else
                {
                    CutoffGroupDetailMaster obj = new CutoffGroupDetailMaster();

                    obj.GroupID = model.GroupID;
                    obj.GroupFilter = type; //model.Filter == "S" ? "Suburb" : "Postcode";
                    obj.ChkFilterValue = model.ChkValue.TrimEnd(',');
                    obj.ListValue = model.ListValue.TrimEnd(',');
                    _db.CutoffGroupDetailMasters.Add(obj);
                    _db.SaveChanges();
                }

                return "Success";
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        public object GetAllGroupDetailByID(int GroupID, string Type)
        {
            try
            {
                if (Type == "U")
                {
                    var pickup = _db.DeliveryPickupMasters.Find(GroupID);
                    return new
                    {
                        GroupDetailList = pickup
                    };
                }
                else
                {
                    var groupList = _db.CutoffGroupDetailMasters.Where(x => x.GroupID == GroupID).ToList();

                    return new
                    {
                        GroupDetailList = groupList
                    };
                }
            }
            catch (Exception ex)
            {
                return new { Message = ex.Message };
            }
        }

        public string DeleteGroupByID(int GroupID, string Type)
        {
            try
            {
                if (Type == "U")
                {
                    var pickUp = _db.DeliveryPickupMasters.Where(x => x.ID == GroupID).FirstOrDefault();

                    _db.DeliveryPickupMasters.Remove(pickUp);
                    _db.SaveChanges();
                }
                else
                {
                    var groupList = _db.CutoffGroupDetailMasters.Where(x => x.GroupID == GroupID).ToList();

                    if (groupList != null)
                    {
                        _db.CutoffGroupDetailMasters.RemoveRange(groupList);
                        _db.SaveChanges();

                        var obj = _db.CutoffGroupMasters.Where(x => x.GroupID == GroupID).FirstOrDefault();

                        if (obj != null)
                        {
                            _db.CutoffGroupMasters.Remove(obj);
                            _db.SaveChanges();
                        }
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        public string RemoveGroupDetails(ManageModelPopDetailsBO model)
        {
            try
            {
                var groupDetail = _db.CutoffGroupDetailMasters.Where(x => x.GroupID == model.GroupID).FirstOrDefault();

                if (groupDetail != null)
                {
                    List<string> existing = groupDetail.ListValue.Split(',').ToList(); ;
                    List<string> newList = model.ListValue.TrimEnd(',').Split(',').ToList();

                    foreach (var n in newList)
                    {
                        existing.Remove(n);
                    }

                    var updatedVal = string.Join(",", existing.Where(c => !string.IsNullOrWhiteSpace(c)));

                    groupDetail.ListValue = updatedVal;
                    _db.SaveChanges();
                }

                return "Success";
            }
            catch (Exception ex)
            {
                return "Error::" + ex.Message;
            }
        }

        #endregion Manage Run Pop

        #region Get order details to Edit

        /// GetRefundPaymentDetail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PaymentRefundMaster GetRefundDetail(long cartID)
        {
            try
            {
                var paymentRefundDetail = _db.PaymentRefundMasters.Where(i => i.CartID == cartID).FirstOrDefault();
                if (paymentRefundDetail != null)
                {
                    return paymentRefundDetail;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PromoCodeMaster GetPromoCodeDetails(string CouponName)
        {
            try
            {
                var promoCodes = _db.PromoCodeMasters.Where(i => i.Code.ToLower() == CouponName.ToLower()).FirstOrDefault();
                if (promoCodes != null)
                {
                    return promoCodes;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<OrderDetailModal> GetOrderDetails(long cartID)
        {
            try
            {
                using (var _db = new SaaviAdminEntities())
                {
                    var record = _db.trnsCartMasters.Where(i => i.CartID == cartID).FirstOrDefault();
                    var recordExist = _db.trnsCartItemsMasters.Where(i => i.CartID == cartID).ToList();
                    if (recordExist != null && recordExist.Count > 0)
                    {
                        var Exist = _db.trnsCartItemsMasterHistories.Where(i => i.CartID == cartID).FirstOrDefault();
                        if (Exist == null)
                        {
                            foreach (var val in recordExist)
                            {
                                var cartitems = new trnsCartItemsMasterHistory()
                                {
                                    CartID = val.CartID,
                                    CartItemID = val.CartItemID,
                                    Price = val.Price,
                                    ProductID = val.ProductID,
                                    CreatedDate = DateTime.Now,
                                    Quantity = val.Quantity,
                                    UnitId = val.UnitId,
                                    FrieghtCharges = (record != null && record.HasFreightCharges != null && record.HasFreightCharges == true) ? record.FrieghtCharges : 0
                                };
                                _db.trnsCartItemsMasterHistories.Add(cartitems);
                            }

                            _db.SaveChanges();
                        }
                    }

                    var res = (from a in _db.trnsCartMasters
                               join b in _db.trnsCartItemsMasters on a.CartID equals b.CartID
                               join c in _db.CustomerMasters on a.CustomerID equals c.CustomerID
                               join h in _db.trnsCartItemsMasterHistories on new { PropertyName1 = b.CartItemID, PropertyName2 = b.CartID } equals new { PropertyName1 = h.CartItemID, PropertyName2 = h.CartID } into tempHis
                               from h in tempHis.DefaultIfEmpty()
                               join p in _db.ProductMasters on b.ProductID equals p.ProductID
                               join u in _db.UnitOfMeasurementMasters on b.UnitId equals u.UOMID
                               join com in _db.DeliveryCommentsMasters on a.CommentID equals com.CommentID into temp
                               from com in temp.DefaultIfEmpty()
                               join add in _db.CustomerDeliveryAddresses on a.AddressID equals add.AddressID into tempAdd
                               from add in tempAdd.DefaultIfEmpty()
                               where a.CartID == cartID
                               select new OrderDetailModal
                               {
                                   CartID = a.CartID,
                                   OrderDate = a.OrderDate,
                                   RunNo = a.RunNo ?? "",
                                   Address = add.AddressID == null ? c.DeliveryAddress1 ?? (c.Address1 ?? "") + " " + (c.Address2 ?? "") + " " + (c.Suburb + " ") + c.PostCode : (add.Address1 ?? "") + " " + (add.Suburb ?? "") + " " + (add.ZIP ?? ""),
                                   Suburb = add.AddressID == null ? c.Suburb ?? "" : add.Suburb,
                                   ProductID = p.ProductID,
                                   ProductCode = p.ProductCode ?? "",
                                   ProductName = p.ProductName ?? "",
                                   UOM = u.UOMDesc ?? "",
                                   Price = b.Price,
                                   PickUpId = (a.PickupID != null) ? a.PickupID : 0,
                                   DeliveryType = a.DeliveryType,
                                   CustomerID = c.CustomerID,
                                   DeliveryName = add.ContactName ?? "",
                                   CustomerName = c.CustomerName ?? "",
                                   CustomerEmail = c.Email ?? "",
                                   CustomerPhone = c.Phone1 ?? "",
                                   Warehouse = add.AddressID != null ? (add.Warehouse == null ? c.Warehouse : add.Warehouse) : (a.Warehouse == null ? c.Warehouse ?? "" : a.Warehouse),
                                   CartItemID = b.CartItemID,
                                   Quantity = b.Quantity,
                                   OrderComments = (a.CommentID != null && a.CommentID > 0) ? com.CommentDescription : "",
                                   IsDelivery = a.IsDelivery ?? true,
                                   CreatedDate = a.CreatedDate,
                                   DeliveryNotes = a.CommentLine ?? "",
                                   RevisedPrice = h.Price,
                                   RevisedQuantity = h.Quantity,
                                   TotatRevisedPrice = b.Price * b.Quantity,
                                   TotatOriginalPrice = h.Price * h.Quantity,
                                   Difference = (b.Price * b.Quantity) - (h.Price * h.Quantity),
                                   CouponName = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.Coupon : "",
                                   CouponAmount = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.CouponAmount : 0,
                                   RevisedFreightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? a.FrieghtCharges : 0,
                                   OriginalFreightCharge = h.FrieghtCharges != null ? h.FrieghtCharges : 0,
                                   InvoiceNumber = a.InvoiceNumber != null ? " / " + a.InvoiceNumber : "",
                                   IsCouponItem = false,
                                   IsHasFreightCharges = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? true : false,
                                   AddressID = add.AddressID == null ? 0 : add.AddressID,
                                   DAddress1 = add.Address1 ?? "",
                                   DSuburb = add.Suburb ?? "",
                                   DZIP = add.ZIP ?? ""
                               }).ToList();

                    var RecordExist = _db.trnsCartMasterHistories.Where(i => i.CartID == cartID).FirstOrDefault();
                    if (record != null && RecordExist == null)
                    {
                        var history = new trnsCartMasterHistory();
                        history.CartID = record.CartID;
                        history.CommentLine = record.CommentLine ?? "";
                        history.RunNo = record.RunNo.Split('_')[0] ?? "";
                        history.OrderDate = record.OrderDate;
                        history.CustomerID = record.CustomerID;
                        history.UserID = record.UserID ?? 0;
                        history.Address1 = (res != null && res.Count > 0) ? res[0].Address ?? "" : "";
                        history.CommentDescription = (res != null && res.Count > 0) ? res[0].OrderComments ?? "" : "";
                        history.CreatedDate = DateTime.Now;
                        history.DeliveryType = record.IsDelivery == true ? "Delivery" : "Pick Up";
                        history.PickupID = record.PickupID;
                        _db.trnsCartMasterHistories.Add(history);
                        _db.SaveChanges();
                    }
                    return res;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion Get order details to Edit

        #region Get all Pick up address

        public List<DeliveryPickup> GetPickUpAddress()
        {
            try
            {
                var pickUp = (from p in _db.DeliveryPickupMasters
                              select new DeliveryPickup
                              {
                                  ID = p.ID,
                                  PickupAddress = p.PickupAddress,
                                  PickupName = p.PickupName
                              }).ToList();

                return pickUp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion Get all Pick up address

        #region Get all delivery runs

        public List<DeliveryRunsBO> GetDeliveryRuns()
        {
            try
            {
                var runs = (from a in _db.CutoffGroupMasters
                            where a.GroupType == "Delivery"
                            select new DeliveryRunsBO
                            {
                                RunNo = a.GroupName,
                                Warehouse = a.GroupName + "_" + a.GroupDetail
                            }).Distinct().ToList();

                //_db.CutoffGroupMasters.Where(x => x.GroupType == "Delivery").Select(d => d.GroupName).Distinct().ToList();

                return runs;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion Get all delivery runs

        #region Get products to add to cart

        public List<AddProductBO> GetProducts(long cartID)
        {
            try
            {
                var cartProducts = _db.trnsCartItemsMasters.Where(c => c.CartID == cartID).Select(s => s.ProductID).ToList();

                var remaining = (from p in _db.ProductMasters
                                 join u in _db.UnitOfMeasurementMasters on p.UOMID equals u.UOMID
                                 where !cartProducts.Contains(p.ProductID)
                                 select new AddProductBO
                                 {
                                     ProductID = p.ProductID,
                                     ProductCode = p.ProductCode,
                                     ProductName = p.ProductName,
                                     Price = p.Price,
                                     UOM = u.UOMDesc
                                 }).OrderByDescending(o => o.Price).ToList();

                return remaining;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion Get products to add to cart

        #region Set cutoff permitted days

        public string SetCutoffPermittedDays(SetCutoffPermittedDaysBO model)
        {
            try
            {
                var pDays = _db.CutoffPermittedDaysMasters.Where(p => p.GroupID == model.GroupID && p.CutoffType == model.PermitBy).FirstOrDefault();

                if (pDays == null)
                {
                    pDays = new CutoffPermittedDaysMaster();

                    pDays.GroupID = model.GroupID;
                    pDays.CutoffType = model.PermitBy;
                    pDays.PermitFrom = model.From;
                    pDays.PermitTo = model.To;
                    pDays.Days = model.Days;
                    pDays.CreatedDate = DateTime.Now;

                    _db.CutoffPermittedDaysMasters.Add(pDays);
                }
                else
                {
                    pDays.GroupID = model.GroupID;
                    pDays.CutoffType = model.PermitBy;
                    pDays.PermitFrom = model.From;
                    pDays.PermitTo = model.To;
                    pDays.Days = model.Days;
                }
                _db.SaveChanges();

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Set cutoff permitted days

        #region Add products to existing orders

        public string AddProductsToOrder(long cartID, List<AddProductsToOrderBO> products)
        {
            try
            {
                var cartItems = new List<trnsCartItemsMaster>();
                var pIds = products.Select(p => p.ProductID).ToList();
                var prods = (from a in _db.ProductMasters
                             where pIds.Contains(a.ProductID)
                             select new
                             {
                                 a.ProductID,
                                 a.UOMID,
                                 a.Price,
                                 //  b.Quantity,
                                 a.IsDonationBox
                             }).ToList();

                foreach (var item in prods)
                {
                    var cItem = new trnsCartItemsMaster();
                    var qty = products.Where(p => p.ProductID == item.ProductID).FirstOrDefault().Quantity;
                    cItem.CartID = cartID;
                    cItem.ProductID = item.ProductID;
                    cItem.UnitId = item.UOMID;
                    cItem.Price = item.Price;
                    cItem.Quantity = qty;
                    cItem.IsDonationBox = item.IsDonationBox;

                    cartItems.Add(cItem);
                }

                _db.trnsCartItemsMasters.AddRange(cartItems);
                _db.SaveChanges();

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Add products to existing orders

        public string DeleteCartItem(long cartItemID)
        {
            try
            {
                var item = _db.trnsCartItemsMasters.Find(cartItemID);

                _db.trnsCartItemsMasters.Remove(item);
                _db.SaveChanges();

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public object updateOrderDetails(UpdateOrderDetailsBO model)
        {
            try
            {
                var cart = _db.trnsCartMasters.Find(model.CartID);
                if (cart != null)
                {
                    if (model.OrderDate.ToShortDateString() == "0001-01-01")
                    {
                        return new { Result = "Please add valid required date.", Total = 0, PamentStatus = model.PaymentDone, CartId = model.CartID };
                    }
                }
                var validCheck = 1;
                var cartHistroyItems = _db.trnsCartItemsMasterHistories.Where(c => c.CartID == model.CartID).ToList();
                var editFreight = model.Items.Where(i => i.ProductID == -99).FirstOrDefault();

                if (editFreight != null && editFreight.Quantity != 0 && editFreight.Quantity != 1)
                {
                    return new { Result = "Please add valid Quantity for fright either 0 or 1.", Total = 0, PamentStatus = model.PaymentDone, CartId = model.CartID };
                }

                if (cartHistroyItems != null && cartHistroyItems.Count > 0)
                {
                    foreach (var item in cartHistroyItems)
                    {
                        var edit = model.Items.Where(i => i.ProductID == item.ProductID).FirstOrDefault();
                        if (item.Quantity < edit.Quantity)
                        {
                            validCheck = 2;
                            break;
                        }
                        if (item.Price < item.Price)
                        {
                            validCheck = 3;
                            break;
                        }
                    }

                    if (editFreight != null)
                    {
                        if (cartHistroyItems[0].FrieghtCharges < editFreight.Price)
                        {
                            validCheck = 4;
                        }
                    }
                }
                if (validCheck == 2)
                {
                    return new { Result = "Please add the less quantity than original value.", Total = 0, PamentStatus = model.PaymentDone, CartId = model.CartID };
                }
                else if (validCheck == 3)
                {
                    return new { Result = "Please add the less price than original price.", Total = 0, PamentStatus = model.PaymentDone, CartId = model.CartID };
                }

                bool IsCommentChanged = false;
                var comment = new DeliveryCommentsMaster();

                if (cart.CommentID != null && cart.CommentID > 0)
                {
                    comment = _db.DeliveryCommentsMasters.Find(cart.CommentID);
                    if (string.Compare(comment.CommentDescription, model.OrderComment, true) != 0)
                    {
                        IsCommentChanged = true;
                        comment.CommentDescription = model.OrderComment;
                        _db.SaveChanges();
                    }
                }
                else if ((cart.CommentID == null || cart.CommentID == 0) && (model.OrderComment != null && model.OrderComment.Length > 0))
                {
                    IsCommentChanged = true;
                    comment.CommentDescription = model.OrderComment;
                    comment.CompanyID = 1;
                    comment.CustomerID = cart.CustomerID;
                    comment.IsActive = true;
                    comment.CreatedDate = DateTime.Now;
                    _db.DeliveryCommentsMasters.Add(comment);
                    _db.SaveChanges();
                }

                if (cart != null)
                {
                    model.DeliveryNotes = !string.IsNullOrEmpty(model.DeliveryNotes) ? model.DeliveryNotes.Trim() : "";
                    model.OrderComment = !string.IsNullOrEmpty(model.OrderComment) ? model.OrderComment.Trim() : "";
                    var check = false;
                    var OldRecord = _db.trnsCartMasterHistories.Where(i => i.CartID == model.CartID && i.CustomerID == cart.CustomerID && i.UserID == cart.UserID && i.OrderDate == model.OrderDate && i.PickupID == model.PickupID).FirstOrDefault();
                    if (OldRecord == null)
                    {
                        check = true;
                    }
                    else
                    {
                        if (OldRecord != null)
                        {
                            if (OldRecord.CommentLine == model.DeliveryNotes)
                            {
                                if (OldRecord.CommentDescription == model.OrderComment)
                                {
                                    check = false;
                                }
                                else
                                {
                                    check = true;
                                }
                            }
                            else
                            {
                                check = true;
                            }
                        }
                    }
                    if (check == true)
                    {
                        var history = new trnsCartMasterHistory();
                        history.CartID = cart.CartID;
                        history.CommentLine = model.DeliveryNotes ?? "";
                        history.RunNo = model.RunNo?.Split('_')[0] ?? "";
                        history.OrderDate = model.OrderDate;
                        history.CustomerID = cart.CustomerID;
                        history.UserID = cart.UserID ?? 0;
                        history.Address1 = model.Address;
                        history.CommentDescription = model.OrderComment;
                        history.CreatedDate = DateTime.Now;
                        history.ModifiedDate = DateTime.Now;
                        history.DeliveryType = model.IsDelivery == true ? "Delivery" : "Pick Up";
                        history.PickupID = model.PickupID;
                        _db.trnsCartMasterHistories.Add(history);
                        _db.SaveChanges();
                    }
                    if (editFreight != null && editFreight.Quantity == 0)
                    {
                        if (cart.HasFreightCharges == true)
                        {
                            cart.FrieghtCharges = 0;
                        }
                    }
                    else if (editFreight != null && editFreight.Quantity != 0)
                    {
                        if (cart.HasFreightCharges == true)
                        {
                            cart.FrieghtCharges = editFreight.Price;
                        }
                    }
                    else
                    {
                        cart.FrieghtCharges = 0;
                    }
                    cart.OrderDate = model.OrderDate;
                    cart.IsDelivery = model.IsDelivery;
                    cart.DeliveryType = model.IsDelivery == true ? "Delivery" : "Pick Up";
                    cart.PickupID = model.PickupID;
                    cart.CommentLine = model.DeliveryNotes ?? "";
                    cart.RunNo = model.RunNo?.Split('_')[0] ?? "";
                    cart.Warehouse = model.RunNo?.Split('_')[1] ?? "";
                    cart.ModifiedDate = DateTime.Now;

                    if (comment != null)
                    {
                        cart.CommentID = comment.CommentID;
                    }

                    if (model.IsAddressChanged || model.RunNo?.Split('_')[0] != cart.RunNo)
                    {
                        var customer = _db.CustomerMasters.Find(cart.CustomerID);
                        customer.DeliveryAddress1 = model.Address;
                    }
                }
                _db.SaveChanges();

                var cartItems = _db.trnsCartItemsMasters.Where(c => c.CartID == model.CartID).ToList();
                double? Total = 0;
                double? Quantity = 0;
                if (cartItems != null && cartItems.Count > 0)
                {
                    foreach (var item in cartItems)
                    {
                        var edit = model.Items.Where(i => i.ProductID == item.ProductID).FirstOrDefault();

                        item.Price = edit.Price;
                        item.Quantity = edit.Quantity;

                        _db.SaveChanges();
                        Quantity += edit.Quantity;
                    }
                    Total = cartItems.Sum(s => s.Price * s.Quantity);
                }
                if (Quantity == 0)
                {
                    var cartLatest = _db.trnsCartMasters.Where(c => c.CartID == model.CartID).FirstOrDefault();
                    cartLatest.OrderStatus = 4;
                    cartLatest.SupplierOrderNumber = "Cancelled";
                    _db.SaveChanges();
                }
                else if (Quantity > 0)
                {
                    var cartLatest = _db.trnsCartMasters.Where(c => c.CartID == model.CartID).FirstOrDefault();
                    if (cartLatest.OrderDate != null && (Convert.ToDateTime(cartLatest.OrderDate).ToShortDateString() == getMST(DateTime.Now).ToShortDateString()))
                    {
                        cartLatest.OrderStatus = 2;
                        cartLatest.SupplierOrderNumber = "Delivering";
                    }
                    else if (cartLatest.OrderDate != null && Convert.ToDateTime(cartLatest.OrderDate).AddDays(1) < getMST(DateTime.Now))
                    {
                        cartLatest.OrderStatus = 5;
                        cartLatest.SupplierOrderNumber = "Delivered";
                    }
                    else
                    {
                        cartLatest.OrderStatus = 1;
                        cartLatest.SupplierOrderNumber = "Open";
                    }

                    _db.SaveChanges();
                }

                return new { Result = "Success", Total = Total, PamentStatus = model.PaymentDone, CartId = model.CartID };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<SP_GetOrdersToExport_Result> DownloadOrders(string orders)
        {
            try
            {
                var res = _db.SP_GetOrdersToExport(orders ?? "", null, null).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region Generate Invoice to send

        public string GenerateAndSendInvoice(string email, long cartID, bool isRefund, string path)
        {
            try
            {
                string res = "Success";
                double? refundAmount = 0;
                if (isRefund)
                {
                    refundAmount = _db.PaymentRefundMasters.Where(x => x.CartID == cartID).FirstOrDefault().RefundAmount;
                }

                var details = (from a in _db.trnsCartMasters
                               join b in _db.trnsCartItemsMasters on a.CartID equals b.CartID
                               join p in _db.ProductMasters on b.ProductID equals p.ProductID
                               join c in _db.CustomerMasters on a.CustomerID equals c.CustomerID
                               join u in _db.UnitOfMeasurementMasters on b.UnitId equals u.UOMID

                               join us in _db.UserMasters on a.UserID equals us.UserID into tempUser
                               from us in tempUser.DefaultIfEmpty()
                               join dc in _db.DeliveryCommentsMasters on a.CommentID equals dc.CommentID into tempCom
                               from dc in tempCom.DefaultIfEmpty()
                               join da in _db.CustomerDeliveryAddresses on a.AddressID equals da.AddressID into tempAdd
                               from da in tempAdd.DefaultIfEmpty()
                               join pc in _db.DeliveryProductCommentsMasters on b.CommentID equals pc.PCommentID into tempPcom
                               from pc in tempPcom.DefaultIfEmpty()
                               join dp in _db.DeliveryPickupMasters on a.PickupID equals dp.ID into tempPick
                               from dp in tempPick.DefaultIfEmpty()
                               where a.CartID == cartID
                               select new OrderBO
                               {
                                   CartID = a.CartID,
                                   ProductID = p.ProductID,
                                   OrderNo = a.ExtDoc ?? "",
                                   InvoiceNo = a.SenderReference ?? "",
                                   OrderDate = a.OrderDate,
                                   CreatedDate = a.CreatedDate,
                                   CustomerName = a.AddressID > 0 ? (da.ContactName ?? "") : (c.CustomerName ?? ""),
                                   ProductName = p.ProductName,
                                   ProductCode = p.ProductCode,
                                   Quantity = b.Quantity,
                                   Price = b.Price,
                                   UOMDesc = u.UOMDesc,
                                   Coupon = a.Coupon ?? "",
                                   IsCouponApplied = a.IsCouponApplied ?? false,
                                   CouponAmount = a.CouponAmount,
                                   HasFreightCharges = a.HasFreightCharges ?? false,
                                   IsGST = b.IsGstApplicable,
                                   Address1 = a.AddressID > 0 ? da.Address1 : "",
                                   Suburb = a.AddressID > 0 ? da.Suburb : "",
                                   Postcode = a.AddressID > 0 ? da.ZIP : "",
                                   State = a.AddressID > 0 ? (da.StateID > 0 ? _db.StateMasters.Where(x => x.StateID == da.StateID).FirstOrDefault().StateName : "") : "",
                                   MCustomerName = c.CustomerName,
                                   MAddress1 = c.Address1 ?? "",
                                   MSuburb = c.Suburb ?? "",
                                   MState = c.StateID > 0 ? _db.StateMasters.Where(x => x.StateID == c.StateID).FirstOrDefault().StateName : "",
                                   MPostcode = c.PostCode ?? "",
                                   FreightAmount = a.FrieghtCharges ?? 0,
                                   Comment = a.CommentID > 0 ? (dc.CommentDescription ?? "") : "",
                                   CommentLine = a.CommentLine ?? "",
                                   ProComment = b.CommentID > 0 ? (pc.CommentDescription ?? "") : "",
                                   MinOrderValue = _db.DeliveryCharges.FirstOrDefault().OrderValue ?? 0,
                                   IsDelivery = a.IsDelivery ?? true,
                                   PickAddress = a.IsDelivery == false ? _db.DeliveryPickupMasters.Where(x => x.ID == a.PickupID).FirstOrDefault().PickupAddress : ""
                               }).ToList();

                string retPath = "";
                if (details != null && details.Count > 0)
                {
                    retPath = GenerateInvoices.GeneratePDF(details, path, refundAmount, _db);

                    if (retPath != "")
                    {
                        var hTable = new Hashtable();
                        hTable["@CName"] = details[0].CustomerName;
                        hTable["@path"] = HelperClass.WBS_BASE_PATH;
                        hTable["@Company"] = HelperClass.CompanyName;

                        string sub = "Invoice for Order# SCF-HD-" + details[0].CartID.ToString();
                        string templateName = "Invoicemain.htm";
                        if (isRefund)
                        {
                            sub = "Refund " + sub;
                            templateName = "InvoiceRefund.htm";
                        }
                        SendMail.SendEMail(email, "", "", sub, HelperClass.TemplatePath, templateName, hTable, retPath, HelperClass.FROM_EMAILID);
                    }
                }
                else
                {
                    res = "No data found for the order";
                }

                return res;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Generate Invoice to send

        #region Update delivery Address

        public string updateDeliveryAddress(int AddressID, string deliveryaddress, string suburb, string zip)
        {
            try
            {
                var record = _db.CustomerDeliveryAddresses.Where(x => x.AddressID == AddressID).FirstOrDefault();

                if (record != null)
                {
                    record.Address1 = deliveryaddress;
                    record.Suburb = suburb;
                    record.ZIP = zip;

                    _db.SaveChanges();

                    return "Success";
                }
                else
                {
                    return "Record not found in database !!!";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Update delivery Address
    }
}