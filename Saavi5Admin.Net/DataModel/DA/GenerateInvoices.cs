﻿using CommonFunctions;
using DataModel.Model;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataModel.DA
{
    public class GenerateInvoices
    {
        public static string GeneratePDF(List<OrderBO> orData, string pdfPath, double? refund, SaaviAdminEntities _db)
        {
            try
            {
                string invoiceNo = "SCF-HD-" + orData[0].CartID.ToString();

                var Pdf = new Document();
                Pdf.DefaultPageSetup.PageFormat = PageFormat.A4;
                Pdf.DefaultPageSetup.Orientation = Orientation.Portrait;
                Pdf.DefaultPageSetup.TopMargin = 5;
                Pdf.DefaultPageSetup.LeftMargin = 15;
                Pdf.DefaultPageSetup.RightMargin = 15;
                Pdf.DefaultPageSetup.BottomMargin = 5;
                Pdf.DefaultPageSetup.DifferentFirstPageHeaderFooter = true;

                Pdf.AddSection();

                #region Styles

                MigraDoc.DocumentObjectModel.Style style = Pdf.Styles["Normal"];
                style.Font.Name = "Calibri";

                style = Pdf.Styles.AddStyle("H1-20", "Normal");
                style.Font.Size = 20;
                style.Font.Bold = true;
                style.Font.Color = HelperClass.TextColorFromHtml("#671f75");

                style = Pdf.Styles.AddStyle("H2-8", "Normal");
                style.Font.Size = 8;

                style = Pdf.Styles.AddStyle("H2-8-Blue", "H2-8");
                style.ParagraphFormat.Font.Color = Colors.Blue;

                style = Pdf.Styles.AddStyle("H2-8B", "H2-8");
                style.Font.Bold = true;

                style = Pdf.Styles.AddStyle("H2-9", "Normal");
                style.Font.Size = 9;

                style = Pdf.Styles.AddStyle("H2-9-Grey", "H2-9");
                style.Font.Color = Colors.Gray;

                style = Pdf.Styles.AddStyle("H2-9-Red", "H2-9");
                style.Font.Color = Colors.Red;

                style = Pdf.Styles.AddStyle("H2-9B", "H2-9");
                style.Font.Bold = true;

                style = Pdf.Styles.AddStyle("H2-9B-Inverse", "H2-9B");
                style.ParagraphFormat.Font.Color = Colors.White;

                style = Pdf.Styles.AddStyle("H2-9B-Color", "H2-9B");
                style.Font.Color = HelperClass.TextColorFromHtml("#000");

                style = Pdf.Styles.AddStyle("H2-10", "Normal");
                style.Font.Size = 10;

                style = Pdf.Styles.AddStyle("H2-10B", "H2-10");
                style.Font.Bold = true;

                style = Pdf.Styles.AddStyle("H2-10B-Red", "H2-10B");
                style.Font.Color = Colors.Red;

                #endregion Styles

                #region Header section for the logo

                HeaderFooter header = Pdf.LastSection.Headers.FirstPage;
                double thirdWidth = Pdf.PageWidth() / 3;
                double halfWidth = Pdf.PageWidth() / 2;

                Table table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Center, Pdf.PageWidth());

                Row row = table.AddRow();
                Image image = row.Cells[0].AddImage(@ConfigurationManager.AppSettings["LogoPath"].ToString() + "headerInv.jpg");
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                image.Height = 100;
                image.Width = Pdf.PageWidth();
                image.RelativeVertical = RelativeVertical.Line;
                image.RelativeHorizontal = RelativeHorizontal.Margin;
                image.Top = ShapePosition.Top;

                row = table.AddRow();
                row.Cells[0].Add(HelperClass.SpacerPara);

                table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Center, Pdf.PageWidth());
                row = table.AddRow();
                TextFrame frame = row.Cells[0].AddTextFrame();
                frame.Height = 36;
                Table subTable = frame.AddTable();
                subTable.AddColumn(halfWidth);
                subTable.AddColumn(halfWidth);

                row = subTable.AddRow();
                //row.TopPadding = 2;
                //row.BottomPadding = 2;
                //row.Height = 10;
                Paragraph para = row.Cells[0].AddParagraph("TAX INVOICE " + invoiceNo, ParagraphAlignment.Center);
                para.Format.Font.Size = 22;
                para.Format.Shading.Color = Colors.Black;
                para.Format.Font.Color = Colors.White;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Top;

                //row.Cells[0].Shading.Color = Colors.Black;
                //row.Cells[0].Format.Font.Color = Colors.White;

                frame = row.Cells[1].AddTextFrame();
                frame.MarginLeft = 15;
                subTable = frame.AddTable();
                subTable.Borders.Top = HelperClass.BorderLineThin;
                subTable.Borders.Bottom = HelperClass.BorderLineThin;
                subTable.Borders.Right = HelperClass.BorderLineThin;
                subTable.Borders.Left = HelperClass.BorderLineThin;
                subTable.AddColumn((halfWidth - 10) / 3);
                subTable.AddColumn((halfWidth - 10) / 3);
                subTable.AddColumn((halfWidth - 10) / 3);

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("Invoice Date", ParagraphAlignment.Center).Format.Font.Bold = true;
                row.Cells[0].Borders.Bottom.Visible = false;

                row.Cells[1].AddParagraph("Account Code", ParagraphAlignment.Center).Format.Font.Bold = true;
                row.Cells[1].Borders.Bottom.Visible = false;

                row.Cells[2].AddParagraph("Page", ParagraphAlignment.Center).Format.Font.Bold = true;
                row.Cells[2].Borders.Bottom.Visible = false;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph(orData[0].OrderDate.To<DateTime>().ToString("dd/MM/yyyy"), ParagraphAlignment.Center);
                row.Cells[0].Borders.Top.Visible = false;

                row.Cells[1].AddParagraph("RETAIL", ParagraphAlignment.Center);
                row.Cells[1].Borders.Top.Visible = false;

                var parag = row.Cells[2].AddParagraph();
                parag.Format.Alignment = ParagraphAlignment.Center;
                parag.Style = "H2-8";
                parag.AddText("Page ");
                parag.AddPageField();
                parag.AddText(" of ");
                parag.AddNumPagesField();
                row.Cells[2].Borders.Top.Visible = false;
                // Start the table for invoice details
                table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Left, halfWidth);
                table.AddColumn(ParagraphAlignment.Left, halfWidth);

                #region Invoice details section

                row = table.AddRow();
                frame = row.Cells[0].AddTextFrame();
                var frame2 = row.Cells[1].AddTextFrame();

                subTable = frame.AddTable();
                subTable.AddColumn(80);
                subTable.AddColumn(halfWidth - 80);

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("Invoice To", ParagraphAlignment.Left).Format.Font.Size = 12;
                row.Cells[0].Borders.Top = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[0].MergeRight = 1;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].CustomerName, ParagraphAlignment.Left).Format.Font.Size = 12; ;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].MAddress1, ParagraphAlignment.Left).Format.Font.Size = 12;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].MSuburb + ", " + orData[0].MPostcode, ParagraphAlignment.Left).Format.Font.Size = 12; ;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].AddParagraph(orData[0].MState ?? "", ParagraphAlignment.Left).Format.Font.Size = 12; ;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;

                row = subTable.AddRow();
                row.TopPadding = 3;
                row.BottomPadding = 2;
                row.Cells[0].AddParagraph("");
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Bottom = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Bottom = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;

                #endregion Invoice details section

                #region Delivery Instructions section

                string comments = (orData[0].Comment ?? "") + (orData[0].CommentLine == "" ? "" : " || " + orData[0].CommentLine);

                if (comments.Length > 140)
                {
                    comments = comments.Substring(0, 140) + ".....";
                }

                row = table.AddRow();
                // frame = row.Cells[1].AddTextFrame();
                frame2.MarginLeft = 15;
                double subWidth = halfWidth - 10;
                subTable = frame2.AddTable();

                subTable.AddColumn(ParagraphAlignment.Right, subWidth - 80);
                subTable.AddColumn(ParagraphAlignment.Right, 80);

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("Delivery Instructions", ParagraphAlignment.Left).Format.Font.Size = 12;
                row.Cells[0].Borders.Top = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].AddParagraph("", ParagraphAlignment.Right).Format.Font.Size = 12;
                row.Cells[1].Borders.Top = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                //row.Cells[0].MergeRight = 1;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph(comments, ParagraphAlignment.Justify).Format.Font.Size = 8;
                row.Cells[0].MergeRight = 1;
                row.Cells[0].MergeDown = 1;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Height = 73;

                row = subTable.AddRow();
                row.Cells[0].AddParagraph("");
                row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                row.Cells[1].Borders.Bottom = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Bottom = HelperClass.BorderLineThin;

                #endregion Delivery Instructions section

                table = header.AddTable();
                table.AddColumn(ParagraphAlignment.Left, Pdf.PageWidth() + 5);

                row = table.AddRow();

                row.Cells[0].AddParagraph("");

                row = table.AddRow();
                if (orData[0].IsDelivery == false && orData[0].PickAddress != "")
                {
                    row.Cells[0].AddParagraph("Reference: " + orData[0].PickAddress, ParagraphAlignment.Left);
                }
                else
                {
                    row.Cells[0].AddParagraph("Reference: " + orData[0].MCustomerName + ", " + orData[0].MAddress1 + ", " + orData[0].MSuburb + ", " + orData[0].Postcode + ", " + orData[0].MState, ParagraphAlignment.Left);
                }

                //row.Cells[0].MergeRight = 1;
                row.Borders.Visible = true;
                row.Borders.Width = 1;

                #endregion Header section for the logo

                #region Items table

                var section2 = Pdf.LastSection;

                section2.PageSetup.TopMargin = 300;
                var mainWidth = Pdf.PageWidth() - 50;

                var tableItems = section2.AddTable();
                tableItems.Borders.Visible = true;
                tableItems.Borders.Width = 1;
                tableItems.TopPadding = 5;
                tableItems.BottomPadding = 5;

                tableItems.AddColumn(mainWidth * .10); // 20 % of the page width and so on
                tableItems.AddColumn(mainWidth * .41);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);
                tableItems.AddColumn(mainWidth * .12);

                row = tableItems.AddRow();
                row.HeadingFormat = true;

                row.Shading.Color = HelperClass.TextColorFromHtml("#c7cbcf");
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph("Code", ParagraphAlignment.Left, "H2-9B");
                row.Cells[1].AddParagraph("Description", ParagraphAlignment.Left, "H2-9B");
                row.Cells[2].AddParagraph("Order", ParagraphAlignment.Center, "H2-9B");
                row.Cells[3].AddParagraph("Supply", ParagraphAlignment.Center, "H2-9B");
                row.Cells[4].AddParagraph("Unit", ParagraphAlignment.Center, "H2-9B");
                row.Cells[5].AddParagraph("Unit Price", ParagraphAlignment.Right, "H2-9B");
                row.Cells[6].AddParagraph("Total", ParagraphAlignment.Right, "H2-9B");

                var itemhist = new List<trnsCartItemsMasterHistory>();
                var refundData = new PaymentRefundMaster();
                if (refund > 0)
                {
                    var cartid = orData[0].CartID;
                    itemhist = _db.trnsCartItemsMasterHistories.Where(x => x.CartID == cartid).ToList();
                    // refundData = _db.PaymentRefundMasters.Where(x => x.CartID == cartid).FirstOrDefault();
                }

                double? sum = 0;
                double? gst = 0;
                double? promo = 0;
                double? freight = 0;
                foreach (var item in orData)
                {
                    double? total = 0;
                    double? price = 0;
                    double? qty = 0;

                    row = tableItems.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].AddParagraph(item.ProductCode, ParagraphAlignment.Left, "H2-9");
                    row.Cells[1].AddParagraph((item.IsGST == true ? "(*)" : "") + item.ProductName, ParagraphAlignment.Left);
                    if (item.ProComment.Trim().Length > 0)
                    {
                        row.Cells[1].AddParagraph(item.ProComment, ParagraphAlignment.Left, "H2-9-Red");
                    }

                    row.Cells[4].AddParagraph(item.UOMDesc, ParagraphAlignment.Center, "H2-9");

                    if (refund > 0)
                    {
                        var hisItem = itemhist.Where(i => i.ProductID == item.ProductID).FirstOrDefault();
                        if (hisItem.Quantity != item.Quantity)
                        {
                            total = hisItem.Quantity * hisItem.Price;
                            price = hisItem.Price;
                            qty = hisItem.Quantity;
                            row.Cells[1].Style = "H2-9-Red";
                            row.Cells[2].AddParagraph(hisItem.Quantity.ToString(), ParagraphAlignment.Center, "H2-9");
                            row.Cells[3].AddParagraph(hisItem.Quantity.ToString(), ParagraphAlignment.Center, "H2-9");
                            row.Cells[5].AddParagraph("-" + string.Format("{0:#,0.00}", price), ParagraphAlignment.Right, "H2-9-Red");
                            row.Cells[6].AddParagraph(string.Format("{0:#,0.00}", 0), ParagraphAlignment.Right, "H2-9");
                        }
                        else
                        {
                            total = item.Quantity * item.Price;
                            price = item.Price;
                            qty = item.Quantity;
                            row.Cells[1].Style = "H2-9";
                            row.Cells[2].AddParagraph(qty.ToString(), ParagraphAlignment.Center, "H2-9");
                            row.Cells[3].AddParagraph(qty.ToString(), ParagraphAlignment.Center, "H2-9");
                            row.Cells[5].AddParagraph(string.Format("{0:#,0.00}", price), ParagraphAlignment.Right, "H2-9");
                            row.Cells[6].AddParagraph(string.Format("{0:#,0.00}", total), ParagraphAlignment.Right, "H2-9");
                        }
                    }
                    else
                    {
                        total = item.Quantity * item.Price;
                        price = item.Price;
                        qty = item.Quantity;
                        row.Cells[1].Style = "H2-9";
                        row.Cells[2].AddParagraph(qty.ToString(), ParagraphAlignment.Center, "H2-9");
                        row.Cells[3].AddParagraph(qty.ToString(), ParagraphAlignment.Center, "H2-9");
                        row.Cells[5].AddParagraph(string.Format("{0:#,0.00}", price), ParagraphAlignment.Right, "H2-9");
                        row.Cells[6].AddParagraph(string.Format("{0:#,0.00}", total), ParagraphAlignment.Right, "H2-9");
                    }

                    sum = sum + total;
                    if (item.IsGST == true)
                    {
                        gst = gst + (total * .1);
                    }
                }

                if (orData[0].HasFreightCharges)
                {
                    row = tableItems.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].AddParagraph("CHARGE", ParagraphAlignment.Left, "H2-9");
                    row.Cells[1].AddParagraph("Delivery Charges", ParagraphAlignment.Left, "H2-9");
                    row.Cells[2].AddParagraph("1", ParagraphAlignment.Center, "H2-9");
                    row.Cells[3].AddParagraph("1", ParagraphAlignment.Center, "H2-9");
                    row.Cells[4].AddParagraph("Fee", ParagraphAlignment.Center, "H2-9");

                    if (refund > 0)
                    {
                        row.Cells[5].AddParagraph("-" + string.Format("{0:#,0.00}", itemhist[0].FrieghtCharges), ParagraphAlignment.Right, "H2-9-Red");
                        row.Cells[6].AddParagraph(string.Format("{0:#,0.00}", 0), ParagraphAlignment.Right, "H2-9");
                    }
                    else
                    {
                        row.Cells[5].AddParagraph(string.Format("{0:#,0.00}", orData[0].FreightAmount), ParagraphAlignment.Right, "H2-9");
                        row.Cells[6].AddParagraph(string.Format("{0:#,0.00}", orData[0].FreightAmount), ParagraphAlignment.Right, "H2-9");
                    }
                }

                #endregion Items table

                #region Footer section

                HeaderFooter footer = section2.Footers.FirstPage;
                //section2.PageSetup.DifferentFirstPageHeaderFooter = true;

                var tableFooter = footer.AddTable();
                tableFooter.AddColumn(ParagraphAlignment.Left, Pdf.PageWidth() / 2);
                tableFooter.AddColumn(ParagraphAlignment.Right, Pdf.PageWidth() / 2);

                row = tableFooter.AddRow();
                row.Cells[0].MergeRight = 1;
                var pFooter = row.Cells[0].AddParagraph("");
                //pFooter.AddFormattedText("Notes: ").Font.Bold = true;
                //pFooter.AddText(" CUSTOMERS PLEASE NOTE: Please note cut off for next day deliveries is midnight.");
                row.BottomPadding = 1;
                //row.Cells[0].Borders.Top = HelperClass.BorderLineThin;
                //row.Cells[0].Borders.Left = HelperClass.BorderLineThin;
                //row.Cells[1].Borders.Right = HelperClass.BorderLineThin;
                row.Cells[0].Borders.Bottom = HelperClass.BorderLineThin;

                row = tableFooter.AddRow();
                row.Cells[0].Add(HelperClass.SpacerPara);

                row = tableFooter.AddRow();
                TextFrame frame3 = row.Cells[0].AddTextFrame();

                Table sub = frame3.AddTable();
                sub.AddColumn((mainWidth / 2) / 2);
                sub.AddColumn((mainWidth / 2) / 2);

                var row1 = sub.AddRow();
                row1.Cells[0].MergeRight = 1;
                row1.Cells[0].AddParagraph("No claim will be recognized if not advised within 24 hours of receipt of goods", ParagraphAlignment.Left, "H2-8");

                row1 = sub.AddRow();
                row1.Cells[0].MergeRight = 1;
                row1.Cells[0].AddParagraph("Delivery fee applies to all orders under $" + string.Format("{0:#,0.00}", orData[0].MinOrderValue), ParagraphAlignment.Left, "H2-8");

                row1 = sub.AddRow();
                row1.Cells[0].MergeRight = 1;
                row1.Cells[0].AddParagraph("(*) INDICATES TAXABLE ITEM", ParagraphAlignment.Left, "H2-8");

                //row1 = sub.AddRow();
                //row1.Cells[0].MergeRight = 1;
                //row1.Cells[0].Add(HelperClass.SpacerPara);

                var frame4 = row.Cells[1].AddTextFrame();
                frame4.MarginLeft = 23;
                sub = frame4.AddTable();
                sub.AddColumn((Pdf.PageWidth() / 2) / 3);
                sub.AddColumn((Pdf.PageWidth() / 2) / 3);
                sub.AddColumn((Pdf.PageWidth() / 2) / 4);

                sub.Borders.Top = HelperClass.BorderLineThin;
                sub.Borders.Right = HelperClass.BorderLineThin;
                sub.Borders.Bottom = HelperClass.BorderLineThin;
                sub.Borders.Left = HelperClass.BorderLineThin;

                var row2 = sub.AddRow();
                var pTerms = row2.Cells[0].AddParagraph("Terms: COD");
                pTerms.Format.Font.Bold = true;
                pTerms.Format.Font.Size = 14;
                row2.Cells[0].MergeDown = 2;
                row2.Cells[1].AddParagraph("GST", ParagraphAlignment.Right, "H2-10B");
                row2.Cells[2].AddParagraph("$" + string.Format("{0:#,0.00}", 0), ParagraphAlignment.Right, "H2-10B");
                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                row2 = sub.AddRow();
                row2.Cells[1].AddParagraph("Promo Code", ParagraphAlignment.Right, "H2-10B");
                if (orData[0].IsCouponApplied)
                {
                    promo = orData[0].CouponAmount;
                    row2.Cells[2].AddParagraph("-$" + string.Format("{0:#,0.00}", orData[0].CouponAmount), ParagraphAlignment.Right, "H2-10B");
                }
                else
                {
                    row2.Cells[2].AddParagraph("$0.00", ParagraphAlignment.Right, "H2-10B");
                }

                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                row2 = sub.AddRow();
                row2.Cells[1].AddParagraph("Delivery Charges", ParagraphAlignment.Right, "H2-10B");
                if (orData[0].HasFreightCharges)
                {
                    freight = orData[0].FreightAmount;
                    if (refund > 0 && orData[0].FreightAmount == 0)
                    {
                        freight = itemhist[0].FrieghtCharges;
                        row2.Cells[2].AddParagraph("-$" + string.Format("{0:#,0.00}", freight), ParagraphAlignment.Right, "H2-10B-Red");
                    }
                    else
                    {
                        row2.Cells[2].AddParagraph("$" + string.Format("{0:#,0.00}", orData[0].FreightAmount), ParagraphAlignment.Right, "H2-10B");
                    }
                }
                else
                {
                    row2.Cells[2].AddParagraph("$0.00", ParagraphAlignment.Right, "H2-10B");
                }
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                if (refund > 0)
                {
                    row2 = sub.AddRow();
                    row2.Cells[1].AddParagraph("Refund Amount", ParagraphAlignment.Right, "H2-10B");
                    row2.Cells[2].AddParagraph("-$" + string.Format("{0:#,0.00}", refund - (itemhist[0].FrieghtCharges ?? 0)), ParagraphAlignment.Right, "H2-10B-Red");
                    row2.Cells[0].Borders.Left.Visible = false;
                    row2.Cells[0].Borders.Top.Visible = false;
                    row2.Cells[0].Borders.Bottom.Visible = false;
                }

                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                row2 = sub.AddRow();
                row2.Cells[1].AddParagraph("Invoice Total", ParagraphAlignment.Right, "H2-10B");
                row2.Cells[2].AddParagraph("$" + string.Format("{0:#,0.00}", ((sum + freight + gst) - promo) - refund), ParagraphAlignment.Right, "H2-10B");

                row2.BottomPadding = 1;
                row2.Cells[0].Borders.Left.Visible = false;
                row2.Cells[0].Borders.Top.Visible = false;
                row2.Cells[0].Borders.Bottom.Visible = false;

                #endregion Footer section

                string name = "SCF-HD-" + orData[0].CartID.ToString() + ".pdf";
                if (refund > 0)
                {
                    name = "SCF-HD-" + orData[0].CartID.ToString() + "_Refund.pdf";
                }
                string pdfGenPath = @pdfPath + name;

                var fInfo = new FileInfo(pdfGenPath);
                if (fInfo.Exists)
                {
                    fInfo.Delete();
                }

                PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfFontEmbedding.Always);
                renderer.Document = Pdf;
                renderer.RenderDocument();
                renderer.PdfDocument.Save(pdfGenPath);

                return pdfGenPath;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }

    public static class HelperClass
    {
        public static string CompanyName = Convert.ToString(ConfigurationManager.AppSettings["CompanyName"]);
        public static string WBS_BASE_PATH = Convert.ToString(ConfigurationManager.AppSettings["WBS_BASE_PATH"]);
        public static string TemplatePath = Convert.ToString(ConfigurationManager.AppSettings["TemplatePath"]);
        public static string FROM_EMAILID = Convert.ToString(ConfigurationManager.AppSettings["FROM_EMAILID"]);
        public static string InvoiceTitle = Convert.ToString(ConfigurationManager.AppSettings["InvoiceTitle"]);
        public static int InvoiceLogoHeight = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceLogoHeight"]);
        public static int InvoiceLogoWidth = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceLogoWidth"]);

        #region MigraDoc Helpers

        public static string FormatNumber<T>(T number, int maxDecimals = 4)
        {
            return Regex.Replace(String.Format("{0:n" + maxDecimals + "}", number),
                                 @"[" + System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator + "]?0+$", "");
        }

        public static double PageWidth(this Section section)
        {
            return (PageWidth(section.Document));
        }

        public static double PageWidth(this Document document)
        {
            Unit width, height;

            PageSetup.GetPageSize(document.DefaultPageSetup.PageFormat, out width, out height);
            if (document.DefaultPageSetup.Orientation == Orientation.Landscape)
                Swap<Unit>(ref width, ref height);

            return (width.Point - document.DefaultPageSetup.LeftMargin.Point - document.DefaultPageSetup.RightMargin.Point);
        }

        public static Column AddColumn(this Table table, ParagraphAlignment align, Unit unit = new Unit())
        {
            Column column = table.AddColumn();
            column.Width = unit;
            column.Format.Alignment = align;
            return column;
        }

        public static Paragraph AddParagraph(this TextFrame frame, string text, ParagraphAlignment align, string style = null)
        {
            return frame.AddParagraph().AddText(text, align, style);
        }

        public static Paragraph AddParagraph(this Cell cell, string text, ParagraphAlignment align, string style = null)
        {
            return cell.AddParagraph().AddText(text, align, style);
        }

        private static Paragraph AddText(this Paragraph paragraph, string text, ParagraphAlignment align, string style = null)
        {
            paragraph.Format.Alignment = align;
            if (style == null)
                paragraph.AddText(text);
            else
                paragraph.AddFormattedText(text, style);
            return paragraph;
        }

        public static string ToCurrency(this decimal number)
        {
            return ToCurrency(number, string.Empty);
        }

        public static string ToCurrency(this decimal number, string symbol)
        {
            return string.Format("{0}{1:N2}", symbol, number);
        }

        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T tmp = lhs; lhs = rhs; rhs = tmp;
        }

        public static Color TextColorFromHtml(string hex)
        {
            if (hex == null)
                return Colors.Black;
            else
                return new Color((uint)System.Drawing.ColorTranslator.FromHtml(hex).ToArgb());
        }

        public static Border BorderLine
        {
            get
            {
                Border bottomLine = new Border();
                bottomLine.Width = new Unit(2.5);
                bottomLine.Color = HelperClass.TextColorFromHtml("#000000");
                return bottomLine;
            }
        }

        public static Border BorderLineThin
        {
            get
            {
                Border bottomLine = new Border();
                bottomLine.Width = new Unit(1);
                bottomLine.Color = HelperClass.TextColorFromHtml("#000000");
                return bottomLine;
            }
        }

        public static Paragraph SpacerPara
        {
            get
            {
                Paragraph paragraph = new Paragraph();
                paragraph.Format.LineSpacingRule = MigraDoc.DocumentObjectModel.LineSpacingRule.Exactly;
                paragraph.Format.LineSpacing = MigraDoc.DocumentObjectModel.Unit.FromMillimeter(0.0);
                return paragraph;
            }
        }

        #endregion MigraDoc Helpers

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }

    public class OrderBO
    {
        public long CartID { get; set; }
        public string OrderNo { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CustomerName { get; set; }
        public string AlphaCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string UOMDesc { get; set; }
        public double? Quantity { get; set; }
        public double? Price { get; set; }
        public string CustType { get; set; }
        public bool HasFreightCharges { get; set; }
        public double? FreightAmount { get; set; }
        public bool IsCouponApplied { get; set; }
        public double? CouponAmount { get; set; }
        public bool? IsGST { get; set; }
        public string ProComment { get; set; }
        public string Comment { get; set; }
        public string CommentLine { get; set; }
        public string MCustomerName { get; set; }
        public string MAddress1 { get; set; }
        public string MSuburb { get; set; }
        public string MPostcode { get; set; }
        public string State { get; set; }
        public string MState { get; set; }
        public double MinOrderValue { get; set; }
        public string Coupon { get; set; }
        public bool? IsDelivery { get; set; }
        public string PickAddress { get; set; }
        public long ProductID { get; set; }
    }
}