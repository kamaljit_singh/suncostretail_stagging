﻿using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonFunctions;
using AutoMapper;
using System.Security.Cryptography;
using System.Data;
using System.Configuration;
using System.Web.Hosting;
using System.IO;

namespace DataModel.DA
{
    public class CompanyDA : CommonDA
    {
        private readonly SaaviAdminEntities _db;

        public CompanyDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region

        /// <summary>
        /// Get all customer from the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<AllCustomerBO> GetAllCustomers(GetCustomersModel model)
        {
            try
            {
                var data = (from cust in _db.CustomerMasters
                            where cust.IsRep == model.IsRep &&
                            (model.SearchText == "" ? true : cust.CustomerName.ToLower().Contains(model.SearchText.ToLower())
                             || cust.ContactName.ToLower().Contains(model.SearchText.ToLower())
                             || cust.Email.ToLower().Contains(model.SearchText.ToLower())
                             || cust.AlphaCode.ToLower().Contains(model.SearchText.ToLower())
                             || cust.Address1.ToLower().Contains(model.SearchText.ToLower())
                             || model.SearchText == "" || model.SearchText == null)
                            select new AllCustomerBO
                            {
                                CustomerID = cust.CustomerID,
                                CustomerName = cust.CustomerName
                            }).ToList();
                if (model.PageSize > 0)
                {
                    data = data.OrderBy(c => c.CustomerName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
                }
                else
                {
                    data = data.OrderBy(c => c.CustomerName).ToList();
                }
                return data.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get all customer count from the database
        /// </summary>
        /// <returns></returns>
        public AllCustomerCountBO GetTotalCustomers()
        {
            try
            {
                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var totalCustomersLastMonth = _db.CustomerMasters.Where(i => i.CreatedDate < firstDayOfMonth && i.IsActive == true && (i.IsDeleted == false || i.IsDeleted == null)).Count();
                var totalCustomersThisMonth = _db.CustomerMasters.Where(i => i.CreatedDate < DateTime.Now && i.IsActive == true && (i.IsDeleted == false || i.IsDeleted == null)).Count();

                return new AllCustomerCountBO
                {
                    LastMonth = totalCustomersLastMonth,
                    ThisMonth = totalCustomersThisMonth
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get customer detail by ID
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public CustomerBO GetCustomerByID(long customerID, string searchText = null)
        {
            var data = (from cus in _db.CustomerMasters
                        join debt in _db.DebtorTypeMasters on cus.DebtorTypeID equals debt.DebtorTypeId into temp
                        from debt in temp.DefaultIfEmpty()

                        join term in _db.TermMasters on cus.TermID equals term.TermID into temp1
                        from term in temp1.DefaultIfEmpty()

                        where cus.CustomerID == customerID && (cus.CustomerName.Contains(searchText) || searchText == null)
                        select new CustomerBO
                        {
                            CustomerID = cus.CustomerID,
                            CustomerName = cus.CustomerName,
                            ABN = cus.ABN,
                            Address1 = cus.Address1,
                            Address2 = cus.Address2,
                            Suburb = cus.Suburb,
                            City = cus.City,
                            StateID = cus.StateID ?? 0,
                            State = cus.StateID == null ? "0" : _db.StateMasters.Where(c => c.StateID == cus.StateID).FirstOrDefault().StateName,
                            CountryID = cus.CountryID ?? 0,
                            Country = cus.CountryID == null ? "0" : _db.CountryMasters.Where(c => c.CountryID == cus.CountryID).FirstOrDefault().CountryName,
                            PostCode = cus.PostCode,
                            ContactName = cus.ContactName,
                            Phone1 = cus.Phone1,
                            Phone2 = cus.Phone2,
                            Fax = cus.Fax,
                            Email = cus.Email,
                            UserEmails = _db.UserMasters.Where(x => x.CustomerID == customerID).Select(x => x.Email).ToList(),
                            SalesmanCode = cus.SalesmanCode,
                            DebtorTypeID = cus.DebtorTypeID ?? 0,
                            DebtorType = debt.DebtorTypeName,
                            AreaCode = cus.AreaCode,
                            TermID = cus.TermID ?? 0,
                            TermDescription = term.TermName,
                            CreditLimit = cus.CreditLimit,
                            DeliveryAddress1 = cus.DeliveryAddress1,
                            DeliveryAddress2 = cus.DeliveryAddress2,
                            DeliverySuburb = cus.DeliverySuburb,
                            DeliveryStateID = cus.DeliveryStateID ?? 0,
                            DeliveryState = cus.DeliveryStateID == null ? "0" : _db.StateMasters.Where(c => c.StateID == cus.DeliveryStateID).FirstOrDefault().StateName,
                            DeliveryCountryID = cus.DeliveryCountryID ?? 0,
                            DeliveryCountry = cus.DeliveryCountryID == null ? "0" : _db.CountryMasters.Where(c => c.CountryID == cus.DeliveryCountryID).FirstOrDefault().CountryName,
                            DeliveryPostCode = cus.DeliveryPostCode,
                            // CustomerServedFromID = cus.CustomerServedFrom != null ? Convert.ToInt32(cus.CustomerServedFrom) : 0,
                            //CustomerServedFrom = servefrom.StateName,
                            IsActive = cus.IsActive ?? false,
                            IsSuspended = cus.IsSuspended ?? false,
                            IsDeleted = cus.IsDeleted ?? false,
                            DebtorOnHold = cus.DebtorOnHold ?? false,
                            IsSundayOrdering = cus.IsSundayOrdering ?? false,
                            CreatedDate = cus.CreatedDate,
                            ModifiedDate = cus.ModifiedDate,
                            Password = _db.UserMasters.Where(x => x.CustomerID == customerID).FirstOrDefault().DefaultPassword ?? "",
                            TagNames = cus.TagNames,
                            IsSpecialCategory = cus.IsSpecialCategory,
                            AlphaCode = cus.AlphaCode,
                            IsParent = cus.IsParent,
                            ParentID = cus.ParentID == null ? 0 : cus.ParentID
                        }).FirstOrDefault();

            return data;
        }

        #endregion

        #region

        /// <summary>
        /// Get Salesman codes
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetSalesmanCode()
        {
            var cust = (from a in _db.CustomerMasters
                        where a.IsRep == false && a.SalesmanCode != null && a.SalesmanCode != ""
                        select new { a.SalesmanCode }).ToList().Distinct();

            return cust;
        }

        #endregion

        #region

        /// <summary>
        /// Get customer detail for update
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public AddEditCustomerBO GetCustomerForEdit(long customerID)
        {
            try
            {
                var data = (from cus in _db.CustomerMasters
                            join debt in _db.DebtorTypeMasters on cus.DebtorTypeID equals debt.DebtorTypeId into temp
                            from debt in temp.DefaultIfEmpty()

                            join term in _db.TermMasters on cus.TermID equals term.TermID into temp1
                            from term in temp1.DefaultIfEmpty()

                            where cus.CustomerID == customerID
                            select new AddEditCustomerBO
                            {
                                CustomerID = cus.CustomerID,
                                CustomerName = cus.CustomerName,
                                ABN = cus.ABN,
                                Address1 = cus.Address1,
                                Address2 = cus.Address2,
                                Suburb = cus.Suburb,
                                City = cus.City,
                                StateID = cus.StateID ?? 0,
                                CountryID = cus.CountryID ?? 0,
                                PostCode = cus.PostCode,
                                ContactName = cus.ContactName,
                                Phone1 = cus.Phone1,
                                Phone2 = cus.Phone2,
                                Fax = cus.Fax,
                                Email = cus.Email,
                                SalesmanCode = cus.SalesmanCode ?? "",
                                DebtorTypeID = cus.DebtorTypeID ?? 0,
                                DebtorType = debt.DebtorTypeName,
                                AreaCode = cus.AreaCode,
                                IsActive = cus.IsActive ?? false,
                                DebtorOnHold = cus.DebtorOnHold ?? false,
                                IsSundayOrdering = cus.IsSundayOrdering ?? false,
                                Password = _db.UserMasters.Where(x => x.CustomerID == customerID).FirstOrDefault().DefaultPassword
                            }).FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Add/Edit Customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageCustomer(AddEditCustomerBO model)
        {
            try
            {
                CustomerMaster cust = new CustomerMaster();
                var data = _db.CustomerMasters.Where(x => x.Email.ToLower() == model.Email.ToLower() && x.CustomerID != model.CustomerID).FirstOrDefault();
                if (data == null)
                {
                    if (model.CustomerID > 0)
                    {
                        cust = _db.CustomerMasters.Where(x => x.CustomerID == model.CustomerID).FirstOrDefault();
                        if (cust != null)
                        {
                            cust.SalesmanCode = model.SalesmanCode;
                            cust.CustomerName = model.CustomerName;
                            cust.ContactName = model.ContactName;
                            cust.Email = model.Email;
                            cust.Phone1 = model.Phone1;
                            cust.Address1 = model.Address1;
                            cust.Address2 = model.Address2;
                            cust.Suburb = model.Suburb;
                            cust.City = model.City;
                            cust.CountryID = model.CountryID;
                            cust.StateID = model.StateID;
                            cust.PostCode = model.PostCode;
                            cust.IsActive = model.IsActive;
                            cust.IsSundayOrdering = model.IsSundayOrdering;
                            model.ModifiedDate = DateTime.Now;
                            // Mapper.Map(model, cust);
                            cust.CompanyID = 1;
                            cust.CustomerID = model.CustomerID;
                        }
                        else { return "No customer available!"; }
                    }
                    else
                    {
                        cust.CompanyID = 1;
                        cust.CreatedDate = DateTime.Now;
                        model.IsRep = model.IsRep;
                        model.DebtorOnHold = true;
                        Mapper.Map(model, cust);

                        _db.CustomerMasters.Add(cust);
                    }
                    _db.SaveChanges();

                    byte[] saltBytes = new byte[8];
                    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                    rng.GetNonZeroBytes(saltBytes);
                    string pwdSalt = Convert.ToBase64String(saltBytes);
                    string pwdHash = "";//

                    var user = _db.UserMasters.Where(x => x.CustomerID == cust.CustomerID).FirstOrDefault();
                    if (user != null)
                    {
                        user.ModifiedOn = DateTime.Now;
                        user.FirstName = model.CustomerName;
                        user.ContactName = model.ContactName;
                        user.Phone = model.Phone1;
                        user.Email = model.Email;
                        user.IsActive = model.IsActive;
                        if (!string.IsNullOrEmpty(model.Password) && model.Password != user.DefaultPassword)
                        {
                            pwdHash = ENCDEC.ComputeHash(model.Password, "MD5", saltBytes);
                            user.PasswordHash = pwdHash;
                            user.PasswordSalt = pwdSalt;
                            user.DefaultPassword = model.Password;
                        }
                        _db.SaveChanges();
                    }
                    else
                    {
                        user = new UserMaster();
                        if (string.IsNullOrEmpty(model.Password))
                        {
                            pwdHash = ENCDEC.ComputeHash("12345678", "MD5", saltBytes);
                            user.DefaultPassword = "12345678";
                        }
                        else
                        {
                            pwdHash = ENCDEC.ComputeHash(model.Password, "MD5", saltBytes);
                            user.DefaultPassword = model.Password;
                        }
                        user.FirstName = model.CustomerName;
                        user.ContactName = model.ContactName;
                        user.Phone = model.Phone1;
                        user.Email = model.Email;
                        user.IsActive = model.IsActive;
                        user.CustomerID = cust.CustomerID;
                        user.PasswordHash = pwdHash;
                        user.PasswordSalt = pwdSalt;
                        user.CreatedOn = DateTime.Now;
                        user.UserTypeID = model.UserTypeID;

                        _db.UserMasters.Add(user);
                        _db.SaveChanges();
                    }

                    //set features for user
                    var feature = _db.spSetUserFeatures(user.UserID);
                    return "Success";
                }
                else { return "Email ID is already exist!"; }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public string DeleteCustomer(long customerID)
        {
            try
            {
                //var cust = _db.CustomerMasters.Include<CustomerMaster, ICollection<UserMaster>>(u => u.).Where(x => x.CustomerID == customerID).FirstOrDefault();
                var cust = _db.CustomerMasters.Where(x => x.CustomerID == customerID).FirstOrDefault();
                if (cust != null)
                {
                    var user = _db.UserMasters.Where(x => x.CustomerID == customerID).ToList();
                    if (user.Count > 0)
                    {
                        _db.UserMasters.RemoveRange(user);
                        _db.SaveChanges();
                    }
                    _db.CustomerMasters.Remove(cust);
                    _db.SaveChanges();

                    return "Success";
                }
                else
                {
                    return "Customer ID does not exist!";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Change Customer status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string SetCustActive(long id, bool status)
        {
            try
            {
                var cust = _db.CustomerMasters.Where(x => x.CustomerID == id).FirstOrDefault();
                if (cust != null)
                {
                    cust.IsActive = status;
                    _db.SaveChanges();
                    return "Success";
                }
                else
                {
                    return "No customer available!";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get new requests
        /// </summary>
        /// <returns></returns>
        public IEnumerable<object> GetNewRequests()
        {
            try
            {
                return (from a in _db.UserMasters
                        where a.UserTypeID == 4 && a.IsActive == false && a.CustomerID == null
                        select new
                        {
                            UserID = a.UserID,
                            BusinessName = a.BusinessName,
                            FirstName = a.FirstName,
                            ContactName = a.FirstName + " " + a.LastName,// a.ContactName,
                            Phone = a.Phone,
                            Email = a.Email,
                            CreatedOn = a.CreatedOn,
                            Street = a.StreetAddress ?? "",
                            Street2 = a.StreetAddress2 ?? "",
                            ABN = a.ABN ?? "",
                            Postcode = a.Postcode ?? " ",
                            Suburb = a.Suburb ?? " "
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Link user to customer
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public string linkUserToCustomer(long userID, long customerID)
        {
            try
            {
                var user = _db.UserMasters.Where(x => x.UserID == userID).FirstOrDefault();
                if (user != null)
                {
                    user.IsActive = true;
                    user.CustomerID = customerID;

                    _db.SaveChanges();

                    //set features for user
                    var feature = _db.spSetUserFeatures(user.UserID);
                    //send mail
                    string companyname = ConfigurationManager.AppSettings["CompanyName"].ToString();
                    string fromEmail = ConfigurationManager.AppSettings["FROM_EMAILID"].ToString();

                    string mailContent = File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplates/linkcustomermail.htm"));
                    mailContent = mailContent.Replace("@pathImages", ConfigurationManager.AppSettings["WBS_BASE_PATH"] + "Images");
                    mailContent = mailContent.Replace("@path", ConfigurationManager.AppSettings["SITE_URL"].ToString());
                    mailContent = mailContent.Replace("@CustomerName", user.FirstName);
                    mailContent = mailContent.Replace("@Username", user.Email);
                    mailContent = mailContent.Replace("@Password", user.DefaultPassword);
                    mailContent = mailContent.Replace("@Company", companyname);
                    SendMail.SendEMail(user.Email, "", "", companyname + " - User Approved", mailContent, "", fromEmail);

                    return "Success";
                }
                else { return "User ID does not exist!"; }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region

        /// <summary>
        /// get features
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public FeaturesModel GetFeatures(long userID)
        {
            try
            {
                DataTable dtUser = _db.CustomerFeaturesMasters.Where(x => x.UserID == userID).ToList().ToDataTable();
                var dtAdmin = _db.AdminFeaturesMasters.ToList().ToDataTable();
                DataTable dtFeaturesDescription = _db.ManageFeaturesDescriptionMasters.ToList().ToDataTable();
                List<FeaturesMasterBo> liuserfeatures = new List<FeaturesMasterBo>();

                if (dtUser.Rows.Count > 0)
                {
                    foreach (DataColumn column in dtUser.Columns)
                    {
                        if (column.ColumnName != "ID" && column.ColumnName != "UserID" && column.ColumnName != "CreatedDate" && column.ColumnName != "ModifiedDate")
                        {
                            foreach (DataColumn column2 in dtAdmin.Columns)
                            {
                                if (column.ColumnName == column2.ColumnName && dtAdmin.Rows[0][column2].ToString() != "" && dtAdmin.Rows[0][column2].ToString().ToLower() != "false")
                                {
                                    FeaturesMasterBo obj = new FeaturesMasterBo();
                                    obj.Name = column.ToString();
                                    obj.Selected = dtUser.Rows[0][column].ToString() == "" ? false : Convert.ToBoolean(dtUser.Rows[0][column]); //_db.CustomerFeaturesMasters.Where(x=>x.UserID==userID).Select(x=>x.colu).FirstOrDefault();// Convert.ToBoolean(dtAdmin.Rows[0][column]);
                                    DataRow[] rows = dtFeaturesDescription.Select("mf_key = '" + column + "' and mf_type = 2");
                                    if (rows.Count() > 0)
                                    {
                                        obj.Description = rows[0].Field<string>("mf_description") ?? "";
                                    }
                                    else
                                    {
                                        obj.Description = "";
                                    }
                                    liuserfeatures.Add(obj);
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataColumn column2 in dtAdmin.Columns)
                    {
                        if (column2.ColumnName != "ID" && column2.ColumnName != "CreatedDate" && column2.ColumnName != "ModifiedDate" && dtAdmin.Rows[0][column2].ToString() != "" && dtAdmin.Rows[0][column2].ToString().ToLower() != "false")
                        {
                            FeaturesMasterBo obj = new FeaturesMasterBo();
                            obj.Name = column2.ToString();
                            obj.Selected = false;
                            DataRow[] rows = dtFeaturesDescription.Select("mf_key = '" + column2 + "' and mf_type = 2");
                            if (rows.Count() > 0)
                            {
                                obj.Description = rows[0].Field<string>("mf_description") ?? "";
                            }
                            else
                            {
                                obj.Description = "";
                            }
                            liuserfeatures.Add(obj);
                        }
                    }
                }
                return new FeaturesModel()
                {
                    AdminFeatures = liuserfeatures,
                    Message = "Success"
                };
            }
            catch (Exception ex)
            {
                return new FeaturesModel()
                {
                    Message = ex.Message
                };
            }
        }

        #endregion

        #region

        /// <summary>
        ///
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public IEnumerable<object> GetUserByCustomerID(long customerID)
        {
            string email = "";
            var cust = _db.CustomerMasters.Where(x => x.CustomerID == customerID).FirstOrDefault();
            if (cust != null)
            {
                email = cust.Email;
            }
            var user = (from a in _db.UserMasters
                        where a.CustomerID == customerID
                        select new
                        {
                            a.UserID,
                            Name = a.Email + (a.FirstName != null ? " (" + a.FirstName + ")" : ""),
                            a.Email,
                            IsManagement = (a.IsManagement != null ? a.IsManagement : false),
                            IsPrimaryEmail = (a.Email.ToLower() == email.ToLower() ? 1 : 0)
                        }).Distinct().ToList();

            return user;
        }

        #endregion

        #region

        /// <summary>
        ///
        /// </summary>
        /// <param name="colName"></param>
        /// <param name="val"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public string UpdateFeatureForUser(string colName, string val, long userID)
        {
            try
            {
                var user = _db.CustomerFeaturesMasters.Where(x => x.UserID == userID).FirstOrDefault();
                if (user == null)
                {
                    CustomerFeaturesMaster obj = new CustomerFeaturesMaster();
                    obj.UserID = userID;
                    obj.CreatedDate = DateTime.Now;
                    obj.ModifiedDate = DateTime.Now;
                    _db.CustomerFeaturesMasters.Add(obj);
                    _db.SaveChanges();
                }
                int update = _db.Database.ExecuteSqlCommand("update dbo.CustomerFeaturesMaster set " + colName + " = " + val + " where UserID =" + userID);

                if (update > 0)
                {
                    return "Success";
                }
                else
                {
                    return "Failed";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Add User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageUser(UserMaster model, string type = "")
        {
            try
            {
                if (model.UserID > 0)
                {
                    var user = _db.UserMasters.Where(x => x.UserID == model.UserID).FirstOrDefault();
                    if (user != null)
                    {
                        if (model.ABN != null && model.ABN == "Edit")
                        {
                            var email = _db.UserMasters.Where(x => x.Email.ToLower() == model.Email.ToLower() && x.UserID != model.UserID).FirstOrDefault();
                            if (email != null)
                            {
                                return "Email already exists for another user";
                            }
                            user.FirstName = model.FirstName;
                            user.Phone = model.Phone;
                            user.Email = model.Email;

                            if (!string.IsNullOrEmpty(model.DefaultPassword))
                            {
                                user.DefaultPassword = model.DefaultPassword;
                                byte[] saltBytes = new byte[8];
                                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                                rng.GetNonZeroBytes(saltBytes);
                                string pwdSalt = Convert.ToBase64String(saltBytes);
                                string pwdHash = "";
                                pwdHash = ENCDEC.ComputeHash(model.DefaultPassword, "MD5", saltBytes);

                                user.PasswordHash = pwdHash;
                                user.PasswordSalt = pwdSalt;
                            }
                        }
                        else
                        {
                            user.IsManagement = model.IsManagement;
                            user.ModifiedOn = DateTime.Now;
                        }

                        _db.SaveChanges();
                        return "Success";
                    }
                    else
                    {
                        return "No user available!";
                    }
                }
                else
                {
                    var user = _db.UserMasters.Where(x => x.Email.ToLower() == model.Email.ToLower()).FirstOrDefault();
                    if (user == null)
                    {
                        byte[] saltBytes = new byte[8];
                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                        rng.GetNonZeroBytes(saltBytes);
                        string pwdSalt = Convert.ToBase64String(saltBytes);
                        string pwdHash = "";
                        if (string.IsNullOrEmpty(model.DefaultPassword))
                        {
                            pwdHash = ENCDEC.ComputeHash("12345678", "MD5", saltBytes);
                            model.DefaultPassword = "12345678";
                        }
                        else
                        {
                            pwdHash = ENCDEC.ComputeHash(model.DefaultPassword, "MD5", saltBytes);
                        }
                        model.PasswordHash = pwdHash;
                        model.PasswordSalt = pwdSalt;
                        model.CreatedOn = DateTime.Now;
                        model.UserTypeID = 4;
                        model.IsActive = true;
                        _db.UserMasters.Add(model);
                        _db.SaveChanges();

                        var data = _db.spSetUserFeatures(model.UserID);
                        return "Success";
                    }
                    else { return "Email ID is already exist!"; }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string DeleteUser(long userID)
        {
            try
            {
                var user = _db.UserMasters.Where(x => x.UserID == userID).FirstOrDefault();
                if (user != null)
                {
                    _db.UserMasters.Remove(user);
                    _db.SaveChanges();
                    return "Success";
                }
                else
                {
                    return "No user available!";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Get pantry list by customer id
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public List<PantryListMaster> GetPantryList(long customerID)
        {
            try
            {
                _db.Configuration.ProxyCreationEnabled = false;
                return _db.PantryListMasters.Where(x => x.CustomerID == customerID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region

        /// <summary>
        /// manage pantry name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManagePantryList(PantryListMaster model)
        {
            try
            {
                var data = _db.PantryListMasters.Where(x => x.PantryListID == model.PantryListID).FirstOrDefault();
                if (data != null)
                {
                    data.PantryListName = model.PantryListName;
                    _db.SaveChanges();
                }
                else { return "No pantry available!"; }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get Product categories

        public List<SelectListItems> GetProductCategory()
        {
            try
            {
                return (from a in _db.ProductCategoriesMasters
                        where a.IsSpecialCategory == true && a.CategoryName != "" && a.CategoryName != null
                        select new SelectListItems
                        {
                            Text = a.CategoryName
                        }).ToList();
                // _db.ProductCategoriesMasters.Where(x => x.IsSpecialCategory == true && x.CategoryName != null && x.CategoryName != "").Select(x=>x.CategoryName).Distinct().ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Manage tag names for customers

        public string ManageCustomerTags(CustomerBO obj)
        {
            try
            {
                var cust = _db.CustomerMasters.Where(x => x.CustomerID == obj.CustomerID).FirstOrDefault();
                if (cust != null)
                {
                    if (obj.IsChild == true)
                    {
                        cust.ParentID = null;
                        _db.SaveChanges();
                    }
                    else
                    {
                        cust.IsSpecialCategory = obj.IsSpecialCategory;
                        cust.TagNames = obj.TagNames;
                        cust.IsParent = obj.IsParent;
                        _db.SaveChanges();

                        var removeChild = _db.CustomerMasters.Where(x => x.ParentID == obj.CustomerID).ToList();
                        if (removeChild != null)
                        {
                            foreach (var rcid in removeChild)
                            {
                                rcid.ParentID = null;
                                _db.SaveChanges();
                            }
                        }
                        foreach (var cid in obj.ChildList)
                        {
                            var custchild = _db.CustomerMasters.Where(x => x.CustomerID == cid).FirstOrDefault();
                            if (custchild != null)
                            {
                                custchild.ParentID = obj.CustomerID;
                                _db.SaveChanges();
                            }
                        }
                    }
                }
                else { return "Customer not avaliable!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion
        #region Get User by id

        /// <summary>
        ///  Get User by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserBO GetUserByID(long id)
        {
            try
            {
                return (from a in _db.UserMasters
                        where a.UserID == id
                        select new UserBO
                        {
                            UserID = a.UserID,
                            UserTypeID = a.UserTypeID,
                            FirstName = a.FirstName,
                            UserName = a.UserName,
                            Email = a.Email,
                            Phone = a.Phone,
                            IsActive = a.IsActive,
                            DefaultPassword = a.DefaultPassword
                        }).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Manage My account

        /// <summary>
        ///  Manage My account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageMyAccount(UserBO model)
        {
            try
            {
                byte[] saltBytes = new byte[8];
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                rng.GetNonZeroBytes(saltBytes);
                string pwdSalt = Convert.ToBase64String(saltBytes);
                string pwdHash = "";
                var user = _db.UserMasters.Where(x => x.Email.ToLower() == model.Email.ToLower() && x.UserID != model.UserID).FirstOrDefault();
                if (user == null)
                {
                    if (model.UserID > 0)
                    {
                        var data = _db.UserMasters.Where(x => x.UserID == model.UserID && (x.UserTypeID == 2 || x.UserTypeID == 6 || x.UserTypeID == 7)).FirstOrDefault();
                        if (data != null)
                        {
                            data.UserName = model.UserName;
                            data.FirstName = model.FirstName;
                            data.Email = model.Email;
                            data.Phone = model.Phone;
                            if (model.DefaultPassword != null)
                            {
                                if (!string.IsNullOrEmpty(model.DefaultPassword) && model.DefaultPassword != data.DefaultPassword)
                                {
                                    pwdHash = ENCDEC.ComputeHash(model.DefaultPassword, "MD5", saltBytes);
                                    data.PasswordHash = pwdHash;
                                    data.PasswordSalt = pwdSalt;
                                    data.DefaultPassword = model.DefaultPassword;
                                }
                                data.IsActive = model.IsActive;
                                data.UserTypeID = model.UserTypeID;
                            }
                            _db.SaveChanges();
                        }
                        else { return "No user available!"; }
                    }
                    else
                    {
                        user = new UserMaster();
                        if (string.IsNullOrEmpty(model.DefaultPassword))
                        {
                            pwdHash = ENCDEC.ComputeHash("12345678", "MD5", saltBytes);
                            user.DefaultPassword = "12345678";
                        }
                        else
                        {
                            pwdHash = ENCDEC.ComputeHash(model.DefaultPassword, "MD5", saltBytes);
                            user.DefaultPassword = model.DefaultPassword;
                        }
                        user.FirstName = model.FirstName;
                        user.UserName = model.UserName;
                        user.Email = model.Email;
                        user.Phone = model.Phone;
                        user.IsActive = model.IsActive;
                        user.PasswordHash = pwdHash;
                        user.PasswordSalt = pwdSalt;
                        user.CreatedOn = DateTime.Now;
                        user.UserTypeID = model.UserTypeID;

                        _db.UserMasters.Add(user);
                        _db.SaveChanges();
                    }
                }
                else { return "Email-ID is already exist!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region Change password

        /// <summary>
        ///  Change password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var data = _db.UserMasters.Where(x => x.UserID == model.UserLoginID).FirstOrDefault();
                if (data != null)
                {
                    byte[] saltBytes = new byte[8];
                    saltBytes = Convert.FromBase64String(data.PasswordSalt);
                    if (ENCDEC.ComputeHash(model.OldPassword, "MD5", saltBytes) == data.PasswordHash)
                    {
                        saltBytes = new byte[8];
                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                        rng.GetNonZeroBytes(saltBytes);
                        string pwdSalt = Convert.ToBase64String(saltBytes);
                        string pwdHash = ENCDEC.ComputeHash(model.NewPassword, "MD5", saltBytes);
                        data.PasswordHash = pwdHash;
                        data.PasswordSalt = pwdSalt;
                        data.DefaultPassword = model.NewPassword;
                        _db.SaveChanges();
                    }
                    else { return "Your old password is wrong!"; }
                }
                else { return "User not available!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region Get admin/rego user

        public List<UserMaster> GetUsers(string usertypeids)
        {
            try
            {
                // usertypeids = "2,6,7";
                return _db.UserMasters.Where(x => usertypeids.Contains(x.UserTypeID.ToString())).OrderByDescending(x => x.UserID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get userType

        /// <summary>
        ///  Get userType
        /// </summary>
        /// <returns></returns>
        public List<UserTypeMaster> GetUserType()
        {
            try
            {
                return _db.UserTypeMasters.Where(x => x.UserTypeID == 6 || x.UserTypeID == 7).OrderByDescending(x => x.UserTypeID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get Customer Child list

        public List<AllChildCustomerBO> GetCustomerChildlist(long PID)
        {
            //Note: Not in used
            try
            {
                //child = _db.CustomerMasters.Where(x => x.ParentID != null || x.ParentID != 0).ToList();
                return (from a in _db.CustomerMasters
                        where (a.ParentID == null || a.ParentID == PID) && (a.IsParent == null || a.IsParent == false)
                        select new AllChildCustomerBO
                        {
                            CustomerID = a.CustomerID,
                            CustomerName = a.CustomerName,
                            ParentID = a.ParentID,
                            Selected = a.ParentID == null ? false : true
                        }).ToList().Where(x => x.CustomerID != PID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get Child By ParentID

        public List<AllChildCustomerBO> GetChildByParentID(ParentChildModel model)
        {
            try
            {
                //Need to review
                List<AllChildCustomerBO> li = new List<AllChildCustomerBO>();
                var bb = new List<AllChildCustomerBO>();
                var aa = (from a in _db.CustomerMasters
                          where
                          (a.ParentID == model.ID)
                          orderby a.CustomerName
                          select new AllChildCustomerBO
                          {
                              CustomerID = a.CustomerID,
                              CustomerName = a.CustomerName,
                              ParentID = a.ParentID,
                              Selected = a.ParentID == null ? false : a.ParentID == 0 ? false : true
                          }).ToList();
                li.AddRange(aa);
                if (model.SearchText == null || model.SearchText == "")
                {
                    //bb = (from a in _db.CustomerMasters
                    //         where
                    //        (a.ParentID != model.ID) && (a.IsParent == null || a.IsParent == false) && (a.ParentID == null || a.ParentID == 0)
                    //         orderby a.CustomerName
                    //         select new AllChildCustomerBO
                    //         {
                    //             CustomerID = a.CustomerID,
                    //             CustomerName = a.CustomerName,
                    //             ParentID = a.ParentID,
                    //             Selected = a.ParentID == null ? false : a.ParentID == 0 ? false : true
                    //         }).ToList();
                }
                else
                {
                    bb = (from a in _db.CustomerMasters
                          where
                          (model.SearchText == "" ? true : a.CustomerName.ToLower().Contains(model.SearchText.ToLower())
                                   || a.ContactName.ToLower().Contains(model.SearchText.ToLower())
                                   || a.Email.ToLower().Contains(model.SearchText.ToLower())
                                   || a.AlphaCode.ToLower().Contains(model.SearchText.ToLower())
                                   || a.Address1.ToLower().Contains(model.SearchText.ToLower())
                                   || model.SearchText == "" || model.SearchText == null) &&
                          (a.IsParent == null || a.IsParent == false) && (a.ParentID == null || a.ParentID == 0)
                          orderby a.CustomerName
                          select new AllChildCustomerBO
                          {
                              CustomerID = a.CustomerID,
                              CustomerName = a.CustomerName,
                              ParentID = a.ParentID,
                              Selected = a.ParentID == null ? false : a.ParentID == 0 ? false : true
                          }
                       ).ToList();
                }
                li.AddRange(bb);
                var search = li.Where(x => x.CustomerID != model.ID && (x.CustomerName != null || x.CustomerName != "")).ToList();

                return (search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get Parent By ChildID

        public List<AllChildCustomerBO> GetParentByChildID(ParentChildModel model)
        {
            try
            {
                //(CustomerID =3031 and IsParent=1) or ParentID is null and (IsParent is null or IsParent = 0)
                return (from a in _db.CustomerMasters
                        where a.CustomerID == model.ID ||
                        (model.SearchText == "" ? true : a.CustomerName.ToLower().Contains(model.SearchText.ToLower())
                             || a.ContactName.ToLower().Contains(model.SearchText.ToLower())
                             || a.Email.ToLower().Contains(model.SearchText.ToLower())
                             || a.AlphaCode.ToLower().Contains(model.SearchText.ToLower())
                             || a.Address1.ToLower().Contains(model.SearchText.ToLower())
                             || model.SearchText == "" || model.SearchText == null) || (a.IsParent == null || a.IsParent == false)
                        select new AllChildCustomerBO
                        {
                            CustomerID = a.CustomerID,
                            CustomerName = a.CustomerName,
                            ParentID = a.ParentID,
                            Selected = a.CustomerID == model.ID ? true : false
                        }).ToList().Where(x => x.CustomerID != model.ID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetParentInfo

        public AllChildCustomerBO GetParentInfo(long PID)
        {
            try
            {
                return (from a in _db.CustomerMasters
                        where a.CustomerID == PID
                        select new AllChildCustomerBO
                        {
                            CustomerID = a.CustomerID,
                            CustomerName = a.CustomerName,
                            ParentID = 0,
                            Selected = true
                        }).First();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        //14092019
        //public List<AllCustomerDetailBO> GetAllCustomersByEmail(GetCustomersModel model)
        //{
        //   var searchresult = (from cust in _db.CustomerMasters
        //                where
        //                 cust.Email.ToLower().Contains(model.SearchText.ToLower())

        //                select new AllCustomerDetailBO
        //                {
        //                    CustomerID = cust.CustomerID,
        //                    CustomerName = cust.CustomerName,
        //                    Email=cust.Email,
        //                    IsRep=cust.IsRep

        //                }).ToList();

        //    return searchresult;
        //    //  return (_db.CustomerMasters.Where(x => x.Email.Contains(emailid)).ToList());
        //}
        public List<AllCustomerBO> GetAllCustomersByEmail(GetCustomersModel model)
        {
            try
            {
                var data = (from cust in _db.CustomerMasters
                            where
                              //cust.Email.ToLower().Contains(model.SearchText.ToLower())
                              cust.Email.ToLower().Trim() == model.SearchText.ToLower().Trim()
                              || model.SearchText == "" || model.SearchText == null
                            select new AllCustomerBO
                            {
                                CustomerID = cust.CustomerID,
                                CustomerName = cust.CustomerName
                            }).ToList();
                if (model.PageSize > 0)
                {
                    data = data.OrderBy(c => c.CustomerName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
                }
                else
                {
                    data = data.OrderBy(c => c.CustomerName).ToList();
                }
                return data.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region

        /// <summary>
        /// Get Manage customer detail by ID
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public CustomerBO GetMangeCustomerByID(long customerID, string searchText = null)
        {
            var data = (from cus in _db.CustomerMasters
                        where cus.CustomerID == customerID && (cus.CustomerName.Contains(searchText) || searchText == null)
                        select new CustomerBO
                        {
                            CustomerID = cus.CustomerID,
                            CustomerName = cus.CustomerName,
                            SalesmanCode = cus.SalesmanCode,
                            ABN = cus.ABN,
                            Address1 = cus.Address1,
                            Address2 = cus.Address2,
                            Email = cus.Email,
                            // UserEmails = _db.UserMasters.Where(x => x.CustomerID == customerID).Select(x => x.Email).ToList(),
                            UserEmailAndID = (from u in _db.UserMasters
                                              where u.CustomerID == customerID
                                              select new UserBO
                                              {
                                                  UserID = u.UserID,
                                                  Email = u.Email,
                                                  DefaultPassword = u.DefaultPassword
                                              }).ToList(),
                            IsActive = cus.IsActive ?? false,
                            Password = _db.UserMasters.Where(x => x.CustomerID == customerID).FirstOrDefault().DefaultPassword ?? "",
                            AlphaCode = cus.AlphaCode,
                            IsRep = cus.IsRep
                        }).FirstOrDefault();

            return data;
        }

        #endregion

        #region Manage tag names for customers

        public string ManageCustomer(CustomerBO obj)
        {
            try
            {
                var cust = _db.CustomerMasters.Where(x => x.CustomerID == obj.CustomerID).FirstOrDefault();
                if (cust != null)
                {
                    cust.IsRep = obj.IsRep;
                    _db.SaveChanges();

                    var users = _db.UserMasters.Where(x => x.CustomerID == obj.CustomerID).ToList();

                    foreach (UserMaster u in users)
                    {
                        if (obj.IsRep == true)
                        {
                            u.UserTypeID = 3;
                        }
                        else
                        {
                            u.UserTypeID = 4;
                        }

                        _db.SaveChanges();
                    }
                    if (obj.UserEmailAndID != null)
                    {
                        byte[] saltBytes = new byte[8];
                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                        rng.GetNonZeroBytes(saltBytes);
                        string pwdSalt = Convert.ToBase64String(saltBytes);
                        string pwdHash = "";

                        foreach (UserBO u in obj.UserEmailAndID)
                        {
                            var userDetails = _db.UserMasters.Where(x => x.UserID == u.UserID && x.CustomerID == obj.CustomerID).FirstOrDefault();
                            if (userDetails != null)
                            {
                                userDetails.Email = u.Email;

                                if (!string.IsNullOrEmpty(u.DefaultPassword) && userDetails.DefaultPassword.ToLower() != u.DefaultPassword.ToLower())
                                {
                                    pwdHash = ENCDEC.ComputeHash(u.DefaultPassword, "MD5", saltBytes);
                                    userDetails.PasswordHash = pwdHash;
                                    userDetails.PasswordSalt = pwdSalt;
                                    userDetails.DefaultPassword = u.DefaultPassword;
                                }

                                _db.SaveChanges();
                            }
                        }
                    }
                    _db.sp_manageCustomerAndUser();
                }
                else { return "Customer not avaliable!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region Get user by customerID

        public UserMaster GetUserByCustomer(long customerID)
        {
            return _db.UserMasters.Where(c => c.CustomerID == customerID).FirstOrDefault();
        }

        #endregion
        //end
    }
}