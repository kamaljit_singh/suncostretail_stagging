﻿using DataModel.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using CommonFunctions;
using System.Data;
using DataModel.BO;
using System.Configuration;
using System.Threading;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Web.Hosting;

namespace DataModel.DA
{
    public class AccountDA
    {
        private readonly SaaviAdminEntities _db;

        public AccountDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region Validate login credentials for a user

        /// <summary>
        /// Validate login credentials for a user
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public LoginResponse ValidateUser(string userName, string password)
        {
            try
            {
                string result = "";
                var userInfo = _db.UserMasters.Where(u => (u.Email == userName || u.UserName == userName) && (u.UserTypeID == 1 || u.UserTypeID == 2 || u.UserTypeID == 6)).FirstOrDefault();

                if (userInfo != null)
                {
                    byte[] saltBytes = new byte[8];

                    saltBytes = Convert.FromBase64String(userInfo.PasswordSalt);

                    if (ENCDEC.ComputeHash(password, "MD5", saltBytes) == userInfo.PasswordHash)
                    {
                        FormsAuthentication.SetAuthCookie(userName, false);

                        var authJson = new
                        {
                            Name = userInfo.FirstName + " " + userInfo.LastName,
                            UserID = userInfo.UserID,
                            Role = userInfo.UserTypeMaster.Name,
                            Email = userInfo.Email
                        };

                        var authTicket = new FormsAuthenticationTicket(1, userInfo.Email, DateTime.Now, DateTime.Now.AddHours(4), false, JsonConvert.SerializeObject(authJson));
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        HttpContext.Current.Response.Cookies.Add(authCookie);

                        result = "Success";
                    }
                    else
                    {
                        result = "The password you entered is incorrect.";
                    }
                }
                else
                {
                    result = "The username you entered is incorrect.";
                }

                return new LoginResponse()
                {
                    Message = result,
                    Role = userInfo != null ? userInfo.UserTypeMaster.Name : ""
                };
            }
            catch (Exception ex)
            {
                return new LoginResponse()
                {
                    Message = ex.Message
                };
            }
        }

        #endregion Validate login credentials for a user

        #region Get admin and main features

        /// <summary>
        /// Get admin and main features
        /// </summary>
        /// <returns></returns>
        public FeaturesModel GetAdminAndMainFeatures()
        {
            try
            {
                DataTable dtAdmin = _db.AdminFeaturesMasters.ToList().ToDataTable();
                DataTable dtMain = _db.MainFeaturesMasters.ToList().ToDataTable();

                DataTable dtFeaturesDescription = _db.ManageFeaturesDescriptionMasters.ToList().ToDataTable();

                List<FeaturesMasterBo> liAdminfeatures = new List<FeaturesMasterBo>();
                List<FeaturesMasterBo> liMainFeatures = new List<FeaturesMasterBo>();

                foreach (DataColumn column in dtAdmin.Columns)
                {
                    if (column.ColumnName != "ID" && column.ColumnName != "CreatedDate" && column.ColumnName != "ModifiedDate")
                    {
                        FeaturesMasterBo obj = new FeaturesMasterBo();
                        obj.Name = column.ToString();
                        obj.Selected = Convert.ToBoolean(dtAdmin.Rows[0][column].ToString() == "" ? false : dtAdmin.Rows[0][column]);
                        DataRow[] rows = dtFeaturesDescription.Select("mf_key = '" + column + "' and mf_type = 2");
                        if (rows.Count() > 0)
                        {
                            obj.Description = rows[0].Field<string>("mf_description") ?? "";
                        }
                        else
                        {
                            obj.Description = "";
                        }

                        liAdminfeatures.Add(obj);
                    }
                }

                foreach (DataColumn column in dtMain.Columns)
                {
                    if (column.ColumnName != "Id" && column.ColumnName != "CreatedDate" && column.ColumnName != "ModifiedDate" && column.ColumnName != "PrimaryAppColor" && column.ColumnName != "SecondaryAppColor" && column.ColumnName != "Currency")
                    {
                        FeaturesMasterBo obj = new FeaturesMasterBo();
                        obj.Name = column.ToString();
                        obj.Selected = Convert.ToBoolean(dtMain.Rows[0][column].ToString() == "" ? false : dtMain.Rows[0][column]);
                        DataRow[] rows = dtFeaturesDescription.Select("mf_key = '" + column + "' and mf_type = 1");
                        if (rows.Count() > 0)
                        {
                            obj.Description = rows[0].Field<string>("mf_description") ?? "";
                        }
                        else
                        {
                            obj.Description = "";
                        }
                        liMainFeatures.Add(obj);
                    }
                }

                return new FeaturesModel()
                {
                    AdminFeatures = liAdminfeatures,
                    MainFeatures = liMainFeatures,
                    Message = "Success"
                };
            }
            catch (Exception ex)
            {
                return new FeaturesModel()
                {
                    Message = ex.Message
                };
            }
        }

        #endregion Get admin and main features

        #region Update admin and main features

        /// <summary>
        /// Update admin and main features
        /// </summary>
        /// <param name="colName"></param>
        /// <param name="val"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public string UpdateAdminOrMainFeature(string colName, string val, string table)
        {
            try
            {
                int update = _db.Database.ExecuteSqlCommand("update dbo." + table + " set " + colName + " = " + val);

                if (table.ToLower() == "adminfeaturesmaster")
                {
                    _db.Database.ExecuteSqlCommand("update dbo.customerfeaturesmaster set " + colName + " = " + val);
                }

                if (update > 0)
                {
                    return "Success";
                }
                else
                {
                    return "Failed";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Update admin and main features

        #region Forgot Password for SuperAdmin/CompanyAdmin

        /// <summary>
        /// Forgot Password for SuperAdmin/CompanyAdmin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                var data = _db.UserMasters.Where(x => x.Email.ToLower() == model.Email.ToLower() && (x.UserTypeID == 1 || x.UserTypeID == 2 || x.UserTypeID == 6)).FirstOrDefault();
                if (data != null)
                {
                    data.ResetCode = Guid.NewGuid().ToString();
                    _db.SaveChanges();
                    string companyname = ConfigurationManager.AppSettings["CompanyName"].ToString();
                    string fromEmail = ConfigurationManager.AppSettings["FROM_EMAILID"].ToString();

                    string mailContent = File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplates/forgotpasswordmail.htm"));
                    mailContent = mailContent.Replace("@pathImages", ConfigurationManager.AppSettings["WBS_BASE_PATH"] + "Images");
                    mailContent = mailContent.Replace("@NAME", data.FirstName + " " + data.LastName);
                    mailContent = mailContent.Replace("@datetime", DateTime.Now.ToShortDateString());
                    mailContent = mailContent.Replace("@Company", companyname);
                    mailContent = mailContent.Replace("@Link", ConfigurationManager.AppSettings["SITE_URL"].ToString() + "Account/ResetPassword/" + data.ResetCode);

                    SendMail.SendEMail(model.Email, "", "", companyname + " - Forgot Password", mailContent, "", fromEmail);
                }
                else { return "Email ID does not exist!"; }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Forgot Password for SuperAdmin/CompanyAdmin

        #region Reset password for SuperAdmin/CompanyAdmin

        /// <summary>
        /// Reset password for SuperAdmin/CompanyAdmin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ResetPassword(ResetPasswordViewModel model)
        {
            try
            {
                var data = _db.UserMasters.Where(x => x.ResetCode == model.Code && x.UserTypeID < 3).FirstOrDefault();
                if (data != null)
                {
                    byte[] saltBytes = new byte[8];
                    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                    rng.GetNonZeroBytes(saltBytes);
                    string pwdSalt = Convert.ToBase64String(saltBytes);
                    string pwdHash = ENCDEC.ComputeHash(model.Password, "MD5", saltBytes);
                    data.PasswordHash = pwdHash;
                    data.PasswordSalt = pwdSalt;
                    data.DefaultPassword = model.Password;
                    data.ResetCode = "";

                    _db.SaveChanges();
                }
                else { return "This link is expired."; }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Reset password for SuperAdmin/CompanyAdmin

        //#region Get userType
        ///// <summary>
        /////  Get userType
        ///// </summary>
        ///// <returns></returns>
        //public List<UserTypeMaster> GetAllUserType()
        //{
        //    try
        //    {
        //        return _db.UserTypeMasters.Where(x => x.UserTypeID == 1 || x.UserTypeID == 2 || x.UserTypeID == 6).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //#endregion
    }
}