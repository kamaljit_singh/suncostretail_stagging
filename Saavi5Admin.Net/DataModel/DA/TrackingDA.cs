﻿using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DA
{
    public class TrackingDA
    {
        private readonly SaaviAdminEntities _db;
        public TrackingDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region Get all vehicle list
        public List<AllOrderVehicleBO> GetAllVehicle(GetListModel model)
        {
            try
            {
                return (from o in _db.trnsCartMasters
                        join d in _db.DeliveryStatusMasters on o.CartID equals d.OrderID
                        join a in _db.VehicleMasters on d.VehicleID equals a.VehicleID
                        join b in _db.UserMasters on a.DriverID equals b.UserID into temp
                        from b in temp.DefaultIfEmpty()
                        where model.SearchText == "" ? true : (a.VehicleName.ToLower().Contains(model.SearchText.ToLower())
                        || a.VehicleNo.ToLower().Contains(model.SearchText.ToLower())
                        || a.RegistrationNo.ToLower().Contains(model.SearchText.ToLower())
                        || d.DelRunNumber==model.SearchText
                        || model.SearchText == null || model.SearchText == "")
                        select new AllOrderVehicleBO
                        {
                            OrderID = o.CartID,
                            DeliveryID = d.DeliveryID,
                            VehicleID = a.VehicleID,
                            VehicleName = a.VehicleName,
                            DriverID = a.DriverID,
                            DriverName = b.FirstName
                        }).Distinct().OrderByDescending(x => x.OrderID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Get delivery track records
        public AllDeliveryTrackBO GetDeliveryTrackList(long? id)
        {
            try
            {
                return (from a in _db.DeliveryTrackingMasters
                        join b in _db.DeliveryStatusMasters on a.DeliveryID equals b.DeliveryID into temp
                        from b in temp.DefaultIfEmpty()
                        where a.DeliveryID == id
                        select new AllDeliveryTrackBO
                        {
                            DriverName = _db.UserMasters.Where(x => x.UserID == b.DriverID).FirstOrDefault().FirstName ?? "",
                            VehicleName = _db.VehicleMasters.Where(x => x.VehicleID == b.VehicleID).FirstOrDefault().VehicleName ?? "",
                            lat = a.Latitutde,
                            lng = a.Longitude,
                            description = "",
                            TrackID = a.ID
                        }).OrderByDescending(x => x.TrackID).Take(1).FirstOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Get live location of all drivers
        public List<AllDeliveryTrackBO> GetDriversLiveLocation()
        {
            try
            {
                return (from a in _db.UserMasters
                        join b in _db.DeliveryStatusMasters on a.UserID equals b.DriverID
                        join c in _db.DeliveryTrackingMasters on b.DeliveryID equals c.DeliveryID 
                        join d in _db.DeliveryTrackingMasters on b.DeliveryID equals d.DeliveryID into temp2
                        from d in temp2.Where(x => x.ID > c.ID).DefaultIfEmpty()
                        where a.UserTypeID == 5 && d.ID == null
                        select new AllDeliveryTrackBO
                        {
                            DriverName = a.FirstName,
                            VehicleName = _db.VehicleMasters.Where(x => x.VehicleID == b.VehicleID).FirstOrDefault().VehicleName ?? "",
                            lat = c.Latitutde,
                            lng = c.Longitude,
                            description = "",
                            TrackID = c.ID
                        }).OrderByDescending(x => x.TrackID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
