﻿using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace DataModel.DA
{
    public class DeliveriesDA
    {
        private readonly SaaviAdminEntities _db;

        public DeliveriesDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region Get customers

        /// <summary>
        /// Get customers
        /// </summary>
        /// <returns></returns>
        public List<AllCustomerBO> GetAllCustomer(GetListModel model)
        {
            try
            {
                return (from a in _db.trnsCartMasters
                        join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID
                        where model.SearchText == "" ? true : (b.CustomerName.ToLower().Contains(model.SearchText.ToLower())
                        || b.ContactName.ToLower().Contains(model.SearchText.ToLower())
                        || b.Email.ToLower().Contains(model.SearchText.ToLower())
                        || b.AlphaCode.ToLower().Contains(model.SearchText.ToLower())
                        || b.Address1.ToLower().Contains(model.SearchText.ToLower())
                        || model.SearchText == null || model.SearchText == "")
                        select new AllCustomerBO
                        {
                            CustomerID = b.CustomerID,
                            CustomerName = b.CustomerName
                        }).Distinct().OrderBy(x => x.CustomerName).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get customers

        #region Get all invoices or by customerid

        /// <summary>
        /// Get all invoices or by customerid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AllOrderBO> GetAllOrderByCustomerID(int? id, int orderID, string type, DateTime? delDate)
        {
            try
            {
                var data = (from a in _db.trnsCartMasters
                            join c in _db.CustomerMasters on a.CustomerID equals c.CustomerID
                            join b in _db.DeliveryStatusMasters on a.CartID equals b.OrderID
                            into temp
                            from b in temp.DefaultIfEmpty()
                            where (a.OrderStatus == 1 || a.OrderStatus == 4 || a.OrderStatus == 6)
                            select new AllOrderBO
                            {
                                OrderID = a.CartID,
                                CustomerID = a.CustomerID,
                                CustomerName = c.CustomerName,
                                HasItemIssues = b.HasItemIssues,
                                OrderDate = a.OrderDate,
                                OrderStatusID = a.OrderStatus,
                                OrderStatus = a.SupplierOrderNumber ?? "Ordered" // (a.OrderStatus == 1) ? "Open" : (a.OrderStatus == 2) ? "Shipped" : (a.OrderStatus == 3) ? "Closed" : "Cancelled"
                            });

                if (orderID > 0)
                {
                    data = data.Where(x => x.OrderID == orderID);
                }

                if (id > 0)
                {
                    data = data.Where(x => x.CustomerID == id);
                }

                if (type == "i")
                {
                    data = data.Where(x => x.OrderStatusID == 9 || x.OrderStatusID == 4 || x.HasItemIssues == true);
                }
                else
                {
                    data = data.Where(x => x.OrderStatusID == 1 || x.OrderStatusID == 6);
                }

                if (delDate != null)
                {
                    data = data.Where(x => x.OrderDate == delDate);
                }

                var res = data.Distinct().OrderByDescending(x => x.OrderID).ToList();

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get all invoices or by customerid

        #region Get order's delivery details

        /// <summary>
        ///  Get order's delivery details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DeliveryDetailBO GetOrderDeliveryDetail(int id)
        {
            try
            {
                return (from a in _db.trnsCartMasters
                        join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID
                        //into temp from b in temp.DefaultIfEmpty()
                        join c in _db.DeliveryStatusMasters on a.CartID equals c.OrderID into temp2
                        from c in temp2.DefaultIfEmpty()
                        where a.CartID == id
                        select new DeliveryDetailBO
                        {
                            OrderNo = a.CartID,
                            CustomerID = b.CustomerID,
                            CustomerName = b.CustomerName,
                            ContactName = b.ContactName,
                            CustomerNumber = b.Phone1,
                            DeliveryAddress = a.AddressID > 0 ? _db.CustomerDeliveryAddresses.Where(x => x.AddressID == a.AddressID).FirstOrDefault().Address1 ?? b.DeliveryAddress1 : b.DeliveryAddress1,
                            EstimatedDeliveryDate = a.OrderDate,
                            OrderStatus = a.SupplierOrderNumber ?? "Ordered", //(a.OrderStatus == 1) ? "Open" : (a.OrderStatus == 2) ? "Shipped" : (a.OrderStatus == 3) ? "Closed" : "Cancelled",
                            GoodsTemp = c.GoodsTemp,
                            IsTempAuto = c.IsTempAuto,
                            Comments = c.Comments,
                            PaymentMethod = c.PaymentMethod,
                            PaymentRecieved = c.PaymentAmount,
                            TotalCartons = (int?)_db.trnsCartItemsMasters.Where(x => x.CartID == id).Count() ?? 0,
                            DeliveryIssuesRaised = (int?)_db.DeliveryIssuesMasters.Where(x => x.DeliveryID == c.DeliveryID).Count() ?? 0,
                            DeliveryID = c.DeliveryID,
                            CancelReason = c.CancelReason,
                            PONumber = a.PONumber
                        }).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get order's delivery details

        #region Get delivery images by

        public List<DeliveryImagesBO> GetDeliveryImages(long deliveryID, bool hasissue)
        {
            try
            {
                return (from a in _db.DeliveryStatusImageMasters
                        join b in _db.DeliveryStatusMasters on a.DeliveryID equals b.DeliveryID
                        where a.DeliveryID == deliveryID && a.IsSignature == false
                        select new DeliveryImagesBO
                        {
                            BaseUrl = a.BaseUrl + b.OrderID + "/",
                            ImageName = a.ImageName
                        }).ToList();
                //return _db.DeliveryStatusImageMasters.Where(x => x.DeliveryID == deliveryID && x.IsIssue == hasissue).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get delivery images by

        #region Get POD all invoices by customerid

        /// <summary>
        /// Get POD all invoices by customerid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AllOrderBO> GetPODAllOrdersByCustomerID(int? id)
        {
            try
            {
                var data = (from a in _db.trnsCartMasters
                            join b in _db.DeliveryStatusMasters on a.CartID equals b.OrderID
                            where a.CustomerID == id && a.OrderStatus > 2
                            select new AllOrderBO
                            {
                                OrderID = a.CartID,
                                CustomerID = a.CustomerID,
                                OrderDate = a.OrderDate,
                                Price = (double?)((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == a.CartID)
                                                   select new
                                                   {
                                                       Price = cd.Price ?? 0,
                                                       Quantity = cd.Quantity ?? 0
                                                   }).ToList().Select(c => (c.Price * c.Quantity)).Sum()) ?? 0,
                                OrderStatus = (a.OrderStatus == 1) ? "Open" : (a.OrderStatus == 2) ? "Shipped" : (a.OrderStatus == 3) ? "Closed" : "Cancelled"
                            });
                return data.OrderByDescending(x => x.OrderID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get POD all invoices by customerid

        #region Get Count for completion sidebar

        /// <summary>
        /// Get Count for completion sidebar
        /// </summary>
        /// <returns></returns>
        public CompletionSideBarBO GetCompletionSidebar()
        {
            try
            {
                var data = (from a in _db.trnsCartMasters
                            join b in _db.DeliveryStatusMasters on a.CartID equals b.OrderID
                            where a.OrderStatus == 3 || a.OrderStatus == 4
                            select new
                            {
                                a.CartID,
                                a.OrderStatus,
                                b.HasItemIssues
                            }).Distinct().ToList();
                var content = new CompletionSideBarBO();
                content.TotalLineItems = data.Count();
                content.TotalLineItemsDelivered = data.Where(x => x.OrderStatus == 3).Count();
                content.TotalLineItemsDeliveredPerc = content.TotalLineItems == 0 ? 0 : Math.Round((Convert.ToDouble(content.TotalLineItemsDelivered) / Convert.ToDouble(content.TotalLineItems)) * 100, 0);
                content.TotalLineItemsIssues = data.Where(x => (x.HasItemIssues == true && x.OrderStatus == 3) || x.OrderStatus == 4).Count();
                content.TotalKPIperc = 0;

                return content;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get Count for completion sidebar

        #region Delivery Issues Item list

        /// <summary>
        /// Delivery Issues Item list
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<DeliveryIssueItemBO> GetDeliveryIssueItemList(int id)
        {
            try
            {
                return (from a in _db.DeliveryStatusMasters
                        join b in _db.DeliveryIssuesMasters on a.DeliveryID equals b.DeliveryID into temp
                        from b in temp.DefaultIfEmpty()
                        join c in _db.ProductMasters on b.ProductID equals c.ProductID
                        where a.OrderID == id && a.HasItemIssues == true
                        select new DeliveryIssueItemBO
                        {
                            ProductID = b.ProductID,
                            ProductCode = c.ProductCode,
                            ProductName = c.ProductName,
                            Description = b.Description
                        }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Delivery Issues Item list

        #region Get all existing driver with assigned vehicle

        /// <summary>
        /// Get all existing driver with assigned vehicle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<AllListBO> GetAllAssignedDrivers(GetListModel model)
        {
            try
            {
                return (from a in _db.UserMasters
                        join b in _db.VehicleMasters on a.UserID equals b.DriverID
                        where a.UserTypeID == 5 && b.IsDeleted == false &&
                        (model.SearchText == "" ? true : a.FirstName.ToLower().Contains(model.SearchText.ToLower())
                         || a.ContactName.ToLower().Contains(model.SearchText.ToLower())
                         || a.Address.ToLower().Contains(model.SearchText.ToLower())
                         || b.VehicleName.ToLower().Contains(model.SearchText.ToLower())
                         || b.VehicleNo.ToLower().Contains(model.SearchText.ToLower())
                         || b.RegistrationNo.ToLower().Contains(model.SearchText.ToLower())
                         || model.SearchText == "" || model.SearchText == null)
                        select new AllListBO
                        {
                            ID = a.UserID,
                            Name = a.FirstName,
                            VehicleNo = b.VehicleNo,
                            RunNo = b.Route
                        }).OrderByDescending(c => c.ID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get all existing driver with assigned vehicle

        #region Get all assigned orders to Driver

        /// <summary>
        /// Get all assigned orders to Driver
        /// </summary>
        /// <param name="id"></param>
        /// <param name="runno"></param>
        /// <returns></returns>
        public List<AllAssignedOrdersBO> GetAllPendingOrders(int id, string runno, string suburb = "")
        {
            try
            {
                var dt = DateTime.Now.Date;
                var dt2 = DateTime.Now.Date.AddDays(3);
                var data = (from a in _db.trnsCartMasters
                            join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID into temp
                            from b in temp.DefaultIfEmpty()
                            join c in _db.CustomerDeliveryAddresses on a.AddressID equals c.AddressID into tempAdd
                            from c in tempAdd.DefaultIfEmpty()

                            where ((a.OrderStatus != 9 || a.OrderStatus != 4) && (a.OrderDate < dt2 && a.OrderDate >= dt) && a.IsDelivery == true)

                            select new AllAssignedOrdersBO
                            {
                                OrderID = a.CartID,
                                CustomerName = b.CustomerName,
                                DriverID = a.DriverID,
                                RunNo = b.RunNo,
                                DeliveryDate = a.OrderDate,
                                IsDelDateCrossed = a.OrderDate < dt.Date ? true : false,
                                Suburb = c.AddressID != null ? c.Suburb : b.Suburb
                            });
                if (id > 0)
                {
                    data = data.Where(x => x.DriverID == id);
                }
                else if (id < 0)
                {
                    data = data.Where(x => x.DriverID > 0);
                }
                else
                {
                    data = data.Where(x => x.DriverID == null);
                }

                if (runno != null && runno != "")
                {
                    data = data.Where(x => x.RunNo == runno);
                }

                if (suburb != "")
                {
                    data = data.Where(x => x.Suburb.ToLower() == suburb.ToLower());
                }

                var res = data.OrderBy(x => x.DeliveryDate).ToList();
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get all assigned orders to Driver

        #region assign order to driver

        /// <summary>
        /// assign order to driver
        /// </summary>
        /// <param name="driverid"></param>
        /// <param name="orderID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string AssignOrderToDriver(int driverid, string orderIDs, string type)
        {
            try
            {
                string[] orderArray = orderIDs == null ? null : orderIDs.Split(',');
                if (orderArray != null && orderArray.Length > 0 && orderIDs != "")
                {
                    var data = _db.trnsCartMasters.Where(x => orderIDs.Contains(x.CartID.ToString())).ToList();
                    if (data.Count > 0)
                    {
                        if (type == "A")
                        {
                            data.ForEach(a => a.DriverID = driverid);
                            _db.SaveChanges();
                            //return "Success";
                            var driver = _db.UserMasters.Where(x => x.UserID == driverid).FirstOrDefault();
                            string html = "";
                            if (driver != null)
                            {
                                foreach (var item in data)
                                {
                                    html += "<tr><td>" + item.CartID + "</td><td>" + _db.CustomerMasters.Where(x => x.CustomerID == item.CustomerID).FirstOrDefault().CustomerName + "</td><td>" + Convert.ToDateTime(item.OrderDate).ToShortDateString() + "</td></tr>";
                                }
                                //send mail
                                string companyname = ConfigurationManager.AppSettings["CompanyName"].ToString();
                                string fromEmail = ConfigurationManager.AppSettings["FROM_EMAILID"].ToString();
                                string mailContent = File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplates/assignordertodriver.htm"));
                                mailContent = mailContent.Replace("@pathImages", ConfigurationManager.AppSettings["WBS_BASE_PATH"] + "Images");
                                mailContent = mailContent.Replace("@NAME", driver.FirstName);
                                mailContent = mailContent.Replace("@Company", companyname);
                                // mailContent = mailContent.Replace("@orderIDs", orderIDs.TrimEnd(','));
                                mailContent = mailContent.Replace("@OrderList", html);

                                SendMail.SendEMail(driver.Email, "", "", companyname + " POD - New order(s) assigned", mailContent, "", fromEmail);

                                //check for notification
                                if (driver.IsNotificationEnabled == true)
                                {
                                    if (driver.DeviceToken != null && driver.DeviceToken != "")
                                    {
                                        SendMessage(driver.DeviceToken, "New invoice(s) assigned.", orderIDs);
                                    }
                                    else { return "Token"; }
                                }
                                else { return "off"; }
                            }
                        }
                        else
                        {
                            data.ForEach(x => x.DriverID = null);
                            _db.SaveChanges();
                        }
                    }
                    else { return "Invoice not avaliable!"; }
                }
                else { return "Select an invoice to drag!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion assign order to driver

        #region Send push notification

        /// <summary>
        /// Send push notification
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <param name="msg"></param>
        /// <param name="orderID"></param>
        public void SendMessage(string deviceToken, string msg, string orderIDs)
        {
            string serverKey = System.Configuration.ConfigurationManager.AppSettings["AndroidserverKey"];
            // deviceToken = "dVMJyNwXYh4%3AAPA91bG464IjIVSSRRCov4ktI7XXyZz0MeZaQJnA9O17PIDCyEaecexijuynf7mmoHxhi4DrfnVvyx4oVyTO2_cn7Fbrz4Q85oJibbrOft39KHqBPKjC1DE91YTDBOn20bR7L8Kh68bu";
            try
            {
                var result = "-1";
                var webAddr = "https://fcm.googleapis.com/fcm/send";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"to\": \"" + deviceToken + "\", \"notification\": {\"title\": \"" + ConfigurationManager.AppSettings["WEBSITE_NAME"].ToString() + " POD\",\"body\": \"" + msg + "\"}\"data\": {\"InvoiceID\": \"" + orderIDs.TrimEnd(',') + "\"}}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                // return result;
            }
            catch (Exception ex)
            {
                //  Response.Write(ex.Message);
            }
        }

        #endregion Send push notification

        #region get customer run number

        /// <summary>
        /// get customer run number
        /// </summary>
        /// <returns></returns>
        public List<SelectListItems> GetCustomerRunNo()
        {
            try
            {
                return (from a in _db.CustomerMasters
                        where a.RunNo != null && a.RunNo != ""
                        select new SelectListItems
                        {
                            Text = a.RunNo,
                            Value = a.RunNo
                        }).Distinct().ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get customer run number

        #region Manage Order's Delivery date

        public string ManageDelDate(int orderID, DateTime? delDate)
        {
            try
            {
                var data = _db.trnsCartMasters.Where(x => x.CartID == orderID).FirstOrDefault();
                if (data != null)
                {
                    data.OrderDate = delDate;
                    _db.SaveChanges();
                }
                else { return "No order avaliable!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Manage Order's Delivery date

        #region Get Delivery Runs By warehouse

        public List<string> DeliveryRunsByWarehouse(string warehouse)
        {
            try
            {
                var res = _db.CutoffGroupMasters.Where(x => x.GroupDetail == warehouse && x.GroupType == "Delivery").Select(s => s.GroupName).ToList();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Delivery Runs By warehouse

        #region Get Suburbs by delivery runs

        public List<string> GetSuburbsByDeliveryRuns(string run)
        {
            try
            {
                var res = (from a in _db.CutoffGroupMasters
                           join b in _db.CutoffGroupDetailMasters on a.GroupID equals b.GroupID
                           where a.GroupName == run && a.GroupType == "Delivery"
                           select new
                           {
                               b.ListValue
                           }).FirstOrDefault().ListValue.Split(',').ToList();

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get Suburbs by delivery runs
    }
}