﻿using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CommonFunctions;

namespace DataModel.DA
{
    public class DriverDA
    {
        private readonly SaaviAdminEntities _db;
        public DriverDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region Get All Driver
        /// <summary>
        /// Get All Driver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<AllListBO> GetAllDrivers(GetListModel model)
        {
            try
            {
                return (from a in _db.UserMasters
                        where a.UserTypeID == 5 &&
                        (model.SearchText == "" ? true : a.FirstName.ToLower().Contains(model.SearchText.ToLower())
                         || a.ContactName.ToLower().Contains(model.SearchText.ToLower())
                         || a.Address.ToLower().Contains(model.SearchText.ToLower())
                         || model.SearchText == "" || model.SearchText == null)
                        select new AllListBO
                        {
                            ID = a.UserID,
                            Name = a.FirstName
                        }).OrderByDescending(c => c.ID).ToList();//.Page(model.PageNumber * model.PageSize, model.PageSize).ToList();

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get User by id
        /// <summary>
        /// Get User by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserBO GetUserByID(int? id)
        {
            try
            {
                return (from a in _db.UserMasters
                        where a.UserID == id
                        select new UserBO
                        {
                            UserID = a.UserID,
                            Address = a.Address,
                            Age = a.Age,
                            DefaultPassword = a.DefaultPassword,
                            Image = a.Image != null && a.Image != "" ? a.Image : "noimage.jpg",
                            DriverLicenseImage = a.DriverLicenseImage != null && a.DriverLicenseImage != "" ? a.DriverLicenseImage : "noimage.jpg",
                            DriverLicenseNo = a.DriverLicenseNo,
                            DriverShift = a.DriverShift,
                            DrivingExperience = a.DrivingExperience,
                            Email = a.Email,
                            FirstName = a.FirstName,
                            Phone = a.Phone,
                            VehicleID = (short?)_db.VehicleMasters.Where(x => x.DriverID == a.UserID).FirstOrDefault().VehicleID ?? 0
                        }).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Manage Driver
        /// <summary>
        /// Manage Driver
        /// </summary>
        /// <returns></returns>
        public string ManageDriver(UserBO model)
        {
            try
            {
                var data = _db.UserMasters.Where(x => x.Email.ToLower() == model.Email.ToLower() && x.UserID != model.UserID).FirstOrDefault();
                if (data == null)
                {
                    byte[] saltBytes = new byte[8];
                    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                    rng.GetNonZeroBytes(saltBytes);
                    string pwdSalt = Convert.ToBase64String(saltBytes);
                    string pwdHash = "";
                    if (model.UserID > 0)
                    {
                        var user = _db.UserMasters.Where(x => x.UserID == model.UserID).FirstOrDefault();
                        if (user != null)
                        {
                            if (model.driverImage != null && model.driverImage.FileName != "")
                            {
                                if (user.Image == null || user.Image == "")
                                {
                                    var filename = Guid.NewGuid() + Path.GetExtension(model.driverImage.FileName);
                                    user.Image = filename;
                                }
                                model.driverImage.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Uploads/Users/"), user.Image));
                            }
                            if (model.licenceImage != null && model.licenceImage.FileName != "")
                            {
                                if (user.DriverLicenseImage == null || user.DriverLicenseImage == "")
                                {
                                    var filename = Guid.NewGuid() + Path.GetExtension(model.licenceImage.FileName);
                                    user.DriverLicenseImage = filename;
                                }
                                model.licenceImage.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Uploads/DriverLicense/"), user.DriverLicenseImage));

                            }
                            user.FirstName = model.FirstName;
                            user.Email = model.Email;
                            user.Phone = model.Phone;
                            user.DriverLicenseNo = model.DriverLicenseNo;
                            user.Address = model.Address;
                            user.DrivingExperience = model.DrivingExperience;
                            user.Age = model.Age;
                            user.DriverShift = model.DriverShift;
                            user.ModifiedOn = DateTime.Now;
                            if (!string.IsNullOrEmpty(model.DefaultPassword) && model.DefaultPassword != user.DefaultPassword)
                            {
                                pwdHash = ENCDEC.ComputeHash(model.DefaultPassword, "MD5", saltBytes);
                                user.PasswordHash = pwdHash;
                                user.PasswordSalt = pwdSalt;
                                user.DefaultPassword = model.DefaultPassword;
                            }
                            _db.SaveChanges();
                            return "Success";
                        }
                        else
                        {
                            return "No user available!";
                        }
                    }
                    else
                    {
                        UserMaster obj = new UserMaster();
                        if (model.driverImage != null && model.driverImage.FileName != "")
                        {
                            var filename = Guid.NewGuid() + Path.GetExtension(model.driverImage.FileName);
                            model.driverImage.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Uploads/Users/"), filename));
                            obj.Image = filename;
                        }
                        if (model.licenceImage != null && model.licenceImage.FileName != "")
                        {
                            var filename = Guid.NewGuid() + Path.GetExtension(model.licenceImage.FileName);
                            model.licenceImage.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Uploads/DriverLicense/"), filename));
                            obj.DriverLicenseImage = filename;
                        }

                        if (string.IsNullOrEmpty(model.DefaultPassword))
                        {
                            pwdHash = ENCDEC.ComputeHash("12345678", "MD5", saltBytes);
                            obj.DefaultPassword = "12345678";
                        }
                        else
                        {
                            pwdHash = ENCDEC.ComputeHash(model.DefaultPassword, "MD5", saltBytes);
                            obj.DefaultPassword = model.DefaultPassword;
                        }
                        obj.FirstName = model.FirstName;
                        obj.Email = model.Email;
                        obj.Phone = model.Phone;
                        obj.DriverLicenseNo = model.DriverLicenseNo;
                        obj.Address = model.Address;
                        obj.DrivingExperience = model.DrivingExperience;
                        obj.Age = model.Age;
                        obj.DriverShift = model.DriverShift;
                        obj.PasswordHash = pwdHash;
                        obj.PasswordSalt = pwdSalt;
                        obj.CreatedOn = DateTime.Now;
                        obj.UserTypeID = 5;
                        obj.IsActive = true;
                        obj.IsNotificationEnabled = true;
                        _db.UserMasters.Add(obj);
                        _db.SaveChanges();

                        return "Success";
                    }
                }
                else
                { return "Email ID is already exist!"; }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region Get All vehicles
        public List<AllListBO> GetAllVehicles(GetListModel model)
        {
            try
            {
                return (from a in _db.VehicleMasters
                        where (model.SearchText == "" ? true : a.VehicleName.ToLower().Contains(model.SearchText.ToLower())
                        || a.VehicleNo.ToLower().Contains(model.SearchText.ToLower())
                        || a.RegistrationNo.ToLower().Contains(model.SearchText.ToLower())
                        || model.SearchText == "" || model.SearchText == null)
                        select new AllListBO
                        {
                            ID = a.VehicleID,
                            Name = a.VehicleNo,
                            IsDeleted = a.IsDeleted,
                            RunNo = a.Route
                        }).OrderByDescending(x => x.ID).ToList();//.Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region GEt vehicle by id
        public VehicleBO GetVehicleByID(int? id)
        {
            try
            {
                return (from a in _db.VehicleMasters
                        join b in _db.VehicleChecklistMasters on a.VehicleID equals b.VehicleID into temp
                        from b in temp.DefaultIfEmpty()
                        where a.VehicleID == id
                        select new VehicleBO
                        {
                            VehicleID = a.VehicleID,
                            DriverID = a.DriverID,
                            VehicleName = a.VehicleName,
                            VehicleNo = a.VehicleNo,
                            RegistrationNo = a.RegistrationNo,
                            Route = a.Route,
                            IsDeleted = a.IsDeleted,
                            CheckListID = b.ID,
                            Shift = b.Shift,
                            IsFixturesFittingOK = b.IsFixturesFittingOK,
                            IsLightSignalOK = b.IsLightSignalOK,
                            IsTyreConditionOK = b.IsTyreConditionOK,
                            IsVehicleConditionOK = b.IsVehicleConditionOK,
                            IsWindscreenOK = b.IsWindscreenOK,
                            OdometerEnd = b.OdometerEnd,
                            OdometerStart = b.OdometerStart,
                            TempChilled = b.TempChilled,
                            TempFrozen = b.TempFrozen,
                            Temprature = b.Temprature,
                            Weight = b.Weight,
                            Comments = b.Comments
                        }).OrderByDescending(x => x.CheckListID == null ? x.VehicleID : x.CheckListID).FirstOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Assign Vehicle to Driver
        /// <summary>
        /// Assign Vehicle to Driver
        /// </summary>
        /// <param name="vID"></param>
        /// <param name="dID"></param>
        /// <returns></returns>
        public string AssignVehicleToDriver(int vID, int dID)
        {
            try
            {
                var data = _db.VehicleMasters.Where(x => x.VehicleID == vID).FirstOrDefault();
                if (data != null)
                {
                    var driver = _db.UserMasters.Where(x => x.UserID == dID && x.UserTypeID == 5).FirstOrDefault();
                    if (driver != null)
                    {
                        var vehicle = _db.VehicleMasters.Where(x => x.DriverID == dID).ToList();
                        vehicle.ForEach(x => x.DriverID = null);
                        _db.SaveChanges();
                        data.DriverID = dID;
                        _db.SaveChanges();
                    }
                    else { return "Driver does not exist!"; }
                }
                else { return "No vehicle available!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region Manage vehicle
        /// <summary>
        /// Manage vehicle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageVehicle(VehicleBO model)
        {
            try
            {
                if (model.VehicleID > 0)
                {
                    var data = _db.VehicleMasters.Where(x => x.VehicleID == model.VehicleID).FirstOrDefault();
                    if (data != null)
                    {
                        data.VehicleName = model.VehicleName;
                        data.VehicleNo = model.VehicleNo;
                        data.RegistrationNo = model.RegistrationNo;
                        data.Route = model.Route;
                        data.ModifiedDate = DateTime.Now;
                        _db.SaveChanges();

                        VehicleChecklistMaster vObj = new VehicleChecklistMaster();
                        vObj.VehicleID = data.VehicleID;
                        vObj.IsActive = true;
                        vObj.IsFixturesFittingOK = model.IsFixturesFittingOK;
                        vObj.IsLightSignalOK = model.IsLightSignalOK;
                        vObj.IsTyreConditionOK = model.IsTyreConditionOK;
                        vObj.IsVehicleConditionOK = model.IsVehicleConditionOK;
                        vObj.IsWindscreenOK = model.IsWindscreenOK;
                        vObj.RunNo = model.Route;
                        vObj.Shift = model.Shift;
                        vObj.CreatedDate = DateTime.Now;
                        vObj.OdometerEnd = model.OdometerEnd;
                        vObj.OdometerStart = model.OdometerStart;
                        vObj.TempChilled = model.TempChilled;
                        vObj.TempFrozen = model.TempFrozen;
                        vObj.Temprature = model.Temprature;
                        vObj.Weight = model.Weight;
                        _db.VehicleChecklistMasters.Add(vObj);
                        _db.SaveChanges();
                    }
                    else { return "No vehicle avaliable!"; }
                }
                else
                {
                    VehicleMaster obj = new VehicleMaster();
                    obj.VehicleName = model.VehicleName;
                    obj.VehicleNo = model.VehicleNo;
                    obj.RegistrationNo = model.RegistrationNo;
                    obj.Route = model.Route;
                    obj.CreatedDate = DateTime.Now;
                    _db.VehicleMasters.Add(obj);
                    _db.SaveChanges();

                    VehicleChecklistMaster vObj = new VehicleChecklistMaster();
                    vObj.VehicleID = obj.VehicleID;
                    vObj.IsActive = true;
                    vObj.IsFixturesFittingOK = model.IsFixturesFittingOK;
                    vObj.IsLightSignalOK = model.IsLightSignalOK;
                    vObj.IsTyreConditionOK = model.IsTyreConditionOK;
                    vObj.IsVehicleConditionOK = model.IsVehicleConditionOK;
                    vObj.IsWindscreenOK = model.IsWindscreenOK;
                    vObj.RunNo = model.Route;
                    vObj.Shift = model.Shift;
                    vObj.CreatedDate = DateTime.Now;
                    _db.VehicleChecklistMasters.Add(vObj);
                    _db.SaveChanges();
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region Delete Vehicle
        /// <summary>
        /// Delete Vehicle
        /// </summary>
        /// <param name="vehicleID"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public string DeleteVehicle(int vehicleID, bool val)
        {
            try
            {
                var data = _db.VehicleMasters.Where(x => x.VehicleID == vehicleID).FirstOrDefault();
                if (data != null)
                {
                    // _db.VehicleMasters.Remove(data);
                    data.IsDeleted = val;
                    _db.SaveChanges();
                }
                else { return "Vehicle not available!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        #endregion

        #region Get driver compliance
        /// <summary>
        ///  Get driver compliance
        /// </summary>
        /// <returns></returns>
        public List<AllDriverCompBO> GetallDriverComp(GetListModel model)
        {
            try
            {
                return (from a in _db.UserMasters
                        join b in _db.trnsCartMasters on a.UserID equals b.DriverID
                        join c in _db.DeliveryStatusMasters on b.CartID equals c.OrderID
                        where a.UserTypeID == 5
                         && (model.SearchText == "" ? true : a.FirstName.ToLower().Contains(model.SearchText.ToLower())
                        || model.SearchText == "" || model.SearchText == null)
                        select new AllDriverCompBO
                        {
                            ID = a.UserID,
                            Name = a.FirstName,
                            InvoiceNo = b.CartID,
                            IsOnline = a.IsOnline == true ? true : false,
                            OrderStatus = (b.OrderStatus == 1) ? "Open" : (b.OrderStatus == 2) ? "Shipped" : (b.OrderStatus == 3) ? "Closed" : "Cancelled",
                            IsCompliance = _db.VehicleChecklistMasters.Where(x => x.DriverID == a.UserID && x.CreatedDate == DateTime.Now).OrderByDescending(x => x.ID).FirstOrDefault() != null ? true : false
                        }).OrderByDescending(c => c.InvoiceNo).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get vehicles with Delivery status
        /// <summary>
        /// Get vehicles with Delivery status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<VehicleDeliveryStatusBO> GetVehiclesDeliveryStatus(GetListModel model)
        {
            try
            {
                return (from a in _db.VehicleMasters
                        join b in _db.DeliveryStatusMasters on a.VehicleID equals b.VehicleID
                        join c in _db.DeliveryStatusMasters on a.VehicleID equals c.VehicleID into temp
                        from c in temp.Where(x => x.DeliveryID > b.DeliveryID).DefaultIfEmpty()
                        join d in _db.trnsCartMasters on b.OrderID equals d.CartID into temp2
                        from d in temp2.DefaultIfEmpty()
                        where c.DeliveryID == null && d.OrderStatus == 3
                        && (model.SearchText == "" ? true : a.VehicleName.ToLower().Contains(model.SearchText.ToLower())
                        || a.VehicleNo.ToLower().Contains(model.SearchText.ToLower())
                        || a.RegistrationNo.ToLower().Contains(model.SearchText.ToLower())
                        || model.SearchText == "" || model.SearchText == null)
                        select new VehicleDeliveryStatusBO
                        {
                            ID = a.VehicleID,
                            Name = a.VehicleNo,
                            RunNo = a.Route,
                            HasItemIssue = b.HasItemIssues
                        }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get distinct Run No's 

        public List<DeliveryRunBO> GetDeliveryRuns()
        {
            try
            {
                var runs = (from c in _db.CustomerMasters
                            where c.RunNo != null && c.RunNo != ""
                            select new DeliveryRunBO
                            {
                                Value = c.RunNo,
                                Text = c.RunNo
                            }).Distinct().ToList();

                return runs;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion
    }
}
