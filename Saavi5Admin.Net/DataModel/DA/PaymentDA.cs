﻿using AutoMapper;
using CommonFunctions;
using DataModel.BO;
using DataModel.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Stripe;

namespace DataModel.DA
{
    public class PaymentDA
    {
        private readonly SaaviAdminEntities _db;

        public PaymentDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region Capture Stripe Payment
        /// <summary>
        /// capture payment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string CapturePayment(PaymentBO model)
        {
            try
            {
                var paymentDetail = _db.PaymentMasters.Where(i => i.CartID == model.CartID).FirstOrDefault();
                if (paymentDetail != null)
                {
                    var availableCaptureAmount = paymentDetail.AuthorizeAmount - paymentDetail.CaptureAmount;
                    if (availableCaptureAmount <= model.Amount)
                    {
                        return "Requested amount is greater than the amount (" + availableCaptureAmount + ") available for capture";
                    }

                    var connectedAccountId = ConfigurationManager.AppSettings["StripeConnectedAccountID"] ?? "";
                    if (connectedAccountId.Length < 5)
                    {
                        return "Connected account ID not configured.";
                    }

                    StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["StripeApiKey"] ?? "";

                    var options = new PaymentIntentCaptureOptions
                    {
                        AmountToCapture = Convert.ToInt64(model.Amount * 100),
                    };
                    var service = new PaymentIntentService();
                    var requestOptions = new RequestOptions();
                    requestOptions.StripeAccount = connectedAccountId;
                    var paymentIntent = service.Capture(paymentDetail.StripeChargeID, options, requestOptions);

                    if (paymentIntent.Status == "succeeded")
                    {
                        paymentDetail.CaptureAmount = model.Amount + paymentDetail.CaptureAmount;
                    }

                    LogPayment(JsonConvert.SerializeObject(paymentIntent).ToString(), paymentDetail.CartID, paymentDetail.PaymentID, paymentIntent.Id, paymentIntent.Status, paymentDetail.AuthorizeAmount, paymentDetail.CaptureAmount);

                    if (paymentIntent.Status == "succeeded")
                    {
                        paymentDetail.IsStripePaymentSuccess = true;
                        paymentDetail.ModifiedBy = "Admin";
                        paymentDetail.ModifiedOn = DateTime.Now;
                        _db.SaveChanges();
                        if (!string.IsNullOrEmpty(Convert.ToString(model.CartID)))
                        {
                            var cartlatest = _db.trnsCartMasters.Where(c => c.CartID == model.CartID).FirstOrDefault();
                            cartlatest.IsPaymentDone= true;
                            cartlatest.StripeChargeID = paymentDetail.StripeChargeID;                            
                            _db.SaveChanges();
                        }
                        var options1 = new PaymentIntentGetOptions();
                        var intent = service.Get(paymentDetail.StripeChargeID, options1, requestOptions);
                        var user = _db.UserMasters.Where(p => p.UserID == model.UserID && p.CustomerID == model.CustomerID).FirstOrDefault();
                        if (intent != null)
                        {                           
                            var updateoptions = new PaymentIntentUpdateOptions();
                            updateoptions.Description = "Suncoast Fresh – Order – " + model.CartID ;
                            if (user != null && !string.IsNullOrEmpty(user.Email))
                            {
                                updateoptions.Description = "Suncoast Fresh – Order - " + model.CartID + " - " + user.Email;
                            }
                            try
                            {
                               PaymentIntent obj = service.Update(paymentDetail.StripeChargeID, updateoptions, requestOptions);
                            }                            
                            catch (StripeException ex)
                            {
                                LogPayment("Updating Payment Intent Description:" + ex.Message, model.CartID);
                            }
                            catch (Exception ex)
                            {
                                LogPayment("Updating Payment Intent Description:" + ex.Message, model.CartID);
                            }
                        }

                        var balanceservice = new BalanceTransactionService();
                        if (intent.Charges != null && intent.Charges.Data != null && intent.Charges.Data.Count > 0)
                        {
                            try
                            {
                                var chargeUpdateoptions = new ChargeUpdateOptions();
                                chargeUpdateoptions.Description = "Suncoast Fresh – Order - " + model.CartID;
                                if (user != null && !string.IsNullOrEmpty(user.Email))
                                {
                                    chargeUpdateoptions.Description = "Suncoast Fresh – Order - " + model.CartID + " - " + user.Email;
                                }
                                var serviceCharge = new ChargeService();
                                Charge charge = serviceCharge.Update(intent.Charges.Data[0].Id, chargeUpdateoptions, requestOptions);
                            }
                            catch (StripeException ex)
                            {
                                LogPayment("Updating Charge Description:" + ex.Message, model.CartID);
                            }
                            catch (Exception ex)
                            {
                                LogPayment("Updating Charge Description:" + ex.Message, model.CartID);
                            }
                            BalanceTransaction objbalance = balanceservice.Get(intent.Charges.Data[0].BalanceTransactionId, null, requestOptions);
                            StripePaymentDetail objStripePaymentDetail = new StripePaymentDetail();
                            objStripePaymentDetail.StripeChargeID = paymentDetail.StripeChargeID;
                            objStripePaymentDetail.TotalCaptureAmount = paymentDetail.CaptureAmount;
                            objStripePaymentDetail.CreatedBy = model.UserID.ToString();
                            objStripePaymentDetail.CreatedOn = DateTime.Now;
                            objStripePaymentDetail.ModifiedOn = DateTime.Now;
                            objStripePaymentDetail.NetAmount = Convert.ToDouble(objbalance.Net) / 100;
                            objStripePaymentDetail.TotalFees = Convert.ToDouble(objbalance.Fee) / 100;
                            foreach (var item in objbalance.FeeDetails)
                            {
                                if (item.Description.ToLower().Trim() == "gst" && item.Type.ToLower().Trim() == "tax")
                                {
                                    objStripePaymentDetail.Tax = Convert.ToDouble(item.Amount) / 100;

                                }
                                else if (item.Description.ToLower() == "stripe processing fees" && item.Type.ToLower() == "stripe_fee")
                                {
                                    objStripePaymentDetail.StripeProcessingFee = Convert.ToDouble(item.Amount) / 100;
                                }
                                else if (item.Description.ToLower().Trim() == "application fee" && item.Type.ToLower().Trim() == "application_fee")
                                {
                                    objStripePaymentDetail.ApplicationFees = Convert.ToDouble(item.Amount) / 100;
                                }
                            }
                            if (intent.Charges.Data[0].Refunds != null && intent.Charges.Data[0].Refunds.Data != null)
                            {
                                var ser11 = new BalanceTransactionService();
                                BalanceTransaction objbalanceRefund = ser11.Get(intent.Charges.Data[0].Refunds.Data[0].BalanceTransactionId, null, requestOptions);
                                objStripePaymentDetail.RefundTax = 0;
                                objStripePaymentDetail.RefundStripeProcessingFee = 0;
                                foreach (var item in objbalanceRefund.FeeDetails)
                                {
                                    if (item.Description.ToLower().Trim() == "gst refund" && item.Type.ToLower().Trim() == "tax")
                                    {
                                        objStripePaymentDetail.RefundTax = (Convert.ToDouble(item.Amount) / 100);

                                    }
                                    else if (item.Description.ToLower() == "stripe processing fee refund" && item.Type.ToLower() == "stripe_fee")
                                    {
                                        objStripePaymentDetail.RefundStripeProcessingFee = Convert.ToDouble(item.Amount) / 100;
                                    }

                                }
                                objStripePaymentDetail.TotalFees = objStripePaymentDetail.TotalFees + objStripePaymentDetail.RefundTax + objStripePaymentDetail.RefundStripeProcessingFee;
                                objStripePaymentDetail.NetAmount = (paymentDetail.AuthorizeAmount) + (Convert.ToDouble(objbalanceRefund.Amount) / 100) - objStripePaymentDetail.TotalFees;
                            }

                            _db.StripePaymentDetails.Add(objStripePaymentDetail);
                            _db.SaveChanges();
                        }

                        return "Success";
                    }
                    else
                    {
                        return "Payment not go as expected with status: " + paymentIntent.Status;
                    }
                }

                return "Payment details not available";
            }
            catch (Exception ex)
            {
                LogPayment(ex.ToString(), model.CartID);
                return "Capture failed because of: " + ex.Message;
            }
        }
        #endregion

        private void LogPayment(string error, long cartId, long pID = 0, string piId = "", string pStatus = "", double? aAmt = 0, double? cAmt = 0)
        {
            //Log Payment response to DB
            PaymentLogMaster pLog = new PaymentLogMaster();
            pLog.PaymentID = pID;
            pLog.CartID = cartId;
            pLog.AuthorizeAmount = aAmt;
            pLog.CaptureAmount = cAmt;
            pLog.CreatedOn = DateTime.Now;
            pLog.StripeChargeID = piId;
            pLog.StripeStatus = pStatus;
            pLog.StripeTokenID = "";
            pLog.StripeResponse = error;
            _db.PaymentLogMasters.Add(pLog);
            _db.SaveChanges();
        }

        #region Connect Stripe Account
        /// <summary>
        /// Connect Stripe Account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ConnectAccountBO ConnectAccount(string code, string clientId)
        {
            try
            {
                StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["StripeApiKey"] ?? "";

                var options = new OAuthTokenCreateOptions
                {
                    GrantType = "authorization_code",
                    Code = code,
                };

                var service = new OAuthTokenService();
                var response = service.Create(options);
                LogPayment(response.ToString(), 0);
                // Access the connected account id in the response
                return new ConnectAccountBO { Connected = true, AccountID = response.StripeUserId };
            }
            catch (Exception ex)
            {
                LogPayment(ex.ToString(), 0);
                return new ConnectAccountBO { Connected = false, Error = ex.Message };
            }
        }
        #endregion


        /// <summary>
        /// GetRefundPaymentDetail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public StripePaymentDetail GetRefundPaymentDetail(long CartID)
        {
            try
            {
                StripePaymentDetail stripedetail = new StripePaymentDetail();
                var paymentDetail = _db.PaymentMasters.Where(i => i.CartID == CartID).FirstOrDefault();
                if (paymentDetail != null)
                {
                    var stripepaymentdetail = _db.StripePaymentDetails.Where(i => i.StripeChargeID == paymentDetail.StripeChargeID).FirstOrDefault();
                    if (stripepaymentdetail != null)
                    {
                        stripedetail = stripepaymentdetail;
                    }

                }
                return stripedetail;
            }
            catch (Exception ex)
            {

                LogPayment(ex.ToString(), CartID);
                return null;
            }
        }
        /// <summary>
        /// GetRefundPaymentDetail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PaymentRefundMaster GetRefundDetail(long CartID)
        {
            try
            {
                var paymentRefundDetail = _db.PaymentRefundMasters.Where(i => i.CartID == CartID).FirstOrDefault();
                if (paymentRefundDetail != null)
                {
                    return paymentRefundDetail;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// GetCustomerDetail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CustomerMaster GetCustomerDetail(long? cartid, long? CustomerID)
        {
            try
            {
                if (CustomerID == 0)
                {
                    var cart = _db.trnsCartMasters.Where(i => i.CartID == cartid).FirstOrDefault();
                    if (cart != null)
                    {
                        var customerDetail = _db.CustomerMasters.Where(i => i.CustomerID == cart.CustomerID).FirstOrDefault();
                        if (customerDetail != null)
                        {
                            return customerDetail;
                        }
                    }
                }
                else
                {
                    var customerDetail = _db.CustomerMasters.Where(i => i.CustomerID == CustomerID).FirstOrDefault();
                    if (customerDetail != null)
                    {
                        return customerDetail;
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #region Refund Stripe Payment 
        /// <summary>
        /// StripeRefundPayment 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string StripeRefundPayment(RefundBO model)
        {
            try
            {
                StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["StripeApiKey"] ?? "";
                var connectedAccountId = ConfigurationManager.AppSettings["StripeConnectedAccountID"] ?? "";
                var refunds = new RefundService();
                var refundOptions = new RefundCreateOptions
                {
                    PaymentIntent = model.PaymentReference,
                    Amount = Convert.ToInt64(model.Amount * 100)
                };
                var requestOptions = new RequestOptions();
                requestOptions.StripeAccount = connectedAccountId;
                var refund = refunds.Create(refundOptions, requestOptions);

                if (refund != null)
                {
                    PaymentRefundMaster pRefundLog = new PaymentRefundMaster();
                    pRefundLog.RefundAmount = model.Amount;
                    pRefundLog.RefundId = refund.Id;
                    pRefundLog.CartID = model.CartID;
                    pRefundLog.CartItemID = -1;
                    pRefundLog.StripeChargeID = model.PaymentReference;
                    pRefundLog.CaptureAmount = model.TotalCaptureAmount;
                    pRefundLog.CreatedOn = DateTime.Now;
                    pRefundLog.StripeRefundStatus = refund.Status;
                    pRefundLog.StripeResponse = Convert.ToString(refund);
                    _db.PaymentRefundMasters.Add(pRefundLog);
                    _db.SaveChanges();
                    return "Success";
                }
                return "Refund fail";
            }
            catch (Exception ex)
            {
                LogPayment(ex.ToString(), model.CartID, 0, model.PaymentReference);
                return "Refund failed because of: " + ex.Message;
            }
        }


        /// <summary>
        /// GetPaymentDetail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string GetPaymentDetail(long CartID)
        {
            try
            {
                string paymentReference = "";
                var paymentDetail = _db.PaymentMasters.Where(i => i.CartID == CartID).FirstOrDefault();
                if (paymentDetail != null)
                {
                    paymentReference = paymentDetail.StripeChargeID;
                    var stripedetail = _db.StripePaymentDetails.Where(i => i.StripeChargeID == paymentDetail.StripeChargeID).FirstOrDefault();


                }
                return paymentReference;
            }
            catch (Exception ex)
            {
                LogPayment(ex.ToString(), CartID);
                return null;
            }
        }
        #endregion
        public List<OrderDetailModal> GetOrderDetails(long cartID)
        {
            try
            {
                using (var _db = new SaaviAdminEntities())
                {

                    var res = (from a in _db.trnsCartMasters
                               join b in _db.trnsCartItemsMasters on a.CartID equals b.CartID
                               join c in _db.CustomerMasters on a.CustomerID equals c.CustomerID
                               join h in _db.trnsCartItemsMasterHistories on b.CartItemID equals h.CartItemID
                               join p in _db.ProductMasters on b.ProductID equals p.ProductID
                               join u in _db.UnitOfMeasurementMasters on b.UnitId equals u.UOMID
                               join com in _db.DeliveryCommentsMasters on a.CommentID equals com.CommentID into temp
                               from com in temp.DefaultIfEmpty()
                               where a.CartID == cartID
                               select new OrderDetailModal
                               {
                                   CartID = a.CartID,
                                   OrderDate = a.OrderDate,
                                   RunNo = a.RunNo ?? "",
                                   Address = c.DeliveryAddress1 ?? (c.Address1 ?? "") + " " + (c.Address2 ?? "") + " " + (c.Suburb + " ") + c.PostCode,
                                   Suburb = c.Suburb ?? "",
                                   ProductID = p.ProductID,
                                   ProductCode = p.ProductCode ?? "",
                                   ProductName = p.ProductName ?? "",
                                   UOM = u.UOMDesc ?? "",
                                   Price = b.Price,
                                   CustomerID = c.CustomerID,
                                   CustomerName = c.CustomerName ?? "",
                                   CustomerEmail = c.Email ?? "",
                                   Warehouse = c.Warehouse ?? "",
                                   CartItemID = b.CartItemID,
                                   Quantity = b.Quantity,
                                   OrderComments = (a.CommentID != null && a.CommentID > 0) ? com.CommentDescription : "",
                                   IsDelivery = a.IsDelivery ?? true,
                                   CreatedDate = a.CreatedDate,
                                   DeliveryNotes = a.CommentLine ?? "",
                                   RevisedPrice = h.Price,
                                   RevisedQuantity = h.Quantity,
                                   TotatRevisedPrice = b.Price * b.Quantity,
                                   TotatOriginalPrice = h.Price * h.Quantity,
                                   Difference = (h.Price * h.Quantity) - (b.Price * b.Quantity),
                                   CouponAmount = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.CouponAmount : 0,
                                   RevisedFreightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? a.FrieghtCharges:0,                                                                                                                                  
                                   OriginalFreightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? (double?)(from soh in _db.DeliveryCharges
                                                                 select new { FraightCharge = soh.DeliveryFee }).Take(1).Select(c => c.FraightCharge).Sum() ?? 0 : 0,
                                   IsHasFreightCharges = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? true : false

                               }).ToList();


                    return res;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<OrderPaymentHistoryModal> GetOrderHistoryDetails(long cartID)
        {
            try
            {
                using (var _db = new SaaviAdminEntities())
                {

                    var res = (from oh in _db.trnsCartMasterHistories
                               join p in _db.DeliveryPickupMasters on oh.PickupID equals p.ID into po
                               from p in po.DefaultIfEmpty()
                               join c in _db.CustomerMasters on oh.CustomerID equals c.CustomerID into cm
                               from  c in cm.DefaultIfEmpty()
                               join a in _db.trnsCartMasters on oh.CartID equals a.CartID into gj
                               from a in gj.DefaultIfEmpty()
                               where oh.CartID == cartID
                               let OriginalFreightCharge = (a.HasFreightCharges != null && a.HasFreightCharges == true) ? (double?)(from soh in _db.DeliveryCharges
                                                                                                                                    select new { FraightCharge = soh.DeliveryFee }).Take(1).Select(c => c.FraightCharge).Sum() ?? 0 : 0
                               let Calprice = (double?)((from cd in _db.trnsCartItemsMasterHistories.Where(s => s.CartID == a.CartID)
                                                         select new { Price = cd.Price ?? 0, Quantity = cd.Quantity ?? 0 }).ToList().Select(c => (c.Price * c.Quantity)).Sum()) ?? 0
                               let coupanAmount = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.CouponAmount : 0
                               select new OrderPaymentHistoryModal
                               {
                                   CartID = oh.CartID,
                                   OrderDate = oh.OrderDate,
                                   RunNo = oh.RunNo ?? "",
                                   Address = oh.Address1 ?? "",
                                   CustomerEmail = c.Email ?? "",
                                   CustomerName = c.CustomerName ?? "",
                                   OrderComments = oh.CommentDescription ?? "",                                   
                                   DeliveryType = oh.DeliveryType ?? "",
                                   CreatedDate = oh.CreatedDate,
                                   DeliveryNotes = oh.CommentLine ?? "",
                                   CouponAmount = (a.IsCouponApplied != null && a.IsCouponApplied == true) ? a.CouponAmount : 0,                                 
                                   TotatRevisedPrice = Calprice + OriginalFreightCharge-coupanAmount,
                                   PickUpAddress = p.PickupAddress,
                                   PickUpName = p.PickupName                      

                               }).ToList();


                    return res;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
