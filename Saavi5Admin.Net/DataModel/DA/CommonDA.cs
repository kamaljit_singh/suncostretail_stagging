﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DA
{
    public class CommonDA
    {
        private readonly SaaviAdminEntities _db;
        public CommonDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region
        /// <summary>
        /// Get Country list
        /// </summary>
        /// <returns></returns>
        public List<CountryMaster> GetAllCountry()
        {
            try
            {
                return _db.CountryMasters.Where(x => x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region
        /// <summary>
        /// Get State list by Country ID
        /// </summary>
        /// <param name="cID"></param>
        /// <returns></returns>
        public List<StateMaster> GetAllStateByCountryID(int cID)
        {
            try
            {
                if (cID > 0)
                {
                    return _db.StateMasters.Where(x => x.IsActive == true && x.CountryID == cID).ToList();
                }
                else
                {
                    return _db.StateMasters.Where(x => x.IsActive == true).ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
