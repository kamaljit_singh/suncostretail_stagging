﻿using AutoMapper;
using CommonFunctions;
using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataModel.DA
{
    public class ProductDA
    {
        private readonly SaaviAdminEntities _db;

        public ProductDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region Get all categories with subcategories

        /// <summary>
        /// Get all categories with subcategories
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<AllCategoryBO> GetAllCatogories(string SearchText)
        {
            try
            {
                return (from cat in _db.MainCategoriesMasters
                        where cat.IsActive == true &&
                       (SearchText == "" ? true : cat.MCategoryName.ToLower().Contains(SearchText.ToLower())
                       || SearchText == "" || SearchText == null)
                        select new AllCategoryBO
                        {
                            CategoryID = cat.MCategoryId,
                            CategoryName = cat.MCategoryName,
                            SubCategoryList = _db.ProductCategoriesMasters.Where(x => x.MCategoryId == cat.MCategoryId && x.IsActive == true).OrderBy(o => o.CategoryCode).ToList()
                        }).OrderByDescending(c => c.CategoryID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get all categories with subcategories

        #region get all products by category id

        /// <summary>
        /// get all products by category id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GetAllProductBO> GetAllProductsByCatID(GetProductsModel model)
        {
            try
            {
                if (model.CategoryID > 0)
                {
                    return (from a in _db.ProductMasters
                            where a.CategoryID == model.CategoryID &&
                           (model.SearchText == "" ? true : a.ProductName.ToLower().Contains(model.SearchText.ToLower())
                           || model.SearchText == "" || model.SearchText == null)
                            select new GetAllProductBO
                            {
                                ProductID = a.ProductID,
                                ProductName = a.ProductName
                            }
                            ).OrderBy(x => x.ProductName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
                }
                else
                {
                    return (from a in _db.ProductMasters
                            where
                           (model.SearchText == "" ? true : a.ProductName.ToLower().Contains(model.SearchText.ToLower())
                           || model.SearchText == "" || model.SearchText == null)
                            select new GetAllProductBO
                            {
                                ProductID = a.ProductID,
                                ProductName = a.ProductName
                            }
                        ).OrderBy(x => x.ProductName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get all products by category id

        #region get product by id

        /// <summary>
        /// get product by id
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        public GetAllProductBO GetProductByID(long productID)
        {
            try
            {
                _db.Configuration.ProxyCreationEnabled = false;
                return (from a in _db.ProductMasters
                        join b in _db.ProductCategoriesMasters on a.CategoryID equals b.CategoryID
                        join c in _db.ProductFiltersMasters on a.FilterID equals c.FilterID into temp
                        from c in temp.DefaultIfEmpty()
                        join d in _db.UnitOfMeasurementMasters on a.UOMID equals d.UOMID into temp2
                        from d in temp2.DefaultIfEmpty()
                        where a.ProductID == productID
                        select new GetAllProductBO
                        {
                            ProductID = a.ProductID,
                            CategoryID = a.CategoryID,
                            CategoryName = b.CategoryName,
                            ProductName = a.ProductName,
                            ProductDescription = a.ProductDescription,
                            ProductCode = a.ProductCode,
                            Price = a.Price,
                            ComPrice = a.ComPrice,
                            ProductImage = a.ProductImage,
                            ThumbNail = a.ThumbNail,
                            IsActive = a.IsActive,
                            IsAvailable = (a.IsAvailable == null || a.IsAvailable == true) ? true : false,
                            PiecesAndWeight = (a.PiecesAndWeight == null) ? false : a.PiecesAndWeight,
                            CreatedDate = a.CreatedDate,
                            ModifiedDate = a.ModifiedDate,
                            PFilterID = a.FilterID,
                            FilterName = c.FilterName,
                            UnitName = d.UOMDesc,
                            UnitID = a.UOMID,
                            GST = a.GST ?? false,
                            ImageList = _db.ProductImageMasters.Where(x => x.ProductID == productID).ToList(),
                            ProductFeature = a.ProductFeature,
                            StockOnHand = a.StockOnHand ?? 0
                        }).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get product by id

        #region Get main category by id

        /// <summary>
        /// Get main category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MainCategoriesMaster getMainCategoryByID(long id)
        {
            try
            {
                return _db.MainCategoriesMasters.Where(x => x.MCategoryId == id).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get main category by id

        #region Add/update main category

        /// <summary>
        /// Add/update main category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageCategory(MainCategoriesMaster model)
        {
            try
            {
                MainCategoriesMaster obj = new MainCategoriesMaster();
                var cat = _db.MainCategoriesMasters.Where(x => x.MCategoryName.ToLower() == model.MCategoryName.ToLower() && x.MCategoryId != model.MCategoryId).FirstOrDefault();
                if (cat == null)
                {
                    if (model.MCategoryId > 0)
                    {
                        cat = _db.MainCategoriesMasters.Where(x => x.MCategoryId == model.MCategoryId).FirstOrDefault();
                        if (cat != null)
                        {
                            cat.ModifiedDate = DateTime.Now;
                            cat.MCategoryName = model.MCategoryName;
                        }
                        else { return "No category available!"; }
                    }
                    else
                    {
                        model.IsActive = true;
                        model.CreatedDate = DateTime.Now;
                        _db.MainCategoriesMasters.Add(model);
                    }
                }
                else { return "Category name already exist!"; }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Add/update main category

        #region delete main category by id

        /// <summary>
        /// delete main category by id
        /// </summary>
        /// <param name="catID"></param>
        /// <returns></returns>
        public string DeleteCategory(long catID)
        {
            try
            {
                var cat = _db.MainCategoriesMasters.Where(x => x.MCategoryId == catID).FirstOrDefault();
                if (cat != null)
                {
                    var product = (from b in _db.ProductCategoriesMasters
                                   join c in _db.ProductMasters on b.CategoryID equals c.CategoryID
                                   where b.MCategoryId == catID
                                   select new { b }).ToList();
                    if (product.Count() == 0)
                    {
                        _db.MainCategoriesMasters.Remove(cat);
                        _db.SaveChanges();
                    }
                    else { return "There are products avaliable under this category, so it can not be deleted."; }
                }
                else { return "No category available!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion delete main category by id

        #region get sub-category by id

        /// <summary>
        /// get sub-category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProductCategoriesMaster getSubCategoryByID(long id)
        {
            try
            {
                return _db.ProductCategoriesMasters.Where(x => x.CategoryID == id).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get sub-category by id

        #region add/update sub-category

        /// <summary>
        /// add/update sub-category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageSubCategory(ProductCategoriesMaster model)
        {
            try
            {
                ProductCategoriesMaster obj = new ProductCategoriesMaster();
                obj = _db.ProductCategoriesMasters.Where(x => x.CategoryName == model.CategoryName && x.CategoryID != model.CategoryID && x.MCategoryId == model.MCategoryId).FirstOrDefault();
                if (obj == null)
                {
                    if (model.CategoryID > 0)
                    {
                        obj = _db.ProductCategoriesMasters.Where(x => x.CategoryID == model.CategoryID).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.ModifiedDate = DateTime.Now;
                            obj.CategoryName = model.CategoryName;
                            obj.IsSpecialCategory = model.IsSpecialCategory;
                            obj.CategoryCode = model.CategoryCode;
                        }
                        else { return "No sub-category available!"; }
                    }
                    else
                    {
                        model.IsActive = true;
                        model.CreatedDate = DateTime.Now;
                        _db.ProductCategoriesMasters.Add(model);
                    }
                }
                else { return "Sub-category name already exist!"; }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion add/update sub-category

        #region delete sub-category

        /// <summary>
        /// delete sub-category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteSubCategory(long id)
        {
            try
            {
                var data = _db.ProductCategoriesMasters.Where(x => x.CategoryID == id).FirstOrDefault();
                if (data != null)
                {
                    var product = _db.ProductMasters.Where(x => x.CategoryID == id).FirstOrDefault();
                    if (product == null)
                    {
                        _db.ProductCategoriesMasters.Remove(data);
                        _db.SaveChanges();
                    }
                    else { return "There are products avaliable under this sub-category, so it can not be deleted."; }
                }
                else { return "No sub-category available!"; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion delete sub-category

        #region Get all sub-category

        /// <summary>
        /// Get all sub-category
        /// </summary>
        /// <returns></returns>
        public List<ProductCategoriesMaster> getAllSubCategory()
        {
            try
            {
                _db.Configuration.ProxyCreationEnabled = false;
                return _db.ProductCategoriesMasters.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get all sub-category

        #region Get all unit of measurements

        /// <summary>
        /// Get all unit of measurements
        /// </summary>
        /// <returns></returns>
        public List<UnitOfMeasurementMaster> getAllUnitofMeasurement()
        {
            try
            {
                _db.Configuration.ProxyCreationEnabled = false;
                return _db.UnitOfMeasurementMasters.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get all unit of measurements

        #region Get all Product filter

        /// <summary>
        /// Get all Product filter
        /// </summary>
        /// <returns></returns>
        public List<ProductFiltersMaster> getAllProductFilter()
        {
            try
            {
                _db.Configuration.ProxyCreationEnabled = false;
                return _db.ProductFiltersMasters.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get all Product filter

        #region add/update products

        /// <summary>
        /// add/update products
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string AddUpdateProduct(GetAllProductBO model)
        {
            try
            {
                ProductMaster obj = new ProductMaster();
                var data = _db.ProductMasters.Where(x => x.ProductCode == model.ProductCode && x.ProductID != model.ProductID).FirstOrDefault();
                if (data == null)
                {
                    if (model.ProductID > 0)
                    {
                        obj = _db.ProductMasters.Where(x => x.ProductID == model.ProductID).FirstOrDefault();
                        if (obj != null)
                        {
                            model.IsActive = model.IsAvailable;
                            model.CreatedDate = obj.CreatedDate;
                            model.ModifiedDate = DateTime.Now;
                            obj.UOMID = model.UnitID;
                            obj.FilterID = model.PFilterID;
                            Mapper.Map(model, obj);
                            _db.SaveChanges();

                            var altOBj = _db.AltUOMMasters.Where(x => x.ProductID == model.ProductID).FirstOrDefault();
                            if (altOBj != null)
                            {
                                altOBj.UOMID = model.UnitID;
                                _db.SaveChanges();
                            }
                            else
                            {
                                altOBj = new AltUOMMaster();
                                altOBj.ProductID = model.ProductID;
                                altOBj.UOMID = model.UnitID;
                                altOBj.QtyPerUnitOfMeasure = 1;
                                _db.AltUOMMasters.Add(altOBj);
                                _db.SaveChanges();
                            }

                            _db.Database.ExecuteSqlCommand("update dbo.Stockwarehouse set stockonhand = " + model.StockOnHand + " where productid =" + model.ProductID);

                            foreach (FileList item in model.FileList)
                            {
                                var img = _db.ProductImageMasters.Where(x => x.ProductID == model.ProductID && x.FileID == item.FileID).FirstOrDefault();
                                if (img != null)
                                {
                                    if (item.File.FileName != null && item.File.FileName != "")
                                    {
                                        item.File.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Uploads/Products/"), img.ProductImage));
                                        //save thumb image in folder
                                        Save(HttpContext.Current.Server.MapPath("~/Content/Uploads/Products/") + img.ProductImage, 232, 242, 85, HttpContext.Current.Server.MapPath("~/Content/Uploads/Products/") + "thumb_" + img.ProductImage, "thumb");
                                    }
                                }
                                else
                                {
                                    item.ProductID = obj.ProductID;
                                    SaveImage(item);
                                }
                            }
                        }
                        else { return "No product available!"; }
                    }
                    else
                    {
                        model.CreatedDate = DateTime.Now;
                        model.IsActive = true;
                        obj.UOMID = model.UnitID;
                        obj.FilterID = model.PFilterID;
                        Mapper.Map(model, obj);
                        _db.ProductMasters.Add(obj);
                        _db.SaveChanges();

                        AltUOMMaster altOBj = new AltUOMMaster();
                        altOBj.ProductID = obj.ProductID;
                        altOBj.UOMID = obj.UOMID;
                        altOBj.QtyPerUnitOfMeasure = 1;
                        _db.AltUOMMasters.Add(altOBj);
                        _db.SaveChanges();

                        foreach (FileList item in model.FileList)
                        {
                            item.ProductID = obj.ProductID;
                            SaveImage(item);
                        }
                    }
                }
                else
                { return "This product code is already taken. Please try another."; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SaveImage(FileList li)
        {
            if (li.File.FileName != null && li.File.FileName != "")
            {
                var filename = Guid.NewGuid() + Path.GetExtension(li.File.FileName);
                li.File.SaveAs(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Uploads/Products/"), filename));
                //save thumb image in folder
                Save(HttpContext.Current.Server.MapPath("~/Content/Uploads/Products/") + filename, 232, 242, 85, HttpContext.Current.Server.MapPath("~/Content/Uploads/Products/") + "thumb_" + filename, "thumb");

                ProductImageMaster obj = new ProductImageMaster();
                obj.ProductID = li.ProductID;
                obj.IsActive = true;
                obj.BaseUrl = ConfigurationManager.AppSettings["SITE_URL"] + "Content/Uploads/Products/";
                obj.FileID = li.FileID;
                obj.ProductImage = filename;
                _db.ProductImageMasters.Add(obj);
                _db.SaveChanges();
            }
            return "Success";
        }

        #endregion add/update products

        #region Save image with resize it

        /// <summary>
        /// Save image with resize it
        /// </summary>
        /// <param name="actualImagePath"></param>
        /// <param name="maxWidth"></param>
        /// <param name="maxHeight"></param>
        /// <param name="quality"></param>
        /// <param name="fileSavePath"></param>
        /// <param name="type"></param>
        public static void Save(string actualImagePath, int maxWidth, int maxHeight, int quality, string fileSavePath, string type)
        {
            try
            {
                int newWidth = 0;
                int newHeight = 0;
                float ratioX = 0;
                float ratioY = 0;
                float ratio = 0;

                Bitmap image = new Bitmap(actualImagePath);
                // Get the image's original width and height
                int originalWidth = image.Width;
                int originalHeight = image.Height;

                if (type == "main")
                {
                    if (originalWidth < maxWidth && originalHeight < maxHeight)
                    {
                        newWidth = image.Width;
                        newHeight = image.Height;
                    }
                    else
                    {
                        // To preserve the aspect ratio
                        ratioX = (float)maxWidth / (float)originalWidth;
                        ratioY = (float)maxHeight / (float)originalHeight;
                        ratio = Math.Min(ratioX, ratioY);

                        // New width and height based on aspect ratio
                        newWidth = (int)(originalWidth * ratio);
                        newHeight = (int)(originalHeight * ratio);
                    }
                }
                else
                {
                    if (originalWidth > maxWidth && originalHeight > maxHeight)
                    {
                        // To preserve the aspect ratio
                        ratioX = (float)maxWidth / (float)originalWidth;
                        ratioY = (float)maxHeight / (float)originalHeight;
                        ratio = Math.Min(ratioX, ratioY);

                        // New width and height based on aspect ratio
                        newWidth = (int)(originalWidth * ratio);
                        newHeight = (int)(originalHeight * ratio);
                    }
                    else
                    {
                        newWidth = originalWidth;
                        newHeight = originalHeight;
                    }
                }

                // Convert other formats (including CMYK) to RGB.
                Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

                // Draws the image in the specified size with quality mode set to HighQuality
                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.DrawImage(image, 0, 0, newWidth, newHeight);
                }

                // Get an ImageCodecInfo object that represents the JPEG codec.
                ImageCodecInfo imageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg);

                // Create an Encoder object for the Quality parameter.
                System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;

                // Create an EncoderParameters object.
                EncoderParameters encoderParameters = new EncoderParameters(1);

                // Save the image as a JPEG file with quality level.
                EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
                encoderParameters.Param[0] = encoderParameter;

                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(fileSavePath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        newImage.Save(memory, imageCodecInfo, encoderParameters);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }
                newImage.Dispose();
                image.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }

        //public void delExistFile(string img, string path)
        //{
        //    if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(path + "/" + img)))
        //    {
        //        System.IO.File.Delete(HttpContext.Current.Server.MapPath(path + "/" + img));
        //    }
        //}

        #endregion Save image with resize it

        #region delete product

        /// <summary>
        /// delete product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteProduct(long id)
        {
            try
            {
                var data = _db.ProductMasters.Where(x => x.ProductID == id).FirstOrDefault();
                if (data != null)
                {
                    var img = _db.ProductImageMasters.Where(x => x.ProductID == id).ToList();
                    if (img.Count > 0)
                    {
                        _db.ProductImageMasters.RemoveRange(img);
                        _db.SaveChanges();
                    }
                    var item = _db.trnsCartItemsMasters.Where(x => x.ProductID == id).ToList();
                    if (item.Count > 0)
                    {
                        _db.trnsCartItemsMasters.RemoveRange(item);
                        _db.SaveChanges();
                    }
                    var altUOM = _db.AltUOMMasters.Where(x => x.ProductID == id).FirstOrDefault();
                    if (altUOM != null)
                    {
                        _db.AltUOMMasters.Remove(altUOM);
                        _db.SaveChanges();
                    }
                    _db.ProductMasters.Remove(data);
                    _db.SaveChanges();
                    return "Success";
                }
                else
                { return "No product available!"; }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion delete product

        #region Add/update product filter

        /// <summary>
        /// Add/update product filter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageProductFilter(ProductFiltersMaster model)
        {
            try
            {
                var data = _db.ProductFiltersMasters.Where(x => x.FilterName.ToLower() == model.FilterName.ToLower() && x.FilterID != model.FilterID).FirstOrDefault();
                if (data == null)
                {
                    if (model.FilterID > 0)
                    {
                        data = _db.ProductFiltersMasters.Where(x => x.FilterID == model.FilterID).FirstOrDefault();
                        if (data != null)
                        {
                            data.ModifiedDate = DateTime.Now;
                            data.FilterName = model.FilterName;
                        }
                        else
                        { return "No filter available!"; }
                    }
                    else
                    {
                        model.CreatedDate = DateTime.Now;
                        model.CompanyID = 1;
                        model.IsActive = true;
                        _db.ProductFiltersMasters.Add(model);
                    }
                }
                else { return "Filter name already exist!"; }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Add/update product filter

        #region Delete product filter

        /// <summary>
        /// Delete product filter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteProductFilter(long id)
        {
            try
            {
                var data = _db.ProductMasters.Where(x => x.FilterID == id).FirstOrDefault();
                if (data == null)
                {
                    var filter = _db.ProductFiltersMasters.Where(x => x.FilterID == id).FirstOrDefault();
                    if (filter != null)
                    {
                        _db.ProductFiltersMasters.Remove(filter);
                        _db.SaveChanges();
                    }
                    else { return "No product filter available!"; }
                }
                else { return "Unable to delete. This filter is used in some products."; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Delete product filter

        #region Add/update unit of measurements

        /// <summary>
        /// Add/update unit of measurements
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageUnitofMeasurements(UnitOfMeasurementMaster model)
        {
            try
            {
                var data = _db.UnitOfMeasurementMasters.Where(x => x.UOMDesc.ToLower() == model.UOMDesc.ToLower() && x.UOMID != model.UOMID).FirstOrDefault();
                if (data == null)
                {
                    if (model.UOMID > 0)
                    {
                        data = _db.UnitOfMeasurementMasters.Where(x => x.UOMID == model.UOMID).FirstOrDefault();
                        if (data != null)
                        {
                            data.ModifiedDate = DateTime.Now;
                            data.UOMDesc = model.UOMDesc;
                            data.Code = model.UOMDesc;
                        }
                        else { return "No unit available!"; }
                    }
                    else
                    {
                        model.CreatedDate = DateTime.Now;
                        model.IsActive = true;
                        model.CompanyID = 1;
                        model.Code = model.UOMDesc;
                        _db.UnitOfMeasurementMasters.Add(model);
                    }
                }
                else { return "Unit name already exist!"; }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Add/update unit of measurements

        #region delete unit of measurement

        /// <summary>
        /// delete unit of measurement
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteUnitofMeasurement(long id)
        {
            try
            {
                var data = _db.ProductMasters.Where(x => x.UOMID == id).FirstOrDefault();
                if (data == null)
                {
                    var unit = _db.UnitOfMeasurementMasters.Where(x => x.UOMID == id).FirstOrDefault();
                    if (unit != null)
                    {
                        _db.UnitOfMeasurementMasters.Remove(unit);
                        _db.SaveChanges();
                    }
                    else { return "No unit available!"; }
                }
                else { return "Unable to delete. This unit is used in some products."; }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion delete unit of measurement

        #region get all non suggestive products

        /// <summary>
        /// get all non suggestive products
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GetAllProductBO> GetAllNonSuggestiveProducts(GetNonProductsModel model)
        {
            try
            {
                return (from a in _db.ProductMasters
                        join b in _db.SuggestiveItemsMasters on a.ProductID equals b.ProductID into temp
                        from b in temp.DefaultIfEmpty()
                        where b.ProductID == null && a.IsActive == true &&
                       (model.SearchText == "" ? true : a.ProductName.ToLower().Contains(model.SearchText.ToLower())
                       || model.SearchText == "" || model.SearchText == null)
                        select new GetAllProductBO
                        {
                            ProductID = a.ProductID,
                            ProductName = a.ProductName + (a.ProductCode == null ? "" : " (" + a.ProductCode + ")")
                        }
                    ).OrderBy(x => x.ProductName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get all non suggestive products

        #region get all non latest products

        /// <summary>
        /// get all non latest products
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GetAllProductBO> GetAllNonLatestProducts(GetNonProductsModel model)
        {
            try
            {
                return (from a in _db.ProductMasters
                        join b in _db.LatestSpecialsMasters on a.ProductID equals b.ProductID into temp
                        from b in temp.DefaultIfEmpty()
                        where b.ProductID == null && a.IsActive == true &&
                       (model.SearchText == "" ? true : a.ProductName.ToLower().Contains(model.SearchText.ToLower())
                       || model.SearchText == "" || model.SearchText == null)
                        select new GetAllProductBO
                        {
                            ProductID = a.ProductID,
                            ProductName = a.ProductName + (a.ProductCode == null ? "" : " (" + a.ProductCode + ")")
                        }
                    ).OrderBy(x => x.ProductName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get all non latest products

        #region get all suggestive products

        /// <summary>
        /// get all suggestive products
        /// </summary>
        /// <returns></returns>
        public List<GetAllProductBO> GetAllSuggestiveProducts()
        {
            try
            {
                return (from a in _db.SuggestiveItemsMasters
                        join b in _db.ProductMasters on a.ProductID equals b.ProductID
                        where a.IsActive == true && b.IsActive == true
                        select new GetAllProductBO
                        {
                            ProductID = a.ProductID,
                            ProductName = b.ProductName + (b.ProductCode == null ? "" : " (" + b.ProductCode + ")")
                        }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get all suggestive products

        #region add/remove suggestive products

        /// <summary>
        /// add/remove suggestive products
        /// </summary>
        /// <param name="productIDs"></param>
        /// <returns></returns>
        public string ManageSuggestiveProduct(string productIDs)
        {
            try
            {
                string[] productArray = productIDs.Split(',');
                if (productArray.Length > 0 && productIDs != "")
                {
                    var data = _db.SuggestiveItemsMasters.Where(x => !productArray.Contains(x.ProductID.ToString())).ToList();
                    if (data.Count > 0)
                    {
                        _db.SuggestiveItemsMasters.RemoveRange(data);
                        _db.SaveChanges();
                    }
                    foreach (string productId in productArray)
                    {
                        Int64 pID = Convert.ToInt64(productId);
                        var product = _db.SuggestiveItemsMasters.Where(x => x.ProductID == pID).FirstOrDefault();
                        if (product != null)
                        {
                            product.ModifiedDate = DateTime.Now;
                            //product.ProductID = Convert.ToInt64(productId);
                            //product.IsActive = true;
                        }
                        else
                        {
                            SuggestiveItemsMaster obj = new SuggestiveItemsMaster();
                            obj.ProductID = Convert.ToInt64(productId);
                            obj.CreatedDate = DateTime.Now;
                            obj.IsActive = true;
                            _db.SuggestiveItemsMasters.Add(obj);
                        }
                        _db.SaveChanges();
                    }
                }
                else
                {
                    var data = _db.SuggestiveItemsMasters.ToList();
                    if (data != null)
                    {
                        _db.SuggestiveItemsMasters.RemoveRange(data);
                        _db.SaveChanges();
                    }
                }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion add/remove suggestive products

        #region get all latest products

        /// <summary>
        /// get all latest products
        /// </summary>
        /// <returns></returns>
        public List<GetAllProductBO> GetAllLatestProducts()
        {
            try
            {
                return (from a in _db.LatestSpecialsMasters
                        join b in _db.ProductMasters on a.ProductID equals b.ProductID
                        where a.IsActive == true && b.IsActive == true
                        select new GetAllProductBO
                        {
                            ProductID = a.ProductID,
                            ProductName = b.ProductName + (b.ProductCode == null ? "" : " (" + b.ProductCode + ")")
                        }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion get all latest products

        #region add/remove latest products

        /// <summary>
        /// add/remove latest products
        /// </summary>
        /// <param name="productIDs"></param>
        /// <returns></returns>
        public string ManageLatestProduct(string productIDs)
        {
            try
            {
                string[] productArray = productIDs.Split(',');
                if (productArray.Length > 0 && productIDs != "")
                {
                    var data = _db.LatestSpecialsMasters.Where(x => !productArray.Contains(x.ProductID.ToString())).ToList();
                    if (data.Count > 0)
                    {
                        _db.LatestSpecialsMasters.RemoveRange(data);
                        _db.SaveChanges();
                    }
                    foreach (string productId in productArray)
                    {
                        Int64 pID = Convert.ToInt64(productId);
                        var product = _db.LatestSpecialsMasters.Where(x => x.ProductID == pID).FirstOrDefault();
                        if (product != null)
                        {
                            product.ModifiedDate = DateTime.Now;
                            //product.ProductID = Convert.ToInt64(productId);
                            //product.IsActive = true;
                        }
                        else
                        {
                            LatestSpecialsMaster obj = new LatestSpecialsMaster();
                            obj.ProductID = Convert.ToInt64(productId);
                            obj.CreatedDate = DateTime.Now;
                            obj.IsActive = true;
                            _db.LatestSpecialsMasters.Add(obj);
                        }
                        _db.SaveChanges();
                    }
                }
                else
                {
                    var data = _db.LatestSpecialsMasters.ToList();
                    if (data != null)
                    {
                        _db.LatestSpecialsMasters.RemoveRange(data);
                        _db.SaveChanges();
                    }
                }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion add/remove latest products

        #region Get latest added product

        /// <summary>
        /// Get latest added product
        /// </summary>
        /// <returns></returns>
        public List<GetAllProductBO> GetLatestAddedProduct(string id)
        {
            try
            {
                DateTime dt = DateTime.Now.AddDays(-10);
                return (from a in _db.ProductMasters
                        where a.IsActive == true && a.CreatedDate <= DateTime.Now && a.CreatedDate >= dt &&
                        (id == "" ? true : a.ProductName.ToLower().Contains(id.ToLower()) || id == "" || id == null)
                        select new GetAllProductBO
                        {
                            ProductID = a.ProductID,
                            ProductName = a.ProductName + (a.ProductCode == null ? "" : " (" + a.ProductCode + ")")
                        }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Get latest added product

        #region

        /// <summary>
        /// Change Product status
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public string SetProductActive(long id, bool status)
        {
            try
            {
                var product = _db.ProductMasters.Where(x => x.ProductID == id).FirstOrDefault();
                if (product != null)
                {
                    product.IsActive = status;
                    _db.SaveChanges();
                    return "Success";
                }
                else
                {
                    return "No product available!";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        //delete images from product
        #region delete product images

        /// <summary>
        /// delete product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteProductImage(long id)
        {
            try
            {
                var data = _db.ProductImageMasters.Where(x => x.ID == id).FirstOrDefault();
                if (data != null)
                {
                    //var img = _db.ProductImageMasters.Where(x => x.ID == id).ToList();
                    //if (img.Count > 0)
                    //{
                    //    _db.ProductImageMasters.RemoveRange(img);
                    //    _db.SaveChanges();
                    //}
                    _db.ProductImageMasters.Remove(data);
                    _db.SaveChanges();
                    return "Success";
                }
                else
                { return "No product available!"; }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion
    }
}