﻿using CommonFunctions;
using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DA
{
    public class MessageDA
    {
        private readonly SaaviAdminEntities _db;
        public MessageDA()
        {
            _db = new SaaviAdminEntities();
        }

        #region 
        /// <summary>
        /// Get all customer from the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<AllCustomerBO> GetAllCustomers(GetCustomersModel model)
        {
            try
            {
                if (model.PageSize > 0)
                {
                    return (from cust in _db.CustomerMasters
                            where cust.IsRep == model.IsRep &&
                            (model.SearchText == "" ? true : cust.CustomerName.ToLower().Contains(model.SearchText.ToLower())
                             || cust.ContactName.ToLower().Contains(model.SearchText.ToLower())
                             || cust.Address1.ToLower().Contains(model.SearchText.ToLower())
                             || model.SearchText == "" || model.SearchText == null)
                            select new AllCustomerBO
                            {
                                CustomerID = cust.CustomerID,
                                CustomerName = cust.CustomerName
                            }).OrderBy(c => c.CustomerName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
                }

                else
                {
                    var groupCustomers = GetGroupCustomers(model.GroupID, "c");
                    var results = (from cust in _db.CustomerMasters
                                   where cust.IsRep == model.IsRep
                                   select new AllCustomerBO
                                   {
                                       CustomerID = cust.CustomerID,
                                       CustomerName = cust.CustomerName
                                   }).OrderBy(c => c.CustomerName).ToList();
                    for (int index = 0; index < groupCustomers.Count; index++)
                    {
                        results.RemoveAll(s => s.CustomerID == groupCustomers[index].CustomerID);
                    }
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Group
        /// <summary>
        /// Get Group 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<GetGroupCustomers> GetGroup(long id)
        {
            try
            {
                if (id > 0)
                {
                    return (from a in _db.GroupsMasters
                            where a.GroupID == id
                            select new GetGroupCustomers
                            {
                                GroupID = a.GroupID,
                                GroupName = a.GroupName
                            }).ToList();
                }
                else
                {
                    return (from a in _db.GroupsMasters
                            select new GetGroupCustomers
                            {
                                GroupID = a.GroupID,
                                GroupName = a.GroupName
                            }).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region get group customers
        /// <summary>
        /// get group customers
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public List<AllCustomerBO> GetGroupCustomers(Int64 groupID, string type)
        {
            try
            {
                if (type == "R")
                {
                    return (from a in _db.UserGroupsMasters
                            join b in _db.CutoffGroupMasters on a.CustomerID equals b.GroupID
                            where a.GroupID == groupID && a.Type == type
                            select new AllCustomerBO
                            {
                                CustomerID = b.GroupID,
                                CustomerName = b.GroupName
                            }).ToList();
                }
                else if (type == "Uniq")
                {
                    return (from a in _db.UserGroupsMasters
                            join b in _db.CutoffGroupMasters on a.CustomerID equals b.GroupID
                            where a.Type == "R"
                            select new AllCustomerBO
                            {
                                CustomerID = b.GroupID,
                                CustomerName = b.GroupName
                            }).ToList();
                }
                else
                {
                    return (from a in _db.UserGroupsMasters
                            join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID
                            where a.GroupID == groupID && a.Type == type
                            select new AllCustomerBO
                            {
                                CustomerID = b.CustomerID,
                                CustomerName = b.CustomerName
                            }).ToList();
                }



            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        #region manage groups
        /// <summary>
        /// manage groups
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageGroups(GetGroupCustomers model)
        {
            try
            {

                if (model.GroupID > 0)
                {
                    var data = _db.GroupsMasters.Where(x => x.GroupID == model.GroupID).FirstOrDefault();
                    if (data != null)
                    {
                        data.ModifiedDate = DateTime.Now;
                        data.GroupName = model.GroupName;
                        _db.SaveChanges();
                    }
                    else { return "No group available!"; }
                }
                else
                {
                    GroupsMaster obj = new GroupsMaster();
                    obj.GroupName = model.GroupName;
                    obj.IsActive = true;
                    obj.CreatedDate = DateTime.Now;
                    _db.GroupsMasters.Add(obj);
                    _db.SaveChanges();
                    model.GroupID = obj.GroupID;
                }

                //manage group customers
                ManageGroupCustomer(model.GroupID, model.GroupCustomers, model.Type);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region 
        /// <summary>
        /// manage group customers
        /// </summary>
        /// <param name="groupID"></param>
        /// <param name="customerIDs"></param>
        /// <returns></returns>
        public string ManageGroupCustomer(long groupID, string customerIDs, string type)
        {
            try
            {
                AddUpdateGroupCustomerandOther(groupID, customerIDs,type);

                if (type == "R")
                {
                  
                    var groups = (from a in _db.CutoffGroupMasters
                                  join b in _db.CutoffGroupDetailMasters on a.GroupID equals b.GroupID
                                  where customerIDs.Contains(a.GroupID.ToString())
                                  select b).ToList();
                    if (groups != null && groups.Count > 0)
                    {
                        customerIDs = "";
                        string[] tempSubrub = null;
                        foreach (var gp in groups)
                        {
                            tempSubrub = gp.ListValue.Split(',');

                            var tempCustomer = _db.CustomerMasters.Where(x => x.IsActive == true && tempSubrub.Contains(x.Suburb)).ToList();

                            foreach (var cs in tempCustomer)
                            {

                                customerIDs += "," + cs.CustomerID;

                            }                          
                        }

                        customerIDs = customerIDs.TrimStart(',');

                        AddUpdateGroupCustomerandOther(groupID, customerIDs.TrimStart(','), "c");
                    }
                }

                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 
        /// <summary>
        /// delete group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteGroup(long id)
        {
            try
            {
                var data = _db.GroupsMasters.Where(x => x.GroupID == id).FirstOrDefault();
                if (data != null)
                {
                    var record = _db.UserGroupsMasters.Where(x => x.GroupID == data.GroupID).ToList();
                    if (record.Count > 0)
                    {
                        _db.UserGroupsMasters.RemoveRange(record);
                        _db.SaveChanges();
                    }

                    _db.GroupsMasters.Remove(data);
                    _db.SaveChanges();
                }
                else { return "No group available!"; }
                return "Success";
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region get group's customers
        /// <summary>
        /// get group's customers
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public List<GetCompanyGroupCustomer> GetCompanyGroupCustomersAll(long groupID)
        {
            try
            {
                var data = _db.GroupsMasters.Where(x => x.GroupID == groupID).FirstOrDefault();
                var cust = new List<GetCompanyGroupCustomer>();
                if (data != null)
                {
                    cust = (from a in _db.GroupsMasters
                            join b in _db.UserGroupsMasters on a.GroupID equals b.GroupID
                            join c in _db.CustomerMasters on b.CustomerID equals c.CustomerID
                            join d in _db.UserMasters on b.CustomerID equals d.CustomerID
                            where (a.GroupID == groupID)
                            select new GetCompanyGroupCustomer
                            {
                                GroupID = a.GroupID,
                                GroupName = a.GroupName,
                                CustomerId = c.CustomerID,
                                ID = d.UserID,
                                CustomerName = c.CustomerName,
                                DeviceToken = d.DeviceToken,
                                DeviceType = d.DeviceType
                            }).ToList();
                }
                return cust;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region get customer/s for notification
        /// <summary>
        /// get group's customers
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public List<GetCompanyGroupCustomer> GetCompanyCustomersForNotification(long customerID)
        {
            try
            {
                List<GetCompanyGroupCustomer> cust = (from c in _db.CustomerMasters
                                                      join u in _db.UserMasters on c.CustomerID equals u.CustomerID
                                                      select new GetCompanyGroupCustomer
                                                      {
                                                          GroupID = 0,
                                                          GroupName = "",
                                                          CustomerId = c.CustomerID,
                                                          ID = u.UserID,
                                                          CustomerName = c.CustomerName,
                                                          DeviceToken = u.DeviceToken,
                                                          DeviceType = u.DeviceType
                                                      }).ToList();
                if (customerID > 0)
                {
                    return cust.Where(i => i.CustomerId == customerID).ToList();
                }
                return cust;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 
        /// <summary>
        /// get notification badge by customer
        /// </summary>
        /// <param name="custID"></param>
        /// <returns></returns>
        public List<NotificationsBadgeMaster> getBadgeByCustomer(long custID)
        {
            return (from a in _db.NotificationsBadgeMasters
                    where a.UserId == custID
                    select new NotificationsBadgeMaster
                    {
                        ID = a.ID,
                        UserId = a.UserId,
                        NotificationType = a.NotificationType,
                        Badge = a.Badge
                    }).ToList();
        }
        #endregion

        #region 
        /// <summary>
        /// manage notification badge b y customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManageNotificationBadge(NotificationsBadgeMaster model)
        {
            try
            {
                if (model.ID > 0)
                {
                    var data = _db.NotificationsBadgeMasters.Where(x => x.ID == model.ID).FirstOrDefault();
                    if (data != null)
                    {
                        data.ModifiedDate = DateTime.Now;
                        data.Badge = model.Badge;
                        data.NotificationType = model.NotificationType;
                    }
                    else { return "No bodge available!"; }
                }
                else
                {
                    model.CreatedDate = DateTime.Now;
                    _db.NotificationsBadgeMasters.Add(model);
                }
                _db.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 

        public string SendMessage(GetMessageModel model)
        {
            try
            {
                string strString = "";
                string[] validUserTypes = { "A", "S", "G" };
                if (Array.Exists(validUserTypes, el => el == model.UserType))
                {
                    //  CompanyBLL obj = new CompanyBLL();
                    // List<CompanyBLL> groupData = new List<CompanyBLL>();
                    var groupData = new List<GetCompanyGroupCustomer>();
                    if (model.UserType == "A")
                    {
                        groupData = GetCompanyCustomersForNotification(0);
                    }
                    else if (model.UserType == "S")
                    {
                        groupData = GetCompanyCustomersForNotification(model.ID);
                    }
                    else
                    {
                        if (model.Type == "d")
                        {
                            groupData = GetCompanyGroupdriversAll(model.ID);
                        }
                        else
                        {
                            groupData = GetCompanyGroupCustomersAll(model.ID);
                        }
                    }


                    var totalCustomers = groupData.Count();
                    if (totalCustomers > 0)
                    {
                        var sentCount = 0;
                        //  string AndroidserverKey = ConfigurationManager.AppSettings["CustAndroidserverKey"];
                        foreach (var item in groupData)
                        {
                            // string Api = ConfigurationManager.AppSettings["GOOGLEAPI"].ToString();//"AIzaSyDbLc4XTgqSqV-5CcvLgaEPWTIY0UHjMEo";
                            string tickerText = "FT";
                            string contentTitle = "FT";
                            string postData = "";
                            int? badgeNumber = 1;
                            //Get Badge
                            //if (item.DeviceType == "iPhone")
                            //{
                            //    Int64 CustomerId = item.ID;
                            //    var badgeDetail = getBadgeByCustomer(CustomerId);
                            //    NotificationsBadgeMaster obj = new NotificationsBadgeMaster();
                            //    obj.UserId = CustomerId;
                            //    if (badgeDetail.Count > 0)
                            //    {
                            //        badgeNumber = badgeDetail[0].Badge + 1;
                            //        obj.ID = badgeDetail[0].ID;
                            //        obj.Badge = badgeNumber;
                            //        obj.NotificationType = "iPhone";
                            //    }
                            //    else
                            //    {
                            //        badgeNumber = 1;
                            //        obj.Badge = badgeNumber;
                            //        obj.NotificationType = "iPhone";
                            //    }
                            //    ManageNotificationBadge(obj);
                            //}

                            PushNotification objPushNotification = new PushNotification();

                            //  PushNotificationsApple objPushNotificationApple = new PushNotificationsApple();
                            if (item.DeviceToken != "" && item.DeviceToken != null)
                            {
                                if (item.DeviceType == "iPhone")
                                {
                                    objPushNotification.PushtoIphone(item.DeviceToken, model.Message);
                                    sentCount++;
                                    //       objPushNotificationApple.PushToIphoneTJ(Convert.ToString(item.DeviceToken), model.Message, badgeNumber, "default");
                                }
                                else if (item.DeviceType.ToLower() == "android" || item.DeviceType.ToLower() == "a")
                                {
                                    postData =
                                    "{ \"registration_ids\": [ \"" + item.DeviceToken + "\" ], " +
                                    "\"data\": {\"tickerText\":\"" + tickerText + "\", " +
                                    "\"contentTitle\":\"" + contentTitle + "\", " +
                                    "\"price\": \"" + model.Message + "\"}}";

                                    //"{\"to\": \"" + deviceToken +
                                    //    "\", \"notification\": {\"title\": \"Saavi 5\",\"body\": \"" 
                                    //    + msg + "\"}\"data\": {\"InvoiceID\": \"" + orderID + "\"}}"


                                    objPushNotification.SendMessage(item.DeviceToken, postData);
                                    sentCount++;
                                    // objPushNotification.PushtoIphone(uID.DeviceToken, msg, eventID, uID.Id);
                                }
                                strString = "Success";
                            }
                            else { strString = "Device token not available!"; }
                        }

                        if (totalCustomers == sentCount)
                        {
                            strString = "Notification sent to all(" + totalCustomers + ") customers";
                        }
                        else if (sentCount == 0)
                        {
                            strString = "No customer with mobile device details was found from total (" + totalCustomers + ".) selected customer/s.";
                        }
                        else
                        {
                            strString = "Notification sent to " + sentCount + " customer/s out of " + totalCustomers + ".";
                        }
                    }
                    else
                    {
                        strString = "No customer avaliable as per selection!";
                    }

                }
                return strString;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 
        /// <summary>
        /// get PDF detail
        /// </summary>
        /// <returns></returns>
        public GetPdfFileModel GetPdfFile(long id, string type)
        {
            try
            {
                var hasPdfData = false;
                GetPdfFileModel pdf = new GetPdfFileModel();
                if (id > 0 && type == "G")
                {
                    var data = _db.GroupsMasters.Where(x => x.GroupID == id).FirstOrDefault();
                    if (data != null && data.Pdf != null)
                    {
                        pdf.GroupID = data.GroupID;
                        pdf.UserType = "G";
                        pdf.CustomerID = 0;
                        pdf.PDF_Name = data.Pdf;
                        hasPdfData = true;
                    }
                }
                else
                {
                    var data = _db.PDFMasters.Where(x => x.CustomerID == id).FirstOrDefault();
                    if (data != null)
                    {
                        pdf.ID = data.ID;
                        pdf.GroupID = 0;
                        if (id == 0) pdf.UserType = "A";
                        else pdf.UserType = "S";
                        pdf.CustomerID = data.CustomerID ?? 0;
                        pdf.PDF_Name = data.PDF;
                        hasPdfData = true;
                    }
                }

                if (hasPdfData == false)
                {
                    var data = _db.PDFMasters.Where(x => x.CustomerID == 0).FirstOrDefault();
                    if (data != null)
                    {
                        pdf.ID = data.ID;
                        pdf.PDF_Name = data.PDF;
                    }
                }

                return pdf;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 
        /// <summary>
        /// manage pdf detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ManagePdfFile(GetPdfFileModel model)
        {
            try
            {
                if (model.GroupID > 0 && model.UserType == "G")
                {
                    //update pdf file for Group
                    var data = _db.GroupsMasters.Where(x => x.GroupID == model.GroupID).FirstOrDefault();
                    if (data != null)
                    {
                        data.Pdf = model.PDF;
                        data.ModifiedDate = DateTime.Now;
                        _db.SaveChanges();
                    }
                    else { return "No group available!"; }
                }
                else
                {
                    var data = _db.PDFMasters.Where(x => x.CustomerID == model.CustomerID).FirstOrDefault();
                    if (data != null)
                    {
                        data.ModifiedDate = DateTime.Now;
                        data.PDF = model.PDF;
                        data.PDF_Name = model.PDF_Name;
                    }
                    else
                    {
                        PDFMaster obj = new PDFMaster();
                        obj.CustomerID = model.CustomerID;
                        obj.CreatedDate = DateTime.Now;
                        obj.PDF = model.PDF;
                        obj.PDF_Name = model.PDF_Name;
                        _db.PDFMasters.Add(obj);
                    }
                    _db.SaveChanges();
                }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 
        /// <summary>
        /// get all group products
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public List<GetAllProductBO> GetAllGroupProducts(long groupID)
        {
            try
            {
                return (from a in _db.GroupsMasters
                        join b in _db.GroupProductsMasters on a.GroupID equals b.GroupID
                        join c in _db.ProductMasters on b.ProductID equals c.ProductID
                        where a.GroupID == groupID && a.IsActive == true && b.IsActive == true && c.IsActive == true
                        select new GetAllProductBO
                        {
                            ProductID = b.ProductID,
                            ProductName = c.ProductName + (c.ProductCode == null ? "" : " (" + c.ProductCode + ")")
                        }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region
        /// <summary>
        /// get all noo group products
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<GetAllProductBO> GetAllNonProducts(GetNonProductsModel model)// string SearchText, long groupID)
        {
            try
            {
                //var groupProducts = GetAllGroupProducts(groupID);
                //var results = (from a in _db.ProductMasters
                //               where a.IsActive == true &&
                //              (SearchText == "" ? true : a.ProductName.ToLower().Contains(SearchText.ToLower())
                //              || SearchText == "" || SearchText == null)
                //               select new GetAllProductBO
                //               {
                //                   ProductID = a.ProductID,
                //                   ProductName = a.ProductName + (a.ProductCode == null ? "" : " (" + a.ProductCode + ")")
                //               }
                //    ).OrderBy(x => x.ProductName).ToList();
                //for (int index = 0; index < groupProducts.Count; index++)
                //{
                //    results.RemoveAll(s => s.ProductID == groupProducts[index].ProductID);
                //}
                //return results;

                //from a in _db.GroupsMasters
                //join b in _db.GroupProductsMasters on a.GroupID equals b.GroupID
                //join c in _db.ProductMasters on b.ProductID equals c.ProductID
                //where a.GroupID == groupID && a.IsActive == true && b.IsActive == true && c.IsActive == true

                return (from a in _db.ProductMasters
                        join b in _db.GroupProductsMasters on a.ProductID equals b.ProductID into temp
                        from b in temp.DefaultIfEmpty()
                            //join c in _db.GroupsMasters on b.GroupID equals c.GroupID 
                        where b.ProductID == null && a.IsActive == true &&
                       (model.SearchText == "" ? true : a.ProductName.ToLower().Contains(model.SearchText.ToLower())
                       || model.SearchText == "" || model.SearchText == null)
                        select new GetAllProductBO
                        {
                            ProductID = a.ProductID,
                            ProductName = a.ProductName + (a.ProductCode == null ? "" : " (" + a.ProductCode + ")")
                        }
                   ).OrderBy(x => x.ProductName).Page(model.PageNumber * model.PageSize, model.PageSize).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 
        /// <summary>
        /// add/remove group products
        /// </summary>
        /// <param name="productIDs"></param>
        /// <returns></returns>
        public string ManageGroupProducts(string productIDs, long groupID)
        {
            try
            {
                string[] productArray = productIDs.Split(',');
                if (productArray.Length > 0 && productIDs != "")
                {
                    var data = _db.GroupProductsMasters.Where(x => x.GroupID == groupID && !productArray.Contains(x.ProductID.ToString())).ToList();
                    if (data.Count > 0)
                    {
                        _db.GroupProductsMasters.RemoveRange(data);
                        _db.SaveChanges();
                    }
                    foreach (string productId in productArray)
                    {
                        Int64 pID = Convert.ToInt64(productId);
                        var product = _db.GroupProductsMasters.Where(x => x.GroupID == groupID && x.ProductID == pID).FirstOrDefault();
                        if (product != null)
                        {
                            product.ModifiedDate = DateTime.Now;
                        }
                        else
                        {
                            GroupProductsMaster obj = new GroupProductsMaster();
                            obj.GroupID = groupID;
                            obj.ProductID = Convert.ToInt64(productId);
                            obj.CreatedDate = DateTime.Now;
                            obj.IsActive = true;
                            _db.GroupProductsMasters.Add(obj);
                        }
                        _db.SaveChanges();
                    }
                }
                else
                {
                    var data = _db.GroupProductsMasters.Where(x => x.GroupID == groupID).ToList();
                    if (data != null)
                    {
                        _db.GroupProductsMasters.RemoveRange(data);
                        _db.SaveChanges();
                    }
                }
                return "Success";
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        //POD********

        #region Get All Driver
        /// <summary>
        /// Get All Driver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<AllListBO> GetAllDrivers(GetListModel model)
        {
            try
            {
                if (model.PageSize > 0)
                {
                    return (from a in _db.UserMasters
                            where a.UserTypeID == 5 &&
                            (model.SearchText == "" ? true : a.FirstName.ToLower().Contains(model.SearchText.ToLower())
                             || a.ContactName.ToLower().Contains(model.SearchText.ToLower())
                             || a.Address.ToLower().Contains(model.SearchText.ToLower())
                             || model.SearchText == "" || model.SearchText == null)
                            select new AllListBO
                            {
                                ID = a.UserID,
                                Name = a.FirstName
                            }).OrderByDescending(c => c.ID).ToList();
                }
                else
                {
                    var groupCustomers = GetGroupUsers(model.GroupID, "d");
                    var results = (from u in _db.UserMasters
                                   where u.UserTypeID == 5
                                   select new AllListBO
                                   {
                                       ID = u.UserID,
                                       Name = u.FirstName
                                   }).OrderByDescending(c => c.ID).ToList();
                    for (int index = 0; index < groupCustomers.Count; index++)
                    {
                        results.RemoveAll(s => s.ID == groupCustomers[index].CustomerID);
                    }
                    return results;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region get group customers
        /// <summary>
        /// get group customers
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public List<AllCustomerBO> GetGroupUsers(Int64 groupID, string type)
        {
            try
            {
                return (from a in _db.UserGroupsMasters
                        join b in _db.UserMasters on a.CustomerID equals b.UserID
                        where a.GroupID == groupID && a.Type == type
                        select new AllCustomerBO
                        {
                            CustomerID = b.UserID,
                            CustomerName = b.FirstName
                        }).ToList();
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        #region get group's drivers
        /// <summary>
        /// get group's drivers
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public List<GetCompanyGroupCustomer> GetCompanyGroupdriversAll(long groupID)
        {
            try
            {
                var data = _db.GroupsMasters.Where(x => x.GroupID == groupID).FirstOrDefault();
                var cust = new List<GetCompanyGroupCustomer>();
                if (data != null)
                {
                    cust = (from a in _db.GroupsMasters
                            join b in _db.UserGroupsMasters on a.GroupID equals b.GroupID
                            join d in _db.UserMasters on b.CustomerID equals d.UserID
                            where (a.GroupID == groupID && b.Type == "d")
                            select new GetCompanyGroupCustomer
                            {
                                GroupID = a.GroupID,
                                GroupName = a.GroupName,
                                CustomerId = d.UserID,
                                ID = d.UserID,
                                CustomerName = d.FirstName,
                                DeviceToken = d.DeviceToken,
                                DeviceType = d.DeviceType
                            }).ToList();
                }
                return cust;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        public List<AllCustomerBO> GetAllDeliveryRun(Int64 groupID, string type)
        {
            try
            {

                if (type == "RM")
                {
                    var groupCustomers = GetGroupCustomers(groupID, "R");
                    return groupCustomers;
                }
                else
                {
                    var groupCustomers = GetGroupCustomers(groupID, "Uniq");
                    var results = (from cust in _db.CutoffGroupMasters

                                   select new AllCustomerBO
                                   {
                                       CustomerID = cust.GroupID,
                                       CustomerName = cust.GroupName
                                   }).OrderBy(c => c.CustomerName).ToList();
                    for (int index = 0; index < groupCustomers.Count; index++)
                    {
                        results.RemoveAll(s => s.CustomerID == groupCustomers[index].CustomerID);
                    }
                    return results;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUpdateGroupCustomerandOther(long groupID, string customerIDs, string type)
        {

            string[] custArray = customerIDs == null ? null : customerIDs.Split(',');

            if (custArray != null && custArray.Length > 0 && customerIDs != "")
            {
                var data = _db.UserGroupsMasters.Where(x => x.GroupID == groupID && !custArray.Contains(x.CustomerID.ToString()) && x.Type == type).ToList();
                if (data.Count > 0)
                {
                    _db.UserGroupsMasters.RemoveRange(data);
                    _db.SaveChanges();
                }
                foreach (var custID in custArray)
                {
                    Int64 cID = Convert.ToInt64(custID);
                    var cust = _db.UserGroupsMasters.Where(x => x.GroupID == groupID && x.CustomerID == cID && x.Type == type).FirstOrDefault();
                    if (cust != null)
                    {
                        cust.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        UserGroupsMaster obj = new UserGroupsMaster();
                        obj.GroupID = groupID;
                        obj.CustomerID = cID;
                        obj.IsActive = true;
                        obj.CreatedDate = DateTime.Now;
                        obj.Type = type;
                        _db.UserGroupsMasters.Add(obj);
                    }
                    _db.SaveChanges();
                }
            }
            else
            {
                var data = _db.UserGroupsMasters.Where(x => x.GroupID == groupID && x.Type == type).ToList();
                if (data.Count > 0)
                {
                    _db.UserGroupsMasters.RemoveRange(data);
                    _db.SaveChanges();
                }
            }

        }
    }
}
