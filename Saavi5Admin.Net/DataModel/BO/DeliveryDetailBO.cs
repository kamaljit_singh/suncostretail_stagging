﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BO
{
    public class DeliveryDetailBO
    {
        public long OrderNo { get; set; }

        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string ContactName { get; set; }
        public string CustomerNumber { get; set; }
        public string DeliveryAddress { get; set; }
        public Nullable<System.DateTime> EstimatedDeliveryDate { get; set; }
        public string OrderStatus { get; set; }
        public string PaymentMethod { get; set; }
        public double? PaymentRecieved { get; set; }
        public string Comments { get; set; }
        public long? DeliveryID { get; set; }
        public int? DeliveryIssuesRaised { get; set; }
        public int? TotalCartons { get; set; }
        public string CancelReason { get; set; }
        public Nullable<double> GoodsTemp { get; set; }
        public Nullable<bool> IsTempAuto { get; set; }
        public string PONumber { get; set; }
    }

    public class DeliveryImagesBO
    {
        public string BaseUrl { get; set; }
        public string ImageName { get; set; }

    }
    public class CompletionSideBarBO
    {
        public int TotalLineItems { get; set; }
        public int TotalLineItemsDelivered { get; set; }
        public double TotalLineItemsDeliveredPerc { get; set; }
        public int TotalLineItemsIssues { get; set; }
        public int TotalKPIperc { get; set; }
    }
    public class DeliveryIssueItemBO
    {
        public long ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
    }
}
