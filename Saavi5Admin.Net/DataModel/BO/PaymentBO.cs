﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DataModel.Model;

namespace DataModel.BO
{
    public class PaymentBO
    {
        public long CustomerID { get; set; }
        public long CartID { get; set; }
        public double Amount { get; set; }
        public long? UserID { get; set; }
        public string PaymentReference { get; set; }
    }

    public class ConnectAccountBO
    {
        public bool Connected { get; set; } = false;
        public string AccountID { get; set; } = "";
        public string Error { get; set; } = "";
    }



    public class RefundBO
    {

        public long CustomerID { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerName { get; set; }
        public long CartID { get; set; }
        public double Amount { get; set; }
        public long? UserID { get; set; }
        public string PaymentReference { get; set; }
        public double? Netpayment { get; set; }
        public double? TotalFees { get; set; }
        public double? Tax { get; set; }
        public double? ApplicationFees { get; set; }
        public double? StripeProcessingFee { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedTime { get; set; }
        public double? RefundTax { get; set; }
        public double? RefundStripeProcessingFee { get; set; }
        public double? TotalCaptureAmount { get; set; }
        public string RefundCreatedOn { get; set; }
        public string RefundCreatedTime { get; set; }
        public string RefundReference { get; set; }
        public int PaymentClick { get; set; }
        public string ShowAmount { get; set; }



    }
    public class OrderRefundHistory
    {
        public string PaymentReference { get; set; }
        public double? OrderTotal { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedTime { get; set; }
        public long CartID { get; set; }
        public string OrderDate { get; set; }
        public string OrderComments { get; set; }
        public string DeliveryNotes { get; set; }
        public bool? IsDelivery { get; set; } = false;
        public string CreatedDate { get; set; }
        public double? TotatOriginal { get; set; }
        public List<OrderDetailModal> OrderDetails { get; set; }
        public bool? isPaymentDone { get; set; }
        public bool isRefundDone { get; set; }
        public double? TotalRevised { get; set; }
        public double? TotalDifference { get; set; }
        public double? Total { get; set; }
        public string RefundCreatedOn { get; set; }
        public string RefundCreatedTime { get; set; }
        public string RefundReference { get; set; }
        public double? RevisedFreightCharge { get; set; }
        public double? CouponAmount { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerName { get; set; }        
        public double? OriginalFreightCharge { get; set; }
        public double? TotalFreightDifference { get; set; }
        public bool? IsHasFreightCharges { get; set; }
        
    }
    public class OrderPaymentHistory
    {
        public long CartID { get; set; }
        public string CreatedOn { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerName { get; set; }        
        public double? CouponAmount { get; set; }
        public List<OrderPaymentHistoryModal> OrderDetails { get; set; }
        public List<OrderPaymentHistoryModal> FirstOrderDetails { get; set; }
        public double? TotatOriginal { get; set; }
        public double? OrderTotal { get; set; }

    }

    public class OrderPaymentHistoryModal
    {
        public long CartID { get; set; }            
        public string CustomerEmail { get; set; }
        public string CustomerName { get; set; }        
        public DateTime? OrderDate { get; set; }       
        public string Address { get; set; }      
        public string RunNo { get; set; }      
        public string OrderComments { get; set; }
        public string DeliveryNotes { get; set; }
        public bool? IsDelivery { get; set; } = false;
        public DateTime? CreatedDate { get; set; }  
        public double? TotatRevisedPrice { get; set; }       
        public double? RevisedPrice { get; set; }
        public double? RevisedQuantity { get; set; }
        public double? CouponAmount { get; set; }      
        public double? OriginalFreightCharge { get; set; }     
        public int? PickUpId { get; set; }
        public string DeliveryType { get; set; }
        public string PickUpAddress { get; set; }
        public string PickUpName { get; set; }
   
        public double? RevisedFreightCharge { get; set; }
       

    }

}
