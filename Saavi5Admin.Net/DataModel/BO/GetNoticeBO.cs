﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BO
{
    public class GetNoticeBO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string OrderCutOff { get; set; }
        public string OrderCutOffDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
