﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataModel.BO
{
    public class UserBO
    {
        public long UserID { get; set; }
        public Nullable<byte> UserTypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string DefaultPassword { get; set; }
        public string Image { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string Address { get; set; }
        public string Age { get; set; }
        public string DriverLicenseNo { get; set; }
        public string DrivingExperience { get; set; }
        public string DriverShift { get; set; }
        public string DriverLicenseImage { get; set; }
        public HttpPostedFileBase driverImage { get; set; }
        public HttpPostedFileBase licenceImage { get; set; }
        public Nullable<short> VehicleID { get; set; }

    }
}
