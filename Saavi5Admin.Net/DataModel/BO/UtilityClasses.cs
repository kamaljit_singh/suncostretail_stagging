﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BO
{
    internal class UtilityClasses
    {
    }

    public class HeaderTypeModal
    {
        public string Type { get; set; }
        public bool ShowPanel { get; set; }
    }

    public class FeaturesMasterBo
    {
        public string Name { get; set; }
        public object Selected { get; set; }
        public string Description { get; set; } = "";
    }

    public class FeaturesModel
    {
        public List<FeaturesMasterBo> MainFeatures { get; set; }
        public List<FeaturesMasterBo> AdminFeatures { get; set; }
        public string Message { get; set; }
    }

    public class LoginResponse
    {
        public string Message { get; set; }
        public string Role { get; set; }
    }

    public class AllCustomerBO
    {
        public string CustomerName { get; set; }
        public long CustomerID { get; set; }
    }

    public class AllCustomerCountBO
    {
        public long ThisMonth { get; set; }
        public long LastMonth { get; set; }
    }

    public class GetCustomersModel
    {
        public bool IsRep { get; set; }
        public string SearchText { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public long GroupID { get; set; }
    }

    public class SelectListItems
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class AllCategoryBO
    {
        public string CategoryName { get; set; }

        public List<ProductCategoriesMaster> SubCategoryList { get; set; }
        public long CategoryID { get; set; }
    }

    public class GetProductsModel
    {
        public string SearchText { get; set; }
        public long CategoryID { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }

    public class GetNonProductsModel
    {
        public string SearchText { get; set; }
        public long GroupID { get; set; }
        public string Type { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }

    public class GetPermittedDaysModel
    {
        public DateTime RequiredDate { get; set; }
        public DateTime RequiredTime { get; set; }
        public string PermittedDays { get; set; }
        public bool IsDayAvailable { get; set; }
        public bool IsPermitSameDay { get; set; }
        public bool AllowPickup { get; set; }
        public bool LimitOrdersTo1Month { get; set; }
        public bool EnableCutoffRules { get; set; }
    }

    public class GetCustomerDeliveryDays
    {
        public string AlphaCode { get; set; }
        public string DayName { get; set; }
    }

    public class GetOrderModel
    {
        public string SearchText { get; set; }
        public string OrderType { get; set; }
        public string SortType { get; set; }
        public string AscDesc { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }

    public class GetStatsModel
    {
        public string StatsType { get; set; }
    }

    public class GetTopCustomerModel
    {
        public string StatsType { get; set; }
        public int n { get; set; }
    }

    public class GetGroupCustomers
    {
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupCustomers { get; set; }
        public string Type { get; set; }
    }

    public class GetMessageModel
    {
        public long ID { get; set; }
        public string UserType { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
    }

    public class GetCompanyGroupCustomer
    {
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public long CustomerId { get; set; }
        public long ID { get; set; }
        public string CustomerName { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
    }

    public class GetPdfFileModel
    {
        public int ID { get; set; }
        public long GroupID { get; set; }
        public long CustomerID { get; set; } = 0;
        public string UserType { get; set; } = "A";
        public string PDF { get; set; }
        public string PDF_Name { get; set; }
    }

    public class GetOrderMail
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Email { get; set; }
    }

    //POD Admin classes

    public class GetListModel
    {
        public string SearchText { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public long GroupID { get; set; }
    }

    public class AllListBO
    {
        public string Name { get; set; }
        public long ID { get; set; }
        public string VehicleNo { get; set; }
        public string RunNo { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class AllOrderVehicleBO
    {
        public string VehicleName { get; set; }
        public long VehicleID { get; set; }
        public long? DriverID { get; set; }
        public string DriverName { get; set; }
        public string RunNo { get; set; }
        public long OrderID { get; set; }
        public long DeliveryID { get; set; }
    }

    public class VehicleDeliveryStatusBO
    {
        public string Name { get; set; }
        public long ID { get; set; }
        public string RunNo { get; set; }
        public bool? HasItemIssue { get; set; }
    }

    public class AllDriverCompBO
    {
        public string Name { get; set; }
        public long ID { get; set; }
        public long InvoiceNo { get; set; }
        public string OrderStatus { get; set; }
        public bool IsCompliance { get; set; }
        public bool? IsOnline { get; set; }
    }

    public class AllDeliveryTrackBO
    {
        public long TrackID { get; set; }
        public string DriverName { get; set; }
        public string VehicleName { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public string description { get; set; }
    }

    public class AllAssignedOrdersBO
    {
        public long OrderID { get; set; }
        public string CustomerName { get; set; }
        public long? DriverID { get; set; }
        public string RunNo { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public bool IsDelDateCrossed { get; set; }
        public string Suburb { get; set; }
    }

    public class DeliveryDateBO
    {
        public int orderID { get; set; }
        public DateTime? PrevDeliveryDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
    }

    public class AllChildCustomerBO
    {
        public string CustomerName { get; set; }
        public long CustomerID { get; set; }
        public long? ParentID { get; set; }
        public bool Selected { get; set; }
    }

    public class ParentChildModel
    {
        public string SearchText { get; set; }
        public long ID { get; set; }
        public bool IsParent { get; set; }
    }

    public class DeliveryRunBO
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class OrderLimitBO
    {
        public string Type { get; set; }
        public string Value { get; set; }
        public Int16 Limit { get; set; }
        public int ID { get; set; }
    }

    public class CutoffTimeBO
    {
        public string Type { get; set; }
        public string Value { get; set; }
        public string CutoffTime { get; set; }
        public int ID { get; set; }
    }

    public class PromoCodeBO
    {
        public string PromoCode { get; set; }
        public string PromoType { get; set; }
        public string Type { get; set; }
        public double PromoValue { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public byte Redemptions { get; set; }
        public double MinCartVal { get; set; }
        public int PromoID { get; set; }
        public long CustomerID { get; set; }
        public string Prods { get; set; }
        public long[] Products { get; set; }
        public bool LimitPerCust { get; set; }
    }

    public class PermittedDaysBO
    {
        public List<PermittedDaysMaster> li { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string PermitBy { get; set; }
        public string Value { get; set; }
        public int ID { get; set; }
    }

    public class ManageModelPopBO
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Days { get; set; }
        public string ModelType { get; set; }
        public int ID { get; set; }
    }

    public class ManageModelPopDetailsBO
    {
        public int GroupID { get; set; }
        public string Filter { get; set; }
        public string ChkValue { get; set; }
        public string ListValue { get; set; }
    }

    public class OrderDetailModal
    {
        public long CartID { get; set; }
        public long CartItemID { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string DeliveryName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public long ProductID { get; set; }
        public double? Price { get; set; }
        public double? Quantity { get; set; }
        public DateTime? OrderDate { get; set; }
        public string UOM { get; set; }
        public string Address { get; set; }
        public string Warehouse { get; set; }
        public string Suburb { get; set; }
        public string RunNo { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string OrderComments { get; set; }
        public string DeliveryNotes { get; set; }
        public bool? IsDelivery { get; set; } = false;
        public DateTime? CreatedDate { get; set; }
        public double? TotatOriginalPrice { get; set; }
        public double? TotatRevisedPrice { get; set; }
        public double? Difference { get; set; }
        public double? RevisedPrice { get; set; }
        public double? RevisedQuantity { get; set; }
        public double? CouponAmount { get; set; }
        public double? RevisedFreightCharge { get; set; }

        public double? OriginalFreightCharge { get; set; }
        public string Email { get; set; }
        public int? PickUpId { get; set; }
        public string DeliveryType { get; set; }
        public string InvoiceNumber { get; set; }
        public string CouponName { get; set; }
        public bool IsCouponItem { get; set; }
        public bool? IsHasFreightCharges { get; set; }

        public int AddressID { get; set; }

        public string DAddress1 { get; set; }
        public string DSuburb { get; set; }
        public string DZIP { get; set; }
    }

    public class GetOrderToEditBO
    {
        public long CartID { get; set; }
        public string OrderDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string DeliveryName { get; set; }
        public long CustomerID { get; set; }
        public string CustomerEmail { get; set; }
        public string Warehouse { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string RunNo { get; set; }
        public string OrderComments { get; set; }
        public string DeliveryNotes { get; set; }
        public bool? IsDelivery { get; set; } = false;
        public string CreatedDate { get; set; }
        public double? TotatOriginal { get; set; }
        public List<OrderDetailModal> OrderDetails { get; set; }
        public bool? isPaymentDone { get; set; }
        public bool isRefundDone { get; set; }
        public double? TotalRevised { get; set; }
        public double? TotalDifference { get; set; }
        public double? Total { get; set; }
        public double? CouponAmount { get; set; }
        public double? RevisedFreight { get; set; }
        public double? OriginalFreight { get; set; }
        public double? TotalFreightDifference { get; set; }
        public string Email { get; set; }
        public string PickUpId { get; set; }
        public string DeliveryType { get; set; }
        public List<DeliveryPickup> PickupList { get; set; }
        public string InvoiceNumber { get; set; }
        public string DiscountPercentage { get; set; }
        public string DiscountValue { get; set; }
        public string CouponName { get; set; }
        public bool? IsHasFreightCharges { get; set; }

        public int AddressID { get; set; }
        public string DAddress1 { get; set; }
        public string DSuburb { get; set; }
        public string DZIP { get; set; }
    }

    public class DeliveryPickup
    {
        public int ID { get; set; }
        public string PickupName { get; set; }
        public string PickupAddress { get; set; }
    }

    public class AddProductBO
    {
        public long CartID { get; set; }
        public long ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string UOM { get; set; }
        public double? Quantity { get; set; }
        public double? Price { get; set; }
    }

    public class SetCutoffPermittedDaysBO
    {
        public int ID { get; set; }
        public int GroupID { get; set; }
        public string Days { get; set; }
        public string PermitBy { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }

    public class GetCutoffPermittedDaysBO
    {
        public int GroupID { get; set; }
        public string PermitBy { get; set; }
    }

    public class AddProductsToOrderBO
    {
        public long ProductID { get; set; }
        public double Quantity { get; set; }
    }

    public class UpdateOrderDetailsBO
    {
        public long CartID { get; set; }
        public DateTime OrderDate { get; set; }
        public bool IsDelivery { get; set; }
        public string OrderComment { get; set; }
        public string RunNo { get; set; }
        public string DeliveryNotes { get; set; }
        public string Address { get; set; }
        public bool IsAddressChanged { get; set; }
        public List<EditProductDetails> Items { get; set; }
        public bool PaymentDone { get; set; }
        public int PickupID { get; set; }
    }

    public class EditProductDetails
    {
        public long ProductID { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
    }

    public class ExportOrdersBO
    {
        public long OrderNo { get; set; }
        public string Customer { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string RequiredDate { get; set; }
        public string OrderDate { get; set; }
        public string ProductName { get; set; }
        public double? Quantity { get; set; }
        public string UOMDesc { get; set; }
        public double? Price { get; set; }
        public string OrderComment { get; set; }
        public string ProductComment { get; set; }
    }

    public class DeliveryRunsBO
    {
        public string RunNo { get; set; }
        public string Warehouse { get; set; }
    }
}