﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BO
{
   public class CustomerBO
    {
        public long CustomerID { get; set; }
        public string AlphaCode { get; set; }
        public string CustomerName { get; set; }
        public string ABN { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public int? StateID { get; set; }
        public string State{ get; set; }
        public int? CountryID { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string ContactName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public List<string> UserEmails { get; set; }
        public string SalesmanCode { get; set; }
        public bool? DebtorOnHold { get; set; }
        public long? DebtorTypeID { get; set; }
        public string DebtorType { get; set; }
        public string AreaCode { get; set; }
        public string ProfileImage { get; set; }
        public long? TermID { get; set; }
        public string Terms { get; set; }
        public string CreditLimit { get; set; }
        public string DeliveryAddress1 { get; set; }
        public string DeliveryAddress2 { get; set; }
        public string DeliverySuburb { get; set; }
        public string DeliveryCity { get; set; }
        public int? DeliveryStateID { get; set; }
        public string DeliveryState { get; set; }
        public string DeliveryPostCode { get; set; }
        public int? CustomerServedFrom { get; set; }
        public bool? IsSuspended { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsRep { get; set; }
        public bool? IsSundayOrdering { get; set; }
        public DateTime? CreatedDate { get; set; }

       
        public DateTime? ModifiedDate { get; set; }
        public int? PRICE_CODE { get; set; }
        public string BalanceTotal { get; set; }
        public double? CurrentBalance { get; set; }
        public double? Day30Balance { get; set; }
        public double? Day60Balance { get; set; }
        public double? Day90PlusBalance { get; set; }
        public string StandardDeliveryDays { get; set; }
        public double? TotalAmountDue { get; set; }
        public double? TotalAmountOwing { get; set; }
        public string TermDescription { get; set; }
        public string PriceLevel { get; set; }
        public bool? FromCustDB { get; set; }
        public string Warehouse { get; set; }
        public double? YTDSales { get; set; }
        public double? MTDSales { get; set; }
        public double? PrevMonthSales { get; set; }
        public string TagNames { get; set; }
        public bool? IsSpecialCategory { get; set; }
        public string ReminderDay { get; set; }
        public string PaymentMethod { get; set; }
        public string RunNo { get; set; }
        public int? DeliveryCountryID { get; internal set; }
        public string DeliveryCountry { get; internal set; }

        public string Password { get; set; }
        public bool? IsParent { get; set; }
        public long? ParentID { get; set; } = 0;
        public bool? IsChild { get; set; }
        public List<Int64> ChildList { get; set; }
        public List<UserBO> UserEmailAndID { get; set; }

    }
}
