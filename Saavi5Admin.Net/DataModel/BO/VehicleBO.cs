﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BO
{
    public class VehicleBO
    {
        public short VehicleID { get; set; }
        public Nullable<long> DriverID { get; set; }
        public string VehicleName { get; set; }
        public bool IsDeleted { get; set; }

        public string VehicleNo { get; set; }
        public string RegistrationNo { get; set; }
        public string Route { get; set; }
        public long? CheckListID { get; set; }
        public string Shift { get; set; }
        public Nullable<double> OdometerStart { get; set; }
        public Nullable<double> OdometerEnd { get; set; }
        public Nullable<bool> IsVehicleConditionOK { get; set; }
        public Nullable<bool> IsWindscreenOK { get; set; }
        public Nullable<bool> IsLightSignalOK { get; set; }
        public Nullable<bool> IsTyreConditionOK { get; set; }
        public Nullable<bool> IsFixturesFittingOK { get; set; }
        public Nullable<double> Weight { get; set; }
        public Nullable<double> Temprature { get; set; }
        public Nullable<double> TempChilled { get; set; }
        public Nullable<double> TempFrozen { get; set; }
        public string Comments { get; set; }
    }
}
