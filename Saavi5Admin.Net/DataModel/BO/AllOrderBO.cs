﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BO
{
    public class AllOrderBO
    {
        public long OrderID { get; set; }
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string PostCode { get; set; }
        public int? OrderStatusID { get; set; }
        public string OrderStatus { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public Nullable<bool> HasItemIssues { get; set; }
        public Nullable<bool> isAuthCount { get; set; }
        public Nullable<bool> isPaymentDone { get; set; }
        public double? CapturePrice { get; set; }
        public long? UserID { get; set; }
        public Nullable<bool> isRefundDone { get; set; }
        public double? FraightCharge { get; set; }
        public double? CouponAmount { get; set; }
        public Nullable<double> ActualPrice { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public Nullable<double> Invoice { get; set; }
        public string Email { get; set; }
    }

    public class CalculatedOrdersBO
    {
        public long OrderID { get; set; }
        public long CustomerID { get; set; }
        public Nullable<double> OrderValue { get; set; }
        public Nullable<double> OrderCostValue { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public bool? IsOrderPlacedByRep { get; set; } = false;
        public string DeviceType { get; set; }
    }

    public class OrderProductsBO
    {
        public long ProductID { get; set; }
        public long CategoryID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public Nullable<double> TotalProductUnitsSold { get; set; }
        public Nullable<double> TotalProductSalesValue { get; set; }
        public Nullable<double> TotalProductMargin { get; set; }
    }

    public class OrderCategoriesBO
    {
        public long CategoryID { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public Nullable<double> TotalCategoryUnitsSold { get; set; }
        public Nullable<double> TotalCategorySalesValue { get; set; }
        public Nullable<double> TotalCategoryMargin { get; set; }
    }

    public class WeekSalesData
    {
        public int OrderDay { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public Nullable<double> OrderTotalValue { get; set; }
    }

    public class OrderStatsBO
    {
        public Nullable<double> TodayOrdersTotal { get; set; } = 0;
        public Nullable<double> TodayAverageValue { get; set; } = 0;
        public Nullable<double> TodayAverageMargin { get; set; } = 0;
        public int TodayOrdersByReps { get; set; } = 0;
        public int TodayTotalOrders { get; set; } = 0;

        public Nullable<double> YesterdayOrdersTotal { get; set; } = 0;
        public Nullable<double> YesterdayAverageValue { get; set; } = 0;
        public Nullable<double> YesterdayAverageMargin { get; set; } = 0;
        public int YesterdayTotalOrders { get; set; } = 0;

        public Nullable<double> SameDayLastWeekOrdersTotal { get; set; } = 0;
        public int SameDayLastWeekTotalOrders { get; set; } = 0;

        public Nullable<double> ThisMonthTotalOrderValue { get; set; } = 0;
        public int ThisMonthTotalOrders { get; set; } = 0;
        public int ThisMonthActiveCustomers { get; set; } = 0;
        public int ThisMonthAppCustomers { get; set; } = 0;

        public int LastMonthActiveCustomers { get; set; } = 0;
        public int LastMonthAppCustomers { get; set; } = 0;
    }

    public class OrderKeyStatsBO
    {
        public Nullable<double> TodayTotal { get; set; } = 0;
        public Nullable<double> YesterdayTotal { get; set; } = 0;
        public Nullable<double> SameDayLastWeekTotal { get; set; } = 0;
        public Nullable<double> ThisMonthTotalValue { get; set; } = 0;
    }

    public class CustomerOrdersBO
    {
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public int TotalOrders { get; set; }
        public Nullable<double> OrdersValue { get; set; }
        public Nullable<double> OrderMarginValue { get; set; }
    }

    public class CartSettingsModel
    {
        public double DiscountValue { get; set; }
        public bool IsPercentage { get; set; }
        public double ProfitLimitValue { get; set; }
        public double? MinCartValue { get; set; }
    }
}