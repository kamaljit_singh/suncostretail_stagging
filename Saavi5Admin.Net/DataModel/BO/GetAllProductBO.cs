﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataModel.BO
{
    public class GetAllProductBO
    {
        public long ProductID { get; set; }
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<double> ComPrice { get; set; }
        public string ProductImage { get; set; }
        public string ThumbNail { get; set; }
        public bool IsActive { get; set; }
        public bool IsAvailable { get; set; }
        public Nullable<bool> PiecesAndWeight { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public long PFilterID { get; set; }
        public string FilterName { get; set; }

        public long UnitID { get; set; }
        public string UnitName { get; set; }
        public int TotalPages { get; set; }
        public List<ProductImageMaster> ImageList { get; set; }
        public List<FileList> FileList { get; set; }
        public string ProductFeature { get; set; }
        public bool GST { get; set; }
        public int? StockOnHand { get; set; }
    }

    public class FileList
    {
        public long ProductID { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string FileID { get; set; }
    }
}