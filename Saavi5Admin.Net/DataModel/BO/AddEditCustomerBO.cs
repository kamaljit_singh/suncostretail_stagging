﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BO
{
    public class AddEditCustomerBO
    {
        public long CustomerID { get; set; }
        public string AlphaCode { get; set; }
        public string CustomerName { get; set; }
        public string ABN { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public int? StateID { get; set; }
        public int? CountryID { get; set; } = 0;
        public string PostCode { get; set; }
        public string ContactName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string SalesmanCode { get; set; } = "0";
        public bool? DebtorOnHold { get; set; }
        public long? DebtorTypeID { get; set; }
        public string DebtorType { get; set; }
        public string AreaCode { get; set; }
        public string ProfileImage { get; set; }
        public bool IsActive { get; set; } = false;
        public bool? IsRep { get; set; } = false;
        public bool IsSundayOrdering { get; set; } = false;
      //  public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Warehouse { get; set; }
        public string TagNames { get; set; }
        public bool? IsSpecialCategory { get; set; }
        public string ReminderDay { get; set; }
        public string PaymentMethod { get; set; }
        public string RunNo { get; set; }
        public string Password { get; set; }
        public byte UserTypeID { get; set; }
    }
}
