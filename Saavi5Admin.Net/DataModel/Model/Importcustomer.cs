//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Importcustomer
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public System.DateTime Last_Active { get; set; }
        public Nullable<System.DateTime> Sign_Up { get; set; }
        public string Email { get; set; }
        public string Orders { get; set; }
        public string Total_Spend { get; set; }
        public string AOV { get; set; }
        public string Country_Region { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public int Postal_Code { get; set; }
    }
}
