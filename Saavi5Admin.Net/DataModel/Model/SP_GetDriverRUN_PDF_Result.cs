//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel.Model
{
    using System;
    
    public partial class SP_GetDriverRUN_PDF_Result
    {
        public long cartid { get; set; }
        public string InvoiceNumber { get; set; }
        public string CustomerName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string PaymentMethod { get; set; }
        public Nullable<double> Amount { get; set; }
        public string Completed { get; set; }
        public string IsCancelled { get; set; }
        public string CancelReason { get; set; }
        public Nullable<double> PaymentAmount { get; set; }
        public string RunNo { get; set; }
        public string ItemIssue { get; set; }
        public string IssueImage { get; set; }
        public string ProductName { get; set; }
        public string DepartureTime { get; set; }
        public string ReturnTime { get; set; }
        public Nullable<bool> Fuel { get; set; }
    }
}
