//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class trnsTempCartMaster
    {
        public long CartID { get; set; }
        public long CustomerID { get; set; }
        public Nullable<bool> IsOrderPlpacedByRep { get; set; }
        public Nullable<long> RepUserID { get; set; }
        public Nullable<long> LoginId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> AddressID { get; set; }
        public string Warehouse { get; set; }
        public string ExtDoc { get; set; }
        public Nullable<bool> IsInvoiceComment { get; set; }
        public Nullable<bool> IsUnloadComment { get; set; }
        public string RunNo { get; set; }
        public string CommentLine { get; set; }
        public string PackagingSequence { get; set; }
        public Nullable<bool> IsSavedOrder { get; set; }
        public Nullable<long> UnloadCommentID { get; set; }
        public Nullable<long> CommentID { get; set; }
        public Nullable<long> InvoiceCommentID { get; set; }
        public Nullable<bool> IsDelivery { get; set; }
        public string Coupon { get; set; }
        public Nullable<bool> IsCouponApplied { get; set; }
        public Nullable<double> CouponAmount { get; set; }
    }
}
