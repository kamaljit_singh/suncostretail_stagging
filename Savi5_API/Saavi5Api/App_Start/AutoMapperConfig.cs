﻿using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Saavi5Api.App_Start
{
	public static class AutoMapperConfig
	{
        /// <summary>
        /// Mappings for Automapper to be used throughout the application
        /// </summary>
		public static void RegisterMappings()
		{
			AutoMapper.Mapper.Initialize(config =>
			{
				config.CreateMap<UserMaster, UserProfile>().ReverseMap();
                config.CreateMap<CustomerFeaturesMaster, CustomerFeaturesBO>().ReverseMap();
                config.CreateMap<PantryListMaster, PantryListBO>().ReverseMap();
                config.CreateMap<PantryListItemsMaster, PantryItemsBO>().ReverseMap();
                config.CreateMap<ProductFiltersMaster, ProductFiltersBO>().ReverseMap();
                config.CreateMap<MainFeaturesMaster, MainFeaturesBO>().ReverseMap();
                config.CreateMap<trnsTempCartMaster, TempCartBO>().ReverseMap();
                config.CreateMap<trnsTempCartItemsMaster, TempCartItems>().ReverseMap();
                config.CreateMap<PantryListItemsMaster, WeeklySalesBO>().ReverseMap();
                config.CreateMap<trnsCartMaster, PlaceOrderBO>().ReverseMap();
                config.CreateMap<trnsCartItemsMaster, OrderItemsBO>().ReverseMap();
                config.CreateMap<trnsCartItemsMaster, trnsTempCartItemsMaster>().ReverseMap();
                config.CreateMap<VehicleChecklistMaster, VehicleChecklistBO>().ReverseMap();
            });
		}
	}
}