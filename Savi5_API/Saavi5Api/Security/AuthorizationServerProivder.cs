﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Saavi5Api.Security
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var form = await context.Request.ReadFormAsync();

                var deviceToken = form["deviceToken"].ToString();
                var deviceType = form["deviceType"].ToString();
                var appType = "";
                var loginType = "";

                if (form["appType"] != null)
                {
                    appType = form["appType"].ToString();
                }

                if (form["LoginType"] != null)
                {
                    loginType = form["LoginType"].ToString();
                }

                var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

                if (allowedOrigin == null) allowedOrigin = "*";

                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

                // Validate your user and base on validation return claim identity or invalid_grant error
                string user = context.UserName;
                string password = context.Password;
                using (var _dbcontext = new Saavi5Entities())
                {
                    var userInfo = new UserMaster();
                    if (string.Compare(appType, "pod", true) == 0)
                    {
                        userInfo = _dbcontext.UserMasters.FirstOrDefault(u => (u.Email == context.UserName || u.UserName == context.UserName) && (u.UserTypeID == 5 || u.UserTypeID == 6));
                    }
                    else if (string.Compare(loginType, "G", true) == 0)
                    {
                        userInfo = _dbcontext.UserMasters.Find(HelperClass.GuestLoginID);
                    }
                    else
                    {
                        userInfo = _dbcontext.UserMasters.FirstOrDefault(u => u.Email == context.UserName && (u.UserTypeID == 4 || u.UserTypeID == 3));
                    }

                    if (userInfo != null)
                    {
                        bool isParent = false;

                        if (userInfo.CustomerID != null)
                        {
                            isParent = _dbcontext.CustomerMasters.Find(userInfo.CustomerID).IsParent ?? false;
                        }

                        if (userInfo.IsActive == false)
                        {
                            context.SetError("Not Authorized", "This user is disabled. Please contact admin for more details.");
                            return;
                        }

                        if (userInfo.CustomerID != null && userInfo.UserTypeID != 5 && userInfo.UserTypeID != 6)
                        {
                            var customer = _dbcontext.CustomerMasters.Find(userInfo.CustomerID);
                            if (customer == null || customer.IsActive == false)
                            {
                                context.SetError("Not Authorized", "This customer is set to Inactive. Please contact admin for more details.");
                                return;
                            }
                        }
                        else
                        {
                            if (userInfo.UserTypeID != 5 && userInfo.UserTypeID != 6)
                            {
                                context.SetError("Not Authorized", "This user is not associated with any customer yet.");
                                return;
                            }
                        }

                        byte[] saltBytes = new byte[8];

                        saltBytes = Convert.FromBase64String(userInfo.PasswordSalt);

                        if (ENCDEC.ComputeHash(password, "MD5", saltBytes) == userInfo.PasswordHash || loginType.ToUpper() == "G")
                        {
                            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                            identity.AddClaim(new Claim("BusinessName", userInfo.BusinessName == null ? "" : userInfo.BusinessName));
                            identity.AddClaim(new Claim("Name", userInfo.FirstName + " " + userInfo.LastName));
                            identity.AddClaim(new Claim("Role", userInfo.UserTypeMaster.Name));
                            identity.AddClaim(new Claim("Email", userInfo.Email));
                            identity.AddClaim(new Claim("UserID", userInfo.UserID.ToString()));
                            identity.AddClaim(new Claim("CustomerID", userInfo.CustomerID.ToString()));

                            var props = new AuthenticationProperties(new Dictionary<string, string>
                                {
                                    {
                                        "BusinessName", userInfo.BusinessName == null ? "" : userInfo.BusinessName
                                    },
                                    {
                                        "Name",  userInfo.FirstName + " " + userInfo.LastName
                                    },
                                    {
                                        "role",userInfo.UserTypeMaster.Name
                                    },
                                    {
                                        "userID", userInfo.UserID.ToString()
                                    },
                                    {
                                        "customerID", userInfo.CustomerID.ToString()
                                    },
                                    {
                                        "isParent", isParent.ToString()
                                    }
                                });

                            if (userInfo.UserTypeID == 5)
                            {
                                long checklistID = 0;
                                string checkDate = "";
                                string currentDate = HelperClass.GetCurrentTime().ToShortDateString();
                                var checklist = _dbcontext.VehicleChecklistMasters.Where(v => v.DriverID == userInfo.UserID && (v.OdometerEnd == 0 || v.OdometerEnd == null)).OrderByDescending(o => o.CreatedDate).FirstOrDefault();
                                if (checklist != null)
                                {
                                    checklistID = checklist.ID;
                                    checkDate = Convert.ToDateTime(checklist.CreatedDate).ToShortDateString();
                                }
                                var userProfile = (from u in _dbcontext.UserMasters
                                                   where u.UserID == userInfo.UserID
                                                   select new
                                                   {
                                                       UserID = u.UserID,
                                                       UserName = u.UserName ?? "",
                                                       FirstName = u.FirstName,
                                                       LastName = u.LastName ?? "",
                                                       UserTypeID = u.UserTypeID,
                                                       Image = u.Image == null ? "" : HelperClass.PODImageUrl + u.Image,
                                                       Email = u.Email,
                                                       Phone = u.Phone ?? "",
                                                       IsNotificationEnabled = u.IsNotificationEnabled ?? false,
                                                       IsOnline = u.IsOnline ?? false,
                                                       ChecklistID = checklistID,
                                                       ChecklistDate = checkDate,
                                                       CurrentDate = currentDate,
                                                       ScanMode = u.ScanMode ?? false
                                                   }).FirstOrDefault();
                                props.Dictionary.Add("userProfile", JsonConvert.SerializeObject(userProfile));
                                props.Dictionary.Remove("customerID");
                            }
                            if (userInfo.UserTypeID == 6)
                            {
                                props.Dictionary.Remove("customerID");
                            }

                            var ticket = new AuthenticationTicket(identity, props);
                            context.Validated(ticket);

                            userInfo.DeviceToken = deviceToken;
                            userInfo.DeviceType = deviceType;
                            userInfo.ModifiedOn = HelperClass.GetCurrentTime();
                            var res = await _dbcontext.SaveChangesAsync();
                        }
                        else
                        {
                            context.SetError("invalid_grant", "The password you entered is incorrect.");
                            return;
                        }
                    }
                    else
                    {
                        if (form["appType"] != null)
                        {
                            context.SetError("invalid_grant", "​The Username you entered is incorrect.");
                        }
                        else
                        {
                            context.SetError("invalid_grant", "​The Email address you entered is incorrect.");
                        }

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}