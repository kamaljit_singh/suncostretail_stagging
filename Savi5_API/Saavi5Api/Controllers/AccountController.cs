﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.DA;
using Saavi5Api.DataAccess.Model;
using Saavi5Api.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Http;

namespace Saavi5Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private readonly AccountDA _da;

        /// <summary>
        /// Account management controller
        /// </summary>
        public AccountController()
        {
            _da = new AccountDA();
        }

        #region Get UserID or CustomerID from token.

        /// <summary>
        /// Get the values from user claims to be used in project
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal long GetClaimsValue(string type)
        {
            string claim = type == "U" ? "UserID" : "CustomerID";
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            return Convert.ToInt64(principal.Claims.Where(c => c.Type == claim).Single().Value);
        }

        #endregion Get UserID or CustomerID from token.

        #region New user registration

        /// <summary>
        /// Method to insert new user
        /// </summary>
        /// <param name="model">Register modal property class</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var res = await _da.RegisterUser(model);
            return Ok(res);
        }

        #endregion New user registration

        #region Forgot Password

        /// <summary>
        /// Forgot Password method
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            var res = await _da.ForgotPassword(model);
            return Ok(res);
        }

        #endregion Forgot Password

        #region Get User Prfile Data

        /// <summary>
        /// Get User profile info by UserID
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetUserProfile")]
        public async Task<IHttpActionResult> GetUserProfile(CustomerfeaturesBO model)
        {
            var res = await _da.GetUserProfile(model);

            return Ok(res);
        }

        #endregion Get User Prfile Data

        #region Set New Password

        /// <summary>
        /// Set a new password after first Login by an App User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("SetPassword")]
        public IHttpActionResult SetNewPassword(SetPasswordModel model)
        {
            var res = _da.SetNewPassword(model);
            return Ok(res);
        }

        #endregion Set New Password

        #region Change Password

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="model">Change password model</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordModel model)
        {
            var res = await _da.ChangePassword(model);
            return Ok(res);
        }

        #endregion Change Password

        #region Update user profile data

        /// <summary>
        /// Update basic user information
        /// </summary>
        /// <param name="model">User Model</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("UpdateProfile")]
        public async Task<IHttpActionResult> UpdateProfile(UserProfile model)
        {
            var res = await _da.UpdateUserProfile(model);
            return Ok(res);
        }

        #endregion Update user profile data

        #region Get default values for the functionalities an app should have

        /// <summary>
        /// Get default values for user CustomerFeatures
        /// </summary>
        /// <param name="model">{"userID":1}</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetCustomerDetailsAndFeatures")]
        public async Task<IHttpActionResult> GetCustomerDetailsAndFeatures(CustomerfeaturesBO model)
        {
            var res = await _da.GetCustomerDetailsAndFeatures(model);
            return Ok(res);
        }

        #endregion Get default values for the functionalities an app should have

        #region Main features and colors for the application

        /// <summary>
        /// Get the main for the application including colors.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetMainFeatures")]
        public async Task<APiResponse> GetMainFeatures()
        {
            var res = await _da.GetMainFeatures();
            return res;
        }

        #endregion Main features and colors for the application

        #region Get available delivery dates and Intro Message

        /// <summary>
        /// Intro message and Delivery dates
        /// </summary>
        /// <param name="apiData">{"customerID":1}</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetDefaultOrderingInfo")]
        [CompressFilter]
        public async Task<IHttpActionResult> GetDefaultOrderingInfo(Object apiData)
        {
            dynamic obj = apiData;
            long customerID = Convert.ToInt64(obj.customerID);
            long userID = GetClaimsValue("U");
            var res = await _da.GetDefaultOrderingInfo(customerID, userID);
            return Ok(res);
        }

        #endregion Get available delivery dates and Intro Message

        #region Get Customer addresses

        /// <summary>
        /// Get customer addresses
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetCustomerAddresses")]
        [CompressFilter]
        public async Task<IHttpActionResult> GetCustomerAddresses()
        {
            long customerID = GetClaimsValue("C");
            var res = await _da.GetCustomerDeliveryAddresss(customerID);
            return Ok(res);
        }

        #endregion Get Customer addresses

        #region Logout

        /// <summary>
        /// Service to clear the device token for a user.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("Logout")]
        public async Task<IHttpActionResult> Logout(LogoutBO model)
        {
            var res = await _da.Logout(model);
            return Ok(res);
        }

        #endregion Logout

        #region Get Page help data

        /// <summary>
        /// Get Text regarding page help
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetPageHelp")]
        public async Task<IHttpActionResult> GetPageHelp(string page, bool isList = false)
        {
            var res = await _da.GetPageHelp(page, isList);
            return Ok(res);
        }

        #endregion Get Page help data

        #region Get pickup locations

        /// <summary>
        /// Get all pickup locations
        /// </summary>
        /// <returns></returns>
        ///
        [HttpGet]
        [Route("GetPickupLocations")]
        public async Task<IHttpActionResult> GetPickupLocations()
        {
            var res = await _da.GetPickupLocations();
            return Ok(res);
        }

        #endregion Get pickup locations

        #region Add Delivery address

        /// <summary>
        /// Add customer delivery addresses
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddDeliveryAddress")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> AddDeliveryAddress(AddCustomerAddressBO model)
        {
            var res = await _da.AddDeliveryAddress(model);

            return Ok(res);
        }

        #endregion Add Delivery address

        #region Delete Delivery address

        /// <summary>
        /// delete customer delivery addresses
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteDeliveryAddress")]
        public async Task<IHttpActionResult> DeleteDeliveryAddress(DelAddress model)
        {
            var res = await _da.DeleteDeliveryAddress(model);

            return Ok(res);
        }

        #endregion Delete Delivery address

        #region Get pickup delivery dates

        /// <summary>
        /// delete customer delivery addresses
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetPickupDeliveryDates")]
        public async Task<IHttpActionResult> GetPickupDeliveryDates(Pickups model)
        {
            var res = await _da.GetPickupDeliveryDates(model);

            return Ok(res);
        }

        #endregion Get pickup delivery dates
    }
}