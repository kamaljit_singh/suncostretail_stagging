﻿using Newtonsoft.Json.Linq;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace Saavi5Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Products")]
    public class ProductsController : ApiController
    {
        private readonly ProductDA _da;

        /// <summary>
        /// Construstoer for products controller
        /// </summary>
        public ProductsController()
        {
            _da = new ProductDA();
        }

        #region Get UserID or CustomerID from token.

        /// <summary>
        /// Get the values from user claims to be used in project
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal long GetClaimsValue(string type)
        {
            string claim = type == "U" ? "UserID" : "CustomerID";
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            return Convert.ToInt64(principal.Claims.Where(c => c.Type == claim).Single().Value);
        }

        #endregion Get UserID or CustomerID from token.

        #region Get product Categories / sub categories

        /// <summary>
        ///
        /// </summary>
        /// <param name="apiData">{"showSubOnly":false}</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ProductCategories")]
        public async Task<APiResponse> GetProductCategories(JObject apiData)
        {
            dynamic obj = apiData;
            long listCustomerId = Convert.ToInt64(obj.listCustomerId);
            bool showSubOnly = Convert.ToBoolean(obj.showSubOnly);
            if (listCustomerId < 1)
            {
                listCustomerId = GetClaimsValue("C");
            }
            APiResponse res = await _da.GetProductCategories(showSubOnly, listCustomerId);

            return res;
        }

        #endregion Get product Categories / sub categories

        #region Get product Filters

        /// <summary>
        /// Get default product filters
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [Route("ProductFilters")]
        public async Task<APiResponse> GetProductFilters()
        {
            APiResponse res = await _da.GetProductFilters();

            return res;
        }

        #endregion Get product Filters

        #region Search products by filters

        /// <summary>
        /// Search products by filters
        /// </summary>
        /// <param name="searchFilters">SearchFilter Model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchProducts")]
        public async Task<APiResponse> GetProductsByFilters(ProductFilters searchFilters)
        {
            long userID = GetClaimsValue("U");
            searchFilters.UserID = userID;
            APiResponse res = await _da.GetProductsByFilters(searchFilters);
            return res;
        }

        #endregion Search products by filters

        #region GetPantryListByCustomer

        /// <summary>
        /// Fetch the default pantry lists for a customer.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCustomerPantry")]
        public async Task<APiResponse> GetPantryListByCustomerID(GetCustomerpantryListModel model)
        {
            return await _da.GetPantryListByCustomerID(model.CustomerID);
        }

        #endregion GetPantryListByCustomer

        #region Insert/Update item into user favorite/pantry list

        /// <summary>
        /// Insert item into user Pantry/favorite list
        /// </summary>
        /// <param name="obj">Model for PantryListItem</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("AddUpdatePantry")]
        public async Task<APiResponse> AddUpdatePantryList(PantryListBO obj)
        {
            obj.CreatedBy = GetClaimsValue("U");
            obj.IsSynced = false;
            obj.IsSorted = false;
            APiResponse res = await _da.AddUpdatePantryList(obj);
            return res;
        }

        #endregion Insert/Update item into user favorite/pantry list

        #region Add Items to Pantry list

        /// <summary>
        /// Add item to a pantry list.
        /// </summary>
        /// <param name="obj">Bo for Pantry</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("AddItemsToPantry")]
        public async Task<APiResponse> AddPantryListItems(PantryItemsBO obj)
        {
            if (obj.CustomerID < 1)
            {
                obj.CustomerID = GetClaimsValue("C");
            }
            APiResponse res = await _da.AddItemsToPantry(obj);
            return res;
        }

        #endregion Add Items to Pantry list

        #region Delete Item from Pantry list

        /// <summary>
        /// Delete item from a pantry list.
        /// </summary>
        /// <param name="obj">Bo for Pantry</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("DeleteItemFromPantry")]
        public async Task<APiResponse> DeletePantryListItem(DeleteFavoriteListItemBO obj)
        {
            obj.CustomerID = GetClaimsValue("C");
            APiResponse res = await _da.DeleteItemFromPantry(obj);
            return res;
        }

        #endregion Delete Item from Pantry list

        #region Get Product by bar Code

        /// <summary>
        /// Get Product by Bar Code
        /// </summary>
        /// <param name="obj"> </param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetProuctByBarCode")]
        public async Task<APiResponse> ProductByBarcode(ProductByBarcode obj)
        {
            return await _da.GetProductByBarcode(obj);
        }

        #endregion Get Product by bar Code

        #region Get items in Pantry

        /// <summary>
        /// Get items in a pantry list
        /// </summary>
        /// <param name="obj">The CustomerID and SortBy can be left out from the Request. </param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetPantryItems")]
        public async Task<APiResponse> GetPantryListItems(PantryFilters obj)
        {
            //if (!obj.IsRepUser)
            //{
            //    obj.CustomerID = GetClaimsValue("C");
            //}
            obj.UserID = GetClaimsValue("U");

            return await _da.GetPantryListItems(obj);
        }

        #endregion Get items in Pantry

        #region Create and insert items into temp cart.

        /// <summary>
        /// Create temp cart and insert items into it.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("InsertUpdateTempCart")]
        public async Task<APiResponse> InsertTempCart(TempCartBO model)
        {
            if (model.RepUserID == null || model.RepUserID == 0)
            {
                model.RepUserID = GetClaimsValue("U");
            }
            APiResponse res = await _da.InsertTempCart(model);
            return res;
        }

        #endregion Create and insert items into temp cart.

        #region Update cart items Data.

        /// <summary>
        ///updates the data into the temo cart or Saved order cart
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("UpdateCartItem")]
        public async Task<APiResponse> UpdateCartItemData(TempCartItems model)
        {
            APiResponse res = await _da.UpdateCartItemData(model);
            return res;
        }

        #endregion Update cart items Data.

        #region Fetch temp cart items for listing

        /// <summary>
        /// Get items in temp cart
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetTempCartItems")]
        public async Task<APiResponse> GetTempCartItems(GetCartBO model)
        {
            APiResponse res = await _da.GetTempCart(model);

            return res;
        }

        #endregion Fetch temp cart items for listing

        #region Get Latest SPecials for customer

        /// <summary>
        /// Get Latest special items
        /// </summary>
        /// <param name="model">{"CustomerID":7,"PageIndex":0,"PageSize":10}</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetLatestSpecials")]
        public async Task<APiResponse> GetLatestSpecials(ProductFilters model)
        {
            model.UserID = GetClaimsValue("U");
            model.CustomerID = GetClaimsValue("C");
            APiResponse res = await _da.GetLatestSpecials(model);

            return res;
        }

        #endregion Get Latest SPecials for customer

        #region Delete favorite list

        /// <summary>
        /// Deletes a favorite list for customer
        /// </summary>
        /// <param name="model">Pass the PantryListID to be deleted</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteFavoriteList")]
        public async Task<APiResponse> DeleteFavoriteList(DeleteFavoriteBO model)
        {
            APiResponse res = await _da.DeleteFavoriteList(model);

            return res;
        }

        #endregion Delete favorite list

        #region Sort pantry list items

        /// <summary>
        /// Sort pantry list items
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetPantryItemsSortOrder")]
        public async Task<APiResponse> SetPantryItemsSortOrder(PantrySortBO model)
        {
            APiResponse res = await _da.SetPantryItemsSortOrder(model);

            return res;
        }

        #endregion Sort pantry list items

        #region Send Item enquiry Email

        /// <summary>
        /// Send Item Enquiry email
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SendItemEnquiry")]
        public async Task<APiResponse> ItemEnquiry(ItemEnquiryBO model)
        {
            model.UserID = GetClaimsValue("U");
            model.CustomerID = GetClaimsValue("C");
            APiResponse res = await _da.ItemEnquiry(model);

            return res;
        }

        #endregion Send Item enquiry Email

        #region Get product Details By ProductID

        /// <summary>
        /// Get the details of a selected product.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetProductDetailByID")]
        public async Task<APiResponse> GetProductDetail(ProductDetailBO model)
        {
            //if (!model.IsRepUser)
            //{
            //    model.CustomerID = GetClaimsValue("C");
            //}
            model.UserID = GetClaimsValue("U");

            APiResponse res = await _da.GetProductDetails(model);

            return res;
        }

        #endregion Get product Details By ProductID

        #region Send special product request

        /// <summary>
        /// Send request to admin for product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SendSpecialProductRequest")]
        public async Task<APiResponse> SendSpecialProductRequest(SpecialProductReqBO model)
        {
            APiResponse res = await _da.SendSpecialProductRequest(model);

            return res;
        }

        #endregion Send special product request

        #region Get customer Suggestive and specials items

        /// <summary>
        /// Get customer Suggestive and specials items
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetSuggestiveItems")]
        [AllowAnonymous]
        public async Task<APiResponse> GetSuggestiveItems(SuggestiveItemsModel model)
        {
            //model.UserID = GetClaimsValue("U");
            //model.CustomerID = GetClaimsValue("C");
            APiResponse res = await _da.GetSuggestiveItems(model);

            return res;
        }

        #endregion Get customer Suggestive and specials items
    }
}