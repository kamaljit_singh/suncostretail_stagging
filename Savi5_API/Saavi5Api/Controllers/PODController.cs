﻿using CommonFunctions;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.DA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace Saavi5Api.Controllers
{
    /// <summary>
    /// Controller with methods for Saavi Proof of Delivery Application
    /// </summary>
    //[Authorize]
    [RoutePrefix("api/POD")]
    public class PODController : ApiController
    {
        private readonly PodDA _da;

        public PODController()
        {
            _da = new PodDA();
        }

        #region Get DriverID from token.

        /// <summary>
        /// Get the values from user claims to be used in project
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal long GetClaimsValue(string type)
        {
            string claim = type == "U" ? "UserID" : "CustomerID";
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            return Convert.ToInt64(principal.Claims.Where(c => c.Type == claim).Single().Value);
        }

        #endregion Get DriverID from token.

        #region Get all vehicles from the database.

        /// <summary>
        /// Get all vehicles from the database.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetVehicles")]
        public async Task<IHttpActionResult> GetVehicles()
        {
            var driverID = GetClaimsValue("U");
            var res = await _da.GetVehiclesRunNo(driverID);
            return Ok(res);
        }

        #endregion Get all vehicles from the database.

        #region Fill the vehicle checklist.

        /// <summary>
        /// Fill the checklist for the Vehicle
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("FillVehicleChecklist")]
        public async Task<IHttpActionResult> FillVehicleChecklist(VehicleChecklistBO model)
        {
            var res = await _da.FillVehicleChecklist(model);
            return Ok(res);
        }

        #endregion Fill the vehicle checklist.

        #region Get delivery Days

        /// <summary>
        /// Get delivery days for driver deliveries
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("GetDeliveryDays")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDeliveryDays()
        {
            var userID = 0;
            var res = await _da.GetDeliveryDays(userID);
            return Ok(res);
        }

        #endregion Get delivery Days

        #region Get deliveries by driverID

        /// <summary>
        /// Get deliveries based on DriverID
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDriverDeliveries")]
        public async Task<IHttpActionResult> GetOrdersForDriver(GetDeliveriesModel model)
        {
            var res = await _da.GetOrdersForDriver(model);
            return Ok(res);
        }

        #endregion Get deliveries by driverID

        #region Get Delivery details for an Order

        /// <summary>
        /// Get delivery details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDeliveryDetails")]
        public async Task<IHttpActionResult> GetDeliveryDetails(DeliveryDetailModel model)
        {
            var res = await _da.GetDeliveryDetails(model);
            return Ok(res);
        }

        #endregion Get Delivery details for an Order

        #region Set Delivery Sort Order

        /// <summary>
        /// Set Sort order for Deliveries based on driver preference.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetDeliverySortOrder")]
        public async Task<IHttpActionResult> SetDeliverySortOrder(DeliverySortBO model)
        {
            var res = await _da.SetDeliverySortOrder(model);
            return Ok(res);
        }

        #endregion Set Delivery Sort Order

        #region Insert, update Delivery status

        /// <summary>
        /// Insert Update delivery run status.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("StartUpdateDelivery")]
        public async Task<IHttpActionResult> StartUpdateDelivery(StartUpdateDeliveryModel model)
        {
            var res = await _da.StartUpdateDelivery(model);
            return Ok(res);
        }

        #endregion Insert, update Delivery status

        #region Send Message to the User who placed the order.

        /// <summary>
        /// Send message to the User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SendMessage")]
        public async Task<IHttpActionResult> SendMessage(SendMessageModel model)
        {
            model.DriverID = GetClaimsValue("U");
            var res = await _da.SendMessage(model);
            return Ok(res);
        }

        #endregion Send Message to the User who placed the order.

        #region Add tracking data

        /// <summary>
        /// Save tracking Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddTrackingData")]
        public async Task<IHttpActionResult> AddTrackingData(TrackingModel model)
        {
            var res = await _da.AddTrackingData(model);
            return Ok(res);
        }

        #endregion Add tracking data

        #region Add images as multipart

        /// <summary>
        /// Save tracking Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadImages")]
        public async Task<IHttpActionResult> UploadImages()
        {
            string deliveryNo = "";
            long deliveryID = 0;
            bool isSignature = false;
            string productid = "";

            var context = HttpContext.Current.Request;
            var imagesLi = new List<DeliveryImagesArray>();
            if (context.Form.Count > 0)
            {
                isSignature = context.Form["IsSignature"].To<bool>();
                deliveryID = context.Form["DeliveryID"].To<long>();
                deliveryNo = context.Form["DeliveryNo"].ToString();
                try
                {
                    productid = context.Form["ProductID"].ToString();
                }
                catch
                {
                }
            }

            if (context.Files.Count > 0)
            {
                for (int i = 0; i < context.Files.Count; i++)
                {
                    var di = new DeliveryImagesArray();
                    string fileName = Guid.NewGuid().ToString() + ".jpg";

                    if (!Directory.Exists(HostingEnvironment.MapPath("~/PODImages/" + deliveryNo)))
                    {
                        Directory.CreateDirectory(HostingEnvironment.MapPath("~/PODImages/" + deliveryNo));
                    }
                    string filePath = HostingEnvironment.MapPath("~/PODImages/" + deliveryNo + "/" + fileName);

                    context.Files[i].SaveAs(filePath);

                    di.ImageName = fileName;
                    di.IsSignature = isSignature;
                    di.DeliveryID = deliveryID;
                    if (productid != "")
                    {
                        di.ProductID = productid.To<long>();
                    }
                    else
                    {
                        di.ProductID = 0;
                    }
                    imagesLi.Add(di);
                }
            }

            var res = await _da.UploadDeliveryImages(imagesLi);

            return Ok(res);
        }

        #endregion Add images as multipart

        #region Update settings

        /// <summary>
        /// Update settings
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSettings")]
        public async Task<IHttpActionResult> UpdateSettings(SettingsBO model)
        {
            var res = await _da.UpdateSettings(model);
            return Ok(res);
        }

        #endregion Update settings

        #region Delivery Run Images

        /// <summary>
        /// Get the list of images for a particular delivery
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetImagesForDelivery")]
        public async Task<IHttpActionResult> GetImagesForDelivery(GalleryBO model)
        {
            var res = await _da.GetImagesForDelivery(model);
            return Ok(res);
        }

        #endregion Delivery Run Images

        #region Add driver Image as multipart

        /// <summary>
        /// Save tracking Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadDriverImage")]
        public async Task<IHttpActionResult> UploadDriverImage()
        {
            long driverID = 0;
            string imageName = "";

            var context = HttpContext.Current.Request;
            var image = new DriverImageBO();
            if (context.Form.Count > 0)
            {
                driverID = context.Form["DriverID"].To<long>();
                imageName = context.Form["ImageName"].ToString();
            }

            if (context.Files.Count > 0)
            {
                for (int i = 0; i < context.Files.Count; i++)
                {
                    string fileName = "";
                    if (string.IsNullOrEmpty(imageName))
                    {
                        fileName = Guid.NewGuid().ToString() + ".jpg";
                    }
                    else
                    {
                        fileName = imageName;
                    }

                    if (!Directory.Exists(HostingEnvironment.MapPath("~/PODImages")))
                    {
                        Directory.CreateDirectory(HostingEnvironment.MapPath("~/PODImages"));
                    }
                    string filePath = HostingEnvironment.MapPath("~/PODImages/" + fileName);

                    context.Files[i].SaveAs(filePath);

                    image.ImageName = fileName;
                    image.DriverID = driverID;
                }
            }

            var res = await _da.UploadDriverImage(image);

            return Ok(res);
        }

        #endregion Add driver Image as multipart

        #region Update online Status

        /// <summary>
        /// Update Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateStatus")]
        public async Task<IHttpActionResult> UpdateOnlineStatus(StatusBO model)
        {
            var res = await _da.UpdateOnlineStatus(model);
            return Ok(res);
        }

        #endregion Update online Status

        #region Get All Drivers

        /// <summary>
        /// Get all the drivers for Admin user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllDrivers")]
        public async Task<IHttpActionResult> GetAllDrivers()
        {
            var res = await _da.GetAllDrivers();
            return Ok(res);
        }

        #endregion Get All Drivers

        #region Assign deliveries to Driver

        /// <summary>
        /// Assign deliveries to a different Driver
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("AssignDeliveriesToDriver")]
        public async Task<IHttpActionResult> AssignDeliveriesToDriver(AssignDeliveriesBO model)
        {
            var res = await _da.AssignDeliveriesToDriver(model);
            return Ok(res);
        }

        #endregion Assign deliveries to Driver

        #region Update online Status

        /// <summary>
        /// Update Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateScanMode")]
        public async Task<IHttpActionResult> UpdateScanMode(ScanBO model)
        {
            var res = await _da.UpdateScanMode(model);
            return Ok(res);
        }

        #endregion Update online Status

        #region Get Runs / Vehicles

        /// <summary>
        /// Gets the Runs and Vehicles details for initial POD screen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRunVehivle")]
        public async Task<IHttpActionResult> GetRunVehicle()
        {
            var res = await _da.GetRunVehicle();
            return Ok(res);
        }

        #endregion Get Runs / Vehicles

        #region Set Runs / Vehicles

        /// <summary>
        /// Sets the Runs and Vehicles details for initial POD screen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetRunVehivle")]
        public async Task<IHttpActionResult> SetRunVehicle(SetRunVehivleBO model)
        {
            var res = await _da.SetRunVehicle(model);
            return Ok(res);
        }

        #endregion Set Runs / Vehicles

        #region Run Manual Sync for specific run and driver

        /// <summary>
        /// Run Manual SYnc for specific run and driver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RunManualSync")]
        public async Task<IHttpActionResult> RunManualSync(RunManualSyncBO model)
        {
            var res = await _da.RunManualSync(model);
            return Ok(res);
        }

        #endregion Run Manual Sync for specific run and driver

        #region Generate PDF with RUN data for driver

        /// <summary>
        /// Generate PDF with RUN data for driver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GenerateDeliveryPDF")]
        public async Task<IHttpActionResult> GenerateDeliveryPDF(GeneratePDFBO model)
        {
            var res = await _da.GenerateDeliveryPDF(model);
            return Ok(res);
        }

        #endregion Generate PDF with RUN data for driver

        #region Fetch truck complaince

        /// <summary>
        /// Fetch truck compliance data using checklist ID
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("FetchTruckCompliance")]
        public async Task<IHttpActionResult> FetchTruckCompliance(GetTruckCompliance model)
        {
            var res = await _da.FetchTruckCompliance(model);
            return Ok(res);
        }

        #endregion Fetch truck complaince

        #region Add driver comment to delivery status master

        /// <summary>
        /// Add driver comment to delivery status master
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddDriverComment")]
        public async Task<IHttpActionResult> AddDriverComment(DriverComment model)
        {
            var res = await _da.AddDriverComment(model);
            return Ok(res);
        }

        #endregion Add driver comment to delivery status master

        #region Demo code

        /// <summary>
        /// Demo code to generate pdf for test. NOT TO BE USED IN MOBILE APPS
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GenerateDeliverySummaryPDF")]
        public async Task<IHttpActionResult> GenerateDeliverySummaryPDF()
        {
            var res = _da.GenerateDeliverySummaryPDF(7539, null);
            //var res = _da.DeliveryConfirmationPDF("5397834", 10573);
            return Ok();
        }

        #endregion Demo code

        #region Send invoice in email

        /// <summary>
        /// Demo code to generate pdf
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SendInvoiceToEmail")]
        public async Task<IHttpActionResult> SendInvoiceToEmail(SendInvoiceBO model)
        {
            var res = await _da.SendInvoiceToEmail(model);
            return Ok(res);
        }

        #endregion Send invoice in email
    }
}