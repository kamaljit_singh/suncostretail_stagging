﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.DA;
using Saavi5Api.DataAccess.Model;
using Saavi5Api.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Http;
using CommonFunctions;

namespace Saavi5Api.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrderController : ApiController
    {
        private readonly OrdersDA _da;

        /// <summary>
        /// Orders and related items controlles
        /// </summary>
        public OrderController()
        {
            _da = new OrdersDA();
        }

        #region Get UserID or CustomerID from token.

        /// <summary>
        /// Get the values from user claims to be used in project
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal long GetClaimsValue(string type)
        {
            string claim = type == "U" ? "UserID" : "CustomerID";
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            return Convert.ToInt64(principal.Claims.Where(c => c.Type == claim).Single().Value);
        }

        #endregion Get UserID or CustomerID from token.

        #region Get customer or products comments

        /// <summary>
        /// Get all customer comments
        /// </summary>
        /// <param name="model">request object for the service</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetComments")]
        public async Task<IHttpActionResult> GetComments(GetComments model)
        {
            var res = await _da.GetComments(model);
            return Ok(res);
        }

        #endregion Get customer or products comments

        #region Add a comment for customer or a product

        /// <summary>
        /// Add a new customer comment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("AddComments")]
        public async Task<IHttpActionResult> AddComments(AddCommentBO model)
        {
            var res = await _da.AddComments(model);

            return Ok(res);
        }

        #endregion Add a comment for customer or a product

        #region Delete Customer or Product comments

        /// <summary>
        /// Delete comment
        /// </summary>
        /// <param name="model">Delete modal</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("DeleteComment")]
        public async Task<IHttpActionResult> DeleteCustomerComment(DeleteCommentBO model)
        {
            var res = await _da.DeleteComment(model);
            return Ok(res);
        }

        #endregion Delete Customer or Product comments

        #region Place order service

        /// <summary>
        /// Save order/ update order/ Place Order
        /// </summary>
        /// <param name="model">OrderStatur = 0 in order to Save/Update  an order. OrderStatur = 1 in order to Place an order.</param>
        /// <returns>
        /// OrderID of the placed order.
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("PlaceOrder")]
        public async Task<IHttpActionResult> PlaceOrder(PlaceOrderBO model)
        {
            //model.UserID = GetClaimsValue("U");
            //model.CustomerID = GetClaimsValue("C");
            var res = await _da.PlaceOrder(model);
            return Ok(res);
        }

        #endregion Place order service

        #region Get order Details by ID

        /// <summary>
        /// Get the saved order by orderID
        /// </summary>
        /// <param name="model"></param>
        /// <returns>
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetOrderDetails")]
        public async Task<IHttpActionResult> GetOrderDetailsAsync(OrderDetailsBO model)
        {
            //model.UserID = GetClaimsValue("U");
            //model.CustomerID = GetClaimsValue("C");

            var res = await _da.GetOrderDetailsCart(model);
            return Ok(res);
        }

        #endregion Get order Details by ID

        #region Delete saved order

        /// <summary>
        /// Enter the order id to be deleted
        /// OrderID == CartID
        /// </summary>
        /// <param name="model"></param>
        /// <returns>
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("DeleteSavedOrder")]
        public async Task<IHttpActionResult> DeleteSavedOrder(DeleteSavedOrder model)
        {
            //dynamic obj = orderID;
            //long oID = Convert.ToInt64(obj.orderID);
            var res = await _da.DeleteOrder(model);
            return Ok(res);
        }

        #endregion Delete saved order

        #region Delete order items

        /// <summary>
        /// Delete an item from the cart
        /// </summary>
        /// <param name="model">Delete model </param>
        /// <returns>
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("DeleteCartItems")]
        public async Task<IHttpActionResult> DeleteCartItem(DeleteCartItemBO model)
        {
            var res = await _da.DeleteCartItems(model);
            return Ok(res);
        }

        #endregion Delete order items

        #region Get Order History

        /// <summary>
        /// List of orders for the customer
        /// </summary>
        /// <returns>
        /// List of orders placed for this customer
        /// </returns>
        [HttpPost]
        [Route("GetOrderHistory")]
        public async Task<IHttpActionResult> GetOrderHistory(GetCustomerpantryListModel model)
        {
            var res = await _da.GetOrderHistory(model.CustomerID);
            return Ok(res);
        }

        #endregion Get Order History

        #region Get Payment Orders

        /// <summary>
        /// List of customer orders for payment
        /// </summary>
        /// <returns>
        /// List of customer orders for payment
        /// </returns>
        [HttpPost]
        [Route("GetPaymentOrders")]
        public async Task<IHttpActionResult> GetPaymentOrders(GetPaymentOrdersModel model)
        {
            var res = await _da.GetPaymentOrders(model.token);
            return Ok(res);
        }

        #endregion Get Payment Orders

        #region Save Past Order Payment Details

        /// <summary>
        /// Save Past Order Payment Details
        /// </summary>
        /// <returns>
        /// </returns>
        [HttpPost]
        [Route("SavePastOrderPayment")]
        public async Task<IHttpActionResult> SavePastOrderPayment(SavePastOrderPaymentModel model)
        {
            var res = await _da.SavePastOrderPayment(model);
            return Ok(res);
        }

        #endregion Save Past Order Payment Details

        #region Get Standing Orders

        /// <summary>
        /// List of standing orders for the customer
        /// </summary>
        /// <returns>
        /// List of standing orders placed for this customer
        /// </returns>
        [HttpPost]
        [Route("GetStandingOrders")]
        public async Task<IHttpActionResult> GetStandingOrders(GetCustomerpantryListModel model)
        {
            var res = await _da.GetOrderHistory(model.CustomerID);
            return Ok(res);
        }

        #endregion Get Standing Orders

        #region Cancel Standing Order

        [HttpPost]
        [Route("CancelStandingOrder")]
        public async Task<IHttpActionResult> CancelStandingOrder(StandingOrderModel model)
        {
            var res = await _da.CancelStandingOrder(model.CartID, model.CustomerID);
            return Ok(res);
        }

        #endregion Cancel Standing Order

        #region Generate Invoice and send email.

        /// <summary>
        /// Generate PDF invoice
        /// </summary>
        /// <returns>
        /// List of orders placed for this customer
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GenerateInvoice")]
        public IHttpActionResult GenerateInvoice(OrderDetailsBO obj)
        {
            //obj.CustomerID = GetClaimsValue("C");
            //obj.UserID = GetClaimsValue("U");

            _da.SendOrderMail(obj);
            return Ok();
        }

        #endregion Generate Invoice and send email.

        #region Get pdf invoices to display or send to email.

        /// <summary>
        /// If IsPdfListing = true, then show the listing of the invoices
        /// Else send the details to the email provided if the email is valid.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetInvoices")]
        public async Task<IHttpActionResult> GetInvoices(GetInvoicesBO model)
        {
            model.CustomerID = GetClaimsValue("C");
            model.UserID = GetClaimsValue("U");

            var res = await _da.GetInvoices(model);
            return Ok(res);
        }

        #endregion Get pdf invoices to display or send to email.

        #region Append order to cart.

        /// <summary>
        /// Append Placed order to existing cart or saved order. Depending on the options.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>
        /// Response messages
        /// </returns>
        [HttpPost]
        [Route("AppendOrderToTempCartForReorder")]
        public async Task<IHttpActionResult> AppendOrderToTempCart(ReorderBO model)
        {
            //model.CustomerID = GetClaimsValue("C");
            //model.UserID = GetClaimsValue("U");

            var res = await _da.AppendOrderToTempCart(model);
            return Ok(res);
        }

        #endregion Append order to cart.

        #region Get count for items in a specific cart

        /// <summary>
        /// Get count for items in a specific cart
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCartCount")]
        public IHttpActionResult GetCartCount(CartCount cart)
        {
            var res = _da.GetCartCount(cart);
            return Ok(res);
        }

        #endregion Get count for items in a specific cart

        #region Get App Discount

        /// <summary>
        /// Get App Discount
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAppDiscount")]
        public IHttpActionResult GetAppDiscount()
        {
            var res = _da.GetAppDiscount();
            return Ok(res);
        }

        #endregion Get App Discount

        #region Get Profit Limit

        /// <summary>
        /// Get Profit Limit
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetProfitLimit")]
        public IHttpActionResult GetProfitLimit()
        {
            var res = _da.GetProfitLimit();
            return Ok(res);
        }

        #endregion Get Profit Limit

        #region Apply discount coupon

        /// <summary>
        /// Apply discount coupon
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("ApplyCoupon")]
        public IHttpActionResult ApplyCoupon(CouponBo model)
        {
            var res = _da.ApplyCoupon(model);
            return Ok(res);
        }

        #endregion Apply discount coupon

        #region Send Order Payment Link

        /// <summary>
        /// Apply discount coupon
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("SendOrderPaymentLink")]
        public IHttpActionResult SendOrderPaymentLink(OrderPaymentLinkRequestModel model)
        {
            var res = _da.SendOrderPaymentLink(model);
            return Ok(res);
        }

        #endregion Send Order Payment Link

        #region Log Errors to server for order posting

        private void LogErrorsToServer(string ex, string cusOrderID, string type)
        {
            using (var _db = new Saavi5Entities())
            {
                trnsCartResponseMaster cr = new trnsCartResponseMaster();
                cr.CartId = cusOrderID.To<long>();
                cr.MethodName = type;
                cr.Response = ex;
                cr.CreatedDate = DateTime.Now;

                _db.trnsCartResponseMasters.Add(cr);
                _db.SaveChanges();
            }
        }

        #endregion Log Errors to server for order posting

        #region Save / Update Recurring orders

        /// <summary>
        /// Save / Update recurring orders for a customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("ManageRecurringOrders")]
        public IHttpActionResult ManageRecurringOrders(RecurringOrdersBO model)
        {
            var res = _da.ManageRecurringOrders(model);
            return Ok(res);
        }

        #endregion Save / Update Recurring orders

        #region Get recurring order

        /// <summary>
        /// Get recurring order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetRecurringOrders")]
        public IHttpActionResult GetRecurringOrders(GetRecurringOrderBO model)
        {
            var res = _da.GetRecurringOrders(model);
            return Ok(res);
        }

        #endregion Get recurring order

        #region Delete recurring order item

        /// <summary>
        /// Delete recurring order item for a customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("DeleteRecurrOrderItem")]
        public IHttpActionResult DeleteRecurrOrderItem(DeleteRecurrItemBO model)
        {
            var res = _da.DeleteRecurrOrderItem(model);
            return Ok(res);
        }

        #endregion Delete recurring order item
    }
}