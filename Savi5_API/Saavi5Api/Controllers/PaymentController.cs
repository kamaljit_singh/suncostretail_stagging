﻿using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.DA;
using System.Threading.Tasks;
using System.Web.Http;

namespace Saavi5Api.Controllers
{
    /// <summary>
    /// Payment Controller
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Payment")]
    public class PaymentController : ApiController
    {
        private readonly PaymentDetailDA _da;
        ///<summary>
        ///Payment Management controller
        /// </summary>
        public PaymentController()
        {
            _da = new PaymentDetailDA();
        }

        #region Get Payment Token
        /// <summary>
        /// Get Payment Token for front end App transactions
        ///  </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetPaymentToken")]
        public async Task<IHttpActionResult> GetPaymentToken(PaymentTokenModel model)
        {
            var res = await _da.GetPaymentToken(model);
            return Ok(res);
        }
        #endregion Get Payment Token

        #region Subscription Payment Methods
        /// <summary>
        /// Cancel Subscription
        ///  </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CancelSubscription")]
        public async Task<IHttpActionResult> CancelSubscription(CancelRecurringpayment model)
        {
            var res = await _da.CancelSubscription(model);
            return Ok(res);
        }
        /// <summary>
        /// Create Subscription
        ///  </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateSubscription")]
        public async Task<IHttpActionResult> CreateSubscription(SubscriptionDetail model)
        {
            var res = await _da.CreateSubscription(model);
            return Ok(res);
        }
        /// <summary>
        /// Log Subscription Payment response received in front end
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveSubcriptionPaymentLog")]
        public async Task<IHttpActionResult> StripeSubcriptionLogMaster(SubcriptionLogMasterModel model)
        {
            _da.StripeSubcriptionLogMaster(model.payResponse.ToString(), model.customerId,"", model.recurringId,"","", model.price);
            return Ok("Done");
        }

        #endregion Cancel Subscription

        #region Get Payment Token
        /// <summary>
        /// Get Payment Token for front end App transactions
        ///  </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("GetPaymentTokenForPastOrders")]
        public async Task<IHttpActionResult> GetPaymentTokenForPastOrders(PaymentTokenForPastOrdersModel model)
        {
            var res = await _da.GetPaymentToken(model);
            return Ok(res);
        }
        #endregion Get Payment Token

        #region Get Stripe Public Keys
        /// <summary>
        /// Get Stripe Public Keys
        ///  </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetStripePublicKeys")]
        public async Task<IHttpActionResult> GetStripePublicKeys()
        {
            var res = await _da.GetStripePublicKeys();
            return Ok(res);
        }
        #endregion Get Stripe Public Keys

        #region Log Payment
        /// <summary>
        /// Log Payment response received in front end
        ///  </summary>
        /// <param name="model"></param>
        /// <param name="tempCartID"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LogPayment")]
        public async Task<IHttpActionResult> LogPayment(LogPaymentModel model)
        {
            _da.LogPayment(model.PayResponse.ToString(), (-1 * model.TempCartID));
            return Ok("Done");
        }
        #endregion Log Payment

    }
}