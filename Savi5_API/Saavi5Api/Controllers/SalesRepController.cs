﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.DA;
using Saavi5Api.DataAccess.Model;
using Saavi5Api.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Http;
using CommonFunctions;

namespace Saavi5Api.Controllers
{
    /// <summary>
    /// Sales Rep Controller
    /// </summary>
    [RoutePrefix("api/Rep")]
    public class SalesRepController : ApiController
    {
        private readonly SalesRepDA _da;

        public SalesRepController()
        {
            _da = new SalesRepDA();
        }

        #region Get UserID or CustomerID from token.
        /// <summary>
        /// Get the values from user claims to be used in project
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal long GetClaimsValue(string type)
        {
            string claim = type == "U" ? "UserID" : "CustomerID";
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            return Convert.ToInt64(principal.Claims.Where(c => c.Type == claim).Single().Value);
        }
        #endregion

        #region Get Sales Rep customers
        /// <summary>
        /// Get Customers belonging to a sales Rep
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetRepCustomers")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetRepCustomers(GetRepCustomersModel model)
        {
            model.UserID = GetClaimsValue("U");
            var res = await _da.GetRepCustomers(model);
            return Ok(res);
        }
        #endregion

        #region Rep Customer Pantry Lists
        /// <summary>
        /// Get pantry lists for a selected customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RepCustomerPantryLists")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetRepCustomerPantryLists(GetCustomerpantryListModel model)
        {
            var res = await _da.GetRepCustomerPantryLists(model);
            return Ok(res);
        }
        #endregion

        #region Get Product Details for SalesREP
        /// <summary>
        /// Get details of product for rep.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetRepProductDetails")]
        public async Task<IHttpActionResult> GetProductDetails(GetRepProductDetailModel model)
        {
            var res = await _da.GetProductDetails(model);
            return Ok(res);
        }
        #endregion

        #region Add customer Special Price
        /// <summary>
        /// Add a special price
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSpecialPrice")]
        public async Task<IHttpActionResult> AddSpecialPrice(AddSpecialPriceBO model)
        {
            var res = await _da.AddSpecialPrice(model);
            return Ok(res);
        }
        #endregion

        #region Save Sales Rep Location
        /// <summary>
        /// Save Sales Rep Location
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveLocation")]
        public async Task<IHttpActionResult> SaveLocation(UserLocationBO model)
        {
            var res = await _da.SaveLocation(model);
            return Ok(res);
        }
        #endregion

        #region Get Order History
        /// <summary>
        /// List of orders for the customer
        /// </summary>
        /// <returns>
        /// List of orders placed for this customer
        /// </returns>
        [HttpPost]
        [Route("GetOrderHistoryRep")]
        public async Task<IHttpActionResult> GetOrderHistoryRep(RepOrderHistoryBO model)
        {
            var res = await _da.GetOrderHistoryRep(model);
            return Ok(res);
        }
        #endregion

        #region Add order and unload comments
        /// <summary>
        /// Add comments to the order
        /// </summary>
        /// <returns>
        /// Comment ID's
        /// </returns>
        [HttpPost]
        [Route("AddCommentsRep")]
        public async Task<IHttpActionResult> AddCommentsRep(List<AddCommentBO> model)
        {
            var res = await _da.AddCommentsRep(model);
            return Ok(res);
        }
        #endregion

        #region Get Customer addresses
        /// <summary>
        /// Get customer addresses
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetCustomerAddressesRep")]
        public async Task<IHttpActionResult> GetCustomerAddressesRep(DeliveryAddressRepBO model)
        {
            var res = await _da.GetCustomerDeliveryAddresssRep(model);
            return Ok(res);
        }
        #endregion

        #region Get Cart Count + Total
        /// <summary>
        /// Get cart count and total cart value
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCartCountRep")]
        public async Task<IHttpActionResult> GetCartCountRep(CartCount model)
        {
            var res = await _da.GetCartCountRep(model);
            return Ok(res);
        }
        #endregion

    }
}
