﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using Saavi5Api.Security;

[assembly: OwinStartup(typeof(Saavi5Api.Startup))]

namespace Saavi5Api
{
	public class Startup
	{
		public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
		public void Configuration(IAppBuilder app)
		{
			HttpConfiguration config = new HttpConfiguration();
			ConfigureOAuth(app);

			WebApiConfig.Register(config);
			app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
			app.UseWebApi(config);
		}

		public void ConfigureOAuth(IAppBuilder app)
		{
			//use a cookie to temporarily store information about a user logging in with a third party login provider
			OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

			OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
			{
				AllowInsecureHttp = true,
				TokenEndpointPath = new PathString("/token"),
				AccessTokenExpireTimeSpan = TimeSpan.FromHours(4),
				Provider = new AuthorizationServerProvider(),

			};

			// Token Generation
			app.UseOAuthAuthorizationServer(OAuthServerOptions);
			app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
			{
				AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
				AuthenticationType = "Bearer"
			});

		}

        
    }
}
