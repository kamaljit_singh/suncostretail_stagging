﻿using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.Tests
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<UserMaster, UserProfile>().ReverseMap();
                config.CreateMap<CustomerFeaturesMaster, CustomerFeaturesBO>().ReverseMap();
            });
        }
    }
}
