﻿using NUnit.Framework;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.DA;
using AutoMapper;
using Newtonsoft.Json.Linq;

namespace Saavi5Api.Tests.Tests
{
    [TestFixture]
    public class AccountControllerTests : BaseAuthenticatedApiTestFixture
    {
        private string _uri;

        protected override string Uri
        {
            get { return _uri; }
        }

        private readonly Saavi5Entities _db;
        public AccountControllerTests()
        {
            AutoMapperConfig.RegisterMappings();
            _db = new Saavi5Entities();
        }

        [Test]
        public async Task GetDefaultOrderingInfo_Test()
        {
            // Arrange
            _uri = "api/account/GetDefaultOrderingInfo";

            // Act
            var response = await GetAsync();
            var result = await response.Content.ReadAsAsync<APiResponse>();

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("Success", result.Message);
        }

        [Test]
        [TestCase(1)]// Test case with userID = 1
        [TestCase(2)]// Test case with userID = 2
        [TestCase(3)]// Test case with userID = 3
        public async Task GetUserProfile_Test(long userID)
        {
            // Arrange
            _uri = "api/account/GetUserProfile";

            JObject obj = new JObject();
            obj["userID"] = userID;

            // Act
            var response = await PostAsync(obj);
            var res= await response.Content.ReadAsAsync<APiResponse>();
            var data = (JArray)res.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            if (userID < 3)
            {
                Assert.AreEqual("No Data", res.Message);
            }
            else
            {
                Assert.AreEqual(1, data.Count());
            }
        }

        [Test]
        [TestCase(1)]// Test case with userID = 1
        [TestCase(2)]// Test case with userID = 2
        [TestCase(3)]// Test case with userID = 3
        public async Task GetCustomerFeatures_Test(long userID)
        {
            // Arrange
            _uri = "api/account/GetCustomerFeatures";

            JObject obj = new JObject();
            obj["userID"] = userID;

            // Act
            var response = await PostAsync(obj);
            var res = await response.Content.ReadAsAsync<APiResponse>();
            var data = (JObject)res.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            if (userID < 3)
            {
                Assert.AreEqual("No Data", res.Message);
            }
            else
            {
                Assert.AreEqual(31, data.Count);
            }
        }


    }
}
