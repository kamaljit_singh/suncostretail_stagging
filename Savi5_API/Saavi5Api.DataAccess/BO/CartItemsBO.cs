﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class CartItemsBO
    {
        public long ProductID { get; set; }
        public long CustomerID { get; set; }
        public long CartID { get; set; }
        public long CartItemID { get; set; }
        public string ProductCode { get; set; }
        public double? Price { get; set; }
        public double? CompanyPrice { get; internal set; }
        public string ProductName { get; internal set; }
        public string ProductDescription { get; internal set; }
        public string CustomerCode { get; internal set; }
        public string ProductImage { get; internal set; }
        public string ProductThumbImage { get; internal set; }
        public long? UOMID { get; internal set; }
        public string UnitName { get; internal set; }
        public string OrderUnitName { get; internal set; }
        public long OrderUnitId { get; internal set; }
        public string FilterName { get; internal set; }
        public double? ProductWeight { get; internal set; }
        public string WeightName { get; internal set; }
        public string WeightDescription { get; internal set; }
        public string IsPieceOrWeight { get; internal set; }
        public bool? IsAvailable { get; internal set; }
        public bool? IsSpecial { get; internal set; }
        public bool? IsDiscountApplicable { get; internal set; }
        public bool? IsCountrywideRewards { get; internal set; }
        public bool? IsInvoiceComment { get; internal set; }
        public bool? IsUnloadComment { get; internal set; }
        public bool? IsNoPantry { get; internal set; }
        public string ExtDoc { get; internal set; }
        public string RunNo { get; internal set; }
        public bool? IsGST { get; internal set; }
        public string PackagingSequence { get; internal set; }
        public string Brand { get; internal set; }
        public string Supplier { get; internal set; }
        public double SpecialPrice { get; internal set; }
        public DateTime? OrderDate { get; internal set; }
        public DateTime? CreatedDate { get; internal set; }
        public double? StockQuantity { get; internal set; }
        public double? Quantity { get; internal set; }
        public List<DynamicUnitPrices> DynamicUOM { get; set; }
        public PriceBO Prices { get; set; }
        public double? PriceTotal { get; set; }
        public double? MinOQ { get; set; }
        public double? MaxOQ { get; set; }
        public double? UnitsPerCarton { get; set; }
        public long? ProdCommentID { get; internal set; }
        public bool BuyIn { get; set; }
        public bool IsStatusIN { get; set; }
        public double? ProdPrice { get; set; }
        public long? LastOrderUOMID { get; internal set; }
        public bool? SellBelowCost { get; set; }
        public string ProductDescription2 { get; set; }
        public string ProductDescription3 { get; set; }
        public string ProductFeature1 { get; set; }
        public string ProductFeature2 { get; set; }
        public string ProductFeature3 { get; set; }
        public bool ProductIsNew { get; set; }
        public bool ProductIsOnSale { get; set; }
        public bool ProductIsBackSoon { get; set; }
        public bool IsDelivery { get; set; }
        public string ShareURL { get; set; }
        public string ProductFeature { get; set; }
        public List<ProductImages> ProductImages { get; set; }
        public double? TotalWithGST { get; set; }
        public bool? IsDonationBox { get; set; }
    }
}