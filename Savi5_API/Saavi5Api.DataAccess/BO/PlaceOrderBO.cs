﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class PlaceOrderBO
    {
        public long CustomerID { get; set; }
        public long TempCartID { get; set; }
        public long UserID { get; set; }
        public long CartID { get; set; }
        public long? CommentID { get; set; }
        public int? AddressID { get; set; }
        public string PONumber { get; set; }
        public int OrderStatus { get; set; }
        public string Comment { get; set; }
        public DateTime? OrderDate { get; set; }
        public string CutOffTime { get; set; }
        public string ExtDoc { get; set; }
        public string PackagingSequence { get; set; }
        public bool? IsAutoOrdered { get; set; } = false;

        // devire and app information
        public string DeviceToken { get; set; }

        public string DeviceType { get; set; }
        public string DeviceVersion { get; set; }
        public string DeviceModel { get; set; }
        public string AppVersion { get; set; }

        public bool SaveOrder { get; set; }
        public bool IsInvoiceComment { get; set; } = false;
        public bool IsUnloadComment { get; set; } = false;
        public long? InvoiceCommentID { get; set; }
        public long? UnloadCommentID { get; set; } = 0;

        public bool? IsOrderPlpacedByRep { get; set; } = false;

        public string RunNo { get; set; }
        public bool HasFreightCharges { get; set; } = false;
        public bool HasNonDeliveryDayCharges { get; set; } = false;
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public DateTime? StandingOrderStartDate { get; set; }
        public List<int> DaysOfMonth { get; set; } //Day of Month
        public List<int> DaysOfWeek { get; set; } //Day 1 to 7 starting from Sunday
        public bool? IsOrderDaily { get; set; }

        public PayIntent PaymentDetails { get; set; }
        public bool IsCouponApplied { get; set; } = false;

        public string Coupon { get; set; } = "";
        public bool IsLeave { get; set; }
        public bool IsContactless { get; set; }
        public int PickupID { get; set; }
        public bool IsDelivery { get; set; }
        public string DeliveryType { get; set; }
        public double CouponAmount { get; set; }
    }

    public class PayIntent
    {
        public string Status { get; set; }
        public string Id { get; set; }
        public long? Amount { get; set; }
        public string Currency { get; set; }
        public string CustomerId { get; set; }
        public bool Livemode { get; set; }
        public string ReceiptEmail { get; set; }
        public string ClientSecret { get; set; }
    }

    public class OrderItemsBO
    {
        public Int64 CartItemID { get; set; }
        public Int64 CartID { get; set; }
        public Int64 ProductId { get; set; }
        public double Price { get; set; }
        public double BasePrice { get; set; }
        public double SpecialPrice { get; set; }
        public double ProductWeight { get; set; }
        public string WeightName { get; set; }
        public string WeightDescription { get; set; }
        public string IsPieceOrWeight { get; set; }
        public long? CommentID { get; set; }
        public double Quantity { get; set; }
        public bool IsSpecial { get; set; }
        public int UnitId { get; set; }
        public double? UnitsPerCarton { get; set; }
        public bool? IsDiscountApplicable { get; set; }
        public bool? IsBoxDiscount { get; set; }
        public string PriceType { get; set; }
        public bool? IsNoPantry { get; set; }
        public DateTime? CreatedDate { get; set; } = DateTime.Now;
        public DateTime? ModifiedDate { get; set; } = DateTime.Now;
    }
}