﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class PantryListBO
    {
        public long PantryListID { get; set; }
        public long CustomerID { get; set; }
        public string PantryListName { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<bool> IsCreatedByRepUser { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string PantryListType { get; set; }
        public Nullable<bool> IsSynced { get; set; }
        public Nullable<bool> IsSorted { get; set; }
        public bool IsCopy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class DeleteFavoriteBO
    {
        public long PantryListID { get; set; }
    }
    public class DeleteFavoriteListItemBO
    {
        public long PantryItemID { get; set; }
        public long PantryListID { get; set; }
        public long CustomerID { get; set; }
        //public long ProductID { get; set; }
    }
    public class PantrySortBO
    {
        public long PantryListID { get; set; }
        public List<long> ProductID { get; set; }
    }

    public class ItemEnquiryBO
    {
        public long ProductID { get; set; }
        public string Comment { get; set; }
        public long CustomerID { get; set; }
        public long UserID { get; set; }
    }
}
