﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class WeeklySalesBO
    {
        public Nullable<double> Week1Sales { get; set; } = 0;
        public Nullable<double> Week2Sales { get; set; } = 0;
        public Nullable<double> Week3Sales { get; set; } = 0;
        public Nullable<double> Week4Sales { get; set; } = 0;
        public Nullable<double> Week5Sales { get; set; } = 0;
        public Nullable<double> Week6Sales { get; set; } = 0;
        public Nullable<double> Week7Sales { get; set; } = 0;
        public Nullable<double> Week8Sales { get; set; } = 0;
        public Nullable<double> Week9Sales { get; set; } = 0;
        public Nullable<double> Week10Sales { get; set; } = 0;
        public Nullable<double> Week11Sales { get; set; } = 0;
        public Nullable<double> Week12Sales { get; set; } = 0;
        public Nullable<double> Week13Sales { get; set; } = 0;
        public Nullable<double> Week14Sales { get; set; } = 0;
        public Nullable<double> Week15Sales { get; set; } = 0;
        public Nullable<double> Week16Sales { get; set; } = 0;
    }
}
