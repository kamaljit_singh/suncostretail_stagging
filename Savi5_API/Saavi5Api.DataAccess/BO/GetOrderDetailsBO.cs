﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class GetOrderDetailsBO
    {
        public long ProductID { get; set; }
        public long CustomerID { get; set; }
        public long CartID { get; set; }
        public long CartItemID { get; set; }
        public string PONumber { get; set; }
        public string ProductCode { get; set; }
        public double? Price { get; set; }
        public double? CompanyPrice { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string CustomerCode { get; set; }
        public string ProductImage { get; set; }
        public string ProductThumbImage { get; set; }
        public long? UOMID { get; set; }
        public string UnitName { get; set; }
        public string OrderUnitName { get; set; }
        public long OrderUnitId { get; set; }
        public string FilterName { get; set; }
        public double? ProductWeight { get; set; }
        public string WeightName { get; set; }
        public string WeightDescription { get; set; }
        public string IsPieceOrWeight { get; set; }
        public bool? IsAvailable { get; set; }
        public bool? IsSpecial { get; set; }
        public bool? IsDiscountApplicable { get; set; }
        public bool? IsCountrywideRewards { get; set; }
        public bool? IsInvoiceComment { get; set; }
        public bool? IsUnloadComment { get; set; }
        public bool? IsNoPantry { get; set; }
        public string ExtDoc { get; set; }
        public string RunNo { get; set; }
        public bool? IsGST { get; set; }
        public string PackagingSequence { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public double? SpecialPrice { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public double? StockQuantity { get; set; }
        public double? Quantity { get; set; }
        public string Warehouse { get; set; }
        public string Comment { get; set; }
        public string CustomerName { get; set; }
        public bool? IsSpecialCategory { get; set; }
        public int? AddressId { get; set; }
        public string Suburb { get; set; }
        public string Address2 { get; set; }
        public string Address1 { get; set; }
        public bool IsPlacedByRep { get; set; }
        public string SalesmanCode { get; set; }
        public bool IsOrderedSpecial { get; internal set; }
        public string CommentLine { get; internal set; }
        public string ContactName { get; internal set; }
        public string ContactNo { get; internal set; }
        public string ProComment { get; set; }
        public bool HasFreightCharges { get; set; }
        public bool HasNonDeliveryDayCharges { get; set; }
        public long? SalesRepUserID { get; internal set; }

        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public bool? IsOrderPostingToFtp { get; set; }
        public bool? RewardItem { get; set; }

        public string ERPStatus { get; set; }
        public long? UserID { get; set; }
        public string InvoiceNumber { get; set; }

        public string ProductDescription2 { get; set; }
        public string ProductDescription3 { get; set; }
        public string ProductFeature1 { get; set; }
        public string ProductFeature2 { get; set; }
        public string ProductFeature3 { get; set; }
        public bool ProductIsNew { get; set; }
        public bool ProductIsOnSale { get; set; }
        public bool ProductIsBackSoon { get; set; }
        public bool IsDelivery { get; set; }
        public string DeliveryType { get; set; }
        public bool? IsDonationBox { get; set; }
        public List<ProductImages> ProductImages { get; internal set; }
    }
}