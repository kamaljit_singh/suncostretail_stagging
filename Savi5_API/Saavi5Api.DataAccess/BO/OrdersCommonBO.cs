﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class OrdersCommonBO
    {
    }

    public class AddCommentBO
    {
        public long CustomerID { get; set; }
        public long ProductID { get; set; }
        public bool IsProduct { get; set; }
        public bool IsInvoice { get; set; }
        public string CommentDescription { get; set; }
    }

    public class GetComments
    {
        public long CustomerID { get; set; }
        public long ProductID { get; set; }
        public bool IsProduct { get; set; }
    }

    public class DeleteCommentBO
    {
        public long CommentID { get; set; }
        public bool IsProduct { get; set; }
    }
}
