﻿using CommonFunctions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class Common
    {
        public static long GetTicks(DateTime date)
        {
            DateTime baseDate = new DateTime(1970, 1, 1);
            TimeSpan diff = date - baseDate;

            return Convert.ToInt64(diff.TotalMilliseconds);
        }

        public static Color HexToColor(string hexColor)
        {
            //Remove # if present
            if (hexColor.IndexOf('#') != -1)
                hexColor = hexColor.Replace("#", "");

            int red = 0;
            int green = 0;
            int blue = 0;

            if (hexColor.Length == 6)
            {
                //#RRGGBB
                red = int.Parse(hexColor.Substring(0, 2), NumberStyles.AllowHexSpecifier);
                green = int.Parse(hexColor.Substring(2, 2), NumberStyles.AllowHexSpecifier);
                blue = int.Parse(hexColor.Substring(4, 2), NumberStyles.AllowHexSpecifier);
            }
            else if (hexColor.Length == 3)
            {
                //#RGB
                red = int.Parse(hexColor[0].ToString() + hexColor[0].ToString(), NumberStyles.AllowHexSpecifier);
                green = int.Parse(hexColor[1].ToString() + hexColor[1].ToString(), NumberStyles.AllowHexSpecifier);
                blue = int.Parse(hexColor[2].ToString() + hexColor[2].ToString(), NumberStyles.AllowHexSpecifier);
            }

            return Color.FromArgb(red, green, blue);
        }
    }

    public static class Ext
    {
        public static long ToTicks(this DateTime? date)
        {
            DateTime baseDate = new DateTime(1970, 1, 1);
            TimeSpan diff = date.To<DateTime>() - baseDate;

            return Convert.ToInt64(diff.TotalMilliseconds);
        }
    }

    public class APiResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }

    public class ProductFilters
    {
        public long CustomerID { get; set; }
        public long listCustomerId { get; set; }
        public long MainCategoryID { get; set; }
        public long SubCategoryID { get; set; }
        public long FilterID { get; set; }
        public string Searchtext { get; set; }
        public bool IsSpecial { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public long? UserID { get; set; }
        public bool IsRepUser { get; set; } = false;
        public bool IsSuggestive { get; set; } = false;
    }

    public class PantryFilters
    {
        public long PantryListID { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SortBy { get; set; }
        public long FilterID { get; set; }
        public long CustomerID { get; set; }
        public long listCustomerId { get; set; }
        public long UserID { get; set; }
        public string Searchtext { get; set; } = "";
        public bool IsRepUser { get; set; } = false;
        public bool ShowAll { get; set; } = false;
        public bool IsWebView { get; set; } = false;
        public string PantryType { get; set; }
    }

    public class ProductByBarcode
    {
        public string Barcode { get; set; } = "";
    }

    public enum ResponseCodes
    {
        Success = 200,
        ServerError = 500,
        NoData = 201,
        NotAuthorised = 401,
        NotFound = 404
    }

    public class DynamicUnitPrices
    {
        public long UOMID { get; set; } = 0;
        public string UOMDesc { get; set; }
        public double? QuantityPerUnit { get; set; } = 0;
        public bool? IsSpecial { get; set; }
        public bool? IsPromotional { get; set; }
        public double Price { get; set; }
        public double? CostPrice { get; set; }
    }

    public class GetCartBO
    {
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public bool IsPlacedByRep { get; set; }
        public bool IsSavedOrder { get; set; }
        public long CartID { get; set; }
    }
    public class CancelRecurringpayment
    {
        public long customerid { get; set; }
        public string planid { get; set; }
        public string devicetype { get; set; }
    }
    public class SubscriptionDetail
    {
        public int planid { get; set; }
        public int newplanid { get; set; }
        public string paymentmethod { get; set; }
        public Int64 customerid { get; set; }      
        public string devicetype { get; set; }
        public double? price { get; set; }
    }
    public class PaymentTokenModel
    {
        public double PaymentTotal { get; set; }
        public string CurrencyCode { get; set; }
        public bool OnlyAuthorize { get; set; }
        public long TempCartID { get; set; }
        public string DeviceType { get; set; }
    }

    public class PaymentTokenForPastOrdersModel
    {
        public double PaymentTotal { get; set; }
        public string CurrencyCode { get; set; }
        public bool OnlyAuthorize { get; set; }
        public List<long> CartIDs { get; set; }
        public string DeviceType { get; set; }
    }
    public class LogPaymentModel
    {
        public string PayResponse { get; set; }
        public long TempCartID { get; set; }
    }

    public class SubcriptionLogMasterModel
    {
        public string payResponse { get; set; }
        public long customerId { get; set; }
        public int recurringId { get; set; }
        public double? price { get; set; }
    }

    public class OrderDetailsBO
    {
        public long CustomerID { get; set; }
        public long OrderID { get; set; }
        public long UserID { get; set; }
        public bool IsOrderHistory { get; set; }
    }

    public class DeleteOrderBO
    {
        public long CustomerID { get; set; }
        public long OrderID { get; set; }
        public long UserID { get; set; }
    }

    public class DeleteCartItemBO
    {
        public long CartItemID { get; set; }
        public bool IsSavedOrder { get; set; } = false;
    }

    public class GetInvoicesBO
    {
        public bool IsPdfListing { get; set; }
        public string Email { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime TillDate { get; set; }
        public long CustomerID { get; set; }
        public long UserID { get; set; }
    }

    public class InvoiceListing
    {
        public string InvoiceNumber { get; set; }
        public string PdfPath { get; set; }
        public string CustomerName { get; set; }
        public string OrderDate { get; set; }
    }

    public class ReorderBO
    {
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public long OrderID { get; set; }
        public bool AppendToSavedOrder { get; set; }
        public List<long> Products { get; set; }
        public bool IsPlacedByRep { get; set; } = false;
    }

    public class CartCount
    {
        public long CartID { get; set; }
        public bool IsSavedOrder { get; set; }
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public bool IsRepUser { get; set; } = false;
    }

    public class ForgotPasswordModel
    {
        public string Email { get; set; }
    }

    public class ProductDetailBO
    {
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public long ProductID { get; set; }
        public bool IsRepUser { get; set; } = false;
        public List<long> Products { get; set; }
    }

    public class ProductImages
    {
        public string ImageName { get; set; }
    }

    public class DeleteSavedOrder
    {
        public long orderID { get; set; }
    }

    public class GetDeliveriesModel
    {
        public long DriverID { get; set; }
        public String DeliveryDate { get; set; }
        public bool IsCompleted { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Searchtext { get; set; }
        public long OrderID { get; set; } = 0;
        public bool ShowAll { get; set; } = false;
        public string OrderStatus { get; set; } = "all";
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public string Suburb { get; set; } = "";
    }

    public class DeliveryDetailModel
    {
        public long OrderID { get; set; }
    }

    public class GetCustomerpantryListModel
    {
        public long CustomerID { get; set; }
    }

    public class GetPaymentOrdersModel
    {
        public string token { get; set; }
    }

    public class SavePastOrderPaymentModel
    {
        public double TotalAmount { get; set; }
        public List<long> OrderIds { get; set; }
        public string Token { get; set; }

        public PayIntent PaymentDetails { get; set; }
    }

    public class StandingOrderModel
    {
        public long CustomerID { get; set; }
        public long CartID { get; set; }
    }

    public class DeliverySortBO
    {
        public long DriverID { get; set; }
        public List<long> OrderIDs { get; set; }
        public string DeliveryDate { get; set; }
    }

    public class SpecialProductReqBO
    {
        public long CustomerID { get; set; }
        public long RepUserID { get; set; }
        public string Details { get; set; }
    }

    public class CustomerfeaturesBO
    {
        public long CustomerID { get; set; }
        public bool IsRepUser { get; set; } = false;
        public long userID { get; set; }
    }

    public class ProductIssuesList
    {
        public string Images { get; set; }
        public string Reason { get; set; }
        public string Product { get; set; }
        public double? Price { get; set; }
        public double? Quantity { get; set; }
    }

    public class SuggestiveItemsModel
    {
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public long CartID { get; set; }
    }

    public class SugProds
    {
        public long ProductID { get; set; }
    }

    public class CouponBo
    {
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public long CartID { get; set; }
        public string Coupon { get; set; }
    }

    public class OrderPaymentLinkRequestModel
    {
        public long CustomerID { get; set; }
        public long UserID { get; set; }
        public string Email { get; set; }
    }

    public class CartRes
    {
        public int Count { get; set; }
        public double? CartTotal { get; set; }
        public double? TotalWithGST { get; set; }
    }

    public class SlotsBO
    {
        public string DeliveryDate { get; set; }
        public int Slots { get; set; }
    }

    public class NoticeBO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string OrderCutOff { get; set; }
        public string OrderCutOffDate { get; set; }
        public Nullable<bool> NotificationsEnabled { get; set; }
        public string SalesRepMessage { get; set; }
        public Nullable<bool> IsPermitSameDay { get; set; }
        public Nullable<bool> LimitOrdersTo1Month { get; set; }
        public Nullable<bool> EnableCutoffRules { get; set; }
        public Nullable<bool> AllowPickup { get; set; }
        public string CutoffType { get; set; }
        public string Company { get; set; }
        public Nullable<int> GroupID { get; set; }
    }

    public class RecurringOrdersBO
    {
        public int ID { get; set; }
        public long CustomerID { get; set; } = 0;
        public int? AddressID { get; set; }
        public int? PickupID { get; set; }
        public string Frequency { get; set; }
        public bool? IsSuspended { get; set; } = false;
        public string OrderType { get; set; }
        public DateTime? StartDate { get; set; }
        public List<RecurrProducts> RecurrProds { get; set; }
    }

    public class RecurrProducts
    {
        public int RecurrProdID { get; set; }
        public long? ProductID { get; set; }
        public double? Quantity { get; set; }
        public double? Price { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
    }

    public class GetRecurringOrderBO
    {
        public long CustomerID { get; set; }
    }

    public class GetAllProductsBO
    {
        public long ProductID { get; set; }
        public double? Price { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
    }

    public class DeleteRecurrItemBO
    {
        public int RecurrProdID { get; set; }
    }
}