﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class ProductFiltersBO
    {
        public long FilterID { get; set; }
        public Nullable<long> CompanyID { get; set; }
        public string FilterName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string FilterCode { get; set; }
    }

    public class FilterForCategory
    {
        public long CategoryID { get; set; }
        public long MainCategoryID { get; set; }
        public double? StockQuantity { get; set; }
    }
}