﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class OrderHistoryBO
    {
        public long CartID { get; set; }
        public string OrderNumber { get; set; }
        public long? CommentID { get; set; }
        public string Comment { get; set; }
        public int? OrderStatus { get; set; }
        public string CheckOrderStatus { get; set; }
        public string OrderDate { get; set; }
        public bool? IsAutoOrdered { get; set; }
        public double? Price { get; set; }
        public long DateFormatLong { get; set; }
        public string OrderStatusDesc { get; set; }
        public DateTime? OrderDateLong { get; set; }
        public bool IsDelivery { get; set; }
    }

    public class PaymentOrderBO
    {
        public long CartID { get; set; }
        public long UserID { get; set; } = 0;
        public string OrderNumber { get; set; }
        public int? OrderStatus { get; set; }
        public string CheckOrderStatus { get; set; }
        public string OrderDate { get; set; }
        public bool? IsAutoOrdered { get; set; }
        public double? Price { get; set; }
        public long DateFormatLong { get; set; }
        public string OrderStatusDesc { get; set; }
        public DateTime? OrderDateLong { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsPaidOrder { get; set; } = false;
        public long? PaymentId { get; set; }
    }

    public class StandingOrderBO
    {
        public long CartID { get; set; }
        public string OrderNumber { get; set; }
        public DateTime? StandingOrderStartDate { get; set; }
        public List<int> DaysOfMonth { get; set; } //Day of Month
        public List<int> DaysOfWeek { get; set; } //Day 1 to 7 starting from Sunday
        public bool? IsOrderDaily { get; set; }
        public long? CommentID { get; set; }
        public string Comment { get; set; }
        public int? OrderStatus { get; set; }
        public string CheckOrderStatus { get; set; }
        public DateTime? OrderDate { get; set; }
        public bool? IsAutoOrdered { get; set; }
        public double? Price { get; set; }
        public long DateFormatLong { get; set; }
        public string OrderStatusDesc { get; set; }
        public bool IsDelivery { get; set; }
    }
}