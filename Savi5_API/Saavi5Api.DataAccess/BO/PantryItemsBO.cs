﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class PantryItemsBO
    {
        public long PantryItemID { get; set; }
        public long PantryListID { get; set; }
        public long ProductID { get; set; }
        public Nullable<double> Quantity { get; set; }
        public Nullable<bool> IsActive { get; set; } = true;
        public Nullable<System.DateTime> CreatedDate { get; set; } = null;
        public Nullable<System.DateTime> ModifiedDate { get; set; } = null;
        public Nullable<double> ShelfLevel { get; set; } = 0;
        public Nullable<double> Price { get; set; }
        public Nullable<int> ShowOrder { get; set; } = 0;
        public Nullable<bool> IsCore { get; set; } = false;
        public Nullable<long> PrimaryUnitOfMeasureId { get; set; }
        public Nullable<long> SecondaryUnitOfMeasureId { get; set; }
        public Nullable<double> Week1Sales { get; set; } = 0;
        public Nullable<double> Week2Sales { get; set; } = 0;
        public Nullable<double> Week3Sales { get; set; } = 0;
        public Nullable<double> Week4Sales { get; set; } = 0;
        public Nullable<double> Week5Sales { get; set; } = 0;
        public Nullable<double> Week6Sales { get; set; } = 0;
        public Nullable<double> Week7Sales { get; set; } = 0;
        public Nullable<double> Week8Sales { get; set; } = 0;
        public Nullable<double> Week9Sales { get; set; } = 0;
        public Nullable<double> Week10Sales { get; set; } = 0;
        public Nullable<double> Week11Sales { get; set; } = 0;
        public Nullable<double> Week12Sales { get; set; } = 0;
        public Nullable<double> Week13Sales { get; set; } = 0;
        public Nullable<double> Week14Sales { get; set; } = 0;
        public Nullable<double> Week15Sales { get; set; } = 0;
        public Nullable<double> Week16Sales { get; set; } = 0;
        public long CustomerID { get; set; }
        public string PantryType { get; set; }
    }
}