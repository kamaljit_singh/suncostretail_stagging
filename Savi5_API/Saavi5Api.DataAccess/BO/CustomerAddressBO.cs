﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class CustomerAddressBO
    {
        public Int64 CustomerId { get; set; }
        public int AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string AddressCode { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string PostCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Err { get; set; }

        //public DaysBOL CustomerRuns { get { return _CustomerRuns; } set { _CustomerRuns = value; } }
        //public DaysBOL PackingSeq { get { return _PackingSeq; } set { _PackingSeq = value; } }
        public string Warehouse { get; set; }

        public string Suburb { get; set; }
        public string ContactName { get; set; }
    }

    public class AddCustomerAddressBO
    {
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string StreetAddress { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public long CustomerID { get; set; }
    }

    public class DelAddress
    {
        public int AddressID { get; set; }
    }

    public class Pickups
    {
        public int PickupID { get; set; }
        public long AddressID { get; set; } = 0;
        public bool? IsDelivery { get; set; } = false;
    }
}