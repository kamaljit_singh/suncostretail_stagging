﻿using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using System.Text.RegularExpressions;

namespace Saavi5Api.DataAccess.BO
{
    public static class HelperClass
    {
        public static TimeZoneInfo TimeZone = TimeZoneInfo.FindSystemTimeZoneById(System.Configuration.ConfigurationManager.AppSettings["TimeZone"].ToString());
        public static string siteName = ConfigurationManager.AppSettings["WEBSITE_NAME"];
        public static string Company = ConfigurationManager.AppSettings["CompanyName"];
        public static string WebsiteUrl = ConfigurationManager.AppSettings["WEBSITE_URL"].ToString();
        public static string WebViewURL = ConfigurationManager.AppSettings["WebView_URL"].ToString();
        public static bool IsCSVGeneration = Convert.ToString(ConfigurationManager.AppSettings["IsCSVGeneration"]) == "1";
        public static bool IsExcelGeneration = Convert.ToString(ConfigurationManager.AppSettings["IsExcelGeneration"]) == "1";
        public static bool IsXMLGeneration = Convert.ToString(ConfigurationManager.AppSettings["IsXMLGeneration"]) == "1";
        public static bool IsPDFGeneration = Convert.ToString(ConfigurationManager.AppSettings["IsPDFGeneration"]) == "1";
        public static bool IsFTPUploading = Convert.ToString(ConfigurationManager.AppSettings["IsFTPUploading"]) == "1";
        public static string FTPUrl = Convert.ToString(ConfigurationManager.AppSettings["FTPUrl"]);
        public static string FTPUserName = Convert.ToString(ConfigurationManager.AppSettings["FTPUserName"]);
        public static string FTPPassword = Convert.ToString(ConfigurationManager.AppSettings["FTPPassword"]);
        public static bool FTP_UsePassive = Convert.ToString(ConfigurationManager.AppSettings["FTP_UsePassive"]) == "1";

        public static string AdminEmail = Convert.ToString(ConfigurationManager.AppSettings["AdminEmail"]);
        public static string EmailTemplatePath = Convert.ToString(ConfigurationManager.AppSettings["TEMPLATE_PATH"]);
        public static string ApiURLLocal = Convert.ToString(ConfigurationManager.AppSettings["APILocal"]);
        public static string ApiURLLive = Convert.ToString(ConfigurationManager.AppSettings["APIURL"]);
        public static string ToEmail = Convert.ToString(ConfigurationManager.AppSettings["TO_EMAILID"]);
        public static string FromEmail = Convert.ToString(ConfigurationManager.AppSettings["FROM_EMAILID"]);
        public static string InvoiceLogo = Convert.ToString(ConfigurationManager.AppSettings["InvoiceLogo"]);
        public static string TempltePath = Convert.ToString(ConfigurationManager.AppSettings["TEMPLATE_PATH"]);
        public static string GoogleAPI = Convert.ToString(ConfigurationManager.AppSettings["GoogleAPIKey"]);
        public static string PODImageUrl = Convert.ToString(ConfigurationManager.AppSettings["PODImageUrl"]);

        public static string OrderFilePrefix = Convert.ToString(ConfigurationManager.AppSettings["OrderFilePrefix"]);
        public static string SalesDepEmail = Convert.ToString(ConfigurationManager.AppSettings["SalesDepEmail"]);
        public static bool InvoiceNotes = Convert.ToString(ConfigurationManager.AppSettings["InvoiceNotes"]) == "1";
        public static string InvoiceTitle = Convert.ToString(ConfigurationManager.AppSettings["InvoiceTitle"]);
        public static int InvoiceLogoHeight = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceLogoHeight"]);
        public static int InvoiceLogoWidth = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceLogoWidth"]);
        public static string InvoiceEmail = Convert.ToString(ConfigurationManager.AppSettings["InvoiceEmail"]);

        public static bool AllowHighSpecialPrice = Convert.ToString(ConfigurationManager.AppSettings["AllowHighSpecialPrice"]) == "1";
        public static bool SendCustomerAndRepMail = Convert.ToString(ConfigurationManager.AppSettings["SendCustomerAndRepMail"]) == "1";
        public static bool ShowWalkthrough = Convert.ToString(ConfigurationManager.AppSettings["ShowWalkthrough"]) == "1";

        public static bool SplitSubCategoryOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitSubCategoryOrder"]) == "1";
        public static bool SplitCategoryOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitCategoryOrder"]) == "1";
        public static bool SplitFoodVegOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitFoodVegOrder"]) == "1";
        public static bool SplitProcVegOrder = Convert.ToString(ConfigurationManager.AppSettings["SplitProcVegOrder"]) == "1";
        public static bool ShowSuggestiveCart = Convert.ToString(ConfigurationManager.AppSettings["ShowSuggestiveCart"]) == "1";

        public static string SplitSubCategoryName = Convert.ToString(ConfigurationManager.AppSettings["SplitSubCategoryName"]);
        public static string SplitCategoryName = Convert.ToString(ConfigurationManager.AppSettings["SplitCategoryName"]);
        public static string DeliveryIssueEmail = Convert.ToString(ConfigurationManager.AppSettings["DeliveryIssueEmail"]);
        public static string ABN = Convert.ToString(ConfigurationManager.AppSettings["ABN"]);
        public static string CreditIssueEmail = Convert.ToString(ConfigurationManager.AppSettings["CreditIssueEmail"]);
        public static long CustomerID = Convert.ToInt64(ConfigurationManager.AppSettings["CustomerID"]);
        public static long ContactNumber = Convert.ToInt64(ConfigurationManager.AppSettings["ContactNumber"]);

        public static long GuestLoginID = Convert.ToInt64(ConfigurationManager.AppSettings["GuestLoginID"]);
        public static string AccountsEmail = Convert.ToString(ConfigurationManager.AppSettings["AccountsEmail"]);

        public static string MailchimpApiKey = Convert.ToString(ConfigurationManager.AppSettings["MailchimpApiKey"]);
        public static string ListID = Convert.ToString(ConfigurationManager.AppSettings["ListID"]);

        public static string formatprice(string price, bool format = false)
        {
            string newPrice = "";
            try
            {
                newPrice = Math.Round(Convert.ToDouble(price), 2).ToString();
                int dotIndex = newPrice.IndexOf(".");
                if (dotIndex >= 0)
                {
                    string afterdot = newPrice.Substring(dotIndex + 1);
                    if (afterdot.Length == 1)
                    {
                        newPrice = newPrice + "0";
                    }
                }
                else
                {
                    newPrice = newPrice + ".00";
                }
            }
            catch (Exception ex)
            {
                newPrice = price;
            }

            return newPrice;
        }

        public static DateTime GetCurrentTime()
        {
            DateTime currentTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, HelperClass.TimeZone);
            return currentTime;
        }

        public static DateTime getMST(DateTime val)
        {
            DateTime serverTime = Convert.ToDateTime(val); // gives you current Time in server timeZone
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZone"].ToString());
            DateTime dt = TimeZoneInfo.ConvertTime(serverTime, tzi);
            return dt;
        }

        public static double GetProductPrice(long ProductId, int Price_Code)
        {
            using (var _db = new Saavi5Entities())
            {
                double prc = 0;
                var prodataforprc = _db.ProductMasters.Where(s => s.ProductID == ProductId).FirstOrDefault();
                prc = Convert.ToDouble(prodataforprc.Price == null ? 0 : prodataforprc.Price); ;
                switch (Price_Code)
                {
                    case 1:
                        prc = Convert.ToDouble(prodataforprc.PRICE1 == null ? 0 : prodataforprc.PRICE1);
                        break;

                    case 2:
                        prc = Convert.ToDouble(prodataforprc.PRICE2 == null ? 0 : prodataforprc.PRICE2);
                        break;

                    case 3:
                        prc = Convert.ToDouble(prodataforprc.PRICE3 == null ? 0 : prodataforprc.PRICE3);
                        break;

                    case 4:
                        prc = Convert.ToDouble(prodataforprc.PRICE4 == null ? 0 : prodataforprc.PRICE4);
                        break;

                    case 5:
                        prc = Convert.ToDouble(prodataforprc.PRICE5 == null ? 0 : prodataforprc.PRICE5);
                        break;

                    case 6:
                        prc = Convert.ToDouble(prodataforprc.PRICE6 == null ? 0 : prodataforprc.PRICE6);
                        break;

                    case 7:
                        prc = Convert.ToDouble(prodataforprc.PRICE7 == null ? 0 : prodataforprc.PRICE7);
                        break;

                    case 8:
                        prc = Convert.ToDouble(prodataforprc.PRICE8 == null ? 0 : prodataforprc.PRICE8);
                        break;

                    case 9:
                        prc = Convert.ToDouble(prodataforprc.PRICE9 == null ? 0 : prodataforprc.PRICE9);
                        break;

                    case 10:
                        prc = Convert.ToDouble(prodataforprc.PRICE10 == null ? 0 : prodataforprc.PRICE10);
                        break;

                    case 11:
                        prc = Convert.ToDouble(prodataforprc.PRICE11 == null ? 0 : prodataforprc.PRICE11);
                        break;

                    case 12:
                        prc = Convert.ToDouble(prodataforprc.PRICE12 == null ? 0 : prodataforprc.PRICE12);
                        break;

                    case 13:
                        prc = Convert.ToDouble(prodataforprc.PRICE13 == null ? 0 : prodataforprc.PRICE13);
                        break;

                    case 14:
                        prc = Convert.ToDouble(prodataforprc.PRICE14 == null ? 0 : prodataforprc.PRICE14);
                        break;

                    case 15:
                        prc = Convert.ToDouble(prodataforprc.PRICE15 == null ? 0 : prodataforprc.PRICE15);
                        break;

                    default:
                        prc = Convert.ToDouble(prodataforprc.PRICE1 == null ? 0 : prodataforprc.PRICE1);
                        break;
                }
                return prc;
            }
        }

        public static string EncodeBase64(string strToEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(strToEncode);
            return System.Convert.ToBase64String(toEncodeAsBytes);
        }

        public static string DecodeBase64(string base64String)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(base64String);
            return System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
        }

        #region MigraDoc Helpers

        public static string FormatNumber<T>(T number, int maxDecimals = 4)
        {
            return Regex.Replace(String.Format("{0:n" + maxDecimals + "}", number),
                                 @"[" + System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator + "]?0+$", "");
        }

        public static double PageWidth(this Section section)
        {
            return (PageWidth(section.Document));
        }

        public static double PageWidth(this Document document)
        {
            Unit width, height;

            PageSetup.GetPageSize(document.DefaultPageSetup.PageFormat, out width, out height);
            if (document.DefaultPageSetup.Orientation == Orientation.Landscape)
                Swap<Unit>(ref width, ref height);

            return (width.Point - document.DefaultPageSetup.LeftMargin.Point - document.DefaultPageSetup.RightMargin.Point);
        }

        public static Column AddColumn(this Table table, ParagraphAlignment align, Unit unit = new Unit())
        {
            Column column = table.AddColumn();
            column.Width = unit;
            column.Format.Alignment = align;
            return column;
        }

        public static Paragraph AddParagraph(this TextFrame frame, string text, ParagraphAlignment align, string style = null)
        {
            return frame.AddParagraph().AddText(text, align, style);
        }

        public static Paragraph AddParagraph(this Cell cell, string text, ParagraphAlignment align, string style = null)
        {
            return cell.AddParagraph().AddText(text, align, style);
        }

        private static Paragraph AddText(this Paragraph paragraph, string text, ParagraphAlignment align, string style = null)
        {
            paragraph.Format.Alignment = align;
            if (style == null)
                paragraph.AddText(text);
            else
                paragraph.AddFormattedText(text, style);
            return paragraph;
        }

        public static string ToCurrency(this decimal number)
        {
            return ToCurrency(number, string.Empty);
        }

        public static string ToCurrency(this decimal number, string symbol)
        {
            return string.Format("{0}{1:N2}", symbol, number);
        }

        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T tmp = lhs; lhs = rhs; rhs = tmp;
        }

        public static Color TextColorFromHtml(string hex)
        {
            if (hex == null)
                return Colors.Black;
            else
                return new Color((uint)System.Drawing.ColorTranslator.FromHtml(hex).ToArgb());
        }

        #endregion MigraDoc Helpers
    }

    public static class SaaviErrorCodes
    {
        public static string Success = "200";
        public static string Error = "500";
        public static string NoData = "200";
        public static string NotFound = "404";
        public static string Unauthorized = "401";
        public static string AlreadyExists = "202";
        public static string NA = "201";
    }

    public class PhoneInfo
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
    }

    public class DeliveryChargesBO
    {
        public double? OrderValue { get; set; }
        public double? DeliveryFee { get; set; }
        public double? NonDeliveryDaysFee { get; set; }
        public string FrightMessage { get; set; }
        public string NonDeliveryDaysMessage { get; set; }
    }
}