﻿using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class SearchPantryListBO
    {
        public long PantryListID { get; set; }
        public string PantryListName { get; set; }
        public long PantryItemID { get; set; }
        public long ProductID { get; set; }
        public long CategoryId { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string UOMDesc { get; set; }
        public long UOMID { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageThumb { get; set; }
        public long UnitId { get; set; }
        public string UnitName { get; set; }
        public long FilterID { get; set; }
        public string FilterName { get; set; }
        public string CategoryName { get; set; }
        public string MainCategoryName { get; set; }
        public double? Price { get; set; }
        public double? CompanyPrice { get; set; }
        public double? Quantity { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsAvailable { get; set; }
        public int? ShowOrder { get; set; }
        public double? ShelfLevel { get; set; }
        public double? StockQuantity { get; set; }
        public bool? IsCore { get; set; }
        public bool? IsSuggestive { get; set; }
        public double? UnitsPerCarton { get; set; }
        public bool? IsCountrywideRewards { get; set; }
        public bool? IsGST { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public bool? IsSpecialCategory { get; set; }
        public WeeklySalesBO WeeklySales { get; set; }
        public bool IsInPantry { get; set; }
        public bool IsInCart { get; set; }
        public List<DynamicUnitPrices> DynamicUOM { get; set; }
        public PriceBO Prices { get; set; }
        public List<ProductImages> ProductImages { get; set; }

        public double? MinOQ { get; set; }
        public double? MaxOQ { get; set; }
        public bool BuyIn { get; set; }
        public bool IsStatusIN { get; set; }
        public long LastOrderUOMID { get; set; }
        public bool? SellBelowCost { get; set; }

        //public double? IsBoxDiscount { get; set; }
        public string Description2 { get; set; }

        public string Description3 { get; set; }
        public string Feature1 { get; set; }
        public string Feature2 { get; set; }
        public string Feature3 { get; set; }
        public bool IsNew { get; set; }
        public bool IsOnSale { get; set; }
        public bool IsBackSoon { get; set; }
        public string Barcode { get; set; }
        public string ShareURL { get; set; }
        public string ProductFeature { get; set; }
        public bool? IsDonationBox { get; set; }
    }

    public class PriceBO
    {
        public double Price { get; set; } = 0;
        public bool? IsSpecial { get; set; } = false;
        public bool? IsPromotional { get; set; } = false;
        public double? CostPrice { get; set; } = 0;
        public long? UOMID { get; set; } = 0;
        public string UOMDesc { get; set; } = "";
        public double? QuantityPerUnit { get; set; } = 0;
    }

    public class LastOrderUOMBO
    {
        public long ProductID { get; set; }
        public long? UOMID { get; set; } = 0;
    }

    public class SearchProductByBarCode
    {
        //public long ProductID { get; set; }
        public string ProductCode { get; set; }

        public string ProductName { get; set; }
        public string Description { get; set; }
    }
}