﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class CustomerFeaturesBO
    {
        public long ID { get; set; }
        public Nullable<long> UserID { get; set; }
        public Nullable<bool> IsSearchProduct { get; set; } = false;
        public Nullable<bool> IsLatestSpecials { get; set; } = false;
        public Nullable<bool> IsCheckDelivery { get; set; } = false;
        public Nullable<bool> IsCheckDeliveryDetail { get; set; } = false;
        public Nullable<bool> IsCheckDeliveryComment { get; set; } = false;
        public Nullable<bool> IsNoticeBoard { get; set; } = false;
        public Nullable<bool> IsInvoice { get; set; } = false;
        public Nullable<bool> IsOrderHistory { get; set; } = false;
        public Nullable<bool> IsPantryList { get; set; } = false;
        public Nullable<bool> IsPantrySorting { get; set; } = false;
        public Nullable<bool> IsPurchaseHistory { get; set; } = false;
        public Nullable<bool> IsNextDeliveryDate { get; set; } = false;
        public Nullable<bool> IsShowPrice { get; set; } = false;
        public Nullable<bool> IsShowStock { get; set; } = false;
        public Nullable<bool> IsSundayOrdering { get; set; } = false;
        public Nullable<bool> IsPONumber { get; set; } = false;
        public Nullable<bool> IsOrderDate { get; set; } = false;
        public Nullable<bool> IsPDF { get; set; } = false;
        public Nullable<bool> IsTargetMarketing { get; set; } = false;
        public Nullable<bool> IsManagement { get; set; } = false;
        public Nullable<bool> IsDynamicUOM { get; set; } = false;
        public Nullable<bool> IsShelfFeature { get; set; } = false;
        public Nullable<bool> IsSaveOrder { get; set; } = false;
        public Nullable<bool> IsAutoOrdering { get; set; } = false;
        public Nullable<bool> IsCoreItems { get; set; } = false;
        public Nullable<bool> IsSuggestiveSell { get; set; } = false;
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsAllowUserToAddPantry { get; set; } = false;
        public Nullable<bool> IsBarCodeScanningEnabled { get; set; } = false;
        public Nullable<bool> IsShowBrand { get; set; } = false;
        public Nullable<bool> IsShowSupplier { get; set; } = false;
        public Nullable<bool> IsShowProductLongDetail { get; set; } = false;
        public Nullable<bool> IsDisplayUnitsPerCarton { get; set; } = false;
        public Nullable<bool> IsFreightChargeApplicable { get; set; } = false;
        public Nullable<bool> IsBrowsingEnabledForHoldCust { get; set; } = false;
        public Nullable<bool> IsAddItemToDefaultPantry { get; set; } = false;
        public Nullable<bool> IsAddItemToPantry { get; set; } = false;
        public Nullable<bool> IsCopyPantryEnabled { get; set; } = false;
        public Nullable<bool> IsHighlightStock { get; set; } = false;
        public Nullable<bool> IsStripePayment { get; set; } = false;
        public Nullable<bool> IsNonDeliveryDayOrdering { get; set; } = false;
        public Nullable<bool> IsProductComment { get; set; } = false;
        public Nullable<bool> IsSpecialProductRequest { get; set; } = false;
        public Nullable<bool> IsMOQ { get; set; } = false;
        public Nullable<bool> IsNoBackorder { get; set; } = false;
        public Nullable<bool> IsMaxOQ { get; set; } = false;
        public Nullable<bool> IsInvoicePdf { get; set; } = false;   
        public Nullable<bool> IsAllowDecimal { get; set; } = false;
        public Nullable<bool> ReturnLastOrderUOM { get; set; } = false;
    }
}
