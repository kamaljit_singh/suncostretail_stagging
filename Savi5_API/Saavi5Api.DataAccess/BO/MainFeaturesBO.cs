﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class MainFeaturesBO
    {
        public Nullable<bool> IsNonFoodVersion { get; set; }
        public Nullable<bool> IsAdvancedPantry { get; set; }
        public Nullable<bool> IsAddSpecialPriceFromPantry { get; set; }
        public Nullable<bool> IsRepProductBrowsing { get; set; }
        public Nullable<bool> IsHighlightRewardItem { get; set; }
        public Nullable<bool> IsShowOrderStatus { get; set; }
        public Nullable<bool> IsShowTermConditionsPopup { get; set; }
        public Nullable<bool> IsShowProductClasses { get; set; }
        public Nullable<bool> IsShowOnlyStockItems { get; set; }
        public Nullable<bool> IsMultipleAddresses { get; set; }
        public Nullable<bool> IsShowContactDetails { get; set; }
        public Nullable<bool> IsLiquorControlPopup { get; set; }
        public Nullable<bool> IsItemEnquiryPopup { get; set; }
        public Nullable<bool> IsShowProductCost { get; set; }
        public Nullable<bool> IsShowProductHistory { get; set; }
        public Nullable<bool> IsPDFSpecialProducts { get; set; }
        public Nullable<bool> IsPictureViewEnabled { get; set; }
        public Nullable<bool> IsShowFutureDate { get; set; }
        public Nullable<bool> IsEnableRepToAddSpecialPrice { get; set; }
        public string PrimaryAppColor { get; set; }
        public string SecondaryAppColor { get; set; }
        public string PrimaryAppColorRGB { get; set; }
        public string SecondaryAppColorRGB { get; set; }
        public bool ShowWalkthrough { get; set; }
        public bool IsShowSubCategory { get; set; } = false;
        public bool IsShowQuantityPopup { get; set; } = false;
        public bool IsDatePickerEnabled { get; set; } = false;
        public bool IsTruckCheckList { get; set; } = false;
        public bool IsDeliveryCompliance { get; set; } = false;
        public bool IsTempratureReader { get; set; } = false;

        public bool IsThumbnail { get; set; } = false;
        public bool IsParentChild { get; set; } = false;
        public string Currency { get; set; }

        public bool ReturnLastOrderUOM { get; set; } = false;
        public bool IsHandleBackOrders { get; set; } = false;
        public bool ShowSalesRepMargins { get; set; } = false;
        public bool CaptureSalesRepLocation { get; set; } = false;
        public bool EnableBarcodeScanning { get; set; } = false;
        public bool AllowOnHoldPostingOrder { get; set; } = false;
        public bool ShowCategory { get; set; } = false;
        public bool ShowPackSize { get; set; } = false;
        public bool AllowAppDiscount { get; set; } = false;
        public bool EnableProfitLimitAlert { get; set; } = false;
        public bool IsSortByGoogle { get; set; } = false;
        public bool IsStripePayment { get; set; } = false;
        public Nullable<bool> OrderMultiples { get; set; } = false;
        public Nullable<bool> FavoriteList { get; set; } = false;
        public Nullable<bool> ShareButtons { get; set; } = false;
        public Nullable<bool> ShowAccount { get; set; } = false;
        public Nullable<bool> IsGSTEnabled { get; set; } = false;
        public Nullable<bool> IsPromoCode { get; set; } = false;
        public Nullable<bool> IsStandingOrder { get; set; } = false;
    }
}