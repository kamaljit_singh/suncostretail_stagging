﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class VehicleChecklistBO
    {
        public long ID { get; set; }
        public Nullable<short> VehicleID { get; set; }
        public Nullable<long> DriverID { get; set; }
        public string RunNo { get; set; }
        public Nullable<bool> IsVehicleConditionOK { get; set; }
        public Nullable<bool> IsWindscreenOK { get; set; } = false;
        public Nullable<bool> IsLightSignalOK { get; set; } = false;
        public Nullable<bool> IsTyreConditionOK { get; set; } = false;
        public Nullable<bool> IsFixturesFittingOK { get; set; } = false;
        public Nullable<double> OdometerStart { get; set; } = 0;
        public Nullable<double> OdometerEnd { get; set; } = 0;
        public Nullable<double> Weight { get; set; } = 0;
        public Nullable<double> Temprature { get; set; } = 0;
        public Nullable<double> TempChilled { get; set; } = 0;
        public Nullable<double> TempChilledEnd { get; set; } = 0;
        public Nullable<double> TempFrozenEnd { get; set; } = 0;
        public Nullable<double> TempFrozen { get; set; } = 0;
        public string Comments { get; set; }
        public string EODComment { get; set; }
        public bool Fuel { get; set; } = false;
        public string Shift { get; set; }
        public string VehicleService { get; set; }
        public Nullable<bool> IsActive { get; set; } = false;
    }

    public class GetTruckCompliance
    {
        //public long CheckListID { get; set; }
        public string VehicleNo { get; set; }
        public long DriverID { get; set; }
        public string Date { get; set; }
        public string RunNo { get; set; }
    }

    public class DriverComment
    {
        public long DeliveryID { get; set; }
        public string Comment { get; set; }
        public long DriverID { get; set; }
        public long OrderID { get; set; }
        public string CustomerName { get; set; }
    }

    public class ImageIssueList
    {
        public string ProductName { get; set; }
        public string OrderID { get; set; }
        public string IssueDesc { get; set; }
        public string ImageURL { get; set; }
        public long DeliveryID { get; set; }
    }

    public class SendInvoiceBO
    {
        public string Email { get; set; }
        public long InvoiceNo { get; set; }
    }
}
