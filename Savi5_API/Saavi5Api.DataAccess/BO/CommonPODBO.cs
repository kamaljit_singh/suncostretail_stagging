﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class DeliveryRunModel
    {
        public long DeliveryID { get; set; }
        public string DeliveryNo { get; set; }
        public long DriverID { get; set; }
        public short VehicleID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Status { get; set; }
        public bool IsCompleted { get; set; }
        public bool LoadedDry { get; set; }
        public bool LoadedChilled { get; set; }
        public bool LoadedFrozen { get; set; }
        public bool IsCancelled { get; set; }
        public string CancelReason { get; set; }
        public string PaymentMethod { get; set; }
        public double PaymentAmount { get; set; }
        public string ReceiversName { get; set; }
        public bool HasIssues { get; set; }
        public long VehicleCheckListID { get; set; }
        public double GoodsTemp { get; set; }
        public bool? IsTempAuto { get; set; }
        public string Comments { get; set; }
        public int? CartonNumber { get; set; }
        public double? Cent5 { get; set; }
        public double? Cent10 { get; set; }
        public double? Cent20 { get; set; }
        public double? Cent50 { get; set; }
        public double? Dollar1 { get; set; }
        public double? Dollar2 { get; set; }
        public double? Dollar5 { get; set; }
        public double? Dollar10 { get; set; }
        public double? Dollar20 { get; set; }
        public double? Dollar50 { get; set; }
        public double? Dollar100 { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool IssueCredit { get; set; } = false;
    }

    public class SendMessageModel
    {
        public long OrderID { get; set; }
        public long DriverID { get; set; }
        public string Message { get; set; }
    }

    public class DeliveryTrackingModel
    {
        public long DeliveryID { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class DeliveryImagesArray
    {
        public string ImageName { get; set; }

        //public string ImageBase64 { get; set; }
        public bool IsIssue { get; set; } = false;

        public bool IsSignature { get; set; }
        public long DeliveryID { get; set; }
        public string DeliveryNo { get; set; }
        public long ProductID { get; set; }
    }

    public class DeliveryIssuesArray
    {
        public long ProductID { get; set; }
        public string IssueDescription { get; set; }
        public double? Quantity { get; set; }
    }

    public class StartUpdateDeliveryModel
    {
        public bool IsDeliveryStart { get; set; }
        public List<DeliveryIssuesArray> DeliveryIssues { get; set; }
        public DeliveryRunModel DeliveryDetails { get; set; }
    }

    public class TrackingModel
    {
        public long DeliveryID { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class SettingsBO
    {
        public bool IsNotificationEnabled { get; set; }
        public long DriverID { get; set; }
    }

    public class StatusBO
    {
        public bool IsOnline { get; set; }
        public long DriverID { get; set; }
    }

    public class GalleryBO
    {
        public string DeliveryNo { get; set; }
    }

    public class DriverImageBO
    {
        public long DriverID { get; set; }
        public string ImageName { get; set; }
    }

    public class AssignDeliveriesBO
    {
        public long DriverID { get; set; }
        public List<long> OrderIDs { get; set; }
    }

    public class DeliveryDaysBO
    {
        public string OrderDate { get; set; }
    }

    public class ScanBO
    {
        public bool ScanMode { get; set; }
        public long DriverID { get; set; }
    }

    public class SetRunVehivleBO
    {
        public string RunNo { get; set; }
        public string Vehicle { get; set; }
        public long DriverID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

    public class RunManualSyncBO
    {
        public string RunNo { get; set; }
        public long DriverID { get; set; }
    }

    public class GeneratePDFBO
    {
        public long DriverID { get; set; }
    }
}