﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class RepCustomersBO
    {
        public long CustomerID { get; set; }
        public string ContactName { get; set; }
        public string CustomerName { get; set; }
        public bool? IsActive { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string LoginEmail { get; set; }
        public long UserID { get; set; }
        public bool? IsSundayOrdering { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string TermDescription { get; set; }
        public string AccountType { get; set; }
        public double? CurrentBalance { get; set; }
        public double? OverDueBalance { get; set; }
        public double? TotalBalance { get; set; }
        public double? YTDSales { get; set; }
        public double? MTDSales { get; set; }
        public double? PrevMonth { get; set; }
        public bool? DebtorOnHold { get; set; }
        public string ABN { get; set; }
        public string AlphaCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string PostCode { get; set; }
        public string SalesmanCode { get; set; }
        public string Suburb { get; set; }
        public string StateName { get; set; }
        public double? BalancePeriod1 { get; set; }
        public double? BalancePeriod2 { get; set; }
        public double? BalancePeriod3 { get; set; }
        public string Warehouse { get; set; }
        public string CreditLimit { get; set; }
        public bool? IsRep { get; set; }
        public string PriceLevel { get; set; }
        public PackingDaysBO PackingSeq { get; set; }
        public RunNoBO RunNo { get; set; }
        public long ExistingCartID { get; set; }
        public double? CartTotal { get; set; }
        public string StandardDeliveryDays { get; set; }
        public List<string> PermittedDeliveryDays { get; set; }

        public object DefaultOrderInfo { get; set; }

    }
}
