﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class DriverDeliveriesBO
    {
        public long OrderID { get; set; }
        public string CustomerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Postcode { get; set; }
        public string Status { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string Suburb { get; set; }
        public string ContactName { get; set; }
        public DateTime? RequestedDeliveryDate { get; set; }
        public string CustomerOrderNo { get; set; }
        public string DeliveryComment { get; set; }
        public string InvoiceNo { get; set; }
        public string Alphacode { get; set; }
        public int TotalItems { get; set; }
        public string Email { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public long? SortOrder { get; set; }
        public string OrderStatus { get; set; }
        public long? DeliveryID { get; set; }
        public DeliveryStatusCompModel DeliveryDetails { get; set; }
        public string DropSequence { get; set; }
        public float Distance { get; set; }
        public double TotalItemsLuxCombo { get; set; }
    }

    public class DeliveryDetailsBO
    {
        public long ProductID { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public double? Quantity { get; set; }
        public double? TotalAmount { get; set; }
    }

    public class CustAddressBO
    {
        public string CustomerName { get; internal set; }
        public string Email { get; internal set; }
        public string Address1 { get; internal set; }
        public string Address2 { get; internal set; }
        public string Phone { get; internal set; }
        public string Postcode { get; internal set; }
        public string State { get; internal set; }
    }

    public class DeliveryStatusCompModel
    {
        public long DeliveryID { get; set; }
        public string DeliveryNo { get; set; }
        public short VehicleID { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool? LoadedDry { get; set; }
        public bool? LoadedChilled { get; set; }
        public bool? LoadedFrozen { get; set; }
        public bool? IsCancelled { get; set; }
        public string CancelReason { get; set; }
        public string PaymentMethod { get; set; }
        public double? PaymentAmount { get; set; }
        public string ReceiversName { get; set; }
        public bool? HasIssues { get; set; }
        public double? GoodsTemp { get; set; }
        public bool? IsTempAuto { get; set; }
        public string Comments { get; set; }
        public List<ItemDetailsBO> ItemDetails { get; set; }
        public string SignatureImage { get; set; }
        public int? CartonNumber { get; set; }
    }

    public class ItemDetailsBO
    {
        public string IssueDescription { get; set; }
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public bool IsIssue { get; set; }
        public double? AmmendedQuantity { get; set; }
        public double? OrderedQuantity { get; set; }
    }
}