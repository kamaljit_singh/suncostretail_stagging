﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{

	public static class EmailSender
	{
		///
		/// Sends an Email.
		///
		public static bool Send(string sender, string senderName, string recipient, string recipientName, string subject, string body)
		{
			var message = new MailMessage()
			{
				From = new MailAddress(sender, senderName),
				Subject = subject,
				Body = body,
				IsBodyHtml = true
			};

			message.To.Add(new MailAddress(recipient, recipientName));

			try
			{
				var client = new SmtpClient();
				client.Send(message);
			}
			catch (Exception ex)
			{
				//handle exeption
				return false;
			}

			return true;
		}

		///
		/// Sends an Email asynchronously
		///
		public static void SendMailAsync(string sender, string senderName, string recipient, string recipientName, string subject, string body)
		{
			var message = new MailMessage()
			{
				From = new MailAddress(sender, senderName),
				Subject = subject,
				Body = body,
				IsBodyHtml = true
			};
			message.To.Add(new MailAddress(recipient, recipientName));

			var client = new SmtpClient();
			client.SendCompleted += MailDeliveryComplete;
			client.SendAsync(message, message);
		}

		private static void MailDeliveryComplete(object sender, AsyncCompletedEventArgs e)
		{
			if (e.Error != null)
			{
				throw e.Error;
			}
			else if (e.Cancelled)
			{
				//handle cancelled
			}
			else
			{
				//handle sent email
				MailMessage message = (MailMessage)e.UserState;
			}
		}
	}
}
