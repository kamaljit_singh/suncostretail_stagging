﻿using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{
    public class SearchProductsBO
    {
        public long ProductID { get; set; }
        public long PantryListID { get; set; }
        public Int64 CategoryID { get; set; }
        public Int64 FilterID { get; set; }
        public Int64 UOMID { get; set; }
        public int ProductCount { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string MainCategoryName { get; set; }
        public string FilterName { get; set; }
        public string UOMDesc { get; set; }
        public string ProductComment { get; set; }
        public double Price { get; set; }
        public double CompanyPrice { get; set; }
        public double SpecialPrice { get; set; }
        public string Description { get; set; }
        public string ProductCode { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageThumb { get; set; }
        public double Quantity { get; set; }
        public bool IsActive { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsManagerApproved { get; set; }
        public bool IsGST { get; set; }
        public string Brand { get; set; }
        public string Supplier { get; set; }
        public bool IsSpecialCategory { get; set; }
        public bool PiecesAndWeight { get; set; }
        public double? StockQuantity { get; set; }
        public WeeklySalesBO WeeklySales { get; set; }
        public bool IsInPantry { get; set; }
        public bool IsInCart { get; set; }
        public List<DynamicUnitPrices> DynamicUOM { get; set; }
        public PriceBO Prices { get; set; }
        public double? UnitsPerCarton { get; set; }
        public List<ProductImages> ProductImages { get; set; }
        public bool IsCountrywideRewards { get; internal set; }
        public double? MinOQ { get; set; }
        public double? MaxOQ { get; set; }
        public bool BuyIn { get; set; }
        public bool IsStatusIN { get; set; }
        public Int64 LastOrderUOMID { get; set; }
        public bool? SellBelowCost { get; set; }

        //public string PriceType { get; set; }
        //public bool IsPromotional { get; set; }
        //public bool IsCountrywideRewards { get; set; }
        public string Description2 { get; set; }

        public string Description3 { get; set; }
        public string Feature1 { get; set; }
        public string Feature2 { get; set; }
        public string Feature3 { get; set; }
        public bool IsNew { get; set; }
        public bool IsOnSale { get; set; }
        public bool IsBackSoon { get; set; }
        public string Barcode { get; set; }
        public string ShareURL { get; set; }
        public string ProductFeature { get; set; }
        public long PantryListItemID { get; set; }
        public bool? IsDonationBox { get; set; }
    }

    public class PdfProductsBO
    {
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageThumb { get; set; }
        public long UOMID { get; set; }
        public string UOMDesc { get; set; }
        public long FilterID { get; set; }
        public string FilterName { get; set; }
        public double? Price { get; set; }
        public double? CompanyPrice { get; set; }
        public double? SpecialPrice { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSpecial { get; set; }
        public bool? IsAvailable { get; set; }
        public bool? PiecesAndWeight { get; set; }
        public double? Quantity { get; set; } = 1;
    }

    public class PDFDetailsBO
    {
        public bool IsGrouped { get; set; }
        public string PdfFile { get; set; }
        public List<PdfProductsBO> PDFProducts { get; set; }
        public List<PDFGroupDetails> GroupDetails { get; set; }
    }

    public class PDFGroupDetails
    {
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public string PdfFile { get; set; }
        public List<PdfProductsBO> PDFProducts { get; set; }
    }
}