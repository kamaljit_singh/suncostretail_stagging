﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saavi5Api.DataAccess.BO
{

    public class GetRepCustomersModel
    {
        public long UserID { get; set; }
        public long CustomerID { get; set; }
        public string Searchtext { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public bool Debug { get; set; } = false;
    }

    public class GetRepProductDetailModel
    {
        public long ProductID { get; set; }
    }

    public class ProductWarehouseBO
    {
        public string Warehouse { get; set; }
        public double? StockOnHand { get; set; }
        public double? StockOnOrder { get; set; }
        public string OrderDueDate { get; set; }
        public long? ProductID { get; set; }
    }

    public class RepProductDetailBO
    {
        public List<ProductWarehouseBO> Warehouses { get; set; }
    }

    public class AddSpecialPriceBO
    {
        public long ProductID { get; set; }
        public long CustomerID { get; set; }
        public long RepUserID { get; set; }
        public long UOMID { get; set; }
        public double Price { get; set; }
        public bool AlwaysApply { get; set; }
        public double? QuantityPerUnit { get; set; } = 0;
        public long CartID { get; set; }
        public bool IsInCart { get; set; } = false;
    }

    public class RepOrderHistoryBO
    {
        public long CustomerID { get; set; }
    }

    public class PackingDaysBO
    {
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
    }

    public class RunNoBO
    {
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
    }

    public class DeliveryAddressRepBO
    {
        public long CustomerID { get; set; }
    }

    public class AddCommentRepBO
    {
        public long CustomerID { get; set; }
        public string InvoiceComment { get; set; }
        public string PickingComment { get; set; }
    }

    public class UserLocationBO
    {
        public long UserID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
