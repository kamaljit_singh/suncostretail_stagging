﻿using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using System.IO;
using System.Web.Hosting;
using CommonFunctions;

namespace Saavi5Api.DataAccess.DA
{
    public class SalesRepDA
    {
        #region Common return method

        public APiResponse ReturnMethod(string responseCode, string message, object result = null)
        {
            return new APiResponse
            {
                ResponseCode = responseCode,
                Message = message,
                Result = result
            };
        }

        #endregion Common return method

        #region Get Rep customers

        public async Task<APiResponse> GetRepCustomers(GetRepCustomersModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var parentChild = _db.MainFeaturesMasters.FirstOrDefault().IsParentChild ?? false;

                    CustomerMaster cust = new CustomerMaster();
                    var isRep = _db.CustomerMasters.Find(model.CustomerID).IsRep ?? false;

                    if (parentChild && !isRep)
                    {
                        cust = await _db.CustomerMasters.Where(s => s.ParentID == model.CustomerID).FirstOrDefaultAsync();

                        if (cust != null)
                        {
                            var customers = (from c in _db.CustomerMasters
                                                 //  where c.ParentID == model.CustomerID
                                                 // 12-04-2019: Change request , Show Panent and Child
                                             where (c.ParentID == model.CustomerID || c.CustomerID == model.CustomerID)
                                             && (c.CustomerName.Contains(model.Searchtext) || c.AlphaCode.Contains(model.Searchtext) || c.ABN.Contains(model.Searchtext) || model.Searchtext == "")
                                             select new
                                             {
                                                 CustomerID = c.CustomerID,
                                                 CustomerName = c.CustomerName,
                                                 Alphacode = c.AlphaCode,
                                                 IsActive = c.IsActive,
                                                 OnHold = c.DebtorOnHold,
                                                 ContactName = c.ContactName
                                             }).ToList();

                            var result = customers.OrderBy(o => o.CustomerName).Page(model.PageIndex * model.PageSize, model.PageSize).ToList();

                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                            {
                                TotalResults = customers.Count(),
                                ChildCustomers = result,
                                TotalPages = (customers.Count() + model.PageSize - 1) / model.PageSize,
                            });
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NotFound, "No customers found.");
                        }
                    }
                    else
                    {
                        cust = await _db.CustomerMasters.Where(s => s.CustomerID == model.CustomerID && s.IsRep == true).FirstOrDefaultAsync();

                        if (cust == null)
                        {
                            return ReturnMethod(SaaviErrorCodes.NotFound, "Not Found");
                        }
                        string salesmanCode = cust.SalesmanCode ?? "";
                        bool hasCountryWideRewards = false;
                        string[] salesmanCodeArray = null;
                        if (salesmanCode != null && salesmanCode != "")
                        {
                            salesmanCodeArray = salesmanCode.Split(new char[] { ',' });
                        }

                        if (cust != null && salesmanCodeArray != null)
                        {
                            var customers = (from cus in _db.CustomerMasters
                                             join us in _db.UserMasters on cus.CustomerID equals us.CustomerID
                                             join debtor in _db.DebtorTypeMasters on cus.DebtorTypeID equals debtor.DebtorTypeId into temp
                                             from debtor in temp.DefaultIfEmpty()
                                             join state in _db.StateMasters on cus.StateID equals state.StateID into temp1
                                             from state in temp1.DefaultIfEmpty()
                                             join term in _db.TermMasters on cus.TermID equals term.TermID into temp2
                                             from term in temp2.DefaultIfEmpty()
                                             where (salesmanCodeArray.Contains(cus.SalesmanCode) && cus.IsRep == false
                                                    && (cus.CustomerName.Contains(model.Searchtext) || cus.AlphaCode.Contains(model.Searchtext) || cus.ABN.Contains(model.Searchtext))
                                                    )
                                             select new RepCustomersBO
                                             {
                                                 CustomerID = cus.CustomerID,
                                                 ContactName = cus.ContactName == null ? "" : cus.ContactName,
                                                 CustomerName = cus.CustomerName == null ? "" : cus.CustomerName,
                                                 IsActive = us.IsActive,
                                                 Firstname = us.FirstName == null ? "" : us.FirstName,
                                                 Lastname = us.LastName == null ? "" : us.LastName,
                                                 LoginEmail = us.Email,
                                                 UserID = us.UserID,
                                                 IsSundayOrdering = cus.IsSundayOrdering == null ? false : cus.IsSundayOrdering,
                                                 Phone1 = cus.Phone1 == null ? "" : cus.Phone1,
                                                 Phone2 = cus.Phone2 == null ? "" : cus.Phone2,
                                                 TermDescription = term.TermName == null ? (cus.TermDescription == null ? "" : cus.TermDescription) : term.TermName.Trim(),
                                                 AccountType = debtor.DebtorTypeName == null ? "N/A" : debtor.DebtorTypeName,
                                                 CurrentBalance = cus.CurrentBalance == null ? 0 : cus.CurrentBalance,
                                                 OverDueBalance = cus.TotalAmountDue == null ? 0 : cus.TotalAmountDue,
                                                 TotalBalance = cus.TotalAmountOwing == null ? 0 : cus.TotalAmountOwing,
                                                 YTDSales = cus.YTDSales == null ? 0 : cus.YTDSales,
                                                 MTDSales = cus.MTDSales == null ? 0 : cus.MTDSales,
                                                 PrevMonth = cus.PrevMonthSales == null ? 0 : cus.PrevMonthSales,
                                                 DebtorOnHold = (cus.DebtorOnHold == null) ? false : cus.DebtorOnHold,
                                                 ABN = cus.ABN == null ? "" : cus.ABN,
                                                 AlphaCode = cus.AlphaCode == null ? "" : cus.AlphaCode,
                                                 Address1 = cus.Address1 == null ? "" : cus.Address1,
                                                 Address2 = cus.Address2 == null ? "" : cus.Address2,
                                                 City = cus.City == null ? "" : cus.City,
                                                 Fax = cus.Fax == null ? "" : cus.Fax,
                                                 Email = cus.Email == null ? "" : cus.Email,
                                                 PostCode = cus.PostCode == null ? "" : cus.PostCode,
                                                 SalesmanCode = cus.SalesmanCode == null ? "" : cus.SalesmanCode,
                                                 Suburb = cus.Suburb == null ? "" : cus.Suburb,
                                                 StateName = state.StateName == null ? "" : state.StateName,
                                                 PriceLevel = cus.PriceLevel == null ? "" : cus.PriceLevel,
                                                 BalancePeriod1 = cus.Day30Balance == null ? 0 : cus.Day30Balance,
                                                 BalancePeriod2 = cus.Day60Balance == null ? 0 : cus.Day60Balance,
                                                 BalancePeriod3 = cus.Day90PlusBalance == null ? 0 : cus.Day90PlusBalance,
                                                 Warehouse = cus.Warehouse == null ? "" : cus.Warehouse.Trim(),
                                                 CreditLimit = cus.CreditLimit == null ? "0" : cus.CreditLimit,
                                                 IsRep = cus.IsRep,
                                                 PackingSeq = new PackingDaysBO()
                                                 {
                                                     Monday = cus.MondayPackingSequence ?? "",
                                                     Tuesday = cus.TuesdayPackingSequence ?? "",
                                                     Wednesday = cus.WednesdayPackingSequence ?? "",
                                                     Thursday = cus.ThursdayPackingSequence ?? "",
                                                     Friday = cus.FridayPackingSequence ?? "",
                                                     Saturday = cus.SaturdayPackingSequence ?? "",
                                                     Sunday = cus.SundayPackingSequence ?? "",
                                                 },
                                                 RunNo = new RunNoBO()
                                                 {
                                                     Monday = cus.MondayShippingAgentCode == "Default" ? cus.RunNo : "",
                                                     Tuesday = cus.TuesdayShippingAgentCode == "Default" ? cus.RunNo : "",
                                                     Wednesday = cus.WednesdayShippingAgentCode == "Default" ? cus.RunNo : "",
                                                     Thursday = cus.ThursdayShippingAgentCode == "Default" ? cus.RunNo : "",
                                                     Friday = cus.FridayShippingAgentCode == "Default" ? cus.RunNo : "",
                                                     Saturday = cus.SaturdayShippingAgentCode == "Default" ? cus.RunNo : "",
                                                     Sunday = cus.SundayShippingAgentCode == "Default" ? cus.RunNo : "",
                                                 },
                                                 ExistingCartID = _db.trnsTempCartMasters.Where(c => c.CustomerID == cus.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == false && c.IsOrderPlpacedByRep == true).FirstOrDefault() == null ? 0 : _db.trnsTempCartMasters.Where(c => c.CustomerID == cus.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == false && c.IsOrderPlpacedByRep == true).FirstOrDefault().CartID,
                                                 CartTotal = _db.trnsTempCartMasters.Where(c => c.CustomerID == cus.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == false && c.IsOrderPlpacedByRep == true).FirstOrDefault() == null ? 0 : _db.trnsTempCartItemsMasters.Where(c => c.CartID == _db.trnsTempCartMasters.Where(cc => cc.CustomerID == cus.CustomerID && cc.RepUserID == model.UserID && cc.IsSavedOrder == false && cc.IsOrderPlpacedByRep == true).FirstOrDefault().CartID).Sum(s => s.Price),
                                                 StandardDeliveryDays = cus.StandardDeliveryDays == null ? "" : cus.StandardDeliveryDays
                                             }).GroupBy(s => s.CustomerID).Select(s => s.FirstOrDefault());

                            var result = customers.OrderBy(o => o.CustomerName).Skip(model.PageIndex * model.PageSize).Take(model.PageSize).ToList();// .Page(model.PageIndex, model.PageSize).ToList();

                            AccountDA objAccount = new AccountDA();
                            foreach (var cus in result)
                            {
                                cus.PermittedDeliveryDays = (cus.StandardDeliveryDays == null || cus.StandardDeliveryDays == "") ? null : _db.PermittedDaysMasters.Where(d => cus.StandardDeliveryDays.Contains(d.Code) && d.IsActive == true).Select(s => s.DayName).ToList();
                                cus.DefaultOrderInfo = await objAccount.GetDefaultOrderingInfo(cus.CustomerID, 0, "Sales");
                            }
                            var runNumbers = _db.ShippingAgents.Where(s => s.IsActive == true).Select(s => s.ShippingAgentCode == null ? "" : s.ShippingAgentCode).ToList();

                            var countryWide = _db.ProductMasters.Where(p => p.RewardItem == true).FirstOrDefault();

                            var defaultDate = GetRequiredDate(HelperClass.GetCurrentTime().ToShortDateString()).ToString("dd/MM/yyyy").Replace("-", "/");
                            var defaultDay = GetRequiredDate(HelperClass.GetCurrentTime().ToShortDateString()).DayOfWeek.ToString();
                            if (countryWide != null)
                            {
                                hasCountryWideRewards = true;
                            }

                            if (model.Debug)
                            {
                                return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                                {
                                    TotalResults = customers.Count(),
                                    Customers = result,
                                    TotalPages = (customers.Count() + model.PageSize - 1) / model.PageSize,
                                    RunNumbers = runNumbers,
                                    CountryWide = hasCountryWideRewards,
                                    DefaultDeliveryDate = defaultDate,
                                    DefaultDeliveryDay = defaultDay
                                });
                            }
                            else
                            {
                                return ReturnMethod(SaaviErrorCodes.Success, "Success", result);
                            }
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NotFound, "Not Found");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Rep customers

        #region Get Rep Customer Pantry lists

        public async Task<APiResponse> GetRepCustomerPantryLists(GetCustomerpantryListModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var pantry = await (from p in _db.PantryListMasters
                                        where p.CustomerID == model.CustomerID
                                        orderby p.PantryListName
                                        select new PantryListBO
                                        {
                                            PantryListID = p.PantryListID,
                                            PantryListName = p.PantryListName,
                                            PantryListType = p.PantryListType,
                                            IsCreatedByRepUser = p.IsCreatedByRepUser,
                                            IsActive = p.IsActive,
                                            IsSynced = p.IsSynced,
                                            CreatedDate = p.CreatedDate
                                        }).OrderBy(o => o.PantryListID).ToListAsync();

                    if (pantry.Count > 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", pantry);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Rep Customer Pantry lists

        #region Get Product details for Sales Rep

        public async Task<APiResponse> GetProductDetails(GetRepProductDetailModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var result = new RepProductDetailBO();

                    StockWareHouse stockwarehouse = await _db.StockWareHouses.Where(s => s.ProductID == model.ProductID).FirstOrDefaultAsync();
                    if (stockwarehouse != null)
                    {
                        var q = (from data in _db.StockWareHouses
                                 where (data.ProductID == model.ProductID)
                                 select new ProductWarehouseBO
                                 {
                                     ProductID = data.ProductID,
                                     StockOnHand = data.IsRwItem == true ? (data.StockOnHandRW == null ? 0 : data.StockOnHandRW) : (data.StockOnHand == null ? 0 : data.StockOnHand),
                                     Warehouse = data.WareHouse == null ? "" : data.WareHouse.Trim()
                                 });
                        result.Warehouses = q.ToList();
                    }
                    if (stockwarehouse == null)
                    {
                        var product = _db.ProductMasters.Where(x => x.ProductID == model.ProductID).FirstOrDefault();
                        result.Warehouses = new List<ProductWarehouseBO>() { new ProductWarehouseBO() {
                                            ProductID = model.ProductID,
                                            StockOnHand = product == null || product.StockOnHand==null ? 0 : product.StockOnHand,
                                            Warehouse ="Default" } };
                    }

                    foreach (OrderedStock orderedstock in _db.OrderedStocks.Where(s => s.ProductID == model.ProductID))
                    {
                        string Warehouse = orderedstock.WareHouse == null ? "" : orderedstock.WareHouse.Trim();
                        if (result.Warehouses.Where(s => s.Warehouse.ToLower() == Warehouse.ToLower()).FirstOrDefault() != null)
                        {
                            result.Warehouses.First(s => s.Warehouse.ToLower() == Warehouse.ToLower()).StockOnOrder = orderedstock.IsRwItem == true ? (orderedstock.StockOnOrderRW == null ? 0 : orderedstock.StockOnOrderRW) : (orderedstock.StockOnOrder == null ? 0 : orderedstock.StockOnOrder);
                            result.Warehouses.First(s => s.Warehouse.ToLower() == Warehouse.ToLower()).OrderDueDate = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime((orderedstock.RequestedReceiptDate == null) ? HelperClass.GetCurrentTime() : orderedstock.RequestedReceiptDate));
                        }
                        else
                        {
                            result.Warehouses.Add(new ProductWarehouseBO()
                            {
                                ProductID = orderedstock.ProductID,
                                StockOnHand = orderedstock.IsRwItem == true ? (orderedstock.StockOnOrderRW == null ? 0 : orderedstock.StockOnOrderRW) : (orderedstock.StockOnOrder == null ? 0 : orderedstock.StockOnOrder),
                                Warehouse = orderedstock.WareHouse == null ? "" : orderedstock.WareHouse,
                                OrderDueDate = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime((orderedstock.RequestedReceiptDate == null) ? HelperClass.GetCurrentTime().AddDays(10) : orderedstock.RequestedReceiptDate))
                            });
                        }
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", result);
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Product details for Sales Rep

        #region Add Special Price

        public async Task<APiResponse> AddSpecialPrice(AddSpecialPriceBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    //StreamReader reader = new StreamReader(JSONdataStream);
                    //string JSONdata = reader.ReadToEnd();

                    var proData = _db.ProductMasters.Where(s => s.ProductID == model.ProductID).FirstOrDefault();
                    var cusData = _db.CustomerMasters.Where(s => s.CustomerID == model.CustomerID).FirstOrDefault();
                    long custID = _db.UserMasters.Find(model.RepUserID).CustomerID.To<long>();
                    var repData = _db.CustomerMasters.Where(s => s.CustomerID == custID).FirstOrDefault();
                    var sPrice = _db.SpecialPriceMasters.Where(s => s.ProductID == model.ProductID && s.CustomerID == model.CustomerID && s.UOMID == null).FirstOrDefault();
                    if (model.UOMID > 0)
                    {
                        sPrice = _db.SpecialPriceMasters.Where(s => s.ProductID == model.ProductID && s.CustomerID == model.CustomerID && s.UOMID == model.UOMID).FirstOrDefault();
                    }
                    string savetype = "U";
                    if (sPrice == null)
                    {
                        sPrice = new SpecialPriceMaster();
                        sPrice.CreatedDate = HelperClass.GetCurrentTime();
                        savetype = "N";
                    }

                    sPrice.ProductID = model.ProductID;
                    sPrice.ModifiedDate = HelperClass.GetCurrentTime();
                    sPrice.CustomerID = model.CustomerID;

                    if (model.QuantityPerUnit > 0)
                    {
                        sPrice.SpecialPrice = model.Price / model.QuantityPerUnit;
                    }
                    else
                    {
                        sPrice.SpecialPrice = model.Price;
                    }

                    sPrice.IsActive = true;
                    sPrice.IsManagerApproved = false;
                    sPrice.IsAlwaysApply = model.AlwaysApply;
                    sPrice.StartDate = HelperClass.GetCurrentTime();
                    sPrice.EndingDate = HelperClass.GetCurrentTime().AddDays(1);

                    if (model.UOMID > 0)
                    {
                        sPrice.UOMID = model.UOMID;
                    }
                    if (savetype == "N")
                    {
                        _db.SpecialPriceMasters.Add(sPrice);
                    }
                    _db.SaveChanges();

                    if (model.IsInCart)
                    {
                        if (model.CartID == 0)
                        {
                            var ct = _db.trnsTempCartMasters.Where(t => t.RepUserID == model.RepUserID && t.IsSavedOrder == false && t.CustomerID == model.CustomerID).FirstOrDefault();
                            if (ct != null)
                            {
                                model.CartID = ct.CartID;
                            }
                        }

                        if (model.CartID > 0)
                        {
                            var cartItem = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.CartID && c.ProductID == model.ProductID).FirstOrDefault();
                            if (cartItem != null)
                            {
                                if (cartItem.UnitId == model.UOMID)
                                {
                                    cartItem.Price = sPrice.SpecialPrice * (model.QuantityPerUnit == 0 ? 1 : model.QuantityPerUnit);
                                    cartItem.IsSpecialPrice = true;

                                    _db.SaveChanges();
                                }
                            }
                        }
                    }

                    if (model.AlwaysApply)
                    {
                        string mailContent = File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplates/specialpriceadmin.htm"));

                        mailContent = mailContent.Replace("@RepName", repData.CustomerName);
                        mailContent = mailContent.Replace("@SalesCode", repData.AlphaCode);
                        mailContent = mailContent.Replace("@CustomerName", repData.CustomerName);
                        mailContent = mailContent.Replace("@ProductCode", proData.ProductCode);
                        mailContent = mailContent.Replace("@ProductDesc", proData.ProductName + "<br/>" + proData.ProductDescription);
                        mailContent = mailContent.Replace("@Price", "$" + HelperClass.formatprice(proData.Price.ToString()));
                        mailContent = mailContent.Replace("@SpecialPrice", "$" + HelperClass.formatprice(model.Price.ToString()));
                        mailContent = mailContent.Replace("@Company", HelperClass.Company);
                        mailContent = mailContent.Replace("@path", HelperClass.WebsiteUrl);

                        SendMail.SendEMail(HelperClass.AdminEmail, "", "", HelperClass.Company + " - Special Price", mailContent, null, HelperClass.FromEmail);
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add Special Price

        #region Save Location

        public async Task<APiResponse> SaveLocation(UserLocationBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var uLocation = new UserLocation();

                    uLocation.UserID = model.UserID;
                    uLocation.Time = HelperClass.GetCurrentTime();
                    uLocation.Latitude = model.Latitude;
                    uLocation.Longitude = model.Longitude;

                    _db.UserLocations.Add(uLocation);

                    _db.SaveChanges();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Save Location

        #region Get Order History

        public async Task<APiResponse> GetOrderHistoryRep(RepOrderHistoryBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var orders = _db.trnsCartMasters.Where(s => s.CustomerID == model.CustomerID).FirstOrDefault();

                    if (orders != null)
                    {
                        var data = await (from or in _db.trnsCartMasters
                                              //join ci in _db.trnsCartItemsMasters on or.CartID equals ci.CartID
                                          join com in _db.DeliveryCommentsMasters on or.CommentID equals com.CommentID into temp
                                          from com in temp.DefaultIfEmpty()
                                          where (or.CustomerID == model.CustomerID && or.IsOrderPlpacedByRep == true)
                                          orderby or.CreatedDate descending
                                          select new OrderHistoryBO
                                          {
                                              CartID = or.CartID,
                                              OrderNumber = or.CartID.ToString() + "/" + (or.SupplierOrderNumber == null ? " - " : or.SupplierOrderNumber),
                                              CommentID = com.CommentID == null ? 0 : com.CommentID,
                                              Comment = com.CommentDescription,
                                              OrderStatus = or.OrderStatus == null ? 1 : or.OrderStatus,
                                              CheckOrderStatus = (or.OrderStatusDescription == null || or.OrderStatusDescription == "") ? "-" : or.OrderStatusDescription,
                                              OrderDateLong = or.CreatedDate ?? DateTime.Now,
                                              IsAutoOrdered = or.IsAutoOrdered ?? false,
                                              Price = ((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == or.CartID)
                                                        select new
                                                        {
                                                            cd.Price,
                                                            cd.Quantity
                                                        }).ToList().Select(c => (c.Price * c.Quantity)).Sum())
                                          }).ToListAsync();

                        if (data.Count > 0)
                        {
                            foreach (var item in data)
                            {
                                item.OrderDate = item.OrderDateLong.To<DateTime>().ToString("yyyy-MM-dd") ?? DateTime.Now.To<DateTime>().ToString("yyyy-MM-dd");
                                item.DateFormatLong = item.OrderDateLong.ToTicks();
                            }
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", data);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Orders found for this customer.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Order History

        #region Add Order comments

        public async Task<APiResponse> AddCommentsRep(List<AddCommentBO> model)
        {
            try
            {
                long commentID = 0;
                long unloadCommentID = 0;

                foreach (var item in model)
                {
                    using (var _db = new Saavi5Entities())
                    {
                        var comment = _db.DeliveryCommentsMasters.Where(c => c.CustomerID == item.CustomerID && c.CommentDescription.ToLower() == item.CommentDescription).FirstOrDefault();

                        if (comment != null)
                        {
                            if (item.IsInvoice)
                            {
                                return ReturnMethod(SaaviErrorCodes.AlreadyExists, "Invoice comment already exists!");
                            }
                            else
                            {
                                return ReturnMethod(SaaviErrorCodes.AlreadyExists, "Unload comment already exists!");
                            }
                        }

                        DeliveryCommentsMaster dcm = new DeliveryCommentsMaster();
                        dcm.CustomerID = item.CustomerID;
                        dcm.CommentDescription = item.CommentDescription;
                        dcm.CompanyID = 1;
                        dcm.IsActive = true;
                        dcm.CreatedDate = DateTime.Now;

                        _db.DeliveryCommentsMasters.Add(dcm);
                        await _db.SaveChangesAsync();

                        if (item.IsInvoice)
                        {
                            commentID = dcm.CommentID;
                        }
                        else
                        {
                            unloadCommentID = dcm.CommentID;
                        }
                    }
                }

                return ReturnMethod(SaaviErrorCodes.Success, "Success", new { CommentID = commentID, UnloadCommentID = unloadCommentID });
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add Order comments

        #region Get Customer delivery Addresses

        public async Task<APiResponse> GetCustomerDeliveryAddresssRep(DeliveryAddressRepBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var addresses = await (from addr in _db.CustomerDeliveryAddresses
                                           join cus in _db.CustomerMasters on addr.CustomerID equals cus.CustomerID
                                           join state in _db.StateMasters on addr.StateID equals state.StateID into tempState
                                           from state in tempState.DefaultIfEmpty()
                                           join count in _db.CountryMasters on addr.CustomerID equals count.CountryID into tempC
                                           from count in tempC.DefaultIfEmpty()
                                           where addr.CustomerID == model.CustomerID && addr.IsActive == true && (addr.Address1 != null || addr.Address1 != "")
                                           orderby addr.CreatedDate descending
                                           select new CustomerAddressBO
                                           {
                                               CustomerId = cus.CustomerID,
                                               AddressId = addr.AddressID,
                                               Address1 = addr.Address1 ?? "",
                                               Address2 = addr.Address2 ?? "",
                                               Address3 = addr.Address3 ?? "",
                                               AddressCode = addr.AddressCode ?? "",
                                               PostCode = addr.ZIP ?? "",
                                               Phone1 = addr.Phone1 ?? "",
                                               Phone2 = addr.Phone2 ?? "",
                                               Phone3 = addr.Phone3 ?? "",
                                               StateName = state.StateName ?? "",
                                               StateId = state.StateID == null ? 0 : state.StateID,
                                               CountryName = count.CountryName == null ? "" : count.CountryName,
                                               CountryId = count.CountryID == null ? 0 : count.CountryID
                                           }).ToListAsync();

                    if (addresses.Count() > 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", addresses);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Addreses found.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message + " Stack Trace: " + ex.InnerException.Message);
            }
        }

        #endregion Get Customer delivery Addresses

        #region Get count for cart items

        public async Task<APiResponse> GetCartCountRep(CartCount model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    double? total = 0;
                    int count = 0;
                    if (model.CartID == 0)
                    {
                        var cart = _db.trnsTempCartMasters.Where(c => c.CustomerID == model.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == model.IsSavedOrder && c.IsOrderPlpacedByRep == model.IsRepUser).FirstOrDefault();

                        if (cart != null)
                        {
                            count = _db.trnsTempCartItemsMasters.Count(c => c.CartID == cart.CartID);

                            if (count > 0)
                            {
                                total = _db.trnsTempCartItemsMasters.Where(t => t.CartID == cart.CartID).Sum(s => s.Price * s.Quantity);
                            }

                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Count = count, TotalPrice = total, CartID = cart.CartID });
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Count = 0, TotalPrice = 0, CartID = 0 });
                        }
                    }
                    else
                    {
                        var ct = _db.trnsTempCartMasters.Find(model.CartID);

                        if (ct != null)
                        {
                            count = _db.trnsTempCartItemsMasters.Count(c => c.CartID == model.CartID);

                            if (count > 0)
                            {
                                total = _db.trnsTempCartItemsMasters.Where(t => t.CartID == model.CartID).Sum(s => s.Price * s.Quantity);
                            }

                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Count = count, TotalPrice = total, CartID = model.CartID });
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Count = 0, TotalPrice = 0, CartID = 0 });
                        }
                    }
                }

                //using (var _db = new Saavi5Entities())
                //{
                //    if (model.CartID == 0)
                //    {
                //        var cart = await _db.trnsTempCartMasters.Where(c => c.CustomerID == model.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == model.IsSavedOrder && c.IsOrderPlpacedByRep == model.IsRepUser).FirstOrDefaultAsync();
                //        if (cart != null)
                //        {
                //            var count = _db.trnsTempCartItemsMasters.Count(c => c.CartID == cart.CartID);
                //            double total = 0;

                //            if (count > 0)
                //            {
                //                var items = _db.trnsTempCartItemsMasters.Where(c => c.CartID == cart.CartID).ToList();

                //                foreach (var prod in items)
                //                {
                //                    var priceData = GetPrice(cart.CustomerID, prod.ProductID, prod.Price.To<double>(), prod.UnitId.To<long>());
                //                    var qty = _db.AltUOMMasters.Where(a => a.UOMID == prod.UnitId && a.ProductID == prod.ProductID).FirstOrDefault();
                //                    if (qty != null)
                //                    {
                //                        prod.Price = priceData.Price * qty.QtyPerUnitOfMeasure ?? 1;
                //                    }
                //                    else
                //                    {
                //                        prod.Price = priceData.Price;
                //                    }
                //                    prod.IsSpecialPrice = priceData.IsSpecial;
                //                }

                //                total = items.Sum(s => s.Price * s.Quantity).To<double>();
                //            }

                //            return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Count = count, TotalPrice = total, CartID = cart.CartID });
                //        }
                //        else
                //        {
                //            return ReturnMethod(SaaviErrorCodes.Success, "No Items in cart", new { Count = 0, TotalPrice = 0, CartID = 0 });
                //        }
                //    }
                //    else
                //    {
                //        var count = _db.trnsTempCartItemsMasters.Count(c => c.CartID == model.CartID);
                //        double total = 0;

                //        if (count > 0)
                //        {
                //            var items = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.CartID).ToList();

                //            foreach (var prods in items)
                //            {
                //                var priceData = GetPrice(model.CustomerID, prods.ProductID, prods.Price.To<double>(), prods.UnitId.To<long>());

                //                var qty = _db.AltUOMMasters.Where(a => a.UOMID == prods.UnitId && a.ProductID == prods.ProductID).FirstOrDefault();
                //                if (qty != null)
                //                {
                //                    prods.Price = priceData.Price * (qty.QtyPerUnitOfMeasure == null ? 1 : qty.QtyPerUnitOfMeasure);
                //                }
                //                else
                //                {
                //                    prods.Price = priceData.Price;
                //                }
                //                prods.IsSpecialPrice = priceData.IsSpecial;
                //            }

                //            total = items.Sum(s => s.Price * s.Quantity).To<double>();
                //        }

                //        // var total = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.CartID).Sum(s => s.Price);
                //        return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Count = count, TotalPrice = total, CartID = model.CartID });
                //    }
                //}
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get count for cart items

        #region Get required dates for Default delivery dates

        private DateTime GetRequiredDate(string OrderDate)
        {
            using (var db = new Saavi5Entities())
            {
                NoticeMaster notice = db.NoticeMasters.FirstOrDefault();
                DateTime requiredDate = DateTime.Now.AddDays(1);
                if (notice != null)
                {
                    string CutOffTime = notice.OrderCutOff;
                    DateTime ordate;
                    bool isorderdateselected = false;
                    if (DateTime.TryParse(OrderDate, out ordate))
                    {
                        if (ordate.Date > DateTime.Now.Date)
                        {
                            requiredDate = ordate;
                            isorderdateselected = true;
                        }
                    }
                    DateTime t1 = Convert.ToDateTime(DateTime.Now);
                    DateTime t2 = Convert.ToDateTime(CutOffTime);

                    if (t1.TimeOfDay.Ticks < t2.TimeOfDay.Ticks && !isorderdateselected)
                    {
                        requiredDate = DateTime.Now;
                    }
                    else
                    {
                        int dct = 0;
                        bool isDayAvailable = true;
                        getrequired:
                        foreach (PermittedDaysMaster dayData in db.PermittedDaysMasters.Where(s => s.IsActive == false))
                        {
                            if (dayData.DayName.ToLower() == requiredDate.DayOfWeek.ToString().ToLower())
                            {
                                requiredDate = requiredDate.AddDays(1);
                                dct++;
                                if (dct <= 7)
                                {
                                    goto getrequired;
                                }
                                else
                                {
                                    isDayAvailable = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                return requiredDate;
            }
        }

        #endregion Get required dates for Default delivery dates

        #region Get Price and price type for products

        private PriceBO GetPrice(long customerId, long productId, double Price, Int64 UOMID, CustomerMaster customer = null)
        {
            using (var _db = new Saavi5Entities())
            {
                PriceBO pricedata = new PriceBO();

                if (customer == null)
                {
                    customer = _db.CustomerMasters.Where(s => s.CustomerID == customerId).FirstOrDefault();
                }
                if (customer == null)
                {
                    pricedata.Price = Price;
                }
                else
                {
                    DateTime date = HelperClass.GetCurrentTime();
                    string CusPriceLevel = customer.PriceLevel == null ? "" : customer.PriceLevel.ToString();
                    var special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == UOMID).FirstOrDefault();
                    if (special == null)
                    {
                        special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == null).FirstOrDefault();
                    }

                    var prlevel_type1 = _db.PriceLevels.Where(s => s.PriceLevel1 == CusPriceLevel && s.ProductID == productId && (s.StartDate == null || s.EndDate == null || (s.EndDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndDate)))).FirstOrDefault();

                    double prc = Price;
                    if (prlevel_type1 != null)
                    {
                        if (prlevel_type1.Price != null && (prlevel_type1.Price < prc || prc == 0))
                        {
                            pricedata.IsPromotional = true;
                            prc = Convert.ToDouble(prlevel_type1.Price);
                        }
                    }
                    pricedata.Price = prc;
                    if (special != null)
                    {
                        double sprc = Convert.ToDouble(special.SpecialPrice == null ? 0 : special.SpecialPrice);
                        //if (sprc < prc || prc == 0)
                        //{
                        pricedata.IsSpecial = true;
                        pricedata.Price = sprc;
                        //}
                    }
                }
                return pricedata;
            }
        }

        public PriceBO GetPrice1(long customerId, long productId, double Price, Int64 UOMID, CustomerMaster customer = null)
        {
            using (var _db = new Saavi5Entities())
            {
                PriceBO pricedata = new PriceBO();
                if (customer == null)
                {
                    customer = _db.CustomerMasters.Where(s => s.CustomerID == customerId).FirstOrDefault();
                }
                if (customer == null)
                {
                    pricedata.Price = Price;
                }
                else
                {
                    string CusPriceLevel = customer.PriceLevel == null ? "" : customer.PriceLevel.ToString();
                    var special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (DateTime.Now >= s.StartDate.Value && DateTime.Now <= s.EndingDate))) && s.UOMID == UOMID).FirstOrDefault();
                    if (special == null)
                    {
                        special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (DateTime.Now >= s.StartDate.Value && DateTime.Now <= s.EndingDate))) && s.UOMID == null).FirstOrDefault();
                    }

                    PriceLevel prlevel_type1 = _db.PriceLevels.Where(s => s.PriceLevel1 == CusPriceLevel && s.Type == 1 && s.ProductID == productId && (s.StartDate == null || s.EndDate == null || (s.EndDate.Value.Year == 1753 || (DateTime.Now >= s.StartDate.Value && DateTime.Now <= s.EndDate)))).FirstOrDefault();
                    PriceLevel prlevel_type3 = _db.PriceLevels.Where(s => s.Type == 3 && s.ProductID == productId && (s.StartDate == null || s.EndDate == null || (s.EndDate.Value.Year == 1753 || (DateTime.Now >= s.StartDate.Value && DateTime.Now <= s.EndDate)))).FirstOrDefault();

                    double type3_Price = (prlevel_type3 != null && prlevel_type3.Price != null) ? Convert.ToDouble(prlevel_type3.Price) : 0;
                    double prc = Price;
                    bool IsSpecial = false;
                    int PriceType = 0; //0 normal, 1 specialprice, 2 specialprice contract, 3 type 3, 4 type 1, 5 type 1 contract
                    if (prlevel_type1 != null && prlevel_type1.ContractPrice == true && special != null && special.ContractPrice == true)
                    {
                        PriceType = 5;
                        prc = Convert.ToDouble(prlevel_type1.Price == null ? 0 : prlevel_type1.Price);

                        if (special.SpecialPrice != null && special.SpecialPrice < prc)
                        {
                            IsSpecial = true;
                            PriceType = 2;
                            prc = Convert.ToDouble(special.SpecialPrice);
                        }

                        if (type3_Price > 0 && type3_Price < prc)
                        {
                            IsSpecial = false;
                            PriceType = 3;
                            prc = type3_Price;
                        }
                    }
                    else if (prlevel_type1 != null && prlevel_type1.ContractPrice == true)
                    {
                        PriceType = 5;
                        prc = Convert.ToDouble(prlevel_type1.Price == null ? 0 : prlevel_type1.Price);

                        if (type3_Price > 0 && type3_Price < prc)
                        {
                            PriceType = 3;
                            prc = type3_Price;
                        }
                    }
                    else if (special != null && special.ContractPrice == true)
                    {
                        IsSpecial = true;
                        PriceType = 2;
                        prc = Convert.ToDouble(special.SpecialPrice == null ? 0 : special.SpecialPrice);

                        if (type3_Price > 0 && type3_Price < prc)
                        {
                            PriceType = 3;
                            IsSpecial = false;
                            prc = type3_Price;
                        }
                    }
                    else
                    {
                        if (special != null)
                        {
                            PriceType = 1;
                            IsSpecial = true;
                            prc = Convert.ToDouble(special.SpecialPrice == null ? 0 : special.SpecialPrice);
                        }
                        if (prlevel_type1 != null)
                        {
                            if (prlevel_type1.Price != null && (prlevel_type1.Price < prc || prc == 0))
                            {
                                PriceType = 4;
                                IsSpecial = false;
                                prc = Convert.ToDouble(prlevel_type1.Price);
                            }
                        }

                        if (type3_Price > 0 && type3_Price < prc)
                        {
                            PriceType = 3;
                            IsSpecial = false;
                            prc = type3_Price;
                        }
                        if (Price > 0 && Price < prc)
                        {
                            PriceType = 0;
                            IsSpecial = false;
                            prc = Price;
                        }
                    }

                    pricedata.Price = prc;
                    if (!IsSpecial && prc != Price)
                    {
                        pricedata.IsPromotional = true;
                    }
                    pricedata.IsSpecial = IsSpecial;
                }
                return pricedata;
            }
        }

        #endregion Get Price and price type for products
    }
}