﻿using AutoMapper;
using CommonFunctions;
using MigraDoc.DocumentObjectModel;
using Newtonsoft.Json;
using PdfSharp;
using PdfSharp.Pdf;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using System.Net.Http;

namespace Saavi5Api.DataAccess.DA
{
    public class PodDA
    {
        #region Common return method

        public APiResponse ReturnMethod(string responseCode, string message, object result = null)
        {
            return new APiResponse
            {
                ResponseCode = responseCode,
                Message = message,
                Result = result
            };
        }

        #endregion Common return method

        #region Get all vehicles from the database.

        public async Task<APiResponse> GetVehiclesRunNo(long driverID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var vehicles = await _db.VehicleMasters.Where(v => v.DriverID == driverID).ToListAsync();
                    var routes = _db.CustomerMasters.Where(s => s.RunNo != null).Select(r => r.RunNo).Distinct().ToList();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Vehicles = vehicles, Routes = routes });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get all vehicles from the database.

        #region Fill vehicle checklist

        public async Task<APiResponse> FillVehicleChecklist(VehicleChecklistBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    long checkListID = 0;
                    if (model.ID == 0)
                    {
                        var checklist = new VehicleChecklistMaster();
                        Mapper.Map(model, checklist);
                        checklist.CreatedDate = HelperClass.GetCurrentTime();
                        _db.VehicleChecklistMasters.Add(checklist);
                        _db.SaveChanges();

                        checkListID = checklist.ID;
                    }
                    else
                    {
                        var checklist = _db.VehicleChecklistMasters.Find(model.ID);

                        if (checklist != null)
                        {
                            checklist.ModifiedDate = HelperClass.GetCurrentTime();
                            checklist.OdometerEnd = model.OdometerEnd;
                            checklist.TempChilledEnd = model.TempChilled;
                            checklist.TempFrozenEnd = model.TempFrozen;
                            checklist.EODComment = model.EODComment;
                            checklist.Fuel = model.Fuel;

                            _db.SaveChanges();

                            checkListID = checklist.ID;

                            // Add the code to sent the PDF to the driver.
                            try
                            {
                                string res = GenerateDeliverySummaryPDF(model.DriverID, checklist);

                                if (res != "ok")
                                {
                                    LogErrorsToServer("Error sending driver suppary report:" + res, "0", "GenerateDeliverySummaryPDF");
                                }
                            }
                            catch (Exception ex)
                            {
                                LogErrorsToServer("Error sending driver suppary report:" + ex.Message, "0", "Error");
                            }
                        }
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { CheckListID = checkListID });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Fill vehicle checklist

        #region Get available delivery days

        public async Task<APiResponse> GetDeliveryDays(long driverID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var date = DateTime.Now.Date;
                    var date2 = DateTime.Now.AddDays(1);

                    // Add a default date to the array
                    var res = new List<DeliveryDaysBO>();
                    string currDate = DateTime.Now.ToString("MM/dd/yyyy");
                    //res.Add(new DeliveryDaysBO { OrderDate = currDate });

                    var dates = await (from d in _db.trnsCartMasters
                                       where (d.OrderDate >= date && d.OrderDate <= DbFunctions.TruncateTime(date2)) && (d.OrderStatus == 6 || d.OrderStatus == 1 || d.OrderStatus == 2)
                                       select new
                                       {
                                           d.OrderDate
                                       }).Distinct().ToListAsync();

                    var result = (from a in dates
                                  select new DeliveryDaysBO
                                  {
                                      OrderDate = Convert.ToDateTime(a.OrderDate).ToString("MM/dd/yyyy").Replace('-', '/')
                                  }).ToList();

                    // merge the array
                    res.AddRange(result);

                    bool exists = false;
                    foreach (var item in result)
                    {
                        if (item.OrderDate == currDate)
                        {
                            exists = true;
                        }
                    }

                    if (!exists)
                    {
                        result.Insert(0, new DeliveryDaysBO { OrderDate = currDate });
                    }

                    if (result.Count >= 2)
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Dates = result.OrderBy(o => o.OrderDate).Take(1) });
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Dates = result });
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get available delivery days

        #region Get all orders from the database for a driver

        public async Task<APiResponse> GetOrdersForDriver(GetDeliveriesModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var driver = _db.UserMasters.Find(model.DriverID);

                    if (driver != null)
                    {
                        driver.Latitude = model.Latitude ?? 0;
                        driver.Longitude = model.Longitude ?? 0;

                        _db.SaveChanges();
                    }

                    DateTime orderDateFrom = DateTime.Now.Date;
                    DateTime orderDateTo = DateTime.Now;
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();

                    bool isSortByGoogle = mainFeatures.IsSortByGoogle ?? false;

                    //int orderstatus = 1;
                    if (!string.IsNullOrEmpty(model.DeliveryDate))
                    {
                        orderDateFrom = Convert.ToDateTime(model.DeliveryDate);
                    }

                    List<int?> orderstatus = new List<int?> { 3, 4 };

                    if (!model.IsCompleted)
                    {
                        if (model.OrderStatus.ToLower() == "all")
                        {
                            orderstatus = new List<int?> { 1, 2, 5, 6 };
                        }
                        else if (model.OrderStatus.ToLower() == "pending")
                        {
                            orderstatus = new List<int?> { 1 };
                        }
                        else if (model.OrderStatus.ToLower() == "ready")
                        {
                            //orderstatus = new List<int?> { 1, 2, 5 }; removed delivered orders from ready query
                            orderstatus = new List<int?> { 1, 2 };
                        }
                        else
                        {
                            orderstatus = new List<int?> { 1, 2, 6, 5 };
                        }
                    }

                    var allOrder = _db.trnsCartMasters.Count(c => c.OrderDate == orderDateFrom && c.DriverID == model.DriverID);
                    var date = DateTime.Now;
                    var dateFrom = DateTime.Now.AddDays(1);

                    var suburbs = (from a in _db.trnsCartMasters
                                   join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID
                                   where a.DriverID == model.DriverID && a.OrderDate >= DbFunctions.TruncateTime(date) && a.OrderDate <= DbFunctions.TruncateTime(dateFrom) //&& runs.Contains(a.RunNo)
                                   select new
                                   {
                                       b.Suburb
                                   }).Distinct().ToArray();

                    var deliveries = (from a in _db.trnsCartMasters
                                      join c in _db.CustomerMasters on a.CustomerID equals c.CustomerID
                                      join d in _db.DeliveryCommentsMasters on a.CommentID equals d.CommentID into temp
                                      from d in temp.DefaultIfEmpty()
                                      join add in _db.CustomerDeliveryAddresses on a.AddressID equals add.AddressID into tempAdd
                                      from add in tempAdd.DefaultIfEmpty()
                                      where a.DriverID == model.DriverID
                                      && ((model.Searchtext != "" || model.OrderID != 0) || orderstatus.Contains(a.OrderStatus))
                                      && (DbFunctions.TruncateTime(a.OrderDate) == DbFunctions.TruncateTime(orderDateFrom))
                                      && (model.Searchtext == "" || c.CustomerName.Contains(model.Searchtext) || c.AlphaCode.Contains(model.Searchtext))
                                      && (model.OrderID == 0 || a.CartID == model.OrderID)
                                      && (model.Suburb == "" || c.Suburb == model.Suburb)

                                      // && (a.InvoiceNumber != null || a.InvoiceNumber != "")
                                      select new DriverDeliveriesBO
                                      {
                                          OrderID = a.CartID,
                                          CustomerName = (add.AddressID != null) ? add.ContactName : c.CustomerName,
                                          Email = c.Email,
                                          Address1 = (add.AddressID != null) ? (add.Address1 ?? "") + ", " + (add.Suburb ?? "") + ", " + (add.ZIP ?? "") + ", " : c.Address1 + ", " + c.Suburb,
                                          Address2 = (add.AddressID != null) ? (add.Address2 ?? "") : a.AddressID > 0 ? add.Address2 : c.Address2,
                                          Phone = (add.AddressID != null) ? add.Phone1 : c.Phone1,
                                          Postcode = add.AddressID != null ? add.ZIP : c.PostCode,
                                          State = add.StateID != null ? _db.StateMasters.Where(s => s.StateID == add.StateID).FirstOrDefault().StateName : "",
                                          Status = a.PackagingStatus,
                                          Suburb = c.Suburb ?? "",
                                          Alphacode = c.AlphaCode,
                                          ContactName = c.ContactName,
                                          RequestedDeliveryDate = a.OrderDate,
                                          CustomerOrderNo = a.ExtDoc,
                                          InvoiceNo = a.InvoiceNumber,
                                          DropSequence = c.DROP_SEQUENCE ?? "0",
                                          DeliveryComment = d.CommentDescription ?? "",
                                          SortOrder = a.DriverSortOrder,
                                          OrderStatus = a.SupplierOrderNumber ?? "Processing", // a.OrderStatus == null ? "" : a.OrderStatus == 3 ? "Completed" : a.OrderStatus == 4 ? "Cancelled" : a.OrderStatus == 2 ? "In Transit" : a.OrderStatus == 5 ? "In Progress" : a.OrderStatus == 6 ? "Released" : a.OrderStatus == 9 ? "Picked" : "Open",
                                          // below section will be null if order is Open Or cancelled
                                          Latitude = a.Latitude ?? "",
                                          Longitude = a.Longitude ?? "",
                                          DeliveryDetails = (from d in _db.DeliveryStatusMasters
                                                             where d.DelRunNumber == a.CartID.ToString()
                                                             && ((model.Searchtext != "" || model.OrderID != 0 || d.HasItemIssues == true) || model.IsCompleted == true)
                                                             select new DeliveryStatusCompModel
                                                             {
                                                                 DeliveryNo = a.CartID.ToString(),
                                                                 DeliveryID = d.DeliveryID,
                                                                 CancelReason = d.CancelReason,
                                                                 GoodsTemp = d.GoodsTemp,
                                                                 Comments = d.Comments,
                                                                 EndTime = d.EndTime,
                                                                 IsTempAuto = d.IsTempAuto,
                                                                 HasIssues = d.HasItemIssues,
                                                                 IsCancelled = d.IsCancelled,
                                                                 LoadedChilled = d.LoadedChilled,
                                                                 LoadedDry = d.LoadedDry,
                                                                 LoadedFrozen = d.LoadedFrozen,
                                                                 PaymentAmount = d.PaymentAmount,
                                                                 PaymentMethod = d.PaymentMethod,
                                                                 ReceiversName = d.ReveiverName,
                                                                 StartTime = d.StartTime,
                                                                 VehicleID = d.VehicleID,
                                                                 CartonNumber = d.CartonNumber ?? 0,
                                                                 SignatureImage = _db.DeliveryStatusImageMasters.Where(im => im.DeliveryID == d.DeliveryID && im.IsSignature == true).FirstOrDefault() == null ? "" : HelperClass.PODImageUrl + a.CartID.ToString() + "/" + _db.DeliveryStatusImageMasters.Where(im => im.DeliveryID == d.DeliveryID && im.IsSignature == true).FirstOrDefault().ImageName,
                                                                 ItemDetails = (from t in _db.trnsCartItemsMasters
                                                                                join p in _db.ProductMasters on t.ProductID equals p.ProductID
                                                                                //join i in _db.DeliveryIssuesMasters on t.ProductID equals i.ProductID into temp
                                                                                //from i in temp.DefaultIfEmpty()
                                                                                where t.CartID == a.CartID
                                                                                select new ItemDetailsBO
                                                                                {
                                                                                    ProductID = p.ProductID,
                                                                                    ProductCode = p.ProductCode,
                                                                                    ProductName = p.ProductName,
                                                                                    IssueDescription = _db.DeliveryIssuesMasters.Where(dl => dl.DeliveryID == d.DeliveryID && dl.ProductID == p.ProductID).FirstOrDefault() == null ? "" : _db.DeliveryIssuesMasters.Where(dl => dl.DeliveryID == d.DeliveryID && dl.ProductID == p.ProductID).FirstOrDefault().Description,
                                                                                    IsIssue = _db.DeliveryIssuesMasters.Where(dl => dl.DeliveryID == d.DeliveryID && dl.ProductID == p.ProductID).FirstOrDefault() == null ? false : true,
                                                                                    AmmendedQuantity = _db.DeliveryIssuesMasters.Where(dl => dl.DeliveryID == d.DeliveryID && dl.ProductID == p.ProductID).FirstOrDefault() == null ? 0 : _db.DeliveryIssuesMasters.Where(dl => dl.DeliveryID == d.DeliveryID && dl.ProductID == p.ProductID).FirstOrDefault().AmmendQuantity,
                                                                                    OrderedQuantity = t.Quantity
                                                                                }).ToList()
                                                             }).FirstOrDefault()
                                      }).OrderBy(o => o.SortOrder);

                    if (model.Searchtext != "" || model.OrderID != 0)
                    {
                        deliveries = deliveries.OrderBy("RequestedDeliveryDate", true);
                    }
                    else
                    {
                        deliveries = deliveries.OrderBy("DropSequence", false);
                    }

                    if (model.IsCompleted)
                    {
                        deliveries = deliveries.OrderBy("RequestedDeliveryDate", true).ThenBy("OrderStatus", true);
                    }
                    var result = new List<DriverDeliveriesBO>();

                    if (model.ShowAll)
                    {
                        if (isSortByGoogle)
                        {
                            result = CalcualteDistance(deliveries, model.Latitude, model.Longitude);
                        }
                        else
                        {
                            result = await deliveries.ToListAsync();
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                        {
                            Deliveries = result,
                            TotalResults = deliveries.Count(),
                            TotalPages = 1,
                            allresults = allOrder,
                            Suburbs = suburbs
                        });
                    }
                    else
                    {
                        if (isSortByGoogle)
                        {
                            result = CalcualteDistance(deliveries, model.Latitude, model.Longitude);
                            result = result.Page(model.PageIndex * model.PageSize, model.PageSize).ToList();
                        }
                        else
                        {
                            result = deliveries.Page(model.PageIndex * model.PageSize, model.PageSize).ToList();
                        }

                        if (result != null && result.Count > 0)
                        {
                            foreach (var item in result)
                            {
                                int QTY = 0;
                                int OtherQTY = 0;
                                var items = _db.trnsCartItemsMasters.Where(x => x.CartID == item.OrderID).ToList();
                                foreach (var prod in items)
                                {
                                    if (prod.UnitId == 1)
                                    {
                                        if ((prod.ProductID == 4 || prod.ProductID == 6))
                                        {
                                            QTY += (prod.Quantity * 2).To<int>();
                                        }
                                        else
                                        {
                                            QTY += (int)prod.Quantity;
                                        }
                                    }
                                    else
                                    {
                                        OtherQTY += (int)prod.Quantity;
                                    }
                                }

                                int nume = Convert.ToInt32(OtherQTY) / 7;
                                int deno = Convert.ToInt32(OtherQTY) % 7;

                                if (deno != 0 && deno <= 7)
                                { QTY += 1; }

                                if (nume != 0)
                                {
                                    QTY = QTY + nume;
                                }

                                item.TotalItems = QTY;
                            }
                        }
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                        {
                            Deliveries = result,
                            TotalResults = deliveries.Count(),
                            TotalPages = (deliveries.Count() + model.PageSize - 1) / model.PageSize,
                            allresults = allOrder,
                            Suburbs = suburbs
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        /// <summary>
        /// Calculate the diatance between origin and delivery addresses. Origin being the live location of the driver.
        /// </summary>
        /// <param name="deliveries"></param>
        /// <param name="lat">Live location latitude</param>
        /// <param name="lng">Live location longitude</param>
        /// <returns></returns>
        public List<DriverDeliveriesBO> CalcualteDistance(IOrderedQueryable<DriverDeliveriesBO> deliveries, float? lat, float? lng)
        {
            try
            {
                var allDeliveries = deliveries.ToList();
                var resultDel = new List<DriverDeliveriesBO>();
                string destinations = "";
                var cnt = allDeliveries.Count;

                if (cnt > 35)
                {
                    int totalSze = (cnt + 34) / 35;

                    for (int i = 0; i < totalSze; i++)
                    {
                        destinations = "";
                        var divided = allDeliveries.Page(i * 35, 35).ToList();
                        foreach (var del in divided)
                        {
                            if (del.Latitude != null && del.Latitude != "")
                            {
                                destinations += del.Latitude + "," + del.Longitude + "|";
                            }
                        }

                        destinations = destinations.TrimEnd('|');

                        if (destinations != "")
                        {
                            HttpClient client = new HttpClient();
                            string url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins={0},{1}&destinations={2}&key={3}";
                            var response = client.GetAsync(string.Format(url, lat.ToString(), lng.ToString(), destinations, HelperClass.GoogleAPI)).Result;

                            if (response.IsSuccessStatusCode)
                            {
                                var result = JsonConvert.DeserializeObject<DistanceCalc>(response.Content.ReadAsStringAsync().Result);

                                var dists = result.rows[0].elements;

                                for (int j = 0; j < divided.Count(); j++)
                                {
                                    if (dists[j].status == "OK")
                                    {
                                        divided[j].Distance = dists[j].distance.text.Replace(" mi", "").Replace(" ft", "").To<float>();
                                    }
                                }

                                resultDel.AddRange(divided);
                            }
                        }
                        else
                        {
                            resultDel.AddRange(divided);
                        }
                    }
                }
                else
                {
                    foreach (var del in allDeliveries)
                    {
                        if (del.Latitude != null && del.Latitude != "")
                        {
                            destinations += del.Latitude + "," + del.Longitude + "|";
                        }
                    }

                    destinations = destinations.TrimEnd('|');

                    if (destinations != "")
                    {
                        HttpClient client = new HttpClient();
                        string url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins={0},{1}&destinations={2}&key={3}";
                        var response = client.GetAsync(string.Format(url, lat.ToString(), lng.ToString(), destinations, HelperClass.GoogleAPI)).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var result = JsonConvert.DeserializeObject<DistanceCalc>(response.Content.ReadAsStringAsync().Result);

                            var dists = result.rows[0].elements;

                            for (int j = 0; j < allDeliveries.Count(); j++)
                            {
                                if (dists[j].status == "OK")
                                {
                                    allDeliveries[j].Distance = dists[j].distance.text.Replace(" mi", "").Replace(" ft", "").To<float>();
                                }
                            }

                            resultDel.AddRange(allDeliveries);
                        }
                    }
                    else
                    {
                        resultDel.AddRange(allDeliveries);
                    }
                }

                //var groupBySub = resultDel.GroupBy(g => g.Suburb).Select(x => x.OrderBy(o => o.Distance).FirstOrDefault()).OrderBy(o => o.Distance).ToList();
                //var groupedBySUbResult = new List<DriverDeliveriesBO>();
                //foreach (var item in groupBySub)
                //{
                //    var fetch = resultDel.Where(r => r.Suburb == item.Suburb).OrderBy(o => o.Distance).ToList();
                //    groupedBySUbResult.AddRange(fetch);
                //}

                return resultDel.OrderBy(o => o.Distance).ToList();
            }
            catch (Exception ex)
            {
                return deliveries.ToList();
            }
        }

        #endregion Get all orders from the database for a driver

        #region Get delivery details from OrderID

        /// <summary>
        /// Get delivery ddetails from OrderID
        /// </summary>
        /// <param name="model">OrderID</param>
        /// <returns></returns>
        public async Task<APiResponse> GetDeliveryDetails(DeliveryDetailModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    string oID = model.OrderID.ToString();

                    var data = await (from a in _db.trnsCartMasters
                                      join c in _db.CustomerMasters on a.CustomerID equals c.CustomerID
                                      join b in _db.trnsCartItemsMasters on a.CartID equals b.CartID
                                      join p in _db.ProductMasters on b.ProductID equals p.ProductID
                                      where a.CartID == model.OrderID
                                      select new DeliveryDetailsBO
                                      {
                                          ProductID = p.ProductID,
                                          ProductCode = p.ProductCode,
                                          Description = p.ProductName,
                                          Quantity = b.Quantity,
                                          TotalAmount = b.Quantity * b.Price
                                      }).ToListAsync();

                    var totalAmount = data.Sum(s => s.TotalAmount);

                    var cus = _db.trnsCartMasters.Where(c => c.CartID == model.OrderID).FirstOrDefault().CustomerID;
                    var custAddress = new DriverDeliveriesBO();
                    string driverComment = "";
                    if (cus > 0)
                    {
                        custAddress = (from a in _db.trnsCartMasters
                                       join c in _db.CustomerMasters on a.CustomerID equals c.CustomerID
                                       join d in _db.DeliveryCommentsMasters on a.CommentID equals d.CommentID into temp
                                       from d in temp.DefaultIfEmpty()
                                       join add in _db.CustomerDeliveryAddresses on a.AddressID equals add.AddressID into tempAdd
                                       from add in tempAdd.DefaultIfEmpty()
                                       where a.CartID == model.OrderID
                                       select new DriverDeliveriesBO
                                       {
                                           OrderID = a.CartID,
                                           CustomerName = c.CustomerName,
                                           Email = c.Email,
                                           Address1 = c.Address1 + ", " + c.Suburb,
                                           Address2 = a.AddressID > 0 ? add.Address2 : c.Address2,
                                           Phone = c.Phone1,
                                           Postcode = a.AddressID > 0 ? add.ZIP : c.PostCode,
                                           State = _db.StateMasters.Where(s => s.StateID == c.StateID).FirstOrDefault().StateName ?? "",
                                           Status = a.PackagingStatus,
                                           OrderStatus = a.SupplierOrderNumber ?? "Processing", // a.OrderStatus == null ? "" : a.OrderStatus == 3 ? "Completed" : a.OrderStatus == 4 ? "Cancelled" : a.OrderStatus == 2 ? "In Transit" : a.OrderStatus == 5 ? "In Progress" : a.OrderStatus == 6 ? "Released" : a.OrderStatus == 9 ? "Picked" : "Open",
                                           Alphacode = c.AlphaCode,
                                           ContactName = c.ContactName,
                                           RequestedDeliveryDate = a.OrderDate,
                                           CustomerOrderNo = a.ExtDoc,
                                           InvoiceNo = a.InvoiceNumber,
                                           DeliveryComment = (d.CommentDescription ?? ""),// + (a.CommentLine != null ? " | " + a.CommentLine : ""),
                                           TotalItems = _db.trnsCartItemsMasters.Count(c => c.CartID == a.CartID),
                                           SortOrder = a.DriverSortOrder,
                                           DeliveryID = a.OrderStatus == 2 ? _db.DeliveryStatusMasters.Where(v => v.DelRunNumber == oID).FirstOrDefault().DeliveryID : 0
                                       }).OrderBy(o => o.SortOrder).FirstOrDefault();

                        var dc = _db.trnsCartMasters.Where(d => d.CartID == model.OrderID).FirstOrDefault();
                        if (dc != null)
                        {
                            var cmt = _db.DeliveryCommentsMasters.Where(d => d.CommentID == dc.DeliveredCommentID).FirstOrDefault();
                            if (cmt != null)
                            {
                                driverComment = cmt.CommentDescription;
                            }
                        }
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success",
                        new
                        {
                            Details = data,
                            CustAddress = custAddress,
                            InvoiceTotal = totalAmount,
                            drivercomment = driverComment
                        });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get delivery details from OrderID

        #region Sort Driver Deliveries

        /// <summary>
        /// Sort Driver deliveries based on Date and index.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<APiResponse> SetDeliverySortOrder(DeliverySortBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    DateTime orderDateFrom = DateTime.Now.Date;
                    DateTime orderDateTo = DateTime.Now;
                    if (!string.IsNullOrEmpty(model.DeliveryDate))
                    {
                        orderDateFrom = Convert.ToDateTime(model.DeliveryDate);
                    }

                    var pantry = await _db.trnsCartMasters.Where(c => c.OrderDate == orderDateFrom && c.DriverID == model.DriverID).ToListAsync();

                    if (pantry != null)
                    {
                        int index = 0;
                        foreach (var orderID in model.OrderIDs)
                        {
                            var orders = _db.trnsCartMasters.Find(orderID);

                            if (orders != null)
                            {
                                orders.DriverSortOrder = index;
                                _db.SaveChanges();
                                index++;
                            }
                        }
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Driver delivery order saved.");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Sort Driver Deliveries

        #region Send Message to User

        /// <summary>
        /// Send message to the user who places the order.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<APiResponse> SendMessage(SendMessageModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var userID = _db.trnsCartMasters.Find(model.OrderID).UserID;

                    var message = new PushMessageMaster();
                    message.UserID = userID;
                    message.Title = "POD-Notification-" + model.OrderID.ToString();
                    message.Message = model.Message;
                    message.CreatedDate = DateTime.Now;

                    _db.PushMessageMasters.Add(message);

                    await _db.SaveChangesAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Send Message to User

        #region Start/update/cancel a delivery

        /// <summary>
        /// Insert / Update delivery related data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<APiResponse> StartUpdateDelivery(StartUpdateDeliveryModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    long oID = model.DeliveryDetails.DeliveryNo.To<long>();
                    var order = _db.trnsCartMasters.Where(c => c.CartID == oID).FirstOrDefault();
                    var cust = _db.CustomerMasters.Find(order.CustomerID);
                    var driver = _db.UserMasters.Find(model.DeliveryDetails.DriverID);

                    string sub = "";
                    string message = "";

                    if (model.IsDeliveryStart)
                    {
                        var delivery = new DeliveryStatusMaster();

                        delivery.DriverID = model.DeliveryDetails.DriverID;
                        delivery.VehicleID = model.DeliveryDetails.VehicleID;
                        delivery.DelRunNumber = model.DeliveryDetails.DeliveryNo;
                        delivery.StartTime = DateTime.Now;
                        delivery.LoadedChilled = model.DeliveryDetails.LoadedChilled;
                        delivery.LoadedDry = model.DeliveryDetails.LoadedDry;
                        delivery.LoadedFrozen = model.DeliveryDetails.LoadedFrozen;
                        delivery.IsCompleted = false;
                        delivery.IsCancelled = false;
                        delivery.OrderID = model.DeliveryDetails.DeliveryNo.To<long>();
                        delivery.CartonNumber = model.DeliveryDetails.CartonNumber;
                        _db.DeliveryStatusMasters.Add(delivery);
                        _db.SaveChanges();

                        order.OrderStatus = 2; // 4 = In transit
                        order.SupplierOrderNumber = "Delivering";
                        _db.SaveChanges();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new { DeliveryID = delivery.DeliveryID });
                    }
                    else
                    {
                        var delivery = _db.DeliveryStatusMasters.Find(model.DeliveryDetails.DeliveryID);
                        delivery.EndTime = DateTime.Now;

                        if (model.DeliveryDetails.IsCancelled)
                        {
                            delivery.IsCancelled = true;
                            delivery.CancelReason = model.DeliveryDetails.CancelReason;
                            delivery.IssueCredit = model.DeliveryDetails.IssueCredit;
                            _db.SaveChanges();

                            if (order != null)
                            {
                                order.OrderStatus = 4; // 4 = Cancelled
                                order.SupplierOrderNumber = "Cancelled";
                                _db.SaveChanges();

                                sub = "Delivery Issue raised for " + cust.CustomerName + " on the " + order.RunNo + " Run";
                                message = "Please be advised that driver " + driver.FirstName + " " + driver.LastName + " has raised a delivery issue for invoice number <b>" + order.InvoiceNumber + "</b> and the delivery issue reason given was <b style='color:red'>" + model.DeliveryDetails.CancelReason + "</b>";

                                try
                                {
                                    var deliveryIssueImages = (from a in _db.DeliveryStatusMasters
                                                               join b in _db.DeliveryStatusImageMasters on a.DeliveryID equals b.DeliveryID
                                                               where a.OrderID == order.CartID //&& b.ProductID == d.ProductID
                                                               select new ProductIssuesList
                                                               {
                                                                   Images = b.IsSignature == true ? "" : b.BaseUrl + a.OrderID + "/" + b.ImageName,
                                                                   Reason = model.DeliveryDetails.CancelReason,
                                                                   Product = ""
                                                               }).ToList();
                                    string issueTable = "";
                                    if (deliveryIssueImages.Count > 0)
                                    {
                                        issueTable += "<br><br><u><b>Photos Taken:</b></u>";
                                        issueTable += "<br><br><table width='100%'><tr><td>Reason</td><td>Product</td><td>Image</td></tr>";
                                        foreach (var item in deliveryIssueImages)
                                        {
                                            issueTable += "<tr><td>" + item.Reason + "</td><td>" + item.Product + "</td><td>";
                                            if (item.Images == "")
                                            {
                                                issueTable += "No Image Added</td></tr>";
                                            }
                                            else
                                            {
                                                issueTable += "<img src='" + item.Images + "' height='200' /></td></tr>";
                                            }
                                        }
                                        issueTable += "</table>";

                                        message = message + issueTable;
                                    }

                                    // SendMail.SendEMail(HelperClass.DeliveryIssueEmail, "", "", sub, message, "", HelperClass.FromEmail);

                                    if (model.DeliveryDetails.IssueCredit)
                                    {
                                        sub = "Credit Request and Item Issue raised for " + cust.CustomerName + " on the " + order.RunNo + " Run";
                                        string msg = "Please be advised that driver " + driver.FirstName + " " + driver.LastName + " completed the delivery with item issues for invoice number  <b>" + order.InvoiceNumber + "</b> and the item issue reasons/Images are given below. " + driver.FirstName + " " + driver.LastName + " has also requested a credit for these item(s)";
                                        msg = msg + issueTable;
                                        SendMail.SendEMail(HelperClass.CreditIssueEmail, "", "", sub, msg, "", HelperClass.FromEmail);
                                    }
                                    else
                                    {
                                        SendMail.SendEMail(HelperClass.DeliveryIssueEmail, "", "", sub, message, "", HelperClass.FromEmail);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogErrorsToServer("Error sending delivery issue email:: " + ex.Message, model.DeliveryDetails.DeliveryID.ToString(), "Delivery Issue");
                                }
                            }
                        }
                        else
                        {
                            delivery.PaymentAmount = model.DeliveryDetails.PaymentAmount;
                            delivery.PaymentMethod = model.DeliveryDetails.PaymentMethod;
                            delivery.ReveiverName = model.DeliveryDetails.ReceiversName;
                            delivery.HasItemIssues = model.DeliveryDetails.HasIssues;
                            delivery.GoodsTemp = model.DeliveryDetails.GoodsTemp;
                            delivery.IsTempAuto = model.DeliveryDetails.IsTempAuto;
                            delivery.Comments = model.DeliveryDetails.Comments;
                            delivery.CartonNumber = model.DeliveryDetails.CartonNumber;
                            delivery.Latitude = model.DeliveryDetails.Latitude;
                            delivery.Longitude = model.DeliveryDetails.Longitude;
                            delivery.IsCompleted = true;
                            delivery.IssueCredit = model.DeliveryDetails.IssueCredit;

                            if (model.DeliveryDetails.PaymentMethod.ToLower() == "cash")
                            {
                                delivery.Cent10 = model.DeliveryDetails.Cent10 ?? 0;
                                delivery.Cent5 = model.DeliveryDetails.Cent5 ?? 0;
                                delivery.Cent20 = model.DeliveryDetails.Cent20 ?? 0;
                                delivery.Cent50 = model.DeliveryDetails.Cent50 ?? 0;
                                delivery.Dollar1 = model.DeliveryDetails.Dollar1 ?? 0;
                                delivery.Dollar2 = model.DeliveryDetails.Dollar2 ?? 0;
                                delivery.Dollar5 = model.DeliveryDetails.Dollar5 ?? 0;
                                delivery.Dollar10 = model.DeliveryDetails.Dollar10 ?? 0;
                                delivery.Dollar20 = model.DeliveryDetails.Dollar20 ?? 0;
                                delivery.Dollar50 = model.DeliveryDetails.Dollar50 ?? 0;
                                delivery.Dollar100 = model.DeliveryDetails.Dollar100 ?? 0;
                            }

                            if (model.DeliveryIssues != null && model.DeliveryIssues.Count > 0)
                            {
                                foreach (var issue in model.DeliveryIssues)
                                {
                                    var delIssue = new DeliveryIssuesMaster();

                                    delIssue.ProductID = issue.ProductID;
                                    delIssue.DeliveryID = delivery.DeliveryID;
                                    delIssue.Description = issue.IssueDescription;
                                    delIssue.CreatedDate = DateTime.Now;
                                    delIssue.AmmendQuantity = issue.Quantity;
                                    _db.DeliveryIssuesMasters.Add(delIssue);
                                    _db.SaveChanges();
                                }

                                sub = "Item Issue raised for " + cust.CustomerName + " on the " + order.RunNo + " Run";
                                message = "Please be advised that driver " + driver.FirstName + " " + driver.LastName + " has completed the delivery with item issues for invoice number <b>" + order.InvoiceNumber + "</b> and the item issue reasons are given below.";

                                try
                                {
                                    string orderN = oID.ToString();

                                    var deliveryIssueImages = (from a in _db.DeliveryStatusMasters
                                                               join b in _db.DeliveryStatusImageMasters on a.DeliveryID equals b.DeliveryID //into tempAdd from b in tempAdd.DefaultIfEmpty()
                                                               join c in _db.DeliveryIssuesMasters on a.DeliveryID equals c.DeliveryID
                                                               join d in _db.ProductMasters on c.ProductID equals d.ProductID
                                                               join e in _db.trnsCartItemsMasters on a.OrderID equals e.CartID
                                                               where a.OrderID == order.CartID && b.ProductID == d.ProductID
                                                               select new ProductIssuesList
                                                               {
                                                                   Images = b.BaseUrl == null ? "" : b.BaseUrl + orderN + "/" + b.ImageName,
                                                                   Reason = c.Description,
                                                                   Product = d.ProductName,
                                                                   Price = e.Price,
                                                                   Quantity = e.Quantity
                                                               }).ToList();

                                    string issueTable = "";
                                    if (deliveryIssueImages.Count() > 0)
                                    {
                                        issueTable += "<br><br><u><b>Photos Taken:</b></u>";
                                        issueTable += "<br><br><table width='100%' border='1'><tr><td>Reason</td><td>Product</td><td>Quantity</td><td>Price</td><td>Image</td></tr>";
                                        foreach (var item in deliveryIssueImages)
                                        {
                                            issueTable += "<tr><td>" + item.Reason + "</td><td>" + item.Product + "</td><td>" + item.Quantity + "</td><td>" + string.Format("{0:#,0.00}", item.Price) + "</td><td>";
                                            if (item.Images == "")
                                            {
                                                issueTable += "No Image Added</td></tr>";
                                            }
                                            else
                                            {
                                                issueTable += "<img src='" + item.Images + "' height='100' /></td></tr>";
                                            }
                                        }
                                        issueTable += "</table>";

                                        message = message + issueTable;
                                    }

                                    if (model.DeliveryDetails.IssueCredit)
                                    {
                                        sub = "Credit Request and Item Issue raised for " + cust.CustomerName + " on the " + order.RunNo + " Run";
                                        string msg = "Please be advised that driver " + driver.FirstName + " " + driver.LastName + " completed the delivery with item issues for invoice number  <b>" + order.InvoiceNumber + "</b> and the item issue reasons/Images are given below. " + driver.FirstName + " " + driver.LastName + " has also requested a credit for these item(s)";
                                        msg = msg + issueTable;
                                        SendMail.SendEMail(HelperClass.CreditIssueEmail, "", "", sub, msg, "", HelperClass.FromEmail);
                                    }
                                    else
                                    {
                                        SendMail.SendEMail(HelperClass.DeliveryIssueEmail, "", "", sub, message, "", HelperClass.FromEmail);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogErrorsToServer("Error sending delivery issue email:: " + ex.Message, model.DeliveryDetails.DeliveryID.ToString(), "Delivery Issue");
                                }
                            }
                            await _db.SaveChangesAsync();

                            if (order != null)
                            {
                                order.OrderStatus = 5; // 3 = Completed
                                order.SupplierOrderNumber = "Delivered";
                                _db.SaveChanges();

                                try
                                {
                                    DeliveryConfirmationPDF(order.InvoiceNumber, model.DeliveryDetails.DeliveryID);
                                }
                                catch (Exception ex)
                                {
                                    LogErrorsToServer("Error sending delivery Summary email:: " + ex.Message, model.DeliveryDetails.DeliveryID.ToString(), "Delivery summary Issue");
                                }
                            }
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new { DeliveryID = delivery.DeliveryID });
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Start/update/cancel a delivery

        #region Add Tracking Data

        /// <summary>
        /// Add tracking data for Admin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<APiResponse> AddTrackingData(TrackingModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var track = new DeliveryTrackingMaster();
                    track.DeliveryID = model.DeliveryID;
                    track.Latitutde = model.Latitude;
                    track.Longitude = model.Longitude;
                    track.TimeStamp = DateTime.Now;

                    _db.DeliveryTrackingMasters.Add(track);
                    _db.SaveChanges();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add Tracking Data

        #region Upload Delivery Images

        public async Task<APiResponse> UploadDeliveryImages(List<DeliveryImagesArray> images)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (images != null)
                    {
                        foreach (var item in images)
                        {
                            var di = new DeliveryStatusImageMaster();

                            di.ImageName = item.ImageName;
                            di.BaseUrl = HelperClass.PODImageUrl;
                            di.IsSignature = item.IsSignature;
                            di.DeliveryID = item.DeliveryID;
                            di.IsIssue = item.IsIssue;
                            di.ProductID = item.ProductID;
                            _db.DeliveryStatusImageMasters.Add(di);
                            await _db.SaveChangesAsync();
                        }
                    }
                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Upload Delivery Images

        #region Update driver settings

        public async Task<APiResponse> UpdateSettings(SettingsBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var user = _db.UserMasters.Find(model.DriverID);

                    if (user != null)
                    {
                        user.IsNotificationEnabled = model.IsNotificationEnabled;
                        await _db.SaveChangesAsync();
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Update driver settings

        #region Get Images for an order

        public async Task<APiResponse> GetImagesForDelivery(GalleryBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    long deliveryID = 0;
                    var delivery = _db.DeliveryStatusMasters.Where(d => d.DelRunNumber == model.DeliveryNo).FirstOrDefault();
                    if (delivery != null)
                    {
                        deliveryID = delivery.DeliveryID;
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No delivery found for the provided delivery run no.");
                    }

                    var images = await _db.DeliveryStatusImageMasters.Where(d => d.DeliveryID == deliveryID).ToListAsync();

                    if (images != null)
                    {
                        var imagesLi = (from a in images
                                        select new
                                        {
                                            ImageName = HelperClass.PODImageUrl + model.DeliveryNo + "/" + a.ImageName,
                                            IsSignature = a.IsSignature
                                        }).ToList();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Images = imagesLi });
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Images for the Delivery Run No");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Images for an order

        #region Upload Driver Image

        public async Task<APiResponse> UploadDriverImage(DriverImageBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (model != null)
                    {
                        var user = _db.UserMasters.Find(model.DriverID);
                        user.Image = model.ImageName;

                        await _db.SaveChangesAsync();
                    }
                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Image = HelperClass.PODImageUrl + model.ImageName });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Upload Driver Image

        #region Update driver online Status

        public async Task<APiResponse> UpdateOnlineStatus(StatusBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var user = _db.UserMasters.Find(model.DriverID);

                    if (user != null)
                    {
                        user.IsOnline = model.IsOnline;
                        await _db.SaveChangesAsync();
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Update driver online Status

        #region Get Drivers for Admin

        public async Task<APiResponse> GetAllDrivers()
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var drivers = await (from a in _db.UserMasters
                                         where a.UserTypeID == 5
                                         select new
                                         {
                                             DriverID = a.UserID,
                                             DriverName = a.FirstName + " " + a.LastName,
                                             Email = a.Email,
                                             Phone = a.Phone,
                                             DriverLisense = a.DriverLicenseNo,
                                             Active = a.IsActive
                                         }).ToListAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Drivers = drivers });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Drivers for Admin

        #region Assign deliveries to a Driver

        public async Task<APiResponse> AssignDeliveriesToDriver(AssignDeliveriesBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    foreach (var order in model.OrderIDs)
                    {
                        var cart = _db.trnsCartMasters.Find(order);

                        cart.DriverID = model.DriverID;
                        cart.ModifiedDate = HelperClass.GetCurrentTime();

                        await _db.SaveChangesAsync();
                    }
                    return ReturnMethod(SaaviErrorCodes.Success, "Orders assigned to selected driver.");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Assign deliveries to a Driver

        #region Update scan mode status

        public async Task<APiResponse> UpdateScanMode(ScanBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var user = _db.UserMasters.Find(model.DriverID);

                    if (user != null)
                    {
                        user.ScanMode = model.ScanMode;
                        await _db.SaveChangesAsync();
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Update scan mode status

        #region Get Runs and Vehicles

        public async Task<APiResponse> GetRunVehicle()
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var date = DateTime.Now;
                    var runNos = await (from a in _db.trnsCartMasters
                                        where a.RunNo != null && a.RunNo != "" && a.OrderDate == DbFunctions.TruncateTime(date) // && a.OrderDate <= DbFunctions.TruncateTime(dateFrom) //&& runs.Contains(a.RunNo)
                                        select new
                                        {
                                            RunNo = a.RunNo + " (" + _db.trnsCartMasters.Count(x => x.RunNo == a.RunNo && x.OrderDate == DbFunctions.TruncateTime(date)) + ")"
                                        }).Distinct().ToArrayAsync();

                    var vehicles = await (from a in _db.VehicleMasters
                                          where a.IsDeleted == false
                                          select new
                                          {
                                              Vehicle = a.VehicleNo
                                          }).ToListAsync();

                    var assignedRuns = await (from a in _db.VehicleMasters
                                              join b in _db.UserMasters on a.DriverID equals b.UserID
                                              select new
                                              {
                                                  RunNo = a.Route,
                                                  Driver = b.FirstName,
                                                  Vehicle = a.VehicleNo
                                              }).ToListAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        Runs = runNos,
                        Vehicles = vehicles,
                        AssignedRuns = assignedRuns
                    });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Runs and Vehicles

        #region Set Run and vehicle for driver

        public async Task<APiResponse> SetRunVehicle(SetRunVehivleBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (model.RunNo.Split('(').Length > 0)
                    {
                        model.RunNo = model.RunNo.Split('(')[0].Trim();
                    }
                    _db.Database.ExecuteSqlCommand("update vehiclemaster set DriverID = NULL where driverID = " + model.DriverID.ToString());
                    _db.Database.ExecuteSqlCommand("update vehiclemaster set Route = NULL where route = '" + model.RunNo + "'");
                    _db.Database.ExecuteSqlCommand("update vehiclemaster set Route = NULL, DriverID = NULL where vehicleNo = '" + model.Vehicle + "'");
                    _db.Database.ExecuteSqlCommand("update vehiclemaster set Route = '" + model.RunNo + "', DriverID = " + model.DriverID + " where vehicleNo = '" + model.Vehicle + "'");

                    // Remove the driver from other runs once a new one is selected

                    _db.Database.ExecuteSqlCommand("update trnscartmaster set DriverID = NULL where RunNo <> '" + model.RunNo + "' and DriverID = " + model.DriverID + "");

                    // Set the Driver to existing orders with the specified RUN No. in the cart table
                    _db.Database.ExecuteSqlCommand("update trnscartmaster set DriverID = " + model.DriverID + " where RunNo = '" + model.RunNo + "' and (OrderDate = convert(date, getdate()) or OrderDate = convert(date, getdate() + 1))");

                    var assignedRuns = await (from a in _db.VehicleMasters
                                              join b in _db.UserMasters on a.DriverID equals b.UserID
                                              select new
                                              {
                                                  RunNo = a.Route,
                                                  Driver = b.FirstName,
                                                  Vehicle = a.VehicleNo
                                              }).ToListAsync();

                    var vehicle = _db.VehicleMasters.Where(v => v.VehicleNo == model.Vehicle).FirstOrDefault();
                    DateTime date = DateTime.Now;

                    HttpClient client = new HttpClient();

                    var pendingChecklists = (from a in _db.VehicleChecklistMasters
                                             where a.VehicleID == vehicle.VehicleID && a.DriverID == model.DriverID && a.RunNo == model.RunNo && DbFunctions.TruncateTime(a.CreatedDate) == DbFunctions.TruncateTime(date)
                                             select new
                                             {
                                                 ChecklistID = a.ID,
                                                 Date = a.CreatedDate,
                                                 VehicleID = a.VehicleID,
                                                 DriverID = a.DriverID,
                                                 RunNo = a.RunNo,
                                                 Oend = a.OdometerEnd
                                             }).OrderByDescending(o => o.Date).FirstOrDefault();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        AssignedRuns = assignedRuns,
                        ifTruckChecklistFilled = pendingChecklists == null ? false : true,
                        ifEndODOFilled = pendingChecklists == null ? false : pendingChecklists.Oend > 0 ? true : false
                    });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Set Run and vehicle for driver

        #region Run Manual SYnc for specific run and driver

        public async Task<APiResponse> RunManualSync(RunManualSyncBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var res = _db.sp_Orders_POD_Manual(model.RunNo, model.DriverID);

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Run Manual SYnc for specific run and driver

        #region Generate Driver summary PDF

        public async Task<APiResponse> GenerateDeliveryPDF(GeneratePDFBO model)
        {
            try
            {
                string ret = GeneratePDF(model.DriverID);

                ///ret = GenerateDeliverySummaryPDF(model.DriverID);

                if (ret != "No Data")
                {
                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { FilePath = HelperClass.ApiURLLive + "/PODImages/" + ret });
                }
                else
                {
                    return ReturnMethod(SaaviErrorCodes.NotFound, "No Run Data for driver.");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GeneratePDF(long driverID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var driverDetails = (from a in _db.UserMasters
                                         join b in _db.VehicleMasters on a.UserID equals b.DriverID
                                         where a.UserID == driverID
                                         select new
                                         {
                                             DriverName = a.FirstName,
                                             Run = b.Route
                                         }).FirstOrDefault();

                    var deliveries = _db.SP_GetDriverRUN_PDF(driverID).ToList();
                    if (deliveries.Count > 0)
                    {
                        var sum = deliveries.Sum(s => s.Amount);
                        var avg = sum / deliveries.Count;

                        var Pdf = new Document();
                        Pdf.DefaultPageSetup.PageFormat = PageFormat.A4;
                        Pdf.DefaultPageSetup.Orientation = Orientation.Landscape;
                        Pdf.DefaultPageSetup.TopMargin = 15;
                        Pdf.DefaultPageSetup.LeftMargin = 15;
                        Pdf.DefaultPageSetup.RightMargin = 15;
                        Pdf.DefaultPageSetup.BottomMargin = 15;
                        Pdf.DefaultPageSetup.DifferentFirstPageHeaderFooter = true;
                        Pdf.AddSection();

                        #region Header section for the logo

                        HeaderFooter header = Pdf.LastSection.Headers.FirstPage;

                        Table table = header.AddTable();
                        table.AddColumn(ParagraphAlignment.Center, Pdf.PageWidth() - 20);
                        table.AddColumn();

                        Row row = table.AddRow();
                        Image image = row.Cells[0].AddImage(@"D:\Apps\SunCoast Fresh\Rf8tjk\Images\header_delivery.jpg");
                        row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                        image.Height = 150;
                        image.Width = Pdf.PageWidth();
                        image.RelativeVertical = RelativeVertical.Line;
                        image.RelativeHorizontal = RelativeHorizontal.Margin;
                        image.Top = ShapePosition.Top;

                        #endregion Header section for the logo

                        Section section = Pdf.LastSection;

                        Paragraph paragraph = section.Headers.FirstPage.AddParagraph();
                        paragraph.AddText("Suncoast Fresh - Driver Delivery Run Summary");
                        paragraph.Format.Font.Size = 17;
                        paragraph.Format.Font.Bold = true;
                        paragraph.Format.Alignment = ParagraphAlignment.Left;
                        paragraph.Format.Borders.Top = BorderLine;

                        Paragraph paragraph2 = section.Headers.FirstPage.AddParagraph();
                        paragraph2.AddText("Driver Summary");
                        paragraph2.Format.Font.Size = 17;
                        paragraph2.Format.Font.Underline = Underline.Single;
                        paragraph2.Format.Font.Bold = true;
                        paragraph2.Format.Alignment = ParagraphAlignment.Left;
                        paragraph2.Format.SpaceBefore = 35;

                        Table dt = section.Headers.FirstPage.AddTable();

                        dt.AddColumn(ParagraphAlignment.Left, 170);
                        dt.AddColumn(ParagraphAlignment.Left, 150);

                        Row r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 20;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Date:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph(DateTime.Now.ToString("dd/MM/yyyy"));
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Driver Name:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph(driverDetails.DriverName);
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Run:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph(driverDetails.Run);
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Departure Time:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph(deliveries[0].DepartureTime ?? "");
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Return Time:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph(deliveries[0].ReturnTime ?? "");
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Number of drops:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph(deliveries.Count.ToString());
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Run Value:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph("$" + string.Format("{0:#,0.00}", sum));
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Average drop value:");
                        r.Cells[0].Format.Font.Size = 11;
                        r.Cells[1].AddParagraph("$" + string.Format("{0:#,0.00}", avg));
                        r.Cells[1].Format.Font.Size = 11;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 15;
                        r.Cells[0].AddParagraph("Truck has more than 1/2 fuel tank and is clean?:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveries[0].Fuel == true ? "Y" : "N", ParagraphAlignment.Left);
                        r.Cells[1].Format.Font.Size = 12;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[1].Format.Font.Color = deliveries[0].Fuel == true ? Colors.Green : Colors.Red;

                        Pdf.LastSection.AddPageBreak();

                        #region Table section for Delivery runs

                        Section section2 = Pdf.LastSection;
                        section2.AddPageBreak();

                        Table tbl = section2.AddTable();

                        tbl.Borders.Top = BorderLine2;
                        tbl.Borders.Bottom = BorderLine2;
                        tbl.Borders.Left = BorderLine2;
                        tbl.Borders.Right = BorderLine2;

                        double width = section2.PageWidth();
                        double productWidth = Unit.FromPoint(180);
                        double numericWidth = (width - productWidth) / 6;

                        //double width = section2.PageWidth();
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 40);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 20);
                        tbl.AddColumn(ParagraphAlignment.Center, productWidth + 80);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 20);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 20);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth + 40);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 10);

                        Row row1 = tbl.AddRow();
                        row1.HeadingFormat = true;
                        row1.Shading.Color = Colors.White;
                        row1.TopPadding = 5;
                        row1.BottomPadding = 5;

                        row1.Cells[0].AddParagraph("Drop No.", ParagraphAlignment.Center);
                        row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[0].Format.Font.Bold = true;

                        row1.Cells[1].AddParagraph("Invoice No.", ParagraphAlignment.Center);
                        row1.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[1].Format.Font.Bold = true;

                        row1.Cells[2].AddParagraph("Customer", ParagraphAlignment.Left);
                        row1.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[2].Format.Font.Bold = true;

                        row1.Cells[3].AddParagraph("Time Arrived", ParagraphAlignment.Center);
                        row1.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[3].Format.Font.Bold = true;

                        row1.Cells[4].AddParagraph("Time Departed", ParagraphAlignment.Center);
                        row1.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[4].Format.Font.Bold = true;

                        row1.Cells[5].AddParagraph("Issue/Payment", ParagraphAlignment.Center);
                        row1.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[5].Format.Font.Bold = true;

                        row1.Cells[6].AddParagraph("Invoice Amount", ParagraphAlignment.Right);
                        row1.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[6].Format.Font.Bold = true;

                        for (int i = 0; i < deliveries.Count; i++)
                        {
                            row1 = tbl.AddRow();
                            Cell cell = row1.Cells[0];
                            cell.Format.SpaceBefore = 4;
                            cell.Format.SpaceAfter = 4;
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph((i + 1).ToString(), ParagraphAlignment.Center);

                            cell = row1.Cells[1];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].InvoiceNumber ?? "", ParagraphAlignment.Center);

                            cell = row1.Cells[2];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].CustomerName, ParagraphAlignment.Left);

                            cell = row1.Cells[3];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].StartTime ?? "", ParagraphAlignment.Center);

                            cell = row1.Cells[4];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].EndTime ?? "", ParagraphAlignment.Center);

                            cell = row1.Cells[5];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            if (deliveries[i].IsCancelled == "Y")
                            {
                                cell.AddParagraph(deliveries[i].CancelReason ?? "", ParagraphAlignment.Center);
                                cell.Format.Font.Color = Colors.Red;
                            }
                            else
                            {
                                if (deliveries[i].PaymentMethod.ToLower() == "cash")
                                {
                                    if (deliveries[i].ItemIssue != null)
                                    {
                                        cell.AddParagraph(deliveries[i].PaymentMethod + ": $" + string.Format("{0:#,0.00}", deliveries[i].PaymentAmount), ParagraphAlignment.Center);
                                        cell.AddParagraph("Prouct: " + deliveries[i].ProductName, ParagraphAlignment.Left).Format.Font.Color = Colors.Red;
                                        cell.AddParagraph("Issue: " + deliveries[i].ItemIssue, ParagraphAlignment.Left).Format.Font.Color = Colors.Red;
                                        cell.AddImage(deliveries[i].IssueImage);
                                    }
                                    else
                                    {
                                        cell.AddParagraph(deliveries[i].PaymentMethod + ": $" + string.Format("{0:#,0.00}", deliveries[i].PaymentAmount), ParagraphAlignment.Center);
                                    }
                                }
                                else
                                {
                                    if (deliveries[i].ItemIssue != null)
                                    {
                                        cell.AddParagraph(deliveries[i].PaymentMethod, ParagraphAlignment.Center);
                                        cell.AddParagraph("Prouct: " + deliveries[i].ProductName, ParagraphAlignment.Left).Format.Font.Color = Colors.Red;
                                        cell.AddParagraph("Issue: " + deliveries[i].ItemIssue, ParagraphAlignment.Left).Format.Font.Color = Colors.Red;

                                        cell.AddImage(deliveries[i].IssueImage);
                                    }
                                    else
                                    {
                                        cell.AddParagraph(deliveries[i].PaymentMethod, ParagraphAlignment.Center);
                                    }
                                }
                            }

                            cell = row1.Cells[6];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph("$" + string.Format("{0:#,0.00}", deliveries[i].Amount), ParagraphAlignment.Right);
                        }

                        #endregion Table section for Delivery runs

                        // Saving the pdf file
                        string unique = Guid.NewGuid().ToString().Replace("-", "");
                        string pdfPath = HostingEnvironment.MapPath("~/PODImages/Driver_" + driverID.ToString() + "_SAAVI_" + unique + ".pdf");

                        PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
                        renderer.Document = Pdf;
                        renderer.RenderDocument();
                        renderer.PdfDocument.Save(pdfPath);

                        return "Driver_" + driverID.ToString() + "_SAAVI_" + unique + ".pdf";
                    }
                    else
                    {
                        return "No Data";
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorsToServer("Share summary PDF: " + ex.Message, driverID.ToString(), "Error: DriverID");
                return "";
            }
        }

        private Border BorderLine
        {
            get
            {
                Border bottomLine = new Border();
                bottomLine.Width = new Unit(2.5);
                bottomLine.Color = HelperClass.TextColorFromHtml("#000000");
                return bottomLine;
            }
        }

        private Border BorderLine2
        {
            get
            {
                Border bottomLine = new Border();
                bottomLine.Width = new Unit(.3);
                bottomLine.Color = HelperClass.TextColorFromHtml("#000000");
                return bottomLine;
            }
        }

        public string GenerateDeliverySummaryPDF(long? driverID, VehicleChecklistMaster chkList)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (chkList == null)
                    {
                        chkList = _db.VehicleChecklistMasters.Where(x => x.OdometerEnd > 0).OrderByDescending(o => o.ID).FirstOrDefault();
                    }

                    var driverDetails = (from a in _db.UserMasters
                                         join b in _db.VehicleMasters on a.UserID equals b.DriverID
                                         where a.UserID == driverID
                                         select new
                                         {
                                             DriverName = a.FirstName,
                                             Run = b.Route,
                                             DEmail = a.Email
                                         }).FirstOrDefault();
                    List<ImageIssueList> productIssues = new List<ImageIssueList>();

                    var deliveries = _db.SP_GetDriverSummaryReport(driverID).ToList();

                    if (deliveries.Count > 0)
                    {
                        var sum = deliveries.Sum(s => s.Amount);
                        var d100 = deliveries.Sum(d => d.Dollar100);
                        var d50 = deliveries.Sum(d => d.Dollar50);
                        var d20 = deliveries.Sum(d => d.Dollar20);
                        var d10 = deliveries.Sum(d => d.Dollar10);
                        var d5 = deliveries.Sum(d => d.Dollar5);
                        var d2 = deliveries.Sum(d => d.Dollar2);
                        var d1 = deliveries.Sum(d => d.Dollar1);
                        var c50 = deliveries.Sum(d => d.Cent50);
                        var c20 = deliveries.Sum(d => d.Cent20);

                        var d100S = 100 * d100;
                        var d50S = 50 * d50;
                        var d20S = 20 * d20;
                        var d10S = 10 * d10;
                        var d5S = 5 * d5;
                        var d2S = 2 * d2;
                        var d1S = 1 * d1;
                        var c50S = .50 * c50;
                        var c20S = .20 * c20;

                        var totalCurreny = d100S + d50S + d20S + d10S + d5S + d2S + d1S + c50S + c20S;

                        var delTime = deliveries.OrderBy(o => o.ArrivedTime).FirstOrDefault().ArrivedTime;
                        var lTime = deliveries.OrderByDescending(o => o.ArrivedTime).FirstOrDefault().EndTime;

                        var delTemp = deliveries.OrderBy(o => o.ArrivedTime).FirstOrDefault().GoodsTemp;
                        var lTemp = deliveries.OrderByDescending(o => o.ArrivedTime).FirstOrDefault().GoodsTemp;

                        var Pdf = new Document();
                        Pdf.DefaultPageSetup.PageFormat = PageFormat.A4;
                        Pdf.DefaultPageSetup.Orientation = Orientation.Landscape;
                        Pdf.DefaultPageSetup.TopMargin = 15;
                        Pdf.DefaultPageSetup.LeftMargin = 15;
                        Pdf.DefaultPageSetup.RightMargin = 15;
                        Pdf.DefaultPageSetup.BottomMargin = 15;
                        Pdf.DefaultPageSetup.DifferentFirstPageHeaderFooter = true;
                        Pdf.AddSection();

                        #region Header section for the logo

                        HeaderFooter header = Pdf.LastSection.Headers.FirstPage;

                        Table table = header.AddTable();
                        table.AddColumn(ParagraphAlignment.Center, Pdf.PageWidth() - 20);
                        table.AddColumn();

                        Row row = table.AddRow();
                        Image image = row.Cells[0].AddImage(@"D:\Apps\SunCoast Fresh\Rf8tjk\Images\header_delivery.jpg");
                        row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                        image.Height = 150;
                        image.Width = Pdf.PageWidth();
                        image.RelativeVertical = RelativeVertical.Line;
                        image.RelativeHorizontal = RelativeHorizontal.Margin;
                        image.Top = ShapePosition.Top;

                        #endregion Header section for the logo

                        Section section = Pdf.LastSection;

                        Paragraph paragraph = section.Headers.FirstPage.AddParagraph();
                        paragraph.AddText("Suncoast Fresh - Returned Delivery Run Summary");
                        paragraph.Format.Font.Size = 17;
                        paragraph.Format.Font.Bold = true;
                        paragraph.Format.Alignment = ParagraphAlignment.Left;
                        paragraph.Format.Borders.Top = BorderLine;

                        Table mainTable = section.Headers.FirstPage.AddTable();
                        double pWidth = section.PageWidth();
                        double dWidth = pWidth / 2;

                        mainTable.AddColumn(ParagraphAlignment.Left, dWidth);
                        mainTable.AddColumn(ParagraphAlignment.Right, dWidth);

                        Row mainRow = mainTable.AddRow();

                        #region Driver details table

                        Paragraph paragraph2 = new Paragraph(); // section.Headers.FirstPage.AddParagraph();
                        paragraph2.AddText("Driver Summary");
                        paragraph2.Format.Font.Size = 17;
                        paragraph2.Format.Font.Underline = Underline.Single;
                        paragraph2.Format.Font.Bold = true;
                        paragraph2.Format.Alignment = ParagraphAlignment.Left;
                        paragraph2.Format.SpaceBefore = 20;

                        Table dt = new Table();//section.Headers.FirstPage.AddTable();

                        dt.AddColumn(ParagraphAlignment.Left, 200);
                        dt.AddColumn(ParagraphAlignment.Left, 150);

                        Row r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 30;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Date:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(DateTime.Now.ToString("dd/MM/yyyy"));
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Driver Name:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveries[0].DriverName);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Run:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveries[0].Run);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 12;
                        r.Cells[0].AddParagraph("Departure Time:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveries[0].DepartureTime);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 15;
                        r.Cells[0].AddParagraph("Return Time:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveries[0].ReturnTime ?? "");
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 15;
                        r.Cells[0].AddParagraph("Number of drops:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveries[0].TotalDrops.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 15;
                        r.Cells[0].AddParagraph("Run Value:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph("$" + string.Format("{0:#,0.00}", deliveries[0].RunValue));
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 15;
                        r.Cells[0].AddParagraph("Average drop value:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph("$" + string.Format("{0:#,0.00}", deliveries[0].AvgRunValue));
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 15;
                        r.Cells[0].AddParagraph("Number of credits:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph("");
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Height = 15;
                        r.Cells[0].AddParagraph("Truck has more than 1/2 fuel tank and is clean?:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(chkList.Fuel == null ? "N" : chkList.Fuel == true ? "Y" : "N", ParagraphAlignment.Left);
                        r.Cells[1].Format.Font.Size = 12;
                        r.Cells[1].Format.Font.Bold = true;

                        if (chkList.Fuel != null && chkList.Fuel == true)
                        {
                            r.Cells[1].Format.Font.Color = Colors.Green;
                        }
                        else
                        {
                            r.Cells[1].Format.Font.Color = Colors.Red;
                        }

                        #endregion Driver details table

                        #region Currency details

                        Paragraph paragraph22 = new Paragraph(); // section.Headers.FirstPage.AddParagraph();
                        paragraph22.AddText("Cash Received Summary");
                        paragraph22.Format.Font.Size = 17;
                        paragraph22.Format.Font.Underline = Underline.Single;
                        paragraph22.Format.Font.Bold = true;
                        paragraph22.Format.Alignment = ParagraphAlignment.Left;
                        paragraph22.Format.SpaceBefore = 20;

                        Table dtRigth = new Table();//section.Headers.FirstPage.AddTable();
                        dWidth = (pWidth / 2) / 3;

                        dtRigth.AddColumn(ParagraphAlignment.Left, dWidth);
                        dtRigth.AddColumn(ParagraphAlignment.Left, dWidth - 50);
                        dtRigth.AddColumn(ParagraphAlignment.Left, dWidth - 80);

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.Cells[0].AddParagraph("Notes/Coins");
                        r.Cells[0].Format.Font.Size = 12;
                        r.Cells[1].AddParagraph("Qty");
                        r.Cells[1].Format.Font.Size = 12;
                        r.Cells[2].AddParagraph("Amount");
                        r.Cells[2].Format.Font.Size = 12;
                        r.Format.Borders.Bottom = BorderLine2;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$100");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(d100.ToString() == "0" ? "" : d100.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(d100S == 0 ? "" : "$" + string.Format("{0:#,0.00}", d100S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$50");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(d50.ToString() == "0" ? "" : d50.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(d50S == 0 ? "" : "$" + string.Format("{0:#,0.00}", d50S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$20");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(d20.ToString() == "0" ? "" : d20.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(d20S == 0 ? "" : "$" + string.Format("{0:#,0.00}", d20S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$10");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(d10.ToString() == "0" ? "" : d10.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(d10S == 0 ? "" : "$" + string.Format("{0:#,0.00}", d10S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$5");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(d5.ToString() == "0" ? "" : d5.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(d5S == 0 ? "" : "$" + string.Format("{0:#,0.00}", d5S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$2");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(d2.ToString() == "0" ? "" : d2.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(d2S == 0 ? "" : "$" + string.Format("{0:#,0.00}", d2S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$1");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(d1.ToString() == "0" ? "" : d1.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(d1S == 0 ? "" : "$" + string.Format("{0:#,0.00}", d1S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$0.50");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(c50.ToString() == "0" ? "" : c50.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(c50S == 0 ? "" : "$" + string.Format("{0:#,0.00}", c50S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("$0.20");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(c20.ToString() == "0" ? "" : c20.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph(c20S == 0 ? "" : "$" + string.Format("{0:#,0.00}", c20S));
                        r.Cells[2].Format.Font.Size = 10;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("Invoice Amount: $" + string.Format("{0:#,0.00}", sum));
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[0].MergeRight = 1;
                        //r.Cells[1].AddParagraph("");
                        //r.Cells[1].Format.Font.Size = 10;
                        //r.Cells[1].Format.Font.Bold = true;
                        r.Cells[2].AddParagraph("$" + string.Format("{0:#,0.00}", totalCurreny));
                        r.Cells[2].Format.Font.Size = 10;
                        r.Cells[2].Format.Borders.Top = BorderLine2;
                        r.Cells[2].Format.Borders.Bottom = BorderLine2;

                        var diff = sum - totalCurreny;

                        r = dtRigth.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 4;
                        r.Cells[0].AddParagraph("Difference: $" + string.Format("{0:#,0.00}", diff));
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[0].MergeRight = 1;

                        if (diff == 0)
                        {
                            r.Cells[0].Format.Font.Color = Colors.Green;
                        }
                        else
                        {
                            r.Cells[0].Format.Font.Color = Colors.Red;
                        }

                        #endregion Currency details

                        mainRow.Cells[0].Elements.Add(paragraph2);
                        mainRow.Cells[0].Elements.Add(dt);

                        mainRow.Cells[1].Elements.Add(paragraph22);
                        mainRow.Cells[1].Elements.Add(dtRigth);

                        Pdf.LastSection.AddPageBreak();

                        #region Table section for Delivery runs

                        Section section2 = Pdf.LastSection;
                        section2.AddPageBreak();

                        Paragraph paragraph3 = section2.AddParagraph();
                        paragraph3.AddText("PROOF OF DELIVERY / CASH SHEET / DISPATCH AND DELIVERY TEMPERATURE");
                        paragraph3.Format.Font.Size = 15;
                        paragraph3.Format.Font.Underline = Underline.Single;
                        paragraph3.Format.Font.Bold = true;
                        paragraph3.Format.Alignment = ParagraphAlignment.Left;
                        paragraph3.Format.SpaceBefore = 30;
                        paragraph3.Format.SpaceAfter = 10;

                        Table tbl = section2.AddTable();

                        tbl.Borders.Top = BorderLine2;
                        tbl.Borders.Bottom = BorderLine2;
                        tbl.Borders.Left = BorderLine2;
                        tbl.Borders.Right = BorderLine2;

                        //double width = section2.PageWidth();
                        tbl.AddColumn(ParagraphAlignment.Center, 35);
                        tbl.AddColumn(ParagraphAlignment.Center);
                        tbl.AddColumn(ParagraphAlignment.Center, 215);
                        tbl.AddColumn(ParagraphAlignment.Center);
                        tbl.AddColumn(ParagraphAlignment.Center);
                        tbl.AddColumn(ParagraphAlignment.Center, 130);
                        tbl.AddColumn(ParagraphAlignment.Center);
                        tbl.AddColumn(ParagraphAlignment.Center, 85);
                        tbl.AddColumn(ParagraphAlignment.Center);

                        Row row1 = tbl.AddRow();
                        row1.HeadingFormat = true;
                        row1.Shading.Color = Colors.White;
                        row1.TopPadding = 5;
                        row1.BottomPadding = 5;

                        row1.Cells[0].AddParagraph("Drop No.", ParagraphAlignment.Center);
                        row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[0].Format.Font.Bold = true;

                        row1.Cells[1].AddParagraph("Invoice No.", ParagraphAlignment.Center);
                        row1.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[1].Format.Font.Bold = true;

                        row1.Cells[2].AddParagraph("Customer", ParagraphAlignment.Left);
                        row1.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[2].Format.Font.Bold = true;

                        row1.Cells[3].AddParagraph("Time Arrived", ParagraphAlignment.Center);
                        row1.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[3].Format.Font.Bold = true;

                        row1.Cells[4].AddParagraph("Time Departed", ParagraphAlignment.Center);
                        row1.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[4].Format.Font.Bold = true;

                        row1.Cells[5].AddParagraph("Issue/Payment", ParagraphAlignment.Center);
                        row1.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[5].Format.Font.Bold = true;

                        row1.Cells[6].AddParagraph("Invoice Amount", ParagraphAlignment.Center);
                        row1.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[6].Format.Font.Bold = true;

                        row1.Cells[7].AddParagraph("Signature", ParagraphAlignment.Center);
                        row1.Cells[7].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[7].Format.Font.Bold = true;

                        row1.Cells[8].AddParagraph("Delivered", ParagraphAlignment.Center);
                        row1.Cells[8].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[8].Format.Font.Bold = true;

                        for (int i = 0; i < deliveries.Count; i++)
                        {
                            row1 = tbl.AddRow();
                            Cell cell = row1.Cells[0];
                            cell.Format.SpaceBefore = 4;
                            cell.Format.SpaceAfter = 4;
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph((i + 1).ToString(), ParagraphAlignment.Center);

                            cell = row1.Cells[1];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].InvoiceNumber ?? "", ParagraphAlignment.Center);

                            cell = row1.Cells[2];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].CustomerName, ParagraphAlignment.Left);

                            cell = row1.Cells[3];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].DepartureTime ?? "", ParagraphAlignment.Center);

                            cell = row1.Cells[4];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].EndTime ?? "", ParagraphAlignment.Center);

                            cell = row1.Cells[5];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            if (deliveries[i].IsCancelled == true)
                            {
                                cell.AddParagraph(deliveries[i].CancelReason ?? "", ParagraphAlignment.Center);
                                cell.Format.Font.Color = Colors.Red;
                            }
                            else
                            {
                                if (deliveries[i].PaymentMethod.ToLower() == "cash")
                                {
                                    cell.AddParagraph(deliveries[i].PaymentMethod + ": $" + string.Format("{0:#,0.00}", deliveries[i].Amount), ParagraphAlignment.Center);
                                }
                                else
                                {
                                    if (deliveries[i].HasItemIssues == true)
                                    {
                                        cell.AddParagraph(deliveries[i].PaymentMethod, ParagraphAlignment.Center);
                                    }
                                    else
                                    {
                                        cell.AddParagraph(deliveries[i].PaymentMethod, ParagraphAlignment.Center);
                                    }
                                }
                            }

                            if (deliveries[i].HasItemIssues == true)
                            {
                                long cID = deliveries[i].Cartid;
                                var itemIssues = (from a in _db.DeliveryStatusMasters
                                                  join b in _db.DeliveryStatusImageMasters on a.DeliveryID equals b.DeliveryID into temp
                                                  from b in temp.DefaultIfEmpty()
                                                  join c in _db.DeliveryIssuesMasters on a.DeliveryID equals c.DeliveryID
                                                  join d in _db.ProductMasters on c.ProductID equals d.ProductID
                                                  join e in _db.trnsCartMasters on a.OrderID equals e.CartID
                                                  where a.OrderID == cID && b.ProductID == d.ProductID
                                                  select new ImageIssueList
                                                  {
                                                      ProductName = d.ProductName,
                                                      IssueDesc = c.Description,
                                                      ImageURL = b.IsSignature == true ? "" : "D:/Apps/SunCoast Fresh/Rf8tjk/PodImages/" + a.OrderID + "/" + b.ImageName,
                                                      OrderID = e.InvoiceNumber ?? "",
                                                      DeliveryID = a.DeliveryID
                                                  }).ToList();

                                if (itemIssues != null)
                                {
                                    productIssues.AddRange(itemIssues);
                                }
                            }

                            cell = row1.Cells[6];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(deliveries[i].Amount == null ? "" : "$" + string.Format("{0:#,0.00}", deliveries[i].Amount), ParagraphAlignment.Center);

                            cell = row1.Cells[7];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            if (deliveries[i].CustomerSignature == null)
                            {
                                cell.AddParagraph("");
                            }
                            else
                            {
                                cell.AddImage(deliveries[i].CustomerSignature).Height = 100;
                            }

                            cell = row1.Cells[8];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            Paragraph pLoc = cell.AddParagraph();
                            pLoc.Format.Font.Color = Colors.Blue;
                            pLoc.Format.Font.Underline = Underline.Dash;

                            Hyperlink pLink = pLoc.AddHyperlink("https://maps.google.com/?q=" + deliveries[i].Latitude + ", " + deliveries[i].Longitude + "", HyperlinkType.Web);
                            pLink.AddFormattedText("Location");
                        }

                        row1 = tbl.AddRow();
                        row1.Cells[5].AddParagraph("Total:");
                        row1.Cells[6].AddParagraph("$" + string.Format("{0:#,0.00}", sum));

                        #endregion Table section for Delivery runs

                        #region Verification section

                        Section section3 = Pdf.LastSection;

                        double width = section2.PageWidth();
                        //double productWidth = Unit.FromPoint(180);
                        double numericWidth = (width) / 7;

                        Paragraph paragraph4 = section3.AddParagraph();
                        paragraph4.AddText("VERIFICATION");
                        paragraph4.Format.Font.Size = 15;
                        paragraph4.Format.Font.Underline = Underline.Single;
                        paragraph4.Format.Font.Bold = true;
                        paragraph4.Format.Alignment = ParagraphAlignment.Left;
                        paragraph4.Format.SpaceBefore = 30;
                        paragraph4.Format.SpaceAfter = 10;

                        //Table t2 = section3.AddTable();
                        //t2.AddColumn(ParagraphAlignment.Center, width);

                        //Row rr = t2.AddRow();
                        //rr.Cells[0].AddParagraph();

                        Table tbl2 = section3.AddTable();
                        tbl2.Borders.Top = BorderLine2;
                        tbl2.Borders.Bottom = BorderLine2;
                        tbl2.Borders.Left = BorderLine2;
                        tbl2.Borders.Right = BorderLine2;

                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth + 8);

                        Row row2 = tbl2.AddRow();
                        row2.HeadingFormat = true;
                        row2.Shading.Color = Colors.White;
                        row2.TopPadding = 5;
                        row2.BottomPadding = 5;

                        row2.Cells[0].AddParagraph("Temperature Details", ParagraphAlignment.Center);

                        row2.Cells[1].AddParagraph("CHILLED", ParagraphAlignment.Center);
                        row2.Cells[1].MergeRight = 1;

                        row2.Cells[3].AddParagraph("FROZEN", ParagraphAlignment.Center);
                        row2.Cells[3].MergeRight = 1;

                        row2.Cells[5].AddParagraph("CHECKED BY", ParagraphAlignment.Center);
                        row2.Cells[5].MergeRight = 1;

                        row2 = tbl2.AddRow();
                        row2.Cells[0].AddParagraph("");
                        row2.Cells[1].AddParagraph("TIME", ParagraphAlignment.Center);
                        row2.Cells[2].AddParagraph("TEMP", ParagraphAlignment.Center);
                        row2.Cells[3].AddParagraph("TIME", ParagraphAlignment.Center);
                        row2.Cells[4].AddParagraph("TEMP", ParagraphAlignment.Center);
                        row2.Cells[5].AddParagraph("SIGNATURE", ParagraphAlignment.Center);
                        row2.Cells[6].AddParagraph("COMMENT", ParagraphAlignment.Center);
                        row2.TopPadding = 2;
                        row2.BottomPadding = 2;

                        row2 = tbl2.AddRow();
                        row2.Cells[0].AddParagraph("DEPARTURE");
                        row2.Cells[1].AddParagraph(deliveries[0].DepartureTime, ParagraphAlignment.Center);
                        row2.Cells[2].AddParagraph(chkList.TempChilled.ToString(), ParagraphAlignment.Center);
                        row2.Cells[3].AddParagraph(deliveries[0].DepartureTime, ParagraphAlignment.Center);
                        row2.Cells[4].AddParagraph(chkList.TempFrozen.ToString(), ParagraphAlignment.Center);
                        row2.Cells[5].AddParagraph(driverDetails.DriverName, ParagraphAlignment.Center);
                        row2.Cells[6].AddParagraph(chkList.Comments ?? "", ParagraphAlignment.Center);
                        row2.TopPadding = 10;
                        row2.BottomPadding = 10;

                        row2 = tbl2.AddRow();
                        row2.Cells[0].AddParagraph("LAST DROP");
                        row2.Cells[1].AddParagraph(deliveries[0].ReturnTime, ParagraphAlignment.Center);
                        row2.Cells[2].AddParagraph(chkList.TempChilledEnd.ToString(), ParagraphAlignment.Center);
                        row2.Cells[3].AddParagraph(deliveries[0].ReturnTime, ParagraphAlignment.Center);
                        row2.Cells[4].AddParagraph(chkList.TempFrozenEnd.ToString(), ParagraphAlignment.Center);
                        row2.Cells[5].AddParagraph(driverDetails.DriverName, ParagraphAlignment.Center);
                        row2.Cells[6].AddParagraph(chkList.EODComment ?? "", ParagraphAlignment.Center);
                        row2.TopPadding = 10;
                        row2.BottomPadding = 10;

                        Table tbl3 = section3.AddTable();
                        tbl3.TopPadding = 20;

                        numericWidth = width / 2;
                        int headerWidth = 150;

                        tbl3.AddColumn(ParagraphAlignment.Left, headerWidth);
                        tbl3.AddColumn(ParagraphAlignment.Center, numericWidth - headerWidth);
                        tbl3.AddColumn(ParagraphAlignment.Left, headerWidth);
                        tbl3.AddColumn(ParagraphAlignment.Center, numericWidth - headerWidth);

                        Row r3 = tbl3.AddRow();
                        r3.Cells[0].AddParagraph("Load Configuration:");
                        r3.Cells[1].AddParagraph("Satisfactory");
                        r3.Cells[2].AddParagraph("Vehicle Checklist:");

                        if (chkList.IsWindscreenOK == true && chkList.IsTyreConditionOK == true && chkList.IsFixturesFittingOK == true && chkList.IsLightSignalOK == true && chkList.IsVehicleConditionOK == true)
                        {
                            r3.Cells[3].AddParagraph("Satisfactory");
                            r3.Cells[3].Format.Font.Color = Colors.Green;
                        }
                        else
                        {
                            r3.Cells[3].AddParagraph("Unsatisfactory");
                            r3.Cells[3].Format.Font.Color = Colors.Red;
                        }
                        r3.BottomPadding = 20;

                        r3 = tbl3.AddRow();
                        r3.Cells[0].AddParagraph("Current End of Run Vehicle ODO: " + chkList.OdometerEnd.ToString());
                        r3.Cells[0].MergeRight = 1;
                        r3.TopPadding = 5;

                        r3 = tbl3.AddRow();
                        r3.Cells[0].AddParagraph("Next Vehicle Service Reminder: " + (chkList.VehicleService ?? ""));
                        r3.Cells[0].MergeRight = 1;
                        r3.TopPadding = 5;
                        r3.BottomPadding = 10;

                        if (chkList.VehicleService.To<float>() < chkList.OdometerEnd)
                        {
                            r3.Cells[0].Format.Font.Color = Colors.Red;
                        }

                        r3 = tbl3.AddRow();
                        r3.Cells[0].AddParagraph("Form Verified:");
                        r3.Cells[1].AddParagraph("_________________________________________________________");
                        r3.Cells[2].AddParagraph("");
                        r3.Cells[3].AddParagraph("");
                        r3.TopPadding = 20;

                        tbl3.KeepTogether = true;

                        #endregion Verification section

                        #region Item Issues section

                        if (productIssues != null && productIssues.Count > 0)
                        {
                            Paragraph paragraph5 = section3.AddParagraph();
                            paragraph5.AddText("Item Issues");
                            paragraph5.Format.Font.Size = 15;
                            paragraph5.Format.Font.Underline = Underline.Single;
                            paragraph5.Format.Font.Bold = true;
                            paragraph5.Format.Alignment = ParagraphAlignment.Left;
                            paragraph5.Format.SpaceBefore = 30;
                            paragraph5.Format.SpaceAfter = 10;

                            Table tbl4 = section3.AddTable();
                            tbl4.Borders.Top = BorderLine2;
                            tbl4.Borders.Bottom = BorderLine2;
                            tbl4.Borders.Left = BorderLine2;
                            tbl4.Borders.Right = BorderLine2;

                            numericWidth = width / 4;
                            tbl4.AddColumn(ParagraphAlignment.Center, numericWidth - 100);
                            tbl4.AddColumn(ParagraphAlignment.Center, numericWidth + 100);
                            tbl4.AddColumn(ParagraphAlignment.Center, numericWidth - 50);
                            tbl4.AddColumn(ParagraphAlignment.Center, numericWidth + 50);

                            r3 = tbl4.AddRow();
                            r3.Cells[0].AddParagraph("INVOICE NO.", ParagraphAlignment.Left);
                            r3.Cells[0].Format.Font.Bold = true;

                            r3.Cells[1].AddParagraph("PRODUCT NAME", ParagraphAlignment.Left);
                            r3.Cells[1].Format.Font.Bold = true;

                            r3.Cells[2].AddParagraph("DESCRIPTION", ParagraphAlignment.Left);
                            r3.Cells[2].Format.Font.Bold = true;

                            r3.Cells[3].AddParagraph("IMAGE", ParagraphAlignment.Left);
                            r3.Cells[3].Format.Font.Bold = true;
                            r3.TopPadding = 5;
                            r3.BottomPadding = 5;
                            var pi = productIssues.GroupBy(g => g.DeliveryID).Select(s => s.First()).ToList();
                            foreach (var item in pi)
                            {
                                r3 = tbl4.AddRow();
                                r3.Cells[0].AddParagraph(item.OrderID.ToString());
                                r3.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                                r3.Cells[1].AddParagraph(item.ProductName, ParagraphAlignment.Left);
                                r3.Cells[1].VerticalAlignment = VerticalAlignment.Center;

                                r3.Cells[2].AddParagraph(item.IssueDesc, ParagraphAlignment.Left);
                                r3.Cells[2].VerticalAlignment = VerticalAlignment.Center;

                                if (item.ImageURL != "")
                                {
                                    r3.Cells[3].AddImage(item.ImageURL).Height = 150;
                                }
                                else
                                {
                                    r3.Cells[3].AddParagraph("No Image added");
                                }
                                r3.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                                r3.TopPadding = 10;
                                r3.BottomPadding = 10;
                            }
                        }

                        #endregion Item Issues section

                        // Saving the pdf file
                        string unique = Guid.NewGuid().ToString().Replace("-", "");
                        string pdfPath = HostingEnvironment.MapPath("~/PODImages/DeliverySummary_" + driverID.ToString() + "_" + unique + ".pdf");

                        PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
                        renderer.Document = Pdf;
                        renderer.RenderDocument();
                        renderer.PdfDocument.Save(pdfPath);

                        string msg = "Dear Admin,<br><br> Please find the the delivery run summary report for driver <b>" + deliveries[0].DriverName + "</b> on the <b>" + deliveries[0].Run + "</b> run. We have attached a copy. <br><br>From the SAAVI Proof of Delivery System";

                        string res = SendMail.SendEMail(driverDetails.DEmail, HelperClass.DeliveryIssueEmail, "", "Suncoast Fresh Delivery Run Summary", msg, pdfPath, HelperClass.FromEmail);

                        return HelperClass.ApiURLLive + "/PodImages/DeliverySummary_" + driverID.ToString() + "_" + unique + ".pdf";
                    }
                    else
                    {
                        return "No Data";
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorsToServer("Error sending driver suppary report:" + ex.Message, "0", "GenerateDeliverySummaryPDF");
                return "Exception in the main method";
            }
        }

        public string DeliveryConfirmationPDF(string invoiceNo, long deliveryID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var deliveryDetails = (from a in _db.trnsCartMasters
                                           join b in _db.DeliveryStatusMasters on a.CartID equals b.OrderID
                                           join d in _db.CustomerMasters on a.CustomerID equals d.CustomerID
                                           join e in _db.DeliveryCommentsMasters on a.CommentID equals e.CommentID into temp
                                           from e in temp.DefaultIfEmpty()
                                           where a.InvoiceNumber == invoiceNo
                                           select new
                                           {
                                               InvoiceNum = a.InvoiceNumber,
                                               CustomerName = d.CustomerName,
                                               Alphacode = d.AlphaCode,
                                               Address1 = d.Address1 ?? "",
                                               Address2 = d.Address2 ?? "",
                                               Suburb = d.Suburb ?? "",
                                               City = d.City ?? "",
                                               PostCode = d.PostCode ?? "",
                                               Contact = d.ContactName,
                                               Phone = d.Phone1,
                                               Email = d.Email,
                                               OrderNo = a.CartID,
                                               PO = a.PONumber,
                                               OrderComment = a.CommentID == null ? "" : e.CommentDescription,
                                               OrderDate = a.OrderDate,
                                               Cartons = b.CartonNumber,
                                               PaymentMethod = b.PaymentMethod,
                                               DeliveryIssue = b.CancelReason ?? "",
                                               Cash = b.PaymentAmount ?? 0,
                                               AcceptedBy = b.ReveiverName,
                                               DeliveryTime = b.EndTime,
                                               HasItemIssues = b.HasItemIssues,
                                               Lat = b.Latitude,
                                               Lng = b.Longitude
                                           }).FirstOrDefault();

                    if (deliveryDetails != null)
                    {
                        var items = (from a in _db.trnsCartMasters
                                     join b in _db.trnsCartItemsMasters on a.CartID equals b.CartID
                                     join c in _db.DeliveryStatusMasters on a.CartID equals c.OrderID
                                     join d in _db.ProductMasters on b.ProductID equals d.ProductID
                                     join u in _db.UnitOfMeasurementMasters on b.UnitId equals u.UOMID
                                     join i in _db.DeliveryIssuesMasters on new { c.DeliveryID, d.ProductID } equals new { i.DeliveryID, i.ProductID } into temp
                                     from i in temp.DefaultIfEmpty()
                                     where a.InvoiceNumber == invoiceNo
                                     select new
                                     {
                                         Product = d.ProductCode,
                                         Description = d.ProductName,
                                         UOM = u.UOMDesc,
                                         Price = b.Price,
                                         OQty = b.Quantity,
                                         AQty = i.AmmendQuantity ?? b.Quantity,
                                         Issues = c.HasItemIssues,
                                         ItemIssue = i.Description ?? ""
                                     }).ToList();

                        var sum = items.Sum(x => x.OQty * x.Price);

                        var Pdf = new Document();
                        Pdf.DefaultPageSetup.PageFormat = PageFormat.A4;
                        Pdf.DefaultPageSetup.Orientation = Orientation.Landscape;
                        Pdf.DefaultPageSetup.TopMargin = 15;
                        Pdf.DefaultPageSetup.LeftMargin = 15;
                        Pdf.DefaultPageSetup.RightMargin = 15;
                        Pdf.DefaultPageSetup.BottomMargin = 15;
                        Pdf.DefaultPageSetup.DifferentFirstPageHeaderFooter = true;
                        Pdf.AddSection();

                        #region Header section for the logo

                        HeaderFooter header = Pdf.LastSection.Headers.FirstPage;

                        Table table = header.AddTable();
                        table.AddColumn(ParagraphAlignment.Center, Pdf.PageWidth() - 20);
                        table.AddColumn();

                        Row row = table.AddRow();
                        Image image = row.Cells[0].AddImage(@"D:\Apps\SunCoast Fresh\Rf8tjk\Images\header_delivery.jpg");
                        row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                        image.Height = 150;
                        image.Width = Pdf.PageWidth();
                        image.RelativeVertical = RelativeVertical.Line;
                        image.RelativeHorizontal = RelativeHorizontal.Margin;
                        image.Top = ShapePosition.Top;

                        #endregion Header section for the logo

                        #region First page PDF code

                        Section section = Pdf.LastSection;

                        Paragraph paragraph = section.Headers.FirstPage.AddParagraph();
                        paragraph.AddText(deliveryDetails.CustomerName.ToUpper() + " DELIVERY ACCEPTANCE CONFIRMATION");
                        paragraph.Format.Font.Size = 17;
                        paragraph.Format.Font.Bold = true;
                        paragraph.Format.Alignment = ParagraphAlignment.Left;
                        paragraph.Format.Borders.Top = BorderLine;

                        //Paragraph paragraph2 = section.Headers.FirstPage.AddParagraph();
                        //paragraph2.AddText("Driver Summary");
                        //paragraph2.Format.Font.Size = 17;
                        //paragraph2.Format.Font.Underline = Underline.Single;
                        //paragraph2.Format.Font.Bold = true;
                        //paragraph2.Format.Alignment = ParagraphAlignment.Left;
                        //paragraph2.Format.SpaceBefore = 35;

                        Table dt = section.Headers.FirstPage.AddTable();

                        dt.AddColumn(ParagraphAlignment.Left, 150);
                        dt.AddColumn(ParagraphAlignment.Left, 190);

                        Row r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 20;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("From:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(HelperClass.Company);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 5;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Invoice No:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.InvoiceNum);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("To:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.Alphacode);
                        r.Cells[1].AddParagraph(deliveryDetails.CustomerName);
                        r.Cells[1].AddParagraph(deliveryDetails.Address1);
                        if (deliveryDetails.Address2 != "")
                        {
                            r.Cells[1].AddParagraph(deliveryDetails.Address2);
                        }
                        if (deliveryDetails.Suburb != "")
                        {
                            r.Cells[1].AddParagraph(deliveryDetails.Suburb);
                        }
                        r.Cells[1].AddParagraph(deliveryDetails.City);
                        r.Cells[1].AddParagraph(deliveryDetails.PostCode);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 2;
                        r.Cells[0].AddParagraph("Contact:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.Contact);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 2;
                        r.BottomPadding = 2;
                        r.Cells[0].AddParagraph("Phone:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.Phone);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 2;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Email:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.Email);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 10;
                        r.BottomPadding = 2;
                        r.Cells[0].AddParagraph("Sales Order No:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.OrderNo.ToString());
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 2;
                        r.BottomPadding = 2;
                        r.Cells[0].AddParagraph("PO No:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.PO ?? "");
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 2;
                        r.BottomPadding = 2;
                        r.Cells[0].AddParagraph("Order Comment:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.OrderComment);
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        r = dt.AddRow();
                        r.HeadingFormat = false;
                        r.TopPadding = 2;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Requested Date:");
                        r.Cells[0].Format.Font.Size = 10;
                        r.Cells[1].AddParagraph(deliveryDetails.OrderDate.To<DateTime>().ToString("dd/MM/yyyy"));
                        r.Cells[1].Format.Font.Size = 10;
                        r.Cells[1].Format.Font.Bold = true;

                        Pdf.LastSection.AddPageBreak();

                        #endregion First page PDF code

                        #region Table section for Delivery runs

                        Section section2 = Pdf.LastSection;
                        section2.AddPageBreak();

                        Table tbl = section2.AddTable();

                        tbl.Borders.Top = BorderLine2;
                        tbl.Borders.Bottom = BorderLine2;
                        tbl.Borders.Left = BorderLine2;
                        tbl.Borders.Right = BorderLine2;

                        double width = section2.PageWidth();
                        double productWidth = Unit.FromPoint(180);
                        double numericWidth = (width - productWidth) / 6;

                        //double width = section2.PageWidth();
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth + 50);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth + 120);
                        tbl.AddColumn(ParagraphAlignment.Center, productWidth - 100);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 40);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 50);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth - 40);
                        tbl.AddColumn(ParagraphAlignment.Center, numericWidth + 60);

                        Row row1 = tbl.AddRow();
                        row1.HeadingFormat = true;
                        row1.Shading.Color = Colors.White;
                        row1.TopPadding = 5;
                        row1.BottomPadding = 5;

                        row1.Cells[0].AddParagraph("Item Code", ParagraphAlignment.Left);
                        row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[0].Format.Font.Bold = true;

                        row1.Cells[1].AddParagraph("Description", ParagraphAlignment.Left);
                        row1.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[1].Format.Font.Bold = true;

                        row1.Cells[2].AddParagraph("UOM", ParagraphAlignment.Center);
                        row1.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[2].Format.Font.Bold = true;

                        row1.Cells[3].AddParagraph("Price", ParagraphAlignment.Center);
                        row1.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[3].Format.Font.Bold = true;

                        row1.Cells[4].AddParagraph("Order Qty", ParagraphAlignment.Center);
                        row1.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[4].Format.Font.Bold = true;

                        row1.Cells[5].AddParagraph("Accepted Qty", ParagraphAlignment.Center);
                        row1.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[5].Format.Font.Bold = true;

                        row1.Cells[6].AddParagraph("Delivery Comments", ParagraphAlignment.Left);
                        row1.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                        row1.Cells[6].Format.Font.Bold = true;

                        for (int i = 0; i < items.Count; i++)
                        {
                            row1 = tbl.AddRow();
                            Cell cell = row1.Cells[0];
                            cell.Format.SpaceBefore = 4;
                            cell.Format.SpaceAfter = 4;
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(items[i].Product, ParagraphAlignment.Left);

                            cell = row1.Cells[1];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(items[i].Description, ParagraphAlignment.Left);

                            cell = row1.Cells[2];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(items[i].UOM, ParagraphAlignment.Center);

                            cell = row1.Cells[3];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph("$" + string.Format("{0:#,0.00}", items[i].Price), ParagraphAlignment.Center);

                            cell = row1.Cells[4];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(items[i].OQty.ToString(), ParagraphAlignment.Center);

                            cell = row1.Cells[5];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(items[i].AQty.ToString(), ParagraphAlignment.Center);

                            cell = row1.Cells[6];
                            cell.VerticalAlignment = VerticalAlignment.Center;
                            cell.AddParagraph(items[i].ItemIssue, ParagraphAlignment.Left);
                            cell.Format.Font.Color = Colors.Red;
                        }

                        #endregion Table section for Delivery runs

                        #region detials summary

                        var signature = (from a in _db.DeliveryStatusMasters
                                         join b in _db.DeliveryStatusImageMasters on a.DeliveryID equals b.DeliveryID
                                         where a.DeliveryID == deliveryID && b.IsSignature == true
                                         select new
                                         {
                                             Signature = "D:/Apps/SunCoast Fresh/Rf8tjk/PodImages/" + a.OrderID + "/" + b.ImageName
                                         }).FirstOrDefault();//.Signature;
                        string sig = "";
                        if (signature != null)
                        {
                            sig = signature.Signature;
                        }
                        else
                        {
                            sig = "";
                        }
                        Section section3 = Pdf.LastSection;

                        Paragraph p2 = section3.AddParagraph();
                        p2.AddText("");
                        p2.Format.LineSpacing = 2;
                        p2.Format.Borders.Top = BorderLine2;
                        p2.Format.SpaceBefore = 20;

                        Table tbl2 = section3.AddTable();
                        tbl2.Borders.Top = BorderLine2;
                        tbl2.Borders.Bottom = BorderLine2;
                        tbl2.Borders.Left = BorderLine2;
                        tbl2.Borders.Right = BorderLine2;

                        width = section3.PageWidth();
                        numericWidth = width / 2;
                        int headerWidth = 150;

                        tbl2.AddColumn(ParagraphAlignment.Left, headerWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth - headerWidth);
                        tbl2.AddColumn(ParagraphAlignment.Left, headerWidth);
                        tbl2.AddColumn(ParagraphAlignment.Center, numericWidth - headerWidth);

                        Row r2 = tbl2.AddRow();
                        r2.TopPadding = 2;
                        r2.BottomPadding = 2;
                        r2.Cells[0].AddParagraph("Items Delivered", ParagraphAlignment.Left);
                        r2.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[1].AddParagraph(items.Count.ToString(), ParagraphAlignment.Left);
                        r2.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[1].Format.Font.Bold = true;

                        r2.Cells[2].AddParagraph("Invoice Total:", ParagraphAlignment.Left);
                        r2.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[3].AddParagraph("$" + string.Format("{0:#,0.00}", sum), ParagraphAlignment.Left);
                        r2.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[3].Format.Font.Bold = true;

                        r2 = tbl2.AddRow();
                        r2.TopPadding = 2;
                        r2.BottomPadding = 2;
                        r2.Cells[0].AddParagraph("Carton's Delivered:", ParagraphAlignment.Left);
                        r2.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[1].AddParagraph(deliveryDetails.Cartons.ToString(), ParagraphAlignment.Left);
                        r2.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[1].Format.Font.Bold = true;

                        r2.Cells[2].AddParagraph("Payment Method:", ParagraphAlignment.Left);
                        r2.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[3].AddParagraph(deliveryDetails.PaymentMethod, ParagraphAlignment.Left);
                        r2.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[3].Format.Font.Bold = true;

                        r2 = tbl2.AddRow();
                        r2.TopPadding = 2;
                        r2.BottomPadding = 2;
                        r2.Cells[0].AddParagraph("Delivery Issue:", ParagraphAlignment.Left);
                        r2.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[1].AddParagraph(deliveryDetails.DeliveryIssue, ParagraphAlignment.Left);
                        r2.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[1].Format.Font.Bold = true;

                        r2.Cells[2].AddParagraph("Cash Received:", ParagraphAlignment.Left);
                        r2.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[3].AddParagraph("$" + string.Format("{0:#,0.00}", deliveryDetails.Cash), ParagraphAlignment.Left);
                        r2.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        r2.Cells[3].Format.Font.Bold = true;
                        r2.Cells[3].Format.Font.Color = Colors.Red;

                        #endregion detials summary

                        #region Delivery summary

                        Section section4 = Pdf.LastSection;

                        Paragraph p3 = section4.AddParagraph();
                        p3.AddText("");
                        p3.Format.LineSpacing = 2;
                        p3.Format.Borders.Top = BorderLine2;
                        p3.Format.SpaceBefore = 20;

                        Table tbl3 = section4.AddTable();
                        tbl3.Borders.Top = BorderLine2;
                        tbl3.Borders.Bottom = BorderLine2;
                        tbl3.Borders.Left = BorderLine2;
                        tbl3.Borders.Right = BorderLine2;

                        tbl3.AddColumn(ParagraphAlignment.Left, headerWidth);
                        tbl3.AddColumn(ParagraphAlignment.Center, numericWidth - headerWidth);
                        tbl3.AddColumn(ParagraphAlignment.Left, headerWidth);
                        tbl3.AddColumn(ParagraphAlignment.Center, numericWidth - headerWidth);

                        r = tbl3.AddRow();
                        r.TopPadding = 2;
                        r.BottomPadding = 2;
                        r.Cells[0].AddParagraph("Delivery Date", ParagraphAlignment.Left);
                        r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        r.Cells[1].AddParagraph(deliveryDetails.OrderDate.To<DateTime>().ToString("dd/MM/yyyy"), ParagraphAlignment.Left);
                        r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        r.Cells[1].Format.Font.Bold = true;

                        r.Cells[2].AddParagraph("Accepted By:", ParagraphAlignment.Left);
                        r.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                        r.Cells[3].AddParagraph(deliveryDetails.AcceptedBy, ParagraphAlignment.Left);
                        r.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        r.Cells[3].Format.Font.Bold = true;

                        r = tbl3.AddRow();
                        r.TopPadding = 5;
                        r.BottomPadding = 5;
                        r.Cells[0].AddParagraph("Delivery Time:", ParagraphAlignment.Left);
                        r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        r.Cells[1].AddParagraph(deliveryDetails.DeliveryTime.To<DateTime>().ToString("hh:mm tt"), ParagraphAlignment.Left);
                        r.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                        r.Cells[1].Format.Font.Bold = true;

                        r.Cells[2].AddParagraph("Signature:", ParagraphAlignment.Left);
                        r.Cells[2].VerticalAlignment = VerticalAlignment.Center;

                        if (sig == "")
                        {
                            r.Cells[3].AddParagraph("No Signature added");
                        }
                        else
                        {
                            r.Cells[3].AddImage(sig).Height = 100;
                        }

                        r.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                        r.Cells[3].Format.Font.Bold = true;

                        r = tbl3.AddRow();
                        r.TopPadding = 2;
                        r.BottomPadding = 2;
                        r.Cells[0].AddParagraph("Delivery Location:", ParagraphAlignment.Left);
                        r.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        Paragraph pLoc = r.Cells[1].AddParagraph("", ParagraphAlignment.Center); //.AddHyperlink("http://www.google.com", HyperlinkType.Web);
                        pLoc.Format.Font.Color = Colors.Blue;
                        pLoc.Format.Font.Underline = Underline.Dash;

                        // Hyperlink pLink = pLoc.AddHyperlink("https://www.google.com/maps/search/?api=1&query=" + deliveryDetails.Address1 + ", " + deliveryDetails.Address2 + ", " + deliveryDetails.City + ", " + deliveryDetails.Suburb + ", " + deliveryDetails.PostCode + "", HyperlinkType.Web);
                        Hyperlink pLink = pLoc.AddHyperlink("https://maps.google.com/?q=" + deliveryDetails.Lat + "," + deliveryDetails.Lng + "", HyperlinkType.Web);
                        pLink.AddFormattedText("Map");
                        r.Cells[1].VerticalAlignment = VerticalAlignment.Center;

                        #endregion Delivery summary

                        #region Item Issues section

                        if (deliveryDetails.HasItemIssues == true)
                        {
                            List<ImageIssueList> productIssues = new List<ImageIssueList>();

                            productIssues = (from a in _db.DeliveryStatusMasters
                                             join e in _db.trnsCartMasters on a.OrderID equals e.CartID
                                             join b in _db.DeliveryStatusImageMasters on a.DeliveryID equals b.DeliveryID
                                             join c in _db.DeliveryIssuesMasters on a.DeliveryID equals c.DeliveryID
                                             join d in _db.ProductMasters on c.ProductID equals d.ProductID
                                             where a.DeliveryID == deliveryID && c.ProductID == d.ProductID && b.IsSignature == false
                                             select new ImageIssueList()
                                             {
                                                 ImageURL = "D:/Apps/SunCoast Fresh/Rf8tjk/PodImages/" + a.OrderID + "/" + b.ImageName,
                                                 DeliveryID = a.DeliveryID,
                                                 OrderID = e.InvoiceNumber ?? a.OrderID.ToString(),
                                                 IssueDesc = c.Description,
                                                 ProductName = d.ProductName
                                             }).ToList();

                            if (productIssues != null && productIssues.Count > 0)
                            {
                                Paragraph paragraph5 = section4.AddParagraph();
                                paragraph5.AddText("Item Issues");
                                paragraph5.Format.Font.Size = 15;
                                paragraph5.Format.Font.Underline = Underline.Single;
                                paragraph5.Format.Font.Bold = true;
                                paragraph5.Format.Alignment = ParagraphAlignment.Left;
                                paragraph5.Format.SpaceBefore = 20;
                                paragraph5.Format.SpaceAfter = 10;

                                Table tbl4 = section3.AddTable();
                                tbl4.Borders.Top = BorderLine2;
                                tbl4.Borders.Bottom = BorderLine2;
                                tbl4.Borders.Left = BorderLine2;
                                tbl4.Borders.Right = BorderLine2;

                                numericWidth = width / 4;
                                tbl4.AddColumn(ParagraphAlignment.Center, numericWidth - 100);
                                tbl4.AddColumn(ParagraphAlignment.Center, numericWidth + 100);
                                tbl4.AddColumn(ParagraphAlignment.Center, numericWidth - 50);
                                tbl4.AddColumn(ParagraphAlignment.Center, numericWidth + 50);

                                Row r3 = tbl4.AddRow();
                                r3.Cells[0].AddParagraph("INVOICE NO.", ParagraphAlignment.Left);
                                r3.Cells[0].Format.Font.Bold = true;

                                r3.Cells[1].AddParagraph("PRODUCT NAME", ParagraphAlignment.Left);
                                r3.Cells[1].Format.Font.Bold = true;

                                r3.Cells[2].AddParagraph("DESCRIPTION", ParagraphAlignment.Left);
                                r3.Cells[2].Format.Font.Bold = true;

                                r3.Cells[3].AddParagraph("IMAGE", ParagraphAlignment.Left);
                                r3.Cells[3].Format.Font.Bold = true;
                                r3.TopPadding = 5;
                                r3.BottomPadding = 5;

                                foreach (var item in productIssues)
                                {
                                    r3 = tbl4.AddRow();
                                    r3.Cells[0].AddParagraph(item.OrderID.ToString());
                                    r3.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                                    r3.Cells[1].AddParagraph(item.ProductName, ParagraphAlignment.Left);
                                    r3.Cells[1].VerticalAlignment = VerticalAlignment.Center;

                                    r3.Cells[2].AddParagraph(item.IssueDesc, ParagraphAlignment.Left);
                                    r3.Cells[2].VerticalAlignment = VerticalAlignment.Center;

                                    if (item.ImageURL != "")
                                    {
                                        r3.Cells[3].AddImage(item.ImageURL).Height = 150;
                                    }
                                    else
                                    {
                                        r3.Cells[3].AddParagraph("No Image added");
                                    }
                                    r3.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                                    r3.TopPadding = 10;
                                    r3.BottomPadding = 10;
                                }
                            }
                        }

                        #endregion Item Issues section

                        // Saving the pdf file
                        string unique = Guid.NewGuid().ToString().Replace("-", "");
                        string pdfPath = HostingEnvironment.MapPath("~/PODImages/DeliveryConfirmation_" + invoiceNo + "_" + unique + ".pdf");

                        PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
                        renderer.Document = Pdf;
                        renderer.RenderDocument();
                        renderer.PdfDocument.Save(pdfPath);

                        string sub = "Delivery Confirmation for Invoice No. " + invoiceNo;
                        string msg = "Dear Customer,<br><br> Please find the delivery confirmation details attached along with this email.<br><br> Suncoast Fresh POD System.";

                        var emailDetails = (from a in _db.trnsCartMasters
                                            join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID
                                            join c in _db.UserMasters on a.UserID equals c.UserID into temp
                                            from c in temp.DefaultIfEmpty()
                                            where a.InvoiceNumber == invoiceNo
                                            select new
                                            {
                                                OrderEmail = c.Email ?? b.Email
                                            }).FirstOrDefault();

                        SendMail.SendEMail(emailDetails.OrderEmail, "", HelperClass.DeliveryIssueEmail, sub, msg, pdfPath, HelperClass.FromEmail);

                        return "Success";
                    }
                    else
                    {
                        return "No Data";
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorsToServer("Delivery confirmation PDF Error: " + ex.Message, invoiceNo, "Error");
                return "Error";
            }
        }

        #endregion Generate Driver summary PDF

        #region Log Errors to server for order posting

        private void LogErrorsToServer(string ex, string cusOrderID, string type)
        {
            using (var _db = new Saavi5Entities())
            {
                trnsCartResponseMaster cr = new trnsCartResponseMaster();
                cr.CartId = cusOrderID.To<long>();
                cr.MethodName = type;
                cr.Response = ex;
                cr.CreatedDate = DateTime.Now;

                _db.trnsCartResponseMasters.Add(cr);
                _db.SaveChanges();
            }
        }

        #endregion Log Errors to server for order posting

        #region Get vehicle checklist

        public async Task<APiResponse> FetchTruckCompliance(GetTruckCompliance model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    //long checkListID = model.CheckListID;
                    DateTime date = DateTime.Now.Date;
                    if (!string.IsNullOrEmpty(model.Date))
                    {
                        date = Convert.ToDateTime(model.Date.Split('/')[1] + "/" + model.Date.Split('/')[0] + "/" + model.Date.Split('/')[2]);
                    }
                    var checkList = await (from a in _db.VehicleMasters
                                           join b in _db.VehicleChecklistMasters on a.VehicleID equals b.VehicleID
                                           where a.VehicleNo == model.VehicleNo && b.DriverID == model.DriverID && DbFunctions.TruncateTime(b.CreatedDate) == DbFunctions.TruncateTime(date) && b.RunNo == model.RunNo
                                           //(checkListID == 0 || b.ID == checkListID) &&
                                           select new
                                           {
                                               VehicleID = a.VehicleID,
                                               VehicleName = a.VehicleName,
                                               VehicleNo = a.VehicleNo,
                                               DriverID = b.DriverID,
                                               IsActive = b.IsActive,
                                               IsFixturesFittingOK = b.IsFixturesFittingOK,
                                               IsLightSignalOK = b.IsLightSignalOK,
                                               IsTyreConditionOK = b.IsTyreConditionOK,
                                               IsVehicleConditionOK = b.IsVehicleConditionOK,
                                               IsWindscreenOK = b.IsWindscreenOK,
                                               OdometerEnd = b.OdometerEnd,
                                               OdometerStart = b.OdometerStart,
                                               RunNo = b.RunNo,
                                               Shift = b.Shift,
                                               TempChilled = b.TempChilled,
                                               TempFrozen = b.TempFrozen,
                                               Temprature = b.Temprature,
                                               Weight = b.Weight,
                                               CreatedDate = b.CreatedDate,
                                               VehicleService = b.VehicleService,
                                               Fuel = b.Fuel,
                                               EODComment = b.EODComment,
                                               Comments = b.Comments,
                                               TempChilledEnd = b.TempChilledEnd,
                                               TempFrozenEnd = b.TempFrozenEnd
                                           }).OrderByDescending(o => o.CreatedDate).FirstOrDefaultAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { VehicleCheckList = checkList });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get vehicle checklist

        #region Add Driver Comment

        public async Task<APiResponse> AddDriverComment(DriverComment model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var dc = new DeliveryCommentsMaster();
                    dc.CompanyID = 1;
                    dc.CustomerID = 0;
                    dc.CommentDescription = model.Comment;
                    dc.CreatedDate = DateTime.Now;
                    dc.ModifiedDate = DateTime.Now;
                    dc.IsActive = true;
                    _db.DeliveryCommentsMasters.Add(dc);
                    _db.SaveChanges();

                    var delStatus = _db.trnsCartMasters.Find(model.OrderID);
                    var driver = _db.UserMasters.Find(model.DriverID);

                    if (delStatus != null)
                    {
                        delStatus.DeliveredCommentID = dc.CommentID;
                        await _db.SaveChangesAsync();

                        string sub = "Delivery comment by driver " + driver.FirstName + " for Invoice number " + delStatus.InvoiceNumber;
                        string msg = "Driver " + driver.FirstName + ", added a comment for Invoice number " + delStatus.InvoiceNumber + ", for customer " + model.CustomerName + ".";

                        msg += "<br>Comment was:<br><i> <p style='color:red;'>" + model.Comment + "</p></i>";

                        SendMail.SendEMail(HelperClass.DeliveryIssueEmail, "", "", sub, msg, "", HelperClass.FromEmail);
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add Driver Comment

        #region Email PDF INVOICE

        public async Task<APiResponse> SendInvoiceToEmail(SendInvoiceBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    string sub = "SUNCOAST FRESH – INVOICE COPY OF " + model.InvoiceNo;
                    string msg = "";
                    string invNo = model.InvoiceNo.ToString();
                    var cart = await _db.trnsCartMasters.Where(c => c.InvoiceNumber == invNo).FirstOrDefaultAsync();
                    if (cart != null)
                    {
                        var cust = await _db.CustomerMasters.FindAsync(cart.CustomerID);
                        var driver = _db.UserMasters.Find(cart.DriverID);
                        string driverName = "";

                        if (driver != null)
                        {
                            driverName = driver.FirstName + " " + driver.LastName;
                        }
                        var obj = new OrdersDA();
                        var bo = new OrderDetailsBO();
                        bo.OrderID = cart.CartID;
                        bo.CustomerID = cart.CustomerID;
                        bo.IsOrderHistory = true;
                        bo.UserID = cart.UserID.To<long>();

                        var details = obj.GetOrderDetails(bo);

                        var pdf = obj.OrderPDF(details, cart.CartID, cust, "", "Yes");

                        msg = "Dear " + cust.CustomerName + ", <br><br> Please find attached a copy of the requested invoice " + model.InvoiceNo + " from your driver " + driverName + " from the " + cart.RunNo + " run. <br><br> From the Team at Suncoast Fresh.";

                        string res = SendMail.SendEMail(model.Email, "", "", sub, msg, pdf, HelperClass.FromEmail);

                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NotFound, "No Invoices found with Order No: " + model.InvoiceNo.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorsToServer(ex.Message, "0", "SendInvoiceEmail");
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Email PDF INVOICE
    }
}