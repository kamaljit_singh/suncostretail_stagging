﻿using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using System.IO;
using System.Web.Hosting;
using System.Collections;
using CommonFunctions;
using System.Net.Http;
using Newtonsoft.Json;

namespace Saavi5Api.DataAccess.DA
{
    public class AccountDA
    {
        #region Register a new user

        public async Task<APiResponse> RegisterUser(RegisterModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    string retMessage = "Success";
                    var checkPostcode = _db.MainFeaturesMasters.FirstOrDefault().IsCheckPostcode ?? false;

                    if (checkPostcode)
                    {
                        var post = _db.ImportPostcodes.Where(p => p.C24Hours == model.Postcode || p.C48Hour == model.Postcode || p.Exclude == model.Postcode).FirstOrDefault();

                        if (post != null)
                        {
                            if (post.C24Hours != null && post.C24Hours == model.Postcode)
                            {
                                retMessage = "Based on your post code, we will use our best endeavours to deliver within the next 24 hours, as long as order are placed by midnight";
                            }
                            else if (post.C48Hour != null && post.C48Hour == model.Postcode)
                            {
                                retMessage = "Based on your post code, we will use our best endeavours to deliver within the next 48 hours, as long as orders are placed by midnight";
                            }
                            else
                            {
                                retMessage = "Based on your post code, we are sorry to advise that your post code is outside our available delivery zones at this time. We apologise for any inconvenience caused but we cannot accept your registration.";
                                return ReturnMethod(SaaviErrorCodes.Error, retMessage);
                            }
                        }
                        else
                        {
                            retMessage = "Based on your post code, we are sorry to advise that your post code is outside our available delivery zones at this time. We apologise for any inconvenience caused but we cannot accept your registration.";
                            return ReturnMethod(SaaviErrorCodes.Error, retMessage);
                        }
                    }

                    var verifyMail = _db.UserMasters.Where(u => u.Email == model.Email).FirstOrDefault();

                    if (verifyMail != null)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "Email already exists for another customer.");
                    }

                    byte[] saltBytes = new byte[8];
                    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                    rng.GetNonZeroBytes(saltBytes);
                    string pwdSalt = Convert.ToBase64String(saltBytes);
                    string pwdHash = ENCDEC.ComputeHash(model.Password, "MD5", saltBytes);

                    var user = new UserMaster()
                    {
                        UserName = model.UserName,
                        Email = model.Email,
                        FirstName = (model.FirstName ?? "") == "" ? model.UserName : model.FirstName,
                        LastName = model.LastName,
                        DeviceToken = model.DeviceToken,
                        DeviceType = model.DeviceType,
                        PasswordHash = pwdHash,
                        PasswordSalt = pwdSalt,
                        UserTypeID = model.UserTypeID,
                        CreatedOn = DateTime.Now,
                        Phone = model.Phone,
                        BusinessName = model.BusinessName,
                        DefaultPassword = model.Password,
                        ExistingUserFlag = model.ExistingAccount,
                        IsActive = false,
                        StreetAddress = model.StreetAddress,
                        StreetAddress2 = model.StreetAddress2,
                        Suburb = model.Suburb,
                        Postcode = model.Postcode,
                        ABN = model.ABN,
                        Latitude = model.Latitude,
                        Longitude = model.Longitude
                    };

                    // code to check if a delivery run for the selected suburb exists or not
                    var run = _db.CutoffGroupDetailMasters.Where(x => x.GroupFilter == "Suburb" && x.ListValue.Contains(model.Suburb)).FirstOrDefault();
                    bool subExists = true;
                    if (run != null)
                    {
                        retMessage = "Important Note: Unfortunately, we do not deliver to your suburb at the moment. However, when you place your order, you can nominate from a range of pick-up locations close to your suburb. We are planning to increase our delivery coverage over time. Thanks!";
                        subExists = false;
                    }

                    if (model.ExistingAccount)
                    {
                        long custID = CreateCustomerForUser(model, _db);

                        AddInitialAddressEntry(model, _db, custID, subExists);

                        user.CustomerID = custID;
                        user.IsActive = true;
                    }

                    _db.UserMasters.Add(user);
                    int result = await _db.SaveChangesAsync();

                    if (result > 0)
                    {
                        //MailChimp
                        AddSubscribeAddress(user.Email, user.FirstName, user.LastName);

                        Hashtable hstbl = new Hashtable();

                        hstbl["@DEAR"] = "Dear";
                        hstbl["@Customer"] = model.BusinessName ?? "";
                        hstbl["@CName"] = (model.BusinessName ?? "") == "" ? model.UserName : model.BusinessName;
                        hstbl["@ContactName"] = model.UserName;
                        hstbl["@Email"] = model.Email;
                        hstbl["@Phone"] = model.Phone;
                        hstbl["@StreetAddress"] = model.StreetAddress;
                        hstbl["@SAddress2"] = model.StreetAddress2;
                        hstbl["@Suburb"] = model.Suburb;
                        hstbl["@Postcode"] = model.Postcode;
                        hstbl["@Abn"] = model.ABN ?? "";
                        //hstbl["@Password"] = model.Password;
                        hstbl["@Username"] = model.Email;

                        string ExistingCustomer = model.ExistingAccount ? "Yes" : "No";

                        hstbl["@ExistingCustomerFlag"] = ExistingCustomer;
                        hstbl["@path"] = HelperClass.ApiURLLive;
                        hstbl["@datetime"] = HelperClass.GetCurrentTime().ToString("dd-MM-yyyy").Replace("-", "/");
                        hstbl["@Company"] = HelperClass.siteName;
                        hstbl["@Adminpath"] = HelperClass.WebsiteUrl + "Admin";

                        string res = SendMail.SendEMail(HelperClass.AdminEmail, "", "", HelperClass.siteName + " – Mobile Ordering Registration Request", HelperClass.TempltePath, "regadminmail.htm", hstbl, "", HelperClass.FromEmail);

                        if (res != "ok")
                        {
                            LogErrorsToServer("Error sending admin email: " + res, "0", "Error");
                        }
                        if (model.ExistingAccount)
                        {
                            res = SendMail.SendEMail(model.Email, "", "", HelperClass.siteName + " – Mobile Ordering Registration", HelperClass.TempltePath, "RegRetailCustomer.htm", hstbl, "", HelperClass.FromEmail);
                            if (res != "ok")
                            {
                                LogErrorsToServer("Error sending Retail email: " + res, "0", "Error");
                            }
                        }
                        else
                        {
                            res = SendMail.SendEMail(model.Email, "", "", HelperClass.siteName + " – Mobile Ordering Registration Request", HelperClass.TempltePath, "regcustomermail.htm", hstbl, "", HelperClass.FromEmail);
                            if (res != "ok")
                            {
                                LogErrorsToServer("Error sending customer email: " + res, "0", "Error");
                            }
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, retMessage);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.Error, "Unknown error occured while registring the user.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        private static long CreateCustomerForUser(RegisterModel model, Saavi5Entities _db)
        {
            var cust = new CustomerMaster();
            string state = _db.DeliveryRuns.Where(x => x.Suburb == model.Suburb).FirstOrDefault()?.State;
            int? stateID = 0;

            if (state != null)
            {
                stateID = _db.StateMasters.Where(x => x.StateCode == state).FirstOrDefault()?.StateID;
            }

            cust.CustomerName = (model.UserName == null || model.UserName == "") ? model.FirstName : model.UserName;
            cust.IsActive = true;
            cust.DebtorOnHold = false;
            cust.AlphaCode = "";
            cust.Email = model.Email;
            cust.Phone1 = model.Phone;
            cust.Address1 = model.StreetAddress;
            cust.Suburb = model.Suburb;
            cust.PostCode = model.Postcode;
            cust.SalesmanCode = "Default";
            cust.IsRep = false;
            cust.CreatedDate = DateTime.Now;
            cust.CompanyID = 1;
            cust.ContactName = (model.UserName == null || model.UserName == "") ? model.FirstName : model.UserName;
            cust.StateID = stateID;

            var run = _db.CutoffGroupDetailMasters.Where(x => x.GroupFilter == "Suburb" && x.ListValue.Contains(model.Suburb)).FirstOrDefault();

            if (run != null)
            {
                var res = _db.CutoffGroupMasters.Where(x => x.GroupID == run.GroupID).FirstOrDefault();
                cust.RunNo = res.GroupName;
                cust.Warehouse = res.GroupDetail;
            }
            else
            {
                cust.RunNo = _db.DeliveryRuns.Where(x => x.Suburb == model.Suburb).FirstOrDefault()?.DeliveryRun1 ?? "";
                cust.Warehouse = _db.DeliveryRuns.Where(x => x.Suburb == model.Suburb).FirstOrDefault()?.Warehouse ?? "Brisbane";
            }

            _db.CustomerMasters.Add(cust);
            _db.SaveChanges();

            return cust.CustomerID;
        }

        private static void AddInitialAddressEntry(RegisterModel model, Saavi5Entities _db, long custID, bool subExists)
        {
            var custAddr = new CustomerDeliveryAddress();
            custAddr.CustomerID = custID;
            custAddr.Address1 = model.StreetAddress;
            custAddr.ZIP = model.Postcode;
            custAddr.Phone1 = model.Phone;
            custAddr.ContactName = model.UserName;
            custAddr.Suburb = model.Suburb.Trim();
            custAddr.IsActive = true;

            if (subExists)
            {
                custAddr.AddressCode = "Y";
            }

            _db.CustomerDeliveryAddresses.Add(custAddr);
            _db.SaveChanges();
        }

        #endregion Register a new user

        #region Get user profile information

        public async Task<APiResponse> GetUserProfile(CustomerfeaturesBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    long? customerID = 0;
                    var user = new UserMaster();
                    var repUser = new UserMaster();
                    var customer = new CustomerMaster();
                    if (model.IsRepUser)
                    {
                        customerID = model.CustomerID;
                        repUser = _db.UserMasters.Find(model.userID);
                        user = _db.UserMasters.Where(u => u.CustomerID == model.CustomerID).FirstOrDefault();
                        model.userID = user.UserID;
                    }
                    else
                    {
                        customerID = _db.UserMasters.Find(model.userID).CustomerID;
                    }
                    bool debtorOnHold = false;
                    if (customerID != null)
                    {
                        customer = _db.CustomerMasters.Find(customerID);
                        debtorOnHold = customer.DebtorOnHold ?? false;
                    }
                    var userProfile = await (from u in _db.UserMasters
                                             where u.UserID == model.userID
                                             select new
                                             {
                                                 Firstname = (u.FirstName == null || u.FirstName == "") ? customer.CustomerName : u.FirstName,
                                                 u.LastName,
                                                 u.UserID,
                                                 u.UserName,
                                                 u.Email,
                                                 u.DefaultPassword,
                                                 Phone = (u.Phone != null || u.Phone != "") ? u.Phone : "",
                                                 u.Image,
                                                 u.CustomerID,
                                                 BusinessName = u.BusinessName ?? customer.CustomerName,
                                                 DebtorOnHold = debtorOnHold,
                                                 Messages = _db.PushMessageMasters.Where(p => p.UserID == model.userID).OrderByDescending(d => d.MessageID).ToList(),
                                                 Salesrep = repUser.FirstName ?? "" + " " + repUser.LastName ?? "",
                                                 CurrentBalance = customer.CurrentBalance ?? 0,
                                                 TotalBalance = customer.TotalAmountOwing ?? 0,
                                                 Bal30Days = customer.Day30Balance ?? 0,
                                                 Bal60Days = customer.Day60Balance ?? 0,
                                                 Bal90Days = customer.Day90PlusBalance ?? 0,
                                                 Bal120PlusDays = customer.Day120PlusBalance ?? 0,
                                                 Terms = customer.TermDescription ?? ""
                                             }).ToListAsync();

                    string message = userProfile.Count > 0 ? "Success" : "No Data";
                    string resCode = userProfile.Count > 0 ? SaaviErrorCodes.Success : SaaviErrorCodes.NoData;

                    return ReturnMethod(resCode, message, userProfile);
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get user profile information

        #region Set new password after first time login.

        public APiResponse SetNewPassword(SetPasswordModel model)
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    var user = db.UserMasters.Where(u => u.ResetCode == model.ResetToken).FirstOrDefault();
                    if (user != null)
                    {
                        byte[] saltBytes = new byte[8];
                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                        rng.GetNonZeroBytes(saltBytes);
                        string pwdSalt = Convert.ToBase64String(saltBytes);
                        string pwdHash = ENCDEC.ComputeHash(model.NewPassword, "MD5", saltBytes);

                        user.PasswordHash = pwdHash;
                        user.PasswordSalt = pwdSalt;
                        user.ModifiedOn = DateTime.Now;
                        user.DefaultPassword = model.NewPassword;
                        // user.ResetCode = "";
                        var res = db.SaveChanges();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NotFound, "Invalid reset token provided.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Set new password after first time login.

        #region Change user password

        public async Task<APiResponse> ChangePassword(ChangePasswordModel model)
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    var user = await db.UserMasters.Where(u => u.UserID == model.UserID).FirstOrDefaultAsync();

                    byte[] saltBytes = new byte[8];
                    saltBytes = Convert.FromBase64String(user.PasswordSalt);
                    if (ENCDEC.ComputeHash(model.OldPassword, "MD5", saltBytes) == user.PasswordHash)
                    {
                        byte[] saltBytesNew = new byte[8];
                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                        rng.GetNonZeroBytes(saltBytesNew);
                        string pwdSalt = Convert.ToBase64String(saltBytes);
                        string pwdHash = ENCDEC.ComputeHash(model.NewPassword, "MD5", saltBytes);

                        user.PasswordHash = pwdHash;
                        user.PasswordSalt = pwdSalt;

                        var res = await db.SaveChangesAsync();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "The old password you provided is incorrect.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Change user password

        #region Update user profile

        public async Task<APiResponse> UpdateUserProfile(UserProfile model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var user = _db.UserMasters.Find(model.UserID);
                    Mapper.Map(model, user);

                    var res = await _db.SaveChangesAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Update user profile

        #region Reset Password for user

        //Need to check which method to use for sending Reset Code
        public async Task<APiResponse> ResetPassword(ResetPasswordModel model)
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    var user = db.UserMasters.Where(u => u.UserID == model.UserID).FirstOrDefault();

                    if (user != null)
                    {
                        byte[] saltBytes = new byte[8];
                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                        rng.GetNonZeroBytes(saltBytes);
                        string pwdSalt = Convert.ToBase64String(saltBytes);
                        string pwdHash = ENCDEC.ComputeHash(model.NewPassword, "MD5", saltBytes);

                        user.PasswordHash = pwdHash;
                        user.PasswordSalt = pwdSalt;
                        user.ModifiedOn = DateTime.Now;
                        user.ResetCode = "";

                        var res = await db.SaveChangesAsync();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "Reset code invalid");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Reset Password for user

        #region Forgot password

        public async Task<APiResponse> ForgotPassword(ForgotPasswordModel model)
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    var user = await db.UserMasters.Where(u => u.Email == model.Email && u.IsActive == true).FirstOrDefaultAsync();
                    string link = "";
                    if (user != null)
                    {
                        string resetCode = Guid.NewGuid().ToString().Replace("-", "");
                        user.ResetCode = resetCode;
                        db.SaveChanges();

                        if (user.UserTypeID == 5)
                        {
                            link = "ResetPODPassword.html?code=" + resetCode;
                        }
                        else
                        {
                            link = "ResetPassword.html?code=" + resetCode;
                        }

                        string mailContent = File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplates/forgotpasswordmail.htm"));

                        mailContent = mailContent.Replace("@NAME", user.FirstName + " " + user.LastName);
                        mailContent = mailContent.Replace("@datetime", HelperClass.GetCurrentTime().ToString("dd/MM/yyyy"));

                        mailContent = mailContent.Replace("@Link", HelperClass.ApiURLLive + link);
                        mailContent = mailContent.Replace("@Company", HelperClass.Company);

                        string res = SendMail.SendEMail(user.Email, "", "", "Forgot Password", mailContent, "", HelperClass.FromEmail);

                        return ReturnMethod(SaaviErrorCodes.Success, "An email with a link to reset your password has been sent to your registered email address.");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NotFound, "Email does not exist.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Forgot password

        #region Get customer details and customer features to be used through out the application.

        public async Task<APiResponse> GetCustomerDetailsAndFeatures(CustomerfeaturesBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (model.IsRepUser)
                    {
                        model.userID = _db.UserMasters.Where(u => u.CustomerID == model.CustomerID).FirstOrDefault().UserID;
                    }

                    var data = await _db.CustomerFeaturesMasters.Where(c => c.UserID == model.userID).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        return new APiResponse
                        {
                            ResponseCode = SaaviErrorCodes.NotFound,
                            Message = "No record found"
                        };
                    }
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();
                    var features = Mapper.Map<CustomerFeaturesBO>(data);

                    var cust = (from cus in _db.CustomerMasters
                                join user in _db.UserMasters on cus.CustomerID equals user.CustomerID
                                join debt in _db.DebtorTypeMasters on cus.DebtorTypeID equals debt.DebtorTypeId into debtor
                                from debt in debtor.DefaultIfEmpty()
                                join st in _db.StateMasters on cus.StateID equals st.StateID into state
                                from st in state.DefaultIfEmpty()
                                join popup in _db.PopupMasters on 1 equals 1 into temp5
                                from popup in temp5.DefaultIfEmpty()
                                where user.UserID == model.userID //&& user.CustomerID == users.CustomerID
                                select new
                                {
                                    user.UserID,
                                    user.FirstName,
                                    user.LastName,
                                    cus.CustomerID,
                                    IsRetailUser = user.ExistingUserFlag ?? false,
                                    ContactName = cus.ContactName ?? "",
                                    CustomerName = cus.CustomerName ?? "",
                                    RunNo = cus.RunNo ?? "",
                                    IsActive = cus.IsActive ?? false,
                                    ProfileImage = HelperClass.WebsiteUrl + "Upload/Customer/" + cus.ProfileImage ?? "",
                                    IsRep = cus.IsRep ?? false,
                                    IsSundayrdering = cus.IsSundayOrdering ?? false,
                                    Phone1 = cus.Phone1 == null ? "" : cus.Phone1,
                                    Phone2 = cus.Phone2 == null ? "" : cus.Phone2,
                                    AccountType = debt.DebtorTypeName == null ? "N/A" : debt.DebtorTypeName,
                                    DebtorOnHold = cus.DebtorOnHold ?? false,
                                    ABN = cus.ABN == null ? "" : cus.ABN,
                                    AlphaCode = cus.AlphaCode == null ? "" : cus.AlphaCode,
                                    Address1 = cus.Address1 == null ? "" : cus.Address1,
                                    Address2 = cus.Address2 == null ? "" : cus.Address2,
                                    City = cus.City == null ? "" : cus.City,
                                    Fax = cus.Fax == null ? "" : cus.Fax,
                                    Email = cus.Email == null ? "" : cus.Email,
                                    PostCode = cus.PostCode == null ? "" : cus.PostCode,
                                    SalesmanCode = cus.SalesmanCode == null ? "" : cus.SalesmanCode,
                                    Suburb = cus.Suburb == null ? "" : cus.Suburb,
                                    StateName = st.StateName == null ? "" : st.StateName,
                                    Warehouse = cus.Warehouse == null ? "" : cus.Warehouse.Trim(),
                                    CreditLimit = cus.CreditLimit == null ? "" : cus.CreditLimit,
                                    CurrentBalance = cus.CurrentBalance == null ? "0" : cus.CurrentBalance.ToString(),
                                    OverDueBalance = cus.TotalAmountDue == null ? "0" : cus.TotalAmountDue.ToString(),
                                    TotalBalance = cus.TotalAmountOwing == null ? "0" : cus.TotalAmountOwing.ToString(),
                                    YTDSales = cus.YTDSales == null ? "0" : cus.YTDSales.ToString(),
                                    MTDSales = cus.MTDSales == null ? "0" : cus.MTDSales.ToString(),
                                    PrevMonth = cus.PrevMonthSales == null ? "0" : cus.PrevMonthSales.ToString(),
                                    //DownloadUrl = HelperClass.WebsiteUrl + "downloadpdf.aspx?i=" + HelperClass.DownloadPDF,
                                    PriceCode = cus.PriceLevel == null ? "" : cus.PriceLevel.ToString(),
                                    BalancePeriod1 = cus.Day30Balance == null ? "" : cus.Day30Balance.ToString(),
                                    BalancePeriod2 = cus.Day60Balance == null ? "" : cus.Day60Balance.ToString(),
                                    BalancePeriod3 = cus.Day90PlusBalance == null ? "" : cus.Day90PlusBalance.ToString(),
                                    //TermConditionTitle = "Welcome message:"//,
                                    TermconditionMessage = popup.Description == null ? "" : popup.Description
                                });

                    var customerDetails = cust.ToList();
                    var contactDetails = new List<PhoneInfo>();

                    if (Convert.ToBoolean(mainFeatures.IsShowContactDetails))
                    {
                        if (customerDetails[0].SalesmanCode != "" && customerDetails[0].IsRep == false)
                        {
                            List<PhoneInfo> PhoneInformation = new List<PhoneInfo>();
                            foreach (var repcustomer in _db.CustomerMasters.Where(s => s.IsRep == true))
                            {
                                // string[] repcodes = repcustomer.SalesmanCode.Split(',');
                                string[] repcodes = (repcustomer.SalesmanCode == null ? "" : repcustomer.SalesmanCode).Split(',');
                                if (repcodes.Contains(customerDetails[0].SalesmanCode))
                                {
                                    if ((repcustomer.Phone1 != null && repcustomer.Phone1.ToString() != "") || (repcustomer.Phone2 != null && repcustomer.Phone2.ToString() != ""))
                                    {
                                        PhoneInfo phninfo = new PhoneInfo();
                                        phninfo.Email = Convert.ToString(repcustomer.Email);
                                        phninfo.Name = Convert.ToString(repcustomer.CustomerName);
                                        phninfo.Phone1 = Convert.ToString(repcustomer.Phone1);
                                        phninfo.Phone2 = Convert.ToString(repcustomer.Phone2);
                                        PhoneInformation.Add(phninfo);
                                    }
                                }
                            }
                            contactDetails = PhoneInformation;
                        }
                    }

                    var deliveryCharges = new List<DeliveryChargesBO>();

                    var delCharges = _db.DeliveryCharges.ToList();

                    if (delCharges != null)
                    {
                        foreach (var delCharge in delCharges)
                        {
                            deliveryCharges.Add(new DeliveryChargesBO()
                            {
                                DeliveryFee = Convert.ToDouble(delCharge.DeliveryFee == null ? 0 : delCharge.DeliveryFee),
                                OrderValue = Convert.ToDouble(delCharge.OrderValue == null ? 0 : delCharge.OrderValue),
                                FrightMessage = "Order under $" + Convert.ToDouble(delCharge.OrderValue == null ? 0 : delCharge.OrderValue) + ", delivery charges $" + Convert.ToDouble(delCharge.DeliveryFee == null ? 0 : delCharge.DeliveryFee) + " will be applicable. Do you want to continue?",
                                NonDeliveryDaysFee = Convert.ToDouble(delCharge.NonDeliveryDayFee == null ? 0 : delCharge.NonDeliveryDayFee),
                                NonDeliveryDaysMessage = "You're placing an order for non delivery day, extra charges $" + Convert.ToDouble(delCharge.NonDeliveryDayFee == null ? 0 : delCharge.NonDeliveryDayFee) + " will be applicable. Do you want to continue?"
                            });
                        }
                    }

                    PDFDetailsBO pdf = new PDFDetailsBO();
                    List<PDFGroupDetails> pdfGroupDetails = new List<PDFGroupDetails>();
                    var customerPDF = _db.PDFMasters.FirstOrDefault();
                    foreach (var cusData in customerDetails)
                    {
                        List<GroupsMaster> grps = new List<GroupsMaster>();
                        var d = (from us in _db.UserGroupsMasters
                                 join cus in _db.GroupsMasters on us.GroupID equals cus.GroupID
                                 where (us.CustomerID == cusData.CustomerID)
                                 select new
                                 { // select only columns you need
                                     cus.GroupID,
                                     cus.GroupName,
                                     cus.Pdf
                                 })
                                .AsEnumerable() // execute query
                                .Select(x => new GroupsMaster
                                { // create instance of class
                                    GroupID = x.GroupID,
                                    GroupName = x.GroupName,
                                    Pdf = x.Pdf
                                });
                        grps = d.ToList();

                        if (grps.Count > 0)
                        {
                            foreach (var item in grps)
                            {
                                PDFGroupDetails pgd = new PDFGroupDetails();

                                pgd.GroupID = item.GroupID;
                                pgd.PdfFile = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Pdf/" + item.Pdf;
                                pgd.GroupName = item.GroupName;

                                var proDT = await (from gp in _db.GroupProductsMasters
                                                   join pro in _db.ProductMasters on gp.ProductID equals pro.ProductID
                                                   join sp in _db.SpecialPriceMasters.Where(s => s.CustomerID == cusData.CustomerID) on pro.ProductID equals sp.ProductID into temp
                                                   from sp in temp.DefaultIfEmpty()
                                                   join unit in _db.UnitOfMeasurementMasters on pro.UOMID equals unit.UOMID
                                                   join filter in _db.ProductFiltersMasters on pro.FilterID equals filter.FilterID
                                                   where (gp.GroupID == item.GroupID)
                                                   orderby (pro.ProductName)
                                                   select new PdfProductsBO
                                                   {
                                                       ProductID = pro.ProductID,
                                                       ProductName = pro.ProductName,
                                                       ProductCode = pro.ProductCode,
                                                       Description = pro.ProductDescription == null ? "" : pro.ProductDescription,
                                                       ProductImage = (pro.ProductImage != null && pro.ProductImage != "") ? HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + pro.ProductImage : "",
                                                       ProductImageThumb = (pro.ProductImage != null && pro.ProductImage != "") ? HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + "thumb_" + pro.ProductImage : "",
                                                       UOMID = unit.UOMID,
                                                       UOMDesc = unit.UOMDesc,
                                                       FilterID = filter.FilterID,
                                                       FilterName = filter.FilterName,
                                                       Price = pro.Price,
                                                       CompanyPrice = (pro.ComPrice == null) ? 0 : pro.ComPrice,
                                                       SpecialPrice = (sp.SpecialPrice == null) ? 0 : sp.SpecialPrice,
                                                       IsActive = pro.IsActive,
                                                       IsSpecial = (sp.SpecialPriceID == null) ? false : true,
                                                       IsAvailable = (pro.IsAvailable == null) ? false : pro.IsAvailable,
                                                       PiecesAndWeight = (pro.PiecesAndWeight == null) ? false : pro.PiecesAndWeight,
                                                       Quantity = 1
                                                   }).ToListAsync();

                                pgd.PDFProducts = proDT;

                                pdfGroupDetails.Add(pgd);
                            }
                            pdf.IsGrouped = true;
                            pdf.PdfFile = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Pdf/" + customerPDF.PDF;
                            pdf.GroupDetails = pdfGroupDetails;
                        }
                        else
                        {
                            pdf.IsGrouped = false;
                            pdf.PdfFile = HelperClass.WebsiteUrl + "Admin/content/Uploads/Pdf/" + customerPDF.PDF;
                        }
                    }

                    string message = customerDetails.Count() > 0 ? "Success" : "No Data";
                    return new APiResponse
                    {
                        ResponseCode = message == "Success" ? SaaviErrorCodes.Success : SaaviErrorCodes.NoData,
                        Message = message,
                        Result = new { customerFeatures = features, customerDetails = customerDetails, ContactDetails = contactDetails, DeliveryCharges = deliveryCharges, PDFDetails = pdf }
                    };
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get customer details and customer features to be used through out the application.

        #region Get customer main features

        public async Task<APiResponse> GetMainFeatures()
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    MainFeaturesBO obj = new MainFeaturesBO();
                    var mainFeatures = await _db.MainFeaturesMasters.FirstOrDefaultAsync();
                    Mapper.Map(mainFeatures, obj);

                    var pmy = Common.HexToColor(obj.PrimaryAppColor);
                    var sec = Common.HexToColor(obj.SecondaryAppColor);
                    obj.PrimaryAppColorRGB = "RGB(" + pmy.R + "," + pmy.G + "," + pmy.B + ")";
                    obj.SecondaryAppColorRGB = "RGB(" + sec.R + "," + sec.G + "," + sec.B + ")";
                    obj.ShowWalkthrough = HelperClass.ShowWalkthrough;

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", obj);
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get customer main features

        #region Get default data for the application once the user logs in

        public async Task<APiResponse> GetDefaultOrderingInfo(long customerID, long userID, string type = "")
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    var customer = db.CustomerMasters.Where(c => c.CustomerID == customerID).FirstOrDefault();
                    var user = db.UserMasters.Find(userID);

                    if (customer == null)
                    {
                        return new APiResponse
                        {
                            ResponseCode = SaaviErrorCodes.NotFound,
                            Message = "Customer does not exist."
                        };
                    }

                    List<string> availableOrderDate = new List<string>();
                    List<long> availableOrderDateTicks = new List<long>();
                    List<string> availableOrderDatesLong = new List<string>();

                    //Introduction Popup details
                    var popUp = await (from n in db.NoticeMasters
                                       select new NoticeBO
                                       {
                                           Title = n.Title,
                                           Description = n.Description,
                                           NotificationsEnabled = n.IsActive ?? false,
                                           //n.OrderCutOff,
                                           Company = HelperClass.Company,
                                           AllowPickup = n.AllowPickup,
                                           CutoffType = n.CutoffType,
                                           GroupID = n.GroupID,
                                           OrderCutOff = n.OrderCutOff
                                       }).FirstOrDefaultAsync();

                    bool isSundayDelivery = false;

                    bool custSunday = customer.IsSundayOrdering ?? false;

                    if (userID != null && userID > 0)
                    {
                        var customerFeatures = db.CustomerFeaturesMasters.Where(u => u.UserID == userID).FirstOrDefault();

                        if (customerFeatures != null)
                        {
                            bool cfSunday = customerFeatures.IsSundayOrdering ?? false;

                            var permittedDaysSunday = db.PermittedDaysMasters.Where(d => d.DayName == "Sunday" && d.IsActive == true && d.PermitBy == popUp.CutoffType).FirstOrDefault();

                            if (permittedDaysSunday != null)
                            {
                                isSundayDelivery = true;

                                if (!cfSunday)
                                {
                                    isSundayDelivery = false;
                                }
                                if (cfSunday == false && custSunday == true)
                                {
                                    isSundayDelivery = false;
                                }
                                if (!custSunday)
                                {
                                    isSundayDelivery = false;
                                }
                            }
                            else
                            {
                                isSundayDelivery = false;
                            }
                        }
                    }

                    string daysAllowed = customer.StandardDeliveryDays;

                    DateTime currentDate = DateTime.Now;
                    DateTime cutoff = Convert.ToDateTime(popUp.OrderCutOff);

                    //Get required date according to the cutoff time set in the db.

                    bool valueExists = false;
                    int groupID = 0;
                    if (popUp.GroupID > 0)
                    {
                        //var groupValue = db.CutoffGroupDetailMasters.Where(g => g.GroupID == popUp.GroupID).FirstOrDefault();
                        if (popUp.CutoffType == "S")
                        {
                            //var groups = db.CutoffGroupDetailMasters.Where(g => g.ListValue.Contains(customer.Suburb) && g.GroupFilter == "Suburb").FirstOrDefault();
                            var groups = (from a in db.CutoffGroupMasters
                                          join b in db.CutoffGroupDetailMasters on a.GroupID equals b.GroupID
                                          where a.GroupType == "Suburb" && b.ListValue.Contains(customer.Suburb)
                                          select b).FirstOrDefault();

                            if (groups != null)
                            {
                                valueExists = true;
                                groupID = groups.GroupID;
                            }
                        }
                        else if (popUp.CutoffType == "P")
                        {
                            var groups = (from a in db.CutoffGroupMasters
                                          join b in db.CutoffGroupDetailMasters on a.GroupID equals b.GroupID
                                          where a.GroupType == "Postcode" && b.ListValue.Contains(customer.PostCode)
                                          select b).FirstOrDefault();
                            // var groups = db.CutoffGroupDetailMasters.Where(g => g.ListValue.Contains(customer.PostCode) && g.GroupFilter == "Postcode").FirstOrDefault();

                            if (groups != null)
                            {
                                valueExists = true;
                                groupID = groups.GroupID;
                            }
                            // valueExists = groupValue.ListValue.Contains(customer.PostCode ?? "");
                        }
                        else if (popUp.CutoffType == "D")
                        {
                            var groups = (from a in db.CutoffGroupMasters
                                          join b in db.CutoffGroupDetailMasters on a.GroupID equals b.GroupID
                                          where a.GroupType == "Delivery" && b.ListValue.Contains(customer.Suburb)
                                          select b).FirstOrDefault();
                            // var groups = db.CutoffGroupDetailMasters.Where(g => g.ListValue.Contains(customer.RunNo) && g.GroupFilter == "Delivery").FirstOrDefault();

                            if (groups != null)
                            {
                                valueExists = true;
                                groupID = groups.GroupID;
                            }
                            //valueExists = groupValue.ListValue.Contains(customer.RunNo ?? "");
                        }
                        else if (popUp.CutoffType == "U")
                        {
                            var groups = (from a in db.OrderCutoffMasters
                                          join d in db.CutoffPermittedDaysMasters on a.GroupID equals d.GroupID
                                          where a.Type == "Pickup"
                                          select a).FirstOrDefault();
                            // var groups = db.CutoffGroupDetailMasters.Where(g => g.ListValue.Contains(customer.RunNo) && g.GroupFilter == "Delivery").FirstOrDefault();

                            if (groups != null)
                            {
                                valueExists = true;
                                groupID = groups.GroupID.To<int>();
                            }
                            //valueExists = groupValue.ListValue.Contains(customer.RunNo ?? "");
                        }
                    }
                    var oCutoffMaster = db.OrderCutoffMasters.Find(groupID);
                    if (oCutoffMaster != null)
                    {
                        // cutoff = Convert.ToDateTime(oCutoffMaster.CutoffDate);
                        popUp.OrderCutOffDate = oCutoffMaster.CutoffDate;
                        popUp.OrderCutOff = oCutoffMaster.CutoffTime;
                    }

                    var days = new List<PermittedDaysMaster>();

                    if (valueExists && groupID > 0)
                    {
                        var cDays = new string[] { };
                        var cutoffDays = db.CutoffPermittedDaysMasters.Where(c => c.GroupID == groupID).FirstOrDefault();
                        if (cutoffDays != null)
                        {
                            cDays = cutoffDays.Days.Split(',');

                            days = db.PermittedDaysMasters.Where(d => cDays.Contains(d.ShortName)).ToList();
                        }
                        else
                        {
                            cutoffDays = db.CutoffPermittedDaysMasters.Where(c => c.GroupID == 0).FirstOrDefault();
                            cDays = cutoffDays.Days.Split(',');
                            days = db.PermittedDaysMasters.Where(d => cDays.Contains(d.ShortName)).ToList();
                        }
                        //days = db.PermittedDaysMasters.Where(d => daysAllowed.Contains(d.Code) && d.IsActive == true && d.PermitBy == popUp.CutoffType).ToList();
                    }
                    else
                    {
                        var cDays = new string[] { };
                        var cutoffDays = db.CutoffPermittedDaysMasters.Where(c => c.GroupID == groupID).FirstOrDefault();

                        cDays = cutoffDays.Days.Split(',');

                        days = db.PermittedDaysMasters.Where(d => cDays.Contains(d.ShortName)).ToList();
                        //days = db.PermittedDaysMasters.Where(d => daysAllowed.Contains(d.Code) && d.IsActive == true).ToList();
                    }

                    if (days == null || days.Count == 0)
                    {
                        if (valueExists)
                        {
                            days = db.PermittedDaysMasters.Where(d => d.IsActive == true && d.PermitBy == popUp.CutoffType && d.GroupID == popUp.GroupID).ToList();

                            if (days == null)
                            {
                                days = db.PermittedDaysMasters.ToList();
                            }
                        }
                        else
                        {
                            days = db.PermittedDaysMasters.ToList();
                        }
                    }

                    DateTime requiredDate = GetRequiredDate(HelperClass.GetCurrentTime().ToShortDateString(), groupID);
                    var slots = new List<SlotsBO>();

                    if (days.Count > 0)
                    {
                        //get next two delivery dates according to Allowed Order dates
                        for (int i = 0; i < 30; i++)
                        {
                            DateTime requiredDate1 = requiredDate.AddDays(i);
                            if (days.Count(d => (d.DayValue - 1) == (int)requiredDate1.DayOfWeek) > 0)
                            {
                                if (!isSundayDelivery && requiredDate1.DayOfWeek.ToString() == "Sunday")
                                {
                                    continue;
                                }

                                string orderDate = requiredDate1.ToString("ddd, dd MMM");
                                availableOrderDateTicks.Add(Common.GetTicks(requiredDate1));
                                availableOrderDate.Add(orderDate);
                                availableOrderDatesLong.Add(requiredDate1.ToShortDateString());
                            }
                        }

                        foreach (var date in availableOrderDatesLong)
                        {
                            int slotCnt = 500;
                            DateTime aDate = date.To<DateTime>();
                            var orderCount = db.trnsCartMasters.Count(x => DbFunctions.TruncateTime(x.OrderDate) == DbFunctions.TruncateTime(aDate));
                            var slts = db.OrderLimitMasters.Where(x => x.GroupID == groupID).FirstOrDefault();
                            if (slts != null)
                            {
                                slotCnt = slts.OrderLimit.To<int>();
                            }

                            int available = slotCnt;
                            if (orderCount > 0)
                            {
                                available = slotCnt - orderCount;
                            }

                            slots.Add(new SlotsBO { DeliveryDate = aDate.ToString("ddd, dd MMM"), Slots = available });
                        }
                    }
                    else
                    {
                        string orderDate = requiredDate.ToString("ddd, dd MMM");
                        availableOrderDateTicks.Add(Common.GetTicks(requiredDate));
                        availableOrderDateTicks.Add(Common.GetTicks(requiredDate.AddDays(1)));

                        availableOrderDate.Add(orderDate);
                        availableOrderDate.Add(requiredDate.AddDays(1).ToString("ddd, dd MMM"));

                        availableOrderDatesLong.Add(requiredDate.ToShortDateString());
                        availableOrderDatesLong.Add(requiredDate.AddDays(1).ToShortDateString());

                        slots.Add(new SlotsBO() { DeliveryDate = requiredDate.ToString("ddd, dd MMM"), Slots = 500 });
                        slots.Add(new SlotsBO() { DeliveryDate = requiredDate.AddDays(1).ToString("ddd, dd MMM"), Slots = 500 });
                    }

                    var pDays = new List<string>();
                    pDays = days.Select(d => d.DayName).ToList();

                    if (!isSundayDelivery)
                    {
                        int idx = pDays.IndexOf("Sunday");
                        if (idx > 0)
                        {
                            pDays.RemoveAt(idx);
                        }
                    }

                    if (type != "")
                    {
                        return new APiResponse
                        {
                            ResponseCode = SaaviErrorCodes.Success,
                            Message = "Success",
                            Result = new
                            {
                                orderDates = availableOrderDate,
                                orderDate2 = availableOrderDateTicks,
                                permittedDays = pDays,
                                sundayOrdering = isSundayDelivery,
                                customerPhone = customer.Phone1,
                                orderDatesLong = availableOrderDatesLong,
                                LimitOrdersTo1Month = popUp.LimitOrdersTo1Month,
                                AllowPickup = popUp.AllowPickup,
                                SlotsByDate = true,
                                Slots = slots
                            }
                        };
                    }
                    else
                    {
                        return new APiResponse
                        {
                            ResponseCode = SaaviErrorCodes.Success,
                            Message = "Success",
                            Result = new
                            {
                                intro = popUp,
                                orderDates = availableOrderDate,
                                orderDate2 = availableOrderDateTicks,
                                permittedDays = pDays,
                                sundayOrdering = isSundayDelivery,
                                customerPhone = customer.Phone1,
                                orderDatesLong = availableOrderDatesLong,
                                LimitOrdersTo1Month = popUp.LimitOrdersTo1Month,
                                AllowPickup = popUp.AllowPickup,
                                SlotsByDate = true,
                                Slots = slots
                            }
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get default data for the application once the user logs in

        #region Get required dates for Default delivery dates

        private DateTime GetRequiredDate(string OrderDate, int groupID = 0)
        {
            using (var db = new Saavi5Entities())
            {
                NoticeMaster notice = db.NoticeMasters.FirstOrDefault();

                byte? hours = 48;
                string groupCutOffTime = "";
                if (groupID > 0)
                {
                    //days = db.CutoffGroupMasters.Find(groupID)?.Day.To<byte>() ?? 36;
                    groupCutOffTime = db.OrderCutoffMasters.Where(c => c.GroupID == groupID).FirstOrDefault()?.CutoffTime ?? notice.OrderCutOff;
                }

                DateTime requiredDate = DateTime.Today.AddHours(hours.To<double>());
                //if (DateTime.Now.DayOfWeek.ToString() != "Sunday")
                //{
                //    requiredDate = DateTime.Now.AddHours(hours.To<double>());
                //}

                if (notice != null)
                {
                    string CutOffTime = groupCutOffTime == "" ? notice.OrderCutOff : groupCutOffTime;
                    DateTime ordate;
                    bool isorderdateselected = false;
                    if (DateTime.TryParse(OrderDate, out ordate))
                    {
                        if (ordate.Date > DateTime.Now.Date)
                        {
                            requiredDate = ordate;
                            isorderdateselected = true;
                        }
                    }
                    DateTime t1 = Convert.ToDateTime(DateTime.Now);
                    DateTime t2 = Convert.ToDateTime(CutOffTime);

                    if (t1.TimeOfDay.Ticks < t2.TimeOfDay.Ticks && !isorderdateselected)
                    {
                        requiredDate = DateTime.Now.AddHours(hours.To<double>());
                    }
                    else
                    {
                        var permittedDays = db.PermittedDaysMasters.ToList();
                        var pDays = db.CutoffPermittedDaysMasters.Where(x => x.GroupID == groupID || x.GroupID == 0).FirstOrDefault();

                        if (pDays != null)
                        {
                            foreach (var item in permittedDays)
                            {
                                if (pDays.Days.Contains(item.ShortName))
                                {
                                    item.IsActive = true;
                                }
                                else
                                {
                                    item.IsActive = false;
                                }
                                db.SaveChanges();
                            }
                        }

                        int dct = 0;
                        bool isDayAvailable = true;
                    getrequired:
                        foreach (PermittedDaysMaster dayData in db.PermittedDaysMasters.Where(s => s.IsActive == false)) //&& s.PermitBy == notice.CutoffType && s.GroupID == notice.GroupID
                        {
                            if (dayData.DayName.ToLower() == requiredDate.DayOfWeek.ToString().ToLower())
                            {
                                requiredDate = requiredDate.AddDays(1);
                                dct++;
                                if (dct <= 7)
                                {
                                    goto getrequired;
                                }
                                else
                                {
                                    isDayAvailable = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                return requiredDate;
            }
        }

        #endregion Get required dates for Default delivery dates

        #region Common return method

        public APiResponse ReturnMethod(string responseCode, string message, object result = null)
        {
            return new APiResponse
            {
                ResponseCode = responseCode,
                Message = message,
                Result = result
            };
        }

        #endregion Common return method

        #region Get Customer delivery Addresses

        public async Task<APiResponse> GetCustomerDeliveryAddresss(long customerID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var addresses = await (from addr in _db.CustomerDeliveryAddresses
                                           join cus in _db.CustomerMasters on addr.CustomerID equals cus.CustomerID
                                           join state in _db.StateMasters on addr.StateID equals state.StateID into tempState
                                           from state in tempState.DefaultIfEmpty()
                                           join count in _db.CountryMasters on addr.CustomerID equals count.CountryID into tempC
                                           from count in tempC.DefaultIfEmpty()
                                           where addr.CustomerID == customerID && addr.IsActive == true
                                           orderby addr.CreatedDate descending
                                           select new CustomerAddressBO
                                           {
                                               CustomerId = cus.CustomerID,
                                               AddressId = addr.AddressID,
                                               Address1 = addr.Address1 == null ? "" : addr.Address1 + ", " + (addr.Address2 == null ? "" : addr.Address2 + ", ") + (addr.Suburb == null ? "" : addr.Suburb + ", ") + (state.StateName == null ? "" : state.StateName + ", ") + (addr.ZIP == null ? "" : addr.ZIP),
                                               Address2 = addr.Address2 ?? "",
                                               Address3 = addr.Address3 ?? "",
                                               AddressCode = addr.AddressCode ?? "",
                                               PostCode = addr.ZIP ?? "",
                                               Phone1 = addr.Phone1 ?? "",
                                               Phone2 = addr.Phone2 ?? "",
                                               Phone3 = addr.Phone3 ?? "",
                                               StateName = state.StateName ?? "",
                                               StateId = state.StateID == null ? 0 : state.StateID,
                                               CountryName = count.CountryName == null ? "" : count.CountryName,
                                               CountryId = count.CountryID == null ? 0 : count.CountryID,
                                               Suburb = addr.Suburb ?? "",
                                               ContactName = addr.ContactName ?? ""
                                           }).OrderByDescending(o => o.AddressId).ToListAsync();

                    if (addresses.Count() > 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", addresses);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Addreses found.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message + " Stack Trace: " + ex.InnerException.Message);
            }
        }

        #endregion Get Customer delivery Addresses

        #region Clear user Token / Logout

        public async Task<APiResponse> Logout(LogoutBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var user = _db.UserMasters.Find(model.UserID);

                    if (user != null)
                    {
                        user.DeviceToken = "";
                        user.ModifiedOn = HelperClass.GetCurrentTime();

                        await _db.SaveChangesAsync();
                    }
                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Clear user Token / Logout

        #region Log Errors to server for order posting

        private void LogErrorsToServer(string ex, string cusOrderID, string type)
        {
            using (var _db = new Saavi5Entities())
            {
                trnsCartResponseMaster cr = new trnsCartResponseMaster();
                cr.CartId = 0;
                cr.MethodName = type;
                cr.Response = ex;
                cr.CreatedDate = DateTime.Now;

                _db.trnsCartResponseMasters.Add(cr);
                _db.SaveChanges();
            }
        }

        #endregion Log Errors to server for order posting

        #region Get page help text

        public async Task<object> GetPageHelp(string page, bool isList)
        {
            using (var _db = new Saavi5Entities())
            {
                if (isList)
                {
                    var pageData = _db.PageHelpMasters.Where(p => p.PageName == page).ToList();
                    return pageData;
                }
                else
                {
                    var pageData = _db.PageHelpMasters.Where(p => p.PageName == page && p.SortOrder == 1).SingleOrDefault();
                    return pageData;
                }
            }
        }

        #endregion Get page help text

        #region Get all Pickup locations

        public async Task<APiResponse> GetPickupLocations()
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var pickUps = await _db.DeliveryPickupMasters.Where(x => x.IsActive == true).ToListAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        Pickups = pickUps
                    });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Success, ex.Message);
            }
        }

        #endregion Get all Pickup locations

        #region Add delivery address address

        public async Task<APiResponse> AddDeliveryAddress(AddCustomerAddressBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    string retMessage = "";
                    model.Suburb = model.Suburb.Trim();
                    var run = _db.CutoffGroupDetailMasters.Where(x => x.GroupFilter == "Suburb" && x.ListValue.Contains(model.Suburb)).FirstOrDefault();
                    bool subExists = true;
                    if (run == null)
                    {
                        retMessage = "Important Note: Unfortunately, we do not deliver to your suburb at the moment. However, when you place your order, you can nominate from a range of pick-up locations close to your suburb. We are planning to increase our delivery coverage over time. Thanks!";
                        subExists = false;
                    }

                    if (subExists)
                    {
                        var cust = new CustomerDeliveryAddress();

                        cust.ContactName = model.ContactName;
                        cust.Suburb = model.Suburb;
                        cust.Address1 = model.StreetAddress;
                        cust.ZIP = model.PostCode;
                        cust.Phone1 = model.Phone;
                        cust.CustomerID = model.CustomerID;
                        cust.IsActive = true;

                        _db.CustomerDeliveryAddresses.Add(cust);
                        await _db.SaveChangesAsync();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                        {
                            AddressID = cust.AddressID
                        });
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NA, retMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add delivery address address

        #region Delete delivery address address

        public async Task<APiResponse> DeleteDeliveryAddress(DelAddress model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var cust = await _db.CustomerDeliveryAddresses.FindAsync(model.AddressID);

                    _db.CustomerDeliveryAddresses.Remove(cust);
                    _db.SaveChanges();
                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Delete delivery address address

        #region Get delivery days for pickups

        public async Task<APiResponse> GetPickupDeliveryDates(Pickups model)
        {
            using (var _db = new Saavi5Entities())
            {
                bool isDelivery = model.IsDelivery ?? false;
                if (isDelivery)
                {
                    return GetDatesBasedOnAddressID(model.AddressID);
                }
                else
                {
                    var notice = await _db.NoticeMasters.FirstOrDefaultAsync();
                    var pickup = _db.DeliveryPickupMasters.Find(model.PickupID);

                    var days = new List<PermittedDaysMaster>();
                    var cDays = new string[] { };
                    var cutoffDays = _db.CutoffPermittedDaysMasters.Where(c => c.GroupID == pickup.ID).FirstOrDefault();

                    if (cutoffDays != null)
                    {
                        cDays = cutoffDays.Days.Split(',');

                        days = _db.PermittedDaysMasters.Where(d => cDays.Contains(d.ShortName)).ToList();
                    }

                    DateTime currentDate = DateTime.Now;
                    //DateTime cutoff = Convert.ToDateTime(popUp.OrderCutOff);
                    List<string> availableOrderDate = new List<string>();
                    List<long> availableOrderDateTicks = new List<long>();
                    List<string> availableOrderDatesLong = new List<string>();

                    DateTime requiredDate = GetRequiredPickupDate(HelperClass.GetCurrentTime().ToShortDateString(), pickup.ID);
                    var slots = new List<SlotsBO>();

                    if (days.Count > 0)
                    {
                        //get next two delivery dates according to Allowed Order dates
                        for (int i = 0; i < 30; i++)
                        {
                            DateTime requiredDate1 = requiredDate.AddDays(i);
                            if (days.Count(d => (d.DayValue - 1) == (int)requiredDate1.DayOfWeek) > 0)
                            {
                                string orderDate = requiredDate1.ToString("ddd, dd MMM");
                                availableOrderDateTicks.Add(Common.GetTicks(requiredDate1));
                                availableOrderDate.Add(orderDate);
                                availableOrderDatesLong.Add(requiredDate1.ToShortDateString());
                            }
                        }

                        foreach (var date in availableOrderDatesLong)
                        {
                            DateTime aDate = date.To<DateTime>();
                            var orderCount = _db.trnsCartMasters.Count(x => DbFunctions.TruncateTime(x.OrderDate) == DbFunctions.TruncateTime(aDate));
                            int slotCnt = _db.OrderLimitMasters.Where(x => x.GroupID == pickup.ID).FirstOrDefault()?.OrderLimit.To<int>() ?? 500;
                            int available = slotCnt;
                            if (orderCount > 0)
                            {
                                available = slotCnt - orderCount;
                            }

                            slots.Add(new SlotsBO { DeliveryDate = aDate.ToString("ddd, dd MMM"), Slots = available });
                        }
                    }
                    else
                    {
                        string orderDate = requiredDate.ToString("ddd, dd MMM");
                        availableOrderDateTicks.Add(Common.GetTicks(requiredDate));
                        availableOrderDateTicks.Add(Common.GetTicks(requiredDate.AddDays(1)));

                        availableOrderDate.Add(orderDate);
                        availableOrderDate.Add(requiredDate.AddDays(1).ToString("ddd, dd MMM"));

                        availableOrderDatesLong.Add(requiredDate.ToShortDateString());
                        availableOrderDatesLong.Add(requiredDate.AddDays(1).ToShortDateString());

                        slots.Add(new SlotsBO() { DeliveryDate = requiredDate.ToString("ddd, dd MMM"), Slots = 500 });
                        slots.Add(new SlotsBO() { DeliveryDate = requiredDate.AddDays(1).ToString("ddd, dd MMM"), Slots = 500 });
                    }
                    var pDays = new List<string>();
                    pDays = days.Select(d => d.DayName).ToList();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        orderDates = availableOrderDate,
                        orderDate2 = availableOrderDateTicks,
                        permittedDays = pDays,
                        sundayOrdering = false,
                        orderDatesLong = availableOrderDatesLong,
                        LimitOrdersTo1Month = notice.LimitOrdersTo1Month,
                        AllowPickup = notice.AllowPickup ?? false,
                        SlotsByDate = true,
                        Slots = slots
                    });
                }
            }
        }

        public APiResponse GetDatesBasedOnAddressID(long addressID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var addr = _db.CustomerDeliveryAddresses.Find(addressID);
                    string run = _db.DeliveryRuns.Where(d => d.Suburb == addr.Suburb).FirstOrDefault()?.DeliveryRun1 ?? "";
                    bool valueExists = false;
                    int groupID = 0;
                    List<string> availableOrderDate = new List<string>();
                    List<long> availableOrderDateTicks = new List<long>();
                    List<string> availableOrderDatesLong = new List<string>();

                    var groupsMain = (from a in _db.CutoffGroupMasters
                                      join b in _db.CutoffGroupDetailMasters on a.GroupID equals b.GroupID
                                      where a.GroupType == "Delivery"
                                      select b).ToList();

                    foreach (var cutOffList in groupsMain)
                    {
                        cutOffList.ListValue = cutOffList.ListValue.Replace(", ", ",").Replace(" ,", ",");
                        var masterSet = new HashSet<string>(cutOffList.ListValue.ToLower().TrimStart().Split(','));
                        var resultsList = masterSet.Contains(addr.Suburb.ToLower().Trim());
                        if (resultsList)
                        {
                            groupID = cutOffList.GroupID;
                            valueExists = true;
                            break;
                        }
                    }

                    //var groups = (from a in _db.CutoffGroupMasters
                    //              join b in _db.CutoffGroupDetailMasters on a.GroupID equals b.GroupID
                    //              where a.GroupType == "Delivery" && b.ListValue.Contains(addr.Suburb)
                    //              select b).FirstOrDefault();
                    //// var groups = db.CutoffGroupDetailMasters.Where(g => g.ListValue.Contains(customer.RunNo) && g.GroupFilter == "Delivery").FirstOrDefault();

                    //if (groups != null)
                    //{
                    //    valueExists = true;
                    //    groupID = groups.GroupID;
                    //}

                    var days = new List<PermittedDaysMaster>();

                    if (valueExists && groupID > 0)
                    {
                        var cDays = new string[] { };
                        var cutoffDays = _db.CutoffPermittedDaysMasters.Where(c => c.GroupID == groupID).FirstOrDefault();
                        if (cutoffDays != null)
                        {
                            cDays = cutoffDays.Days.Split(',');

                            days = _db.PermittedDaysMasters.Where(d => cDays.Contains(d.ShortName)).ToList();
                        }
                        else
                        {
                            cutoffDays = _db.CutoffPermittedDaysMasters.Where(c => c.GroupID == 0).FirstOrDefault();
                            cDays = cutoffDays.Days.Split(',');
                            days = _db.PermittedDaysMasters.Where(d => cDays.Contains(d.ShortName)).ToList();
                        }
                        //days = db.PermittedDaysMasters.Where(d => daysAllowed.Contains(d.Code) && d.IsActive == true && d.PermitBy == popUp.CutoffType).ToList();
                    }
                    else
                    {
                        var cDays = new string[] { };
                        var cutoffDays = _db.CutoffPermittedDaysMasters.Where(c => c.GroupID == groupID).FirstOrDefault();

                        cDays = cutoffDays.Days.Split(',');

                        days = _db.PermittedDaysMasters.Where(d => cDays.Contains(d.ShortName)).ToList();
                        //days = db.PermittedDaysMasters.Where(d => daysAllowed.Contains(d.Code) && d.IsActive == true).ToList();
                    }

                    if (days == null || days.Count == 0)
                    {
                        var cDayss = new string[] { };
                        var cutoffDays2 = _db.CutoffPermittedDaysMasters.Where(d => d.GroupID == 0).FirstOrDefault();
                        cDayss = cutoffDays2.Days.Split(',');

                        days = _db.PermittedDaysMasters.Where(d => cDayss.Contains(d.ShortName)).ToList();
                    }

                    DateTime requiredDate = GetRequiredDate(HelperClass.GetCurrentTime().ToShortDateString(), groupID);

                    var slots = new List<SlotsBO>();
                    bool isSundayDelivery = false;

                    if (days.Count > 0)
                    {
                        //get next two delivery dates according to Allowed Order dates
                        for (int i = 0; i < 30; i++)
                        {
                            DateTime requiredDate1 = requiredDate.AddDays(i);
                            if (days.Count(d => (d.DayValue - 1) == (int)requiredDate1.DayOfWeek) > 0)
                            {
                                if (!isSundayDelivery && requiredDate1.DayOfWeek.ToString() == "Sunday")
                                {
                                    continue;
                                }

                                string orderDate = requiredDate1.ToString("ddd, dd MMM");
                                availableOrderDateTicks.Add(Common.GetTicks(requiredDate1));
                                availableOrderDate.Add(orderDate);
                                availableOrderDatesLong.Add(requiredDate1.ToShortDateString());
                            }
                        }

                        foreach (var date in availableOrderDatesLong)
                        {
                            int slotCnt = 500;
                            DateTime aDate = date.To<DateTime>();
                            var orderCount = _db.trnsCartMasters.Count(x => DbFunctions.TruncateTime(x.OrderDate) == DbFunctions.TruncateTime(aDate));
                            var slts = _db.OrderLimitMasters.Where(x => x.GroupID == groupID).FirstOrDefault();
                            if (slts != null)
                            {
                                slotCnt = slts.OrderLimit.To<int>();
                            }

                            int available = slotCnt;
                            if (orderCount > 0)
                            {
                                available = slotCnt - orderCount;
                            }

                            slots.Add(new SlotsBO { DeliveryDate = aDate.ToString("ddd, dd MMM"), Slots = available });
                        }
                    }
                    else
                    {
                        string orderDate = requiredDate.ToString("ddd, dd MMM");
                        availableOrderDateTicks.Add(Common.GetTicks(requiredDate));
                        availableOrderDateTicks.Add(Common.GetTicks(requiredDate.AddDays(1)));

                        availableOrderDate.Add(orderDate);
                        availableOrderDate.Add(requiredDate.AddDays(1).ToString("ddd, dd MMM"));

                        availableOrderDatesLong.Add(requiredDate.ToShortDateString());
                        availableOrderDatesLong.Add(requiredDate.AddDays(1).ToShortDateString());

                        slots.Add(new SlotsBO() { DeliveryDate = requiredDate.ToString("ddd, dd MMM"), Slots = 500 });
                        slots.Add(new SlotsBO() { DeliveryDate = requiredDate.AddDays(1).ToString("ddd, dd MMM"), Slots = 500 });
                    }

                    var pDays = new List<string>();
                    pDays = days.Select(d => d.DayName).ToList();

                    if (!isSundayDelivery)
                    {
                        int idx = pDays.IndexOf("Sunday");
                        if (idx > 0)
                        {
                            pDays.RemoveAt(idx);
                        }
                    }

                    return new APiResponse
                    {
                        ResponseCode = SaaviErrorCodes.Success,
                        Message = "Success",
                        Result = new
                        {
                            orderDates = availableOrderDate,
                            orderDate2 = availableOrderDateTicks,
                            permittedDays = pDays,
                            sundayOrdering = isSundayDelivery,
                            orderDatesLong = availableOrderDatesLong,
                            SlotsByDate = true,
                            Slots = slots
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion Get delivery days for pickups

        #region Get required dates for pickups

        private DateTime GetRequiredPickupDate(string OrderDate, int groupID = 0)
        {
            using (var db = new Saavi5Entities())
            {
                NoticeMaster notice = db.NoticeMasters.FirstOrDefault();

                byte hours = 48;
                string cutoffFrom = "";
                string cutoffTill = "";
                if (groupID > 0)
                {
                    //days = db.CutoffGroupMasters.Find(groupID).Day.To<byte>();
                    cutoffFrom = db.CutoffPermittedDaysMasters.Where(c => c.GroupID == groupID).FirstOrDefault()?.PermitFrom;
                    cutoffTill = db.CutoffPermittedDaysMasters.Where(c => c.GroupID == groupID).FirstOrDefault()?.PermitTo;
                }

                DateTime requiredDate = DateTime.Now.AddHours(hours);

                // string CutOffTime = groupCutOffTime == "" ? notice.OrderCutOff : groupCutOffTime;
                DateTime ordate;
                bool isorderdateselected = false;
                if (DateTime.TryParse(OrderDate, out ordate))
                {
                    if (ordate.Date > DateTime.Now.Date)
                    {
                        requiredDate = ordate;
                        isorderdateselected = true;
                    }
                }
                DateTime t1 = Convert.ToDateTime(DateTime.Now);
                DateTime t2 = Convert.ToDateTime(cutoffFrom);
                // DateTime t3 = Convert.ToDateTime(cutoffTill);

                if (t1.TimeOfDay.Ticks < t2.TimeOfDay.Ticks && !isorderdateselected)
                {
                    requiredDate = DateTime.Now.AddHours(hours);
                }
                else
                {
                    var permittedDays = db.PermittedDaysMasters.ToList();
                    var pDays = db.CutoffPermittedDaysMasters.Where(x => x.GroupID == groupID || x.GroupID == 0).FirstOrDefault();

                    if (pDays != null)
                    {
                        foreach (var item in permittedDays)
                        {
                            if (pDays.Days.Contains(item.ShortName))
                            {
                                item.IsActive = true;
                            }
                            else
                            {
                                item.IsActive = false;
                            }
                            db.SaveChanges();
                        }
                    }

                    int dct = 0;
                    bool isDayAvailable = true;
                getrequired:
                    foreach (PermittedDaysMaster dayData in db.PermittedDaysMasters.Where(s => s.IsActive == false)) //&& s.PermitBy == notice.CutoffType && s.GroupID == notice.GroupID
                    {
                        if (dayData.DayName.ToLower() == requiredDate.DayOfWeek.ToString().ToLower())
                        {
                            requiredDate = requiredDate.AddDays(1);
                            dct++;
                            if (dct <= 7)
                            {
                                goto getrequired;
                            }
                            else
                            {
                                isDayAvailable = false;
                                break;
                            }
                        }
                    }
                }

                return requiredDate;
            }
        }

        #endregion Get required dates for pickups

        #region MailChimp

        private void AddSubscribeAddress(string email, string Fname, string Lname)
        {
            try
            {
                string apiKey = HelperClass.MailchimpApiKey; //your API KEY created by you.
                string dataCenter = "us11";
                string listID = HelperClass.ListID;  //The ListID of the list you want to use.

                SubscribeClassCreatedByMe subscribeRequest = new SubscribeClassCreatedByMe
                {
                    email_address = email,
                    status = "subscribed"
                };
                subscribeRequest.merge_fields = new MergeFieldClassCreatedByMe();
                subscribeRequest.merge_fields.FNAME = Fname;
                subscribeRequest.merge_fields.LNAME = Lname;

                using (HttpClient client = new HttpClient())
                {
                    var uri = "https://" + dataCenter + ".api.mailchimp.com/";
                    var endpoint = "3.0/lists/" + listID + "/members";

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", apiKey);
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(uri);

                    //NOTE: To avoid the method being 'async' we access to '.Result'
                    HttpResponseMessage response = client.PostAsJsonAsync(endpoint, subscribeRequest).Result;//PostAsJsonAsync method serializes an object to
                                                                                                             //JSON and then sends the JSON payload in a POST request
                                                                                                             //StatusCode == 200
                                                                                                             // -> Status == "subscribed" -> Is on the list now
                                                                                                             // -> Status == "unsubscribed" -> this address used to be on the list, but is not anymore
                                                                                                             // -> Status == "pending" -> This address requested to be added with double-opt-in but hasn't confirmed yet
                                                                                                             // -> Status == "cleaned" -> This address bounced and has been removed from the list

                    //StatusCode == 404
                    if ((response.IsSuccessStatusCode))
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorsToServer("MailChimp:AddSubscribeAddress:: " + ex.Message, "0", "Error");
            }
        }

        #endregion MailChimp
    }
}