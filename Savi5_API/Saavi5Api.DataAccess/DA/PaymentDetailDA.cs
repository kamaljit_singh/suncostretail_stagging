﻿using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using System.IO;
using System.Web.Hosting;
using System.Collections;
using System.Web.ModelBinding;
using SaaviErrorCodes = Saavi5Api.DataAccess.BO.SaaviErrorCodes;
using System.Configuration;
using Stripe.Terminal;
using Stripe;
using Newtonsoft.Json;

namespace Saavi5Api.DataAccess.DA
{
    public class PaymentDetailDA
    {
        public string secretKey { get; set; }

        #region Common return method
        public APiResponse ReturnMethod(string responseCode, string message, object result = null)
        {
            return new APiResponse
            {
                ResponseCode = responseCode,
                Message = message,
                Result = result
            };
        }
        #endregion Common return method

        #region  Get Payment Token
        public async Task<string> GetPaymentToken(PaymentTokenModel model)
        {
            try
            {
                return await System.Threading.Tasks.Task.Run(() =>
                {
                    string Detail = "Temp Cart Request :  Items Order : ";
                    using (var _db = new Saavi5Entities())
                    {

                        var tCartItems = _db.trnsTempCartItemsMasters.Where(t => t.CartID == model.TempCartID).ToList();
                        if (tCartItems != null && tCartItems.Count > 0)
                        {
                            foreach (var item in tCartItems)
                            {
                                Detail = Detail + item.ProductID + " - " + item.Quantity + " - " + item.Price + " - " + (item.IsSpecialPrice == null ? false : true) + " - " + item.CreatedDate + "  Next ";
                            }
                        }
                        else
                        {
                            Detail = "No Items ";

                        }

                    }


                    Detail = Detail + " ( Other Request Detail : " + model.DeviceType + " - " + model.TempCartID + " - " + model.PaymentTotal + " - " + model.OnlyAuthorize + " - " + model.CurrencyCode + " ) ";
                    LogPayment(Detail, (-1 * model.TempCartID), 0, "", "", model.PaymentTotal, model.PaymentTotal); // Negative cart ID for TempCartID in log table.
                    StripeConfiguration.ApiKey = System.Configuration.ConfigurationManager.AppSettings["StripeApiKey"] ?? "";
                    var service = new PaymentIntentService();
                    var netPayable = Convert.ToInt64(model.PaymentTotal * 100);
                    if (model.OnlyAuthorize)
                    {
                        netPayable = netPayable + Convert.ToInt64(model.PaymentTotal * 10);//Capture amount 10% extra in case of authorization only. Stripe do not take decimal point, e.g. payment 15.55 is being sent as 1555, hence for percentage calculation /100 is cancelled.
                    }
                    var portalFeePercentage = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["StripePortalFeePercentageFor50AndBelow"] ?? "");
                    if (portalFeePercentage > 100)
                    {
                        portalFeePercentage = 100;
                    }
                    else if (portalFeePercentage < 0)
                    {
                        portalFeePercentage = 0;
                    }
                    var portalFee = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["StripePortalFeeFlatAbove50"] ?? "");
                    if (portalFee > 50)
                    {
                        portalFee = 50.00;
                    }
                    else if (portalFee < 0)
                    {
                        portalFee = 0.00;
                    }

                    if (model.PaymentTotal <= 50)
                    {
                        portalFee = (model.PaymentTotal * portalFeePercentage / 100);
                    }
                    var portalFeeLong = Convert.ToInt64(portalFee * 100); //Stripe do not take decimal point, e.g. payment 15.55 is being sent as 1555
                    var createOptions = new PaymentIntentCreateOptions
                    {
                        PaymentMethodTypes = new List<string>
                        {
                        "card",
                        },
                        Amount = netPayable,
                        Currency = model.CurrencyCode,
                        ApplicationFeeAmount = portalFeeLong,
                        CaptureMethod = (model.OnlyAuthorize ? "manual" : "automatic"),
                        Metadata = new Dictionary<string, string>
  {
                            {"DeviceType", Convert.ToString(model.DeviceType)},
    {"OrderId", Convert.ToString(model.TempCartID)},
  },


                    };

                    var requestOptions = new RequestOptions();
                    requestOptions.StripeAccount = System.Configuration.ConfigurationManager.AppSettings["StripeConnectedAccountID"] ?? "";
                    var serviceObject = service.Create(createOptions, requestOptions);

                    LogPayment(JsonConvert.SerializeObject(serviceObject), (-1 * model.TempCartID), 0, serviceObject.Id, serviceObject.Status, (serviceObject.Amount / 100), (model.OnlyAuthorize ? 0 : (serviceObject.Amount / 100))); // Negative cart ID for TempCartID in log table.

                    return serviceObject.ClientSecret;
                });
            }
            catch (Exception ex)
            {
                LogPayment(ex.ToString(), (-1 * model.TempCartID)); // Negative cart ID for TempCartID in log table.
                return "Invalid";
            }
        }
        #endregion Get Payment Token
        #region  Subscription Payment Methods

        public async Task<string> CreateSubscription(SubscriptionDetail req)
        {
            try
            {
                return await System.Threading.Tasks.Task.Run(() =>
                {
                    try
                    {
                        var requestOptions = new RequestOptions();
                        requestOptions.StripeAccount = System.Configuration.ConfigurationManager.AppSettings["StripeConnectedAccountID"] ?? "";
                        using (var _db = new Saavi5Entities())
                        {

                            StripeSubcriptionLogMaster("Start of subscription Process : ", req.customerid, "", req.planid);
                            string stripecustomerId = string.Empty;
                            string subscriptionId = string.Empty;

                            var customerdetail = _db.StripeSubcriptionMasters.Where(p => p.CustomerID == req.customerid).FirstOrDefault();
                            try
                            {

                                if (customerdetail == null || (customerdetail != null && string.IsNullOrEmpty(customerdetail.StripeCustomerID)))
                                {
                                    var customerEmaildetail = _db.CustomerMasters.Where(p => p.CustomerID == req.customerid).FirstOrDefault();
                                    var customer = new Stripe.Customer();
                                    var optionsCustomer = new CustomerCreateOptions
                                    {
                                        Email = customerEmaildetail.Email
                                    };
                                    var Customerservice = new CustomerService();
                                    customer = Customerservice.Create(optionsCustomer, requestOptions);
                                    stripecustomerId = customer.Id;
                                }
                                else
                                {
                                    stripecustomerId = customerdetail.StripeCustomerID;
                                }
                                var options1 = new PaymentMethodAttachOptions
                                {
                                    Customer = stripecustomerId,
                                };
                                var servicepaymentMethod = new PaymentMethodService();
                                var paymentMethod = servicepaymentMethod.Attach(req.paymentmethod, options1, requestOptions);

                                // Update customer's default invoice payment method
                                var customerOptions = new CustomerUpdateOptions
                                {
                                    InvoiceSettings = new CustomerInvoiceSettingsOptions
                                    {
                                        DefaultPaymentMethod = paymentMethod.Id,
                                    },
                                };
                                var customerService = new CustomerService();
                                customerService.Update(stripecustomerId, customerOptions, requestOptions);

                            }
                            catch (Exception ex)
                            {
                                StripeSubcriptionLogMaster("Error Occured During Customer Creation and assign payment method " + ex.Message, req.customerid, stripecustomerId, req.planid, "", "", 0);
                                return "Failed to create subscription";
                            }
                            // Create subscription

                            var options = new ProductCreateOptions
                            {
                                Name = "Product",
                            };

                            var service = new ProductService();
                            var product = service.Create(options, requestOptions);
                            var optionsPlan = new PlanCreateOptions
                            {
                                Amount = Convert.ToInt64(req.price),
                                Currency = "aud",
                                Interval = "15day",
                                Product = product.Id
                            };
                            var servicePlan = new PlanService();
                            Plan plan = servicePlan.Create(optionsPlan, requestOptions);
                            var subscriptionOptions = new SubscriptionCreateOptions
                            {
                                Customer = stripecustomerId,
                                Items = new List<SubscriptionItemOptions>
                                 {
                                 new SubscriptionItemOptions
                                         {  Plan =plan.Id },
                                 },
                                ApplicationFeePercent = 10,


                            };
                            subscriptionOptions.AddExpand("latest_invoice.payment_intent");
                            var subscriptionService = new SubscriptionService();
                            try
                            {
                                Subscription subscription = subscriptionService.Create(subscriptionOptions, requestOptions);
                                if (subscription != null && (subscription.Status == "trialing" || subscription.Status == "active"))
                                {
                                    //Log Stripe Subcription response to DB
                                    StripeSubcriptionMaster objStripeSubcriptionMaster = new StripeSubcriptionMaster();
                                    objStripeSubcriptionMaster.PlanId = req.planid;
                                    objStripeSubcriptionMaster.CustomerID = req.customerid;
                                    objStripeSubcriptionMaster.StripeCustomerID = stripecustomerId;
                                    objStripeSubcriptionMaster.StripePriceID = "StripePriceId";
                                    objStripeSubcriptionMaster.Amount = 0;
                                    objStripeSubcriptionMaster.CreatedOn = DateTime.Now;
                                    // objStripeSubcriptionMaster.CreatedBy = req.customerid;
                                    objStripeSubcriptionMaster.StripeStatus = subscription.Status;
                                    objStripeSubcriptionMaster.StripeSubcriptionID = subscription.Id;
                                    objStripeSubcriptionMaster.IsActive = true;
                                    objStripeSubcriptionMaster.IsStripePaymentSuccess = subscription.Status == "trialing" ? false : true;
                                    _db.StripeSubcriptionMasters.Add(objStripeSubcriptionMaster);
                                    _db.SaveChanges();
                                    subscriptionId = subscription.Id;
                                }
                                else
                                {
                                    StripeSubcriptionLogMaster("", req.customerid, stripecustomerId, req.planid, "StripePriceId", subscription.Status, 0);
                                }
                            }
                            catch (StripeException ex)
                            {
                                StripeSubcriptionLogMaster(ex.Message, req.customerid, stripecustomerId, req.planid, "StripePriceId", "", 0);
                                return "Failed to create subscription";
                            }
                            catch (Exception ex)
                            {
                                StripeSubcriptionLogMaster(ex.Message, req.customerid, stripecustomerId, req.planid, "StripePriceId", "", 0);
                                return "Failed to create subscription";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        StripeSubcriptionLogMaster(ex.Message, req.customerid, "", req.planid);
                        return "Invalid";
                    }


                    return "You have Sucessfully Subscribe" + "PlanName" + " With payment of $" + 0 + "/Month" + "," + "subscriptionId";
                });
            }
            catch (Exception ex)
            {
                StripeSubcriptionLogMaster(ex.ToString(), req.customerid);
                return "Invalid";
            }
        }
        public async Task<string> CancelSubscription(CancelRecurringpayment model)
        {
            try
            {
                return await System.Threading.Tasks.Task.Run(() =>
                {
                    try
                    {
                        using (var _db = new Saavi5Entities())
                        {

                            StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["StripeApiKey"] ?? "";
                            var subcriptionDetail = _db.StripeSubcriptionMasters.Where(p => p.CustomerID == model.customerid && p.StripePriceID == model.planid && p.IsActive == true).FirstOrDefault();
                            if (subcriptionDetail != null && subcriptionDetail.StripeSubcriptionID != null)
                            {
                                var cancelOptions = new SubscriptionCancelOptions
                                {
                                    InvoiceNow = false,
                                    Prorate = false,
                                };
                                var service = new SubscriptionService();
                                Subscription subscription = service.Cancel(subcriptionDetail.StripeSubcriptionID, cancelOptions);
                                if (subscription.Status == "canceled")
                                {
                                    subcriptionDetail.IsActive = false;
                                    subcriptionDetail.ModifiedOn = DateTime.Now;
                                    subcriptionDetail.ModifiedBy = Convert.ToString(model.customerid);
                                    _db.SaveChanges();
                                }
                                else
                                {
                                    StripeSubcriptionLogMaster("Error Occured cancel Subscription " + subscription.Status, model.customerid, "", 0, model.planid);
                                    return "0";
                                }
                            }
                        }

                    }
                    catch (StripeException ex)
                    {
                        StripeSubcriptionLogMaster("Error Occured cancel Subscription " + ex.Message, model.customerid, "", 0, model.planid);
                        return "0";
                    }
                    catch (Exception ex)
                    {
                        StripeSubcriptionLogMaster("Error Occured cancel Subscription " + ex.Message, model.customerid, "", 0, model.planid);
                        return "0";
                    }
                    return "1";
                });
            }
            catch (Exception ex)
            {
                StripeSubcriptionLogMaster(ex.ToString(), model.customerid); // Negative cart ID for TempCartID in log table.
                return "Invalid";
            }
        }
        public void StripeSubcriptionLogMaster(string error, long CustomerId = 0, string StripeCustomerId = "", int pID = 0, string spiId = "", string pStatus = "", double? aAmt = 0)
        {
            using (var _db = new Saavi5Entities())
            {
                //Log Stripe Subcription response to DB
                StripeSubcriptionLogMaster pLog = new StripeSubcriptionLogMaster();
                pLog.PlanId = pID;
                pLog.CustomerID = CustomerId;
                pLog.StripeCustomerID = StripeCustomerId;
                pLog.StripePriceID = spiId;
                pLog.StripeResponse = error;
                pLog.Amount = aAmt;
                pLog.CreatedOn = DateTime.Now;
                pLog.StripeStatus = pStatus;
                _db.StripeSubcriptionLogMasters.Add(pLog);
                _db.SaveChanges();
            }
        }
        #endregion Subscription Payment Methods
        #region  Get Payment Token for Past Orders
        public async Task<string> GetPaymentToken(PaymentTokenForPastOrdersModel model)
        {
            try
            {
                return await System.Threading.Tasks.Task.Run(() =>
                {
                    string Detail = "Past Order Payment Request for Order Ids: " + String.Join(",", model.CartIDs);


                    Detail += " ( Other Request Detail : " + model.DeviceType + " - " + model.PaymentTotal + " - " + model.OnlyAuthorize + " - " + model.CurrencyCode + " ) ";
                    LogPayment(Detail, 0, 0, "", "", model.PaymentTotal, model.PaymentTotal); // Negative cart ID for TempCartID in log table.
                    StripeConfiguration.ApiKey = System.Configuration.ConfigurationManager.AppSettings["StripeApiKey"] ?? "";
                    var service = new PaymentIntentService();
                    var netPayable = Convert.ToInt64(model.PaymentTotal * 100);
                    if (model.OnlyAuthorize)
                    {
                        netPayable = netPayable + Convert.ToInt64(model.PaymentTotal * 10);//Capture amount 10% extra in case of authorization only. Stripe do not take decimal point, e.g. payment 15.55 is being sent as 1555, hence for percentage calculation /100 is cancelled.
                    }
                    var portalFeePercentage = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["StripePortalFeePercentageFor50AndBelow"] ?? "");
                    if (portalFeePercentage > 100)
                    {
                        portalFeePercentage = 100;
                    }
                    else if (portalFeePercentage < 0)
                    {
                        portalFeePercentage = 0;
                    }
                    var portalFee = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["StripePortalFeeFlatAbove50"] ?? "");
                    if (portalFee > 50)
                    {
                        portalFee = 50.00;
                    }
                    else if (portalFee < 0)
                    {
                        portalFee = 0.00;
                    }

                    if (model.PaymentTotal <= 50)
                    {
                        portalFee = (model.PaymentTotal * portalFeePercentage / 100);
                    }
                    var portalFeeLong = Convert.ToInt64(portalFee * 100); //Stripe do not take decimal point, e.g. payment 15.55 is being sent as 1555
                    var createOptions = new PaymentIntentCreateOptions
                    {
                        PaymentMethodTypes = new List<string>
                        {
                        "card",
                        },
                        Amount = netPayable,
                        Currency = model.CurrencyCode,
                        ApplicationFeeAmount = portalFeeLong,
                        CaptureMethod = (model.OnlyAuthorize ? "manual" : "automatic"),
                        Metadata = new Dictionary<string, string>
  {
                            {"DeviceType", Convert.ToString(model.DeviceType)},
                            {"OrderId", ""},
                            {"OrderIds", String.Join(",", model.CartIDs)},
  },
                    };

                    var requestOptions = new RequestOptions();
                    requestOptions.StripeAccount = System.Configuration.ConfigurationManager.AppSettings["StripeConnectedAccountID"] ?? "";
                    var serviceObject = service.Create(createOptions, requestOptions);

                    LogPayment(JsonConvert.SerializeObject(serviceObject), 0, 0, serviceObject.Id, serviceObject.Status, (serviceObject.Amount / 100), (model.OnlyAuthorize ? 0 : (serviceObject.Amount / 100))); // Negative cart ID for TempCartID in log table.

                    return serviceObject.ClientSecret;
                });
            }
            catch (Exception ex)
            {
                LogPayment(ex.ToString(), 0); // Negative cart ID for TempCartID in log table.
                return "Invalid";
            }
        }
        #endregion Get Payment Token for Past Orders

        #region  Get Stripe Public Keys
        public async Task<APiResponse> GetStripePublicKeys()
        {
            return ReturnMethod(SaaviErrorCodes.Success, "Success", new
            {
                PublicKey = ConfigurationManager.AppSettings["StripePublicKey"] ?? "",
                ConnectedAccountID = ConfigurationManager.AppSettings["StripeConnectedAccountID"] ?? ""
            });
        }
        #endregion Get Stripe Public Keys

        public void LogPayment(string error, long cartId, long pID = 0, string piId = "", string pStatus = "", double? aAmt = 0, double? cAmt = 0)
        {
            using (var _db = new Saavi5Entities())
            {
                //Log Payment response to DB
                PaymentLogMaster pLog = new PaymentLogMaster();
                pLog.PaymentID = pID;
                pLog.CartID = cartId;
                pLog.AuthorizeAmount = aAmt;
                pLog.CaptureAmount = cAmt;
                pLog.CreatedOn = DateTime.Now;
                pLog.StripeChargeID = piId;
                pLog.StripeStatus = pStatus;
                pLog.StripeTokenID = "";
                pLog.StripeResponse = error;
                _db.PaymentLogMasters.Add(pLog);
                _db.SaveChanges();
            }
        }
    }
}