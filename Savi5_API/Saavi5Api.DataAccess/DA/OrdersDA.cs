﻿using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using CommonFunctions;
using System.Web;
using System.IO;
using System.Configuration;
using Invoicer;
using Invoicer.Models;

using Invoicer.Services;
using System.Web.Hosting;
using System.Collections;
using Microsoft.Office.Interop.Excel;
using BarcodeLib;
using System.Data;
using System.Net;
using System.Data.Entity.Validation;
using Newtonsoft.Json;
using Stripe;
using Address = Invoicer.Models.Address;
using File = System.IO.File;

namespace Saavi5Api.DataAccess.DA
{
    public class OrdersDA
    {
        #region Common return method

        public APiResponse ReturnMethod(string responseCode, string message, object result = null)
        {
            return new APiResponse
            {
                ResponseCode = responseCode,
                Message = message,
                Result = result
            };
        }

        #endregion Common return method

        #region Get all the comments for a customer saved during saving an Order

        public async Task<APiResponse> GetComments(GetComments model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var comments = (IEnumerable<object>)null;
                    if (!model.IsProduct)
                    {
                        comments = await (from com in _db.DeliveryCommentsMasters
                                          where com.CustomerID == model.CustomerID
                                          select new
                                          {
                                              com.CommentID,
                                              com.CommentDescription
                                          }).OrderByDescending(o => o.CommentID).ToListAsync();
                    }
                    else
                    {
                        comments = await (from com in _db.DeliveryProductCommentsMasters
                                          where com.CustomerID == model.CustomerID && com.ProductID == model.ProductID
                                          select new
                                          {
                                              com.PCommentID,
                                              com.CommentDescription
                                          }).OrderByDescending(o => o.PCommentID).ToListAsync();
                    }

                    if (comments.Count() > 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", comments);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get all the comments for a customer saved during saving an Order

        #region Add customer comments

        public async Task<APiResponse> AddComments(AddCommentBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    long commentID = 0;
                    if (!model.IsProduct)
                    {
                        var comment = _db.DeliveryCommentsMasters.Where(c => c.CustomerID == model.CustomerID && c.CommentDescription.ToLower() == model.CommentDescription).FirstOrDefault();

                        if (comment != null)
                        {
                            return ReturnMethod(SaaviErrorCodes.AlreadyExists, "Comment already exists!");
                        }

                        DeliveryCommentsMaster dcm = new DeliveryCommentsMaster();
                        dcm.CustomerID = model.CustomerID;
                        dcm.CommentDescription = model.CommentDescription;
                        dcm.CompanyID = 1;
                        dcm.IsActive = true;
                        dcm.CreatedDate = DateTime.Now;

                        _db.DeliveryCommentsMasters.Add(dcm);
                        await _db.SaveChangesAsync();

                        commentID = dcm.CommentID;
                    }
                    else
                    {
                        var comment = _db.DeliveryProductCommentsMasters.Where(c => c.CustomerID == model.CustomerID && c.ProductID == model.ProductID && c.CommentDescription.ToLower() == model.CommentDescription).FirstOrDefault();

                        if (comment != null)
                        {
                            return ReturnMethod(SaaviErrorCodes.AlreadyExists, "Comment already exists!");
                        }

                        DeliveryProductCommentsMaster dpc = new DeliveryProductCommentsMaster();
                        dpc.CustomerID = model.CustomerID;
                        dpc.ProductID = model.ProductID;
                        dpc.CompanyID = 1;
                        dpc.CreatedDate = DateTime.Now;
                        dpc.IsActive = true;
                        dpc.CommentDescription = model.CommentDescription;

                        _db.DeliveryProductCommentsMasters.Add(dpc);
                        await _db.SaveChangesAsync();
                        commentID = dpc.PCommentID;
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { CommentID = commentID });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add customer comments

        #region Delete customer comment

        public async Task<APiResponse> DeleteComment(DeleteCommentBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (model.IsProduct)
                    {
                        var comment = _db.DeliveryProductCommentsMasters.Find(model.CommentID);

                        _db.DeliveryProductCommentsMasters.Remove(comment);
                    }
                    else
                    {
                        var comment = _db.DeliveryCommentsMasters.Find(model.CommentID);

                        _db.DeliveryCommentsMasters.Remove(comment);
                    }

                    await _db.SaveChangesAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Comment deleted.");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Delete customer comment

        #region Save order/ update order/ Place Order

        private bool CheckIfPaymentRequired(long userId, long tempCartID)
        {
            try
            {
                bool res = false;
                using (var _db = new Saavi5Entities())
                {
                    var customerFeatures = _db.CustomerFeaturesMasters.Where(i => i.UserID == userId).FirstOrDefault();
                    if (customerFeatures != null && customerFeatures.IsStripePayment == true)
                    {
                        var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();
                        if (mainFeatures != null && mainFeatures.IsStripePayment == true)
                        {
                            res = true;
                        }
                    }
                    if (tempCartID > 0 && res)
                    {
                        var tempCart = _db.trnsTempCartMasters.Find(tempCartID);
                        var tempTotal = _db.trnsTempCartItemsMasters.Where(x => x.CartID == tempCartID).Sum(s => s.Quantity * s.Price);

                        if (tempCart.IsCouponApplied != null && tempCart.IsCouponApplied == true)
                        {
                            if (tempCart.CouponAmount == tempTotal)
                            {
                                res = false;
                            }
                            else
                            {
                                res = true;
                            }
                        }
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<APiResponse> PlaceOrder(PlaceOrderBO model)
        {
            try
            {
                //Place Order functionality
                using (var _db = new Saavi5Entities())
                {
                    PaymentDetailDA _da = new PaymentDetailDA();
                    if (model == null)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "Request model empty.");
                    }

                    PaymentDetailDA _pda = new PaymentDetailDA();
                    _pda.LogPayment(JsonConvert.SerializeObject(model), (-1 * model.TempCartID));

                    if (model.SaveOrder)
                    {
                        using (var db = new Saavi5Entities())
                        {
                            var tCart = db.trnsTempCartMasters.Where(t => t.CustomerID == model.CustomerID && t.RepUserID == model.UserID && t.IsSavedOrder == true).AsNoTracking().ToList();

                            if (tCart.Count == 0)
                            {
                                var orderToSave = db.trnsTempCartMasters.Find(model.TempCartID);
                                orderToSave.IsSavedOrder = true;
                                await db.SaveChangesAsync();

                                return ReturnMethod(SaaviErrorCodes.Success, "Order Saved successfully.");
                            }
                            else
                            {
                                return ReturnMethod(SaaviErrorCodes.AlreadyExists, "You already have a saved Order. Please delete it or append items to that order.");
                            }
                        }
                    }

                    var paymentRequired = CheckIfPaymentRequired(model.UserID, model.TempCartID);
                    if (paymentRequired == true && model.PaymentDetails == null)
                    {
                        return ReturnMethod(SaaviErrorCodes.Error, "Failed to place order because payment is required for this order, if payment form not appeared, please retry by reloading the cart page and try again. If issue persists please contact administration.");
                    }

                    if (model.PaymentDetails != null && !model.SaveOrder)
                    {
                        try
                        {
                            var payStatus = model.PaymentDetails.Status.ToLower();
                            if (payStatus != "succeeded" && payStatus != "requires_capture")
                            {
                                return ReturnMethod(SaaviErrorCodes.Error, "Payment failed, please retry.");
                            }
                        }
                        catch (StripeException ex)
                        {
                            _pda.LogPayment("During Payment:" + ex.Message, (-1 * model.TempCartID));
                        }
                        catch (Exception ex)
                        {
                            _pda.LogPayment("During Payment:" + ex.Message, (-1 * model.TempCartID));
                        }
                    }

                    // Insert Main cart details into CartMaster table
                    trnsCartMaster cart = new trnsCartMaster();
                    Mapper.Map(model, cart);

                    if (model.OrderDate == null)
                    {
                        model.OrderDate = DateTime.Now;
                    }
                    else
                    {
                        if (Convert.ToDateTime(model.OrderDate).Year == 1)
                        {
                            model.OrderDate = DateTime.Now;
                        }
                        else
                        {
                            model.OrderDate = Convert.ToDateTime(model.OrderDate);
                        }
                    }

                    cart.OrderDate = model.OrderDate;
                    cart.CreatedDate = DateTime.Now;
                    cart.ModifiedDate = DateTime.Now;

                    if (model.HasFreightCharges != null && model.HasFreightCharges == true)
                    {
                        cart.FrieghtCharges = _db.DeliveryCharges.FirstOrDefault()?.DeliveryFee;
                    }

                    if (model.StandingOrderStartDate.HasValue)
                    {
                        cart.IsStandingOrder = true;
                    }

                    // make sure there is coupon amount associated with the coupon code
                    if (model.IsCouponApplied && model.CouponAmount == 0)
                    {
                        var promo = _db.PromoCodeMasters.Where(x => x.Code == model.Coupon).FirstOrDefault();

                        if (promo != null)
                        {
                            if (promo.CodeType.ToLower() == "value")
                            {
                                cart.CouponAmount = promo.Value;
                            }
                            else
                            {
                                var sum = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.TempCartID).Sum(s => s.Quantity * s.Price);
                                cart.CouponAmount = sum - ((sum * promo.Value) / 100);
                            }
                        }
                    }

                    _db.trnsCartMasters.Add(cart);

                    _db.SaveChanges();

                    var orderID = cart.CartID;

                    PaymentMaster paymentMasters = new PaymentMaster();

                    try
                    {
                        if (model.PaymentDetails != null)
                        {
                            paymentMasters.CartID = orderID;
                            paymentMasters.AuthorizeAmount = Convert.ToDouble(model.PaymentDetails.Amount) / 100;
                            paymentMasters.CaptureAmount = (model.PaymentDetails.Status == "requires_capture") ? 0 : paymentMasters.AuthorizeAmount;
                            paymentMasters.CreatedBy = model.UserID.ToString();
                            paymentMasters.CreatedOn = DateTime.Now;
                            paymentMasters.CustomerID = model.CustomerID;
                            paymentMasters.IsStripeAuthorisationSuccess = true;
                            paymentMasters.IsStripePaymentSuccess = (model.PaymentDetails.Status == "requires_capture") ? false : true;
                            paymentMasters.IsAuthOnlyTransaction = !paymentMasters.IsStripePaymentSuccess;
                            paymentMasters.StripeChargeID = model.PaymentDetails.Id;
                            paymentMasters.StripeCustomerID = model.PaymentDetails.CustomerId;
                            try
                            {
                                if (cart.DeviceType.ToLower() == "iphone" || cart.DeviceType.ToLower() == "android")
                                {
                                    paymentMasters.StripeTokenID = model.PaymentDetails.ClientSecret ?? "";
                                }
                                else
                                {
                                    try
                                    {
                                        paymentMasters.StripeTokenID = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(model.PaymentDetails.ClientSecret));
                                    }
                                    catch (Exception ex)
                                    {
                                        paymentMasters.StripeTokenID = "";
                                        _pda.LogPayment("Device Type not coming:" + ex.Message, orderID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                paymentMasters.StripeTokenID = "";
                                _pda.LogPayment("Device Type Giving issue:" + ex.Message, orderID);
                            }
                            paymentMasters.UserID = model.UserID;

                            _db.PaymentMasters.Add(paymentMasters);
                            _db.SaveChanges();

                            try
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(orderID)))
                                {
                                    var cartlatest = _db.trnsCartMasters.Where(c => c.CartID == orderID).FirstOrDefault();
                                    cartlatest.IsPaymentDone = (model.PaymentDetails.Status == "requires_capture") ? false : true;
                                    cartlatest.StripeChargeID = model.PaymentDetails.Id;
                                    _db.SaveChanges();
                                }
                                if (model.PaymentDetails.Status != "requires_capture")
                                {
                                    var requestOptions = new RequestOptions();
                                    requestOptions.StripeAccount = System.Configuration.ConfigurationManager.AppSettings["StripeConnectedAccountID"] ?? "";
                                    var options = new PaymentIntentGetOptions();
                                    var service = new PaymentIntentService();
                                    var intent = service.Get(model.PaymentDetails.Id, options, requestOptions);
                                    var user = _db.UserMasters.Where(p => p.UserID == model.UserID && p.CustomerID == model.CustomerID).FirstOrDefault();
                                    if (intent != null)
                                    {
                                        try
                                        {
                                            var updateoptions = new PaymentIntentUpdateOptions();
                                            updateoptions.Description = "Suncoast Fresh – Order - " + orderID;
                                            if (user != null && !string.IsNullOrEmpty(user.Email))
                                            {
                                                updateoptions.Description = "Suncoast Fresh – Order - " + orderID + " - " + user.Email;
                                            }
                                            PaymentIntent obj = service.Update(model.PaymentDetails.Id, updateoptions, requestOptions);
                                        }
                                        catch (StripeException ex)
                                        {
                                            _pda.LogPayment("Updating Charge Description:" + ex.Message, orderID);
                                        }
                                        catch (Exception ex)
                                        {
                                            _pda.LogPayment("Updating Charge Description:" + ex.Message, orderID);
                                        }
                                    }
                                    var balanceservice = new BalanceTransactionService();
                                    if (intent.Charges != null && intent.Charges.Data != null && intent.Charges.Data.Count > 0)
                                    {
                                        try
                                        {
                                            var chargeUpdateoptions = new ChargeUpdateOptions();
                                            chargeUpdateoptions.Description = "Suncoast Fresh – Order - " + orderID;
                                            if (user != null && !string.IsNullOrEmpty(user.Email))
                                            {
                                                chargeUpdateoptions.Description = "Suncoast Fresh – Order - " + orderID + " - " + user.Email;
                                            }
                                            var serviceCharge = new ChargeService();
                                            Charge charge = serviceCharge.Update(intent.Charges.Data[0].Id, chargeUpdateoptions, requestOptions);
                                        }
                                        catch (StripeException ex)
                                        {
                                            _pda.LogPayment("Updating Charge Description:" + ex.Message, orderID);
                                        }
                                        catch (Exception ex)
                                        {
                                            _pda.LogPayment("Updating Charge Description:" + ex.Message, orderID);
                                        }
                                        BalanceTransaction objbalance = balanceservice.Get(intent.Charges.Data[0].BalanceTransactionId, null, requestOptions);
                                        StripePaymentDetail objStripePaymentDetail = new StripePaymentDetail();
                                        objStripePaymentDetail.StripeChargeID = model.PaymentDetails.Id;
                                        objStripePaymentDetail.TotalCaptureAmount = paymentMasters.AuthorizeAmount;
                                        objStripePaymentDetail.CreatedBy = model.UserID.ToString();
                                        objStripePaymentDetail.CreatedOn = DateTime.Now;
                                        objStripePaymentDetail.ModifiedOn = DateTime.Now;
                                        objStripePaymentDetail.NetAmount = Convert.ToDouble(objbalance.Net) / 100;
                                        objStripePaymentDetail.TotalFees = Convert.ToDouble(objbalance.Fee) / 100;
                                        foreach (var item in objbalance.FeeDetails)
                                        {
                                            if (item.Description.ToLower().Trim() == "gst" && item.Type.ToLower().Trim() == "tax")
                                            {
                                                objStripePaymentDetail.Tax = Convert.ToDouble(item.Amount) / 100;
                                            }
                                            else if (item.Description.ToLower() == "stripe processing fees" && item.Type.ToLower() == "stripe_fee")
                                            {
                                                objStripePaymentDetail.StripeProcessingFee = Convert.ToDouble(item.Amount) / 100;
                                            }
                                            else if (item.Description.ToLower().Trim() == "application fee" && item.Type.ToLower().Trim() == "application_fee")
                                            {
                                                objStripePaymentDetail.ApplicationFees = Convert.ToDouble(item.Amount) / 100;
                                            }
                                        }

                                        _db.StripePaymentDetails.Add(objStripePaymentDetail);
                                        _db.SaveChanges();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                _pda.LogPayment("During Stripe payment detail:" + ex.Message, orderID);
                            }
                        }
                    }
                    catch (StripeException ex)
                    {
                        _pda.LogPayment("After Payment:" + ex.Message, orderID);
                    }
                    catch (Exception ex)
                    {
                        _pda.LogPayment("After Payment:" + ex.Message, orderID);
                    }

                    try
                    {
                        // Insert cart items into the CarItem Master table
                        var cartItems = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.TempCartID).ToList();
                        List<trnsCartItemsMaster> cartLi = new List<trnsCartItemsMaster>();

                        Mapper.Map(cartItems, cartLi);
                        //Mapper.Map(model.OrderItems, cartItems);

                        List<long> productIDs = new List<long>();
                        foreach (var item in cartLi)
                        {
                            item.CartID = orderID;

                            if (item.IsSpecialPrice != null && item.IsSpecialPrice == true)
                            {
                                productIDs.Add(item.ProductID);
                            }
                        }

                        if (productIDs.Count > 0)
                        {
                            DeleteSpecialPrice(productIDs, model.CustomerID);
                        }

                        _db.trnsCartItemsMasters.AddRange(cartLi);

                        await _db.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        _pda.LogPayment("Error during adding items in cart item master table and deleteing temp cart items master entries with temp cart id - " + model.TempCartID + "-" + ex.Message, orderID);
                    }

                    // Code to save standing order start
                    if (model.StandingOrderStartDate.HasValue)
                    {
                        var standingOrderData = new StandingOrderMaster();
                        standingOrderData.CartID = orderID;
                        standingOrderData.StartDate = model.StandingOrderStartDate ?? DateTime.Now;
                        if (model.IsOrderDaily == true)
                        {
                            standingOrderData.Daily = model.IsOrderDaily;
                        }
                        else if (model.DaysOfMonth.Count > 0)
                        {
                            standingOrderData.DaysOfMonth = string.Join(",", model.DaysOfMonth);
                        }
                        else if (model.DaysOfWeek.Count > 0)
                        {
                            standingOrderData.DaysOfWeek = string.Join(",", model.DaysOfWeek);
                        }
                        _db.StandingOrderMasters.Add(standingOrderData);
                        await _db.SaveChangesAsync();
                    }
                    // Code to save standing order end

                    var delTempCartItems = _db.trnsTempCartItemsMasters.Where(t => t.CartID == model.TempCartID).ToList();

                    if (delTempCartItems != null)
                    {
                        _db.trnsTempCartItemsMasters.RemoveRange(delTempCartItems);
                        _db.SaveChanges();
                    }
                    var delCart = _db.trnsTempCartMasters.Find(model.TempCartID);
                    if (delCart != null)
                    {
                        _db.trnsTempCartMasters.Remove(delCart);
                        _db.SaveChanges();
                    }

                    // create files and send out email with attachment
                    OrderDetailsBO od = new OrderDetailsBO();

                    od.OrderID = orderID;
                    od.IsOrderHistory = true;
                    od.CustomerID = model.CustomerID;
                    od.UserID = model.UserID;

                    string mail = "";
                    try
                    {
                        mail = SendOrderMail(od);
                    }
                    catch (Exception)
                    {
                        mail = "Failed to send email.";
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Order Placed", new { OrderID = orderID, MailStatus = mail });
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    //    eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    LogErrorsToServer("following validation errors type: " + eve.Entry.Entity.GetType().Name + "in state:" + eve.Entry.State, "", "Place Order");

                    foreach (var ve in eve.ValidationErrors)
                    {
                        //Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                        //    ve.PropertyName, ve.ErrorMessage);

                        LogErrorsToServer("Error Property: " + ve.PropertyName + "Error:" + ve.ErrorMessage, "", "Place Order");
                    }
                }
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message + "\n\r\t Inner Exception: " + ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                LogErrorsToServer("Error Message: " + ex.Message, "", "Place Order");
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        public string DeleteSpecialPrice(List<long> ProductIds, Int64 CustomerId)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var items = new List<SpecialPriceMaster>();

                    foreach (var productID in ProductIds)
                    {
                        var sPrice = _db.SpecialPriceMasters.Where(s => s.ProductID == productID && s.CustomerID == CustomerId && s.IsAlwaysApply == false).FirstOrDefault();
                        if (sPrice != null)
                        {
                            items.Add(sPrice);
                        }
                    }

                    _db.SpecialPriceMasters.RemoveRange(items);
                    _db.SaveChanges();

                    return "";     // Success !
                }
            }
            catch (Exception ex)
            {
                return ex.StackTrace;//ex.StackTrace;
            }
        }

        #endregion Save order/ update order/ Place Order

        #region Get saved order details from the database.

        public List<GetOrderDetailsBO> GetOrderDetails(OrderDetailsBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var customer = _db.CustomerMasters.Find(model.CustomerID);

                    string warehouse = customer.Warehouse ?? "";
                    if (!model.IsOrderHistory)
                    {
                        var cartV = _db.trnsTempCartMasters.Where(c => c.CartID == model.OrderID && c.CustomerID == model.CustomerID && c.IsSavedOrder == true).FirstOrDefault();

                        if (cartV != null)
                        {
                            var data = (from cart in _db.trnsTempCartMasters
                                        join cartItems in _db.trnsTempCartItemsMasters on cart.CartID equals cartItems.CartID
                                        join prod in _db.ProductMasters on cartItems.ProductID equals prod.ProductID
                                        join cust in _db.CustomerMasters on cart.CustomerID equals cust.CustomerID
                                        join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                        join mcat in _db.MainCategoriesMasters on cat.MCategoryId equals mcat.MCategoryId
                                        join unit in _db.UnitOfMeasurementMasters on prod.UOMID equals unit.UOMID

                                        join rwunit in _db.UnitOfMeasurementMasters on prod.RW_UOMID equals rwunit.UOMID into temp3
                                        from rwunit in temp3.DefaultIfEmpty()

                                        join orunit in _db.UnitOfMeasurementMasters on cartItems.UnitId equals orunit.UOMID into temp2
                                        from orunit in temp2.DefaultIfEmpty()

                                        join filter in _db.ProductFiltersMasters on prod.FilterID equals filter.FilterID

                                        join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into temp6
                                        from soh in temp6.DefaultIfEmpty()

                                        join cusadd in _db.CustomerDeliveryAddresses on cart.AddressID equals cusadd.AddressID into temp4
                                        from cusadd in temp4.DefaultIfEmpty()

                                        where (cartItems.CartID == cartV.CartID
                                                && prod.IsAvailable == true
                                                && prod.IsActive == true && cat.IsActive == true
                                                && mcat.IsActive == true && filter.IsActive == true
                                                && (warehouse == "" || soh.WareHouse == null ||
                                                    (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse)
                                               ))
                                        select new GetOrderDetailsBO
                                        {
                                            CartItemID = cartItems.CartItemID,
                                            CartID = cart.CartID,
                                            ProductID = prod.ProductID,
                                            ProductCode = prod.ProductCode,
                                            ProductName = prod.ProductName,
                                            ProductDescription = prod.ProductDescription ?? "",
                                            ProductDescription2 = prod.ProductDescription2 ?? "",
                                            ProductDescription3 = prod.ProductDescription3 ?? "",
                                            ProductFeature1 = prod.ProductFeature1 ?? "",
                                            ProductFeature2 = prod.ProductFeature2 ?? "",
                                            ProductFeature3 = prod.ProductFeature3 ?? "",
                                            ProductIsNew = prod.IsNew ?? false,
                                            ProductIsOnSale = prod.IsOnSale ?? false,
                                            ProductIsBackSoon = prod.IsBackSoon ?? false,
                                            IsDelivery = cart.IsDelivery ?? false,
                                            CreatedDate = cart.CreatedDate ?? DateTime.Now,
                                            Price = cartItems.Price,
                                            SpecialPrice = cartItems.Price == null ? 0 : cartItems.Price,
                                            CompanyPrice = prod.ComPrice ?? 0,
                                            Quantity = cartItems.Quantity,
                                            IsPlacedByRep = cart.IsOrderPlpacedByRep ?? false,
                                            SalesRepUserID = cart.RepUserID,
                                            CustomerName = cust.CustomerName,
                                            CustomerCode = cust.AlphaCode,
                                            CustomerID = cust.CustomerID,
                                            Address1 = cusadd.AddressID != null ? (cusadd.Address1 != null ? cusadd.Address1.Trim() : "") : (cust.Address1 != null ? cust.Address1.Trim() : ""),
                                            Address2 = cusadd.AddressID != null ? (cusadd.Address2 != null ? cusadd.Address2.Trim() : "") : (cust.Address2 != null ? cust.Address2.Trim() : ""),
                                            Suburb = cusadd.AddressID != null ? (cusadd.Address3 != null ? cusadd.Address3.Trim() : "") : (cust.Suburb != null ? cust.Suburb.Trim() : ""),
                                            AddressId = cart.AddressID ?? 0,

                                            ProductImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + prod.ProductImage,
                                            ProductThumbImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + "thumb_" + prod.ProductImage,
                                            UOMID = cartItems.UnitId,//(prod.RandomWeightItem == true && rwunit.UOMID != null) ? unit.UOMID : (unit.UOMID == null ? 0 : unit.UOMID),
                                            UnitName = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? (rwunit.UOMDesc == null ? "" : rwunit.UOMDesc) : (unit.UOMDesc == null ? "" : unit.UOMDesc),
                                            OrderUnitName = orunit.Code ?? unit.Code,
                                            OrderUnitId = (orunit.UOMID == null) ? unit.UOMID : orunit.UOMID,
                                            FilterName = filter.FilterName,
                                            ProductWeight = cartItems.ProductWeight ?? 0,
                                            WeightName = cartItems.WeightName,
                                            WeightDescription = cartItems.WeightDescription,
                                            IsPieceOrWeight = cartItems.IsPieceOrWeight,
                                            IsAvailable = prod.IsAvailable ?? false,
                                            StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                            IsSpecial = false,//(sp.SpecialPriceID == null) ? false : true,
                                            IsDiscountApplicable = false,
                                            IsCountrywideRewards = prod.RewardItem ?? false,
                                            IsInvoiceComment = cart.IsInvoiceComment ?? false,
                                            IsUnloadComment = cart.IsUnloadComment ?? false,
                                            IsNoPantry = cartItems.IsNoPantry ?? false,
                                            ExtDoc = cart.ExtDoc ?? "",
                                            RunNo = cart.RunNo ?? "",
                                            IsGST = prod.GST ?? false,
                                            PackagingSequence = cart.PackagingSequence ?? "",
                                            Brand = prod.Brand ?? "",
                                            Supplier = prod.Supplier ?? "",
                                            IsSpecialCategory = cat.IsSpecialCategory ?? false,
                                            IsDonationBox = prod.IsDonationBox ?? false
                                        }).ToList();

                            return data;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        var cartV = _db.trnsCartMasters.Where(c => c.CartID == model.OrderID && c.CustomerID == model.CustomerID).FirstOrDefault();

                        if (cartV != null)
                        {
                            var data = (from cart in _db.trnsCartMasters
                                        join cartItems in _db.trnsCartItemsMasters on cart.CartID equals cartItems.CartID
                                        join prod in _db.ProductMasters on cartItems.ProductID equals prod.ProductID
                                        join cust in _db.CustomerMasters on cart.CustomerID equals cust.CustomerID
                                        join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                        join mcat in _db.MainCategoriesMasters on cat.MCategoryId equals mcat.MCategoryId
                                        join unit in _db.UnitOfMeasurementMasters on prod.UOMID equals unit.UOMID

                                        join comment in _db.DeliveryCommentsMasters on cart.CommentID equals comment.CommentID into cTemp
                                        from comment in cTemp.DefaultIfEmpty()

                                        join proCom in _db.DeliveryProductCommentsMasters on cartItems.CommentID equals proCom.PCommentID into cpTemp
                                        from proCom in cpTemp.DefaultIfEmpty()

                                        join rwunit in _db.UnitOfMeasurementMasters on prod.RW_UOMID equals rwunit.UOMID into temp3
                                        from rwunit in temp3.DefaultIfEmpty()

                                        join orunit in _db.UnitOfMeasurementMasters on cartItems.UnitId equals orunit.UOMID into temp2
                                        from orunit in temp2.DefaultIfEmpty()

                                        join filter in _db.ProductFiltersMasters on prod.FilterID equals filter.FilterID

                                        join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into temp6
                                        from soh in temp6.DefaultIfEmpty()

                                        join cusadd in _db.CustomerDeliveryAddresses on cart.AddressID equals cusadd.AddressID into temp4
                                        from cusadd in temp4.DefaultIfEmpty()
                                        where (cartItems.CartID == cartV.CartID
                                                && prod.IsAvailable == true
                                                && prod.IsActive == true && cat.IsActive == true
                                                && mcat.IsActive == true && filter.IsActive == true
                                                && (warehouse == "" || soh.WareHouse == null ||
                                                    (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse)
                                               ))
                                        select new GetOrderDetailsBO
                                        {
                                            UserID = cart.UserID,
                                            ProductID = prod.ProductID,
                                            CartItemID = cartItems.CartItemID,
                                            CartID = cart.CartID,
                                            ProductCode = prod.ProductCode,
                                            ProductName = prod.ProductName,
                                            PONumber = cart.PONumber,
                                            CategoryName = mcat.MCategoryName,
                                            SubCategoryName = cat.CategoryName,
                                            ProductDescription = prod.ProductDescription ?? "",
                                            ProductDescription2 = prod.ProductDescription2 ?? "",
                                            ProductDescription3 = prod.ProductDescription3 ?? "",
                                            ProductFeature1 = prod.ProductFeature1 ?? "",
                                            ProductFeature2 = prod.ProductFeature2 ?? "",
                                            ProductFeature3 = prod.ProductFeature3 ?? "",
                                            ProductIsNew = prod.IsNew ?? false,
                                            ProductIsOnSale = prod.IsOnSale ?? false,
                                            ProductIsBackSoon = prod.IsBackSoon ?? false,
                                            IsDelivery = cart.IsDelivery ?? false,
                                            OrderDate = cart.OrderDate ?? DateTime.Now,
                                            CreatedDate = cart.CreatedDate ?? DateTime.Now,
                                            Price = cartItems.Price ?? 0,
                                            SpecialPrice = cartItems.Price == null ? 0 : cartItems.Price,
                                            CompanyPrice = prod.ComPrice ?? 0,
                                            Quantity = cartItems.Quantity ?? 0,
                                            IsPlacedByRep = cart.IsOrderPlpacedByRep ?? false,
                                            Comment = comment.CommentDescription ?? "",
                                            SalesRepUserID = cart.UserID,
                                            CustomerName = cust.CustomerName,
                                            CustomerCode = cust.AlphaCode,
                                            CustomerID = cust.CustomerID,
                                            Address1 = cusadd.AddressID != null ? (cusadd.Address1 != null ? cusadd.Address1.Trim() : "") : (cust.Address1 != null ? cust.Address1.Trim() : ""),
                                            Address2 = cusadd.AddressID != null ? (cusadd.Address2 != null ? cusadd.Address2.Trim() : "") : (cust.Address2 != null ? cust.Address2.Trim() : ""),
                                            Suburb = cusadd.AddressID != null ? (cusadd.Address3 != null ? cusadd.Address3.Trim() : "") : (cust.Suburb != null ? cust.Suburb.Trim() : ""),
                                            AddressId = cart.AddressID ?? 0,
                                            SalesmanCode = cust.SalesmanCode,
                                            ContactName = cust.ContactName,
                                            ContactNo = cust.Phone1,
                                            InvoiceNumber = cart.InvoiceNumber ?? "",
                                            ProductImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + prod.ProductImage,
                                            ProductThumbImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + "thumb_" + prod.ProductImage,
                                            UOMID = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? unit.UOMID : (unit.UOMID == null ? 0 : unit.UOMID),
                                            UnitName = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? (rwunit.UOMDesc == null ? "" : rwunit.UOMDesc) : (unit.UOMDesc == null ? "" : unit.UOMDesc),
                                            OrderUnitName = orunit.UOMDesc ?? unit.UOMDesc,
                                            OrderUnitId = (orunit.UOMID == null) ? unit.UOMID : orunit.UOMID,
                                            FilterName = filter.FilterName,
                                            ProductWeight = cartItems.ProductWeight ?? 0,
                                            WeightName = cartItems.WeightName,
                                            WeightDescription = cartItems.WeightDescription,
                                            IsPieceOrWeight = cartItems.IsPieceOrWeight,
                                            IsAvailable = prod.IsAvailable ?? false,
                                            StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                            IsSpecial = cartItems.IsSpecialPrice ?? false,
                                            IsDiscountApplicable = false,
                                            IsCountrywideRewards = prod.RewardItem ?? false,
                                            IsInvoiceComment = cart.IsInvoiceComment ?? false,
                                            IsUnloadComment = cart.IsUnloadComment ?? false,
                                            IsNoPantry = cartItems.IsNoPantry ?? false,
                                            ExtDoc = cart.ExtDoc ?? "",
                                            RunNo = cart.RunNo ?? "",
                                            IsGST = prod.GST ?? false,
                                            PackagingSequence = cart.PackagingSequence ?? "",
                                            Brand = prod.Brand ?? "",
                                            Supplier = prod.Supplier ?? "",
                                            IsSpecialCategory = cat.IsSpecialCategory ?? false,
                                            IsOrderedSpecial = cartItems.IsSpecialPrice ?? false,
                                            CommentLine = cart.CommentLine ?? "",
                                            ProComment = proCom.CommentDescription ?? "",
                                            HasFreightCharges = cart.HasFreightCharges ?? false,
                                            HasNonDeliveryDayCharges = cart.HasNonDeliveryDayCharges ?? false,
                                            Warehouse = warehouse,
                                            IsOrderPostingToFtp = cart.IsOrderPostingToFTP,
                                            RewardItem = prod.RewardItem ?? false,
                                            ERPStatus = prod.ERPStatus,
                                            DeliveryType = cart.DeliveryType ?? "",
                                            IsDonationBox = prod.IsDonationBox ?? false,
                                            ProductImages = (from a in _db.ProductImageMasters
                                                             where a.ProductID == prod.ProductID
                                                             select new ProductImages
                                                             {
                                                                 ImageName = a.BaseUrl + "thumb_" + a.ProductImage
                                                             }).ToList(),
                                        }).ToList();

                            return data;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<APiResponse> GetOrderDetailsCart(OrderDetailsBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var customer = _db.CustomerMasters.Find(model.CustomerID);
                    var customerFeatures = _db.CustomerFeaturesMasters.Where(x => x.UserID == model.UserID).FirstOrDefault();

                    bool isDynamicUOM = false;
                    if (customerFeatures != null)
                    {
                        isDynamicUOM = Convert.ToBoolean(customerFeatures.IsDynamicUOM);
                    }

                    string warehouse = customer.Warehouse ?? "";
                    if (!model.IsOrderHistory)
                    {
                        var cartV = _db.trnsTempCartMasters.Where(c => c.CartID == model.OrderID && c.CustomerID == model.CustomerID && c.IsSavedOrder == true).FirstOrDefault();

                        if (cartV != null)
                        {
                            var data = await (from cart in _db.trnsTempCartMasters
                                              join cartItems in _db.trnsTempCartItemsMasters on cart.CartID equals cartItems.CartID
                                              join prod in _db.ProductMasters on cartItems.ProductID equals prod.ProductID
                                              join cust in _db.CustomerMasters on cart.CustomerID equals cust.CustomerID
                                              join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                              join mcat in _db.MainCategoriesMasters on cat.MCategoryId equals mcat.MCategoryId
                                              join unit in _db.UnitOfMeasurementMasters on prod.UOMID equals unit.UOMID

                                              join rwunit in _db.UnitOfMeasurementMasters on prod.RW_UOMID equals rwunit.UOMID into temp3
                                              from rwunit in temp3.DefaultIfEmpty()

                                              join orunit in _db.UnitOfMeasurementMasters on cartItems.UnitId equals orunit.UOMID into temp2
                                              from orunit in temp2.DefaultIfEmpty()

                                              join filter in _db.ProductFiltersMasters on prod.FilterID equals filter.FilterID

                                              join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into temp6
                                              from soh in temp6.DefaultIfEmpty()

                                              join cusadd in _db.CustomerDeliveryAddresses on cart.AddressID equals cusadd.AddressID into temp4
                                              from cusadd in temp4.DefaultIfEmpty()

                                              where (cartItems.CartID == cartV.CartID
                                                      && prod.IsAvailable == true
                                                      && prod.IsActive == true && cat.IsActive == true
                                                      && mcat.IsActive == true && filter.IsActive == true
                                                      && (warehouse == "" || soh.WareHouse == null ||
                                                          (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse)
                                                     ))
                                              select new CartItemsBO
                                              {
                                                  CartItemID = cartItems.CartItemID,
                                                  CartID = cart.CartID,
                                                  ProductID = prod.ProductID,
                                                  ProductCode = prod.ProductCode,
                                                  ProductName = prod.ProductName,
                                                  ProductDescription = prod.ProductDescription ?? "",

                                                  ProductDescription2 = prod.ProductDescription2 ?? "",
                                                  ProductDescription3 = prod.ProductDescription3 ?? "",
                                                  ProductFeature1 = prod.ProductFeature1 ?? "",
                                                  ProductFeature2 = prod.ProductFeature2 ?? "",
                                                  ProductFeature3 = prod.ProductFeature3 ?? "",
                                                  ProductIsNew = prod.IsNew ?? false,
                                                  ProductIsOnSale = prod.IsOnSale ?? false,
                                                  ProductIsBackSoon = prod.IsBackSoon ?? false,
                                                  IsDelivery = cart.IsDelivery ?? false,

                                                  CreatedDate = cart.CreatedDate ?? DateTime.Now,
                                                  Price = cartItems.Price,
                                                  CompanyPrice = prod.ComPrice ?? 0,
                                                  Quantity = cartItems.Quantity,
                                                  CustomerCode = cust.AlphaCode,
                                                  CustomerID = cust.CustomerID,

                                                  ProductImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + prod.ProductImage,
                                                  ProductThumbImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + "thumb_" + prod.ProductImage,
                                                  UOMID = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? unit.UOMID : (unit.UOMID == null ? 0 : unit.UOMID),
                                                  UnitName = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? (rwunit.UOMDesc == null ? "" : rwunit.Code) : (unit.UOMDesc == null ? "" : unit.Code),
                                                  OrderUnitName = orunit.Code ?? unit.Code,
                                                  OrderUnitId = (orunit.UOMID == null) ? unit.UOMID : orunit.UOMID,
                                                  FilterName = filter.FilterName,
                                                  ProductWeight = cartItems.ProductWeight ?? 0,
                                                  WeightName = cartItems.WeightName,
                                                  WeightDescription = cartItems.WeightDescription,
                                                  IsPieceOrWeight = cartItems.IsPieceOrWeight,
                                                  IsAvailable = prod.IsAvailable ?? false,
                                                  StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                                  IsSpecial = false,//(sp.SpecialPriceID == null) ? false : true,
                                                  IsDiscountApplicable = false,
                                                  IsCountrywideRewards = prod.RewardItem ?? false,
                                                  IsInvoiceComment = cart.IsInvoiceComment ?? false,
                                                  IsUnloadComment = cart.IsUnloadComment ?? false,
                                                  IsNoPantry = cartItems.IsNoPantry ?? false,
                                                  ExtDoc = cart.ExtDoc ?? "",
                                                  RunNo = cart.RunNo ?? "",
                                                  IsGST = prod.GST ?? false,
                                                  PackagingSequence = cart.PackagingSequence ?? "",
                                                  Brand = prod.Brand ?? "",
                                                  Supplier = prod.Supplier ?? "",
                                                  IsDonationBox = prod.IsDonationBox ?? false
                                              }).ToListAsync();

                            var result = new List<CartItemsBO>();

                            ProductDA obj = new ProductDA();

                            result = obj.GetPricesLogicCart(model.CustomerID, data, isDynamicUOM, _db, model.UserID, false);

                            return ReturnMethod(SaaviErrorCodes.Success, "Success", result);
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                        }
                    }
                    else
                    {
                        var cartV = _db.trnsCartMasters.Where(c => c.CartID == model.OrderID && c.CustomerID == model.CustomerID).FirstOrDefault();

                        if (cartV != null)
                        {
                            var data = await (from cart in _db.trnsCartMasters
                                              join cartItems in _db.trnsCartItemsMasters on cart.CartID equals cartItems.CartID
                                              join prod in _db.ProductMasters on cartItems.ProductID equals prod.ProductID
                                              join cust in _db.CustomerMasters on cart.CustomerID equals cust.CustomerID
                                              join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                              join mcat in _db.MainCategoriesMasters on cat.MCategoryId equals mcat.MCategoryId
                                              join unit in _db.UnitOfMeasurementMasters on prod.UOMID equals unit.UOMID

                                              join rwunit in _db.UnitOfMeasurementMasters on prod.RW_UOMID equals rwunit.UOMID into temp3
                                              from rwunit in temp3.DefaultIfEmpty()

                                              join orunit in _db.UnitOfMeasurementMasters on cartItems.UnitId equals orunit.UOMID into temp2
                                              from orunit in temp2.DefaultIfEmpty()

                                              join filter in _db.ProductFiltersMasters on prod.FilterID equals filter.FilterID

                                              join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into temp6
                                              from soh in temp6.DefaultIfEmpty()

                                              join cusadd in _db.CustomerDeliveryAddresses on cart.AddressID equals cusadd.AddressID into temp4
                                              from cusadd in temp4.DefaultIfEmpty()
                                              where (cartItems.CartID == cartV.CartID
                                                      && prod.IsAvailable == true
                                                      && prod.IsActive == true && cat.IsActive == true
                                                      && mcat.IsActive == true && filter.IsActive == true
                                                      && (warehouse == "" || soh.WareHouse == null ||
                                                          (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse)
                                                     ))
                                              select new CartItemsBO
                                              {
                                                  ProductID = prod.ProductID,
                                                  CartItemID = cartItems.CartItemID,
                                                  CartID = cart.CartID,
                                                  ProductCode = prod.ProductCode,
                                                  ProductName = prod.ProductName,
                                                  ProductDescription = prod.ProductDescription ?? "",

                                                  ProductDescription2 = prod.ProductDescription2 ?? "",
                                                  ProductDescription3 = prod.ProductDescription3 ?? "",
                                                  ProductFeature1 = prod.ProductFeature1 ?? "",
                                                  ProductFeature2 = prod.ProductFeature2 ?? "",
                                                  ProductFeature3 = prod.ProductFeature3 ?? "",
                                                  ProductIsNew = prod.IsNew ?? false,
                                                  ProductIsOnSale = prod.IsOnSale ?? false,
                                                  ProductIsBackSoon = prod.IsBackSoon ?? false,
                                                  IsDelivery = cart.IsDelivery ?? false,

                                                  OrderDate = cart.OrderDate ?? DateTime.Now,
                                                  CreatedDate = cart.CreatedDate ?? DateTime.Now,
                                                  Price = cartItems.Price,
                                                  CompanyPrice = prod.ComPrice ?? 0,
                                                  Quantity = cartItems.Quantity,

                                                  CustomerCode = cust.AlphaCode,
                                                  CustomerID = cust.CustomerID,

                                                  ProductImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + prod.ProductImage,
                                                  ProductThumbImage = HelperClass.WebsiteUrl + "Admin/Content/Uploads/Products/" + "thumb_" + prod.ProductImage,
                                                  UOMID = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? unit.UOMID : (unit.UOMID == null ? 0 : unit.UOMID),
                                                  UnitName = (prod.RandomWeightItem == true && rwunit.UOMID != null) ? (rwunit.UOMDesc == null ? "" : rwunit.UOMDesc) : (unit.UOMDesc == null ? "" : unit.UOMDesc),
                                                  OrderUnitName = orunit.UOMDesc ?? unit.UOMDesc,
                                                  OrderUnitId = (orunit.UOMID == null) ? unit.UOMID : orunit.UOMID,
                                                  FilterName = filter.FilterName,
                                                  ProductWeight = cartItems.ProductWeight ?? 0,
                                                  WeightName = cartItems.WeightName,
                                                  WeightDescription = cartItems.WeightDescription,
                                                  IsPieceOrWeight = cartItems.IsPieceOrWeight,
                                                  IsAvailable = prod.IsAvailable ?? false,
                                                  StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                                  IsSpecial = false,//(sp.SpecialPriceID == null) ? false : true,
                                                  IsDiscountApplicable = false,
                                                  IsCountrywideRewards = prod.RewardItem ?? false,
                                                  IsInvoiceComment = cart.IsInvoiceComment ?? false,
                                                  IsUnloadComment = cart.IsUnloadComment ?? false,
                                                  IsNoPantry = cartItems.IsNoPantry ?? false,
                                                  ExtDoc = cart.ExtDoc ?? "",
                                                  RunNo = cart.RunNo ?? "",
                                                  IsGST = prod.GST ?? false,
                                                  PackagingSequence = cart.PackagingSequence ?? "",
                                                  Brand = prod.Brand ?? "",
                                                  Supplier = prod.Supplier ?? "",
                                                  IsDonationBox = prod.IsDonationBox ?? false
                                              }).ToListAsync();

                            var result = new List<CartItemsBO>();

                            ProductDA obj = new ProductDA();

                            // result = obj.GetPricesLogicCart(model.CustomerID, data, isDynamicUOM);

                            return ReturnMethod(SaaviErrorCodes.Success, "Success", data);
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get saved order details from the database.

        #region Delete Saved order

        public async Task<APiResponse> DeleteOrder(DeleteSavedOrder model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var cartItems = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.orderID).ToList();

                    // remove all the items that are there in the cart
                    _db.trnsTempCartItemsMasters.RemoveRange(cartItems);
                    _db.SaveChanges();

                    // remove the main cart record
                    var cart = _db.trnsTempCartMasters.Where(m => m.CartID == model.orderID).FirstOrDefault();
                    if (cart != null)
                    {
                        _db.trnsTempCartMasters.Remove(cart);
                        await _db.SaveChangesAsync();
                        return ReturnMethod(SaaviErrorCodes.Success, "Saved order deleted.");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NotFound, "Order not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Delete Saved order

        #region Delete cart items

        public async Task<APiResponse> DeleteCartItems(DeleteCartItemBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var cartItem = _db.trnsTempCartItemsMasters.Find(model.CartItemID);

                    if (cartItem != null)
                    {
                        _db.trnsTempCartItemsMasters.Remove(cartItem);

                        var cart = _db.trnsTempCartMasters.Find(cartItem.CartID);

                        cart.Coupon = "";
                        cart.IsCouponApplied = false;
                        cart.CouponAmount = 0;
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Item deleted");
                    }

                    await _db.SaveChangesAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Item deleted");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Delete cart items

        #region Get Order History

        public async Task<APiResponse> GetOrderHistory(long customerID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var orders = _db.trnsCartMasters.Where(s => s.CustomerID == customerID).FirstOrDefault();

                    if (orders != null)
                    {
                        var data = await (from or in _db.trnsCartMasters
                                          join com in _db.DeliveryCommentsMasters on or.CommentID equals com.CommentID into temp
                                          from com in temp.DefaultIfEmpty()
                                          where (or.CustomerID == customerID)
                                          orderby or.CreatedDate descending
                                          select new OrderHistoryBO
                                          {
                                              CartID = or.CartID,
                                              OrderNumber = "SCF-HD-" + or.CartID.ToString() + " / " + (or.SenderReference == null ? " - " : or.SenderReference),
                                              CommentID = com.CommentID == null ? 0 : com.CommentID,
                                              Comment = com.CommentDescription,
                                              OrderStatus = or.OrderStatus == null ? 1 : or.OrderStatus,
                                              OrderStatusDesc = or.SupplierOrderNumber ?? "", //or.OrderStatus == null ? "Open" : or.OrderStatus == 1 ? "Open" : or.OrderStatus == 2 ? "Processing" : or.OrderStatus == 3 ? "Completed" : or.OrderStatus == 4 ? "Cancelled" : or.OrderStatus == 5 ? "In Progress" : or.OrderStatus == 6 ? "Released" : or.OrderStatus == 9 ? "Picked" : "",
                                              CheckOrderStatus = (or.OrderStatusDescription == null || or.OrderStatusDescription == "") ? "-" : or.OrderStatusDescription,
                                              OrderDateLong = or.CreatedDate ?? DateTime.Now,
                                              //OrderDate = or.CreatedDate ?? DateTime.Now,
                                              IsAutoOrdered = or.IsAutoOrdered ?? false,
                                              IsDelivery = or.IsDelivery ?? false,
                                              Price = ((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == or.CartID)
                                                        select new
                                                        {
                                                            cd.Price,
                                                            cd.Quantity
                                                        }).ToList().Select(c => (c.Price * c.Quantity)).Sum())
                                          }).ToListAsync();

                        if (data.Count > 0)
                        {
                            foreach (var item in data)
                            {
                                item.OrderDate = item.OrderDateLong.To<DateTime>().ToString("yyyy-MM-dd") ?? DateTime.Now.To<DateTime>().ToString("yyyy-MM-dd");
                                item.DateFormatLong = item.OrderDateLong.ToTicks();
                            }
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", data);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Orders found for this customer.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Order History

        #region Get Order History

        public async Task<APiResponse> GetPaymentOrders(string token)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    token = HelperClass.DecodeBase64(token);

                    var user = _db.UserPaymentLinkTokenMasters
                        .Join(_db.UserMasters,
                            t => t.UserId,
                            u => u.UserID,
                            (t, u) => new { t.Token, u }
                        )
                        .Where(x => x.Token == token).FirstOrDefault();

                    if (user == null)
                    {
                        ReturnMethod(SaaviErrorCodes.NotFound, "Invalid Payment Token!");
                    }

                    var retailCustomerID = HelperClass.CustomerID;

                    var data = await (from or in _db.trnsCartMasters
                                      join pay in _db.PaymentMasters on or.CartID equals pay.CartID into temp
                                      from pay in temp.DefaultIfEmpty()
                                      where or.CustomerID == user.u.CustomerID && or.OrderStatus == 1
                                      orderby or.CreatedDate descending
                                      select new PaymentOrderBO
                                      {
                                          CartID = or.CartID,
                                          UserID = or.UserID ?? 0,
                                          PaymentId = pay.PaymentID,
                                          IsPaidOrder = pay.IsStripeAuthorisationSuccess ?? false,
                                          OrderNumber = "SCF-HD-" + or.CartID.ToString() + " / " + (or.SenderReference == null ? " - " : or.SenderReference),
                                          OrderStatus = or.OrderStatus == null ? 1 : or.OrderStatus,
                                          OrderStatusDesc = or.SupplierOrderNumber ?? "Open", //or.OrderStatus == null ? "Open" : or.OrderStatus == 1 ? "Open" : or.OrderStatus == 2 ? "Processing" : or.OrderStatus == 3 ? "Completed" : or.OrderStatus == 4 ? "Cancelled" : or.OrderStatus == 5 ? "In Progress" : or.OrderStatus == 6 ? "Released" : or.OrderStatus == 9 ? "Picked" : "",
                                          CheckOrderStatus = (or.OrderStatusDescription == null || or.OrderStatusDescription == "") ? "-" : or.OrderStatusDescription,
                                          OrderDateLong = or.CreatedDate ?? DateTime.Now,
                                          //OrderDate = or.CreatedDate ?? DateTime.Now,
                                          IsAutoOrdered = or.IsAutoOrdered ?? false,
                                          IsDelivery = or.IsDelivery ?? false,
                                          Price = ((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == or.CartID)
                                                    select new
                                                    {
                                                        cd.Price,
                                                        cd.Quantity
                                                    }).ToList().Select(c => (c.Price * c.Quantity)).Sum())
                                      })
                                      //.Where(x => x.IsPaidOrder == false)
                                      .ToListAsync();
                    if (retailCustomerID == user.u.CustomerID)
                    {
                        data = data.Where(x => x.UserID == user.u.UserID).ToList();
                    }

                    if (data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            item.OrderDate = item.OrderDateLong.To<DateTime>().ToString("yyyy-MM-dd") ?? DateTime.Now.To<DateTime>().ToString("yyyy-MM-dd");
                            item.DateFormatLong = item.OrderDateLong.ToTicks();
                        }
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", data);
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Order History

        #region Save Past Order Payment

        public async Task<APiResponse> SavePastOrderPayment(SavePastOrderPaymentModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var token = HelperClass.DecodeBase64(model.Token);

                    var user = _db.UserPaymentLinkTokenMasters
                        .Join(_db.UserMasters,
                            t => t.UserId,
                            u => u.UserID,
                            (t, u) => new { t.Token, u }
                        )
                        .Where(x => x.Token == token).FirstOrDefault();

                    if (user == null)
                    {
                        ReturnMethod(SaaviErrorCodes.NotFound, "Invalid Payment Token!");
                    }

                    var retailCustomerID = HelperClass.CustomerID;

                    var data = await (from or in _db.trnsCartMasters
                                      join pay in _db.PaymentMasters on or.CartID equals pay.CartID into temp
                                      from pay in temp.DefaultIfEmpty()
                                      where or.CustomerID == user.u.CustomerID && or.OrderStatus == 1
                                      orderby or.CreatedDate descending
                                      select new PaymentOrderBO
                                      {
                                          CartID = or.CartID,
                                          UserID = or.UserID ?? 0,
                                          PaymentId = pay.PaymentID,
                                          IsPaidOrder = pay.IsStripeAuthorisationSuccess ?? false,
                                          OrderNumber = "SCF-HD-" + or.CartID.ToString() + " / " + (or.SenderReference == null ? " - " : or.SenderReference),
                                          OrderStatus = or.OrderStatus == null ? 1 : or.OrderStatus,
                                          OrderStatusDesc = or.SupplierOrderNumber ?? "Open", //or.OrderStatus == null ? "Open" : or.OrderStatus == 1 ? "Open" : or.OrderStatus == 2 ? "Processing" : or.OrderStatus == 3 ? "Completed" : or.OrderStatus == 4 ? "Cancelled" : or.OrderStatus == 5 ? "In Progress" : or.OrderStatus == 6 ? "Released" : or.OrderStatus == 9 ? "Picked" : "",
                                          CheckOrderStatus = (or.OrderStatusDescription == null || or.OrderStatusDescription == "") ? "-" : or.OrderStatusDescription,
                                          OrderDateLong = or.CreatedDate ?? DateTime.Now,
                                          //OrderDate = or.CreatedDate ?? DateTime.Now,
                                          IsAutoOrdered = or.IsAutoOrdered ?? false,
                                          IsDelivery = or.IsDelivery ?? false,
                                          Price = ((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == or.CartID)
                                                    select new
                                                    {
                                                        cd.Price,
                                                        cd.Quantity
                                                    }).ToList().Select(c => (c.Price * c.Quantity)).Sum())
                                      })
                                      //.Where(x => x.IsPaidOrder == false)
                                      .ToListAsync();
                    if (retailCustomerID == user.u.CustomerID)
                    {
                        data = data.Where(x => x.UserID == user.u.UserID).ToList();
                    }

                    if (data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            item.OrderDate = item.OrderDateLong.To<DateTime>().ToString("yyyy-MM-dd") ?? DateTime.Now.To<DateTime>().ToString("yyyy-MM-dd");
                            item.DateFormatLong = item.OrderDateLong.ToTicks();
                        }
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Save Past Order Payment

        #region Get Standing Orders

        public async Task<APiResponse> GetStandingOrders(long customerID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var orders = _db.trnsCartMasters.Where(s => s.CustomerID == customerID && s.IsStandingOrder == true).FirstOrDefault();

                    if (orders != null)
                    {
                        var data = await (from or in _db.trnsCartMasters
                                          join so in _db.StandingOrderMasters on or.CartID equals so.CartID
                                          join com in _db.DeliveryCommentsMasters on or.CommentID equals com.CommentID into temp
                                          from com in temp.DefaultIfEmpty()
                                          where (or.CustomerID == customerID && or.IsStandingOrder == true)
                                          orderby or.CreatedDate descending
                                          select new StandingOrderBO
                                          {
                                              CartID = or.CartID,
                                              OrderNumber = "SCF-HD-" + or.CartID.ToString() + " / " + (or.SenderReference == null ? " - " : or.SenderReference),
                                              StandingOrderStartDate = so.StartDate,
                                              DaysOfMonth = so.DaysOfMonth.Split(',').Select(Int32.Parse).ToList(),
                                              DaysOfWeek = so.DaysOfWeek.Split(',').Select(Int32.Parse).ToList(),
                                              IsOrderDaily = so.Daily,
                                              CommentID = com.CommentID == null ? 0 : com.CommentID,
                                              Comment = com.CommentDescription,
                                              OrderStatus = or.OrderStatus == null ? 1 : or.OrderStatus,
                                              OrderStatusDesc = or.SupplierOrderNumber ?? "", //or.OrderStatus == null ? "Open" : or.OrderStatus == 1 ? "Open" : or.OrderStatus == 2 ? "Processing" : or.OrderStatus == 3 ? "Completed" : or.OrderStatus == 4 ? "Cancelled" : "",
                                              CheckOrderStatus = "", // (or.OrderStatusDescription == null || or.OrderStatusDescription == "") ? "-" : or.OrderStatusDescription,
                                              OrderDate = or.CreatedDate ?? DateTime.Now,
                                              IsAutoOrdered = or.IsAutoOrdered ?? false,
                                              IsDelivery = or.IsDelivery ?? false,
                                              Price = ((from cd in _db.trnsCartItemsMasters.Where(s => s.CartID == or.CartID)
                                                        select new
                                                        {
                                                            cd.Price,
                                                            cd.Quantity
                                                        }).ToList().Select(c => (c.Price * c.Quantity)).Sum())
                                          }).ToListAsync();

                        if (data.Count > 0)
                        {
                            foreach (var item in data)
                            {
                                item.DateFormatLong = item.OrderDate.ToTicks();
                            }
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", data);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Orders found for this customer.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Standing Orders

        #region Cancel Standing Order

        public async Task<APiResponse> CancelStandingOrder(long cartId, long customerId)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var order = _db.trnsCartMasters.Where(s => s.CartID == cartId && s.CustomerID == customerId).FirstOrDefault();

                    if (order != null)
                    {
                        order.IsStandingOrder = false;
                        await _db.SaveChangesAsync();

                        var standingInstructions = _db.StandingOrderMasters.Where(o => o.CartID == cartId).FirstOrDefault();
                        _db.StandingOrderMasters.Remove(standingInstructions);
                        await _db.SaveChangesAsync();
                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Order found for this customer.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Cancel Standing Order

        #region Get Invoice listing / Send invoice PDFs

        public async Task<APiResponse> GetInvoices(GetInvoicesBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (model.IsPdfListing)
                    {
                        List<InvoiceListing> iL = new List<InvoiceListing>();

                        var orders = await _db.trnsCartMasters.Where(c => c.CustomerID == model.CustomerID && c.OrderStatus == 1).OrderByDescending(o => o.CartID).ToListAsync();
                        if (orders.Count > 0)
                        {
                            foreach (var item in orders)
                            {
                                iL.Add(new InvoiceListing()
                                {
                                    InvoiceNumber = HelperClass.OrderFilePrefix + item.CartID,
                                    PdfPath = HelperClass.ApiURLLive + "Invoices/" + HelperClass.OrderFilePrefix + item.CartID.ToString() + ".pdf",
                                    OrderDate = String.Format("{0:MM/dd/yy}", item.OrderDate),
                                    CustomerName = _db.CustomerMasters.Find(item.CustomerID).CustomerName
                                });
                            }
                            return ReturnMethod(SaaviErrorCodes.Success, "Success", iL);
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Invoices for this customer.");
                        }
                    }
                    else
                    {
                        var orders = (from a in _db.trnsCartMasters
                                      join b in _db.CustomerMasters on a.CustomerID equals b.CustomerID
                                      where a.CustomerID == model.CustomerID && a.OrderStatus == 1
                                            && a.OrderDate >= model.FromDate && a.OrderDate <= model.TillDate
                                      select new InvoiceListing
                                      {
                                          InvoiceNumber = HelperClass.OrderFilePrefix + a.CartID,
                                          PdfPath = "Invoices/" + HelperClass.OrderFilePrefix + a.CartID + ".pdf"
                                      }).OrderByDescending(o => o.InvoiceNumber).ToList();

                        string content = "Please find attached, the list of Invoices which fall inside the specified date interval.<br><br>";

                        content += "<ul>";
                        foreach (var item in orders)
                        {
                            content += "<li>";
                            content += "<a href='" + HelperClass.ApiURLLive + item.PdfPath + "' target='_blank'>" + HelperClass.ApiURLLive + item.PdfPath + "<a>";
                            content += "</li>";
                        }

                        content += "</ul>";

                        if (orders.Count == 0)
                        {
                            content = "No invoices found for the dates specified.";
                        }

                        SendMail.SendEMail(model.Email, "", "", "Requested Invoices", content, "", HelperClass.FromEmail);

                        return ReturnMethod(SaaviErrorCodes.Success, "Email sent");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Invoice listing / Send invoice PDFs

        #region Order files / Email Method

        public string OrderPDF(List<GetOrderDetailsBO> orData, long orderID, CustomerMaster cust, string name, string pathPOD = "")
        {
            if (HelperClass.IsPDFGeneration)
            {
                try
                {
                    using (var _db = new Saavi5Entities())
                    {
                        string barcodePath = "";
                        string newFileName = "SCF-HD-" + orderID + name + ".pdf";
                        // Code to create barcode from an OrderID in Saavi
                        try
                        {
                            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                            b.Alignment = AlignmentPositions.RIGHT;
                            System.Drawing.Image img = b.Encode(TYPE.CODE128, orderID.ToString(), System.Drawing.Color.Black, System.Drawing.Color.White, 200, 70);

                            barcodePath = HostingEnvironment.MapPath("~/OrderBarcodes/" + orderID.ToString() + "_barcode.png");
                            img.Save(barcodePath);
                        }
                        catch
                        {
                            barcodePath = "";
                        }

                        List<ItemRow> liItems = new List<ItemRow>();
                        List<InvoicerPOD.Models.ItemRow> liItemsPOD = new List<InvoicerPOD.Models.ItemRow>();

                        foreach (var item in orData)
                        {
                            liItems.Add(new ItemRow()
                            {
                                QTY = (double)item.Quantity,
                                SKU = item.ProductCode,
                                Description = item.ProductName,
                                Comment = item.ProComment,
                                Price = (double)item.Price * (double)item.Quantity,
                                Unit = item.OrderUnitName,
                                WW = "",
                                FW = ""
                            });

                            if (pathPOD != "")
                            {
                                liItemsPOD.Add(new InvoicerPOD.Models.ItemRow()
                                {
                                    QTY = (double)item.Quantity,
                                    SKU = item.ProductCode,
                                    Description = item.ProductName,
                                    Comment = item.ProComment,
                                    Price = (double)item.Price * (double)item.Quantity,
                                    Unit = item.OrderUnitName,
                                    WW = "",
                                    FW = ""
                                });
                            }
                        }

                        var orderComment = orData[0].Comment;
                        var commentLine = orData[0].CommentLine;

                        double? total = liItems.Sum(i => i.Price).To<double>();
                        double? totalWithGst = 0;

                        double? totalGst = 0;
                        string gstRate = "0";
                        double? priceForGst = orData.Where(g => g.IsGST == true).Sum(s => s.Price * s.Quantity);

                        if (priceForGst > 0)
                        {
                            totalGst = (priceForGst * 10) / 100;

                            totalWithGst = total + totalGst;

                            gstRate = "10";
                        }
                        else
                        {
                            totalWithGst = total;
                        }

                        string fileName = "";
                        string invNo = orData[0].InvoiceNumber ?? orderID.ToString();
                        if (pathPOD == "")
                        {
                            fileName = HostingEnvironment.MapPath("~/Invoices/" + HelperClass.OrderFilePrefix + orderID.ToString() + name + ".pdf");
                        }
                        else
                        {
                            fileName = HostingEnvironment.MapPath("~/PODImages/SentInvoices/" + HelperClass.OrderFilePrefix + "POD-" + invNo + ".pdf");
                        }

                        var invoiceNotes = new List<DetailRow>();
                        var totals = new List<TotalRow>();

                        if (HelperClass.InvoiceNotes)
                        {
                            invoiceNotes = new List<DetailRow> {
                                DetailRow.Make("NOTES:",
                                "Make all cheques payable to "+HelperClass.Company+".",
                                "",
                                "If you have any questions concerning this invoice, contact our accounts department.",
                                "",
                                "Thank you for your business.")
                                };
                        }
                        else
                        {
                            invoiceNotes = new List<DetailRow> {
                                DetailRow.Make("NOTES:",
                                "",
                                "Thank you for your business.")
                                };
                        }

                        if (orData[0].HasFreightCharges == true || orData[0].HasNonDeliveryDayCharges == true)
                        {
                            double? nonDeliveryDayCharges = 0;
                            double? freightCharges = 0;

                            var deliveryCharges = _db.DeliveryCharges.FirstOrDefault();

                            if (deliveryCharges != null)
                            {
                                nonDeliveryDayCharges = deliveryCharges.NonDeliveryDayFee;
                                freightCharges = deliveryCharges.DeliveryFee;
                            }

                            totals.Add(TotalRow.Make("Sub Total", (double)total));
                            if (orData[0].HasFreightCharges)
                            {
                                totals.Add(TotalRow.Make("Freight Charges", (double)freightCharges));
                            }
                            else
                            {
                                freightCharges = 0;
                            }
                            if (orData[0].HasNonDeliveryDayCharges)
                            {
                                totals.Add(TotalRow.Make("Non Deliveryday Charges", (double)nonDeliveryDayCharges));
                            }
                            else
                            {
                                nonDeliveryDayCharges = 0;
                            }

                            totals.Add(TotalRow.Make("GST @ " + gstRate + "%", (double)totalGst));

                            totals.Add(TotalRow.Make("Total", (double)(totalWithGst.To<double>() + freightCharges.To<double>() + nonDeliveryDayCharges.To<double>()), true));
                        }
                        else
                        {
                            totals = new List<TotalRow> {
                                TotalRow.Make("Sub Total", (double)total),
                                TotalRow.Make("GST @ "+ gstRate + "%", (double)totalGst),
                                TotalRow.Make("Total", (double)totalWithGst, true),
                                };
                        }

                        if (pathPOD == "")
                        {
                            new InvoicerApi(SizeOption.A4, OrientationOption.Portrait, "$")
                                .Reference(orderID.ToString())
                                .Title(HelperClass.InvoiceTitle)
                                .BillingDate((DateTime)orData[0].CreatedDate)
                                .DueDate((DateTime)orData[0].OrderDate)
                                .TextColor("#505050")
                                .BackColor("#eef8fc")
                                .Image(HostingEnvironment.MapPath("~/Images/logo.png"), HelperClass.InvoiceLogoWidth, HelperClass.InvoiceLogoHeight)
                                .BarcodeImage(barcodePath, 180, 35)
                                .Client(Address.Make("ORDER COMMENT(s)", new string[] { orderComment, commentLine }))
                                .Company(Address.Make("CUSTOMER", new string[] { "Name: " + cust.CustomerName, "Contact Person: " + cust.ContactName, "Address: " + cust.Address1, "City: " + cust.City, "Warehouse: " + cust.Warehouse }))

                                // List of items in the Order
                                .Items(liItems)
                                .Totals(totals)

                                .Details(invoiceNotes)

                                .Footer(HelperClass.Company)

                                .Save(fileName);
                        }
                        else
                        {
                            var totalsPOD = new List<InvoicerPOD.Models.TotalRow> {
                                InvoicerPOD.Models.TotalRow.Make("Sub Total", (double)total),
                                InvoicerPOD.Models.TotalRow.Make("GST @ 10%", (double)totalGst),
                                InvoicerPOD.Models.TotalRow.Make("Total", (double)totalWithGst, true),
                                };

                            var invoiceNotesPOD = new List<InvoicerPOD.Models.DetailRow> {
                                InvoicerPOD.Models.DetailRow.Make("NOTES:",
                                "OWNERSHIP IN GOODS NOT PASS UNTIL PAYMENT, BUT RISK PASSES ON DELIVERY.",
                                "NO CLAIMS RECOGNISED UNLESS RECEIVED WITHIN 24 HOURS.",
                                "",
                                "A 2% SURCHARGE APPLIES TO ALL MASTERCARD AND VISA PAYMENTS")
                                };

                            new InvoicerPOD.Services.InvoicerApi(InvoicerPOD.Models.SizeOption.A4, InvoicerPOD.Models.OrientationOption.Portrait, "$")
                               .Reference(invNo)
                               .ABN("ABN: " + HelperClass.ABN)
                               .Title("TAX INVOICE")
                               .BillingDate((DateTime)orData[0].CreatedDate)
                               .DueDate((DateTime)orData[0].OrderDate)
                               .TextColor("#505050")
                               .BackColor("#eef8fc")
                               .Image(HostingEnvironment.MapPath("~/Images/logo.png"), HelperClass.InvoiceLogoWidth, HelperClass.InvoiceLogoHeight)
                               .BarcodeImage(barcodePath, 180, 35)
                               .Client(InvoicerPOD.Models.Address.Make("ORDER COMMENT(s)", new string[] { orderComment, commentLine }))
                               .Company(InvoicerPOD.Models.Address.Make("CUSTOMER", new string[] { cust.CustomerName, cust.ContactName, cust.Address1, cust.City }))

                               // List of items in the Order
                               .Items(liItemsPOD)
                               .Totals(totalsPOD)
                               .Details(invoiceNotesPOD)
                               .Footer(HelperClass.Company, HostingEnvironment.MapPath("~/Images/delivery_footer.png"))
                               .Save(fileName);
                        }

                        return fileName;
                    }
                }
                catch (Exception ex)
                {
                    LogErrorsToServer("Error While generating PDF: " + ex.ToString(), orderID.ToString(), "Generate PDF");
                    // throw ex;
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public string OrderCSV(List<GetOrderDetailsBO> orData, long orderID)
        {
            if (HelperClass.IsCSVGeneration)
            {
                try
                {
                    var li = orData;
                    string newFileName = "SCF-HD-" + orderID + "_" + DateTime.Now.ToString("ddMMyy") + "_" + DateTime.Now.ToString("HHmm") + ".csv";// +li[0].MOrderID.ToString() + ".csv";

                    string filePath = HostingEnvironment.MapPath("~/Invoices/" + newFileName);

                    //if (CheckIfFileExistsOnFTPAndFetch(newFileName, filePath))
                    //{
                    //    if (!File.Exists(filePath))
                    //    {
                    //        File.Create(filePath).Close();
                    //    }
                    //}

                    StringBuilder sb = new StringBuilder();
                    string data = string.Empty;
                    var lineitems = 0;
                    for (int i = 0; i <= li.Count - 1; i++)
                    {
                        string requiredDate = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(li[i].OrderDate));

                        string isrep = "0";
                        if (li[i].IsPlacedByRep)
                        {
                            isrep = "1";
                        }

                        string cuscomment = Convert.ToString(li[i].Comment == null ? "" : li[i].Comment);
                        if (cuscomment.Length > 50)
                        {
                            cuscomment = cuscomment.Substring(0, 50);
                        }
                        string ProComment = Convert.ToString(li[i].Comment == null ? "" : li[i].Comment);
                        if (ProComment.Length > 50)
                        {
                            ProComment = ProComment.Substring(0, 50);
                        }
                        lineitems++;

                        string isdefaultaddress = li[i].AddressId == 0 ? "Y" : "N";

                        data = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26}",
                                            li[i].CustomerCode, String.Format("{0:dd/MM/yyyy}", DateTime.Now), "SA" + li[i].CartID, (li[i].PONumber == "" ? "SA" + li[i].CartID : li[i].PONumber), lineitems, isrep, li[i].ProductCode, li[i].OrderUnitName, li[i].Quantity, li[i].SpecialPrice, requiredDate, cuscomment.Replace("\n", "").Replace("\r", ""), ProComment.Replace("\n", "").Replace("\r", ""), (li[i].Address1 == "" ? "Blank" : li[i].Address1).Replace("\n", "").Replace("\r", ""), li[i].Address2.Replace("\n", "").Replace("\r", ""), (li[i].Suburb == "" ? "Blank" : li[i].Suburb), li[i].Warehouse, li[i].RunNo, "\"" + li[i].ExtDoc.Replace("\n", "").Replace("\r", "") + "\"", (li[i].IsNoPantry.To<bool>() ? "Y" : "N"), (li[i].IsInvoiceComment.To<bool>() ? "Y" : "N"), (li[i].IsUnloadComment.To<bool>() ? "Y" : "N"), li[i].PackagingSequence, isdefaultaddress, li[i].SalesmanCode, (li[i].IsOrderedSpecial ? "Y" : "N"), li[i].CommentLine);

                        sb.Append(data);
                        sb.AppendLine();
                    }

                    File.AppendAllText(filePath, sb.ToString());

                    // check the flag and upload the tile to FTP
                    if (HelperClass.IsFTPUploading)
                    {
                        FTPHelper ftp = new FTPHelper(HelperClass.FTPUrl, HelperClass.FTPUserName, HelperClass.FTPPassword, HelperClass.FTP_UsePassive, orderID);
                        ftp.upload(newFileName, filePath);
                    }

                    return filePath;
                }
                catch (Exception ex)
                {
                    throw ex;
                    //return ex.Message + ex.StackTrace;
                }
            }
            else
            {
                return "";
            }
        }

        public string OrderExcel(List<GetOrderDetailsBO> orData, long orderID)
        {
            if (HelperClass.IsExcelGeneration)
            {
                Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                try
                {
                    string newFileName = "Order_SA" + orderID + ".xlsx";// +li[0].MOrderID.ToString() + ".csv";
                    string filePath = HostingEnvironment.MapPath("~/Invoices/" + newFileName);
                    // string pdffilePath = HostingEnvironment.MapPath("~/Invoices/" + "Order_SA" + model.OrderID + ".pdf");
                    if (!File.Exists(filePath))
                    {
                        File.Copy(HostingEnvironment.MapPath("~/template/order_summary_template.xlsx"), filePath);

                        //Create workbook object
                        string str = filePath;
                        Microsoft.Office.Interop.Excel.Workbook workbook = excel.Workbooks.Open(Filename: str);
                        try
                        {
                            //Create worksheet object
                            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];

                            //CheckIfFileExistsDelete(newFileName, filePath);
                            //if (CheckIfFileExistsOnFTPAndFetch(newFileName, filePath))
                            //{
                            //    if (!File.Exists(filePath))
                            //    {
                            //        File.Create(filePath).Close();
                            //    }
                            //}
                            var results = orData;
                            // Column Headings
                            int iColumn = 0;
                            // Row Data
                            int rows = 0;
                            excel.Cells[3, 1] = results[0].CustomerName;
                            excel.Cells[2, 1] = "Date: " + Convert.ToDateTime(results[0].OrderDate).ToString("yyyy-MM-dd");

                            //time gen row
                            Microsoft.Office.Interop.Excel.Range timegenrow = (Microsoft.Office.Interop.Excel.Range)excel.Rows[31];
                            if (results.Count == 1)
                            {
                                Microsoft.Office.Interop.Excel.Range row2 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[11];
                                row2.Delete();
                            }
                            for (int i = 7; i < results.Count + 7; i++)
                            {
                                string dt = Convert.ToString(excel.Cells[i, 4]);
                                if (rows > 1)
                                {
                                    int copyCols = 1;
                                    if (rows % 2 == 0)
                                    {
                                        for (copyCols = 1; copyCols < 14; copyCols++)
                                        {
                                            Microsoft.Office.Interop.Excel.Range R1 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[10];
                                            R1.Copy(Type.Missing);

                                            Microsoft.Office.Interop.Excel.Range R2 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[i];
                                            R2.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
                                                Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                                        }
                                    }
                                    else
                                    {
                                        for (copyCols = 1; copyCols < 14; copyCols++)
                                        {
                                            Microsoft.Office.Interop.Excel.Range R1 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[11];
                                            R1.Copy(Type.Missing);

                                            Microsoft.Office.Interop.Excel.Range R2 = (Microsoft.Office.Interop.Excel.Range)excel.Rows[i];
                                            R2.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
                                                Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                                        }
                                    }
                                }
                                excel.Cells[i, 1] = orderID.ToString();
                                excel.Cells[i, 2] = results[rows].CustomerCode;
                                excel.Cells[i, 3] = results[rows].CustomerName;
                                excel.Cells[i, 4] = results[rows].ContactName;  //  customer contact
                                excel.Cells[i, 5] = results[rows].ContactNo; // logged in use id
                                excel.Cells[i, 6] = "";  //ProName + "\r\n" + results[rows].ProComment;
                                excel.Cells[i, 7] = results[rows].OrderDate;
                                excel.Cells[i, 8] = getrequireddate(results[rows].OrderDate.To<DateTime>().ToShortDateString());
                                excel.Cells[i, 9] = results[rows].Price;
                                excel.Cells[i, 10] = results[rows].UnitName;
                                excel.Cells[i, 11] = results[rows].Quantity.ToString();
                                excel.Cells[i, 12] = results[rows].PONumber;
                                excel.Cells[i, 13] = results[rows].Comment;

                                rows++;
                            }

                            int genTimeRow = 10 + results.Count + 2;

                            timegenrow.Copy(Type.Missing);
                            Microsoft.Office.Interop.Excel.Range timegenrownew = (Microsoft.Office.Interop.Excel.Range)excel.Rows[genTimeRow];
                            timegenrownew.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
                                Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                            excel.Cells[genTimeRow, 1] = "Generated at: " + DateTime.Now.ToShortTimeString() + ", " + DateTime.Now.ToShortDateString();

                            timegenrow.Delete();
                            ((Microsoft.Office.Interop.Excel._Worksheet)worksheet).Activate();

                            //Save the workbook
                            workbook.Save();

                            //Close the Workbook
                            workbook.Close();

                            //// Finally Quit the Application
                            ((Microsoft.Office.Interop.Excel._Application)excel).Quit();

                            // check the flag and upload the tile to FTP
                            if (HelperClass.IsFTPUploading)
                            {
                                FTPHelper ftp = new FTPHelper(HelperClass.FTPUrl, HelperClass.FTPUserName, HelperClass.FTPPassword, HelperClass.FTP_UsePassive, orderID);
                                ftp.upload(newFileName, filePath);
                            }

                            return filePath;
                        }
                        catch (Exception ex)
                        {
                            ((Microsoft.Office.Interop.Excel._Application)excel).Quit();
                            foreach (_Workbook wb in excel.Workbooks)
                            {
                                wb.Close();
                            }
                            throw ex;
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                catch (Exception ex)
                {
                    ((Microsoft.Office.Interop.Excel._Application)excel).Quit();
                    throw ex;
                }
            }
            else
            {
                return "";
            }
        }

        public string OrderXML(List<GetOrderDetailsBO> orData, long orderID, string name)
        {
            if (HelperClass.IsXMLGeneration)
            {
                try
                {
                    var li = orData;
                    string newFileName = "orders_" + orderID + name + ".xml";// +li[0].MOrderID.ToString() + ".csv";

                    string filePath = HostingEnvironment.MapPath("~/Invoices/" + newFileName);
                    string defaultxmlPath = HostingEnvironment.MapPath("~/Template/Default.xml");
                    string ItemsxmlPath = HostingEnvironment.MapPath("~/Template/Items.xml");

                    string defaultcontent = "";
                    if (File.Exists(defaultxmlPath) && File.Exists(defaultxmlPath))
                    {
                        StreamReader reader = File.OpenText(defaultxmlPath);
                        Hashtable ht = new Hashtable();
                        defaultcontent = reader.ReadToEnd();
                        reader.Close();
                        string itemscontent = "";
                        string itemsmaincontent = "";
                        reader = File.OpenText(ItemsxmlPath);
                        itemscontent = reader.ReadToEnd();
                        reader.Close();
                        string orderDate = li[0].CreatedDate.To<DateTime>().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");
                        DateTime requiredDate_dt = getrequireddate(li[0].OrderDate.ToString());
                        string requiredDate = requiredDate_dt.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");

                        long cuid = li[0].CustomerID;
                        for (int i = 0; i <= li.Count - 1; i++)
                        {
                            ht = new Hashtable();
                            ht["@OrderId"] = li[i].CartID;
                            ht["@CustomerCode"] = CleanInvalidXmlChars(li[i].CustomerCode);
                            ht["@WareHouseId"] = CleanInvalidXmlChars(li[i].Warehouse ?? "");
                            ht["@ProductCode"] = CleanInvalidXmlChars(li[i].ProductCode);

                            string proname_withcomment = li[i].ProductName;
                            //string proname_withcomment2 = "";
                            //if (li[i].ProComment != "" && li[i].ProComment != null)
                            //{
                            //proname_withcomment += " - " + li[i].ProComment.Replace("/", "-");
                            //}
                            if (proname_withcomment.Length > 59)
                            {
                                //proname_withcomment2 = proname_withcomment.Substring(69);
                                proname_withcomment = proname_withcomment.Substring(0, 59);
                            }
                            ht["@ProductName"] = CleanInvalidXmlChars(proname_withcomment);
                            ht["@ProductQuantity"] = li[i].Quantity;
                            ht["@ProductPrice"] = li[i].SpecialPrice;

                            ht["@RequiredDate"] = requiredDate;
                            ht["@LineType"] = "N";
                            ht["@LineNumber"] = i + 1;
                            string UnitName = (li[i].UnitName == null ? "" : li[i].UnitName);
                            if (li[i].OrderUnitName != "")
                            {
                                UnitName = li[i].OrderUnitName;
                            }
                            ht["@SALESUNIT"] = CleanInvalidXmlChars(UnitName);
                            ht["@SPAREDATE"] = DateTime.Today.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");

                            itemsmaincontent += SendMail.ReplaceFileVariablesInTemplateFile(ht, itemscontent);

                            if (li[i].ProComment != null && li[i].ProComment.Trim() != "")
                            {
                                ht = new Hashtable();
                                ht["@OrderId"] = li[i].CartID;
                                ht["@CustomerCode"] = CleanInvalidXmlChars(li[i].CustomerCode);
                                ht["@WareHouseId"] = CleanInvalidXmlChars(li[i].Warehouse ?? "");
                                ht["@ProductCode"] = "/C";
                                ht["@ProductName"] = CleanInvalidXmlChars(li[i].ProComment);
                                ht["@ProductQuantity"] = "0.00";// li[i].Quantity;
                                ht["@ProductPrice"] = "0.00";// li[i].SpecialPrice;
                                ht["@RequiredDate"] = requiredDate;
                                ht["@LineType"] = "L";
                                ht["@LineNumber"] = i + 1;
                                ht["@SALESUNIT"] = "";// CleanInvalidXmlChars(UnitName);
                                ht["@SPAREDATE"] = DateTime.Today.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");

                                itemsmaincontent += SendMail.ReplaceFileVariablesInTemplateFile(ht, itemscontent);
                            }
                        }
                        ht = new Hashtable();
                        ht["@Items"] = itemsmaincontent;
                        ht["@OrderId"] = li[0].CartID;
                        ht["@CustomerCode"] = CleanInvalidXmlChars(li[0].CustomerCode);
                        ht["@WareHouseId"] = CleanInvalidXmlChars(li[0].Warehouse ?? "");
                        ht["@OrderDate"] = Convert.ToDateTime(orderDate).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");
                        ht["@RequiredDate"] = requiredDate;
                        ht["@SPAREDATE"] = DateTime.Today.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");
                        if (li[0].RunNo != null && li[0].RunNo.Length > 5)
                        {
                            ht["@RunNo"] = li[0].RunNo.Substring(0, 5);
                        }
                        else
                        {
                            ht["@RunNo"] = li[0].RunNo;
                        }

                        string OrderComment1 = li[0].Comment == null ? "" : li[0].Comment.Replace("/", "-") ?? "";
                        string OrderComment2 = orderID.ToString() + name + "_";
                        OrderComment2 += li[0].PONumber == "" ? "" : "-" + li[0].PONumber;

                        if (OrderComment1.Length > 25)
                        {
                            OrderComment1 = OrderComment1.Substring(0, 25);
                            // OrderComment2 = OrderComment1.Substring(25) + OrderComment2;
                            if (OrderComment2.Length > 25)
                            {
                                OrderComment2 = OrderComment2.Substring(0, 25);
                            }
                        }
                        ht["@OrderComment1"] = CleanInvalidXmlChars(OrderComment1.TrimStart('-'));
                        ht["@OrderComment2"] = CleanInvalidXmlChars(OrderComment2.TrimStart('-'));
                        defaultcontent = SendMail.ReplaceFileVariablesInTemplateFile(ht, defaultcontent);

                        if (!File.Exists(filePath))
                        {
                            File.Create(filePath).Close();
                        }
                        File.AppendAllText(filePath, defaultcontent);

                        //   bool error = false;

                        try
                        {
                            DataSet ds = new DataSet();
                            ds.ReadXmlSchema(HostingEnvironment.MapPath("~/Template/schema.xsd"));
                            ds.ReadXml(filePath);

                            // For Testing
                            using (var _db = new Saavi5Entities())
                            {
                                int filter = -1;

                                if (name == "_B") { filter = 4; }
                                else if (name == "_C") { filter = 9; }
                                else if (name == "_A") { filter = 0; }

                                if (filter != -1)
                                {
                                    var op = _db.CartMissingOrders.Where(x => x.CartID == orderID && x.OrderType == name && x.OrderName == newFileName).FirstOrDefault();
                                    if (op == null)
                                    {
                                        CartMissingOrder p = new CartMissingOrder();
                                        p.CartID = orderID;
                                        p.OrderName = newFileName;
                                        p.OrderType = name;
                                        p.customerID = cuid;
                                        p.UserID = li[0].UserID;
                                        p.FilterID = filter;
                                        p.OrderDate = li[0].OrderDate;
                                        p.IsOrderPostingToFTP = false;
                                        _db.CartMissingOrders.Add(p);
                                        _db.SaveChanges();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            // error = true;
                            string iE = "";
                            if (ex.InnerException != null)
                            {
                                iE = ":::: Inner Exception ::::" + ex.InnerException.Message;
                            }

                            using (var _db = new Saavi5Entities())
                            {
                                var cartV = _db.trnsCartMasters.Where(c => c.CartID == orderID && c.CustomerID == cuid).FirstOrDefault();

                                if (cartV != null)
                                {
                                    cartV.IsOrderPostingToFTP = true;
                                    _db.SaveChanges();
                                }
                            }

                            LogErrorsToServer(ex.Message + iE, orderID.ToString(), "XML Schema error");
                            string msg = ": Dear Company Admin, This email alert is to advise that SAAVI order number " + orderID.ToString() + ", for customer " + orData[0].CustomerName + ". Saavi has been notified of this. <br><br>Saavi Mobile Ordering Team";
                            SendMail.SendEMail(HelperClass.AdminEmail, "", "phil@saavi.com.au", "Failed SAAVI Order Placement – order number " + orderID.ToString() + " for customer " + orData[0].CustomerName, msg, filePath, HelperClass.FromEmail);
                        }

                        //    if (!error)
                        //    {
                        //        // check the flag and upload the tile to FTP
                        //        if (HelperClass.IsFTPUploading)
                        //        //  if (HelperClass.IsFTPUploading)   //Add for check orderdate is tomorrow
                        //        {
                        //            bool isSuccess = false;
                        //            FTPHelper ftp = new FTPHelper(HelperClass.FTPUrl, HelperClass.FTPUserName, HelperClass.FTPPassword, HelperClass.FTP_UsePassive, orderID);
                        //            isSuccess = ftp.upload(newFileName, filePath);

                        //            if (!isSuccess)
                        //            {
                        //                for (int i = 0; i < 3; i++)
                        //                {
                        //                    LogErrorsToServer("FTP upload failed for order id " + orderID + ". Attempt " + (i + 1), orderID.ToString(), "Error");
                        //                    ftp.delete(newFileName);
                        //                    isSuccess = ftp.upload(newFileName, filePath);
                        //                    if (isSuccess)
                        //                    {
                        //                        break;
                        //                    }
                        //                }
                        //            }

                        //            if (!isSuccess)
                        //            {
                        //                LogErrorsToServer("FTP upload failed for order id " + orderID, orderID.ToString(), "Error");
                        //                string msg = ": Dear Company Admin, This email alert is to advise that SAAVI order number " + orderID.ToString() + " for customer " + orData[0].CustomerName + " was not able to be placed within your ERP import folder due to network access reasons. We will continue for another 3 re-tries. If this order is not able to be placed, please take the appropriate action and either obtain the order file from the order confirmation email which has already been sent or manually enter the details.Attached is a copy of the order file.";
                        //                SendMail.SendEMail(HelperClass.AdminEmail, "", "", "Failed SAAVI Order Placement – order number " + orderID.ToString() + " for customer " + orData[0].CustomerName, msg, filePath, HelperClass.FromEmail);
                        //            }
                        //            else
                        //            {
                        //                using (var _db = new Saavi5Entities())
                        //                {
                        //                    var cartV = _db.trnsCartMasters.Where(c => c.CartID == orderID && c.CustomerID == cuid).FirstOrDefault();

                        //                    if (cartV != null)
                        //                    {
                        //                        cartV.IsOrderPostingToFTP = true;
                        //                        _db.SaveChanges();
                        //                    }
                        //                }

                        //            }
                        //        }

                        //    }

                        //}

                        return filePath;
                    }
                    return "";
                }
                catch (Exception ex)
                {
                    LogErrorsToServer("XML creation error:  " + orderID + ". Error :" + ex.Message, orderID.ToString(), "Error");
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Create Excel/ csv/ pdf and XML files and send and email after an order is placed.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string SendOrderMail(OrderDetailsBO model)
        {
            using (var _db = new Saavi5Entities())
            {
                string attachments = "";
                string salesManCode = "";
                var orderDetails = GetOrderDetails(model);
                var originalOrder = orderDetails;
                var customer = _db.CustomerMasters.Find(model.CustomerID);

                salesManCode = customer.SalesmanCode ?? "";

                try
                {
                    try
                    {
                        //attachments += OrderXML(orderDetails, model.OrderID, "A");

                        string pdfPath = OrderPDF(originalOrder, model.OrderID, customer, "");

                        attachments = string.Format("{0}", pdfPath);
                    }
                    catch (Exception ex)
                    {
                        LogErrorsToServer(ex.Message, model.OrderID.ToString(), "Error Generating PDF");
                    }

                    string mailResult = "";
                    if (customer != null)
                    {
                        // string cusEmail = customer.Email;

                        string repMail = "";
                        string SalesCode = "";
                        string RepName = "";
                        string postcode = "";

                        string subject = "Order successfully submitted | " + HelperClass.Company + " | Order reference # SCF-HD-" + model.OrderID.ToString();

                        var repCustomer = new CustomerMaster();
                        var repUser = _db.UserMasters.Find(originalOrder[0].SalesRepUserID);

                        string cusEmail = repUser.Email ?? customer.Email;

                        if (originalOrder[0].IsPlacedByRep)
                        {
                            if (repUser != null)
                            {
                                repCustomer = _db.CustomerMasters.Find(repUser.CustomerID);

                                repMail = repUser.Email;
                                SalesCode = repCustomer.SalesmanCode;
                                RepName = repCustomer.CustomerName;
                            }
                        }

                        var orderData = _db.trnsCartMasters.Where(s => s.CartID == model.OrderID).FirstOrDefault();
                        var comment = _db.DeliveryCommentsMasters.Where(s => s.CommentID == orderData.CommentID).FirstOrDefault();

                        Hashtable htbl = new Hashtable();

                        htbl["@OrderId"] = "SCF-HD-" + model.OrderID.ToString();
                        htbl["@ContactName"] = (customer.ContactName == null) ? customer.CustomerName : customer.ContactName;
                        htbl["@Phone"] = (customer.Phone1 == null) ? "" : customer.Phone1;
                        htbl["@Postcode"] = customer.PostCode == null ? "" : customer.PostCode;
                        htbl["@ABN"] = (customer.ABN == null) ? "" : customer.ABN;
                        htbl["@OrderDate"] = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime((orderData.CreatedDate == null) ? DateTime.Now : orderData.CreatedDate));
                        htbl["@OrderTime"] = (Convert.ToDateTime((orderData.CreatedDate == null) ? DateTime.Now : orderData.CreatedDate)).ToString("t");
                        htbl["@path"] = HelperClass.ApiURLLive;
                        htbl["@Company"] = HelperClass.Company;

                        // check if order type is delivery or pickup and fill Shipping Address accordingly
                        orderData.IsDelivery = orderData.IsDelivery ?? true;

                        if (orderData.IsDelivery == false)
                        {
                            var pickUp = _db.DeliveryPickupMasters.Find(orderData.PickupID);

                            if (pickUp != null)
                            {
                                htbl["@name"] = pickUp.PickupName;
                                htbl["@street"] = pickUp.PickupAddress;
                                htbl["@suburb"] = "";
                                htbl["@zip"] = "";
                                htbl["@phone1"] = repUser.Phone ?? "";

                                postcode = repUser.Postcode;
                            }
                            else // Incase the onder contains only donation items, as there will be no pick up ID associated.
                            {
                                htbl["@name"] = repUser.FirstName;
                                htbl["@street"] = (repUser.StreetAddress ?? "");
                                htbl["@suburb"] = repUser.Suburb ?? "";
                                htbl["@zip"] = repUser.Postcode ?? "";
                                htbl["@phone1"] = (repUser.Phone == null) ? "" : repUser.Phone;
                            }
                        }
                        else
                        {
                            if (orderData.AddressID != null && orderData.AddressID > 0)
                            {
                                var addr = _db.CustomerDeliveryAddresses.Find(orderData.AddressID);
                                if (addr != null)
                                {
                                    htbl["@name"] = addr.ContactName ?? repUser.FirstName;
                                    htbl["@street"] = addr.Address1;
                                    htbl["@suburb"] = addr.Suburb;
                                    htbl["@zip"] = addr.ZIP;
                                    htbl["@phone1"] = addr.Phone1 ?? "";

                                    postcode = addr.ZIP;
                                }
                            }
                            else
                            {
                                if (repUser.ExistingUserFlag == true)
                                {
                                    htbl["@name"] = repUser.FirstName;
                                    htbl["@street"] = (repUser.StreetAddress ?? "");
                                    htbl["@suburb"] = repUser.Suburb ?? "";
                                    htbl["@zip"] = repUser.Postcode ?? "";
                                    htbl["@phone1"] = (repUser.Phone == null) ? "" : repUser.Phone;

                                    postcode = repUser.Postcode;
                                }
                                else
                                {
                                    htbl["@name"] = (customer.CustomerName == null) ? "" : customer.CustomerName;
                                    htbl["@street"] = (customer.Address1 ?? "") + " " + (customer.Address2 ?? "");
                                    htbl["@suburb"] = customer.Suburb ?? "";
                                    htbl["@zip"] = customer.PostCode == null ? "" : customer.PostCode;
                                    htbl["@phone1"] = customer.Phone1 ?? "";
                                    cusEmail = customer.Email;
                                    postcode = customer.PostCode;
                                }
                            }
                        }

                        htbl["@OrderId"] = "SCF-HD-" + model.OrderID.ToString();

                        string bottomDetails = "";
                        if (orderData.PONumber != null && orderData.PONumber != "")
                        {
                            htbl["@PONum"] = orderData.PONumber;
                        }
                        else
                        {
                            htbl["@PONum"] = "NO P/O PROVIDED";
                        }

                        bottomDetails += "<tr><td width='35%' class='p-l'><i>Delivery Type:</i></td><td><i style='color:red'>" + originalOrder[0].DeliveryType + "</i></td></tr>";

                        //if (comment != null && comment.CommentDescription != "")
                        //{
                        //    bottomDetails += "<tr><td class='p-l'>Delivery Notes:</td><td><i style='color:red'>" + comment.CommentDescription + "</i></td></tr>";
                        //}

                        // fetch the delivery notes
                        // string delnotes = DeliveryNotes(_db, postcode);
                        if (comment != null && comment.CommentDescription != "")
                        {
                            bottomDetails += "<tr><td class='p-l'><i>Delivery Notes: </i></td><td>" + comment.CommentDescription ?? "" + "</td></tr>";
                        }
                        else
                        {
                            bottomDetails += "<tr><td class='p-l'><i>Delivery Notes: </i></td><td></td></tr>";
                        }

                        bottomDetails += "<tr><td class='p-l'><i>Payment Status: </i></td><td>" + PaymentDetails(model.OrderID, _db) + "</td></tr>";

                        DateTime? requiredDate = orderData.OrderDate;
                        htbl["@RequiredDate"] = String.Format("{0:dd/MM/yyyy}", requiredDate);

                        if (repMail != "")
                        {
                            subject = "Order placed by Sales Rep:" + RepName + " - Order for customer " + customer.CustomerName + ". Order reference # SCF-HD-HDS-" + model.OrderID.ToString();
                            bottomDetails += "<tr><td class='p-l'><i>Sales Rep: </i></td><td ><i style='color:red'>Order placed on customer behalf by sales rep: " + RepName + "</i></td></tr>";
                        }

                        htbl["@COMMENTDETAILS"] = bottomDetails;

                        string items = "";

                        double TotalPrice = 0;
                        double finalOrderAmount = 0;
                        var li = originalOrder;

                        foreach (var ci in li)
                        {
                            string prodImage = ci.ProductImages[0].ImageName;
                            bool checkImage = CheckIfImageExists(prodImage);

                            if (!checkImage)
                            {
                                prodImage = HelperClass.ApiURLLive + "images/no-product.png";
                            }

                            string procomment = ci.ProComment;

                            items += "<tr style='border-bottom:1px solid gray;' height='70'>" +
                                 "<td><img hspace='10' align='left' src='" + prodImage + "' alt='picsum' height='74' width='74' style='padding:5px 0' />" +
                                 "<span style='vertical-align:top;'>" + ci.ProductName;// + "')<br /><b>Code:</b> </span>";
                            items += "<br/><b>Code:</b> " + ci.ProductCode;

                            if (procomment != "")
                            {
                                items += "<br/><i style='color:red;'>" + procomment + "</i>";
                            }

                            items += "</span></td>";

                            items += "<td align='center'>" + ci.Quantity + "</td>";

                            if (ci.OrderUnitName != null && ci.OrderUnitName != "")
                            {
                                items += "<td  align='center'>" + ci.OrderUnitName + "</td>";
                            }
                            else
                            {
                                items += "<td  align='center'>" + ci.UnitName + "</td>";
                            }

                            items += "<td align='right'>$" + HelperClass.formatprice(Convert.ToString(ci.SpecialPrice)) + "</td>";

                            items += "<td align='right' class='p-r'>$" + HelperClass.formatprice(Convert.ToString((ci.Quantity * ci.SpecialPrice))) + "</td></tr>";

                            TotalPrice += Convert.ToDouble(ci.Quantity * ci.SpecialPrice);
                        }

                        htbl["@Products"] = items;

                        string orderAmountCalc = "";

                        finalOrderAmount = TotalPrice;
                        //double gst = TotalPrice * .1;
                        double gst = 0;

                        orderAmountCalc += "<tr><td align='right' width='80%'>Subtotal (" + li.Count.ToString() + " items):</td>" +
                                          "<td align='right' class='p-r'>$ " + HelperClass.formatprice(Convert.ToString(TotalPrice)) + "</td></tr>";

                        // Check for freight charges
                        if (orderData.HasFreightCharges != null && orderData.HasFreightCharges == true && orderData.IsDelivery == true)
                        {
                            var freight = _db.DeliveryCharges.FirstOrDefault().DeliveryFee;
                            orderAmountCalc += "<tr><td align='right'>Freight Charges:</td>" +
                                                "<td align='right' class='p-r'>$ " + HelperClass.formatprice(Convert.ToString(freight)) + "</td></tr>";

                            finalOrderAmount = (TotalPrice + freight).To<double>();
                        }

                        // check for promo codes
                        if (orderData.IsCouponApplied != null && orderData.IsCouponApplied == true)
                        {
                            orderAmountCalc += "<tr><td align='right'>Promo Code (" + orderData.Coupon + "):</td>" +
                                          "<td align='right' class='p-r'>-$ " + HelperClass.formatprice(Convert.ToString(orderData.CouponAmount)) + "</td></tr>";
                            finalOrderAmount = (finalOrderAmount - orderData.CouponAmount).To<double>();
                        }

                        orderAmountCalc += "<tr><td align='right'>Order Total:</td>" +
                                                "<td align='right' class='p-r'>$ " + HelperClass.formatprice(Convert.ToString(finalOrderAmount)) + "</td></tr>";

                        htbl["@orderAmountCal"] = orderAmountCalc;

                        //For customer login
                        string customername = (customer.CustomerName == null) ? "" : customer.CustomerName;

                        htbl["@CustomerName"] = customername;
                        htbl["@Email"] = cusEmail;

                        mailResult = SendMail.SendEMail(HelperClass.ToEmail, "", "", subject, HelperClass.TempltePath, "orderemailnew.html", htbl, "", HelperClass.FromEmail);

                        string mailResultcusEmail = "";
                        string mailResultrepMail = "";

                        // Method to sent the accounts email in case of payment
                        SendAccountsEMail(model.OrderID.ToString(), _db, customer, cusEmail, orderData);

                        if (HelperClass.SendCustomerAndRepMail)
                        {
                            mailResultcusEmail = SendMail.SendEMail(cusEmail, "", "", subject, HelperClass.TempltePath, "orderemailNew.html", htbl, "", HelperClass.FromEmail);

                            if (repMail != "")
                            {
                                htbl["@CustomName"] = RepName;
                                htbl["@customMessage"] = "You have placed the following sales order on behalf of your customer from the Mobile Ordering App:";

                                if (cusEmail != repMail)
                                {
                                    mailResultrepMail = SendMail.SendEMail(repMail, "", "", subject, HelperClass.TempltePath, "OrderEmailNew.html", htbl, "", HelperClass.FromEmail);
                                }
                            }

                            if (mailResultrepMail != "ok")
                            {
                                LogErrorsToServer(mailResultrepMail, model.OrderID.ToString(), "Mail:Error");
                            }
                            else if (mailResultcusEmail != "ok")
                            {
                                LogErrorsToServer(mailResultcusEmail, model.OrderID.ToString(), "Mail:Error");
                            }
                        }
                    }

                    // return "Success";
                    if (mailResult == "ok")
                    {
                        return "Success";
                    }
                    else
                    {
                        LogErrorsToServer(mailResult, model.OrderID.ToString(), "Mail:Error");
                        return mailResult;
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        private void SendAccountsEMail(string orderID, Saavi5Entities _db, CustomerMaster customer, string cusEmail, trnsCartMaster orderData)
        {
            try
            {
                if (orderData.StripeChargeID != null && orderData.StripeChargeID != "")
                {
                    var stripePayment = _db.StripePaymentDetails.Where(x => x.StripeChargeID == orderData.StripeChargeID).FirstOrDefault();

                    if (stripePayment != null)
                    {
                        // Add code to sent the stripe email
                        Hashtable payTbl = new Hashtable();
                        // Payment data
                        payTbl["@OrderId"] = "SCF-HD-" + orderID;
                        payTbl["@stripeChargeID"] = stripePayment.StripeChargeID;
                        payTbl["@captureAmount"] = HelperClass.formatprice(stripePayment.TotalCaptureAmount.ToString());
                        payTbl["@netAmount"] = HelperClass.formatprice(stripePayment.NetAmount.ToString());
                        payTbl["@totalFee"] = HelperClass.formatprice(stripePayment.TotalFees.ToString());
                        payTbl["@tax"] = HelperClass.formatprice(stripePayment.Tax.ToString());
                        payTbl["@stripeFee"] = HelperClass.formatprice(stripePayment.ApplicationFees.ToString());

                        payTbl["@OrderDate"] = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime((orderData.CreatedDate == null) ? DateTime.Now : orderData.CreatedDate));
                        payTbl["@OrderTime"] = (Convert.ToDateTime((orderData.CreatedDate == null) ? DateTime.Now : orderData.CreatedDate)).ToString("t");
                        payTbl["@path"] = HelperClass.ApiURLLive;
                        payTbl["@Company"] = HelperClass.Company;

                        // Customer Info // need to check addressed based on the type of selection
                        bool added = false;
                        if (orderData.AddressID != null && orderData.AddressID > 0)
                        {
                            var addr = _db.CustomerDeliveryAddresses.Find(orderData.AddressID);
                            payTbl["@custName"] = customer.CustomerName;
                            payTbl["@SAddress"] = addr.Address1;
                            payTbl["@Suburb"] = addr.Suburb;
                            payTbl["@Postcode"] = addr.ZIP;
                            payTbl["@Email"] = cusEmail;
                            payTbl["@Phone"] = addr.Phone1;
                            added = true;
                        }

                        if (orderData.PickupID != null && orderData.PickupID > 0)
                        {
                            var addr = _db.DeliveryPickupMasters.Find(orderData.PickupID);
                            payTbl["@custName"] = customer.CustomerName;
                            payTbl["@SAddress"] = addr.PickupAddress;
                            payTbl["@Suburb"] = "";
                            payTbl["@Postcode"] = "";
                            payTbl["@Email"] = cusEmail;
                            payTbl["@Phone"] = customer.Phone1;
                            added = true;
                        }

                        if (!added)
                        {
                            payTbl["@custName"] = customer.CustomerName;
                            payTbl["@SAddress"] = customer.Address1;
                            payTbl["@Suburb"] = customer.Suburb;
                            payTbl["@Postcode"] = customer.PostCode;
                            payTbl["@Email"] = cusEmail;
                            payTbl["@Phone"] = customer.Phone1;
                        }
                        string sub = "Payment details for Order SCF-HD-" + orderID;
                        string mailRes = SendMail.SendEMail(HelperClass.AccountsEmail, "", "", sub, HelperClass.TempltePath, "PaymentEmail.html", payTbl, "", HelperClass.FromEmail);
                        if (mailRes != "ok")
                        {
                            LogErrorsToServer(mailRes, orderID, "Error while sending Accounts email");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorsToServer(ex.Message, orderID, "Error in Account email sending function");
            }
        }

        private static string PaymentDetails(long orderID, Saavi5Entities _db)
        {
            var paymentData = _db.PaymentMasters.Where(s => s.CartID == orderID).FirstOrDefault();

            var payStatus = "";
            if (paymentData != null)
            {
                if (paymentData.IsStripePaymentSuccess == true)
                {
                    payStatus = "Paid in Full";
                }
                else if (paymentData.IsStripeAuthorisationSuccess == true)
                {
                    payStatus = "<span style=\"color: #f00;\">Authorized - Yet to be Charged</span>";
                }
                else
                {
                    payStatus = "Payment Failed";
                }
            }
            else
            {
                payStatus = "Payment Failed";
            }

            return payStatus;
        }

        private static string DeliveryNotes(Saavi5Entities _db, string postcode)
        {
            string delnotes = "Standard: Next Day Delivery";
            if (!string.IsNullOrEmpty(postcode))
            {
                try
                {
                    var post = _db.ImportPostcodes.Where(p => p.C24Hours == postcode || p.C48Hour == postcode || p.Exclude == postcode).FirstOrDefault();

                    if (post != null)
                    {
                        if (post.C48Hour != null && post.C48Hour == postcode)
                        {
                            delnotes = "Regional: 48 Hours Delivery";
                        }
                    }
                }
                catch (Exception)
                {
                    delnotes = "";
                }
            }
            else
            {
                delnotes = "";
            }

            return delnotes;
        }

        #endregion Order files / Email Method

        #region order file Helper

        /// <summary>
        /// Check if the url is valid
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool CheckIfImageExists(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "HEAD";

            bool exists;
            try
            {
                request.GetResponse();
                exists = true;
            }
            catch
            {
                exists = false;
            }
            return exists;
        }

        public string addCommas(int length)
        {
            string c = string.Empty;
            for (int i = 0; i < length - 1; i++)
            {
                c += ",";
            }
            return c;
        }

        public static string CleanInvalidXmlChars(string text)
        {
            text = text.Replace("<", "&lt;");
            text = text.Replace(">", "&gt;");
            text = text.Replace("&", "&amp;");
            text = text.Replace("'", "&apos;");
            text = text.Replace("\"", "&quot;");
            return text;
        }

        public DateTime getrequireddate(string OrderDate)
        {
            using (var _db = new Saavi5Entities())
            {
                var notice = _db.NoticeMasters.FirstOrDefault();
                DateTime requiredDate = DateTime.Now.AddDays(1);
                if (notice != null)
                {
                    string CutOffTime = notice.OrderCutOff;
                    DateTime ordate;
                    bool isorderdateselected = false;
                    if (DateTime.TryParse(OrderDate, out ordate))
                    {
                        if (ordate.Date > DateTime.Now.Date)
                        {
                            requiredDate = ordate;
                            isorderdateselected = true;
                        }
                    }
                    DateTime t1 = Convert.ToDateTime(DateTime.Now);
                    DateTime t2 = Convert.ToDateTime(CutOffTime);

                    if (t1.TimeOfDay.Ticks < t2.TimeOfDay.Ticks && !isorderdateselected)
                    {
                        requiredDate = DateTime.Now;
                    }
                    else
                    {
                        int dct = 0;
                        bool isDayAvailable = true;
                    getrequired:
                        foreach (var dayData in _db.PermittedDaysMasters.Where(s => s.IsActive == false))
                        {
                            if (string.Compare(dayData.DayName, requiredDate.DayOfWeek.ToString(), true) == 0)
                            {
                                requiredDate = requiredDate.AddDays(1);
                                dct++;
                                if (dct <= 7)
                                {
                                    goto getrequired;
                                }
                                else
                                {
                                    isDayAvailable = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                return requiredDate;
            }
        }

        #endregion order file Helper

        #region Append existing orders to cart while Reorder

        public async Task<APiResponse> AppendOrderToTempCart(ReorderBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (model.AppendToSavedOrder)
                    {
                        // existing saved order items
                        var savedOrderItems = _db.trnsTempCartItemsMasters.Where(t => t.CartID == model.OrderID).ToList();

                        // check for existing tempcart for customer
                        var tempCart = _db.trnsTempCartMasters.Where(c => c.CustomerID == model.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == false && c.IsOrderPlpacedByRep == model.IsPlacedByRep).FirstOrDefault();

                        List<trnsTempCartItemsMaster> liTemp = new List<trnsTempCartItemsMaster>();

                        // create a temp cart if it doesn't exist
                        if (tempCart == null)
                        {
                            trnsTempCartMaster tc = new trnsTempCartMaster();
                            tc.CustomerID = model.CustomerID;
                            tc.RepUserID = model.UserID;
                            tc.CreatedDate = DateTime.Now;
                            tc.IsOrderPlpacedByRep = model.IsPlacedByRep;
                            tc.IsSavedOrder = false;

                            _db.trnsTempCartMasters.Add(tc);
                            _db.SaveChanges();

                            Mapper.Map(savedOrderItems, liTemp);

                            // add new tempcart id to the existing Saved order items
                            foreach (var item in liTemp)
                            {
                                item.CartID = tc.CartID;
                            }

                            // add all the Saved order items to Empty temp cart
                            List<trnsTempCartItemsMaster> tempCartItems = new List<trnsTempCartItemsMaster>();
                            foreach (var tempItem in liTemp)
                            {
                                var ti = _db.trnsTempCartItemsMasters.Where(t => t.CartID == tempItem.CartID && t.ProductID == tempItem.ProductID).FirstOrDefault();

                                if (ti == null)
                                {
                                    tempCartItems.Add(tempItem);
                                }
                            }

                            _db.trnsTempCartItemsMasters.AddRange(tempCartItems);
                            _db.SaveChanges();

                            return ReturnMethod(SaaviErrorCodes.Success, "Success");
                        }
                        else
                        {
                            liTemp = _db.trnsTempCartItemsMasters.Where(t => t.CartID == tempCart.CartID).ToList();
                            List<trnsTempCartItemsMaster> liTempDistinct = new List<trnsTempCartItemsMaster>();

                            if (liTemp.Count > 0)
                            {
                                // Insert temp cart Items Producs to the ProductID List
                                model.Products = liTemp.Select(s => s.ProductID).ToList();

                                // Insert distinct saved order items to the New liTempDistinct List
                                liTempDistinct = savedOrderItems.Where(o => !model.Products.Contains(o.ProductID)).ToList();

                                foreach (var tempItem in liTempDistinct)
                                {
                                    tempItem.CartID = tempCart.CartID;
                                }

                                liTempDistinct.AddRange(liTemp);

                                List<trnsTempCartItemsMaster> tempCartItems = new List<trnsTempCartItemsMaster>();
                                foreach (var tempItem in liTempDistinct)
                                {
                                    var ti = _db.trnsTempCartItemsMasters.Where(t => t.CartID == tempItem.CartID && t.ProductID == tempItem.ProductID).FirstOrDefault();

                                    if (ti == null)
                                    {
                                        tempCartItems.Add(tempItem);
                                    }
                                }

                                _db.trnsTempCartItemsMasters.AddRange(tempCartItems);
                                _db.SaveChanges();
                            }

                            if (liTemp.Count == 0)
                            {
                                List<trnsTempCartItemsMaster> tempCartItems = new List<trnsTempCartItemsMaster>();
                                foreach (var item in savedOrderItems)
                                {
                                    var ti = _db.trnsTempCartItemsMasters.Where(t => t.CartID == tempCart.CartID && t.ProductID == item.ProductID).FirstOrDefault();

                                    if (ti == null)
                                    {
                                        item.CartID = tempCart.CartID;
                                        tempCartItems.Add(item);
                                    }
                                }
                                _db.trnsTempCartItemsMasters.AddRange(tempCartItems);
                                _db.SaveChanges();
                            }
                            return ReturnMethod(SaaviErrorCodes.Success, "Success");
                        }
                    }
                    else
                    {
                        var orderItems = _db.trnsCartItemsMasters.Where(t => t.CartID == model.OrderID).ToList();

                        if (orderItems != null)
                        {
                            List<trnsTempCartItemsMaster> liTemp = new List<trnsTempCartItemsMaster>();
                            trnsTempCartMaster tc = new trnsTempCartMaster();

                            var tempCart = _db.trnsTempCartMasters.Where(c => c.CustomerID == model.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == false && c.IsOrderPlpacedByRep == model.IsPlacedByRep).ToList();

                            if (tempCart == null || tempCart.Count == 0)
                            {
                                tc.CustomerID = model.CustomerID;
                                tc.RepUserID = model.UserID;
                                tc.CreatedDate = DateTime.Now;
                                tc.IsOrderPlpacedByRep = model.IsPlacedByRep;
                                tc.IsSavedOrder = false;

                                _db.trnsTempCartMasters.Add(tc);

                                _db.SaveChanges();
                            }

                            if (model.Products.Count > 0)
                            {
                                orderItems = orderItems.Where(l => model.Products.Contains(l.ProductID)).ToList();
                            }

                            Mapper.Map(orderItems, liTemp);

                            foreach (var item in liTemp)
                            {
                                var prod = _db.ProductMasters.Find(item.ProductID);
                                if (prod != null)
                                {
                                    var prices = GetPrice(model.CustomerID, item.ProductID, prod.Price.To<double>(), item.UnitId.To<long>());
                                    var qty = _db.AltUOMMasters.Where(a => a.ProductID == item.ProductID && a.UOMID == item.UnitId).FirstOrDefault();

                                    if (qty != null)
                                    {
                                        item.Price = prices.Price * (qty.QtyPerUnitOfMeasure ?? 1);
                                    }
                                    else
                                    {
                                        item.Price = prices.Price;
                                    }

                                    item.IsSpecialPrice = prices.IsSpecial;
                                }
                                item.CartID = tempCart.Count > 0 ? tempCart[0].CartID : tc.CartID;
                            }

                            var distinctItems = new List<trnsTempCartItemsMaster>();
                            if (tempCart.Count > 0)
                            {
                                List<long> productID = new List<long>();
                                var cartID = tempCart[0].CartID;
                                var tempCartOld = _db.trnsTempCartItemsMasters.Where(c => c.CartID == cartID).ToList();
                                foreach (var item in tempCartOld)
                                {
                                    productID.Add(item.ProductID);
                                }
                                distinctItems = liTemp.Where(t => !productID.Contains(t.ProductID)).ToList();

                                // Add only distinct items to the existing temp cart of user.
                                _db.trnsTempCartItemsMasters.AddRange(distinctItems);
                            }
                            else
                            {
                                _db.trnsTempCartItemsMasters.AddRange(liTemp);
                            }

                            await _db.SaveChangesAsync();

                            return ReturnMethod(SaaviErrorCodes.Success, "Success");
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Append existing orders to cart while Reorder

        #region Get count for cart items

        public APiResponse GetCartCount(CartCount model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    double? total = 0;
                    double? totalWithGST = 0;
                    if (model.CartID == 0)
                    {
                        var cart = _db.trnsTempCartMasters.Where(c => c.CustomerID == model.CustomerID && c.RepUserID == model.UserID && c.IsSavedOrder == model.IsSavedOrder && c.IsOrderPlpacedByRep == model.IsRepUser).FirstOrDefault();

                        if (cart != null)
                        {
                            var count = _db.trnsTempCartItemsMasters.Count(c => c.CartID == cart.CartID);

                            if (count > 0)
                            {
                                total = _db.trnsTempCartItemsMasters.Where(p => p.CartID == cart.CartID).Sum(s => s.Price * s.Quantity);
                                var t = (from a in _db.trnsTempCartItemsMasters
                                         join b in _db.ProductMasters on a.ProductID equals b.ProductID
                                         where a.CartID == cart.CartID
                                         select new
                                         {
                                             totalWithGST = (b.GST == true ? ((a.Price * .1) + a.Price) * a.Quantity : a.Price * a.Quantity)
                                         }).ToList().Sum(x => x.totalWithGST).Value;
                                totalWithGST = t;// ?? 0;
                            }

                            var couponAmount = UpdateCouponAmount(cart.CartID, _db, totalWithGST, cart);

                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                            {
                                Count = count,
                                CartTotal = total,
                                TotalWithGST = totalWithGST,
                                CouponAmount = couponAmount
                            });
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new { Count = 0, CartTotal = total, TotalWithGST = totalWithGST });
                        }
                    }
                    else
                    {
                        var count = _db.trnsTempCartItemsMasters.Count(c => c.CartID == model.CartID);

                        if (count > 0)
                        {
                            total = _db.trnsTempCartItemsMasters.Where(p => p.CartID == model.CartID).Sum(s => s.Price * s.Quantity);
                            var t = (from a in _db.trnsTempCartItemsMasters
                                     join b in _db.ProductMasters on a.ProductID equals b.ProductID
                                     where a.CartID == model.CartID
                                     select new
                                     {
                                         totalWithGST = (b.GST == true ? ((a.Price * .1) + a.Price) * a.Quantity : a.Price * a.Quantity)
                                     }).Sum(x => x.totalWithGST).Value;
                            totalWithGST = t;
                        }

                        var couponAmount = UpdateCouponAmount(model.CartID, _db, totalWithGST, null);

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                        {
                            Count = count,
                            CartTotal = total,
                            TotalWithGST = totalWithGST,
                            CouponAmount = couponAmount
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, "Error", new { Message = ex.Message });
            }
        }

        private double? UpdateCouponAmount(long cartID, Saavi5Entities _db, double? totalWithGST, trnsTempCartMaster cart = null)
        {
            if (cart == null)
            {
                cart = _db.trnsTempCartMasters.Find(cartID);
            }

            double? couponAmount = 0;
            if (cart.IsCouponApplied != null && cart.IsCouponApplied == true)
            {
                double? promoProductsAmount = 0;
                var promo = _db.PromoCodeMasters.Where(p => p.Code == cart.Coupon).FirstOrDefault();
                long[] pIDs = new long[] { 0 };

                if (promo.Products != null && promo.Products.Length > 0)
                {
                    pIDs = Array.ConvertAll(promo.Products.Split(','), long.Parse);

                    var cartitems = _db.trnsTempCartItemsMasters.Where(c => pIDs.Contains(c.ProductID) && c.CartID == cartID).ToList();

                    if (cartitems != null || cartitems.Count > 0)
                    {
                        promoProductsAmount = _db.trnsTempCartItemsMasters.Where(c => pIDs.Contains(c.ProductID) && c.CartID == cartID).Sum(s => s.Price * s.Quantity);
                        if (promo.CodeType == "Discount")
                        {
                            cart.CouponAmount = totalWithGST - (totalWithGST - ((promoProductsAmount * promo.Value) / 100));
                        }
                        else
                        {
                            cart.CouponAmount = totalWithGST - (totalWithGST - promo.Value);
                        }

                        couponAmount = cart.CouponAmount;
                        _db.SaveChanges();
                    }
                }
                else
                {
                    if (promo.CodeType == "Discount")
                    {
                        cart.CouponAmount = totalWithGST - (totalWithGST - ((totalWithGST * promo.Value) / 100));
                    }
                    else
                    {
                        cart.CouponAmount = totalWithGST - (totalWithGST - promo.Value);
                    }

                    couponAmount = cart.CouponAmount;
                    _db.SaveChanges();
                }
            }

            return couponAmount;
        }

        #endregion Get count for cart items

        #region Get App Discount

        public APiResponse GetAppDiscount()
        {
            using (var _db = new Saavi5Entities())
            {
                var discount = _db.Discounts.FirstOrDefault();

                if (discount != null)
                {
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();

                    bool allowAppDiscount = mainFeatures.AllowAppDiscount.To<bool>();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        DiscountValue = discount.DiscountValue,
                        IsPercentage = discount.IsPercentage,
                        AllowAppDiscount = allowAppDiscount,
                        MinCartValue = discount.MinCartValue ?? 0
                    });
                }
                else
                {
                    return ReturnMethod(SaaviErrorCodes.NoData, "No Discount Found.");
                }
            }
        }

        #endregion Get App Discount

        #region Get Profit Limit

        public APiResponse GetProfitLimit()
        {
            using (var _db = new Saavi5Entities())
            {
                var profitLimit = _db.ProfitMarginLimits.FirstOrDefault();

                if (profitLimit != null)
                {
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();

                    bool enableProfitLimitAlert = mainFeatures.EnableProfitLimitAlert.To<bool>();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { ProfitLimitValue = profitLimit.ProfitLimitValue, EnableProfitLimitAlert = enableProfitLimitAlert });
                }
                else
                {
                    return ReturnMethod(SaaviErrorCodes.NoData, "No Record Found.");
                }
            }
        }

        #endregion Get Profit Limit

        #region Log Errors to server for order posting

        private void LogErrorsToServer(string ex, string cusOrderID, string type)
        {
            using (var _db = new Saavi5Entities())
            {
                trnsCartResponseMaster cr = new trnsCartResponseMaster();
                cr.CartId = cusOrderID.To<long>();
                cr.MethodName = type;
                cr.Response = ex;
                cr.CreatedDate = DateTime.Now;

                _db.trnsCartResponseMasters.Add(cr);
                _db.SaveChanges();
            }
        }

        #endregion Log Errors to server for order posting

        #region Get prices

        private PriceBO GetPrice(long customerId, long productId, double Price, Int64 UOMID, CustomerMaster customer = null)
        {
            using (var _db = new Saavi5Entities())
            {
                PriceBO pricedata = new PriceBO();

                if (customer == null)
                {
                    customer = _db.CustomerMasters.Where(s => s.CustomerID == customerId).FirstOrDefault();
                }
                if (customer == null)
                {
                    pricedata.Price = Price;
                }
                else
                {
                    DateTime date = HelperClass.GetCurrentTime();
                    var special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == UOMID).FirstOrDefault();
                    if (special == null)
                    {
                        special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == null).FirstOrDefault();
                    }

                    double prc = Price;

                    double PricefromPriceCode = HelperClass.GetProductPrice(productId, customer.PRICE_CODE.To<int>());
                    if (PricefromPriceCode > 0)
                    {
                        prc = PricefromPriceCode;
                        pricedata.IsPromotional = true;
                    }
                    else
                    {
                        prc = Price;
                        pricedata.IsPromotional = false;
                        pricedata.Price = prc;
                    }

                    if (special != null && special.SpecialPrice != null)
                    {
                        double sprc = Convert.ToDouble(special.SpecialPrice);

                        pricedata.IsSpecial = true;
                        pricedata.Price = sprc;
                    }
                    pricedata.Price = prc;
                }
                return pricedata;
            }
        }

        #endregion Get prices

        #region Apply Discount Coupon

        public APiResponse ApplyCoupon(CouponBo model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var cartCountModel = new CartCount()
                    {
                        UserID = model.UserID,
                        CustomerID = model.CustomerID,
                        CartID = model.CartID,
                        IsRepUser = false,
                        IsSavedOrder = false
                    };
                    var res = this.GetCartCount(cartCountModel).Result;

                    var cartData = JsonConvert.DeserializeObject<CartRes>(JsonConvert.SerializeObject(res));
                    var coupon = model.Coupon.ToUpper();
                    var tempCart = _db.trnsTempCartMasters.Find(model.CartID);
                    double? promoProductsAmount = 0;

                    var promo = _db.PromoCodeMasters.Where(p => p.Code == coupon).FirstOrDefault();

                    if (promo == null)
                    {
                        return CouponReturn(cartData, "That particular promo code is not valid. Please try again.", cartData.TotalWithGST);
                    }
                    else
                    {
                        /// Check if the coupon is already applied or not.
                        if (tempCart.Coupon != null && tempCart.Coupon == coupon)
                        {
                            return CouponReturn(cartData, "Sorry, you have already applied that promo code.", cartData.TotalWithGST);
                        }

                        // check if there are any items in the cart and proceed accordingly
                        var cItems = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.CartID).ToList();
                        if (cItems == null || cItems.Count == 0)
                        {
                            return CouponReturn(cartData, "Sorry you do not have any items in your cart to apply a promo code. Please add at least one item.", cartData.TotalWithGST);
                        }

                        // Check if the products selected in the promocode setting exist and continue if they do else return
                        long[] pIDs = new long[] { 0 };

                        if (promo.Products != null && promo.Products.Length > 0)
                        {
                            pIDs = Array.ConvertAll(promo.Products.Split(','), long.Parse);

                            var cartitems = _db.trnsTempCartItemsMasters.Where(c => pIDs.Contains(c.ProductID) && c.CartID == model.CartID).ToList();

                            if (cartitems == null || cartitems.Count == 0)
                            {
                                return CouponReturn(cartData, "Sorry, the promo code for which this product applies to is not in your cart. Please add the correct product to the cart in order to use this promo code.", cartData.TotalWithGST);
                            }
                            else
                            {
                                promoProductsAmount = _db.trnsTempCartItemsMasters.Where(c => pIDs.Contains(c.ProductID) && c.CartID == model.CartID).Sum(s => s.Price * s.Quantity);
                            }
                        }

                        /// Check is the customer is valid
                        if (promo.CustomerID > 0)
                        {
                            if (promo.CustomerID != model.CustomerID)
                            {
                                return CouponReturn(cartData, "Sorry, this promo code is valid for a different customer. It can not be redeemed by you", cartData.TotalWithGST);
                            }
                        }

                        // check if the coupon has exceeded its limit
                        if (promo.Redemptions != null && promo.Redemptions > 0)
                        {
                            int count = 0;
                            if (promo.CustomerID > 0)
                            {
                                count = _db.trnsCartMasters.Count(c => c.Coupon == promo.Code && c.CustomerID == promo.CustomerID);

                                if (count > promo.Redemptions)
                                {
                                    return CouponReturn(cartData, "Sorry this promo code has reached its maximum allowable redemptions. Please try another code", cartData.TotalWithGST);
                                }
                            }
                            else
                            {
                                count = _db.trnsCartMasters.Count(c => c.Coupon == promo.Code);

                                if (count > promo.Redemptions)
                                {
                                    return CouponReturn(cartData, "Sorry this promo code has reached its maximum allowable redemptions. Please try another code", cartData.TotalWithGST);
                                }
                            }
                        }

                        // if the above condition is satisfied then continue
                        if (promo.StartDate.To<DateTime>().Date <= DateTime.Now.Date && promo.EndDate.To<DateTime>().Date >= DateTime.Now.Date)
                        {
                            // check if the limit 1 per customer is applicable and return
                            var existingCount = _db.trnsCartMasters.Count(x => x.CustomerID == model.CustomerID && x.Coupon == promo.Code);
                            if (existingCount >= 1 && promo.Limit1PerCustomer == true)
                            {
                                return CouponReturn(cartData, "Sorry you have already used this promo code before. Only 1 promo code is permitted per customer. Please try another code", cartData.TotalWithGST);
                            }

                            double? discountedValue = 0;

                            if (promo.CodeType.ToLower() == "value")
                            {
                                discountedValue = cartData.TotalWithGST - promo.Value;
                            }
                            else
                            {
                                if (promoProductsAmount > 0)
                                {
                                    discountedValue = cartData.TotalWithGST - ((promoProductsAmount * promo.Value) / 100);
                                }
                                else
                                {
                                    discountedValue = cartData.TotalWithGST - ((cartData.TotalWithGST * promo.Value) / 100);
                                }
                            }

                            if (promo.MinCartValue != null && promo.MinCartValue > 0)
                            {
                                if (cartData.TotalWithGST >= promo.MinCartValue)
                                {
                                    tempCart.Coupon = coupon;
                                    tempCart.IsCouponApplied = true;
                                    tempCart.CouponAmount = cartData.TotalWithGST - discountedValue;

                                    if (tempCart.CouponAmount > cartData.TotalWithGST)
                                    {
                                        return CouponReturn(cartData, "Sorry, this promo code requires a minimum cart value of $" + promo.Value.ToString() + ". Please add more items to the cart in order to redeem this promo code", discountedValue);
                                    }
                                    _db.SaveChanges();

                                    return CouponReturn(cartData, "Your promo code was applied successfully.", discountedValue);
                                }
                                else
                                {
                                    return CouponReturn(cartData, "Sorry, this promo code requires a minimum cart value of $" + promo.MinCartValue.ToString() + ". Please add more items to the cart in order to redeem this promo code", cartData.TotalWithGST);
                                }
                            }

                            // update the temo cart with the coupon
                            tempCart.Coupon = coupon;
                            tempCart.IsCouponApplied = true;
                            tempCart.CouponAmount = cartData.TotalWithGST - discountedValue;

                            if (tempCart.CouponAmount > cartData.TotalWithGST)
                            {
                                return CouponReturn(cartData, "Sorry, this promo code requires a minimum cart value of $" + promo.Value.ToString() + ". Please add more items to the cart in order to redeem this promo code", discountedValue);
                            }

                            _db.SaveChanges();

                            return CouponReturn(cartData, "Your promo code was applied successfully.", discountedValue);
                        }
                        else
                        {
                            return CouponReturn(cartData, "Sorry, that promo code has expired.", cartData.TotalWithGST);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, "Error", new { Message = ex.Message });
            }
        }

        private APiResponse CouponReturn(CartRes cartData, string message, double? discountValue)
        {
            return ReturnMethod(SaaviErrorCodes.Success, "Success", new
            {
                Message = message,
                Count = cartData.Count,
                CartTotal = cartData.CartTotal,
                TotalWithGST = cartData.TotalWithGST,
                TotalAmountWithDiscount = discountValue
            });
        }

        #endregion Apply Discount Coupon

        #region Send Order Payment Link

        public APiResponse SendOrderPaymentLink(OrderPaymentLinkRequestModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var user = _db.UserMasters.Where(x => x.UserID == model.UserID && x.CustomerID == model.CustomerID).FirstOrDefault();

                    if (user == null)
                    {
                        return ReturnMethod(SaaviErrorCodes.NotFound, "User details not found!");
                    }

                    var tokenRecord = _db.UserPaymentLinkTokenMasters.Where(x => x.UserId == user.UserID).FirstOrDefault();

                    string token = "";
                    if (tokenRecord != null)
                    {
                        token = tokenRecord.Token.ToString();
                    }
                    else
                    {
                        token = Guid.NewGuid().ToString();

                        UserPaymentLinkTokenMaster tokRec = new UserPaymentLinkTokenMaster()
                        {
                            UserId = user.UserID,
                            Token = token
                        };
                        _db.UserPaymentLinkTokenMasters.Add(tokRec);
                        _db.SaveChanges();
                    }
                    string link = HelperClass.WebViewURL + "/Main/Payment?token=" + HelperClass.EncodeBase64(token);

                    Hashtable hstbl = new Hashtable();

                    hstbl["@Attention"] = user.FirstName ?? user.UserName;
                    hstbl["@PayLink"] = link;

                    hstbl["@path"] = HelperClass.WebsiteUrl;
                    hstbl["@Company"] = HelperClass.siteName;
                    string email = (model.Email == null || model.Email == "") ? user.Email : model.Email;
                    SendMail.SendEMail(email, "", "", "Payment Link - " + HelperClass.siteName, HelperClass.TempltePath, "paymentLink.htm", hstbl, "", HelperClass.FromEmail);

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        PaymentLink = link
                    });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, "Error", new { Message = ex.Message });
            }
        }

        #endregion Send Order Payment Link

        #region Save / Update Recurring orders for a customer

        public APiResponse ManageRecurringOrders(RecurringOrdersBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var reccur = new RecurringOrdersMaster();

                    if (model.ID > 0)
                    {
                        reccur = _db.RecurringOrdersMasters.Find(model.ID);
                        if (reccur != null)
                        {
                            reccur.ModifiedDate = DateTime.Now;
                            reccur.Frequency = model.Frequency;
                            reccur.StartDate = model.StartDate;
                            reccur.IsSuspended = model.IsSuspended;
                            reccur.AddressID = model.AddressID;
                            reccur.PickupID = model.PickupID;
                            reccur.OrderType = model.OrderType;

                            //Mapper.Map(model, reccur);
                            var recurrItems = new List<RecurringOrderDetailsMaster>();
                            foreach (var items in model.RecurrProds)
                            {
                                var detail = _db.RecurringOrderDetailsMasters.Where(x => x.RecurrID == model.ID && x.ProductID == items.ProductID).FirstOrDefault();
                                if (detail != null)
                                {
                                    if (detail.Quantity != items.Quantity)
                                    {
                                        detail.Quantity = items.Quantity;
                                        _db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    var rItem = new RecurringOrderDetailsMaster();
                                    rItem.RecurrID = reccur.ID;
                                    rItem.ProductID = items.ProductID;
                                    rItem.Quantity = items.Quantity;
                                    rItem.CreatedDate = DateTime.Now;

                                    recurrItems.Add(rItem);
                                }
                            }

                            if (recurrItems.Count > 0)
                            {
                                _db.RecurringOrderDetailsMasters.AddRange(recurrItems);
                            }

                            _db.SaveChanges();
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.Error, "Recurring order details not found for editing.");
                        }
                    }
                    else
                    {
                        reccur.CreatedDate = DateTime.Now;
                        reccur.Frequency = model.Frequency;
                        reccur.StartDate = model.StartDate;
                        reccur.IsSuspended = model.IsSuspended;
                        reccur.AddressID = model.AddressID;
                        reccur.PickupID = model.PickupID;
                        reccur.OrderType = model.OrderType;
                        reccur.CustomerID = model.CustomerID;

                        //Mapper.Map(model, reccur);
                        _db.RecurringOrdersMasters.Add(reccur);
                        _db.SaveChanges();

                        var recurrItems = new List<RecurringOrderDetailsMaster>();
                        foreach (var item in model.RecurrProds)
                        {
                            var rItem = new RecurringOrderDetailsMaster();
                            rItem.RecurrID = reccur.ID;
                            rItem.ProductID = item.ProductID;
                            rItem.Quantity = item.Quantity;
                            rItem.CreatedDate = DateTime.Now;

                            recurrItems.Add(rItem);
                        }

                        _db.RecurringOrderDetailsMasters.AddRange(recurrItems);
                        _db.SaveChanges();
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        ReccurID = reccur.ID
                    });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, "Error", new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Save / Update Recurring orders for a customer

        #region Get Recurring orders for a customer

        public APiResponse GetRecurringOrders(GetRecurringOrderBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var res = (from a in _db.RecurringOrdersMasters
                               where a.CustomerID == model.CustomerID
                               select new RecurringOrdersBO
                               {
                                   ID = a.ID,
                                   AddressID = a.AddressID ?? 0,
                                   CustomerID = a.CustomerID,
                                   Frequency = a.Frequency,
                                   IsSuspended = a.IsSuspended,
                                   OrderType = a.OrderType,
                                   PickupID = a.PickupID ?? 0,
                                   StartDate = a.StartDate,
                                   RecurrProds = (from b in _db.RecurringOrderDetailsMasters
                                                  join c in _db.ProductMasters on b.ProductID equals c.ProductID
                                                  where b.RecurrID == a.ID
                                                  select new RecurrProducts
                                                  {
                                                      RecurrProdID = b.ID,
                                                      ProductID = b.ProductID,
                                                      Quantity = b.Quantity,
                                                      ProductCode = c.ProductCode,
                                                      ProductName = c.ProductName,
                                                      Price = c.Price
                                                  }).ToList()
                               }).FirstOrDefault();

                    //_db.RecurringOrdersMasters.Where(x => x.CustomerID == model.CustomerID && x.IsSuspended == false).FirstOrDefault();
                    if (res != null)
                    {
                        var prodIDs = res.RecurrProds.Select(x => x.ProductID).ToList();
                        var remainingProds = (from a in _db.ProductMasters
                                              where !prodIDs.Contains(a.ProductID) && a.IsActive == true
                                              select new GetAllProductsBO
                                              {
                                                  ProductID = a.ProductID,
                                                  Price = a.Price,
                                                  ProductName = a.ProductName,
                                                  ProductCode = a.ProductCode
                                              }).ToList();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                        {
                            RecurringOrder = res,
                            AllProducts = remainingProds
                        });
                    }
                    else
                    {
                        var all = (from a in _db.ProductMasters
                                   where a.IsActive == true
                                   select new GetAllProductsBO
                                   {
                                       ProductID = a.ProductID,
                                       Price = a.Price,
                                       ProductName = a.ProductName,
                                       ProductCode = a.ProductCode
                                   }).ToList();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                        {
                            RecurringOrder = new RecurringOrdersBO()
                            {
                                AddressID = 0,
                                PickupID = 0,
                                ID = 0,
                                StartDate = DateTime.Now
                            },
                            AllProducts = all
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, "Error", new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Get Recurring orders for a customer

        #region Delete item from recurring order of a customer

        public APiResponse DeleteRecurrOrderItem(DeleteRecurrItemBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var item = _db.RecurringOrderDetailsMasters.Find(model.RecurrProdID);

                    if (item != null)
                    {
                        _db.RecurringOrderDetailsMasters.Remove(item);
                        _db.SaveChanges();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NotFound, "Item not found");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, "Error", new
                {
                    Message = ex.Message
                });
            }
        }

        #endregion Delete item from recurring order of a customer
    }
}