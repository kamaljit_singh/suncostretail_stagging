﻿using AutoMapper;
using Saavi5Api.DataAccess.BO;
using Saavi5Api.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonFunctions;
using System.Web.Hosting;
using System.IO;

namespace Saavi5Api.DataAccess.DA
{
    public class ProductDA
    {
        #region Common return method

        public APiResponse ReturnMethod(string responseCode, string message, object result = null)
        {
            return new APiResponse
            {
                ResponseCode = responseCode,
                Message = message,
                Result = result
            };
        }

        #endregion Common return method

        #region Get product categories and sub categories

        public async Task<APiResponse> GetProductCategories(bool showSubOnly, long customerId)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();
                    var ShowCategory = mainFeatures.ShowCategory.To<bool>();

                    var customer = _db.CustomerMasters.Where(s => s.CustomerID == customerId).FirstOrDefault();
                    string TagNames = customer != null ? Convert.ToString(customer.TagNames == null ? "" : customer.TagNames) : "";
                    bool IsSpecialCategory = customer != null ? Convert.ToBoolean(customer.IsSpecialCategory) : false;
                    string[] TagNamesArray = null;
                    TagNamesArray = TagNames.Split(new char[] { ',' });

                    bool showOnlyStockItems = false;
                    if (mainFeatures != null)
                    {
                        showOnlyStockItems = mainFeatures.IsShowOnlyStockItems.To<bool>();
                    }

                    List<FilterForCategory> query = (
                                      from prod in _db.ProductMasters
                                      join uom in _db.UnitOfMeasurementMasters on prod.UOMID equals uom.UOMID
                                      join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                      join mc in _db.MainCategoriesMasters on cat.MCategoryId equals mc.MCategoryId
                                      join pf in _db.ProductFiltersMasters on prod.FilterID equals pf.FilterID

                                      // left join on stock warehouse table.
                                      join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into sw
                                      from soh in sw.DefaultIfEmpty()

                                      where (prod.IsActive == true && cat.IsActive == true && mc.IsActive == true && pf.IsActive == true

                                             )
                                      select new FilterForCategory
                                      {
                                          CategoryID = prod.CategoryID,
                                          MainCategoryID = mc.MCategoryId,
                                          StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                      }).Where(s => showOnlyStockItems == false || s.StockQuantity > 0).Distinct().ToList();

                    long[] MCategory = query.Select(x => x.MainCategoryID).Distinct().ToList().ToArray();
                    long[] Category = query.Select(x => x.CategoryID).Distinct().ToList().ToArray();

                    var data = (IEnumerable<object>)null;
                    if (showSubOnly || ShowCategory == false)
                    {
                        data = await (from m in _db.MainCategoriesMasters
                                      join p in _db.ProductCategoriesMasters on m.MCategoryId equals p.MCategoryId
                                      where m.IsActive == true && p.IsActive == true && MCategory.Contains(m.MCategoryId) && Category.Contains(p.CategoryID) &&
                                      (p.IsSpecialCategory == false || p.IsSpecialCategory == null || (p.IsSpecialCategory == true && IsSpecialCategory == true && TagNamesArray.Contains(p.CategoryName)))
                                      orderby p.CategoryName ascending
                                      select new
                                      {
                                          m.MCategoryId,
                                          m.MCategoryName,
                                          m.MCategoryCode,
                                          p.CategoryID,
                                          p.CategoryName,
                                          p.IsSpecialCategory,
                                          p.CategoryCode
                                      }).OrderBy(p => p.CategoryCode).ToListAsync();
                    }
                    else if (ShowCategory)
                    {
                        data = await (from m in _db.MainCategoriesMasters
                                      where m.IsActive == true && MCategory.Contains(m.MCategoryId)
                                      orderby m.MCategoryName ascending
                                      select new
                                      {
                                          m.MCategoryId,
                                          m.MCategoryName,
                                          m.IsActive,
                                          m.MCategoryCode,
                                          SubCategories = (from p in _db.ProductCategoriesMasters
                                                           where p.MCategoryId == m.MCategoryId && Category.Contains(p.CategoryID) && p.IsActive == true
                                                           && (p.IsSpecialCategory == false || p.IsSpecialCategory == null || (p.IsSpecialCategory == true && IsSpecialCategory == true && TagNamesArray.Contains(p.CategoryName))

                                                           )
                                                           orderby p.CategoryName ascending
                                                           select new
                                                           {
                                                               p.MCategoryId,
                                                               p.CategoryID,
                                                               p.CategoryName,
                                                               p.IsSpecialCategory,
                                                               p.CategoryCode
                                                           }).OrderBy(p => p.CategoryCode).ToList()
                                      }).ToListAsync();
                    }
                    if (data.Count() > 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", data);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Records found");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get product categories and sub categories

        #region Get product Filters

        public async Task<APiResponse> GetProductFilters()
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var filters = await (from f in _db.ProductFiltersMasters
                                         where f.IsActive == true
                                         select new
                                         {
                                             f.FilterID,
                                             FilterName = f.FilterName.ToUpper()
                                         }).ToListAsync();

                    var item = new { FilterID = Convert.ToInt64(0), FilterName = "ALL" };
                    filters.Insert(0, item);

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", filters);
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get product Filters

        #region Search products by filters and paging.

        public async Task<APiResponse> GetProductsByFilters(ProductFilters searchFilters)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    long? userIDForFeatures = searchFilters.UserID;
                    if (searchFilters.IsRepUser)
                    {
                        userIDForFeatures = _db.UserMasters.Where(u => u.CustomerID == searchFilters.CustomerID).FirstOrDefault().UserID;
                    }
                    var customer = _db.CustomerMasters.Find(searchFilters.CustomerID);
                    var customerCatFilter = customer;
                    if (searchFilters.listCustomerId > 0)
                    {
                        customerCatFilter = _db.CustomerMasters.Find(searchFilters.listCustomerId);
                    }

                    string warehouse = customer.Warehouse ?? "";
                    var customerFeatures = _db.CustomerFeaturesMasters.Where(x => x.UserID == userIDForFeatures).FirstOrDefault();
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();

                    bool isDynamicUOM = false;
                    if (customerFeatures != null)
                    {
                        isDynamicUOM = customerFeatures.IsDynamicUOM.To<bool>();
                    }

                    bool showOnlyStockItems = false;
                    bool returnLastOrderUOM = false;
                    if (mainFeatures != null)
                    {
                        showOnlyStockItems = mainFeatures.IsShowOnlyStockItems.To<bool>();
                        returnLastOrderUOM = mainFeatures.ReturnLastOrderUOM.To<bool>();
                    }
                    string TagNames = customerCatFilter != null ? Convert.ToString(customerCatFilter.TagNames == null ? "" : customerCatFilter.TagNames) : "";
                    bool IsSpecialCategory = customerCatFilter != null ? Convert.ToBoolean(customerCatFilter.IsSpecialCategory) : false;
                    string[] TagNamesArray = null;

                    TagNamesArray = TagNames.Split(new char[] { ',' });

                    if (!searchFilters.IsSpecial)
                    {
                        List<Int64> productCategories = new List<Int64>();

                        foreach (ProductCategoriesMaster msubcats in _db.ProductCategoriesMasters.Where(s => s.MCategoryId == searchFilters.MainCategoryID && s.IsActive == true))
                        {
                            productCategories.Add(msubcats.CategoryID);
                        }

                        //check if the filters return any data. If yes then proceed further.
                        ProductMaster filters = await _db.ProductMasters.Where(s => (s.CategoryID == searchFilters.SubCategoryID || searchFilters.SubCategoryID == 0 || searchFilters.SubCategoryID == -1)
                                                                && (productCategories.Contains(s.CategoryID) || searchFilters.MainCategoryID == 0)
                                                                && (s.FilterID == searchFilters.FilterID || searchFilters.FilterID == 0)
                                                                && (s.ProductName.Contains(searchFilters.Searchtext) || s.ProductDescription.Contains(searchFilters.Searchtext) || s.ProductCode.Contains(searchFilters.Searchtext) || s.BarCodeText.Contains(searchFilters.Searchtext) || s.BarcodeGTIN.Contains(searchFilters.Searchtext))
                                                                && s.IsActive == true).FirstOrDefaultAsync();

                        if (filters != null)
                        {
                            IOrderedQueryable<SearchProductsBO> query;

                            query = (
                                from prod in _db.ProductMasters
                                join uom in _db.UnitOfMeasurementMasters on prod.UOMID equals uom.UOMID
                                join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                join mc in _db.MainCategoriesMasters on cat.MCategoryId equals mc.MCategoryId
                                join pf in _db.ProductFiltersMasters on prod.FilterID equals pf.FilterID

                                // left join on stock warehouse table.
                                join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into sw
                                from soh in sw.DefaultIfEmpty()
                                    //Left join with AlternateUom master
                                    //join au in _db.AltUOMMasters on new { UOM = uom.UOMID.ToString(), Product = p.ProductID.ToString() } equals new { UOM = au.UOMID.ToString(), Product = au.ProductID.ToString() } into auTemp
                                    //from au in auTemp.DefaultIfEmpty()

                                where ((prod.CategoryID == searchFilters.SubCategoryID || searchFilters.SubCategoryID == 0 || searchFilters.SubCategoryID == -1)
                                       && (productCategories.Contains(prod.CategoryID) || searchFilters.MainCategoryID == 0)
                                       && (prod.FilterID == searchFilters.FilterID || searchFilters.FilterID == 0)
                                       && (searchFilters.Searchtext == "" | prod.ProductName.Contains(searchFilters.Searchtext) || prod.ProductDescription.Contains(searchFilters.Searchtext)
                                            || prod.ProductCode.Contains(searchFilters.Searchtext) || cat.CategoryName.Contains(searchFilters.Searchtext)
                                            || prod.BarCodeText.Contains(searchFilters.Searchtext) || prod.BarcodeGTIN.Contains(searchFilters.Searchtext))

                                       && prod.IsActive == true && cat.IsActive == true && mc.IsActive == true && pf.IsActive == true
                                       && (warehouse == "" || (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse))
                                       && (((cat.IsSpecialCategory == false || cat.IsSpecialCategory == null) || (cat.IsSpecialCategory == true && IsSpecialCategory == true && TagNamesArray.Contains(cat.CategoryName)))))
                                select new SearchProductsBO
                                {
                                    ProductID = prod.ProductID,
                                    ProductName = prod.ProductName,
                                    ProductCode = prod.ProductCode,
                                    Description = prod.ProductDescription ?? "",

                                    Description2 = prod.ProductDescription2 ?? "",
                                    Description3 = prod.ProductDescription3 ?? "",
                                    Feature1 = prod.ProductFeature1 ?? "",
                                    Feature2 = prod.ProductFeature2 ?? "",
                                    Feature3 = prod.ProductFeature3 ?? "",
                                    IsNew = prod.IsNew ?? false,
                                    IsOnSale = prod.IsOnSale ?? false,
                                    IsBackSoon = prod.IsBackSoon ?? false,
                                    Barcode = prod.BarCodeText,

                                    ProductImage = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + prod.ProductImage : "",
                                    ProductImageThumb = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + "thumb_" + prod.ProductImage : "",
                                    UOMID = uom.UOMID,
                                    UOMDesc = uom.Code,
                                    SellBelowCost = prod.SellBelowCost ?? false,
                                    FilterID = prod.FilterID,
                                    FilterName = pf.FilterName,
                                    CategoryName = cat.CategoryName,
                                    MainCategoryName = mc.MCategoryName,
                                    CategoryID = prod.CategoryID,
                                    Price = prod.Price ?? 0,
                                    CompanyPrice = prod.ComPrice ?? 0,
                                    IsActive = prod.IsActive,
                                    BuyIn = prod.BuyIn ?? false,
                                    IsStatusIN = (prod.ERPStatus == "IN") ? true : false,
                                    IsAvailable = prod.IsAvailable ?? false,
                                    UnitsPerCarton = prod.UnitsPerCarton == null ? 1 : prod.UnitsPerCarton,
                                    PiecesAndWeight = prod.PiecesAndWeight ?? false,// Convert.ToBoolean((prod.PiecesAndWeight == null) ? false : prod.PiecesAndWeight),
                                    IsCountrywideRewards = prod.RewardItem ?? false,
                                    //stockwarehouse
                                    IsGST = prod.GST ?? false,
                                    IsSpecialCategory = cat.IsSpecialCategory ?? false,
                                    StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                    Brand = prod.Brand == null ? "" : prod.Brand,
                                    Supplier = prod.Supplier == null ? "" : prod.Supplier.Trim(),
                                    MinOQ = prod.MinOQ ?? 0,
                                    MaxOQ = prod.MaxOQ ?? 0,
                                    ProductImages = (from a in _db.ProductImageMasters
                                                     where a.ProductID == prod.ProductID
                                                     select new ProductImages
                                                     {
                                                         ImageName = a.BaseUrl + a.ProductImage
                                                     }).ToList(),
                                    ShareURL = prod.ShareURL ?? "",
                                    ProductFeature = prod.ProductFeature ?? "",
                                    IsDonationBox = prod.IsDonationBox ?? false
                                }).Where(s => showOnlyStockItems == false || s.StockQuantity > 0).OrderBy(p => p.ProductID);

                            var result = query.OrderByDescending(x => x.Price).Skip(searchFilters.PageIndex * searchFilters.PageSize).Take(searchFilters.PageSize).ToList(); //Page(searchFilters.PageIndex, searchFilters.PageSize).ToList();

                            #region Code to update IsInPantry, IsInCart and Prices data for products

                            result = UpdateIsInPantryAndIsInCart(searchFilters, result, isDynamicUOM, _db, searchFilters.UserID.To<long>(), returnLastOrderUOM).ToList();

                            #endregion Code to update IsInPantry, IsInCart and Prices data for products

                            return new APiResponse
                            {
                                ResponseCode = SaaviErrorCodes.Success,
                                Message = "Success",
                                Result = new { products = result, TotalResults = query.Count(), TotalPages = (query.Count() + searchFilters.PageSize - 1) / searchFilters.PageSize }
                            };
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                        }
                    }
                    else
                    {
                        SpecialPriceMaster sPrice = await _db.SpecialPriceMasters.Where(s => s.CustomerID == searchFilters.CustomerID).FirstOrDefaultAsync();

                        if (sPrice != null)
                        {
                            IOrderedQueryable<SearchProductsBO> query;

                            query = (from sp in _db.SpecialPriceMasters
                                     join pro in _db.ProductMasters on sp.ProductID equals pro.ProductID
                                     join filter in _db.ProductFiltersMasters on pro.FilterID equals filter.FilterID
                                     join cats in _db.ProductCategoriesMasters on pro.CategoryID equals cats.CategoryID
                                     join mcats in _db.MainCategoriesMasters on cats.MCategoryId equals mcats.MCategoryId
                                     join unit in _db.UnitOfMeasurementMasters on pro.UOMID equals unit.UOMID

                                     //join rwunit in FT.mstUnitOfMeasurements on pro.RW_UOMID equals rwunit.UOMID into temp3from rwunit in temp3.DefaultIfEmpty()
                                     join soh in _db.StockWareHouses on pro.ProductID equals soh.ProductID into temp6
                                     from soh in temp6.DefaultIfEmpty()

                                     where (
                                     sp.CustomerID == searchFilters.CustomerID
                                     && (pro.FilterID == searchFilters.FilterID || searchFilters.FilterID == 0)
                                     && (pro.ProductName.Contains(searchFilters.Searchtext) || pro.ProductDescription.Contains(searchFilters.Searchtext)
                                     || pro.ProductCode.Contains(searchFilters.Searchtext) || cats.CategoryName.Contains(searchFilters.Searchtext))
                                     && pro.IsActive == true && cats.IsActive == true && mcats.IsActive == true && filter.IsActive == true
                                     && (warehouse == "" || (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse))
                                     && sp.IsActive == true && ((cats.IsSpecialCategory == false || cats.IsSpecialCategory == null) || (cats.IsSpecialCategory == true && IsSpecialCategory == true && TagNamesArray.Contains(cats.CategoryName)))
                                     )
                                     orderby (pro.ProductName)
                                     select new SearchProductsBO
                                     {
                                         ProductID = pro.ProductID,
                                         ProductName = pro.ProductName,
                                         ProductCode = pro.ProductCode,
                                         Description = pro.ProductDescription == null ? "" : pro.ProductDescription,
                                         Description2 = pro.ProductDescription2 ?? "",
                                         Description3 = pro.ProductDescription3 ?? "",
                                         Feature1 = pro.ProductFeature1 ?? "",
                                         Feature2 = pro.ProductFeature2 ?? "",
                                         Feature3 = pro.ProductFeature3 ?? "",
                                         IsNew = pro.IsNew ?? false,
                                         IsOnSale = pro.IsOnSale ?? false,
                                         IsBackSoon = pro.IsBackSoon ?? false,
                                         Barcode = pro.BarCodeText,
                                         ProductImage = (pro.ProductImage != null && pro.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + pro.ProductImage : "",
                                         ProductImageThumb = (pro.ProductImage != null && pro.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + "thumb_" + pro.ProductImage : "",
                                         UOMID = pro.UOMID,
                                         UOMDesc = unit.Code ?? "",
                                         FilterID = filter.FilterID,
                                         FilterName = filter.FilterName,
                                         CategoryName = cats.CategoryName,
                                         MainCategoryName = mcats.MCategoryName,
                                         BuyIn = pro.BuyIn ?? false,
                                         SellBelowCost = pro.SellBelowCost ?? false,
                                         IsStatusIN = (pro.ERPStatus == "IN") ? true : false,
                                         Price = Convert.ToDouble(pro.Price),
                                         CompanyPrice = Convert.ToDouble((pro.ComPrice == null) ? 0 : pro.ComPrice),
                                         SpecialPrice = Convert.ToDouble(sp.SpecialPrice),
                                         IsManagerApproved = Convert.ToBoolean((sp.IsManagerApproved == null) ? true : sp.IsManagerApproved),
                                         IsActive = pro.IsActive,
                                         IsSpecial = true,
                                         UnitsPerCarton = pro.UnitsPerCarton == null ? 1 : pro.UnitsPerCarton,
                                         IsAvailable = Convert.ToBoolean((pro.IsAvailable == null) ? false : pro.IsAvailable),
                                         StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                         IsGST = Convert.ToBoolean(pro.GST == null ? false : pro.GST),
                                         Brand = pro.Brand == null ? "" : pro.Brand,
                                         Supplier = pro.Supplier == null ? "" : pro.Supplier.Trim(),
                                         IsSpecialCategory = Convert.ToBoolean((cats.IsSpecialCategory == null) ? false : cats.IsSpecialCategory),
                                         IsCountrywideRewards = pro.RewardItem ?? false,
                                         MinOQ = pro.MinOQ ?? 0,
                                         MaxOQ = pro.MaxOQ ?? 0,
                                         ProductImages = (from a in _db.ProductImageMasters
                                                          where a.ProductID == pro.ProductID
                                                          select new ProductImages
                                                          {
                                                              ImageName = a.BaseUrl + a.ProductImage
                                                          }).ToList(),
                                         ShareURL = pro.ShareURL ?? "",
                                         ProductFeature = pro.ProductFeature ?? "",
                                         IsDonationBox = pro.IsDonationBox ?? false
                                     }).Where(s => showOnlyStockItems == false || s.StockQuantity > 0).OrderBy(p => p.ProductID);

                            var result = query.OrderByDescending(o => o.Price).Skip(searchFilters.PageIndex * searchFilters.PageSize).Take(searchFilters.PageSize).ToList(); // query.Page(searchFilters.PageSize, searchFilters.PageSize).ToList();

                            #region Code to update IsInPantry, IsInCart and Prices data for products

                            result = UpdateIsInPantryAndIsInCart(searchFilters, result, isDynamicUOM, _db, searchFilters.UserID.To<long>(), returnLastOrderUOM).ToList();

                            #endregion Code to update IsInPantry, IsInCart and Prices data for products

                            return new APiResponse
                            {
                                ResponseCode = SaaviErrorCodes.Success,
                                Message = "Success",
                                Result = new { products = result, TotalResults = query.Count(), TotalPages = (query.Count() + searchFilters.PageSize - 1) / searchFilters.PageSize }
                            };
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        /// <summary>
        /// Loop through the items in the list and update the values for Pantry and Cart accordingly/
        /// </summary>
        /// <param name="searchFilters"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private List<SearchProductsBO> UpdateIsInPantryAndIsInCart(ProductFilters searchFilters, List<SearchProductsBO> result, bool isDynamicUOM, Saavi5Entities _db, long userID, bool lastOrderUom)
        {
            foreach (var item in result)
            {
                long productID = item.ProductID;

                if (lastOrderUom)
                {
                    var lastUom = (from tcm in _db.trnsCartMasters
                                   join tcim in _db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                   where tcm.UserID == userID
                                   orderby tcm.CreatedDate descending
                                   group tcim by tcim.ProductID into g
                                   select new LastOrderUOMBO { ProductID = g.FirstOrDefault().ProductID, UOMID = g.FirstOrDefault().UnitId }).FirstOrDefault();

                    if (lastUom != null)
                    {
                        item.LastOrderUOMID = lastUom.UOMID.To<long>();
                    }
                }

                var data = (from cart in _db.trnsTempCartMasters
                            join cartItems in _db.trnsTempCartItemsMasters on cart.CartID equals cartItems.CartID
                            where cart.CustomerID == searchFilters.CustomerID
                                  && cart.RepUserID == searchFilters.UserID
                                  && cartItems.ProductID == productID && cart.IsSavedOrder == false
                            select new { cart.CartID, cartItems.Quantity, cartItems.UnitId }).FirstOrDefault();

                if (data != null)
                {
                    item.IsInCart = true;
                    item.Quantity = data.Quantity ?? 0;
                    item.UOMID = data.UnitId.To<long>();
                }
                else
                {
                    item.IsInCart = false;
                }

                var pantryData = (from pantry in _db.PantryListMasters
                                  join pantryItem in _db.PantryListItemsMasters on pantry.PantryListID equals pantryItem.PantryListID
                                  where pantry.CustomerID == searchFilters.CustomerID && pantryItem.ProductID == productID
                                  select new
                                  {
                                      pantryItem,
                                      pantry.PantryListType
                                  }
                                 ).FirstOrDefault();

                if (pantryData != null)
                {
                    item.IsInPantry = true;
                    item.PantryListID = pantryData.pantryItem.PantryListID;
                    if (pantryData.PantryListType == "F")
                    {
                        item.PantryListItemID = pantryData.pantryItem.PantryItemID;
                    }
                }
                else
                {
                    item.IsInPantry = false;
                }

                if (isDynamicUOM)
                {
                    item.DynamicUOM = (from a in _db.UnitOfMeasurementMasters
                                       join b in _db.AltUOMMasters on a.UOMID equals b.UOMID
                                       where b.ProductID == item.ProductID // add check for manage features
                                       select new DynamicUnitPrices
                                       {
                                           UOMID = a.UOMID,
                                           UOMDesc = a.Code,
                                           QuantityPerUnit = b.QtyPerUnitOfMeasure ?? 1
                                       }).ToList();

                    foreach (var price in item.DynamicUOM)
                    {
                        var priceData = GetPrice(searchFilters.CustomerID, item.ProductID, item.Price, price.UOMID);

                        price.Price = (price.QuantityPerUnit > 1 ? (priceData.Price * price.QuantityPerUnit) : priceData.Price).To<double>();// priceData.Price;
                        price.IsSpecial = priceData.IsSpecial;
                        price.IsPromotional = priceData.IsPromotional;
                        price.CostPrice = (price.QuantityPerUnit > 1 ? (item.CompanyPrice * price.QuantityPerUnit) : item.CompanyPrice).To<double>();
                    }

                    if (item.DynamicUOM == null || item.DynamicUOM.Count == 0)
                    {
                        var dPrice = new DynamicUnitPrices();
                        var priceData = GetPrice(searchFilters.CustomerID, item.ProductID, item.Price, item.UOMID);

                        var upc = (item.UnitsPerCarton == null || item.UnitsPerCarton == 0) ? 1 : item.UnitsPerCarton;

                        dPrice.UOMDesc = item.UOMDesc;
                        dPrice.UOMID = item.UOMID;
                        dPrice.QuantityPerUnit = item.UnitsPerCarton ?? 1;
                        dPrice.Price = priceData.Price * upc ?? 1;
                        dPrice.IsPromotional = false;
                        dPrice.IsSpecial = priceData.IsSpecial;
                        dPrice.CostPrice = item.CompanyPrice * upc ?? 1;
                        item.DynamicUOM.Add(dPrice);
                    }
                }
                else
                {
                    var upc = (item.UnitsPerCarton == null || item.UnitsPerCarton == 0) ? 1 : item.UnitsPerCarton;

                    item.Prices = GetPrice(searchFilters.CustomerID, item.ProductID, item.Price, item.UOMID);
                    item.Prices.UOMID = item.UOMID;
                    item.Prices.UOMDesc = item.UOMDesc;
                    item.Prices.CostPrice = item.CompanyPrice * upc ?? 1;
                    item.Prices.QuantityPerUnit = item.UnitsPerCarton ?? 1;
                }
            }

            return result;
        }

        #endregion Search products by filters and paging.

        #region Get pantry lists for a customer

        public async Task<APiResponse> GetPantryListByCustomerID(long customerID)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var pantry = await (from p in _db.PantryListMasters
                                        where p.CustomerID == customerID && p.PantryListType != "D"
                                        orderby p.PantryListName
                                        select new PantryListBO
                                        {
                                            PantryListID = p.PantryListID,
                                            PantryListName = p.PantryListName,
                                            PantryListType = p.PantryListType,
                                            IsCreatedByRepUser = p.IsCreatedByRepUser,
                                            IsActive = p.IsActive,
                                            IsSynced = p.IsSynced,
                                            CreatedDate = p.CreatedDate
                                        }).OrderByDescending(o => o.PantryListID).ToListAsync();

                    if (pantry.Count > 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Success", pantry);
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get pantry lists for a customer

        #region Add update pantry/fvorite list for a customer

        public async Task<APiResponse> AddUpdatePantryList(PantryListBO obj)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (obj != null)
                    {
                        //If PantryListID > 0 then update the existing record
                        if (obj.PantryListID > 0 && obj.IsCopy == false)
                        {
                            var pantry = await _db.PantryListMasters.Where(p => p.PantryListName == obj.PantryListName && p.CustomerID == obj.CustomerID).FirstOrDefaultAsync();
                            // check if the name already exists.
                            if (pantry != null)
                            {
                                return ReturnMethod(SaaviErrorCodes.AlreadyExists, "Name already exists for this Customer.");
                            }
                            pantry.PantryListName = obj.PantryListName;
                            pantry.ModifiedDate = DateTime.Now;

                            await _db.SaveChangesAsync();
                        }
                        else // Insert new value into the database
                        {
                            var pantry = await _db.PantryListMasters.Where(p => p.PantryListName == obj.PantryListName && p.CustomerID == obj.CustomerID).FirstOrDefaultAsync();
                            if (pantry != null)
                            {
                                return ReturnMethod(SaaviErrorCodes.AlreadyExists, "Name already exists for this Customer.");
                            }
                            else
                            {
                                // map the BO with Entity properties and insert.
                                PantryListMaster pl = new PantryListMaster();
                                Mapper.Map(obj, pl);
                                //pl.PantryListID = 0;
                                pl.CreatedDate = DateTime.Now;
                                pl.IsActive = true;
                                pl.PantryListType = obj.IsCopy == true ? "CC" : "C"; // C for pantry Created by a customer. CC for copied
                                _db.PantryListMasters.Add(pl);

                                await _db.SaveChangesAsync();

                                if (obj.IsCopy)
                                {
                                    var pantryItems = _db.PantryListItemsMasters.Where(p => p.PantryListID == obj.PantryListID).ProjectToList<PantryItemsBO>();

                                    List<PantryListItemsMaster> li = new List<PantryListItemsMaster>();
                                    foreach (var item in pantryItems)
                                    {
                                        PantryListItemsMaster pli = new PantryListItemsMaster();
                                        Mapper.Map(item, pli);
                                        pli.PantryItemID = 0;
                                        pli.PantryListID = pl.PantryListID;
                                        li.Add(pli);
                                    }

                                    _db.PantryListItemsMasters.AddRange(li);
                                    _db.SaveChanges();
                                }
                            }
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "Empty request object.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add update pantry/fvorite list for a customer

        #region Add items to default Pantry or user favorite list

        public async Task<APiResponse> AddItemsToPantry(PantryItemsBO obj)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (obj == null)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No data in request object.");
                    }

                    if (obj.PantryListID == 0)
                    {
                        PantryListMaster pl;
                        if (obj.PantryType == "F")
                        {
                            pl = _db.PantryListMasters.Where(p => p.CustomerID == obj.CustomerID && p.PantryListType == "F").FirstOrDefault();
                            if (pl == null)
                            {
                                pl = new PantryListMaster();

                                pl.CustomerID = obj.CustomerID;
                                pl.PantryListName = "Default Favorite";
                                pl.CreatedBy = obj.CustomerID;
                                pl.IsSynced = false;
                                pl.IsCreatedByRepUser = false;
                                pl.IsActive = true;
                                pl.CreatedDate = DateTime.Now;
                                pl.IsActive = true;
                                pl.PantryListType = "F"; // F for default favorite list Created by a customer.
                                _db.PantryListMasters.Add(pl);

                                _db.SaveChanges();
                            }
                        }
                        else
                        {
                            pl = _db.PantryListMasters.Where(p => p.CustomerID == obj.CustomerID && p.PantryListType == "D").FirstOrDefault();
                        }

                        if (pl != null)
                        {
                            obj.PantryListID = pl.PantryListID;
                        }
                    }

                    if (obj.PantryListID == 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No pantry exist for this customer.");
                    }

                    var item = _db.PantryListItemsMasters.Where(p => p.PantryListID == obj.PantryListID && p.ProductID == obj.ProductID).FirstOrDefault();

                    if (item != null)
                    {
                        //item.Quantity = item.Quantity + obj.Quantity;
                        return ReturnMethod(SaaviErrorCodes.AlreadyExists, "Product already exists in specified List");
                    }
                    else
                    {
                        // map the BO with Entity properties and insert.
                        PantryListItemsMaster pl = new PantryListItemsMaster();
                        Mapper.Map(obj, pl);
                        pl.CreatedDate = DateTime.Now;
                        pl.ModifiedDate = DateTime.Now;
                        _db.PantryListItemsMasters.Add(pl);
                    }
                    await _db.SaveChangesAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add items to default Pantry or user favorite list

        #region Delete item from pantry list

        public async Task<APiResponse> DeleteItemFromPantry(DeleteFavoriteListItemBO obj)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (obj == null || obj.PantryListID == 0 || obj.PantryItemID == 0 || obj.CustomerID == 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "Invalid data in request object.");
                    }

                    //var pl = _db.PantryListMasters.Where(p => p.CustomerID == obj.CustomerID && p.PantryListID == obj.PantryListID).FirstOrDefault();

                    //if (pl == null)
                    //{
                    //    return ReturnMethod(SaaviErrorCodes.NoData, "No pantry exist for this customer.");
                    //}

                    var item = _db.PantryListItemsMasters.Where(p => p.PantryListID == obj.PantryListID && p.PantryItemID == obj.PantryItemID).FirstOrDefault();

                    if (item == null)
                    {
                        return ReturnMethod(SaaviErrorCodes.NotFound, "Item not found in specified List");
                    }
                    else
                    {
                        _db.PantryListItemsMasters.Remove(item);
                    }
                    await _db.SaveChangesAsync();

                    return ReturnMethod(SaaviErrorCodes.Success, "Success");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Delete item from pantry list

        #region Get Product by Barcode text

        public async Task<APiResponse> GetProductByBarcode(ProductByBarcode obj)
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    if (obj.Barcode != "")
                    {
                        var productData = db.ProductMasters.Where(p => p.BarCodeText == obj.Barcode).FirstOrDefault();

                        if (productData != null)
                        {
                            SearchProductByBarCode pro = new SearchProductByBarCode();
                            pro.ProductCode = productData.ProductCode;
                            pro.ProductName = productData.ProductName;
                            pro.Description = productData.ProductDescription;
                            return ReturnMethod(SaaviErrorCodes.Success, "Success", new { product = pro });
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Default pantry list for this customer.");
                        }
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "Barcode is required.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Product by Barcode text

        #region Get Pantry list items for a specific Pantry or Default pantry

        public async Task<APiResponse> GetPantryListItems(PantryFilters obj)
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    if (obj.PantryListID == 0)
                    {
                        PantryListMaster defaultPantry;
                        if (obj.PantryType == "F") // F for default favorite list
                        {
                            defaultPantry = db.PantryListMasters.Where(p => p.PantryListType == "F" && p.CustomerID == obj.CustomerID).FirstOrDefault();
                        }
                        else
                        {
                            defaultPantry = db.PantryListMasters.Where(p => p.PantryListType == "D" && p.CustomerID == obj.CustomerID).FirstOrDefault();
                        }

                        if (defaultPantry != null)
                        {
                            var defPantryID = defaultPantry.PantryListID;
                            obj.PantryListID = defPantryID;

                            int c = db.PantryListItemsMasters.Count(p => p.PantryListID == obj.PantryListID);
                            if (c == 0)
                            {
                                return ReturnMethod(SaaviErrorCodes.NoData, "No items in Default pantry list for this customer.");
                            }
                        }
                        else
                        {
                            return ReturnMethod(SaaviErrorCodes.NoData, "No Default pantry list for this customer.");
                        }
                    }

                    var pantryItems = await db.PantryListItemsMasters.Where(p => p.PantryListID == obj.PantryListID).FirstOrDefaultAsync();

                    // check if there are items in the pantry list.
                    // If not then exit the method.
                    if (pantryItems == null)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No items in selected pantry list.");
                    }

                    var customer = await db.CustomerMasters.Where(c => c.CustomerID == obj.CustomerID).FirstOrDefaultAsync();
                    var customerCatFilter = customer;
                    if (obj.listCustomerId > 0)
                    {
                        customerCatFilter = await db.CustomerMasters.Where(c => c.CustomerID == obj.listCustomerId).FirstOrDefaultAsync();
                    }
                    //var mainFeatures = await db.MainFeaturesMasters.FirstOrDefaultAsync();

                    long userIDForfeatures = obj.UserID;
                    if (obj.IsRepUser)
                    {
                        userIDForfeatures = db.UserMasters.Where(u => u.CustomerID == obj.CustomerID).FirstOrDefault().UserID;
                    }
                    var customerFeatures = db.CustomerFeaturesMasters.Where(x => x.UserID == userIDForfeatures).FirstOrDefault();
                    var mainFeatures = db.MainFeaturesMasters.FirstOrDefault();

                    bool isDynamicUOM = false;
                    bool pantrySorting = false;
                    if (customerFeatures != null)
                    {
                        isDynamicUOM = Convert.ToBoolean(customerFeatures.IsDynamicUOM);
                        pantrySorting = Convert.ToBoolean(customerFeatures.IsPantrySorting);
                    }

                    bool showOnlyStockItems = false;
                    bool returnLastOrderUOM = false;
                    if (mainFeatures != null)
                    {
                        showOnlyStockItems = mainFeatures.IsShowOnlyStockItems.To<bool>();

                        returnLastOrderUOM = mainFeatures.ReturnLastOrderUOM.To<bool>();
                    }

                    //bool IsSpecialCategory = customer != null ? Convert.ToBoolean(customer.IsSpecialCategory) : false;
                    //string warehouse = customer != null ? customer.Warehouse == null ? "" : customer.Warehouse.Trim().ToLower() : "";

                    string TagNames = customerCatFilter != null ? Convert.ToString(customerCatFilter.TagNames == null ? "" : customerCatFilter.TagNames) : "";
                    bool IsSpecialCategory = customerCatFilter != null ? Convert.ToBoolean(customerCatFilter.IsSpecialCategory) : false;
                    string[] TagNamesArray = null;
                    TagNamesArray = TagNames.Split(new char[] { ',' });

                    string warehouse = customer != null ? customer.Warehouse == null ? "" : customer.Warehouse.Trim().ToLower() : "";

                    IEnumerable<SearchPantryListBO> query;
                    query = (from pl in db.PantryListItemsMasters
                             join p in db.PantryListMasters on pl.PantryListID equals p.PantryListID
                             join pro in db.ProductMasters on pl.ProductID equals pro.ProductID
                             join cats in db.ProductCategoriesMasters on pro.CategoryID equals cats.CategoryID
                             join mcats in db.MainCategoriesMasters on cats.MCategoryId equals mcats.MCategoryId
                             join unit in db.UnitOfMeasurementMasters on pro.UOMID equals unit.UOMID

                             join rwunit in db.UnitOfMeasurementMasters on pro.RW_UOMID equals rwunit.UOMID into temp3
                             from rwunit in temp3.DefaultIfEmpty()

                             join filter in db.ProductFiltersMasters on pro.FilterID equals filter.FilterID

                             join sugg in db.SuggestiveItemsMasters on pro.ProductID equals sugg.ProductID into temp
                             from sugg in temp.DefaultIfEmpty()

                             join soh in db.StockWareHouses on pro.ProductID equals soh.ProductID into temp6
                             from soh in temp6.DefaultIfEmpty()

                             where pl.PantryListID == obj.PantryListID
                             && ((cats.IsSpecialCategory == false || cats.IsSpecialCategory == null) || (cats.IsSpecialCategory == true && IsSpecialCategory == true && TagNamesArray.Contains(cats.CategoryName)))
                             && pro.IsActive == true
                             && pl.IsActive == true
                             && cats.IsActive == true
                             && mcats.IsActive == true
                             && filter.IsActive == true
                             && (pro.FilterID == obj.FilterID || obj.FilterID == 0)
                             && (warehouse == "" || (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse))
                            && (obj.Searchtext == "" || pro.ProductName.Contains(obj.Searchtext) || pro.ProductDescription.Contains(obj.Searchtext) || pro.ProductCode.Contains(obj.Searchtext) || pro.BarcodeGTIN.Contains(obj.Searchtext) || pro.BarCodeText.Contains(obj.Searchtext))
                             select new SearchPantryListBO
                             {
                                 PantryListID = pl.PantryListID,
                                 PantryListName = p.PantryListName,
                                 PantryItemID = pl.PantryItemID,
                                 ProductID = pl.ProductID,
                                 ProductCode = pro.ProductCode,
                                 Description = pro.ProductDescription == null ? "" : pro.ProductDescription,
                                 Description2 = pro.ProductDescription2 ?? "",
                                 Description3 = pro.ProductDescription3 ?? "",
                                 Feature1 = pro.ProductFeature1 ?? "",
                                 Feature2 = pro.ProductFeature2 ?? "",
                                 Feature3 = pro.ProductFeature3 ?? "",
                                 IsNew = pro.IsNew ?? false,
                                 IsOnSale = pro.IsOnSale ?? false,
                                 IsBackSoon = pro.IsBackSoon ?? false,
                                 Barcode = pro.BarCodeText,
                                 UOMDesc = unit.Code,
                                 UOMID = unit.UOMID,
                                 SellBelowCost = pro.SellBelowCost ?? false,
                                 ProductName = pro.ProductName,
                                 ProductImage = (pro.ProductImage != null && pro.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + pro.ProductImage : "",
                                 ProductImageThumb = (pro.ProductImage != null && pro.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + "thumb_" + pro.ProductImage : "",
                                 UnitId = (pro.RandomWeightItem == true && rwunit.UOMID != null) ? unit.UOMID : (unit.UOMID == null ? 0 : unit.UOMID),
                                 UnitName = (pro.RandomWeightItem == true && rwunit.UOMID != null) ? (rwunit.UOMDesc == null ? "" : rwunit.Code) : (unit.UOMDesc == null ? "" : unit.Code),
                                 FilterID = filter.FilterID,
                                 FilterName = (filter.FilterName == null ? "" : filter.FilterName).Trim(),
                                 CategoryName = cats.CategoryName,
                                 MainCategoryName = mcats.MCategoryName,
                                 Price = pro.Price == null ? 0 : pro.Price,
                                 CategoryId = pro.CategoryID,
                                 CompanyPrice = (pro.ComPrice == null) ? 0 : pro.ComPrice,
                                 Quantity = pl.Quantity,
                                 BuyIn = pro.BuyIn ?? false,
                                 IsStatusIN = (pro.ERPStatus == "IN") ? true : false,
                                 IsActive = pl.IsActive,
                                 IsAvailable = (pro.IsAvailable == null) ? false : pro.IsAvailable,
                                 ShowOrder = (pl.ShowOrder == null) ? 0 : pl.ShowOrder,
                                 ShelfLevel = (pl.ShelfLevel == null) ? 0 : pl.ShelfLevel,
                                 StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                 IsCore = (pl.IsCore == null) ? false : pl.IsCore,
                                 IsSuggestive = (sugg.ProductID == null) ? false : ((sugg.IsActive == null) ? false : sugg.IsActive),
                                 UnitsPerCarton = pro.UnitsPerCarton == null ? 1 : pro.UnitsPerCarton,
                                 //IsBoxDiscount = Convert.ToDouble(pro.UNITS_PER_OUTER == null ? 0 : pro.UNITS_PER_OUTER) > 0,
                                 IsCountrywideRewards = pro.RewardItem ?? false,
                                 IsGST = pro.GST == null ? false : pro.GST,
                                 Brand = pro.Brand == null ? "" : pro.Brand,
                                 Supplier = pro.Supplier == null ? "" : pro.Supplier.Trim(),
                                 IsSpecialCategory = (cats.IsSpecialCategory == null) ? false : cats.IsSpecialCategory,
                                 MinOQ = pro.MinOQ ?? 0,
                                 MaxOQ = pro.MaxOQ ?? 0,
                                 ProductImages = (from a in db.ProductImageMasters
                                                  where a.ProductID == pro.ProductID
                                                  select new ProductImages
                                                  {
                                                      ImageName = a.BaseUrl + a.ProductImage
                                                  }).ToList(),
                                 ShareURL = pro.ShareURL ?? "",
                                 ProductFeature = pro.ProductFeature ?? "",
                                 IsDonationBox = pro.IsDonationBox ?? false
                             }).Where(s => showOnlyStockItems == false || s.StockQuantity > 0);

                    var result = (List<SearchPantryListBO>)null;

                    if (obj.ShowAll)
                    {
                        if (pantrySorting)
                        {
                            result = query.OrderBy(o => o.ShowOrder).ToList();
                            // result = result.OrderBy(o => o.ShowOrder).ToList();
                        }
                        else
                        {
                            result = query.OrderByDescending(o => o.Price).ToList();
                        }
                    }
                    else
                    {
                        if (pantrySorting)
                        {
                            result = query.OrderBy(o => o.ShowOrder).Skip(obj.PageIndex * obj.PageSize).Take(obj.PageSize).ToList();
                            // result = result.OrderBy(o => o.ShowOrder).ToList();
                        }
                        else
                        {
                            result = query.OrderByDescending(o => o.Price).Skip(obj.PageIndex * obj.PageSize).Take(obj.PageSize).ToList();
                        }
                    }

                    #region Code to update IsInPantry, IsInCart and Prices data for products

                    result = UpdateIsInPantryAndIsInCartPantry(obj, result, isDynamicUOM, db, obj.UserID, returnLastOrderUOM).ToList();

                    #endregion Code to update IsInPantry, IsInCart and Prices data for products

                    int totalPages = obj.ShowAll ? 1 : (query.Count() + obj.PageSize - 1) / obj.PageSize;

                    if (query.Count() == 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No items in selected pantry list.");
                    }
                    else
                    {
                        var savedOrder = db.trnsTempCartMasters.Where(c => c.CustomerID == obj.CustomerID && c.RepUserID == obj.UserID && c.IsSavedOrder == true).FirstOrDefault();

                        long savedOrderID = 0;

                        if (savedOrder != null)
                        {
                            savedOrderID = savedOrder.CartID;
                        }

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new { products = result, TotalResults = query.Count(), TotalPages = totalPages, SavedOrder = savedOrderID });
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        private List<SearchPantryListBO> UpdateIsInPantryAndIsInCartPantry(PantryFilters searchFilters, List<SearchPantryListBO> result, bool isDynamicUOM, Saavi5Entities _db, long userID, bool lastOrderUom)
        {
            foreach (var item in result)
            {
                long productID = item.ProductID;

                if (lastOrderUom)
                {
                    var lastUom = (from tcm in _db.trnsCartMasters
                                   join tcim in _db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                   where tcm.UserID == userID
                                   orderby tcm.CreatedDate descending
                                   group tcim by tcim.ProductID into g
                                   select new LastOrderUOMBO { ProductID = g.FirstOrDefault().ProductID, UOMID = g.FirstOrDefault().UnitId }).FirstOrDefault();

                    if (lastUom != null)
                    {
                        item.LastOrderUOMID = lastUom.UOMID.To<long>();
                    }
                }

                var data = (from cart in _db.trnsTempCartMasters
                            join cartItems in _db.trnsTempCartItemsMasters on cart.CartID equals cartItems.CartID
                            where cart.CustomerID == searchFilters.CustomerID
                                  && cart.RepUserID == searchFilters.UserID
                                  && cartItems.ProductID == productID && cart.IsSavedOrder == false
                            select new { cart.CartID, cartItems.Quantity, cartItems.UnitId }).FirstOrDefault();

                if (data != null)
                {
                    item.IsInCart = true;
                    item.Quantity = data.Quantity ?? 0;
                    item.UOMID = data.UnitId.To<long>();
                }
                else
                {
                    item.IsInCart = false;
                }

                item.IsInPantry = true;
                item.PantryListID = searchFilters.PantryListID;

                if (searchFilters.IsRepUser || searchFilters.IsWebView)
                {
                    var pantryData = _db.PantryListItemsMasters.Where(p => p.PantryListID == searchFilters.PantryListID && p.ProductID == productID).FirstOrDefault();
                    if (pantryData != null)
                    {
                        var salesBO = new WeeklySalesBO();
                        Mapper.Map(pantryData, salesBO);
                        item.WeeklySales = salesBO;
                    }
                }

                if (isDynamicUOM)
                {
                    item.DynamicUOM = (from a in _db.UnitOfMeasurementMasters
                                       join b in _db.AltUOMMasters on a.UOMID equals b.UOMID
                                       where b.ProductID == item.ProductID // add check for manage features
                                       select new DynamicUnitPrices
                                       {
                                           UOMID = a.UOMID,
                                           UOMDesc = a.Code,
                                           QuantityPerUnit = b.QtyPerUnitOfMeasure ?? 1
                                       }).ToList();

                    foreach (var price in item.DynamicUOM)
                    {
                        var priceData = GetPrice(searchFilters.CustomerID, item.ProductID, item.Price.To<Double>(), price.UOMID);

                        price.Price = (price.QuantityPerUnit > 0 ? (priceData.Price * price.QuantityPerUnit) : priceData.Price).To<double>();//priceData.Price; //
                        price.IsSpecial = priceData.IsSpecial;
                        price.IsPromotional = priceData.IsPromotional;
                        price.CostPrice = (price.QuantityPerUnit > 0 ? (item.CompanyPrice * price.QuantityPerUnit) : item.CompanyPrice).To<double>();
                    }

                    if (item.DynamicUOM == null || item.DynamicUOM.Count == 0)
                    {
                        var dPrice = new DynamicUnitPrices();
                        var priceData = GetPrice(searchFilters.CustomerID, item.ProductID, item.Price.To<double>(), item.UOMID);

                        var upc = (item.UnitsPerCarton == null || item.UnitsPerCarton == 0) ? 1 : item.UnitsPerCarton;

                        dPrice.UOMDesc = item.UOMDesc;
                        dPrice.UOMID = item.UOMID;
                        dPrice.QuantityPerUnit = item.UnitsPerCarton ?? 1;
                        dPrice.Price = priceData.Price * upc ?? 1;
                        dPrice.IsPromotional = false;
                        dPrice.IsSpecial = priceData.IsSpecial;
                        dPrice.CostPrice = item.CompanyPrice * upc ?? 1;

                        item.DynamicUOM.Add(dPrice);
                    }
                }
                else
                {
                    var upc = (item.UnitsPerCarton == null || item.UnitsPerCarton == 0) ? 1 : item.UnitsPerCarton;

                    item.Prices = GetPrice(searchFilters.CustomerID, item.ProductID, item.Price.To<Double>(), item.UOMID);
                    item.Prices.UOMID = item.UOMID;
                    item.Prices.UOMDesc = item.UOMDesc;
                    item.Prices.CostPrice = item.CompanyPrice * upc ?? 1;
                    item.Prices.QuantityPerUnit = upc ?? 1;
                }
            }

            return result;
        }

        #endregion Get Pantry list items for a specific Pantry or Default pantry

        #region Add/update items in Temp Cart

        public async Task<APiResponse> InsertTempCart(TempCartBO obj)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (obj.InCart == null)
                    {
                        obj.InCart = false;
                    }
                    var cart = await _db.trnsTempCartMasters.Where(c => c.CustomerID == obj.CustomerID && c.RepUserID == obj.RepUserID && c.IsOrderPlpacedByRep == obj.IsOrderPlpacedByRep && c.IsSavedOrder == obj.IsSavedOrder).FirstOrDefaultAsync();
                    if (cart == null)
                    {
                        cart = new trnsTempCartMaster();
                        cart.CreatedDate = DateTime.Now;
                        Mapper.Map(obj, cart);
                        _db.trnsTempCartMasters.Add(cart);
                    }
                    else
                    {
                        cart.ModifiedDate = DateTime.Now;
                        cart.RunNo = obj.RunNo;
                        cart.CommentLine = obj.CommentLine;
                        cart.PackagingSequence = obj.PackagingSequence;
                    }

                    _db.SaveChanges();

                    if (obj.InCart == false)
                    {
                        if (obj.CartItem != null)
                        {
                            var cartItem = _db.trnsTempCartItemsMasters.Where(c => c.CartID == cart.CartID && c.ProductID == obj.CartItem.ProductID).FirstOrDefault();

                            if (cartItem == null)
                            {
                                cartItem = new trnsTempCartItemsMaster();
                                Mapper.Map(obj.CartItem, cartItem);
                                cartItem.CreatedDate = DateTime.Now;
                                cartItem.CartID = cart.CartID;
                                cartItem.IsNoPantry = obj.CartItem.IsNoPantry;
                                _db.trnsTempCartItemsMasters.Add(cartItem);
                            }
                            else
                            {
                                cartItem.Quantity = obj.CartItem.Quantity;
                                cartItem.Price = obj.CartItem.Price;
                                cartItem.UnitId = obj.CartItem.UnitId;
                                cartItem.IsNoPantry = obj.CartItem.IsNoPantry;
                                cartItem.ModifiedDate = DateTime.Now;
                                cartItem.IsSpecialPrice = obj.CartItem.IsSpecialPrice;
                            }
                            _db.SaveChanges();
                        }
                    }
                    else
                    {
                        if (obj.SuggestiveCartItems != null && obj.SuggestiveCartItems.Count > 0)
                        {
                            var cartItems = new List<trnsTempCartItemsMaster>();
                            foreach (var item in obj.SuggestiveCartItems)
                            {
                                var cartItem = _db.trnsTempCartItemsMasters.Where(c => c.CartID == cart.CartID && c.ProductID == item.ProductID).FirstOrDefault();
                                if (cartItem == null)
                                {
                                    cartItem = new trnsTempCartItemsMaster();
                                    Mapper.Map(item, cartItem);
                                    cartItem.CreatedDate = DateTime.Now;
                                    cartItem.CartID = cart.CartID;
                                    cartItem.IsNoPantry = false;
                                    _db.trnsTempCartItemsMasters.Add(cartItem);
                                }
                                else
                                {
                                    cartItem.Quantity = item.Quantity;
                                    cartItem.Price = item.Price;
                                    cartItem.UnitId = item.UnitId;
                                    cartItem.IsNoPantry = item.IsNoPantry ?? false;
                                    cartItem.ModifiedDate = DateTime.Now;
                                    cartItem.IsSpecialPrice = item.IsSpecialPrice;
                                }

                                _db.SaveChanges();
                            }
                        }
                    }
                    return new APiResponse
                    {
                        ResponseCode = SaaviErrorCodes.Success,
                        Message = "Success",
                        Result = new { CartID = cart.CartID }
                    };
                }
            }
            catch (Exception ex)
            {
                return new APiResponse
                {
                    ResponseCode = SaaviErrorCodes.Error,
                    Message = ex.Message
                };
            }
        }

        #endregion Add/update items in Temp Cart

        #region Add/Update cart item details

        /// <summary>
        /// Update the cart data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<APiResponse> UpdateCartItemData(TempCartItems model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    double? priceTotal = 0;
                    var cartItem = _db.trnsTempCartItemsMasters.Where(c => c.CartID == model.CartID && c.ProductID == model.ProductID).FirstOrDefault();

                    if (cartItem != null)
                    {
                        Mapper.Map(model, cartItem);
                        cartItem.Quantity = model.Quantity;
                        cartItem.Price = model.Price;
                        cartItem.UnitId = model.UnitId;
                        cartItem.ModifiedDate = DateTime.Now;
                        priceTotal = cartItem.Price * cartItem.Quantity;
                        await _db.SaveChangesAsync();
                    }
                    else
                    {
                        cartItem = new trnsTempCartItemsMaster();
                        Mapper.Map(model, cartItem);
                        cartItem.CreatedDate = DateTime.Now;
                        priceTotal = cartItem.Price * cartItem.Quantity;

                        _db.trnsTempCartItemsMasters.Add(cartItem);
                        await _db.SaveChangesAsync();

                        return ReturnMethod(SaaviErrorCodes.Success, "Item Added", new { PriceTotal = priceTotal });
                    }

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new { PriceTotal = priceTotal });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Add/Update cart item details

        #region Get items in temp cart of a user / customer

        public async Task<APiResponse> GetTempCart(GetCartBO model)
        {
            try
            {
                using (var db = new Saavi5Entities())
                {
                    var customer = db.CustomerMasters.Find(model.CustomerID);

                    string warehouse = customer.Warehouse ?? "";
                    trnsTempCartMaster tempCart = new trnsTempCartMaster();

                    if (model.CartID != 0)
                    {
                        tempCart = db.trnsTempCartMasters.Where(c => c.CartID == model.CartID).FirstOrDefault();
                    }
                    else
                    {
                        tempCart = db.trnsTempCartMasters.Where(c => c.RepUserID == model.UserID && c.CustomerID == model.CustomerID && c.IsOrderPlpacedByRep == model.IsPlacedByRep && c.IsSavedOrder == model.IsSavedOrder).FirstOrDefault();
                    }
                    if (tempCart != null)
                    {
                        long? userIDForFeatures = model.UserID;
                        if (model.IsPlacedByRep)
                        {
                            userIDForFeatures = db.UserMasters.Where(u => u.CustomerID == model.CustomerID).FirstOrDefault().UserID;
                        }

                        var customerFeatures = db.CustomerFeaturesMasters.Where(x => x.UserID == userIDForFeatures).FirstOrDefault();
                        var mainFeatures = db.MainFeaturesMasters.FirstOrDefault();

                        bool isDynamicUOM = false;
                        if (customerFeatures != null)
                        {
                            isDynamicUOM = Convert.ToBoolean(customerFeatures.IsDynamicUOM);
                        }

                        bool showOnlyStockItems = false;
                        bool returnLastOrderUOM = false;
                        if (mainFeatures != null)
                        {
                            showOnlyStockItems = mainFeatures.IsShowOnlyStockItems.To<bool>();
                            returnLastOrderUOM = mainFeatures.ReturnLastOrderUOM.To<bool>();
                        }

                        List<CartItemsBO> data;

                        data = await (from cart in db.trnsTempCartMasters
                                      join cartItems in db.trnsTempCartItemsMasters on cart.CartID equals cartItems.CartID
                                      join prod in db.ProductMasters on cartItems.ProductID equals prod.ProductID
                                      join cust in db.CustomerMasters on cart.CustomerID equals cust.CustomerID
                                      join cat in db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                                      join mcat in db.MainCategoriesMasters on cat.MCategoryId equals mcat.MCategoryId
                                      join unit in db.UnitOfMeasurementMasters on prod.UOMID equals unit.UOMID

                                      //join rwunit in db.UnitOfMeasurementMasters on prod.RW_UOMID equals rwunit.UOMID into temp3
                                      //from rwunit in temp3.DefaultIfEmpty()

                                      join orunit in db.UnitOfMeasurementMasters on cartItems.UnitId equals orunit.UOMID into temp2
                                      from orunit in temp2.DefaultIfEmpty()

                                      join filter in db.ProductFiltersMasters on prod.FilterID equals filter.FilterID

                                      join soh in db.StockWareHouses on prod.ProductID equals soh.ProductID into temp6
                                      from soh in temp6.DefaultIfEmpty()

                                      join lou in (
                                        from tcm in db.trnsCartMasters
                                        join tcim in db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                        where tcm.UserID == model.UserID
                                        orderby tcm.CreatedDate descending
                                        group tcim by tcim.ProductID into g
                                        select new LastOrderUOMBO { ProductID = g.FirstOrDefault().ProductID, UOMID = g.FirstOrDefault().UnitId }
                                     ) on prod.ProductID equals lou.ProductID into tempLOU
                                      from lou in tempLOU.DefaultIfEmpty()

                                      where (cartItems.CartID == tempCart.CartID
                                              && prod.IsAvailable == true
                                              && prod.IsActive == true && cat.IsActive == true
                                              && mcat.IsActive == true && filter.IsActive == true
                                              && (warehouse == "" || soh.WareHouse == null || (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse))
                                              )
                                      select new CartItemsBO
                                      {
                                          ProductID = prod.ProductID,
                                          CartItemID = cartItems.CartItemID,
                                          CartID = cart.CartID,
                                          ProductCode = prod.ProductCode,
                                          OrderDate = cart.CreatedDate ?? DateTime.Now,
                                          CreatedDate = cart.CreatedDate ?? DateTime.Now,
                                          Price = cartItems.Price,
                                          BuyIn = prod.BuyIn ?? false,
                                          IsStatusIN = (prod.ERPStatus == "IN") ? true : false,
                                          LastOrderUOMID = lou.UOMID ?? 0,
                                          ProdPrice = prod.Price,
                                          SellBelowCost = prod.SellBelowCost ?? false,
                                          //SpecialPrice = Convert.ToDouble(sp.SpecialPrice == null ? 0 : sp.SpecialPrice),
                                          CompanyPrice = prod.ComPrice ?? 0,
                                          Quantity = cartItems.Quantity,
                                          ProductName = prod.ProductName,
                                          ProductDescription = prod.ProductDescription ?? "",
                                          ProductDescription2 = prod.ProductDescription2 ?? "",
                                          ProductDescription3 = prod.ProductDescription3 ?? "",
                                          ProductFeature1 = prod.ProductFeature1 ?? "",
                                          ProductFeature2 = prod.ProductFeature2 ?? "",
                                          ProductFeature3 = prod.ProductFeature3 ?? "",
                                          ProductIsNew = prod.IsNew ?? false,
                                          ProductIsOnSale = prod.IsOnSale ?? false,
                                          ProductIsBackSoon = prod.IsBackSoon ?? false,
                                          CustomerCode = cust.AlphaCode,
                                          CustomerID = cust.CustomerID,
                                          ProductImage = HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + prod.ProductImage,
                                          ProductThumbImage = HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + "thumb_" + prod.ProductImage,
                                          UOMID = cartItems.UnitId, //(prod.RandomWeightItem == true && rwunit.UOMID != null) ? unit.UOMID : (unit.UOMID == null ? 0 : unit.UOMID),
                                          UnitName = unit.Code,// (prod.RandomWeightItem == true && rwunit.UOMID != null) ? (rwunit.UOMDesc == null ? "" : rwunit.UOMDesc) : (unit.UOMDesc == null ? "" : unit.UOMDesc),
                                          OrderUnitName = orunit.Code ?? unit.Code,
                                          OrderUnitId = (orunit.UOMID == null) ? unit.UOMID : orunit.UOMID,
                                          FilterName = filter.FilterName,
                                          ProductWeight = cartItems.ProductWeight ?? 0,
                                          WeightName = cartItems.WeightName,
                                          WeightDescription = cartItems.WeightDescription,
                                          IsPieceOrWeight = cartItems.IsPieceOrWeight,
                                          IsAvailable = prod.IsAvailable ?? false,
                                          StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                          IsSpecial = false,//(sp.SpecialPriceID == null) ? false : true,
                                          IsDiscountApplicable = false,
                                          IsCountrywideRewards = prod.RewardItem ?? false,
                                          IsInvoiceComment = cart.IsInvoiceComment ?? false,
                                          IsUnloadComment = cart.IsUnloadComment ?? false,
                                          IsNoPantry = cartItems.IsNoPantry ?? false,
                                          ExtDoc = cart.ExtDoc ?? "",
                                          RunNo = cart.RunNo ?? "",
                                          IsGST = prod.GST ?? false,
                                          PackagingSequence = cart.PackagingSequence ?? "",
                                          Brand = prod.Brand ?? "",
                                          Supplier = prod.Supplier ?? "",
                                          PriceTotal = cartItems.Price * cartItems.Quantity,
                                          MinOQ = prod.MinOQ ?? 0,
                                          MaxOQ = prod.MaxOQ ?? 0,
                                          UnitsPerCarton = prod.UnitsPerCarton ?? 0,
                                          ProdCommentID = cartItems.CommentID ?? 0,
                                          ProductImages = (from a in db.ProductImageMasters
                                                           where a.ProductID == prod.ProductID
                                                           select new ProductImages
                                                           {
                                                               ImageName = a.BaseUrl + a.ProductImage
                                                           }).ToList(),
                                          ShareURL = prod.ShareURL ?? "",
                                          ProductFeature = prod.ProductFeature ?? "",
                                          IsDonationBox = prod.IsDonationBox ?? false,
                                          TotalWithGST = prod.GST == true ? ((cartItems.Price * .1) + cartItems.Price) * cartItems.Quantity : cartItems.Price * cartItems.Quantity
                                      }).AsNoTracking().ToListAsync();

                        var result = (List<CartItemsBO>)null;

                        #region Code to update IsInPantry, IsInCart and Prices data for products

                        result = GetPricesLogicCart(model.CustomerID, data, isDynamicUOM, db, model.UserID, returnLastOrderUOM).ToList();

                        #endregion Code to update IsInPantry, IsInCart and Prices data for products

                        var cartTotal = result.Sum(s => s.PriceTotal);
                        var cartTotalWithGST = result.Sum(s => s.TotalWithGST);

                        var isAuthCount = (from a in result
                                           where a.OrderUnitName.ToLower() == "kg" || a.OrderUnitName.ToLower() == "kgs" || a.OrderUnitName.ToLower() == "kilo" || a.OrderUnitName.ToLower() == "kilogram"
                                           select new
                                           {
                                               a.ProductCode
                                           }).ToList();

                        var minCart = db.Discounts.FirstOrDefault();

                        return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                        {
                            CartItems = result,
                            CartTotal = cartTotal,
                            CartTotalGST = cartTotalWithGST,
                            AuthPayment = (isAuthCount != null && isAuthCount.Count > 0) ? true : false,
                            MinCartValue = minCart.MinCartValue ?? 0,
                            Coupon = tempCart.Coupon ?? "",
                            IsCouponApplied = tempCart.IsCouponApplied ?? false,
                            CouponAmount = tempCart.CouponAmount ?? 0
                        });
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        public List<CartItemsBO> GetPricesLogicCart(long customerID, List<CartItemsBO> result, bool isDynamicUOM, Saavi5Entities _db, long userID, bool lastOrderUom)
        {
            foreach (var item in result)
            {
                long productID = item.ProductID;
                if (lastOrderUom)
                {
                    var lastUom = (from tcm in _db.trnsCartMasters
                                   join tcim in _db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                   where tcm.UserID == userID
                                   orderby tcm.CreatedDate descending
                                   group tcim by tcim.ProductID into g
                                   select new LastOrderUOMBO { ProductID = g.FirstOrDefault().ProductID, UOMID = g.FirstOrDefault().UnitId }).FirstOrDefault();

                    if (lastUom != null)
                    {
                        item.LastOrderUOMID = lastUom.UOMID;
                    }
                }

                var prod = _db.ProductMasters.Find(productID);

                if (isDynamicUOM)
                {
                    item.DynamicUOM = (from a in _db.UnitOfMeasurementMasters
                                       join b in _db.AltUOMMasters on a.UOMID equals b.UOMID
                                       where b.ProductID == item.ProductID // add check for manage features
                                       select new DynamicUnitPrices
                                       {
                                           UOMID = a.UOMID,
                                           UOMDesc = a.Code,
                                           QuantityPerUnit = b.QtyPerUnitOfMeasure ?? 1
                                       }).ToList();

                    foreach (var price in item.DynamicUOM)
                    {
                        var priceData = GetPrice(customerID, item.ProductID, item.ProdPrice.To<Double>(), price.UOMID);

                        price.Price = (price.QuantityPerUnit > 1 ? (priceData.Price * price.QuantityPerUnit) : priceData.Price).To<double>();
                        price.IsSpecial = priceData.IsSpecial;
                        price.IsPromotional = priceData.IsPromotional;
                        price.CostPrice = (price.QuantityPerUnit > 1 ? (item.CompanyPrice * price.QuantityPerUnit) : item.CompanyPrice).To<double>();

                        if (item.OrderUnitId == price.UOMID)
                        {
                            item.PriceTotal = price.Price * item.Quantity ?? 0;
                        }
                    }

                    if (item.DynamicUOM == null || item.DynamicUOM.Count == 0)
                    {
                        var dPrice = new DynamicUnitPrices();
                        var priceData = GetPrice(customerID, item.ProductID, item.ProdPrice.To<double>(), item.UOMID.To<long>());

                        var qty = _db.AltUOMMasters.Where(a => a.UOMID == item.OrderUnitId && a.ProductID == item.ProductID).FirstOrDefault();
                        if (qty != null)
                        {
                            item.Price = priceData.Price * qty.QtyPerUnitOfMeasure ?? 1;
                            item.PriceTotal = item.Price * item.Quantity;
                            dPrice.QuantityPerUnit = qty.QtyPerUnitOfMeasure ?? 1;
                        }
                        else
                        {
                            var upc = (item.UnitsPerCarton == null || item.UnitsPerCarton == 0) ? 1 : item.UnitsPerCarton;

                            item.Price = priceData.Price;
                            item.PriceTotal = item.Price * item.Quantity;
                            dPrice.QuantityPerUnit = upc;
                        }
                        item.IsSpecial = priceData.IsSpecial;

                        dPrice.UOMDesc = item.OrderUnitName;
                        dPrice.UOMID = item.OrderUnitId;
                        dPrice.Price = priceData.Price * dPrice.QuantityPerUnit.To<double>();
                        dPrice.IsPromotional = priceData.IsPromotional;
                        dPrice.IsSpecial = priceData.IsSpecial;
                        dPrice.CostPrice = item.CompanyPrice * dPrice.QuantityPerUnit.To<double>();

                        item.DynamicUOM.Add(dPrice);
                    }
                }
                else
                {
                    var prices = GetPrice(customerID, item.ProductID, item.ProdPrice.To<Double>(), item.UOMID.To<Int64>());
                    var qty = _db.AltUOMMasters.Where(a => a.UOMID == item.OrderUnitId && a.ProductID == item.ProductID).FirstOrDefault();

                    var upc = (item.UnitsPerCarton == null || item.UnitsPerCarton == 0) ? 1 : item.UnitsPerCarton;

                    if (qty != null)
                    {
                        item.Price = prices.Price * qty.QtyPerUnitOfMeasure ?? 1;
                        item.PriceTotal = item.Price * item.Quantity;
                    }
                    else
                    {
                        item.Price = prices.Price;
                        item.PriceTotal = item.Price * item.Quantity;
                    }

                    item.IsSpecial = prices.IsSpecial;
                    item.Prices = prices;
                    item.Prices.UOMID = item.OrderUnitId;
                    item.Prices.UOMDesc = item.OrderUnitName;
                    item.Prices.CostPrice = item.CompanyPrice * upc ?? 1;
                    item.Prices.QuantityPerUnit = upc ?? 1;

                    item.Price = prices.Price;
                    item.UOMID = prices.UOMID;
                }
            }

            return result;
        }

        #endregion Get items in temp cart of a user / customer

        #region Get Price and price type for products

        private PriceBO GetPrice_Old(long customerId, long productId, double Price, Int64 UOMID, CustomerMaster customer = null)
        {
            using (var _db = new Saavi5Entities())
            {
                PriceBO pricedata = new PriceBO();

                if (customer == null)
                {
                    customer = _db.CustomerMasters.Where(s => s.CustomerID == customerId).FirstOrDefault();
                }
                if (customer == null)
                {
                    pricedata.Price = Price;
                }
                else
                {
                    DateTime date = HelperClass.GetCurrentTime();
                    string CusPriceLevel = customer.PriceLevel == null ? "" : customer.PriceLevel.ToString();
                    var special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == UOMID).FirstOrDefault();
                    if (special == null)
                    {
                        special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == null).FirstOrDefault();
                    }

                    var prlevel_type1 = _db.PriceLevels.Where(s => s.PriceLevel1 == CusPriceLevel && s.ProductID == productId && (s.StartDate == null || s.EndDate == null || (s.EndDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndDate)))).FirstOrDefault();

                    double prc = Price;
                    if (prlevel_type1 != null)
                    {
                        if (prlevel_type1.Price != null && (prlevel_type1.Price < prc || prc == 0))
                        {
                            pricedata.IsPromotional = true;
                            prc = Convert.ToDouble(prlevel_type1.Price);
                        }
                    }
                    pricedata.Price = prc;
                    if (special != null)
                    {
                        if (HelperClass.AllowHighSpecialPrice)
                        {
                            double sprc = Convert.ToDouble(special.SpecialPrice == null ? 0 : special.SpecialPrice);
                            pricedata.IsSpecial = true;
                            pricedata.Price = sprc;
                        }
                        else
                        {
                            double sprc = Convert.ToDouble(special.SpecialPrice == null ? 0 : special.SpecialPrice);
                            if (sprc < prc || prc == 0)
                            {
                                pricedata.IsSpecial = true;
                                pricedata.Price = sprc;
                            }
                        }
                    }
                }
                return pricedata;
            }
        }

        private PriceBO GetPrice(long customerId, long productId, double Price, Int64 UOMID, CustomerMaster customer = null)
        {
            using (var _db = new Saavi5Entities())
            {
                PriceBO pricedata = new PriceBO();

                if (customer == null)
                {
                    customer = _db.CustomerMasters.Where(s => s.CustomerID == customerId).FirstOrDefault();
                }
                if (customer == null)
                {
                    pricedata.Price = Price;
                }
                else
                {
                    DateTime date = HelperClass.GetCurrentTime();
                    var special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == UOMID).FirstOrDefault();
                    if (special == null)
                    {
                        special = _db.SpecialPriceMasters.Where(s => s.IsActive == true && s.CustomerID == customerId && s.ProductID == productId && (s.StartDate == null || s.EndingDate == null || (s.EndingDate.Value.Year == 1753 || (date >= s.StartDate.Value && date <= s.EndingDate))) && s.UOMID == null).FirstOrDefault();
                    }

                    double prc = Price;

                    double PricefromPriceCode = HelperClass.GetProductPrice(productId, customer.PRICE_CODE.To<int>());
                    if (PricefromPriceCode > 0)
                    {
                        prc = PricefromPriceCode;
                        pricedata.IsPromotional = true;
                        pricedata.Price = prc;
                    }
                    else
                    {
                        prc = Price;
                        pricedata.IsPromotional = false;
                        pricedata.Price = prc;
                    }

                    if (special != null && special.SpecialPrice != null)
                    {
                        double sprc = Convert.ToDouble(special.SpecialPrice);

                        pricedata.IsSpecial = true;
                        pricedata.Price = sprc;
                    }
                    //pricedata.Price = prc;
                }
                return pricedata;
            }
        }

        #endregion Get Price and price type for products

        #region Get Latest specials

        /// <summary>
        /// Get Latest special items
        /// </summary>
        /// <param name="searchFilters"></param>
        /// <returns></returns>
        public async Task<APiResponse> GetLatestSpecials(ProductFilters searchFilters)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var customer = _db.CustomerMasters.Find(searchFilters.CustomerID);
                    var customerCatFilter = customer;
                    if (searchFilters.listCustomerId > 0)
                    {
                        customerCatFilter = _db.CustomerMasters.Find(searchFilters.listCustomerId);
                    }

                    string warehouse = customer.Warehouse ?? "";
                    long? userIDForfeatures = searchFilters.UserID;
                    if (searchFilters.IsRepUser)
                    {
                        userIDForfeatures = _db.UserMasters.Where(u => u.CustomerID == searchFilters.CustomerID).FirstOrDefault().UserID;
                    }

                    var customerFeatures = _db.CustomerFeaturesMasters.Where(x => x.UserID == userIDForfeatures).FirstOrDefault();
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();

                    bool isDynamicUOM = false;
                    if (customerFeatures != null)
                    {
                        isDynamicUOM = customerFeatures.IsDynamicUOM.To<bool>();
                    }

                    bool showOnlyStockItems = false;
                    bool returnLastOrderUOM = false;
                    if (mainFeatures != null)
                    {
                        showOnlyStockItems = mainFeatures.IsShowOnlyStockItems.To<bool>();
                        returnLastOrderUOM = mainFeatures.ReturnLastOrderUOM.To<bool>();
                    }

                    string TagNames = customerCatFilter != null ? Convert.ToString(customerCatFilter.TagNames == null ? "" : customerCatFilter.TagNames) : "";
                    bool IsSpecialCategory = customerCatFilter != null ? Convert.ToBoolean(customerCatFilter.IsSpecialCategory) : false;
                    string[] TagNamesArray = null;
                    TagNamesArray = TagNames.Split(new char[] { ',' });

                    IOrderedQueryable<SearchProductsBO> query;

                    query = (from prod in _db.ProductMasters
                             join latest in _db.LatestSpecialsMasters on prod.ProductID equals latest.ProductID
                             // Left join with special prices.
                             join special in _db.SpecialPriceMasters.Where(s => s.CustomerID == searchFilters.CustomerID) on prod.ProductID equals special.ProductID into temp
                             from special in temp.DefaultIfEmpty()

                             join uom in _db.UnitOfMeasurementMasters on prod.UOMID equals uom.UOMID
                             join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                             join mc in _db.MainCategoriesMasters on cat.MCategoryId equals mc.MCategoryId
                             join pf in _db.ProductFiltersMasters on prod.FilterID equals pf.FilterID

                             // left join on stock warehouse table.
                             join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into sw
                             from soh in sw.DefaultIfEmpty()
                                 //Left join with AlternateUom master
                                 //join au in _db.AltUOMMasters on new { UOM = uom.UOMID.ToString(), Product = p.ProductID.ToString() } equals new { UOM = au.UOMID.ToString(), Product = au.ProductID.ToString() } into auTemp
                                 //from au in auTemp.DefaultIfEmpty()

                             join lou in (
                                from tcm in _db.trnsCartMasters
                                join tcim in _db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                where tcm.UserID == searchFilters.UserID
                                orderby tcm.CreatedDate descending
                                group tcim by tcim.ProductID into g
                                select new LastOrderUOMBO { ProductID = g.FirstOrDefault().ProductID, UOMID = g.FirstOrDefault().UnitId }
                             ) on prod.ProductID equals lou.ProductID into tempLOU
                             from lou in tempLOU.DefaultIfEmpty()

                             where (latest.IsActive == true && prod.IsActive == true && cat.IsActive == true && mc.IsActive == true && pf.IsActive == true
                               && (warehouse == "" || (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse))
                               && (((cat.IsSpecialCategory == false || cat.IsSpecialCategory == null) || (cat.IsSpecialCategory == true && IsSpecialCategory == true && TagNamesArray.Contains(cat.TagNames)))))

                             select new SearchProductsBO
                             {
                                 ProductID = prod.ProductID,
                                 ProductName = prod.ProductName,
                                 ProductCode = prod.ProductCode,
                                 Description = prod.ProductDescription ?? "",
                                 Description2 = prod.ProductDescription2 ?? "",
                                 Description3 = prod.ProductDescription3 ?? "",
                                 Feature1 = prod.ProductFeature1 ?? "",
                                 Feature2 = prod.ProductFeature2 ?? "",
                                 Feature3 = prod.ProductFeature3 ?? "",
                                 IsNew = prod.IsNew ?? false,
                                 IsOnSale = prod.IsOnSale ?? false,
                                 IsBackSoon = prod.IsBackSoon ?? false,
                                 Barcode = prod.BarCodeText,
                                 ProductImage = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + prod.ProductImage : "",
                                 ProductImageThumb = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "admin/content/Uploads/Products/" + "thumb_" + prod.ProductImage : "",
                                 UOMID = uom.UOMID,
                                 UOMDesc = uom.Code,
                                 LastOrderUOMID = lou.UOMID ?? 0,
                                 FilterID = prod.FilterID,
                                 FilterName = pf.FilterName,
                                 CategoryName = cat.CategoryName,
                                 MainCategoryName = mc.MCategoryName,
                                 CategoryID = prod.CategoryID,
                                 Price = prod.Price ?? 0,
                                 SellBelowCost = prod.SellBelowCost ?? false,
                                 CompanyPrice = prod.ComPrice ?? 0,
                                 IsActive = prod.IsActive,
                                 BuyIn = prod.BuyIn ?? false,
                                 IsStatusIN = (prod.ERPStatus == "IN") ? true : false,
                                 IsAvailable = prod.IsAvailable ?? false,
                                 UnitsPerCarton = prod.UnitsPerCarton == null ? 1 : prod.UnitsPerCarton,
                                 PiecesAndWeight = prod.PiecesAndWeight ?? false,// Convert.ToBoolean((prod.PiecesAndWeight == null) ? false : prod.PiecesAndWeight),
                                 IsCountrywideRewards = prod.RewardItem ?? false,                                     //stockwarehouse
                                 IsGST = prod.GST ?? false,
                                 IsSpecialCategory = cat.IsSpecialCategory ?? false,
                                 StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                                 Brand = prod.Brand == null ? "" : prod.Brand,
                                 Supplier = prod.Supplier == null ? "" : prod.Supplier.Trim(),
                                 MinOQ = prod.MinOQ ?? 0,
                                 MaxOQ = prod.MaxOQ ?? 0,
                                 ProductImages = (from a in _db.ProductImageMasters
                                                  where a.ProductID == prod.ProductID
                                                  select new ProductImages
                                                  {
                                                      ImageName = a.BaseUrl + a.ProductImage
                                                  }).ToList(),
                                 ShareURL = prod.ShareURL ?? "",
                                 ProductFeature = prod.ProductFeature ?? "",
                                 IsDonationBox = prod.IsDonationBox ?? false
                             }).Where(s => showOnlyStockItems == false || s.StockQuantity > 0).OrderBy(p => p.ProductID);

                    var result = new List<SearchProductsBO>();

                    if (searchFilters.PageIndex == 0)
                    {
                        result = await query.ToListAsync();
                    }
                    else
                    {
                        result = query.Page(searchFilters.PageIndex, searchFilters.PageSize).ToList();
                    }

                    if (result.Count == 0)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                    }

                    #region Code to update IsInPantry, IsInCart and Prices data for products

                    result = UpdateIsInPantryAndIsInCart(searchFilters, result, isDynamicUOM, _db, searchFilters.UserID.To<long>(), returnLastOrderUOM).ToList();

                    #endregion Code to update IsInPantry, IsInCart and Prices data for products

                    if (searchFilters.IsSuggestive)
                    {
                        return new APiResponse
                        {
                            ResponseCode = SaaviErrorCodes.Success,
                            Message = "Success",
                            Result = result
                        };
                    }
                    else
                    {
                        return new APiResponse
                        {
                            ResponseCode = SaaviErrorCodes.Success,
                            Message = "Success",
                            Result = new { LatestSpecials = result, TotalResults = query.Count(), TotalPages = (query.Count() + searchFilters.PageSize - 1) / searchFilters.PageSize }
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get Latest specials

        #region Delete favorite list

        /// <summary>
        /// Deletes the favorite list for a customer.
        /// </summary>
        /// <param name="obj">PantryListID</param>
        /// <returns></returns>
        public async Task<APiResponse> DeleteFavoriteList(DeleteFavoriteBO obj)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var pantry = _db.PantryListMasters.Find(obj.PantryListID);
                    if (pantry.PantryListType == "C" || pantry.PantryListType == "CC")
                    {
                        var pantryItems = _db.PantryListItemsMasters.Where(p => p.PantryListID == obj.PantryListID).ToList();

                        _db.PantryListItemsMasters.RemoveRange(pantryItems);
                        _db.SaveChanges();

                        _db.PantryListMasters.Remove(pantry);
                        await _db.SaveChangesAsync();

                        return ReturnMethod(SaaviErrorCodes.Success, "Favorite List deleted successfully.");
                    }
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.Success, "Default Pantry List can not be deleted.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Delete favorite list

        #region Sort items in pantry list

        /// <summary>
        /// Set pantry sort order
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<APiResponse> SetPantryItemsSortOrder(PantrySortBO obj)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var pantry = _db.PantryListMasters.Find(obj.PantryListID);

                    if (pantry != null)
                    {
                        pantry.IsSorted = true;
                        await _db.SaveChangesAsync();
                    }

                    int index = 0;
                    foreach (var product in obj.ProductID)
                    {
                        var pantryItems = _db.PantryListItemsMasters.Where(p => p.PantryListID == obj.PantryListID && p.ProductID == product).FirstOrDefault();

                        if (pantryItems != null)
                        {
                            pantryItems.ShowOrder = index;
                            pantryItems.ModifiedDate = DateTime.Now;
                            _db.SaveChanges();
                            index++;
                        }
                    }
                    return ReturnMethod(SaaviErrorCodes.Success, "Pantry Items sort order saved.");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Sort items in pantry list

        #region Item Enquiry Popup

        /// <summary>
        /// Send Enwuiry email to the respective mail.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<APiResponse> ItemEnquiry(ItemEnquiryBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var customer = _db.CustomerMasters.Find(model.CustomerID);
                    var user = _db.UserMasters.Find(model.UserID);
                    var product = _db.ProductMasters.Find(model.ProductID);

                    string html = "Dear Admin," +
                        "<br>We have received an enquiry for product " + product.ProductCode + ", Product Name : " + product.ProductName + "." +
                        "<br><br>Customer Name:  " + customer.CustomerName + "" +
                        "<br>Email: " + customer.Email + "" +
                        "<br>Contact: " + customer.ContactName + "" +
                        "<br>Phone: " + customer.Phone1 + "" +
                        "<br><br> Item Comment: <br> " + model.Comment + "";

                    SendMail.SendEMail(HelperClass.ToEmail, "", "", "Item Enquiry - " + product.ProductCode, html, "", HelperClass.FromEmail);
                    return ReturnMethod(SaaviErrorCodes.Success, "Your enquiry has been sent to the respective department.");
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Item Enquiry Popup

        #region Get Details of a single product

        public async Task<APiResponse> GetProductDetails(ProductDetailBO searchFilters)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    string warehouse = _db.CustomerMasters.Find(searchFilters.CustomerID).Warehouse ?? "";

                    long? userIDForfeatures = searchFilters.UserID;
                    if (searchFilters.IsRepUser)
                    {
                        userIDForfeatures = _db.UserMasters.Where(u => u.CustomerID == searchFilters.CustomerID).FirstOrDefault().UserID;
                    }
                    var customer = await _db.CustomerMasters.FindAsync(searchFilters.CustomerID);
                    var customerFeatures = _db.CustomerFeaturesMasters.Where(x => x.UserID == userIDForfeatures).FirstOrDefault();
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();

                    bool isDynamicUOM = false;
                    if (customerFeatures != null)
                    {
                        isDynamicUOM = customerFeatures.IsDynamicUOM.To<bool>();
                    }

                    bool showOnlyStockItems = false;
                    bool returnLastOrderUOM = false;
                    if (mainFeatures != null)
                    {
                        showOnlyStockItems = mainFeatures.IsShowOnlyStockItems.To<bool>();
                        returnLastOrderUOM = mainFeatures.ReturnLastOrderUOM.To<bool>();
                    }
                    bool IsSpecialCategory = customer != null ? Convert.ToBoolean(customer.IsSpecialCategory) : false;

                    IOrderedQueryable<SearchProductsBO> query;

                    query = (
                        from prod in _db.ProductMasters
                        join uom in _db.UnitOfMeasurementMasters on prod.UOMID equals uom.UOMID
                        join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                        join mc in _db.MainCategoriesMasters on cat.MCategoryId equals mc.MCategoryId
                        join pf in _db.ProductFiltersMasters on prod.FilterID equals pf.FilterID

                        // left join on stock warehouse table.
                        join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into sw
                        from soh in sw.DefaultIfEmpty()
                            //Left join with AlternateUom master
                            //join au in _db.AltUOMMasters on new { UOM = uom.UOMID.ToString(), Product = p.ProductID.ToString() } equals new { UOM = au.UOMID.ToString(), Product = au.ProductID.ToString() } into auTemp
                            //from au in auTemp.DefaultIfEmpty()

                        where (prod.ProductID == searchFilters.ProductID
                                            //&& ((cat.IsSpecialCategory == false || cat.IsSpecialCategory == null)
                                            //    || (cat.IsSpecialCategory == true && IsSpecialCategory == true))
                                            && (warehouse == "" || (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse))
                                            )
                        //&& TagNamesArray.Contains(Convert.ToString(cats.TagName))
                        select new SearchProductsBO
                        {
                            ProductID = prod.ProductID,
                            ProductName = prod.ProductName,
                            ProductCode = prod.ProductCode,
                            Description = prod.ProductDescription ?? "",
                            Description2 = prod.ProductDescription2 ?? "",
                            Description3 = prod.ProductDescription3 ?? "",
                            Feature1 = prod.ProductFeature1 ?? "",
                            Feature2 = prod.ProductFeature2 ?? "",
                            Feature3 = prod.ProductFeature3 ?? "",
                            IsNew = prod.IsNew ?? false,
                            IsOnSale = prod.IsOnSale ?? false,
                            IsBackSoon = prod.IsBackSoon ?? false,
                            Barcode = prod.BarCodeText ?? "",
                            ProductImage = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "Admin/content/Uploads/Products/" + prod.ProductImage : "",
                            ProductImageThumb = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "Admin/content/Uploads/Products/" + "thumb_" + prod.ProductImage : "",
                            UOMID = uom.UOMID,
                            UOMDesc = uom.Code,
                            FilterID = prod.FilterID,
                            FilterName = pf.FilterName,
                            CategoryName = cat.CategoryName,
                            MainCategoryName = mc.MCategoryName,
                            IsCountrywideRewards = prod.RewardItem ?? false,
                            CategoryID = prod.CategoryID,
                            Price = prod.Price ?? 0,
                            BuyIn = prod.BuyIn ?? false,
                            SellBelowCost = prod.SellBelowCost ?? false,
                            IsStatusIN = (prod.ERPStatus == "IN") ? true : false,
                            CompanyPrice = prod.ComPrice ?? 0,
                            IsActive = prod.IsActive,
                            IsAvailable = prod.IsAvailable ?? false,
                            UnitsPerCarton = prod.UnitsPerCarton == null ? 1 : prod.UnitsPerCarton,
                            PiecesAndWeight = prod.PiecesAndWeight ?? false,// Convert.ToBoolean((prod.PiecesAndWeight == null) ? false : prod.PiecesAndWeight),
                            IsGST = prod.GST ?? false,
                            IsSpecialCategory = cat.IsSpecialCategory ?? false,
                            StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                            Brand = prod.Brand == null ? "" : prod.Brand,
                            Supplier = prod.Supplier == null ? "" : prod.Supplier.Trim(),
                            ProductImages = (from a in _db.ProductImageMasters
                                             where a.ProductID == prod.ProductID
                                             select new ProductImages
                                             {
                                                 ImageName = a.BaseUrl + a.ProductImage
                                             }).ToList(),
                            Quantity = 0,
                            MinOQ = prod.MinOQ ?? 0,
                            MaxOQ = prod.MaxOQ ?? 0,
                            ShareURL = prod.ShareURL ?? "",
                            ProductFeature = prod.ProductFeature ?? "",
                            IsDonationBox = prod.IsDonationBox ?? false
                        }).Where(s => showOnlyStockItems == false || s.StockQuantity > 0).OrderBy(p => p.ProductID);

                    var result = query.ToList();
                    if (result == null)
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "Product data not available.");
                    }

                    #region Code to update IsInPantry, IsInCart and Prices data for products

                    result = UpdateIsInPantryAndIsInCartProductDetail(searchFilters, result, isDynamicUOM, _db, searchFilters.UserID, returnLastOrderUOM);

                    #endregion Code to update IsInPantry, IsInCart and Prices data for products

                    return new APiResponse
                    {
                        ResponseCode = SaaviErrorCodes.Success,
                        Message = "Success",
                        Result = new
                        {
                            product = result.FirstOrDefault(),
                            Suggestive = result
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        /// <summary>
        /// Get pantry detals for a single product
        /// </summary>
        /// <param name="searchFilters"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private List<SearchProductsBO> UpdateIsInPantryAndIsInCartProductDetail(ProductDetailBO searchFilters, List<SearchProductsBO> results, bool isDynamicUOM, Saavi5Entities _db, long userID, bool lastOrderUom)
        {
            foreach (var result in results)
            {
                long productID = result.ProductID;
                if (lastOrderUom)
                {
                    var lastUom = (from tcm in _db.trnsCartMasters
                                   join tcim in _db.trnsCartItemsMasters on tcm.CartID equals tcim.CartID
                                   where tcm.UserID == userID
                                   orderby tcm.CreatedDate descending
                                   group tcim by tcim.ProductID into g
                                   select new LastOrderUOMBO { ProductID = g.FirstOrDefault().ProductID, UOMID = g.FirstOrDefault().UnitId }).FirstOrDefault();

                    if (lastUom != null)
                    {
                        result.LastOrderUOMID = lastUom.UOMID.To<long>();
                    }
                }
                var data = (from cart in _db.trnsTempCartMasters
                            join cartItems in _db.trnsTempCartItemsMasters on cart.CartID equals cartItems.CartID
                            where cart.CustomerID == searchFilters.CustomerID
                                  && cart.RepUserID == searchFilters.UserID
                                  && cartItems.ProductID == productID && cart.IsSavedOrder == false
                            select new { cart.CartID, cartItems.Quantity, cartItems.UnitId }).FirstOrDefault();

                if (data != null)
                {
                    result.IsInCart = true;
                    result.Quantity = data.Quantity ?? 0;
                    result.UOMID = data.UnitId.To<long>();
                }
                else
                {
                    result.IsInCart = false;
                }

                var pantryData = (from pantry in _db.PantryListMasters
                                  join pantryItem in _db.PantryListItemsMasters on pantry.PantryListID equals pantryItem.PantryListID
                                  where pantry.CustomerID == searchFilters.CustomerID && pantryItem.ProductID == productID
                                  select new
                                  {
                                      pantryItem
                                  }
                                 ).FirstOrDefault();

                if (pantryData != null)
                {
                    result.IsInPantry = true;
                    result.PantryListID = pantryData.pantryItem.PantryListID;
                    result.WeeklySales = new WeeklySalesBO()
                    {
                        Week1Sales = pantryData.pantryItem.Week1Sales,
                        Week2Sales = pantryData.pantryItem.Week2Sales,
                        Week3Sales = pantryData.pantryItem.Week3Sales,
                        Week4Sales = pantryData.pantryItem.Week4Sales,
                        Week5Sales = pantryData.pantryItem.Week5Sales,
                        Week6Sales = pantryData.pantryItem.Week6Sales,
                        Week7Sales = pantryData.pantryItem.Week7Sales,
                        Week8Sales = pantryData.pantryItem.Week8Sales,
                        Week9Sales = pantryData.pantryItem.Week9Sales,
                        Week10Sales = pantryData.pantryItem.Week10Sales,
                        Week11Sales = pantryData.pantryItem.Week11Sales,
                        Week12Sales = pantryData.pantryItem.Week12Sales,
                        Week13Sales = pantryData.pantryItem.Week13Sales,
                        Week14Sales = pantryData.pantryItem.Week14Sales,
                        Week15Sales = pantryData.pantryItem.Week15Sales,
                        Week16Sales = pantryData.pantryItem.Week16Sales,
                    };

                    Mapper.Map(pantryData.pantryItem, result.WeeklySales);
                }
                else
                {
                    result.IsInPantry = false;
                }

                if (isDynamicUOM)
                {
                    result.DynamicUOM = (from a in _db.UnitOfMeasurementMasters
                                         join b in _db.AltUOMMasters on a.UOMID equals b.UOMID
                                         where b.ProductID == result.ProductID // add check for manage features
                                         select new DynamicUnitPrices
                                         {
                                             UOMID = a.UOMID,
                                             UOMDesc = a.Code,
                                             QuantityPerUnit = b.QtyPerUnitOfMeasure ?? 1
                                         }).ToList();

                    foreach (var price in result.DynamicUOM)
                    {
                        var priceData = GetPrice(searchFilters.CustomerID, result.ProductID, result.Price, price.UOMID);

                        price.Price = (price.QuantityPerUnit > 1 ? (priceData.Price * price.QuantityPerUnit) : priceData.Price).To<double>();// priceData.Price;
                        price.IsSpecial = priceData.IsSpecial;
                        price.IsPromotional = priceData.IsPromotional;
                        price.CostPrice = (price.QuantityPerUnit > 1 ? (result.CompanyPrice * price.QuantityPerUnit) : result.CompanyPrice).To<double>();
                    }

                    if (result.DynamicUOM == null || result.DynamicUOM.Count == 0)
                    {
                        var dPrice = new DynamicUnitPrices();
                        var priceData = GetPrice(searchFilters.CustomerID, result.ProductID, result.Price.To<double>(), result.UOMID);

                        var upc = (result.UnitsPerCarton == null || result.UnitsPerCarton == 0) ? 1 : result.UnitsPerCarton;

                        dPrice.UOMDesc = result.UOMDesc;
                        dPrice.UOMID = result.UOMID;
                        dPrice.QuantityPerUnit = result.UnitsPerCarton ?? 1;
                        dPrice.Price = priceData.Price * upc ?? 1;
                        dPrice.IsPromotional = false;
                        dPrice.IsSpecial = priceData.IsSpecial;
                        dPrice.CostPrice = result.CompanyPrice * upc ?? 1;
                        result.DynamicUOM.Add(dPrice);
                    }
                }
                else
                {
                    var upc = (result.UnitsPerCarton == null || result.UnitsPerCarton == 0) ? 1 : result.UnitsPerCarton;

                    result.Prices = GetPrice(searchFilters.CustomerID, result.ProductID, result.Price, result.UOMID);
                    result.Prices.UOMID = result.UOMID;
                    result.Prices.UOMDesc = result.UOMDesc;
                    result.Prices.CostPrice = result.CompanyPrice * upc ?? 1;
                    result.Prices.QuantityPerUnit = upc ?? 1;
                }
            }
            return results;
        }

        #endregion Get Details of a single product

        #region Special product request

        public async Task<APiResponse> SendSpecialProductRequest(SpecialProductReqBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    var customer = await _db.CustomerMasters.Where(s => s.CustomerID == model.CustomerID).FirstOrDefaultAsync();
                    if (customer != null)
                    {
                        string mailContent = File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplates/productrequestadmin.htm"));

                        mailContent = mailContent.Replace("@ContactName", customer.ContactName);
                        mailContent = mailContent.Replace("@Account", (customer.AlphaCode == null) ? "" : customer.AlphaCode);
                        mailContent = mailContent.Replace("@Phone", customer.Phone1 == null ? "" : customer.Phone1);
                        mailContent = mailContent.Replace("@ContactName", customer.ContactName);

                        string cusEmail = Convert.ToString(customer.Email == null ? "" : customer.Email);
                        if (customer.StateID != null && customer.StateID != 0)
                        {
                            string state = Convert.ToString((from cd in _db.StateMasters.Where(s => s.StateID == customer.StateID).Take(1)
                                                             select new
                                                             {
                                                                 cd.StateName
                                                             }).ToList().Select(c => c.StateName).First());
                            mailContent = mailContent.Replace("@State", state);
                        }
                        else
                        {
                            mailContent = mailContent.Replace("@State", "");
                        }
                        mailContent = mailContent.Replace("@Postcode", customer.PostCode);
                        mailContent = mailContent.Replace("@ABN", (customer.ABN == null) ? "" : customer.ABN);
                        mailContent = mailContent.Replace("@RequestDate", String.Format("{0:dd/MM/yyyy}", HelperClass.GetCurrentTime()));
                        mailContent = mailContent.Replace("@RequestTime", HelperClass.GetCurrentTime().ToString("t"));
                        mailContent = mailContent.Replace("@path", HelperClass.WebsiteUrl);
                        mailContent = mailContent.Replace("@Company", HelperClass.Company);

                        //For customer login
                        string customername = (customer.CustomerName == null) ? "" : customer.CustomerName;
                        mailContent = mailContent.Replace("@CustomerName", customername);
                        mailContent = mailContent.Replace("@Email", cusEmail);
                        mailContent = mailContent.Replace("@Details", model.Details);

                        SendMail.SendEMail(HelperClass.AdminEmail, "", "", HelperClass.Company + " - Product Request", mailContent, "", HelperClass.FromEmail);

                        return ReturnMethod(SaaviErrorCodes.Success, "Success");
                    }   // Success !
                    else
                    {
                        return ReturnMethod(SaaviErrorCodes.NoData, "No Data");
                    }
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Special product request

        #region Get customer Suggestive and specials items

        public async Task<APiResponse> GetSuggestiveItems(SuggestiveItemsModel model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    if (model.CartID == 0)
                    {
                        model.CartID = _db.trnsTempCartMasters.Where(c => c.CustomerID == model.CustomerID && c.IsSavedOrder == false).FirstOrDefault().CartID;
                    }

                    var tempCart = _db.trnsTempCartItemsMasters.Where(t => t.CartID == model.CartID);

                    var date = DateTime.Now.AddDays(-30);
                    var prevCartItems = (from a in _db.trnsCartMasters
                                         join b in _db.trnsCartItemsMasters on a.CartID equals b.CartID

                                         join c in tempCart on b.ProductID equals c.ProductID into temp2
                                         from c in temp2.DefaultIfEmpty()

                                         where a.CustomerID == model.CustomerID && c.CartItemID == null && DbFunctions.TruncateTime(a.OrderDate) > DbFunctions.TruncateTime(date)
                                         select new
                                         {
                                             b.ProductID
                                         }).Distinct().ToList();

                    var prods = new List<long>();
                    foreach (var prod in prevCartItems)
                    {
                        prods.Add(prod.ProductID);
                    }

                    var prodBO = new ProductDetailBO()
                    {
                        Products = prods,
                        CustomerID = model.CustomerID,
                        UserID = model.UserID,
                        IsRepUser = false,
                        ProductID = 0
                    };

                    var prodDetails = this.GetSuggestiveProducts(prodBO);

                    List<long> suggestiveProds = _db.SuggestiveItemsMasters.Select(x => x.ProductID).ToList();

                    prodBO = new ProductDetailBO()
                    {
                        Products = suggestiveProds,
                        CustomerID = model.CustomerID,
                        UserID = model.UserID,
                        IsRepUser = false,
                        ProductID = 0
                    };

                    var suggestiveItems = this.GetSuggestiveProducts(prodBO);

                    return ReturnMethod(SaaviErrorCodes.Success, "Success", new
                    {
                        Specials = suggestiveItems,
                        SuggestiveItems = HelperClass.ShowSuggestiveCart ? prodDetails : new List<SearchProductsBO>()
                    });
                }
            }
            catch (Exception ex)
            {
                return ReturnMethod(SaaviErrorCodes.Error, ex.Message);
            }
        }

        #endregion Get customer Suggestive and specials items

        #region Get suggestive products

        public List<SearchProductsBO> GetSuggestiveProducts(ProductDetailBO model)
        {
            try
            {
                using (var _db = new Saavi5Entities())
                {
                    string warehouse = _db.CustomerMasters.Find(model.CustomerID).Warehouse ?? "";

                    var customerFeatures = _db.CustomerFeaturesMasters.Where(x => x.UserID == model.UserID).FirstOrDefault();
                    var mainFeatures = _db.MainFeaturesMasters.FirstOrDefault();

                    bool isDynamicUOM = false;
                    if (customerFeatures != null)
                    {
                        isDynamicUOM = customerFeatures.IsDynamicUOM.To<bool>();
                    }

                    bool showOnlyStockItems = false;
                    bool returnLastOrderUOM = false;
                    if (mainFeatures != null)
                    {
                        showOnlyStockItems = mainFeatures.IsShowOnlyStockItems.To<bool>();
                        returnLastOrderUOM = mainFeatures.ReturnLastOrderUOM.To<bool>();
                    }

                    IOrderedQueryable<SearchProductsBO> query;

                    query = (
                        from prod in _db.ProductMasters
                        join uom in _db.UnitOfMeasurementMasters on prod.UOMID equals uom.UOMID
                        join cat in _db.ProductCategoriesMasters on prod.CategoryID equals cat.CategoryID
                        join mc in _db.MainCategoriesMasters on cat.MCategoryId equals mc.MCategoryId
                        join pf in _db.ProductFiltersMasters on prod.FilterID equals pf.FilterID

                        // left join on stock warehouse table.
                        join soh in _db.StockWareHouses on prod.ProductID equals soh.ProductID into sw
                        from soh in sw.DefaultIfEmpty()

                        where (model.Products.Contains(prod.ProductID)
                        && (warehouse == "" || (soh.WareHouse != null && soh.WareHouse.ToString().ToLower().Trim() == warehouse)))
                        //&& TagNamesArray.Contains(Convert.ToString(cats.TagName))
                        select new SearchProductsBO
                        {
                            ProductID = prod.ProductID,
                            ProductName = prod.ProductName,
                            ProductCode = prod.ProductCode,
                            Description = prod.ProductDescription ?? "",
                            Description2 = prod.ProductDescription2 ?? "",
                            Description3 = prod.ProductDescription3 ?? "",
                            Feature1 = prod.ProductFeature1 ?? "",
                            Feature2 = prod.ProductFeature2 ?? "",
                            Feature3 = prod.ProductFeature3 ?? "",
                            IsNew = prod.IsNew ?? false,
                            IsOnSale = prod.IsOnSale ?? false,
                            IsBackSoon = prod.IsBackSoon ?? false,
                            Barcode = prod.BarCodeText ?? "",
                            ProductImage = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "Admin/content/Uploads/Products/" + prod.ProductImage : "",
                            ProductImageThumb = (prod.ProductImage != null && prod.ProductImage != "") ? HelperClass.WebsiteUrl + "Admin/content/Uploads/Products/" + "thumb_" + prod.ProductImage : "",
                            UOMID = uom.UOMID,
                            UOMDesc = uom.Code,
                            FilterID = prod.FilterID,
                            FilterName = pf.FilterName,
                            CategoryName = cat.CategoryName,
                            MainCategoryName = mc.MCategoryName,
                            IsCountrywideRewards = prod.RewardItem ?? false,
                            CategoryID = prod.CategoryID,
                            Price = prod.Price ?? 0,
                            BuyIn = prod.BuyIn ?? false,
                            SellBelowCost = prod.SellBelowCost ?? false,
                            IsStatusIN = (prod.ERPStatus == "IN") ? true : false,
                            CompanyPrice = prod.ComPrice ?? 0,
                            IsActive = prod.IsActive,
                            IsAvailable = prod.IsAvailable ?? false,
                            UnitsPerCarton = prod.UnitsPerCarton == null ? 1 : prod.UnitsPerCarton,
                            PiecesAndWeight = prod.PiecesAndWeight ?? false,// Convert.ToBoolean((prod.PiecesAndWeight == null) ? false : prod.PiecesAndWeight),
                            IsGST = prod.GST ?? false,
                            IsSpecialCategory = cat.IsSpecialCategory ?? false,
                            StockQuantity = soh.IsRwItem == true ? (soh.StockOnHandRW == null ? 0 : soh.StockOnHandRW) : (soh.StockOnHand == null ? 0 : soh.StockOnHand),
                            Brand = prod.Brand == null ? "" : prod.Brand,
                            Supplier = prod.Supplier == null ? "" : prod.Supplier.Trim(),
                            ProductImages = (from a in _db.ProductImageMasters
                                             where a.ProductID == prod.ProductID
                                             select new ProductImages
                                             {
                                                 ImageName = a.BaseUrl + a.ProductImage
                                             }).ToList(),
                            Quantity = 0,
                            MinOQ = prod.MinOQ ?? 0,
                            MaxOQ = prod.MaxOQ ?? 0,
                            ShareURL = prod.ShareURL ?? "",
                            ProductFeature = prod.ProductFeature ?? "",
                            IsDonationBox = prod.IsDonationBox ?? false
                        }).Where(s => showOnlyStockItems == false || s.StockQuantity > 0).OrderBy(p => p.ProductID);

                    var result = query.ToList();
                    if (result == null)
                    {
                        return null;
                    }

                    #region Code to update IsInPantry, IsInCart and Prices data for products

                    result = UpdateIsInPantryAndIsInCartProductDetail(model, result, isDynamicUOM, _db, model.UserID, returnLastOrderUOM);

                    #endregion Code to update IsInPantry, IsInCart and Prices data for products

                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Get suggestive products
    }
}