//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Saavi5Api.DataAccess.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class PermittedDaysMaster
    {
        public int ID { get; set; }
        public string DayName { get; set; }
        public string ShortName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> DayValue { get; set; }
        public string Code { get; set; }
        public string PermitBy { get; set; }
        public string PermitFrom { get; set; }
        public string PermitTo { get; set; }
        public Nullable<int> GroupID { get; set; }
    }
}
