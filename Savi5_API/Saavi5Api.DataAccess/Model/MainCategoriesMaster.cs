//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Saavi5Api.DataAccess.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class MainCategoriesMaster
    {
        public long MCategoryId { get; set; }
        public Nullable<long> CompanyID { get; set; }
        public string MCategoryName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> SyncCategoryId { get; set; }
        public string MCategoryCode { get; set; }
    
        public virtual CompanyMaster CompanyMaster { get; set; }
    }
}
